declare interface Environment {
    APP_BASE_HREF: string;
    CSN: string;
    LOCALE: string;
    IS_DEVELOPMENT: boolean;
    AUTHORIZATION: string;
    OPEN_ID_TOKEN: string;
    INDAGIA_START_DATE: string;
    API: string;
    API_VERSION: string;
    APP_CDN_URL: string;
}

declare interface AppEnvironment {
    isProduction: boolean;
}

declare let ENV: Environment;

declare let dataLayer: any[];
