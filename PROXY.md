# Connect to the new backend

## 1. Backend

Pull the backend from BitBucket, install all dependencies, start it and import the fixtures:

```bash
git pull accounting-reports

bin/composer.sh install

# Start backend
docker-compose up

# Import fixtures
docker exec -it accounting-reports_php_1 php bin/console bexio:accounting-reports:accounting-records:es-fixture-import
docker exec -it accounting-reports_php_1 php bin/console bexio:accounting-reports:accounts:es-fixture-import
docker exec -it accounting-reports_php_1 php bin/console bexio:accounting-reports:value-added-taxes:es-fixture-import
```

More infos can be found in the projects README.

## 2. Proxy Configuration

Create a file `proxy.conf.json`:

```json
{
  "/api": {
    "target": "http://localhost:8000",
    "secure": false
  }
}
```

> Tip: Add a pattern like `*.custom.*` to your global `.gitignore` file and name
> the proxy config file `proxy.conf.custom.json` so that git doesn't care about
> this additional file.

## 3. Frontend

In addition for the proxy to work, set the value of `baseApi` in the constructor
of `AccountingReportsHttpService` to "/api":

```ts
/* account-ledger-http.service.ts (excerpt) */
constructor (...) {
    super(httpClient, environment, baseApi, csn);
    baseApi = '/api'; // <---
    this.csn = 'foobarBazbaz'; // Optionally you can use another CSN
    this.api = baseApi;
}
```

Now start the frontend with the additional `--proxy-config <proxy-config>` flag,
e.g.

```sh
yarn serve --app=accounting_reports --proxy-config proxy.conf.custom.json
```

> Tip: Create a convenience script to simplify the app startup.  
> Check the appendix for [an example](#convenience-script-example).

---

## Appendix

### Convenience script example

Name the script for example `serve.custom.sh` (see tip above)
and then execute it with `./serve.custom.sh`.

```sh
#!/bin/sh
yarn serve --app=accounting_reports --proxy-config proxy.conf.custom.json
```
