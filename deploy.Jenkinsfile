DEFAULT_APPLICATIONS = [
    "accounting_reports",
    "accountant_portal",
    "billing",
    "document_designer",
    "manual_entries"
]

properties([
    buildDiscarder(logRotator(numToKeepStr: "30")),
    parameters([
        choice(choices: DEFAULT_APPLICATIONS, description: "Application to deploy.", name: "frontendApplication"),
        string(defaultValue: "none", description: "The version of the frontend to deploy.", name: "frontendVersion", trim: true),
        choice(choices: mont_blanc_envs(), description: "The Google project to deploy to.", name: "environment"),
    ]),
    disableConcurrentBuilds()
])

node("docker") {
    try {
        registryCredentials = "gcr:bexio-build"
        deployContainer = docker.image("eu.gcr.io/bexio-build/pipelines/bx-kubernetes-client:master")

        docker.withRegistry("https://eu.gcr.io", registryCredentials) {
            deployContainer.pull()
        }

        stage("Deploy") {
            def environment = mont_blanc_env(params.environment)
            def googleBucketUrl = "${environment.gcp_bucket_prefix}-static"
            def artefactName = "${params.frontendApplication}.tar.gz"
            def artefactVersion = params.frontendVersion.replaceAll("/", "-")
            def staticBucketPlaceholder = "_BEXIO_FE_DEPLOYMENT_REPLACED_BASE_URI_"
            def cdnVersionPlaceholder = "_BEXIO_FE_DEPLOYMENT_REPLACED_VERSION_"

            def staticBucketUrl = "//static.${environment.k8s_environment_domain}/frontend/${params.frontendApplication}/${artefactVersion}/"
            if(environment.gcp_project == 'bexio-dev') {
                staticBucketUrl = "//${googleBucketUrl}.storage.googleapis.com/frontend/${params.frontendApplication}/${artefactVersion}/"
            }

            if (params.frontendVersion != "none") {
                deployContainer.inside("--entrypoint='' --privileged --user 0") {
                    // Login to Google
                    withCredentials([file(credentialsId: environment.gcp_service_account_credential, variable: "CREDENTIALS_JSON")]) {
                        sh "gcloud auth activate-service-account --key-file=\${CREDENTIALS_JSON}"
                    }

                    // Deploy frontend
                    // Get frontend artefact
                    sh """
                        gsutil cp gs://bx-artifacts/frontend/${params.frontendApplication}/${artefactVersion}/${artefactName} ./app.tar.gz
                        mkdir -p ./frontend && tar xf ./app.tar.gz -C ./frontend
                    """

                    // Replace app-cdn version
                    sh "grep -rl ${cdnVersionPlaceholder} ./frontend | xargs sed -i s@${cdnVersionPlaceholder}@${artefactVersion}@g || true"

                    // Replace base uri
                    sh "grep -rl ${staticBucketPlaceholder} ./frontend | xargs sed -i s@${staticBucketPlaceholder}@${staticBucketUrl}@g"

                    // Deploy to static bucket
                    sh """
                        gsutil rsync -d -r ./frontend/ gs://${googleBucketUrl}/frontend/${params.frontendApplication}/${artefactVersion}/
                        gsutil cp ./frontend/index.html gs://${googleBucketUrl}/frontend/${params.frontendApplication}/
                    """

                    // Remove old artifacts
                    // - Get all `index.html` files for given application
                    // - Remove "TOTAL" line (it is last line from gsutil ls)
                    // - Remove currently uploaded articat
                    // - Get only creation date and GS path
                    // - Sort by date (first column)
                    // - Remove last line (the newest entry)
                    // - Get only GS path
                    // - Remove `/index.html` suffix from GS path
                    // - Remove old artefacts folders
                    sh """
                        gsutil -q ls -l "gs://${googleBucketUrl}/frontend/${params.frontendApplication}/*/index.html" \
                            | grep -v "TOTAL" \
                            | grep -v "gs://${googleBucketUrl}/frontend/${params.frontendApplication}/${artefactVersion}/" \
                            | awk '{print \$2,\$3}' \
                            | sort -k 1 \
                            | head -n -1 \
                            | awk '{print \$2}' \
                            | sed s@\\/index.html\\\$@@ \
                            | xargs -I {} gsutil -m rm -r {}
                    """
                }
            } else {
                echo "Skipping deployment"
            }
        }
    } finally {
        bexio_cleanWs()
    }
}
