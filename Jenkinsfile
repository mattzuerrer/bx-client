import groovy.json.*

def buildAndPushPackageToArtifactory(packageData) {
   createAndPushPackage('accountant_portal', packageData)
   createAndPushPackage('accounting_reports', packageData)
   createAndPushPackage('billing', packageData)
   createAndPushPackage('manual_entries', packageData)
}

def createAndPushPackage(app_name, packageData) {
    def ARTIFACT_NAME = "${app_name}-${packageData.name}.tar.gz"
    def PLACEHOLDER = "_BEXIO_FE_DEPLOYMENT_REPLACED_VERSION_"
    def LEGACY_VERSION = ""

    sh "mkdir -p ./tar/${app_name}"
    sh "cp -r ./dist/apps/${app_name}/* ./tar/${app_name}/"
    sh "grep -rl ${PLACEHOLDER} ./tar/${app_name} | xargs sed -i s@${PLACEHOLDER}@${LEGACY_VERSION}@g"
    sh "echo ${packageData} >> ./tar/${app_name}/git-info"
    sh "tar -cv --owner=0 --group=0 --exclude='.git' -z -f ${ARTIFACT_NAME} -C ./tar/${app_name}/ ."

    def server = Artifactory.newServer url: 'https://bexio.jfrog.io/bexio/', credentialsId: 'bexio-artifactory'
    server.publishBuildInfo(getArtifactoryBuildInfo(ARTIFACT_NAME))
    server.upload(getArtifactoryUploadSpec(packageData.path, ARTIFACT_NAME))
}

def getArtifactoryBuildInfo(String artifactName) {
    def buildInfo = Artifactory.newBuildInfo()
    buildInfo.name = artifactName
    buildInfo.env.capture = true

    /*
        def ARTIFACT_GENERATIONS = 10
        def ARTIFACT_DELETE_OLD_GENERATIONS = true
        buildInfo.retention maxBuilds: ARTIFACT_GENERATIONS, deleteBuildArtifacts: ARTIFACT_DELETE_OLD_GENERATIONS
    */

    return buildInfo
}

def getArtifactoryUploadSpec(String artifactPath, String artifactName) {
    def uploadSpec =  [
       files:[
           [
               pattern: artifactName,
               target: "bexio-dev/bexio/bx-client/${artifactPath}/${artifactName}"
           ]
       ]
   ];

   return JsonOutput.toJson(uploadSpec)
}

def runShellCommand(String command) {
     def status = sh(returnStatus: true, script: command + " > output.txt")

     if (status != 0) {
       return null
     }

     return readFile('output.txt').trim()
}

def getPackageGitMetaData(String hash, String branch, String tag=null) {
  def formattedBranch = branch.replaceAll('/','-')

  Object name = formattedBranch
  Object path = formattedBranch
  if (tag != null && tag.length() > 0) {
    name = tag + '_' + hash
    path = tag
  }

  return [
    branch: formattedBranch,
    hash: hash,
    tag: tag,
    path: path,
    name: name + '_angular2'
  ]
}

node('docker') {
  def gitInfo = checkout scm
  def GIT_BRANCH = gitInfo.GIT_BRANCH
  def GIT_COMMIT_HASH = runShellCommand("git rev-parse --verify --short HEAD")
  def GIT_TAG = runShellCommand("git describe --exact-match --tags")
  def GIT_PACKAGE_DATA = getPackageGitMetaData(GIT_COMMIT_HASH, GIT_BRANCH, GIT_TAG)

  echo "USING ${GIT_COMMIT_HASH} as tag for Docker containers"

  docker.withRegistry('https://bexio-docker-local.jfrog.io', 'bexio-artifactory') {

    def environment  = docker.image 'jenkins-node:10'

    environment.inside {

      stage ('Installing dependencies') {
        sh 'rm -rf node_modules && yarn cache clean'
        sh 'yarn install'
      }

      stage ('Running tests') {
        sh 'yarn affected:test --all --parallel --no-watch'
        sh 'yarn lint-all'
      }

      stage ('Building applications') {
        /* THIS WILL BE USED WHEN FRONTEND APPS RUN ON DOCKER
            Causes builds to be domain-aware, check artifactory deployment if enabled

            sh 'yarn build:accountant_portal --deploy-url=_DOCKER_REPLACED_BASE_URI_'
            sh 'yarn build:billing --deploy-url=_DOCKER_REPLACED_BASE_URI_'
            sh 'yarn build:manual_entries --deploy-url=_DOCKER_REPLACED_BASE_URI_'
            sh 'yarn build:accounting_reports --deploy-url=_DOCKER_REPLACED_BASE_URI_'
        */

        sh 'yarn build:accountant_portal'
        sh 'yarn build:accounting_reports'
        sh 'yarn build:billing'
        sh 'yarn build:manual_entries'
      }
    }
  }

  stage ('Building images') {
    if (branch_name == "master" || branch_name =~ "production" ||
        branch_name =~ "integration/*" || branch_name =~ "preview/*" ||
        branch_name =~ "feature/*" || branch_name =~ "release/*" ||
        branch_name =~ /d*\.\d*\.\d*/ ) {

      echo "Building and push artifacts for '${branch_name}'"
      buildAndPushPackageToArtifactory(GIT_PACKAGE_DATA);

    } else {
      echo "Branch name pattern '${branch_name}' unknown, skipping image building and package deployment."
    }
  }
}
