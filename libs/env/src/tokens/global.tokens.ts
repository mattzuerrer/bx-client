import { InjectionToken } from '@angular/core';

export const windowLocationToken = new InjectionToken('WINDOW_LOCATION');
