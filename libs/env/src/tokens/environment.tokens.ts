import { InjectionToken } from '@angular/core';

export const apiToken = new InjectionToken('API');
export const authorizationToken = new InjectionToken('AUTHORIZATION');
export const csnToken = new InjectionToken('CSN');
export const openIdToken = new InjectionToken('OPEN_ID_TOKEN');
export const defaultLocaleToken = new InjectionToken('DEFAULT_LOCALE');
export const localeToken = new InjectionToken('LOCALE');
export const languageToken = new InjectionToken('LANGUAGE');
export const defaultShortLocaleToken = new InjectionToken('DEFAULAPP_ASSET_URLT_SHORT_LOCALE');
export const shortLocaleToken = new InjectionToken('SHORT_LOCALE');
export const apiVersionToken = new InjectionToken('API_VERSION');
export const appEnvironmentToken = new InjectionToken('APP_ENVIRONMENT');
export const indagiaStartDateToken = new InjectionToken('INDAGIA_START_DATE');
export const version = new InjectionToken('APP_VERSION');
