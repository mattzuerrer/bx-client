import { ModuleWithProviders, NgModule } from '@angular/core';

import { environmentProviders, globalProviders } from './providers';

@NgModule()
export class EnvModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: EnvModule,
            providers: [environmentProviders, globalProviders]
        };
    }
}
