export const defaultLocale = 'de-CH';
export const defaultShortLocale = 'de';
export const defaultAuthorization = '';
export const defaultApi = '';
export const defaultApiVersion = '1';
export const defaultOpenIdToken = '';
export const defaultAppAssetUrl = '/';
