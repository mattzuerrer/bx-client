import { APP_BASE_HREF } from '@angular/common';
import { LOCALE_ID } from '@angular/core';

import {
    apiToken,
    apiVersionToken,
    authorizationToken,
    csnToken,
    defaultLocaleToken,
    defaultShortLocaleToken,
    indagiaStartDateToken,
    languageToken,
    localeToken,
    openIdToken,
    shortLocaleToken
} from '../tokens';

const defaultLanguage = 'de';

import {
    defaultApi,
    defaultApiVersion,
    defaultAppAssetUrl,
    defaultAuthorization,
    defaultLocale,
    defaultOpenIdToken,
    defaultShortLocale
} from '../config';

let APP_GLOBAL_ENV: Partial<Environment> = {};

if (typeof ENV === 'object') {
    APP_GLOBAL_ENV = ENV;
}

export function provideLocale(): string {
    return APP_GLOBAL_ENV.LOCALE || defaultLocale;
}

export function provideShortLocale(locale: string): string {
    const localeParts = locale.split('-');
    return localeParts.length === 2 ? localeParts[0] : defaultShortLocale;
}

export function provideAppBaseHref(): string {
    return APP_GLOBAL_ENV.APP_BASE_HREF;
}

export function provideCsn(): string {
    return APP_GLOBAL_ENV.CSN;
}

export function provideApi(): string {
    return APP_GLOBAL_ENV.API || defaultApi;
}

export function provideApiVersion(): string {
    return APP_GLOBAL_ENV.API_VERSION || defaultApiVersion;
}

export function provideAuthorization(): string {
    return APP_GLOBAL_ENV.AUTHORIZATION || defaultAuthorization;
}

export function provideOpenIdToken(): string {
    return APP_GLOBAL_ENV.OPEN_ID_TOKEN || defaultOpenIdToken;
}

export function provideIndagiaToken(): string {
    return APP_GLOBAL_ENV.INDAGIA_START_DATE || null;
}

export function provideAppCdnUrl(): string {
    return APP_GLOBAL_ENV.APP_CDN_URL || defaultAppAssetUrl;
}

/**
 * Get language from locale.
 * @param locale
 * @returns {string}
 */
export function getLanguage(locale: string): string {
    if (locale) {
        return locale.split('-')[0];
    }
    return defaultLanguage;
}

export const environmentProviders = [
    { provide: APP_BASE_HREF, useFactory: provideAppBaseHref },
    { provide: LOCALE_ID, useFactory: provideLocale },
    { provide: localeToken, useFactory: provideLocale },
    { provide: defaultLocaleToken, useValue: defaultLocale },
    { provide: shortLocaleToken, useFactory: provideShortLocale, deps: [localeToken] },
    { provide: defaultShortLocaleToken, useValue: defaultShortLocale },
    { provide: csnToken, useFactory: provideCsn },
    { provide: openIdToken, useFactory: provideOpenIdToken },
    { provide: apiToken, useFactory: provideApi },
    { provide: authorizationToken, useFactory: provideAuthorization },
    { provide: apiVersionToken, useFactory: provideApiVersion },
    { provide: languageToken, useFactory: getLanguage, deps: [LOCALE_ID] },
    { provide: indagiaStartDateToken, useFactory: provideIndagiaToken }
];
