import { windowLocationToken } from '../tokens';

export function provideLocation(): any {
    return window && window.location;
}

export const globalProviders = [{ provide: windowLocationToken, useFactory: provideLocation }];
