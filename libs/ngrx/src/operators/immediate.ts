import { Observable } from 'rxjs';

export const immediate = (fn: (x: any) => void) => <T>(source: Observable<T>) =>
    new Observable<T>(observer => {
        return source.subscribe({
            next(x: any): void {
                setTimeout(() => fn(x), 0);
                observer.next(x);
            },
            error(err: any): void {
                observer.error(err);
            },
            complete(): void {
                observer.complete();
            }
        });
    });
