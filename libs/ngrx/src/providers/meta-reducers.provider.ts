import { appEnvironmentToken } from '@bx-client/env';
import { ActionReducer, META_REDUCERS } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';

// tslint:disable-next-line:no-empty-interface
export interface State {}

export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
    return (state: State, action: any): State => {
        // tslint:disable:no-console
        console.log('state', state);
        console.log('action', action);
        // tslint:enable:no-console

        return reducer(state, action);
    };
}

export function getMetaReducers(environment: AppEnvironment): any {
    return environment.isProduction ? [] : [logger, storeFreeze];
}

export const metaReducersProvider = {
    provide: META_REDUCERS,
    useFactory: getMetaReducers,
    deps: [appEnvironmentToken]
};
