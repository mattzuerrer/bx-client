import { appEnvironmentToken } from '@bx-client/env';
import {
    ɵngrx_modules_store_devtools_store_devtools_c as createReduxDevtoolsExtension,
    ɵngrx_modules_store_devtools_store_devtools_j as REDUX_DEVTOOLS_EXTENSION
} from '@ngrx/store-devtools';

export function createDevtoolsExtension(environment: AppEnvironment): any {
    return environment.isProduction ? null : createReduxDevtoolsExtension();
}

export const devtoolsExtensionProvider = {
    provide: REDUX_DEVTOOLS_EXTENSION,
    useFactory: createDevtoolsExtension,
    deps: [appEnvironmentToken]
};
