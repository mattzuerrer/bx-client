import { ModuleWithProviders, NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { devtoolsExtensionProvider, metaReducersProvider } from './providers';
import { EffectsService, StateService } from './services';

@NgModule({
    imports: [StoreModule.forRoot({}), EffectsModule.forRoot([]), StoreDevtoolsModule.instrument()],
    exports: [StoreModule, EffectsModule],
    providers: [EffectsService, StateService, devtoolsExtensionProvider, metaReducersProvider]
})
export class NgrxRootModule {}

@NgModule()
export class NgrxModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: NgrxRootModule
        };
    }
}
