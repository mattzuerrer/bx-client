import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';

@Injectable()
export class StateService {
    constructor(protected store: Store<any>) {}

    dispatch(action: Action): void {
        this.store.dispatch(action);
    }
}
