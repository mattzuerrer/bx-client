import { Injectable } from '@angular/core';
import { EffectNotification, OnRunEffects } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { immediate } from '../operators';

import { StateService } from './state.service';

@Injectable()
export class EffectsService implements OnRunEffects {
    initActions: Action[] = [];

    constructor(protected stateService: StateService) {}

    ngrxOnRunEffects(resolvedEffects$: Observable<EffectNotification>): Observable<EffectNotification> {
        const initActions$ = of(...this.initActions).pipe(
            immediate(action => this.stateService.dispatch(action)),
            switchMap(() => resolvedEffects$)
        );
        return this.initActions.length ? initActions$ : resolvedEffects$;
    }
}
