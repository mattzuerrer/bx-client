export { LocalStorageConfigInterface } from './src/local-storage-config-interface';
export { BxLocalStorageModule } from './src/localStorage.module';
export { localStorageConfigToken } from './src/localService.tokens';
export * from './src/services';
