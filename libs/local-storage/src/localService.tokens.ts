import { InjectionToken } from '@angular/core';

export const localStorageConfigToken = new InjectionToken('LOCAL_STORAGE_CONFIG_TOKEN');
