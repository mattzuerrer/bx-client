import { ModuleWithProviders, NgModule } from '@angular/core';
import { LocalStorageModule } from 'angular-2-local-storage';

import { BxLocalStorageService } from './services';

@NgModule({
    imports: [LocalStorageModule.withConfig({ prefix: '', storageType: 'localStorage' })],
    exports: [LocalStorageModule],
    providers: [BxLocalStorageService]
})
export class LocalStorageRootModule {}

@NgModule({
    imports: [LocalStorageModule.withConfig({ prefix: '', storageType: 'localStorage' })],
    exports: [LocalStorageModule]
})
export class BxLocalStorageModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: LocalStorageRootModule
        };
    }
}
