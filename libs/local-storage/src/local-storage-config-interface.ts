export interface LocalStorageConfigInterface {
    appPrefix: string;
    keys: {
        [key: string]: string;
    };
}
