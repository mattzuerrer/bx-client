import { Inject, Injectable } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import { csnToken } from 'libs/env/index';

import { localStorageConfigToken } from '../localService.tokens';

@Injectable()
export class BxLocalStorageService {
    private companyPrefix = 'bexio';
    private csn: string;
    private appName: string;

    constructor(
        private localStorageService: LocalStorageService,
        /* tslint:disable:no-unused-variable */
        @Inject(csnToken) private injectedCsn: string,
        @Inject(localStorageConfigToken) private config: any
    ) {
        this.csn = injectedCsn;
        this.appName = config.appPrefix;
    }

    get(key: string, includeCsn: boolean = true): any {
        this.handleOldKeys(key);
        return this.localStorageService.get(this.getWholeKey(key, includeCsn));
    }

    set(key: string, value: any, includeCsn: boolean = true): void {
        this.localStorageService.set(this.getWholeKey(key, includeCsn), value);
    }

    remove(key: string, includeCsn: boolean = true): void {
        this.localStorageService.remove(this.getWholeKey(key, includeCsn));
    }

    private getWholeKey(keySuffix: string, includeCsn: boolean): string {
        return `${this.companyPrefix}.${this.appName}${includeCsn ? '.' + this.csn : ''}.${keySuffix}`;
    }

    // Is it year 2020? Feel free to remove this function!
    private handleOldKeys(key: string): void {
        if (this.appName === 'manual-entries') {
            const oldKey = `bx-manual-entries.${key}`;
            const oldValue = this.localStorageService.get(oldKey);
            if (oldValue) {
                this.set(key, oldValue, false);
                this.localStorageService.remove(oldKey);
            }
        }
    }
}
