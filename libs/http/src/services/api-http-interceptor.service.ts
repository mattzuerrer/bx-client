import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { localeToken } from '@bx-client/env';
import { Observable } from 'rxjs';

@Injectable()
export class ApiHttpInterceptorService implements HttpInterceptor {
    constructor(@Inject(localeToken) private locale: string) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let headers = req.headers;
        headers = headers.set('Content-Type', 'application/json');
        headers = headers.set('Accept', 'application/json');
        headers = headers.set('Accept-Language', this.locale);

        const clone = req.clone({ headers });
        return next.handle(clone);
    }
}
