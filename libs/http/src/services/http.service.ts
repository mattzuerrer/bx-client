import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { concatMap, delay, map, retryWhen } from 'rxjs/operators';

import { responseMapper } from '../helpers';
import { httpApiProviderToken } from '../http.tokens';
import { HttpOptions, HttpRetryOptions } from '../models';

@Injectable()
export class HttpService {
    protected api: string;
    protected apiSpecificHeaders: HttpHeaders;

    constructor(private httpClient: HttpClient, @Inject(httpApiProviderToken) apiUrlGenerator: any) {
        this.api = apiUrlGenerator.getApiUrl();
        this.apiSpecificHeaders = apiUrlGenerator.getHeaders();
    }

    getApiUrl(): string {
        return this.api;
    }

    get(cls: any, url: string, options: HttpOptions = {}, mapFunction: any = null): Observable<any> {
        return this.getRequest(url, options).pipe(map(responseMapper(cls, options, mapFunction)));
    }

    retryGet(cls: any, url: string, options: HttpOptions = {}, retryOptions: HttpRetryOptions, mapFunction: any = null): Observable<any> {
        options.observe = 'response'; // always fully observe request in retry case, state must be available

        return this.getRequest(url, options).pipe(
            map(response => {
                if (retryOptions.retryHttpStates.includes(response.status)) {
                    throw response; // because retryWhen() only listens to errors
                }

                return response;
            }),
            map(responseMapper(cls, options, mapFunction)),
            retryWhen(thrownError => {
                return thrownError.pipe(
                    concatMap((response, retryAttempt) => {
                        if (retryAttempt >= retryOptions.attempts || !retryOptions.retryHttpStates.includes(response.status)) {
                            return throwError(response);
                        }

                        return of(response).pipe(delay(retryOptions.delay));
                    })
                );
            })
        );
    }

    post(cls: any, url: string, body: any = null, options: HttpOptions = {}, mapFunction: any = null): Observable<any> {
        const headers = this.mergeHeaders(options, this.apiSpecificHeaders);
        return this.httpClient.post(`${this.api}/${url}`, body, <any>headers).pipe(map(responseMapper(cls, options, mapFunction)));
    }

    put(cls: any, url: string, body: string = null, options: HttpOptions = {}, mapFunction: any = null): Observable<any> {
        const headers = this.mergeHeaders(options, this.apiSpecificHeaders);
        return this.httpClient.put(`${this.api}/${url}`, body, <any>headers).pipe(map(responseMapper(cls, options, mapFunction)));
    }

    remove(cls: any, url: string, options: HttpOptions = {}, mapFunction: any = null): Observable<any> {
        const headers = this.mergeHeaders(options, this.apiSpecificHeaders);
        return this.httpClient.delete(`${this.api}/${url}`, <any>headers).pipe(map(responseMapper(cls, options, mapFunction)));
    }

    patch(cls: any, url: string, body: any = null, options: HttpOptions = {}, mapFunction: any = null): Observable<any> {
        const headers = this.mergeHeaders(options, this.apiSpecificHeaders);
        return this.httpClient.patch(`${this.api}/${url}`, body, <any>headers).pipe(map(responseMapper(cls, options, mapFunction)));
    }

    private getRequest(url: string, options: HttpOptions = {}): Observable<any> {
        const headers = this.mergeHeaders(options, this.apiSpecificHeaders);
        return this.httpClient.get(`${this.api}/${url}`, <any>headers);
    }

    private mergeHeaders(options: HttpOptions, apiSpecificHeaders: HttpHeaders): HttpOptions {
        const headers = Object.assign(new HttpHeaders(), options.headers, apiSpecificHeaders);
        return Object.assign(options, { headers });
    }
}
