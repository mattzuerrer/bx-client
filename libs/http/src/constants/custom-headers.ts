/** According to [RFC 6648](https://tools.ietf.org/html/rfc6648), prefixing custom HTTP headers with `X-` is deprecated. */

export const totalCountHeader = 'Total-Count';
export const limitHeader = 'Limit';
export const offsetHeader = 'Offset';

export const deprTotalCountHeader = `X-${totalCountHeader}`;
export const deprLimitHeader = `X-${limitHeader}`;
export const deprOffsetHeader = `X-${offsetHeader}`;
