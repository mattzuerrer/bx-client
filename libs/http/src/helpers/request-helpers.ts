import { HttpParams } from '@angular/common/http';
import { isArray } from '@bx-client/utils';

import { limitQueryParam, observeResponse, offsetQueryParam } from '../constants';

/**
 * Creates custom pagination http params for given page index and page size.
 *
 * @param pageIndex
 * @param pageSize
 * @returns {HttpParams}
 */
const paginationParams = (pageIndex, pageSize): HttpParams => {
    let params = new HttpParams();
    const limit = pageSize.toString();
    const offset = (pageIndex * pageSize).toString();
    params = params.set(limitQueryParam, limit);
    params = params.set(offsetQueryParam, offset);
    return params;
};

/**
 * Creates pagination options for given page index and page size.
 * Sets custom paginated property.
 *
 * @param pageIndex
 * @param pageSize
 */
export const paginationOptions = (pageIndex, pageSize) => ({
    observe: observeResponse,
    params: paginationParams(pageIndex, pageSize),
    paginated: true
});

/**
 * Adds each element of the map to the `HttpParams` and returns the newly created instance.
 * @param paramMap A map of parameters where the key is the parameter name
 * @param existingParams
 */
export const setQueryParamsFromMap = (paramMap: Map<string, string | string[]>, existingParams = new HttpParams()): HttpParams => {
    paramMap.forEach((value, key) => {
        if (isArray(value)) {
            value.forEach(item => (existingParams = existingParams.append(key, item)));
        } else {
            existingParams = existingParams.set(key, value);
        }
    });
    return existingParams;
};
