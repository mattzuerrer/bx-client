import { HttpResponse } from '@angular/common/http';
import { UtilitiesService, ValidationService } from '@bx-client/common';
import { plainToClass } from 'class-transformer';

import { blob, deprLimitHeader, deprOffsetHeader, deprTotalCountHeader, limitHeader, offsetHeader, totalCountHeader } from '../constants';
import { HttpOptions } from '../models';

type mapProjectType = (response: any) => any;

type responseMapperType = (cls: any, options: HttpOptions, mapFunction: any) => mapProjectType;

const validation = new ValidationService();
const utilitiesService = new UtilitiesService(validation);

/**
 * Maps observable response body to camel case and then to class object for a given class.
 * If class is not provided it only returns body in camel case.
 *
 * @param cls
 * @param {HttpOptions} options
 * @param mapFunction
 *
 * @returns {(response: any) => (any | any[])}
 */
const mapResponseSimple: responseMapperType = (cls: any, options: HttpOptions, mapFunction: any) => (response: any) => {
    if (options.responseType === blob) {
        return response;
    }

    if (response instanceof HttpResponse) {
        response = response.body;
    }

    if (mapFunction) {
        response = mapFunction(response);
    }

    if (cls) {
        const classTransformOptions = options.classTransformerOptions || {};
        return plainToClass(cls, utilitiesService.snakeToCamel(response), classTransformOptions);
    }

    return utilitiesService.snakeToCamel(response);
};

/**
 * Maps observable response body and headers to paginated object.
 *
 * @param cls
 * @param {HttpOptions} options
 * @param mapFunction
 *
 * @returns {(response: any) => {items: any; length: number | number; pageIndex: number; pageSize: number}}
 */
const mapResponsePaginated: responseMapperType = (cls: any, options: HttpOptions, mapFunction: any) => (response: any) => {
    const body = response.body;
    const headers = response.headers;

    const items = mapResponseSimple(cls, options, mapFunction)(body);
    const length = parseInt(headers.get(totalCountHeader) || headers.get(deprTotalCountHeader), 10) || 0;
    const pageSize = parseInt(headers.get(limitHeader) || headers.get(deprLimitHeader), 10) || -1;
    const offset = parseInt(headers.get(offsetHeader) || headers.get(deprOffsetHeader), 10) || 0;
    const pageIndex = offset / pageSize;

    return {
        items,
        length,
        pageIndex,
        pageSize
    };
};

/**
 * Returns either simple mapper or paginated mapper depending on paginated option.
 *
 * @param cls
 * @param {HttpOptions} options
 * @param mapFunction
 *
 * @returns {mapProjectType}
 */
export const responseMapper: (cls: any, options: HttpOptions, mapFunction: any) => mapProjectType = (cls, options, mapFunction) =>
    options.paginated ? mapResponsePaginated(cls, options, mapFunction) : mapResponseSimple(cls, options, mapFunction);
