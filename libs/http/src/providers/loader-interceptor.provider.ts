import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { LoaderInterceptorService } from '../services';

export const loaderInterceptorProvider = { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptorService, multi: true };
