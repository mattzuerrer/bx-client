import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { ApiHttpInterceptorService } from '../services';

export const apiHttpInterceptorProvider = { provide: HTTP_INTERCEPTORS, useClass: ApiHttpInterceptorService, multi: true };
