import { Inject, Injectable } from '@angular/core';
import { apiToken, apiVersionToken, appEnvironmentToken, openIdToken } from '@bx-client/env';

import { BaseHttpApiProviderService } from './base-http-api-provider.service';

@Injectable()
export class ApiJwtInterceptorProvider extends BaseHttpApiProviderService {
    constructor(
        @Inject(appEnvironmentToken) environment: AppEnvironment,
        @Inject(openIdToken) jwtToken: string,
        @Inject(apiToken) baseApi: string,
        @Inject(apiVersionToken) apiVersion: string
    ) {
        super(baseApi);

        this.api = `${baseApi}/${apiVersion}`;
        this.apiSpecificHeaders = this.apiSpecificHeaders.set('api-version', apiVersion);
        this.apiSpecificHeaders = this.apiSpecificHeaders.set('Authorization', `Bearer ${jwtToken}`);
    }
}
