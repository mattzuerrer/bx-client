import { Inject, Injectable } from '@angular/core';
import { apiToken, apiVersionToken, appEnvironmentToken, authorizationToken, csnToken } from '@bx-client/env';

import { devApiIndex, prodApiIndex } from '../config';

import { BaseHttpApiProviderService } from './base-http-api-provider.service';

@Injectable()
export class OfficeHttpApiProviderService extends BaseHttpApiProviderService {
    constructor(
        @Inject(appEnvironmentToken) environment: AppEnvironment,
        @Inject(authorizationToken) authorization: string,
        @Inject(apiToken) baseApi: string,
        @Inject(csnToken) csn: string,
        @Inject(apiVersionToken) apiVersion: string
    ) {
        super(baseApi);
        const apiIndex = environment.isProduction ? prodApiIndex : devApiIndex;
        this.api = `${baseApi}/${apiIndex}/${csn}`;

        this.apiSpecificHeaders = this.apiSpecificHeaders.set('api-version', apiVersion);
        if (!environment.isProduction) {
            this.apiSpecificHeaders = this.apiSpecificHeaders.set('Authorization', `Bearer ${authorization}`);
        }
    }
}
