import { HttpHeaders } from '@angular/common/http';
import { Inject } from '@angular/core';
import { apiToken } from '@bx-client/env';

export abstract class BaseHttpApiProviderService {
    protected api: string;
    protected apiSpecificHeaders: HttpHeaders = new HttpHeaders();

    constructor(@Inject(apiToken) baseApi: string) {
        this.api = baseApi;
    }

    getApiUrl(): string {
        return this.api;
    }
    getHeaders(): HttpHeaders {
        return this.apiSpecificHeaders;
    }
}
