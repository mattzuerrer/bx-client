import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { GeneralHttpInterceptorService } from '../services';

export const generalHttpInterceptorProvider = { provide: HTTP_INTERCEPTORS, useClass: GeneralHttpInterceptorService, multi: true };
