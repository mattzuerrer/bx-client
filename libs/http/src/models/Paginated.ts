export interface Paginated<T> {
    items: T;
    pageIndex: number;
    pageSize: number;
    length: number;
}
