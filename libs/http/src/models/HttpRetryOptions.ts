export interface HttpRetryOptions {
    attempts: number;
    delay: number; // in milliseconds
    retryHttpStates: number[];
}
