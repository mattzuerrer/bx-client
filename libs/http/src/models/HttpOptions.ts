import { HttpHeaders, HttpParams } from '@angular/common/http';
import { ClassTransformOptions } from 'class-transformer';

export interface HttpOptions {
    headers?: HttpHeaders;
    params?: HttpParams;
    withCredentials?: boolean;
    observe?: string;
    responseType?: string;
    paginated?: boolean;
    classTransformerOptions?: ClassTransformOptions;
}
