import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule } from '@angular/core';

import { LoaderOverlayModule } from './modules';
import { generalHttpInterceptorProvider, loaderInterceptorProvider } from './providers';
import { GeneralHttpInterceptorService, HttpService, LoaderService, OfficeHttpApiProviderService } from './services';

@NgModule({
    imports: [HttpClientModule, LoaderOverlayModule],
    exports: [HttpClientModule, LoaderOverlayModule],
    providers: [
        GeneralHttpInterceptorService,
        LoaderService,
        HttpService,
        generalHttpInterceptorProvider,
        loaderInterceptorProvider,
        OfficeHttpApiProviderService
    ]
})
export class HttpRootModule {}

@NgModule({
    imports: [HttpClientModule, LoaderOverlayModule],
    exports: [HttpClientModule, LoaderOverlayModule]
})
export class HttpModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: HttpRootModule
        };
    }
}
