import { HttpClient } from '@angular/common/http';
import { HttpService } from '@bx-client/http';
import { noop } from 'rxjs';

export class HttpServiceMock {
    get(): void {
        noop();
    }

    post(): void {
        noop();
    }

    remove(): void {
        noop();
    }

    put(): void {
        noop();
    }

    patch(): void {
        noop();
    }

    getApiUrl(): void {
        noop();
    }
}

export const HttpServiceMockProvider = {
    provide: HttpService,
    useClass: HttpServiceMock
};

export const HttpClientMockProvider = {
    provide: HttpClient,
    useClass: HttpServiceMock
};
