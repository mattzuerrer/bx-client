import { InjectionToken } from '@angular/core';

export const httpApiProviderToken = new InjectionToken('HTTP_API_PROVIDER_TOKEN');
