import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Input } from '@angular/core';

import { LoaderService } from '../../services';

@Component({
    selector: 'app-loader-overlay',
    templateUrl: 'loader-overlay.component.html',
    styleUrls: ['loader-overlay.component.scss'],
    animations: [
        trigger('triggerAnimation', [
            state('in', style({ opacity: 1 })),

            transition(':enter', [style({ opacity: 0 }), animate(100)]),

            transition(':leave', animate(100, style({ opacity: 0 })))
        ])
    ]
})
export class LoaderOverlayComponent {
    disable = false;

    @Input()
    set disableLoader(disable: boolean) {
        this.disable = disable;
    }

    constructor(public loaderService: LoaderService) {}
}
