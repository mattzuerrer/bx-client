import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material';

import { LoaderService } from '../../services';

import { LoaderOverlayComponent } from './loader-overlay.component';

@NgModule({
    imports: [MatProgressSpinnerModule, CommonModule],
    providers: [LoaderService],
    declarations: [LoaderOverlayComponent],
    exports: [LoaderOverlayComponent, MatProgressSpinnerModule]
})
export class LoaderOverlayModule {}
