import { Action } from '@ngrx/store';

import { Message } from './models/Message';

export namespace MessageActions {
    export const INFO_MSG = '[Message] Info message';
    export const SUCCESS_MSG = '[Message] Success message';
    export const WARNING_MSG = '[Message] Warning message';
    export const ERROR_MSG = '[Message] Error message';

    export class InfoMessage implements Action {
        readonly type = INFO_MSG;

        constructor(public payload: Message) {}
    }

    export class SuccessMessage implements Action {
        readonly type = SUCCESS_MSG;

        constructor(public payload: Message) {}
    }

    export class WarningMessage implements Action {
        readonly type = WARNING_MSG;

        constructor(public payload: Message) {}
    }

    export class ErrorMessage implements Action {
        readonly type = ERROR_MSG;

        constructor(public payload: Message) {}
    }

    export type Actions = InfoMessage | SuccessMessage | WarningMessage | ErrorMessage;
}
