import { Injectable } from '@angular/core';
import { applyChanges } from '@bx-client/utils';
import { Action } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';

import { ToastOptionsData } from '../models';

@Injectable()
export class MessageService {
    action$ = new Subject<Action>();

    constructor(private toastr: ToastrService) {}

    info(messageKey: string, titleKey?: string, options?: ToastOptionsData): void {
        this.toastr.info(messageKey, titleKey, options);
    }

    success(messageKey: string, titleKey?: string, options?: ToastOptionsData): void {
        this.toastr.success(messageKey, titleKey, options);
    }

    warning(messageKey: string, titleKey?: string, options?: ToastOptionsData): void {
        this.toastr.warning(messageKey, titleKey, options);
    }

    error(messageKey: string, titleKey?: string, options?: ToastOptionsData): void {
        this.toastr.error(messageKey, titleKey, applyChanges({ timeOut: 0 }, options));
    }
}
