import { noop, Subject } from 'rxjs';

import { MessageService } from './message.service';

export class MessageServiceMock {
    action$: Subject<any> = new Subject<any>();

    info(): void {
        noop();
    }

    success(): void {
        noop();
    }

    warning(): void {
        noop();
    }

    error(): void {
        noop();
    }

    init(): void {
        noop();
    }
}

export const MessageServiceMockProvider = {
    provide: MessageService,
    useClass: MessageServiceMock
};
