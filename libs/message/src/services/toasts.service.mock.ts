import { ToastrService } from 'ngx-toastr';
import { noop, Subject } from 'rxjs';

const createToastCloseButton = () => {
    const closeButton = document.createElement('span');
    closeButton.classList.add('toast-close-button');
    return closeButton;
};

const createToast = () => {
    const toast = document.createElement('div');
    toast.appendChild(createToastCloseButton());
    return toast;
};

const createToastContainer = numberOfToasts => {
    const toastsContainer = document.createElement('div');
    for (let i = 0; i < numberOfToasts; i++) {
        toastsContainer.appendChild(createToast());
    }
    return toastsContainer;
};

export class ToastsManagerServiceMock {
    container: any = { location: { nativeElement: createToastContainer(5) } };

    click$: Subject<any> = new Subject<any>();

    setRootViewContainerRef(): void {
        noop();
    }

    onClickToast(): Subject<any> {
        return this.click$;
    }

    init(): void {
        noop();
    }

    info(): void {
        noop();
    }

    success(): void {
        noop();
    }

    warning(): void {
        noop();
    }

    error(): void {
        noop();
    }
}

export const ToastsManagerServiceMockProvider = {
    provide: ToastrService,
    useClass: ToastsManagerServiceMock
};
