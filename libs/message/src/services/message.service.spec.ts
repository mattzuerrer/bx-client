import { inject, TestBed } from '@angular/core/testing';
import { ToastrModule, ToastrService } from 'ngx-toastr';

import { MessageService } from './message.service';

const message = 'message';

const title = 'title';

const options = {
    titleClass: 'title-class',
    messageClass: 'message-class'
};

describe('Message service', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ToastrModule.forRoot()],
            providers: [MessageService, ToastrService]
        });
    });

    it(
        'should show info',
        inject([MessageService, ToastrService], (messageService, toastrService) => {
            spyOn(toastrService, 'info');
            messageService.info(message, title, options);
            expect(toastrService.info).toHaveBeenCalledTimes(1);
            expect(toastrService.info).toHaveBeenCalledWith(message, title, options);
        })
    );

    it(
        'should show success',
        inject([MessageService, ToastrService], (messageService, toastrService) => {
            spyOn(toastrService, 'success');
            messageService.success(message, title, options);
            expect(toastrService.success).toHaveBeenCalledTimes(1);
            expect(toastrService.success).toHaveBeenCalledWith(message, title, options);
        })
    );

    it(
        'should show warning',
        inject([MessageService, ToastrService], (messageService, toastrService) => {
            spyOn(toastrService, 'warning');
            messageService.warning(message, title, options);
            expect(toastrService.warning).toHaveBeenCalledTimes(1);
            expect(toastrService.warning).toHaveBeenCalledWith(message, title, options);
        })
    );

    it(
        'should show error with custom timeOut option',
        inject([MessageService, ToastrService], (messageService, toastrService) => {
            spyOn(toastrService, 'error');
            messageService.error(message, title, options);
            expect(toastrService.error).toHaveBeenCalledTimes(1);
            expect(toastrService.error).toHaveBeenCalledWith(message, title, Object.assign({ timeOut: 0 }, options));
        })
    );
});
