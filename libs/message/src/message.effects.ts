import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { MessageActions } from './message.actions';
import { MessageService } from './services';

@Injectable()
export class MessagesEffects {
    @Effect({ dispatch: false })
    showInfoMessage$: Observable<any> = this.actions$.pipe(
        ofType<MessageActions.InfoMessage>(MessageActions.INFO_MSG),
        map((action: MessageActions.InfoMessage) => action.payload),
        tap(payload => this.messageService.info(payload.messageKey, payload.titleKey, payload.options))
    );

    @Effect({ dispatch: false })
    showSuccessMessage$: Observable<any> = this.actions$.pipe(
        ofType<MessageActions.SuccessMessage>(MessageActions.SUCCESS_MSG),
        map((action: MessageActions.SuccessMessage) => action.payload),
        tap(payload => this.messageService.success(payload.messageKey, payload.titleKey, payload.options))
    );

    @Effect({ dispatch: false })
    showWarningMessage$: Observable<any> = this.actions$.pipe(
        ofType<MessageActions.WarningMessage>(MessageActions.WARNING_MSG),
        map((action: MessageActions.WarningMessage) => action.payload),
        tap(payload => this.messageService.warning(payload.messageKey, payload.titleKey, payload.options))
    );

    @Effect({ dispatch: false })
    showErrorMessage$: Observable<any> = this.actions$.pipe(
        ofType<MessageActions.ErrorMessage>(MessageActions.ERROR_MSG),
        map((action: MessageActions.ErrorMessage) => action.payload),
        tap(payload => this.messageService.error(payload.messageKey, payload.titleKey, payload.options))
    );

    constructor(private actions$: Actions, private messageService: MessageService) {}
}
