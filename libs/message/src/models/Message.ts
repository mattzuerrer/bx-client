import { ToastOptionsData } from './ToastOptionsData';

export interface Message {
    messageKey: string;
    titleKey?: string;
    options?: Partial<ToastOptionsData>;
}
