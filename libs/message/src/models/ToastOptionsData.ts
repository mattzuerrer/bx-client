import { Action } from '@ngrx/store';
import { IndividualConfig } from 'ngx-toastr';

export interface ToastOptionsData extends IndividualConfig {
    data?: Partial<ToastAdditionalProperties>;
}

interface ToastAdditionalProperties {
    action: Action;
    translateParams: object;
}
