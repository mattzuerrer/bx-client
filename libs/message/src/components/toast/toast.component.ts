import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, HostListener, OnInit } from '@angular/core';
import { Toast, ToastPackage, ToastrService } from 'ngx-toastr';

import { ToastOptionsData } from '../../models';
import { MessageService } from '../../services';

@Component({
    selector: 'app-toast',
    templateUrl: './toast.component.html',
    animations: [
        trigger('flyInOut', [
            state(
                'inactive',
                style({
                    display: 'none',
                    opacity: 0
                })
            ),
            state('active', style({ opacity: 1 })),
            state('removed', style({ opacity: 0 })),
            transition('inactive => active', animate('300ms ease-in')),
            transition('active => removed', animate('300ms ease-in'))
        ])
    ]
})
export class ToastComponent extends Toast implements OnInit {
    options: ToastOptionsData;
    message: string;
    translateParams: object;

    constructor(protected toastrService: ToastrService, public toastPackage: ToastPackage, private messageService: MessageService) {
        super(toastrService, toastPackage);
    }

    ngOnInit(): void {
        if (this.options.data && this.options.data.translateParams) {
            this.translateParams = this.options.data.translateParams;
        }
    }

    @HostListener('click')
    tapToast(): void {
        if (this.options.data && this.options.data.action) {
            this.messageService.action$.next(this.options.data.action);
        }
        this.remove();
    }

    close(event: Event): void {
        // This click event should not bubble up to prevent triggering Host click
        event.stopPropagation();
        this.remove();
    }
}
