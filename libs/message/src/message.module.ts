import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { TranslateModule } from '@ngx-translate/core';
import { ToastrModule } from 'ngx-toastr';

import { ToastComponent } from './components/toast';
import { MessagesEffects } from './message.effects';
import { MessageService } from './services';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        ToastrModule.forRoot({
            toastComponent: ToastComponent,
            maxOpened: 100,
            timeOut: 4000,
            extendedTimeOut: 0,
            newestOnTop: false,
            closeButton: true
        }),
        EffectsModule.forFeature([MessagesEffects])
    ],
    providers: [MessageService],
    declarations: [ToastComponent],
    entryComponents: [ToastComponent]
})
export class MessageRootModule {}

@NgModule()
export class MessageModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: MessageRootModule
        };
    }
}
