import { inject, TestBed } from '@angular/core/testing';
import { TranslateServiceMockProvider } from '@bx-client/i18n';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { Observable } from 'rxjs';

import { MessageActions } from './message.actions';
import { MessagesEffects } from './message.effects';
import { MessageService, ToastsManagerServiceMockProvider } from './services';

describe('Message Effects', () => {
    let messageEffects: MessagesEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<MessagesEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                MessagesEffects,
                MessageService,
                TranslateServiceMockProvider,
                ToastsManagerServiceMockProvider,
                provideMockActions(() => actions)
            ]
        });

        messageEffects = TestBed.get(MessagesEffects);
        metadata = getEffectsMetadata(messageEffects);
    });

    it(
        'should call messageService.info on INFO_MSG action',
        inject([MessageService], messageService => {
            const payload = {
                messageKey: 'message_key',
                titleKey: 'title_key',
                options: {}
            };
            actions = cold('a', { a: new MessageActions.InfoMessage(payload) });
            spyOn(messageService, 'info').and.callThrough();
            const expected = cold('b', { b: payload });
            expect(messageEffects.showInfoMessage$).toBeObservable(expected);
            expect(messageService.info).toHaveBeenCalledTimes(1);
            expect(messageService.info).toHaveBeenCalledWith(payload.messageKey, payload.titleKey, payload.options);
        })
    );

    it(
        'should call messageService.success on SUCCESS_MSG action',
        inject([MessageService], messageService => {
            const payload = {
                messageKey: 'message_key',
                titleKey: 'title_key',
                options: {}
            };
            actions = cold('a', { a: new MessageActions.SuccessMessage(payload) });
            spyOn(messageService, 'success').and.callThrough();
            const expected = cold('b', { b: payload });
            expect(messageEffects.showSuccessMessage$).toBeObservable(expected);
            expect(messageService.success).toHaveBeenCalledTimes(1);
            expect(messageService.success).toHaveBeenCalledWith(payload.messageKey, payload.titleKey, payload.options);
        })
    );

    it(
        'should call messageService.warning on WARNING_MSG action',
        inject([MessageService], messageService => {
            const payload = {
                messageKey: 'message_key',
                titleKey: 'title_key',
                options: {}
            };
            actions = cold('a', { a: new MessageActions.WarningMessage(payload) });
            spyOn(messageService, 'warning').and.callThrough();
            const expected = cold('b', { b: payload });
            expect(messageEffects.showWarningMessage$).toBeObservable(expected);
            expect(messageService.warning).toHaveBeenCalledTimes(1);
            expect(messageService.warning).toHaveBeenCalledWith(payload.messageKey, payload.titleKey, payload.options);
        })
    );

    it(
        'should call messageService.error on ERROR_MSG action',
        inject([MessageService], messageService => {
            const payload = {
                messageKey: 'message_key',
                titleKey: 'title_key',
                options: {}
            };
            actions = cold('a', { a: new MessageActions.ErrorMessage(payload) });
            spyOn(messageService, 'error').and.callThrough();
            const expected = cold('b', { b: payload });
            expect(messageEffects.showErrorMessage$).toBeObservable(expected);
            expect(messageService.error).toHaveBeenCalledTimes(1);
            expect(messageService.error).toHaveBeenCalledWith(payload.messageKey, payload.titleKey, payload.options);
        })
    );

    it('should test metadata', () => {
        expect(metadata.showInfoMessage$).toEqual({ dispatch: false });
        expect(metadata.showSuccessMessage$).toEqual({ dispatch: false });
        expect(metadata.showWarningMessage$).toEqual({ dispatch: false });
        expect(metadata.showErrorMessage$).toEqual({ dispatch: false });
    });
});
