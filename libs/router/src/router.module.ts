import { ModuleWithProviders, NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { routerReducer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';

import { featureName } from './config';
import { routerStateSerializerProvider } from './providers';
import { RouterEffects } from './router.effects';

@NgModule({
    imports: [
        StoreRouterConnectingModule.forRoot({ stateKey: featureName }),
        StoreModule.forFeature(featureName, routerReducer),
        EffectsModule.forFeature([RouterEffects])
    ],
    providers: [routerStateSerializerProvider]
})
export class RouterRootModule {}

@NgModule()
export class RouterModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: RouterRootModule
        };
    }
}
