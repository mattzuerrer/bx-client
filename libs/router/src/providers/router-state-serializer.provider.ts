import { RouterStateSerializer } from '@ngrx/router-store';

import { CustomRouterStateSerializer } from '../services';

export const routerStateSerializerProvider = { provide: RouterStateSerializer, useClass: CustomRouterStateSerializer };
