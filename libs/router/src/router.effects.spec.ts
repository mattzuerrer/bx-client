import { Location } from '@angular/common';
import { inject, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { noop, Observable } from 'rxjs';

import { RouterActions } from './router.actions';
import { RouterEffects } from './router.effects';

const locationStub = {
    back: () => noop(),
    forward: () => noop()
};

const locationProvider = {
    provide: Location,
    useValue: locationStub
};

const routerStub = {
    navigate: () => noop()
};

const routerProvider = {
    provide: Router,
    useValue: routerStub
};

describe('Routing effect', () => {
    let routerEffects: RouterEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<RouterEffects>;

    const navigationPayload = { path: ['test'], query: { a: 'q1' } };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [RouterEffects, provideMockActions(() => actions), locationProvider, routerProvider]
        });

        routerEffects = TestBed.get(RouterEffects);
        metadata = getEffectsMetadata(routerEffects);
    });

    it(
        'should navigate to path on GO',
        inject([Router], router => {
            actions = cold('a', { a: new RouterActions.Go(navigationPayload) });
            spyOn(router, 'navigate').and.callThrough();
            const expected = cold('b', { b: navigationPayload });
            expect(routerEffects.navigate$).toBeObservable(expected);
            expect(router.navigate).toHaveBeenCalledTimes(1);
            expect(router.navigate).toHaveBeenCalledWith(['test'], { queryParams: { a: 'q1' } });
        })
    );

    it(
        'should navigate back on BACK',
        inject([Location], location => {
            actions = cold('a', { a: new RouterActions.Back() });
            spyOn(location, 'back').and.callThrough();
            const expected = cold('b', { b: new RouterActions.Back() });
            expect(routerEffects.navigateBack$).toBeObservable(expected);
            expect(location.back).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should navigate forward on FORWARD',
        inject([Location], location => {
            actions = cold('a', { a: new RouterActions.Forward() });
            spyOn(location, 'forward').and.callThrough();
            const expected = cold('b', { b: new RouterActions.Forward() });
            expect(routerEffects.navigateForward$).toBeObservable(expected);
            expect(location.forward).toHaveBeenCalledTimes(1);
        })
    );

    it('should test metadata', () => {
        expect(metadata.navigate$).toEqual({ dispatch: false });
        expect(metadata.navigateBack$).toEqual({ dispatch: false });
        expect(metadata.navigateForward$).toEqual({ dispatch: false });
    });
});
