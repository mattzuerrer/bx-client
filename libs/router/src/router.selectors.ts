import { RouterReducerState } from '@ngrx/router-store';
import { createFeatureSelector } from '@ngrx/store';

import { featureName } from './config';
import { RouterStateUrl } from './models';

export namespace RouterSelectors {
    export const getState = createFeatureSelector<RouterReducerState<RouterStateUrl>>(featureName);
}
