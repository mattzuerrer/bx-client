import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { EffectsService } from '@bx-client/ngrx';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { RouterActions } from './router.actions';

@Injectable()
export class RouterEffects extends EffectsService {
    @Effect({ dispatch: false })
    navigate$: Observable<any> = this.actions$.pipe(
        ofType<RouterActions.Go>(RouterActions.GO),
        map(action => action.payload),
        tap(({ path, query: queryParams, extras }) => this.router.navigate(path, { queryParams, ...extras }))
    );

    @Effect({ dispatch: false })
    navigateBack$: Observable<any> = this.actions$.pipe(ofType<RouterActions.Back>(RouterActions.BACK), tap(() => this.location.back()));

    @Effect({ dispatch: false })
    navigateForward$: Observable<any> = this.actions$.pipe(
        ofType<RouterActions.Forward>(RouterActions.FORWARD),
        tap(() => this.location.forward())
    );

    constructor(private actions$: Actions, private router: Router, private location: Location) {
        super(null);
    }
}
