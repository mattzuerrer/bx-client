import { CdnService } from '@bx-client/core/src/environments';

export class CdnServiceMock {
    getAssetUrl(appVersion: string): string {
        return '/assets';
    }
}

export const CdnServiceMockProvider = {
    provide: CdnService,
    useClass: CdnServiceMock
};
