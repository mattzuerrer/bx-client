import { Inject, Injectable } from '@angular/core';
import { appEnvironmentToken } from '@bx-client/env';
import { provideAppCdnUrl } from '@bx-client/env/src/providers/environment.provider';

@Injectable()
export class CdnService {
    constructor(@Inject(appEnvironmentToken) private environment: AppEnvironment) {}

    getAssetUrl(appVersion: string = ''): string {
        return this.environment.isProduction && appVersion !== '' ? `${provideAppCdnUrl()}${appVersion}/assets` : '/assets';
    }
}
