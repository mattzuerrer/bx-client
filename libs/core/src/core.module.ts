import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule as NgRouterModule } from '@angular/router';
import { appEnvironmentToken, EnvModule, version } from '@bx-client/env';
import { HttpModule } from '@bx-client/http';
import { I18nModule } from '@bx-client/i18n';
import { NgrxModule } from '@bx-client/ngrx';
import { RouterModule } from '@bx-client/router';

import { CdnService, environment } from './environments';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgRouterModule,
        EnvModule.forRoot(),
        I18nModule.forRoot(),
        HttpModule.forRoot(),
        NgrxModule.forRoot(),
        RouterModule.forRoot()
    ],
    exports: [
        BrowserModule,
        BrowserAnimationsModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgRouterModule,
        EnvModule,
        I18nModule,
        HttpModule,
        NgrxModule,
        RouterModule
    ]
})
export class CoreRootModule {}

@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, NgRouterModule, I18nModule],
    exports: [CommonModule, FormsModule, ReactiveFormsModule, NgRouterModule, I18nModule]
})
export class CoreModule {
    static forRoot(appVersion: string): ModuleWithProviders {
        return {
            ngModule: CoreRootModule,
            providers: [
                CdnService,
                { provide: appEnvironmentToken, useValue: environment },
                { provide: version, useValue: appVersion }
            ]
        };
    }
}
