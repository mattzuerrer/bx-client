import { ModuleWithProviders, NgModule } from '@angular/core';
import { HotkeyModule } from 'angular2-hotkeys';

import { hotkeyOptions } from './config';

@NgModule({
    imports: [HotkeyModule.forRoot(hotkeyOptions)],
    exports: [HotkeyModule]
})
export class HotkeysRootModule {}

@NgModule({
    imports: [HotkeyModule],
    exports: [HotkeyModule]
})
export class HotkeysModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: HotkeysRootModule
        };
    }
}
