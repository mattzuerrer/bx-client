import { IHotkeyOptions } from 'angular2-hotkeys';

export const hotkeyOptions: IHotkeyOptions = {
    disableCheatSheet: true
};
