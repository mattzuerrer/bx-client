export class Opaque {
    constructor(private value: string) {}

    toString(): string {
        return this.value;
    }
}
