/**
 * @deprecated Use utilities provided by @bx-client/utils
 */
export const identity = item => item;

/**
 * @deprecated Use utilities provided by @bx-client/utils
 */
export const negation = item => !item;

/**
 * @deprecated Use utilities provided by @bx-client/utils
 */
export const sortBy = prop => items => items.sort((a, b) => (a[prop].toLowerCase() > b[prop].toLowerCase() ? 1 : -1));

/**
 * @deprecated Use utilities provided by @bx-client/utils
 */
export const isActive = item => item.isActive;

/**
 * @deprecated Use utilities provided by @bx-client/utils
 */
export const uniqueId = (item, index, arr) => arr.findIndex(current => current.id === item.id) === index;

/**
 * @deprecated Use utilities provided by @bx-client/utils
 */
export const by = (prop: string) => (...props: any[]) => item => props.indexOf(item[prop]) > -1;

/**
 * @deprecated Use utilities provided by @bx-client/utils
 */
export const byId = by('id');

/**
 * @deprecated Use utilities provided by @bx-client/utils
 */
export const byType = by('type');

/**
 * @deprecated Use utilities provided by @bx-client/utils
 */
export const sum = (a, b) => a + b;

/**
 * @deprecated Use utilities provided by @bx-client/utils
 */
export const or = (a, b) => item => a(item) || b(item);

/**
 * @deprecated Use utilities provided by @bx-client/utils
 */
export const and = (a, b) => item => a(item) && b(item);

/**
 * @deprecated Use utilities provided by @bx-client/utils
 */
export const isUnique = arr => new Set(arr).size === arr.length;
