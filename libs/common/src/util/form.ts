import { FormArray, FormControl, FormGroup } from '@angular/forms';

/**
 * @param formGroup
 * @param ignore An array of control names which shouldn't be checked
 */
export function isFormGroupEmpty(formGroup: FormGroup, ignore: string[] = []): boolean {
    return Object.keys(formGroup.controls).every(controlName => {
        if (ignore.includes(controlName)) {
            return true;
        }

        const control = formGroup.controls[controlName];

        if (control instanceof FormGroup) {
            return isFormGroupEmpty(control);
        }

        if (control instanceof FormArray) throw new Error('Not yet implemented'); // tslint:disable-line:curly

        // Special case since `0` is a falsely value
        if ((control as FormControl).value === 0) {
            return false;
        }

        return !(control as FormControl).value; // Return true for falsely values (no value = empty control)
    });
}
