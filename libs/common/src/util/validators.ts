import { AbstractControl, FormArray, ValidationErrors, ValidatorFn } from '@angular/forms';
import { byId } from '@bx-client/utils';
import * as moment from 'moment';
import * as isEmail from 'validator/lib/isEmail';

import { identity, isUnique } from './filters';

export interface DateValidationError extends Pick<ValidationErrors, 'date'> {
    date: { value: any };
}

/**
 * @whatItDoes Provides a set of validators used by form controls.
 *
 * A validator is a function that processes a {\@link FormControl} or collection of
 * controls and returns a map of errors. A null map means that validation has passed.
 *
 * ### Example
 *
 * ```typescript
 * var loginControl = new FormControl("", Validators.required)
 * ```
 *
 */

export class Validators {
    static delta = 0.000001;
    static minPasswordLength = 8;

    /**
     * Validator that requires controls to have a value larger than provided.
     * @param {?} min
     * @return {?}
     */
    static largerThan(min: number): ValidatorFn {
        const fn = (control: AbstractControl): { [key: string]: any } => {
            const value: number = control.value;
            return value > min ? null : { largerthan: { required: min, value } };
        };

        return fn;
    }

    /**
     * Validator that requires controls to have a value smaller than provided.
     * @param {?} max
     * @return {?}
     */
    static smallerThan(max: number): ValidatorFn {
        const fn = (control: AbstractControl): { [key: string]: any } => {
            const value: number = control.value;
            // !value as null check for number
            return !value || value < max ? null : { smallerthan: { required: max, value } };
        };

        return fn;
    }

    static notEqual(validationValue: number, delta: number = this.delta): ValidatorFn {
        const fn = (control: AbstractControl): { [key: string]: any } => {
            const value: number = control.value;
            return Math.abs(value - validationValue) >= delta ? null : { notEqual: { required: validationValue, value } };
        };

        return fn;
    }

    static fieldsEqual(a: string, b: string): ValidatorFn {
        const fn = (control: AbstractControl): { [key: string]: any } => {
            const valueA = control.get(a).value;
            const valueB = control.get(b).value;
            return valueA === valueB ? null : { fieldsequal: { value: valueA, compared: valueB } };
        };

        return fn;
    }

    static fieldsNotEqual(a: string, b: string): ValidatorFn {
        const fn = (control: AbstractControl): { [key: string]: any } => {
            const valueA = control.get(a).value;
            const valueB = control.get(b).value;
            return valueA !== valueB || !valueA ? null : { fieldsnotequal: { value: valueA } };
        };

        return fn;
    }

    static xor(a: string, b: string): ValidatorFn {
        const fn = (control: AbstractControl): { [key: string]: any } => {
            const valueA = control.get(a).value;
            const valueB = control.get(b).value;
            return !!valueA !== !!valueB ? null : { xor: { value: valueA, compared: valueB } };
        };

        return fn;
    }

    static uniqueArray(key: string): ValidatorFn {
        const fn = (controlArray: AbstractControl): { [key: string]: any } => {
            const items = (<FormArray>controlArray).controls.map(control => control.get(key).value).filter(identity);
            return isUnique(items) ? null : { uniquearray: { value: key } };
        };

        return fn;
    }

    static email(control: AbstractControl): { [key: string]: any } {
        const value = control.value;
        return !value || isEmail(value) ? null : { email: { value } };
    }

    static multiUniqueArray(...keys: string[]): ValidatorFn {
        const fn = (controlArray: AbstractControl): { [keys: string]: any } => {
            const items = (<FormArray>controlArray).controls
                .map(control => keys.map(key => control.get(key).value).filter(identity))
                .reduce((prev, current) => prev.concat(current), []);
            return isUnique(items) ? null : { multiuniquearray: { value: keys } };
        };

        return fn;
    }

    static firstItemUnique(...keys: string[]): ValidatorFn {
        const fn = (controlArray: AbstractControl): { [keys: string]: any } => {
            const first = keys.map(key => (<FormArray>controlArray).at(0).get(key).value).filter(identity);
            const rest = (<FormArray>controlArray).controls
                .filter((_, i) => i > 0)
                .map(control => keys.map(key => control.get(key).value).filter(identity));
            let index = 0;
            return rest.every((values, i) => {
                index = i + 1;
                return values.every(value => first.indexOf(value) === -1);
            })
                ? null
                : { firstitemunique: { value: keys, index } };
        };

        return fn;
    }

    static isInArray(options: any[]): ValidatorFn {
        const fn = (control: AbstractControl): { [key: string]: any } => {
            const value: any = control.value;
            return !value || options.some(option => option.id === value) ? null : { isInArray: { options, value } };
        };

        return fn;
    }

    static accountRange(accounts: any[]): ValidatorFn {
        const fn = (control: AbstractControl): { [key: string]: any } => {
            const fromAccountId = control.get('fromAccount').value;
            const toAccountId = control.get('toAccount').value;
            if (!toAccountId) {
                return null;
            }

            const fromAccount = accounts.filter(byId(fromAccountId)).map(account => account.accountNo)[0];
            const toAccount = accounts.filter(byId(toAccountId)).map(account => account.accountNo)[0];

            if (!fromAccountId) {
                return { accountRange: { fromAccount, toAccount } };
            }

            return fromAccount <= toAccount ? null : { accountRange: { fromAccount, toAccount } };
        };

        return fn;
    }

    static date(control: AbstractControl): DateValidationError | null {
        if (control.value) {
            const momentValue = !moment.isMoment(control.value) ? moment(control.value) : control.value;
            return momentValue.isValid() ? null : { date: { value: control.value } };
        }
        return null;
    }

    static dateRange(control: AbstractControl): any {
        const fromDate = control.get('fromDate').value;
        const toDate = control.get('toDate').value;
        if (toDate && fromDate && !toDate.isSameOrAfter(fromDate)) {
            return { dateRange: { fromDate, toDate } };
        } else {
            return null;
        }
    }

    static url(control: AbstractControl): { [key: string]: any } {
        const value = control.value;
        const isUrl = new RegExp(/^(http[s]?):\/\/?[a-z0-9]+([\-.]{1}[a-z0-9]+)*\.[a-z]{2,5}?(\/.*)?(\?.*)?(#.*)?$/gm);
        return !value || isUrl.test(value) ? null : { url: { value } };
    }

    static phone(control: AbstractControl): { [key: string]: any } {
        const value = control.value;
        const isPhone = new RegExp(/^\+?[0-9]+(\s|-|[0-9])*$/gm);
        return !value || isPhone.test(value) ? null : { phone: { value } };
    }

    static arePasswordsSame(control: AbstractControl): { [key: string]: any } {
        const password = control.get('password').value;
        const passwordRepeat = control.get('passwordRepeat').value;

        return password === passwordRepeat ? null : { notSame: { password, passwordRepeat } };
    }

    static strongPassword(control: AbstractControl): { [key: string]: any } {
        const hasNumber = /\d/.test(control.value);
        const hasUpper = /[A-Z]/.test(control.value);
        const hasLower = /[a-z]/.test(control.value);
        const valid = hasNumber && hasUpper && hasLower && control.value.length >= Validators.minPasswordLength;

        return valid ? null : { passwordNotStrong: true };
    }
}
