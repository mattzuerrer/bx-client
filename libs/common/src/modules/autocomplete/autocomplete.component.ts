import {
    Component,
    ElementRef,
    EventEmitter,
    forwardRef,
    HostBinding,
    HostListener,
    Input,
    Output,
    ViewChild,
    ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { byId } from '@bx-client/utils';

// tslint:disable-next-line:no-var-requires escape-string-regexp is a CommonJS module and cannot be imported using ES6 syntax
const escapeStringRegexp = require('escape-string-regexp');

export interface Item {
    id: number | string;
    name: string;
    format(): string;
}

const UP_ARROW = 38;
const DOWN_ARROW = 40;
const ENTER = 13;
const TAB = 9;
const ESCAPE = 27;

const autocompleteValueAccessor = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => AutocompleteComponent),
    multi: true
};

@Component({
    selector: 'app-autocomplete',
    templateUrl: 'autocomplete.component.html',
    styleUrls: ['autocomplete.component.scss'],
    providers: [autocompleteValueAccessor],
    encapsulation: ViewEncapsulation.None
})
export class AutocompleteComponent implements ControlValueAccessor {
    @Output() change: EventEmitter<any> = new EventEmitter<any>();

    @Output() newItemClicked: EventEmitter<void> = new EventEmitter<void>();

    @Input() readonly = false;

    @Input()
    @HostBinding('class.autocomplete-disabled')
    disabled = false;

    @Input() tabindex = 0;

    @Input() placeholder = '';

    @Input() isStrict = true;

    @Input() items: Item[] = [];

    @Input() isAriaInvalid = false;

    @Input() newItemOption: string = undefined;

    @ViewChild('input') inputField: ElementRef;

    focusedOption = 0;
    inputValue = '';
    inputFocused = false;
    list: Item[] = [];
    private selectedItem: Item = null;
    private preventBlur = false;

    constructor(private element: ElementRef) {}

    // tslint:disable:no-empty
    onChange(value: any): void {}

    onTouched(): any {}
    // tslint:enable:no-empty

    @Input() itemFormatter: (item: Item) => string = item => item.format();

    @Input()
    get value(): any {
        return this.selectedItem;
    }

    set value(value: any) {
        this.inputValue = '';
        this.selectedItem = this.items.find(item => value && item.id === value.id);
        if (this.selectedItem) {
            this.inputValue = this.formatValue(this.selectedItem);
        } else if (value && !this.isStrict) {
            this.inputValue = value;
        }
    }

    get isMenuVisible(): boolean {
        return (this.inputFocused || this.preventBlur) && !this.readonly;
    }

    formatValue(item: Item): string {
        return item && this.itemFormatter(item);
    }

    writeValue(value: any): void {
        this.preventBlur = false;
        if (this.isStrict) {
            this.value = this.items.find(byId(value));
        } else {
            this.value = value;
        }
    }

    registerOnTouched(fn: () => any): void {
        this.onTouched = fn;
    }

    registerOnChange(fn: (value: any) => void): void {
        this.onChange = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    /**
     * input event listener
     */
    handleKeyDown(event: KeyboardEvent): void {
        if (!this.disabled) {
            switch (event.keyCode) {
                case ESCAPE:
                    event.stopPropagation();
                    event.preventDefault();
                    if (this.inputValue) {
                        this.onClear();
                    }
                    break;
                case TAB:
                    if (this.isMenuVisible && this.list.length && this.inputValue) {
                        this.selectOption(event, this.focusedOption);
                    } else {
                        this.onClear(true);
                    }
                    this.handleMouseLeave();
                    break;
                case ENTER:
                    if (this.isMenuVisible && this.list.length) {
                        this.selectOption(event, this.focusedOption);
                        this.handleFocus();
                    }
                    this.handleMouseLeave();
                    break;
                case DOWN_ARROW:
                    event.preventDefault();
                    event.stopPropagation();
                    if (this.isMenuVisible && this.list.length) {
                        this.focusedOption =
                            this.focusedOption === this.list.length - 1 ? 0 : Math.min(this.focusedOption + 1, this.list.length - 1);
                        this.updateScroll();
                    }
                    break;
                case UP_ARROW:
                    event.preventDefault();
                    event.stopPropagation();
                    if (this.isMenuVisible && this.list.length) {
                        this.focusedOption = this.focusedOption === 0 ? this.list.length - 1 : Math.max(0, this.focusedOption - 1);
                        this.updateScroll();
                    }
                    break;
            }
        }
    }

    /**
     * input event listner
     * @param event
     * @param value
     */
    handleKeyup(event: KeyboardEvent, value: string): void {
        if (!this.disabled) {
            switch (event.keyCode) {
                case ESCAPE:
                case TAB:
                case DOWN_ARROW:
                case UP_ARROW:
                    break;
                default:
                    this.updateList(value);
            }
        }
    }

    /**
     * select option
     * @param event
     * @param index of selected option
     */
    selectOption(event: Event, index: number): void {
        this.selectedItem = this.list[index];
        this.inputValue = this.formatValue(this.selectedItem);
        this.change.emit(this.selectedItem);
        this.onChange(this.selectedItem.id);
    }

    /**
     * clear selected suggestion
     */
    onClear(keepInput: boolean = false): void {
        if (!this.disabled) {
            if (!keepInput) {
                this.inputValue = '';
            }
            this.selectedItem = null;
            this.updateList(this.inputValue);
            this.change.emit(this.selectedItem);
            this.onChange(null);
        }
    }

    /**
     * input focus listener
     */
    handleFocus(): void {
        this.inputFocused = true;
        this.updateList(this.inputValue);
        this.focusedOption = 0;
        this.onTouched();
    }

    /**
     * input blur listener
     */
    @HostListener('blur')
    handleBlur(): void {
        this.inputFocused = false;
        if (this.inputValue === '' && !this.preventBlur) {
            this.onClear();
        }
        if (!this.isStrict && !(this.selectedItem && this.selectedItem.name === this.inputValue)) {
            this.change.emit(this.inputValue);
            this.onChange(this.inputValue);
        }
    }

    /**
     * suggestion menu mouse enter listener
     */
    handleMouseEnter(): void {
        this.preventBlur = true;
    }

    /**
     * suggestion menu mouse leave listener
     */
    handleMouseLeave(): void {
        this.preventBlur = false;
    }

    onNewItem(): void {
        this.newItemClicked.emit();
    }

    focusInput(): void {
        this.inputField.nativeElement.focus();
    }

    /**
     * update scroll of suggestion menu
     */
    private updateScroll(): void {
        if (this.focusedOption < 0) {
            return;
        }
        const menuContainer = this.element.nativeElement.querySelector('.autocomplete-menu');
        if (!menuContainer) {
            return;
        }

        const choices = menuContainer.querySelectorAll('.option');
        if (choices.length < 1) {
            return;
        }

        const highlighted: any = choices[this.focusedOption];
        if (!highlighted) {
            return;
        }

        const top: number = highlighted.offsetTop + highlighted.clientHeight - menuContainer.scrollTop;
        const height: number = menuContainer.offsetHeight;

        if (top > height) {
            menuContainer.scrollTop += top - height;
        } else if (top < highlighted.clientHeight) {
            menuContainer.scrollTop -= highlighted.clientHeight - top;
        }
    }

    /**
     * Update suggestion to filter the query
     */
    private updateList(query: string): void {
        this.list = this.items.filter(this.filterItems(query));
        if (this.list.length && this.list[0].name !== query) {
            this.selectedItem = null;
        }
    }

    /**
     * Filter items by given query
     *
     * @param query
     * @returns {(item:Item)=>boolean}
     */
    private filterItems(query: string): (item: Item) => boolean {
        return item => new RegExp(escapeStringRegexp(query), 'ig').test(item.format());
    }
}
