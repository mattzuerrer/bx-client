import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { AutocompleteComponent } from './autocomplete.component';
import { HighlightPipe } from './autocomplete.pipe';

@NgModule({
    imports: [CommonModule, FormsModule, TranslateModule],
    declarations: [AutocompleteComponent, HighlightPipe],
    exports: [AutocompleteComponent]
})
export class AutocompleteModule {}
