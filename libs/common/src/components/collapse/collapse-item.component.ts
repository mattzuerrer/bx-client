/*tslint:disable:component-selector */
import { AfterViewChecked, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';

@Component({
    selector: 'bx-collapse-item',
    templateUrl: 'collapse-item.component.html',
    styleUrls: ['collapse-item.component.scss']
})
export class BxCollapseItemComponent implements AfterViewChecked {
    @Input() title: string;
    @Input() isVisible = false;
    @Output() toggle: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('collapse') element: ElementRef;

    private _height: number;

    get height(): number {
        return this.isVisible ? this._height : 0;
    }

    constructor(private _changeDetectionRef: ChangeDetectorRef) {}

    ngAfterViewChecked(): void {
        this._height = this.element.nativeElement.scrollHeight;
        this._changeDetectionRef.detectChanges();
    }

    resetHeight(): void {
        this.element.nativeElement.style.height = null;
    }

    toggleItem(): void {
        const isVisible = this.isVisible;
        this.toggle.emit();
        this.isVisible = !isVisible;
    }
}
