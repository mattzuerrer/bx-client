/*tslint:disable:component-selector */
import { AfterContentInit, Component, ContentChildren, forwardRef, OnDestroy, QueryList, ViewEncapsulation } from '@angular/core';
import { merge, Subscription } from 'rxjs';

import { BxCollapseItemComponent } from './collapse-item.component';

@Component({
    selector: 'bx-collapse',
    template: `<ng-content></ng-content>`,
    encapsulation: ViewEncapsulation.None
})
export class BxCollapseComponent implements AfterContentInit, OnDestroy {
    @ContentChildren(forwardRef(() => BxCollapseItemComponent))
    items: QueryList<BxCollapseItemComponent>;

    private subscription: Subscription;

    ngAfterContentInit(): void {
        const itemsArray = this.items.toArray();
        this.subscription = merge(...itemsArray.map(item => item.toggle)).subscribe(() =>
            itemsArray.forEach(item => (item.isVisible = false))
        );
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
