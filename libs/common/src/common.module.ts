import { CommonModule as CommonModuleNg } from '@angular/common';
import { NgModule } from '@angular/core';

import { bexioComponents } from './components';
import { bexioServices } from './services';

@NgModule({
    imports: [CommonModuleNg],
    providers: [bexioServices],
    declarations: [bexioComponents],
    exports: [bexioComponents]
})
export class CommonModule {}
