import { Injectable } from '@angular/core';

@Injectable()
export class ValidationService {
    isObject(obj: any): boolean {
        return obj === Object(obj);
    }

    isNumber(value: string): boolean {
        return /^[-+]?\d+$/.test(value);
    }

    isArray(obj: any): boolean {
        return obj instanceof Array;
    }
}
