import { Inject, Injectable } from '@angular/core';
import { indagiaStartDateToken } from '@bx-client/env';
import * as moment from 'moment';

@Injectable()
export class IndagiaService {
    private readonly indagiaStartDate: string;

    constructor(@Inject(indagiaStartDateToken) indagiaStartDate: string) {
        this.indagiaStartDate = indagiaStartDate;
    }

    indagiaActivationDate(toDate: Date): boolean {
        if (this.indagiaStartDate) {
            return moment(this.indagiaStartDate).isSameOrBefore(moment(toDate).format('YYYY-MM-DD'));
        }

        return false;
    }
}
