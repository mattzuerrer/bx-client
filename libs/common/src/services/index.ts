import { IndagiaService } from './indagia.service';
import { UtilitiesService } from './utilities.service';
import { ValidationService } from './validation.service';

export { IndagiaService } from './indagia.service';
export { UtilitiesService } from './utilities.service';
export { ValidationService } from './validation.service';

export const bexioServices: any[] = [UtilitiesService, ValidationService, IndagiaService];
