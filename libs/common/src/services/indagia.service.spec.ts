import { getTestBed, TestBed } from '@angular/core/testing';
import { indagiaStartDateToken } from '@bx-client/env';

import * as using from 'jasmine-data-provider';
import { configureTestSuite } from 'ng-bullet';

import { IndagiaService } from './indagia.service';

describe('AccountLedgerIndagiaService', () => {
    let injector: TestBed;
    let sut: IndagiaService;

    configureTestSuite(() => {
        TestBed.configureTestingModule({
            providers: [IndagiaService, { provide: indagiaStartDateToken, useValue: '2017-01-01' }]
        });

        injector = getTestBed();
        sut = injector.get(IndagiaService);
    });

    it('should be created', () => {
        expect(sut).toBeTruthy();
    });

    interface IsIndagiaActiveInterface {
        toDate: Date;
        expectedResult: boolean;
    }

    const isIndagiaActiveProvider: IsIndagiaActiveInterface[] = [
        { toDate: new Date('2016-12-01'), expectedResult: false },
        { toDate: new Date('2017-01-01'), expectedResult: true },
        { toDate: new Date('2018-12-01'), expectedResult: true }
    ];

    using(isIndagiaActiveProvider, (data: IsIndagiaActiveInterface) => {
        it(`indagiaActivationDate() should return "${data.expectedResult}" based on toDate`, () => {
            expect(sut.indagiaActivationDate(data.toDate)).toBe(data.expectedResult);
        });
    });

});
