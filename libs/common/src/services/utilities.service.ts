import { Injectable } from '@angular/core';
import { camelToSnakeCase, snakeToCamelCase } from '@bx-client/utils';

import { ValidationService } from './validation.service';

const standardObjects = [Date];

@Injectable()
export class UtilitiesService {
    constructor(private validationService: ValidationService) {}

    snakeToCamelArray(objects: object[]): object[] {
        for (let i = 0; i < objects.length; i++) {
            objects[i] = this.snakeToCamel(objects[i]);
        }
        return objects;
    }

    camelToSnakeArray(objects: object[]): object[] {
        for (let i = 0; i < objects.length; i++) {
            objects[i] = this.camelToSnake(objects[i]);
        }
        return objects;
    }

    snakeToCamel(object: object): object {
        return this.transformKeys(object, snakeToCamelCase);
    }

    camelToSnake(object: object): object {
        return this.transformKeys(object, camelToSnakeCase);
    }

    private transformKeys(obj: any, convert: (str: string) => string): object {
        if (!this.validationService.isObject(obj) || standardObjects.some(standardObject => obj instanceof standardObject)) {
            return obj;
        }
        if (this.validationService.isArray(obj)) {
            for (let i = 0; i < obj.length; i++) {
                obj[i] = this.transformKeys(obj[i], convert);
            }
            return obj;
        }
        const output = {};
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                output[convert(key)] = this.transformKeys(obj[key], convert);
            }
        }
        return output;
    }
}
