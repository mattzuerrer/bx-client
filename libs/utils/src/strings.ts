/**
 * Transforms a camelCased string into a snake_cased one.
 * @param {string} str The string in camelCase to be transformed.
 */
export function camelToSnakeCase(str: string): string {
    return str.replace(/([A-Z])/g, a => '_' + a.toLowerCase());
}

/**
 * Transforms a snake_cased string into a camelCased one.
 * @param {string} str The string in snake_case to be transformed.
 */
export function snakeToCamelCase(str: string): string {
    return str.replace(/_\w/g, a => a[1].toUpperCase());
}
