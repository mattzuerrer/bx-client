/**
 * Helper for applying changes to state with forced typescript checks.
 * Spread operator does not force typescript checks as returned object is no longer an object literal.
 * Read more: https://stackoverflow.com/a/44530943
 *
 * @param state
 * @param changes
 */
export const applyChanges = <S, K extends keyof S>(state: S, changes: Pick<S, K>): S => Object.assign({}, state, changes);
