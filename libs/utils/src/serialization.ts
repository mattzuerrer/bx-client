type serializationStrategy = 'exposeAll' | 'excludeAll';
export const excludeAllStrategy: serializationStrategy = 'excludeAll';
export const exposeAllStrategy: serializationStrategy = 'exposeAll';

export function serializeType<T>(object: T): () => T {
    return (): T => object;
}
