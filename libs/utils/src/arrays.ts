export function isArray<T>(value: T | T[]): value is T[] {
    return value instanceof Array;
}
