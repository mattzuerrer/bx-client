import * as moment from 'moment';

export function convertToISO8601String(date: Date): string {
    return moment(date).format('YYYY-MM-DD');
}
