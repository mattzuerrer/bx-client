import { NgModule } from '@angular/core';

import { PluralizePipe } from './pluralize.pipe';
import { SafeHtmlPipe } from './safe-html.pipe';

@NgModule({
    declarations: [PluralizePipe, SafeHtmlPipe],
    exports: [PluralizePipe, SafeHtmlPipe]
})
export class PipesModule {}
