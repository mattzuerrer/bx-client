import { inject, TestBed } from '@angular/core/testing';
import { DomSanitizer } from '@angular/platform-browser';
import { noop } from 'rxjs';

import { SafeHtmlPipe } from './safe-html.pipe';

const html = '<b>test html</b>';

const safeHtml = 'SAFE_<b>test html</b>_SAFE';

const domSanitizerProvider = {
    provide: DomSanitizer,
    useValue: {
        bypassSecurityTrustHtml: () => noop()
    }
};

describe('Safe html pipe', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({ providers: [SafeHtmlPipe, domSanitizerProvider] });
    });

    it(
        'should transform string to trusted html',
        inject([SafeHtmlPipe, DomSanitizer], (safeHtmlPipe, domSanitizer) => {
            spyOn(domSanitizer, 'bypassSecurityTrustHtml').and.returnValue(safeHtml);
            const result = safeHtmlPipe.transform(html);
            expect(domSanitizer.bypassSecurityTrustHtml).toHaveBeenCalledWith(html);
            expect(result).toBe(safeHtml);
        })
    );
});
