import { I18nPluralPipe } from '@angular/common';
import { ChangeDetectorRef, Pipe, PipeTransform } from '@angular/core';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';

const pluralMappings = {
    users: { '=1': 'globals.users.singular', other: 'globals.users.plural' },
    additional_users: { '=1': 'globals.additional_users.singular', other: 'globals.additional_users.plural' },
    employees: { '=1': 'globals.employees.singular', other: 'globals.employees.plural' },
    days: { '=1': 'globals.days.singular', other: 'globals.days.plural' },
    decimal_places: { '=1': 'globals.decimal_places.singular', other: 'globals.decimal_places.plural' }
};

@Pipe({ name: 'pluralize', pure: true })
export class PluralizePipe implements PipeTransform {
    translatePipe: TranslatePipe;

    constructor(private i18nPluralPipe: I18nPluralPipe, translateService: TranslateService, changeDetectorRef: ChangeDetectorRef) {
        this.translatePipe = new TranslatePipe(translateService, changeDetectorRef);
    }

    transform(count: number, key: string): string {
        const pluralMapping = pluralMappings[key] || { other: '' };
        const translationKey = this.i18nPluralPipe.transform(count, pluralMapping);
        const translationParams = { count };
        return this.translatePipe.transform(translationKey, translationParams);
    }
}
