import { I18nPluralPipe } from '@angular/common';
import { ChangeDetectorRef } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';

import { I18nPluralMockPipe } from './mocks/i18n-plural.mock.pipe';
import { TranslateMockPipe } from './mocks/translate.mock.pipe';
import { PluralizePipe } from './pluralize.pipe';

const I18nPluralPipeProvider = {
    provide: I18nPluralPipe,
    useClass: I18nPluralMockPipe
};

const translatePipeProvider = {
    provide: TranslatePipe,
    useClass: TranslateMockPipe
};

const translateServiceProviders = {
    provide: TranslateService,
    useValue: null
};

const changeDetectorRefProvider = {
    provide: ChangeDetectorRef,
    useValue: null
};

describe('Pluralize pipe', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [PluralizePipe, I18nPluralPipeProvider, translatePipeProvider, translateServiceProviders, changeDetectorRefProvider]
        });
    });

    describe('users key', () => {
        it(
            'should transform one user',
            inject([PluralizePipe, I18nPluralPipe, TranslatePipe], (pluralizePipe, i18nPluralPipe, translatePipe) => {
                pluralizePipe.translatePipe = translatePipe;
                spyOn(i18nPluralPipe, 'transform').and.returnValue('users_key_singular');
                spyOn(translatePipe, 'transform').and.returnValue('pluralized_users');
                const result = pluralizePipe.transform(1, 'users');
                expect(i18nPluralPipe.transform).toHaveBeenCalledWith(1, {
                    '=1': 'globals.users.singular',
                    other: 'globals.users.plural'
                });
                expect(translatePipe.transform).toHaveBeenCalledWith('users_key_singular', {
                    count: 1
                });
                expect(result).toBe('pluralized_users');
            })
        );

        it(
            'should transform multiple users',
            inject([PluralizePipe, I18nPluralPipe, TranslatePipe], (pluralizePipe, i18nPluralPipe, translatePipe) => {
                pluralizePipe.translatePipe = translatePipe;
                spyOn(i18nPluralPipe, 'transform').and.returnValue('users_key_plural');
                spyOn(translatePipe, 'transform').and.returnValue('pluralized_users');
                const result = pluralizePipe.transform(15, 'users');
                expect(i18nPluralPipe.transform).toHaveBeenCalledWith(15, {
                    '=1': 'globals.users.singular',
                    other: 'globals.users.plural'
                });
                expect(translatePipe.transform).toHaveBeenCalledWith('users_key_plural', {
                    count: 15
                });
                expect(result).toBe('pluralized_users');
            })
        );
    });

    describe('unknown key', () => {
        it(
            'should transform to empty string',
            inject([PluralizePipe, I18nPluralPipe, TranslatePipe], (pluralizePipe, i18nPluralPipe, translatePipe) => {
                pluralizePipe.translatePipe = translatePipe;
                spyOn(i18nPluralPipe, 'transform').and.returnValue('');
                spyOn(translatePipe, 'transform').and.returnValue('');
                const result = pluralizePipe.transform(15, 'unknown');
                expect(i18nPluralPipe.transform).toHaveBeenCalledWith(15, { other: '' });
                expect(translatePipe.transform).toHaveBeenCalledWith('', { count: 15 });
                expect(result).toBe('');
            })
        );
    });
});
