import { NgModule } from '@angular/core';

import { TranslateMockPipe } from './translate.mock.pipe';

export { TranslateServiceMockProvider } from './translate.service.mock';
export { TranslateMockPipe } from './translate.mock.pipe';
export { GlobalizeServiceMock, GlobalizeServiceMockProvider } from './globalize.service.mock';

@NgModule({
    declarations: [TranslateMockPipe]
})
export class I18nMockModule {}
