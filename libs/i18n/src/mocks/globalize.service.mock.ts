import { noop } from 'rxjs';

import { GlobalizeService } from '../services';

// tslint:disable-next-line:no-var-requires parse-decimal-number is a CommonJS module and cannot be imported using ES6 syntax
const parseDecimalNumber: any = require('parse-decimal-number');

export class GlobalizeServiceMock {
    static defaultLocale = 'de-CH';

    static supportedLocales: any = { 'de-CH': { delimiters: { thousands: "'", decimal: '.' } } };

    init(locale: string): void {
        noop();
    }

    parseNumber(number: string): number {
        return parseDecimalNumber(number, this.getLocaleFormat().delimiters);
    }

    getLocale(): string {
        return 'de-CH';
    }

    getLocaleFormat(): any {
        return { delimiters: { thousands: "'", decimal: '.' } };
    }

    toCurrency(): string {
        return '1\'000.00 CHF';
    }

    toNumber(): number {
        return 1000;
    }
}

export const GlobalizeServiceMockProvider = {
    provide: GlobalizeService,
    useClass: GlobalizeServiceMock
};
