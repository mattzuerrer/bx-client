import { Directive, ElementRef, forwardRef, HostListener, Input, Renderer2 } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { GlobalizeService } from '../services';

const currencyFormatValueAccessor = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CurrencyFormatAccessorDirective),
    multi: true
};

@Directive({ selector: 'input[appCurrencyInput]', providers: [currencyFormatValueAccessor] })
export class CurrencyFormatAccessorDirective implements ControlValueAccessor {
    @Input() customFormat: string;

    constructor(private renderer: Renderer2, private elementRef: ElementRef, private globalizeService: GlobalizeService) {}

    // tslint:disable:no-empty
    onChange(value: any): void {}

    onTouched(): any {}
    // tslint:disable:no-empty

    registerOnTouched(fn: () => any): void {
        this.onTouched = fn;
    }

    registerOnChange(fn: (value: any) => void): void {
        this.onChange = fn;
    }

    writeValue(value: string): void {
        this.renderer.setProperty(this.elementRef.nativeElement, 'value', this.globalizeService.toCurrency(value, this.customFormat));
    }

    setDisabledState(isDisabled: boolean): void {
        this.renderer.setProperty(this.elementRef.nativeElement, 'disabled', isDisabled);
    }

    @HostListener('focus', ['$event.target.value'])
    onFocus(value: string): void {
        value = this.globalizeService.toCurrency(value);
        this.renderer.setProperty(this.elementRef.nativeElement, 'value', value);
    }

    @HostListener('blur', ['$event.target.value'])
    onBlur(value: string): void {
        this.onTouched();
        const parsedValue = this.globalizeService.toCurrency(value, this.customFormat);
        if (parsedValue !== value) {
            this.onChange(this.globalizeService.toNumber(parsedValue));
            this.writeValue(parsedValue);
        }
    }

    @HostListener('input', ['$event.target.value'])
    onInput(value: string): void {
        const parsedValue = this.globalizeService.toNumber(value);
        this.onChange(parsedValue);
    }
}
