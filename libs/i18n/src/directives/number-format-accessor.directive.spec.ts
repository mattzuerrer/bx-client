import { TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { MockComponent } from 'ng2-mock-component';

import { GlobalizeServiceMockProvider } from '../mocks';

import { NumberFormatAccessorDirective } from './number-format-accessor.directive';

describe('NumberFormatAccessorDirective', () => {
    let fixture: any;

    const TestComponent: any = MockComponent({ template: '<div><input appNumberInput /></div>' });

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [NumberFormatAccessorDirective, TestComponent],
            providers: [GlobalizeServiceMockProvider]
        });
    });

    it('should handle focus event', () => {
        fixture = TestBed.createComponent(TestComponent);
        fixture.detectChanges();

        const directiveElement = fixture.debugElement.query(By.directive(NumberFormatAccessorDirective));
        expect(directiveElement).not.toBeNull();

        const input = fixture.nativeElement.querySelector('input');
        input.value = "1'000.00";
        directiveElement.triggerEventHandler('focus', { target: input });
        expect(input.value).toBe('1000.00');
    });

    it('should handle blur event', () => {
        fixture = TestBed.createComponent(TestComponent);
        fixture.detectChanges();

        const directiveElement = fixture.debugElement.query(By.directive(NumberFormatAccessorDirective));
        expect(directiveElement).not.toBeNull();

        const input = fixture.nativeElement.querySelector('input');
        input.value = '1000.00';
        directiveElement.triggerEventHandler('blur', { target: input });
        expect(input.value).toBe('1,000.00');
    });
});
