import { NgModule } from '@angular/core';

import { CurrencyFormatAccessorDirective } from './currency-format-accessor.directive';
import { NumberFormatAccessorDirective } from './number-format-accessor.directive';

@NgModule({
    declarations: [CurrencyFormatAccessorDirective, NumberFormatAccessorDirective],
    exports: [CurrencyFormatAccessorDirective, NumberFormatAccessorDirective]
})
export class I18nDirectivesModule {}
