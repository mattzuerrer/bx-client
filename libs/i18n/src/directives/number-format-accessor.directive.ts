import { DecimalPipe } from '@angular/common';
import { Directive, ElementRef, forwardRef, HostListener, Input, Renderer2 } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { GlobalizeService } from '../services';

const numberFormatValueAccessor = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => NumberFormatAccessorDirective),
    multi: true
};

@Directive({ selector: 'input[appNumberInput]', providers: [numberFormatValueAccessor, DecimalPipe] })
export class NumberFormatAccessorDirective implements ControlValueAccessor {
    @Input() numberFormat = '1.2-2';

    @Input() defaultValue = 0;

    constructor(
        private renderer: Renderer2,
        private elementRef: ElementRef,
        private globalizeService: GlobalizeService,
        private toDecimal: DecimalPipe
    ) {}

    // tslint:disable:no-empty
    onChange(value: any): void {}

    onTouched(): any {}
    // tslint:disable:no-empty

    registerOnTouched(fn: () => any): void {
        this.onTouched = fn;
    }

    registerOnChange(fn: (value: any) => void): void {
        this.onChange = fn;
    }

    writeValue(value: number): void {
        const formattedValue = this.toDecimal.transform(this.checkDefaultValue(value), this.numberFormat);
        this.renderer.setProperty(this.elementRef.nativeElement, 'value', formattedValue);
    }

    setDisabledState(isDisabled: boolean): void {
        this.renderer.setProperty(this.elementRef.nativeElement, 'disabled', isDisabled);
    }

    @HostListener('focus', ['$event.target.value'])
    onFocus(value: string): void {
        const format = this.globalizeService.getLocaleFormat();
        value = value.replace(new RegExp(format.delimiters.thousands, 'g'), '');
        this.renderer.setProperty(this.elementRef.nativeElement, 'value', value);
    }

    @HostListener('blur', ['$event.target.value'])
    onBlur(value: string): void {
        this.onTouched();
        const parsedValue = this.globalizeService.parseNumber(value);
        this.writeValue(parsedValue);
    }

    @HostListener('input', ['$event.target.value'])
    onInput(value: string): void {
        const parsedValue = this.globalizeService.parseNumber(value);
        this.onChange(parsedValue);
    }

    private checkDefaultValue(value: number): number {
        let toNumber = Number(value);
        if (isNaN(toNumber)) {
            toNumber = this.defaultValue;
            this.onChange(toNumber);
        }
        return toNumber;
    }
}
