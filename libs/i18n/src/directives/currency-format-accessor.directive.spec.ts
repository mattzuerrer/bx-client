import { inject, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { MockComponent } from 'ng2-mock-component';

import { GlobalizeServiceMockProvider } from '../mocks';
import { GlobalizeService } from '../services';

import { CurrencyFormatAccessorDirective } from './currency-format-accessor.directive';

describe('CurrencyFormatAccessorDirective', () => {
    let fixture: any;

    const TestComponent: any = MockComponent({ template: '<div><input appCurrencyInput /></div>' });

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [CurrencyFormatAccessorDirective, TestComponent],
            providers: [GlobalizeServiceMockProvider]
        });
    });

    it(
        'should handle focus event',
        inject([GlobalizeService], globalizeService => {
            fixture = TestBed.createComponent(TestComponent);
            fixture.detectChanges();

            const directiveElement = fixture.debugElement.query(By.directive(CurrencyFormatAccessorDirective));
            expect(directiveElement).not.toBeNull();

            spyOn(globalizeService, 'toCurrency').and.returnValue("1'001.99 CHF");

            const input = fixture.nativeElement.querySelector('input');
            input.value = "1'002.88";
            directiveElement.triggerEventHandler('focus', { target: input });
            expect(globalizeService.toCurrency).toHaveBeenCalledWith("1'002.88");
            expect(globalizeService.toCurrency).toHaveBeenCalledTimes(1);
            expect(input.value).toBe("1'001.99 CHF");
        })
    );

    it(
        'should handle blur event',
        inject([GlobalizeService], globalizeService => {
            fixture = TestBed.createComponent(TestComponent);
            fixture.detectChanges();

            spyOn(globalizeService, 'toCurrency').and.returnValue("1'001.99 CHF");

            const directiveElement = fixture.debugElement.query(By.directive(CurrencyFormatAccessorDirective));
            expect(directiveElement).not.toBeNull();

            const input = fixture.nativeElement.querySelector('input');
            input.value = "1'002.88";
            directiveElement.triggerEventHandler('blur', { target: input });
            expect(input.value).toBe("1'001.99 CHF");
        })
    );
});
