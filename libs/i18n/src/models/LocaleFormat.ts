import { LocaleAbbreviation } from './LocaleAbbreviation';
import { LocaleCurrency } from './LocaleCurrency';
import { LocaleDelimiter } from './LocaleDelimiter';

export interface LocaleFormat {
    delimiters: LocaleDelimiter;
    abbreviations: LocaleAbbreviation;
    currency: LocaleCurrency;
    ordinal(arg?: any): string;
}
