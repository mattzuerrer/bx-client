export interface LocaleAbbreviation {
    thousand: string;
    million: string;
    billion: string;
    trillion: string;
}
