export interface LocaleDelimiter {
    thousands: string;
    decimal: string;
}
