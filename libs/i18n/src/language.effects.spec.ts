import { inject, TestBed } from '@angular/core/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { noop, Observable } from 'rxjs';

import { LanguageActions } from './language.actions';
import { LanguageEffects } from './language.effects';
import { LanguageService } from './services';

const languageServiceStub = {
    setLanguage: () => noop(),
    setToBrowserLanguage: () => noop()
};

describe('Message Effects', () => {
    let languageEffects: LanguageEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<LanguageEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                LanguageEffects,
                {
                    provide: LanguageService,
                    useValue: languageServiceStub
                },
                provideMockActions(() => actions)
            ]
        });

        languageEffects = TestBed.get(LanguageEffects);
        metadata = getEffectsMetadata(languageEffects);
    });

    it(
        'should call setLanguage on SET_LANGUAGE action',
        inject([LanguageService], languageService => {
            const payload = 'en';
            spyOn(languageService, 'setLanguage').and.callThrough();

            actions = cold('a', { a: new LanguageActions.SetLanguageAction(payload) });
            const expected = cold('b', { b: new LanguageActions.SetLanguageAction(payload) });

            expect(languageEffects.setLanguage$).toBeObservable(expected);
            expect(languageService.setLanguage).toHaveBeenCalledTimes(1);
            expect(languageService.setLanguage).toHaveBeenCalledWith(payload);
        })
    );

    it(
        'should call setToBrowserLanguage on SET_TO_BROWSER_LANGUAGE action',
        inject([LanguageService], languageService => {
            spyOn(languageService, 'setToBrowserLanguage').and.callThrough();

            actions = cold('a', { a: new LanguageActions.SetToBrowserLanguageAction() });
            const expected = cold('b', { b: new LanguageActions.SetToBrowserLanguageAction() });

            expect(languageEffects.setToBrowserLanguage$).toBeObservable(expected);
            expect(languageService.setToBrowserLanguage).toHaveBeenCalledTimes(1);
            expect(languageService.setToBrowserLanguage).toHaveBeenCalledWith();
        })
    );

    it('should test metadata', () => {
        expect(metadata.setLanguage$).toEqual({ dispatch: false });
        expect(metadata.setToBrowserLanguage$).toEqual({ dispatch: false });
    });
});
