import { Action } from '@ngrx/store';

export namespace LanguageActions {
    export const SET_LANGUAGE = '[Language] Set language';
    export const SET_TO_BROWSER_LANGUAGE = '[Language] Set to browser language';

    export class SetLanguageAction implements Action {
        readonly type = SET_LANGUAGE;

        constructor(public payload: string) {}
    }

    export class SetToBrowserLanguageAction implements Action {
        readonly type = SET_TO_BROWSER_LANGUAGE;
    }

    export type Actions = SetLanguageAction | SetToBrowserLanguageAction;
}
