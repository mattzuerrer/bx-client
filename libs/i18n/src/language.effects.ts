import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { LanguageActions } from './language.actions';
import { LanguageService } from './services';

@Injectable()
export class LanguageEffects {
    @Effect({ dispatch: false })
    setLanguage$: Observable<any> = this.actions$.pipe(
        ofType<LanguageActions.SetLanguageAction>(LanguageActions.SET_LANGUAGE),
        tap(action => this.languageService.setLanguage(action.payload))
    );

    @Effect({ dispatch: false })
    setToBrowserLanguage$: Observable<any> = this.actions$.pipe(
        ofType<LanguageActions.SetToBrowserLanguageAction>(LanguageActions.SET_TO_BROWSER_LANGUAGE),
        tap(() => this.languageService.setToBrowserLanguage())
    );

    constructor(private actions$: Actions, private languageService: LanguageService) {}
}
