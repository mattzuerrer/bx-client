import { InjectionToken } from '@angular/core';

export const localeFormatToken = new InjectionToken('LOCALE_FORMAT');

export const defaultLocaleFormatToken = new InjectionToken('DEFAULT_LOCALE_FORMAT');

export const supportedLocaleFormatsToken = new InjectionToken('SUPPORTED_LOCALE_FORMATS');

export const defaultNumberFormatToken = new InjectionToken('DEFAULT_NUMBER_FORMAT');
