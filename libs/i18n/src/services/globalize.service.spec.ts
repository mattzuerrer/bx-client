import { TestBed } from '@angular/core/testing';
import { defaultLocaleToken, localeToken } from '@bx-client/env';

import { localesFormat } from '../config';
import { localeFormatProviders, numberFormatProvider } from '../providers';
import { matDatepickerDateFormatToken } from '../providers/material/tokens';
import { localeFormatToken } from '../tokens';

import { GlobalizeService } from './globalize.service';

describe('Globalize service', () => {
    let globalizeService: GlobalizeService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                GlobalizeService,
                { provide: localeToken, useValue: 'de-CH' },
                { provide: defaultLocaleToken, useValue: 'de-CH' },
                { provide: matDatepickerDateFormatToken },
                localeFormatProviders,
                numberFormatProvider
            ]
        });
    });

    it('should init swiss German locale and parse localized format', () => {
        globalizeService = TestBed.get(GlobalizeService);
        expect(globalizeService.getLocale()).toBe('de-CH');
        expect(globalizeService.getLocaleFormat().delimiters.thousands).toEqual("'");
        expect(globalizeService.getLocaleFormat().delimiters.decimal).toEqual('.');

        expect(globalizeService.toCurrency('0.00')).toBe('0.00');
        expect(globalizeService.toCurrency('0.30')).toBe('0.30');
        expect(globalizeService.toCurrency('3.24')).toBe('3.24');
        expect(globalizeService.toCurrency("1'098.746")).toBe("1'098.746");
        expect(globalizeService.toCurrency("234'567'121.987")).toBe("234'567'121.987");
        expect(globalizeService.toCurrency('')).toBe('0.00');
        expect(globalizeService.toCurrency("1'098.746letters")).toBe("1'098.746");

        expect(globalizeService.toNumber('0.00')).toBe(0.0);
        expect(globalizeService.toNumber('0.30')).toBe(0.3);
        expect(globalizeService.toNumber('3.24')).toBe(3.24);
        expect(globalizeService.toNumber("1'098.746")).toBe(1098.746);
        expect(globalizeService.toNumber("234'567'121.987")).toBe(234567121.987);
        expect(globalizeService.toNumber('')).toBe(null);
        expect(globalizeService.toNumber("1'098.746letters")).toBe(1098.746);
    });

    it('should init swiss French locale and parse localized format', () => {
        TestBed.overrideProvider(localeToken, {
            useValue: 'fr-CH'
        }).overrideProvider(localeFormatToken, {
            useValue: localesFormat['fr-CH']
        });
        globalizeService = TestBed.get(GlobalizeService);
        expect(globalizeService.getLocale()).toBe('fr-CH');
        expect(globalizeService.getLocaleFormat().delimiters.thousands).toEqual("'");
        expect(globalizeService.getLocaleFormat().delimiters.decimal).toEqual('.');
    });

    it('should init swiss Italien locale and parse localized format', () => {
        TestBed.overrideProvider(localeToken, {
            useValue: 'it-CH'
        }).overrideProvider(localeFormatToken, {
            useValue: localesFormat['it-CH']
        });
        globalizeService = TestBed.get(GlobalizeService);
        expect(globalizeService.getLocale()).toBe('it-CH');
        expect(globalizeService.getLocaleFormat().delimiters.thousands).toEqual("'");
        expect(globalizeService.getLocaleFormat().delimiters.decimal).toEqual('.');
    });

    it('should init German locale and parse localized format', () => {
        TestBed.overrideProvider(localeToken, {
            useValue: 'de-DE'
        }).overrideProvider(localeFormatToken, {
            useValue: localesFormat['de-DE']
        });
        globalizeService = TestBed.get(GlobalizeService);
        expect(globalizeService.getLocale()).toBe('de-DE');
        expect(globalizeService.getLocaleFormat().delimiters.thousands).toEqual('.');
        expect(globalizeService.getLocaleFormat().delimiters.decimal).toEqual(',');

        expect(globalizeService.toCurrency('0,00')).toBe('0,00');
        expect(globalizeService.toCurrency('0,30')).toBe('0,30');
        expect(globalizeService.toCurrency('3,24')).toBe('3,24');
        expect(globalizeService.toCurrency('1.098,746')).toBe('1.098,746');
        expect(globalizeService.toCurrency('234.567.121,987')).toBe('234.567.121,987');
        expect(globalizeService.toCurrency('')).toBe('0,00');
        expect(globalizeService.toCurrency('1.098,746letters')).toBe('1.098,746');

        expect(globalizeService.toNumber('0,00')).toBe(0.0);
        expect(globalizeService.toNumber('0,30')).toBe(0.3);
        expect(globalizeService.toNumber('3,24')).toBe(3.24);
        expect(globalizeService.toNumber('1.098,746')).toBe(1098.746);
        expect(globalizeService.toNumber('234.567.121,987')).toBe(234567121.987);
        expect(globalizeService.toNumber('')).toBe(null);
        expect(globalizeService.toNumber('1.098,746letters')).toBe(1098.746);
    });

    it('should init austrian German locale and parse localized format', () => {
        TestBed.overrideProvider(localeToken, {
            useValue: 'de-AT'
        }).overrideProvider(localeFormatToken, {
            useValue: localesFormat['de-AT']
        });
        globalizeService = TestBed.get(GlobalizeService);
        expect(globalizeService.getLocale()).toBe('de-AT');
        expect(globalizeService.getLocaleFormat().delimiters.thousands).toEqual('.');
        expect(globalizeService.getLocaleFormat().delimiters.decimal).toEqual(',');
    });

    it('should init english locale and parse localized format', () => {
        TestBed.overrideProvider(localeToken, {
            useValue: 'en-GB'
        }).overrideProvider(localeFormatToken, {
            useValue: localesFormat['en-GB']
        });
        globalizeService = TestBed.get(GlobalizeService);
        expect(globalizeService.getLocale()).toBe('en-GB');
        expect(globalizeService.getLocaleFormat().delimiters.thousands).toEqual(',');
        expect(globalizeService.getLocaleFormat().delimiters.decimal).toEqual('.');

        expect(globalizeService.toCurrency('0.00')).toBe('0.00');
        expect(globalizeService.toCurrency('0.30')).toBe('0.30');
        expect(globalizeService.toCurrency('3.24')).toBe('3.24');
        expect(globalizeService.toCurrency('1,098.746')).toBe('1,098.746');
        expect(globalizeService.toCurrency('234,567,121.987')).toBe('234,567,121.987');
        expect(globalizeService.toCurrency('')).toBe('0.00');
        expect(globalizeService.toCurrency('1,098.746letters')).toBe('1,098.746');

        expect(globalizeService.toNumber('0.00')).toBe(0.0);
        expect(globalizeService.toNumber('0.30')).toBe(0.3);
        expect(globalizeService.toNumber('3.24')).toBe(3.24);
        expect(globalizeService.toNumber('1,098.746')).toBe(1098.746);
        expect(globalizeService.toNumber('234,567,121.987')).toBe(234567121.987);
        expect(globalizeService.toNumber('')).toBe(null);
        expect(globalizeService.toNumber('1,098.746letters')).toBe(1098.746);
    });

    it('should allow providing a custom format optionally', () => {
        expect(globalizeService.toCurrency('1.123456789', '0,0.000')).toBe('1.123');
        expect(globalizeService.toCurrency('1.123456789')).toBe('1.123457');
    });
});
