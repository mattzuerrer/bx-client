import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class LanguageService {
    constructor(private translateService: TranslateService) {}

    setLanguage(language: string): void {
        this.translateService.use(this.getShortLocale(language));
    }

    setToBrowserLanguage(): void {
        this.translateService.use(this.translateService.getBrowserLang());
    }

    private getShortLocale(language: string): string {
        return language.split('_')[0];
    }
}
