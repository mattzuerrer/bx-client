import { inject, TestBed } from '@angular/core/testing';
import { LanguageService } from '@bx-client/i18n';
import { TranslateService } from '@ngx-translate/core';
import { noop } from 'rxjs';

const translateServiceStub = {
    use: () => noop(),
    getBrowserLang: () => noop()
};

describe('Language service', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                LanguageService,
                {
                    provide: TranslateService,
                    useValue: translateServiceStub
                }
            ]
        });
    });

    it(
        'should get a short locale from a language string',
        inject([LanguageService], languageService => {
            expect(languageService.getShortLocale('en_GB')).toBe('en');
        })
    );

    it(
        'should invoke translate service setLanguage',
        inject([LanguageService, TranslateService], (languageService, translateService) => {
            spyOn(languageService, 'setLanguage').and.callThrough();
            spyOn(translateService, 'use').and.callThrough();

            languageService.setLanguage('en');

            expect(languageService.setLanguage).toHaveBeenCalledTimes(1);
            expect(languageService.setLanguage).toHaveBeenCalledWith('en');
            expect(translateService.use).toHaveBeenCalledTimes(1);
            expect(translateService.use).toHaveBeenCalledWith('en');
        })
    );

    it(
        'should invoke translate service setToBrowserLanguage',
        inject([LanguageService, TranslateService], (languageService, translateService) => {
            spyOn(languageService, 'setToBrowserLanguage').and.callThrough();
            spyOn(translateService, 'use').and.callThrough();
            spyOn(translateService, 'getBrowserLang').and.callThrough();

            languageService.setToBrowserLanguage();

            expect(languageService.setToBrowserLanguage).toHaveBeenCalledTimes(1);
            expect(translateService.use).toHaveBeenCalledTimes(1);
            expect(translateService.getBrowserLang).toHaveBeenCalledTimes(1);
        })
    );
});
