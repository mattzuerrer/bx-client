import { Inject, Injectable } from '@angular/core';
import { MatDateFormats } from '@angular/material';
import { localeToken } from '@bx-client/env';
import * as numeral from 'numeral';

import { LocaleFormat } from '../models';
import { matDatepickerDateFormatToken } from '../providers/material/tokens';
import { defaultNumberFormatToken, localeFormatToken } from '../tokens';

// tslint:disable-next-line:no-var-requires parse-decimal-number is a CommonJS module and cannot be imported using ES6 syntax
const parseDecimalNumber: any = require('parse-decimal-number');

@Injectable()
export class GlobalizeService {
    private numeral: any = numeral;

    constructor(
        @Inject(localeToken) private locale: string,
        @Inject(localeFormatToken) private localeFormat: LocaleFormat,
        @Inject(matDatepickerDateFormatToken) private matDatepickerDateFormat: MatDateFormats,
        @Inject(defaultNumberFormatToken) private defaultNumberFormat: string
    ) {
        this.numeral.locale(this.locale);
        this.numeral.register('locale', this.locale, this.localeFormat);
        this.numeral.defaultFormat(this.defaultNumberFormat);
    }

    parseNumber(number: string): number {
        return parseDecimalNumber(number, this.localeFormat.delimiters);
    }

    getLocale(): string {
        return this.locale;
    }

    getLocaleFormat(): LocaleFormat {
        return this.localeFormat;
    }

    getMatDateFormat(): MatDateFormats {
        return this.matDatepickerDateFormat;
    }

    toCurrency(number: any, customFormat?: string): string {
        return this.numeral(number).format(customFormat);
    }

    toNumber(number: any): number {
        return this.numeral(number).value();
    }

    getFormatFromMatDatepicker(): string {
        return this.matDatepickerDateFormat.display.dateInput.replace(new RegExp('D', 'g'), 'd').replace(new RegExp('Y', 'g'), 'y');
    }
}
