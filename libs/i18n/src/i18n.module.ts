import { ModuleWithProviders, NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { TranslateModule } from '@ngx-translate/core';

import { I18nDirectivesModule } from './directives';
import { LanguageEffects } from './language.effects';
import {
    localeFormatProviders,
    localesInitProvider,
    numberFormatProvider,
    translateInitProvider,
    translateLoaderProvider
} from './providers';
import { matDatepickerLocaleFormatProvider } from './providers/material';
import { GlobalizeService, LanguageService } from './services';

@NgModule({
    imports: [
        TranslateModule.forRoot({
            loader: translateLoaderProvider
        }),
        EffectsModule.forFeature([LanguageEffects]),
        I18nDirectivesModule
    ],
    exports: [TranslateModule, I18nDirectivesModule],
    providers: [
        localeFormatProviders,
        localesInitProvider,
        numberFormatProvider,
        translateInitProvider,
        matDatepickerLocaleFormatProvider,
        GlobalizeService,
        LanguageService
    ]
})
export class I18nRootModule {}

@NgModule({
    imports: [TranslateModule, I18nDirectivesModule],
    exports: [TranslateModule, I18nDirectivesModule]
})
export class I18nModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: I18nRootModule
        };
    }
}
