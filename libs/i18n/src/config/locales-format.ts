import { LocaleFormat } from '../models';

export const localesFormat: { [locale: string]: LocaleFormat } = {
    'de-CH': {
        delimiters: { thousands: "'", decimal: '.' },
        abbreviations: { thousand: 'k', million: 'm', billion: 'b', trillion: 't' },
        currency: { symbol: 'CHF' },
        ordinal(): string {
            return '.';
        }
    },

    'de-DE': {
        delimiters: { thousands: '.', decimal: ',' },
        abbreviations: { thousand: 'k', million: 'm', billion: 'b', trillion: 't' },
        currency: { symbol: '€' },
        ordinal(): string {
            return '.';
        }
    },

    'de-AT': {
        delimiters: { thousands: '.', decimal: ',' },
        abbreviations: { thousand: 'k', million: 'm', billion: 'b', trillion: 't' },
        currency: { symbol: '€' },
        ordinal(): string {
            return '.';
        }
    },

    'fr-CH': {
        delimiters: { thousands: "'", decimal: '.' },
        abbreviations: { thousand: 'k', million: 'm', billion: 'b', trillion: 't' },
        currency: { symbol: 'CHF' },
        ordinal(number: number): string {
            return number === 1 ? 'er' : 'e';
        }
    },

    'en-GB': {
        delimiters: { thousands: ',', decimal: '.' },
        abbreviations: { thousand: 'k', million: 'm', billion: 'b', trillion: 't' },
        currency: { symbol: '£' },
        ordinal(): string {
            return 'th';
        }
    },
    'it-CH': {
        delimiters: { thousands: "'", decimal: '.' },
        abbreviations: { thousand: 'k', million: 'm', billion: 'b', trillion: 't' },
        currency: { symbol: 'CHF' },
        ordinal(): string {
            return '.';
        }
    }
};
