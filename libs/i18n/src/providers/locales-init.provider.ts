import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import localeDeAt from '@angular/common/locales/de-AT';
import localeDeCh from '@angular/common/locales/de-CH';
import localeEnGb from '@angular/common/locales/en-GB';
import localeFrCh from '@angular/common/locales/fr-CH';
import localeItCh from '@angular/common/locales/it-CH';
import { APP_INITIALIZER } from '@angular/core';

export function provideLocalesData(): () => void {
    return () => {
        registerLocaleData(localeDe);
        registerLocaleData(localeDeCh);
        registerLocaleData(localeDeAt);
        registerLocaleData(localeFrCh);
        registerLocaleData(localeEnGb);
        registerLocaleData(localeItCh);
    };
}

export const localesInitProvider = {
    provide: APP_INITIALIZER,
    useFactory: provideLocalesData,
    multi: true
};
