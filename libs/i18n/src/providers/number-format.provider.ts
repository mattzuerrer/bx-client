import { defaultNumberFormat } from '../config';
import { defaultNumberFormatToken } from '../tokens';

export const numberFormatProvider = { provide: defaultNumberFormatToken, useValue: defaultNumberFormat };
