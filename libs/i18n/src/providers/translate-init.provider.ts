import { APP_INITIALIZER } from '@angular/core';
import { defaultShortLocaleToken, shortLocaleToken } from '@bx-client/env';
import { TranslateService } from '@ngx-translate/core';

export function initTranslateService(translateService: TranslateService, defaultLanguage: string, language: string): () => void {
    return () => {
        translateService.setDefaultLang(defaultLanguage);
        translateService.use(language);
    };
}

export const translateInitProvider = {
    provide: APP_INITIALIZER,
    useFactory: initTranslateService,
    deps: [TranslateService, defaultShortLocaleToken, shortLocaleToken],
    multi: true
};
