import { MatDateFormats } from '@angular/material';

export const matDatepickerFormats: { [locale: string]: MatDateFormats } = {
    'de-CH': {
        parse: {
            dateInput: 'DD.MM.YYYY'
        },
        display: {
            dateInput: 'DD.MM.YYYY',
            monthYearLabel: 'MMM YYYY',
            dateA11yLabel: 'LL',
            monthYearA11yLabel: 'MMMM YYYY'
        }
    },

    'de-DE': {
        parse: {
            dateInput: 'DD.MM.YYYY'
        },
        display: {
            dateInput: 'DD.MM.YYYY',
            monthYearLabel: 'MMM YYYY',
            dateA11yLabel: 'LL',
            monthYearA11yLabel: 'MMMM YYYY'
        }
    },

    'de-AT': {
        parse: {
            dateInput: 'DD.MM.YYYY'
        },
        display: {
            dateInput: 'DD.MM.YYYY',
            monthYearLabel: 'MMM YYYY',
            dateA11yLabel: 'LL',
            monthYearA11yLabel: 'MMMM YYYY'
        }
    },

    'fr-CH': {
        parse: {
            dateInput: 'DD.MM.YYYY'
        },
        display: {
            dateInput: 'DD.MM.YYYY',
            monthYearLabel: 'MMM YYYY',
            dateA11yLabel: 'LL',
            monthYearA11yLabel: 'MMMM YYYY'
        }
    },

    'en-GB': {
        parse: {
            dateInput: 'DD/MM/YYYY'
        },
        display: {
            dateInput: 'DD/MM/YYYY',
            monthYearLabel: 'MMM YYYY',
            dateA11yLabel: 'LL',
            monthYearA11yLabel: 'MMMM YYYY'
        }
    }
};
