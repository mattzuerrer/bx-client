import { MatDateFormats } from '@angular/material';

import { defaultLocaleToken, localeToken } from '../../../../env/index';

import { matDatepickerFormats } from './config/index';
import { matDatepickerDateFormatToken } from './tokens/index';

export function provideMatDatepickerLocaleDateFormat(locale: string, defaultLocale: string): MatDateFormats {
    return matDatepickerFormats[locale] || matDatepickerFormats[defaultLocale];
}

export const matDatepickerLocaleFormatProvider = {
    provide: matDatepickerDateFormatToken,
    useFactory: provideMatDatepickerLocaleDateFormat,
    deps: [localeToken, defaultLocaleToken]
};
