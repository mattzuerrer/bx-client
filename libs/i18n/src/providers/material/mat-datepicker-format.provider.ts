import { MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatDateFormats } from '@angular/material';

import { GlobalizeService } from '../../services';

export function matDateLocaleFactory(globalizeService: GlobalizeService): string {
    return globalizeService.getLocale();
}

export function matDateFormatFactory(globalizeService: GlobalizeService): MatDateFormats {
    return globalizeService.getMatDateFormat();
}

export const matDatePickerFormatProvider = [
    { provide: MAT_DATE_LOCALE, useFactory: matDateLocaleFactory, deps: [GlobalizeService] },
    { provide: MAT_DATE_FORMATS, useFactory: matDateFormatFactory, deps: [GlobalizeService] }
];
