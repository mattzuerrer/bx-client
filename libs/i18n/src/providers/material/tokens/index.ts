import { InjectionToken } from '@angular/core';

export const matDatepickerDateFormatToken = new InjectionToken('MAT_DATEPICKER_DATE_FORMAT');
