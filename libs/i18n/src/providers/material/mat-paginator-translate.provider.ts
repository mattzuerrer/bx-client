import { Injectable, OnDestroy } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Injectable()
export class MatPaginatorTranslateProvider extends MatPaginatorIntl implements OnDestroy {
    unsubscribe: Subject<void> = new Subject<void>();
    ofLabel = 'of';

    constructor(private translate: TranslateService) {
        super();

        this.translate.onLangChange.pipe(takeUntil(this.unsubscribe)).subscribe(() => {
            this.getAndInitTranslations();
        });

        this.getAndInitTranslations();
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    // tslint:disable:semicolon linter can't deal with semicolons after member function
    getRangeLabel = (page: number, pageSize: number, length: number) => {
        if (length === 0 || pageSize === 0) {
            return `0 ${this.ofLabel} ${length}`;
        }
        length = Math.max(length, 0);
        const startIndex = page * pageSize;
        const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
        return `${startIndex + 1} - ${endIndex} ${this.ofLabel} ${length}`;
    };
    // tslint:enable:semicolon

    private getAndInitTranslations(): void {
        this.translate
            .get(['mat_pagination.items_per_page', 'mat_pagination.next_page', 'mat_pagination.previous_page', 'mat_pagination.of_label'])
            .pipe(takeUntil(this.unsubscribe))
            .subscribe(translation => {
                this.itemsPerPageLabel = translation['mat_pagination.items_per_page'];
                this.nextPageLabel = translation['mat_pagination.next_page'];
                this.previousPageLabel = translation['mat_pagination.previous_page'];
                this.ofLabel = translation['mat_pagination.of_label'];
                this.changes.next();
            });
    }
}

export const matPaginatorTranslateProvider = {
    provide: MatPaginatorIntl,
    useClass: MatPaginatorTranslateProvider
};
