import { defaultLocaleToken, localeToken } from '@bx-client/env';

import { localesFormat } from '../config';
import { LocaleFormat } from '../models';
import { defaultLocaleFormatToken, localeFormatToken, supportedLocaleFormatsToken } from '../tokens';

export function provideDefaultLocaleFormat(
    supportedLocaleFormats: { [locale: string]: LocaleFormat },
    defaultLocale: string
): LocaleFormat {
    return supportedLocaleFormats[defaultLocale];
}

export function provideLocaleFormat(
    supportedLocaleFormats: { [locale: string]: LocaleFormat },
    locale: string,
    defaultLocale: string
): LocaleFormat {
    return supportedLocaleFormats[locale] || supportedLocaleFormats[defaultLocale];
}

export const localeFormatProviders = [
    { provide: supportedLocaleFormatsToken, useValue: localesFormat },
    {
        provide: defaultLocaleFormatToken,
        useFactory: provideDefaultLocaleFormat,
        deps: [supportedLocaleFormatsToken, defaultLocaleToken]
    },
    {
        provide: localeFormatToken,
        useFactory: provideLocaleFormat,
        deps: [supportedLocaleFormatsToken, localeToken, defaultLocaleToken]
    }
];
