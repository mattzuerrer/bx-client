import { HttpClient } from '@angular/common/http';
import { CdnService } from '@bx-client/core/src/environments/service/cdn.service';
import { appEnvironmentToken, version } from '@bx-client/env';
import { TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function provideLoader(http: HttpClient, cdn: CdnService, env: AppEnvironment, appVersion: string): TranslateHttpLoader {
    const suffix: string = env ? '' : `?${+new Date()}`;

    return new TranslateHttpLoader(http, `${cdn.getAssetUrl(appVersion)}/i18n/`, `.json${suffix}`);
}

export const translateLoaderProvider = {
    provide: TranslateLoader,
    useFactory: provideLoader,
    deps: [HttpClient, CdnService, appEnvironmentToken, version]
};
