import { inject, TestBed } from '@angular/core/testing';
import { StateService } from '@bx-client/ngrx';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { noop, Observable, of, throwError } from 'rxjs';

import { taxes } from './fixtures';
import { TaxApiService } from './services';
import { TaxActions } from './tax.actions';
import { TaxEffects } from './tax.effects';

const apiServiceStub = {
    getTaxes: () => noop()
};

const stateServiceStub = {};

const initialActions = [new TaxActions.LoadAction()];

describe('Tax effects', () => {
    let taxEffects: TaxEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<TaxEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                TaxEffects,
                { provide: TaxApiService, useValue: apiServiceStub },
                { provide: StateService, useValue: stateServiceStub },
                provideMockActions(() => actions)
            ]
        });

        taxEffects = TestBed.get(TaxEffects);
        metadata = getEffectsMetadata(taxEffects);
    });

    it('should have initial actions', () => {
        expect(taxEffects.initActions).toEqual(initialActions);
    });

    it(
        'should return LOAD_SUCCESS if LOAD is successful',
        inject([TaxApiService], apiService => {
            actions = cold('a', { a: new TaxActions.LoadAction() });
            spyOn(apiService, 'getTaxes').and.returnValue(of({ vats: taxes }));
            const expected = cold('b', { b: new TaxActions.LoadSuccessAction({ vats: taxes }) });
            expect(taxEffects.fetch$).toBeObservable(expected);
            expect(apiService.getTaxes).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_SUCCESS if LOAD is successful (auto)',
        inject([TaxApiService], apiService => {
            const thrownError = { status: 404 };
            actions = cold('a', { a: new TaxActions.LoadAction() });
            spyOn(apiService, 'getTaxes').and.returnValue(throwError(thrownError));
            const expected = cold('b', { b: new TaxActions.LoadFailAction(thrownError.status) });
            expect(taxEffects.fetch$).toBeObservable(expected);
            expect(apiService.getTaxes).toHaveBeenCalledTimes(1);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetch$).toEqual({ dispatch: true });
    });
});
