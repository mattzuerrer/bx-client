import { applyChanges } from '@bx-client/utils';

import { Tax } from './models';
import { TaxActions } from './tax.actions';

export interface State {
    collection: Tax[];
    loaded: boolean;
    loading: boolean;
}

const initialState: State = {
    collection: [],
    loaded: false,
    loading: false
};

export function reducer(state: State = initialState, action: TaxActions.Actions): State {
    switch (action.type) {
        case TaxActions.LOAD:
            return applyChanges(state, { loaded: false, loading: true });
        case TaxActions.LOAD_SUCCESS:
            return applyChanges(state, { collection: action.payload.vats, loaded: true, loading: false });
        case TaxActions.LOAD_FAIL:
            return applyChanges(state, { loaded: false, loading: false });
        default:
            return state;
    }
}
