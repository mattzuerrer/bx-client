import { InjectionToken } from '@angular/core';

export const taxesApiToken = new InjectionToken('TAXES_API');
