import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { featureName } from './config';
import { TaxApiService, TaxStateService } from './services';
import { TaxEffects } from './tax.effects';
import { reducer } from './tax.reducer';

@NgModule({
    imports: [StoreModule.forFeature(featureName, reducer), EffectsModule.forFeature([TaxEffects])],
    providers: [TaxApiService, TaxStateService]
})
export class TaxModule {}
