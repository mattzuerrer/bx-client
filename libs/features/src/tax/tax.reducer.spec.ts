import { taxes } from './fixtures';
import { TaxActions } from './tax.actions';

import { reducer } from './tax.reducer';

const initialState = {
    collection: [],
    loaded: false,
    loading: false
};

describe('Taxes reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should set loading on load', () => {
        const state = { collection: [], loaded: false, loading: false };
        const action = new TaxActions.LoadAction();
        expect(reducer(state, action).loading).toBeTruthy();
    });

    it('should set loading and loaded on load fail', () => {
        const state = { collection: [], loaded: false, loading: true };
        const action = new TaxActions.LoadFailAction(404);
        expect(reducer(state, action).loaded).toBeFalsy();
        expect(reducer(state, action).loading).toBeFalsy();
    });

    it('should return new state on load success', () => {
        const state = { collection: [], loaded: false, loading: true };
        const action = new TaxActions.LoadSuccessAction({ vats: taxes });
        expect(reducer(state, action)).toEqual({ collection: taxes, loaded: true, loading: false });
    });
});
