import { createFeatureSelector, createSelector } from '@ngrx/store';

import { featureName } from './config';
import { State } from './tax.reducer';

export namespace TaxSelectors {
    export const getState = createFeatureSelector<State>(featureName);

    export const getCollection = createSelector(getState, (state: State) => state.collection);

    export const getLoaded = createSelector(getState, (state: State) => state.loaded);

    export const getLoading = createSelector(getState, (state: State) => state.loading);
}
