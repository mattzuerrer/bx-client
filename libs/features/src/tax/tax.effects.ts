import { Injectable } from '@angular/core';
import { EffectsService, StateService } from '@bx-client/ngrx';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { TaxApiService } from './services';
import { TaxActions } from './tax.actions';

@Injectable()
export class TaxEffects extends EffectsService {
    @Effect()
    fetch$: Observable<any> = this.actions$.pipe(
        ofType<TaxActions.LoadAction>(TaxActions.LOAD),
        switchMap(() =>
            this.apiService
                .getTaxes()
                .pipe(
                    map(taxes => new TaxActions.LoadSuccessAction(taxes)),
                    catchError(response => of(new TaxActions.LoadFailAction(response.status)))
                )
        )
    );

    initActions: Action[] = [new TaxActions.LoadAction()];

    constructor(private actions$: Actions, private apiService: TaxApiService, protected stateService: StateService) {
        super(stateService);
    }
}
