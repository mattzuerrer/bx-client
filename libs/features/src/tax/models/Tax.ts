export class Tax {
    id: number;

    name: string;

    code: string;

    type: string;

    accountId: number;

    taxSettlementType: string;

    value: number;

    startYear: number;

    endYear: number;

    isActive: boolean;

    displayName: string;

    digit: number;

    format(): string {
        return this.code || this.displayName;
    }
}
