import { Inject, Injectable, Optional } from '@angular/core';
import { HttpService } from '@bx-client/http';
import { Observable } from 'rxjs';

import { Tax } from '../models';
import { taxesApiToken } from '../tax.tokens';

@Injectable()
export class TaxApiService {
    protected api = '';

    constructor(
        private httpService: HttpService,
        @Optional()
        @Inject(taxesApiToken)
        taxesApi: string
    ) {
        if (taxesApi) {
            this.api = taxesApi;
        } else {
            this.api = 'vats';
        }
    }

    /**
     * Get taxes.
     *
     * @returns {Observable<Tax[]>}
     */
    getTaxes(): Observable<{ vats: Tax[] }> {
        return this.httpService.get(Tax, this.api, {});
    }
}
