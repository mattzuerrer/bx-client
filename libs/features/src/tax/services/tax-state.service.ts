import { Injectable } from '@angular/core';
import { StateService } from '@bx-client/ngrx';
import { select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Tax } from '../models';
import { TaxSelectors } from '../tax.selectors';

@Injectable()
export class TaxStateService extends StateService {
    taxesLoaded$: Observable<boolean> = this.store.pipe(select(TaxSelectors.getLoaded));

    taxesLoading$: Observable<boolean> = this.store.pipe(select(TaxSelectors.getLoading));

    taxes$: Observable<Tax[]> = this.store.pipe(select(TaxSelectors.getCollection));
}
