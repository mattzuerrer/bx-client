import { Action } from '@ngrx/store';

import { Tax } from './models';

export namespace TaxActions {
    export const LOAD = '[Tax] Load';
    export const LOAD_SUCCESS = '[Tax] Load Success';
    export const LOAD_FAIL = '[Tax] Load Fail';

    export class LoadAction implements Action {
        readonly type = LOAD;
    }

    export class LoadSuccessAction implements Action {
        readonly type = LOAD_SUCCESS;

        constructor(public payload: { vats: Tax[] }) {}
    }

    export class LoadFailAction implements Action {
        readonly type = LOAD_FAIL;

        constructor(public payload: number) {}
    }

    export type Actions = LoadAction | LoadSuccessAction | LoadFailAction;
}
