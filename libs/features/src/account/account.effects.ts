import { Injectable } from '@angular/core';
import { EffectsService, StateService } from '@bx-client/ngrx';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap, switchMap } from 'rxjs/operators';

import { AccountActions } from './account.actions';
import { AccountApiService } from './services';

@Injectable()
export class AccountEffects extends EffectsService {
    @Effect()
    fetch$: Observable<any> = this.actions$.pipe(
        ofType<AccountActions.LoadAction>(AccountActions.LOAD),
        switchMap(() =>
            this.apiService
                .getAccounts()
                .pipe(
                    map(accounts => new AccountActions.LoadSuccessAction(accounts)),
                    catchError(() => of(new AccountActions.LoadFailAction()))
                )
        )
    );

    @Effect()
    fetchAccountGroups$: Observable<any> = this.actions$.pipe(
        ofType<AccountActions.LoadBalance>(AccountActions.LOAD_BALANCE),
        mergeMap(() =>
            this.apiService
                .getAccountGroups()
                .pipe(
                    map(accountGroups => new AccountActions.LoadBalanceSuccessAction(accountGroups)),
                    catchError(() => of(new AccountActions.LoadFailAction()))
                )
        )
    );

    @Effect()
    onLoadSuccess$: Observable<any> = this.actions$.pipe(
        ofType<AccountActions.LoadSuccessAction>(AccountActions.LOAD_SUCCESS),
        map(() => new AccountActions.LoadBalance())
    );

    initActions: Action[] = [new AccountActions.LoadAction()];

    constructor(private actions$: Actions, private apiService: AccountApiService, protected stateService: StateService) {
        super(stateService);
    }
}
