import { AccountActions } from './account.actions';
import { accountGroups, accounts } from './fixtures';

import { reducer } from './account.reducer';

describe('Account reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual({ collection: [], loaded: false, loadedGroups: false });
    });

    it('should return existing state on load', () => {
        const state = { collection: [], loaded: false, loadedGroups: false };
        const action = new AccountActions.LoadAction();
        expect(reducer(state, action)).toBe(state);
    });

    it('should return loaded true on load fail', () => {
        const state = { collection: [], loaded: false, loadedGroups: false };
        const action = new AccountActions.LoadFailAction();
        expect(reducer(state, action)).toEqual({ collection: [], loaded: true, loadedGroups: false });
    });

    it('should return accounts', () => {
        const state = { collection: [], loaded: false, loadedGroups: false };
        const action = new AccountActions.LoadSuccessAction(accounts);
        expect(reducer(state, action)).toEqual({
            collection: accounts,
            loaded: true,
            loadedGroups: false
        });
    });

    it('should return accounts mapped to account groups', () => {
        const state = { collection: [], loaded: false, loadedGroups: false };
        const action = new AccountActions.LoadBalanceSuccessAction(accountGroups);
        expect(reducer(state, action)).toEqual({
            collection: accounts,
            loaded: true,
            loadedGroups: true
        });
    });
});
