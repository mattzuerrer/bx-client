export interface AccountAutocompleteConfig {
    inputAccountNo?: boolean;
    inputAccountName?: boolean;
    listAccountNo?: boolean;
    listAccountName?: boolean;
    listAccountAmount?: boolean;
}
