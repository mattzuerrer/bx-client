import {
    Component,
    ElementRef,
    EventEmitter,
    forwardRef,
    HostBinding,
    HostListener,
    Input,
    Output,
    ViewChild,
    ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Account } from '@bx-client/features/account';

import { AccountAutocompleteConfig } from './AccountAutocompleteConfig';

// tslint:disable-next-line:no-var-requires escape-string-regexp is a CommonJS module and cannot be imported using ES6 syntax
const escapeStringRegexp = require('escape-string-regexp');

const accountAutocompleteValueAccessor = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => AccountAutocompleteComponent),
    multi: true
};

const UP_ARROW = 38;
const DOWN_ARROW = 40;
const ENTER = 13;
const TAB = 9;
const ESCAPE = 27;

const isNumeric = n => !isNaN(parseFloat(n)) && isFinite(n);

@Component({
    selector: 'app-account-autocomplete',
    templateUrl: 'account-autocomplete.component.html',
    styleUrls: ['account-autocomplete.component.scss'],
    providers: [accountAutocompleteValueAccessor],
    encapsulation: ViewEncapsulation.None
})
export class AccountAutocompleteComponent implements ControlValueAccessor {
    @Output() change: EventEmitter<any> = new EventEmitter<any>();

    @Input() readonly = false;

    @Input() isValid = true;

    @Input()
    @HostBinding('class.account-autocomplete-disabled')
    disabled = false;

    @Input() tabindex = 0;

    @Input() placeholder = '';

    @Input() accounts: Account[] = [];

    @Input() accountGroupsLoaded: boolean;

    @Input()
    config: AccountAutocompleteConfig = {
        inputAccountNo: true,
        inputAccountName: false,
        listAccountNo: true,
        listAccountName: true,
        listAccountAmount: true
    };

    @ViewChild('input') input: ElementRef;

    selectedAccount: Account = null;
    list: Account[] = [];
    focusedOption = 0;
    inputValue = '';
    inputFocused = false;

    private preventBlur = false;

    constructor(private element: ElementRef) {}

    // tslint:disable:no-empty
    onChange(value: any): void {}

    onTouched(): any {}
    // tslint:disable:no-empty

    @Input()
    get value(): any {
        return this.selectedAccount;
    }

    set value(value: any) {
        this.inputValue = '';
        this.selectedAccount = this.accounts.find(account => value && account.id === value.id);
        if (this.selectedAccount) {
            this.inputValue = this.getFormattedInputValue(this.selectedAccount);
        }
    }

    get isMenuVisible(): boolean {
        return (this.inputFocused || this.preventBlur) && !this.readonly;
    }

    get isAccountListVisible(): boolean {
        return this.isMenuVisible && this.accountGroupsLoaded;
    }

    get isLoadingSpinnerVisible(): boolean {
        return this.isMenuVisible && !this.accountGroupsLoaded;
    }

    writeValue(value: any): void {
        this.preventBlur = false;
        this.value = this.accounts.find(account => value && account.id === value);
    }

    registerOnTouched(fn: () => any): void {
        this.onTouched = fn;
    }

    registerOnChange(fn: (value: any) => void): void {
        this.onChange = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    /**
     * input event listener
     */
    handleKeyDown(event: KeyboardEvent): void {
        if (!this.disabled) {
            switch (event.keyCode) {
                case ESCAPE:
                    event.stopPropagation();
                    event.preventDefault();
                    if (this.inputValue) {
                        this.onClear();
                    }
                    break;
                case TAB:
                    if (this.isMenuVisible && this.list.length && this.inputValue) {
                        this.selectOption(event, this.focusedOption);
                    } else {
                        this.onClear(true);
                    }
                    this.handleMouseLeave();
                    this.blur();
                    break;
                case ENTER:
                    if (this.isMenuVisible && this.list.length) {
                        this.selectOption(event, this.focusedOption);
                        this.handleFocus(null);
                    }
                    this.handleMouseLeave();
                    this.blur();
                    break;
                case DOWN_ARROW:
                    event.preventDefault();
                    event.stopPropagation();
                    if (this.isMenuVisible && this.list.length) {
                        this.focusedOption =
                            this.focusedOption === this.list.length - 1 ? 0 : Math.min(this.focusedOption + 1, this.list.length - 1);
                        this.updateScroll();
                    }
                    break;
                case UP_ARROW:
                    event.preventDefault();
                    event.stopPropagation();
                    if (this.isMenuVisible && this.list.length) {
                        this.focusedOption = this.focusedOption === 0 ? this.list.length - 1 : Math.max(0, this.focusedOption - 1);
                        this.updateScroll();
                    }
                    break;
            }
        }
    }

    /**
     * input event listner
     * @param event
     * @param value
     */
    handleKeyup(event: KeyboardEvent, value: string): void {
        if (!this.disabled) {
            switch (event.keyCode) {
                case ESCAPE:
                case TAB:
                case DOWN_ARROW:
                case UP_ARROW:
                    break;
                default:
                    this.updateList(value);
            }
        }
    }

    /**
     * select option
     * @param event
     * @param index of selected account
     */
    selectOption(event: Event, index: number): void {
        this.selectedAccount = this.list[index];
        this.inputValue = this.getFormattedInputValue(this.list[index]);
        this.change.emit(this.selectedAccount);
        this.onChange(this.selectedAccount.id);
    }

    /**
     * Clear selected suggestion.
     *
     * If we have an invalid input on pressing TAB we want to keep and report it to onChange() in
     * order to trigger an error state.
     */
    onClear(keepInput: boolean = false): void {
        if (!this.disabled) {
            if (!keepInput) {
                this.inputValue = '';
            }
            this.selectedAccount = null;
            this.updateList(this.inputValue);
            this.change.emit(this.selectedAccount);
            this.onChange(keepInput ? this.inputValue : null);
        }
    }

    /**
     * input focus listener
     */
    handleFocus(event: any): void {
        if (event) {
            event.target.select();
        }
        this.inputFocused = true;
        this.updateList(this.inputValue);
        this.focusedOption = 0;
    }

    /**
     * input blur listener
     */
    @HostListener('blur')
    handleBlur(): void {
        const inputValue: string = this.inputValue;
        const accountViaNumber = this.accounts.find(
            account =>
                isNumeric(this.inputValue)
                    ? inputValue && account.accountNo === inputValue
                    : inputValue && this.getFormattedInputValue(account) === inputValue
        );

        this.inputFocused = false;
        if (!this.preventBlur) {
            if (accountViaNumber) {
                this.selectOption(null, this.focusedOption);
            } else {
                this.onClear(this.inputValue !== '');
            }
        }

        this.onTouched();
    }

    /**
     * suggestion menu mouse enter listener
     */
    handleMouseEnter(): void {
        this.preventBlur = true;
    }

    /**
     * suggestion menu mouse leave listener
     */
    handleMouseLeave(): void {
        this.preventBlur = false;
    }

    /**
     * Set focus to the input element.
     */
    focus(): void {
        setTimeout(() => this.input.nativeElement.focus(), 0);
    }

    private blur(): void {
        setTimeout(() => this.input.nativeElement.blur(), 0);
    }

    /**
     * update scroll of suggestion menu
     */
    private updateScroll(): void {
        if (this.focusedOption < 0) {
            return;
        }
        const menuContainer = this.element.nativeElement.querySelector('.account-autocomplete-menu');
        if (!menuContainer) {
            return;
        }

        const choices = menuContainer.querySelectorAll('.account-option');
        if (choices.length < 1) {
            return;
        }

        const highlighted: any = choices[this.focusedOption];
        if (!highlighted) {
            return;
        }

        const top: number = highlighted.offsetTop + highlighted.clientHeight - menuContainer.scrollTop;
        const height: number = menuContainer.offsetHeight;

        if (top > height) {
            menuContainer.scrollTop += top - height;
        } else if (top < highlighted.clientHeight) {
            menuContainer.scrollTop -= highlighted.clientHeight - top;
        }
    }

    /**
     * Update suggestion to filter the query
     */
    private updateList(query: string): void {
        this.list = this.accounts.filter(this.filterAccounts(query));
        if (this.list.length && this.list[0].accountNo !== query) {
            this.selectedAccount = null;
        }
    }

    /**
     * Filter accounts by given query
     *
     * @param query
     * @returns {(account:Account)=>boolean}
     */
    private filterAccounts(query: string): (account: Account) => boolean {
        return account =>
            isNumeric(query)
                ? account.parentAccountGroups.indexOf(query) > -1 || account.accountNo.indexOf(query) === 0
                : new RegExp(escapeStringRegexp(query), 'ig').test(account.accountNo + ' - ' + account.name);
    }

    /**
     * generate input string considering config
     *
     * @param account
     */
    private getFormattedInputValue(account: Account): string {
        let inputValue = '';
        if (this.config.inputAccountNo) {
            inputValue += account.accountNo;
        }
        if (this.config.inputAccountName && this.config.inputAccountNo) {
            inputValue += ' - ' + account.name;
        } else if (this.config.inputAccountName) {
            inputValue += account.name;
        }
        return inputValue;
    }
}
