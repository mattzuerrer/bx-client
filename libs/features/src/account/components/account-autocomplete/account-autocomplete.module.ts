import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';

import { AccountAutocompleteComponent } from './account-autocomplete.component';
import { AccountAutocompleteHighlightPipe } from './account-autocomplete.pipe';

@NgModule({
    declarations: [AccountAutocompleteComponent, AccountAutocompleteHighlightPipe],
    imports: [CommonModule, TranslateModule, FormsModule, MatProgressSpinnerModule],
    exports: [AccountAutocompleteComponent]
})
export class AccountAutocompleteModule {}
