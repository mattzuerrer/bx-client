import { FormControl } from '@angular/forms';
import { isNumber, isString } from 'util';

export class AccountValidators {
    /**
     * Validates whether autocomplete input represents an account.
     *
     * @returns {(control:FormControl)=>{[p: string]: any}}
     */
    static isAccount(control: FormControl): { [key: string]: any } {
        // a number is a valid account id, a string is an invalid input. null is no account
        // provided and valid as well.
        if (isNumber(control.value)) {
            return null;
        } else if (isString(control.value)) {
            return { isAccount: true };
        }
        return null;
    }
}
