import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material';
import { TranslateMockPipe } from '@bx-client/i18n';

import { HighlightMockPipe } from '../../mocks';

import { AccountAutocompleteComponent } from './account-autocomplete.component';

describe('Debit dredit autocomplete', () => {
    let component: AccountAutocompleteComponent;
    let fixture: ComponentFixture<AccountAutocompleteComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [AccountAutocompleteComponent, HighlightMockPipe, TranslateMockPipe],
            imports: [MatProgressSpinnerModule, FormsModule]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(AccountAutocompleteComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create AccountAutocompleteComponent', () => {
        expect(component).toBeTruthy();
    });
});
