import { NgModule } from '@angular/core';

import { AccountAutocompleteModule } from './account-autocomplete';

export * from './account-autocomplete';

@NgModule({
    imports: [AccountAutocompleteModule],
    exports: [AccountAutocompleteModule]
})
export class ComponentsModule {}
