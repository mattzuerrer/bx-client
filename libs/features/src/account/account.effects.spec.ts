import { inject, TestBed } from '@angular/core/testing';
import { StateService } from '@bx-client/ngrx';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { noop, Observable, of, throwError } from 'rxjs';

import { AccountActions } from './account.actions';
import { AccountEffects } from './account.effects';
import { accounts } from './fixtures';
import { AccountApiService } from './services';

const apiServiceStub = {
    getAccounts: () => noop(),
    getAccountGroups: () => noop()
};

const stateServiceStub = {};

const initialActions = [new AccountActions.LoadAction()];

describe('Account effects', () => {
    let accountEffects: AccountEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<AccountEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                AccountEffects,
                { provide: AccountApiService, useValue: apiServiceStub },
                { provide: StateService, useValue: stateServiceStub },
                provideMockActions(() => actions)
            ]
        });

        accountEffects = TestBed.get(AccountEffects);
        metadata = getEffectsMetadata(accountEffects);
    });

    it('should have initial actions', () => {
        expect(accountEffects.initActions).toEqual(initialActions);
    });

    it(
        'should return LOAD_SUCCESS if LOAD is successful',
        inject([AccountApiService], apiService => {
            actions = cold('a', { a: new AccountActions.LoadAction() });
            spyOn(apiService, 'getAccounts').and.returnValue(of(accounts));
            const expected = cold('b', { b: new AccountActions.LoadSuccessAction(accounts) });
            expect(accountEffects.fetch$).toBeObservable(expected);
            expect(apiService.getAccounts).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_FAIL if LOAD fails',
        inject([AccountApiService], apiService => {
            actions = cold('a', { a: new AccountActions.LoadAction() });
            spyOn(apiService, 'getAccounts').and.returnValue(throwError(null));
            const expected = cold('b', { b: new AccountActions.LoadFailAction() });
            expect(accountEffects.fetch$).toBeObservable(expected);
            expect(apiService.getAccounts).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_BALANCE on LOAD_SUCCESS',
        inject([StateService], stateService => {
            actions = cold('a', { a: new AccountActions.LoadSuccessAction(accounts) });
            const expected = cold('b', { b: new AccountActions.LoadBalance() });
            expect(accountEffects.onLoadSuccess$).toBeObservable(expected);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetch$).toEqual({ dispatch: true });
        expect(metadata.fetchAccountGroups$).toEqual({ dispatch: true });
    });
});
