import { Action } from '@ngrx/store';

import { Account, AccountGroup } from './models';

export namespace AccountActions {
    export const LOAD = '[Accounts] Load';
    export const LOAD_SUCCESS = '[Accounts] Load Success';
    export const LOAD_BALANCE = '[Accounts] Load Balance';
    export const LOAD_BALANCE_SUCCESS = '[Accounts] Load Balance Success';
    export const LOAD_FAIL = '[Accounts] Load Fail';

    export class LoadAction implements Action {
        readonly type = LOAD;
    }

    export class LoadSuccessAction implements Action {
        readonly type = LOAD_SUCCESS;

        constructor(public payload: Account[]) {}
    }

    export class LoadBalance implements Action {
        readonly type = LOAD_BALANCE;
    }

    export class LoadBalanceSuccessAction implements Action {
        readonly type = LOAD_BALANCE_SUCCESS;

        constructor(public payload: AccountGroup[]) {}
    }

    export class LoadFailAction implements Action {
        readonly type = LOAD_FAIL;
    }

    export type Actions = LoadAction | LoadSuccessAction | LoadFailAction | LoadBalance | LoadBalanceSuccessAction;
}
