import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AccountEffects } from './account.effects';
import { reducer } from './account.reducer';
import { ComponentsModule } from './components';
import { featureName } from './config';
import { AccountApiService, AccountStateService } from './services';

@NgModule({
    imports: [ComponentsModule, StoreModule.forFeature(featureName, reducer), EffectsModule.forFeature([AccountEffects])],
    exports: [ComponentsModule],
    providers: [AccountApiService, AccountStateService]
})
export class AccountModule {}
