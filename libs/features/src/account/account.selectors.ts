import { createFeatureSelector, createSelector } from '@ngrx/store';

import { State } from './account.reducer';
import { featureName } from './config';

export namespace AccountSelectors {
    export const getState = createFeatureSelector<State>(featureName);

    export const getCollection = createSelector(getState, (state: State) => state.collection);

    export const getLoaded = createSelector(getState, (state: State) => state.loaded);

    export const getLoadedGroups = createSelector(getState, (state: State) => state.loadedGroups);
}
