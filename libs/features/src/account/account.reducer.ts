import { isActive, uniqueId } from '@bx-client/utils';

import { AccountActions } from './account.actions';
import { Account, AccountGroup } from './models';

export interface State {
    collection: Account[];
    loaded: boolean;
    loadedGroups: boolean;
}

const initialState: State = {
    collection: [],
    loaded: false,
    loadedGroups: false
};

const reduceAccountGroup = (accounts: Account[], accountGroup: AccountGroup): Account[] =>
    accounts.concat(flattenAccountGroup(accountGroup));

const flattenAccountGroup = (accountGroup: AccountGroup): Account[] =>
    accountGroup.accountGroups.reduce(reduceAccountGroup, accountGroup.accounts).map(account =>
        Object.assign(new Account(), account, {
            parentAccountGroups: [...account.parentAccountGroups, accountGroup.accountNo]
        })
    );

const pluckAccounts = (accountGroups: AccountGroup[]): Account[] =>
    accountGroups
        .filter(isActive)
        .reduce(reduceAccountGroup, [])
        .filter(uniqueId);

const sortAccounts = (a: Account, b: Account): number => (parseInt(a.accountNo, 10) > parseInt(b.accountNo, 10) ? 1 : -1);

export function reducer(state: State = initialState, action: AccountActions.Actions): State {
    switch (action.type) {
        case AccountActions.LOAD_SUCCESS:
            return { collection: action.payload, loaded: true, loadedGroups: state.loadedGroups };
        case AccountActions.LOAD_BALANCE_SUCCESS:
            return {
                collection: pluckAccounts(action.payload)
                    .slice()
                    .sort(sortAccounts),
                loaded: true,
                loadedGroups: true
            };
        case AccountActions.LOAD_FAIL:
            return { ...state, loaded: true };
        default:
            return state;
    }
}
