import { Injectable } from '@angular/core';
import { HttpService } from '@bx-client/http';
import { Observable } from 'rxjs';

import { Account, AccountGroup } from '../models';

@Injectable()
export class AccountApiService {
    constructor(private httpService: HttpService) {}

    /**
     * Get accounts.
     *
     * @returns {Observable<Account[]>}
     */
    getAccounts(): Observable<Account[]> {
        return this.httpService.get(Account, 'accounts');
    }

    /**
     * Get account groups.
     *
     * @returns {Observable<AccountGroup[]>}
     */
    getAccountGroups(): Observable<AccountGroup[]> {
        return this.httpService.get(AccountGroup, 'account_groups?embed=account,balance');
    }
}
