import { Injectable } from '@angular/core';
import { StateService } from '@bx-client/ngrx';
import { select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AccountSelectors } from '../account.selectors';
import { Account } from '../models';

@Injectable()
export class AccountStateService extends StateService {
    accountGroupsLoaded$: Observable<boolean> = this.store.pipe(select(AccountSelectors.getLoadedGroups));

    accounts$: Observable<Account[]> = this.store.pipe(select(AccountSelectors.getCollection));

    accountsLoaded$: Observable<boolean> = this.store.pipe(select(AccountSelectors.getLoaded));
}
