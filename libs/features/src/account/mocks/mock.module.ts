import { NgModule } from '@angular/core';

import { AccountAutocompleteMockComponent } from './account-autocomplete.mock.component';
import { HighlightMockPipe } from './highlight.mock.pipe';

@NgModule({
    declarations: [AccountAutocompleteMockComponent, HighlightMockPipe],
    exports: [AccountAutocompleteMockComponent, HighlightMockPipe]
})
export class AccountMockModule {}
