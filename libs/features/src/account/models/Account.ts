import { Currency } from '@bx-client/features/currency';
import { serializeType } from '@bx-client/utils';
import { Type } from 'class-transformer';

export class Account {
    id: number;

    accountGroupId: number;

    accountNo: string;

    internalKey: string;

    balanceAmount: number;

    isActive: boolean;

    isLocked: boolean;

    name: string;

    taxId: number;

    @Type(serializeType(Currency))
    currency: Currency;

    currencyId: number;

    baseCurrencyId: number;

    type: string;

    parentAccountGroups: string[] = [];

    get isDifferentCurrency(): boolean {
        return this.baseCurrencyId !== this.currencyId;
    }
}
