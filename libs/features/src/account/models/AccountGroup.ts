import { serializeType } from '@bx-client/utils';
import { Type } from 'class-transformer';

import { Account } from './Account';

export class AccountGroup {
    id: number;

    name: string;

    isLocked: boolean;

    isActive: boolean;

    accountNo: string;

    @Type(serializeType(Account))
    accounts: Account[] = [];

    @Type(serializeType(AccountGroup))
    accountGroups: AccountGroup[] = [];
}
