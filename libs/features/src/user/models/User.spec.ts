import { User } from '@bx-client/features/user';

describe('User model', () => {
    it('should return user fullname on format()', () => {
        const user = new User();
        user.firstname = 'James';
        user.lastname = 'Sildner';

        expect(user.format()).toBe('James Sildner');
    });
});
