/**
 * Formattable data item. Formats the item for display to not rely on the name property.
 */
interface Formattable {
    /**
     * Format the item display
     */
    format(): string;
}

export class User implements Formattable {
    id: number;

    nickname: string;

    firstname: string;

    lastname: string;

    email: string;

    phoneFixed: string;

    phoneMobile: string;

    color: string;

    isSuperadmin: string;

    format(): string {
        return `${this.firstname || ''} ${this.lastname || ''}`;
    }
}
