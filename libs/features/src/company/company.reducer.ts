import { PayloadAction } from '@bx-client/ngrx';
import { applyChanges } from '@bx-client/utils';

import { CompanyActions } from './company.actions';
import { Company } from './models';

export interface State {
    company: Company;
    loaded: boolean;
    loading: boolean;
}

const initialState: State = {
    company: Object.assign(new Company(), { currencyId: 1 }),
    loaded: false,
    loading: false
};

export function reducer(state: State = initialState, action: PayloadAction): State {
    switch (action.type) {
        case CompanyActions.LOAD:
            return applyChanges(state, { loading: true });
        case CompanyActions.LOAD_SUCCESS:
            return applyChanges(state, { company: action.payload, loaded: true, loading: false });
        case CompanyActions.LOAD_FAIL:
            return applyChanges(state, { loaded: false, loading: false });
        default:
            return state;
    }
}
