import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { CompanyEffects } from './company.effects';
import { reducer } from './company.reducer';
import { featureName } from './config';
import { CompanyApiService } from './services';

@NgModule({
    imports: [StoreModule.forFeature(featureName, reducer), EffectsModule.forFeature([CompanyEffects])],
    providers: [CompanyApiService]
})
export class CompanyModule {}
