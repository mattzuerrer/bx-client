import { createFeatureSelector, createSelector } from '@ngrx/store';

import { State } from './company.reducer';
import { featureName } from './config';

export namespace CompanySelectors {
    export const getState = createFeatureSelector<State>(featureName);

    export const getLoading = createSelector(getState, (state: State) => state.loading);

    export const getLoaded = createSelector(getState, (state: State) => state.loaded);

    export const getCompany = createSelector(getState, (state: State) => state.company);
}
