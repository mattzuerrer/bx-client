import { inject, TestBed } from '@angular/core/testing';
import { StateService } from '@bx-client/ngrx';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { noop, Observable, of, throwError } from 'rxjs';

import { CompanyActions } from './company.actions';
import { CompanyEffects } from './company.effects';
import { company as companyFixture } from './fixtures/company.fixtures';
import { Company } from './models';
import { CompanyApiService } from './services';

const apiServiceStub = {
    getCompany: () => noop()
};

const stateServiceStub = {};

const initialActions = [new CompanyActions.LoadAction()];

describe('Company effect', () => {
    let companyEffects: CompanyEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<CompanyEffects>;

    const company = Object.assign(new Company(), companyFixture);

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                CompanyEffects,
                { provide: CompanyApiService, useValue: apiServiceStub },
                { provide: StateService, useValue: stateServiceStub },
                provideMockActions(() => actions)
            ]
        });

        companyEffects = TestBed.get(CompanyEffects);
        metadata = getEffectsMetadata(companyEffects);
    });

    it('should have initial actions', () => {
        expect(companyEffects.initActions).toEqual(initialActions);
    });

    it(
        'should return LOAD_SUCCESS on LOAD if fetch$ is successful',
        inject([CompanyApiService], companyApiService => {
            actions = cold('a', { a: new CompanyActions.LoadAction() });
            spyOn(companyApiService, 'getCompany').and.returnValue(of(company));
            const expected = cold('b', { b: new CompanyActions.LoadSuccessAction(company) });
            expect(companyEffects.fetch$).toBeObservable(expected);
            expect(companyApiService.getCompany).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_FAIL on LOAD if fetch$ fails',
        inject([CompanyApiService], companyApiService => {
            actions = cold('a', { a: new CompanyActions.LoadAction() });
            spyOn(companyApiService, 'getCompany').and.returnValue(throwError(null));
            const expected = cold('b', { b: new CompanyActions.LoadFailAction() });
            expect(companyEffects.fetch$).toBeObservable(expected);
            expect(companyApiService.getCompany).toHaveBeenCalledTimes(1);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetch$).toEqual({ dispatch: true });
    });
});
