import { Injectable } from '@angular/core';
import { EffectsService, StateService } from '@bx-client/ngrx';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { CompanyActions } from './company.actions';
import { CompanyApiService } from './services';

@Injectable()
export class CompanyEffects extends EffectsService {
    @Effect()
    fetch$: Observable<any> = this.actions$.pipe(
        ofType<CompanyActions.LoadAction>(CompanyActions.LOAD),
        switchMap(() =>
            this.companyApiService
                .getCompany()
                .pipe(
                    map(company => new CompanyActions.LoadSuccessAction(company)),
                    catchError(() => of(new CompanyActions.LoadFailAction()))
                )
        )
    );

    initActions: Action[] = [new CompanyActions.LoadAction()];

    constructor(private actions$: Actions, private companyApiService: CompanyApiService, protected stateService: StateService) {
        super(stateService);
    }
}
