import { plainToClass } from 'class-transformer';

import { Company } from '../models';

export const company = plainToClass(Company, {
    name: 'Mock company',
    currencyId: 1,
    countryId: 1,
    companyShortName: 'abc123'
});
