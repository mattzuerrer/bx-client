import { CompanyActions } from './company.actions';
import * as companyReducerFixtures from './fixtures';
import { Company } from './models';

import { reducer } from './company.reducer';

const initialState = {
    company: Object.assign(new Company(), { currencyId: 1 }),
    loaded: false,
    loading: false
};

const loadedState = {
    company: companyReducerFixtures.company,
    loaded: true,
    loading: false
};

describe('Company reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return new state on load', () => {
        const state = initialState;
        const action = new CompanyActions.LoadAction();
        expect(reducer(state, action).loading).toBe(true);
    });

    it('should return new state on load success', () => {
        const state = initialState;
        const action = new CompanyActions.LoadSuccessAction(companyReducerFixtures.company);
        expect(reducer(state, action)).toEqual(loadedState);
    });

    it('should return loaded false and loading false on load fail', () => {
        const state = initialState;
        const action = new CompanyActions.LoadFailAction();
        expect(reducer(state, action).loaded).toBe(false);
        expect(reducer(state, action).loading).toBe(false);
    });
});
