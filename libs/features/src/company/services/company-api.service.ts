import { Injectable } from '@angular/core';
import { HttpService } from '@bx-client/http';
import { Observable } from 'rxjs';

import { Company } from '../models';

@Injectable()
export class CompanyApiService {
    constructor(private httpService: HttpService) {}

    /**
     * Get company profile.
     *
     * @returns {Observable<Company>}
     */
    getCompany(): Observable<Company> {
        return this.httpService.get(Company, 'company');
    }
}
