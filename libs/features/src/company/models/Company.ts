export class Company {
    name: string;

    currencyId: number;

    countryId: number;

    companyShortName: string;
}
