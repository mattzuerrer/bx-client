import { Action } from '@ngrx/store';

import { Company } from './models';

export namespace CompanyActions {
    export const LOAD = '[Company] Load';
    export const LOAD_SUCCESS = '[Company] Load Success';
    export const LOAD_FAIL = '[Company] Load Fail';

    export class LoadAction implements Action {
        readonly type = LOAD;
    }

    export class LoadSuccessAction implements Action {
        readonly type = LOAD_SUCCESS;

        constructor(public payload: Company) {}
    }

    export class LoadFailAction implements Action {
        readonly type = LOAD_FAIL;
    }

    export type Actions = LoadAction | LoadSuccessAction | LoadFailAction;
}
