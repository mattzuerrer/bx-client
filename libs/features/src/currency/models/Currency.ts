import { Exclude } from 'class-transformer';

export class Currency {
    id: number;

    name: string;

    roundFactor: number;

    exchangeRate: number;

    @Exclude()
    get text(): string {
        return this.name;
    }
}
