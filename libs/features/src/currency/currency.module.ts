import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { featureName } from './config';
import { CurrencyEffects } from './currency.effects';
import { reducer } from './currency.reducer';
import { CurrencyApiService, CurrencyStateService } from './services';

@NgModule({
    imports: [StoreModule.forFeature(featureName, reducer), EffectsModule.forFeature([CurrencyEffects])],
    providers: [CurrencyApiService, CurrencyStateService]
})
export class CurrencyModule {}
