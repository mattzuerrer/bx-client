import { createFeatureSelector, createSelector } from '@ngrx/store';

import { featureName } from './config';
import { State } from './currency.reducer';

export namespace CurrencySelectors {
    export const getState = createFeatureSelector<State>(featureName);

    export const getCollection = createSelector(getState, (state: State) => state.collection);

    export const getLoaded = createSelector(getState, (state: State) => state.loaded);
}
