import { inject, TestBed } from '@angular/core/testing';
import { StateService } from '@bx-client/ngrx';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { noop, Observable, of, throwError } from 'rxjs';

import { CurrencyActions } from './currency.actions';
import { CurrencyEffects } from './currency.effects';
import { currencies } from './fixtures';
import { CurrencyApiService } from './services';

const apiServiceStub = {
    getCurrencies: () => noop()
};

const stateServiceStub = {};

const initialActions = [new CurrencyActions.LoadAction()];

describe('Currency effects', () => {
    let currencyEffects: CurrencyEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<CurrencyEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                CurrencyEffects,
                { provide: CurrencyApiService, useValue: apiServiceStub },
                { provide: StateService, useValue: stateServiceStub },
                provideMockActions(() => actions)
            ]
        });

        currencyEffects = TestBed.get(CurrencyEffects);
        metadata = getEffectsMetadata(currencyEffects);
    });

    it('should have initial actions', () => {
        expect(currencyEffects.initActions).toEqual(initialActions);
    });

    it(
        'should return LOAD_SUCCESS if LOAD is successful',
        inject([CurrencyApiService], apiService => {
            actions = cold('a', { a: new CurrencyActions.LoadAction() });
            spyOn(apiService, 'getCurrencies').and.returnValue(of(currencies));
            const expected = cold('b', { b: new CurrencyActions.LoadSuccessAction(currencies) });
            expect(currencyEffects.fetch$).toBeObservable(expected);
            expect(apiService.getCurrencies).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_SUCCESS if LOAD is successful (auto)',
        inject([CurrencyApiService], apiService => {
            actions = cold('a', { a: new CurrencyActions.LoadAction() });
            spyOn(apiService, 'getCurrencies').and.returnValue(throwError(null));
            const expected = cold('b', { b: new CurrencyActions.LoadFailAction() });
            expect(currencyEffects.fetch$).toBeObservable(expected);
            expect(apiService.getCurrencies).toHaveBeenCalledTimes(1);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetch$).toEqual({ dispatch: true });
    });
});
