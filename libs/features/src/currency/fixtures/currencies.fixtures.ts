import { plainToClass } from 'class-transformer';

import { Currency } from '../models';

export const currencies = plainToClass(Currency, [
    { id: 1, name: 'CHF', round_factor: 0.05, exchange_rate: 1 },
    { id: 2, name: 'EUR', round_factor: 0.01, exchange_rate: 1.05 },
    { id: 3, name: 'USD', round_factor: 0.01, exchange_rate: 1.2 },
    { id: 4, name: 'GBP', round_factor: 0.01, exchange_rate: 1 },
    { id: 5, name: 'BRL', round_factor: 0.01, exchange_rate: 1 },
    { id: 6, name: 'JPY', round_factor: 0.01, exchange_rate: 1 },
    { id: 7, name: 'CNY', round_factor: 0.01, exchange_rate: 1 }
]);
