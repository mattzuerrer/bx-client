import { Action } from '@ngrx/store';

import { Currency } from './models';

export namespace CurrencyActions {
    export const LOAD = '[Currencies] Load';
    export const LOAD_SUCCESS = '[Currencies] Load Success';
    export const LOAD_FAIL = '[Currencies] Load Fail';

    export class LoadAction implements Action {
        readonly type = LOAD;
    }

    export class LoadSuccessAction implements Action {
        readonly type = LOAD_SUCCESS;

        constructor(public payload: Currency[]) {}
    }

    export class LoadFailAction implements Action {
        readonly type = LOAD_FAIL;
    }

    export type Actions = LoadAction | LoadSuccessAction | LoadFailAction;
}
