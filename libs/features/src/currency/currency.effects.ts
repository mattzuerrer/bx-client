import { Injectable } from '@angular/core';
import { EffectsService, StateService } from '@bx-client/ngrx';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { CurrencyActions } from './currency.actions';
import { CurrencyApiService } from './services';

@Injectable()
export class CurrencyEffects extends EffectsService {
    @Effect()
    fetch$: Observable<any> = this.actions$.pipe(
        ofType<CurrencyActions.LoadAction>(CurrencyActions.LOAD),
        switchMap(() =>
            this.apiService
                .getCurrencies()
                .pipe(
                    map(currencies => new CurrencyActions.LoadSuccessAction(currencies)),
                    catchError(() => of(new CurrencyActions.LoadFailAction()))
                )
        )
    );

    initActions: Action[] = [new CurrencyActions.LoadAction()];

    constructor(private actions$: Actions, private apiService: CurrencyApiService, protected stateService: StateService) {
        super(stateService);
    }
}
