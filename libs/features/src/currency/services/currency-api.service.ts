import { Injectable } from '@angular/core';
import { HttpService } from '@bx-client/http';
import { Observable } from 'rxjs';

import { Currency } from '../models';

@Injectable()
export class CurrencyApiService {
    constructor(private httpService: HttpService) {}

    /**
     * Get currencies.
     *
     * @returns {Observable<Currency[]>}
     */
    getCurrencies(): Observable<Currency[]> {
        return this.httpService.get(Currency, 'currencies?embed=exchange_rate');
    }
}
