import { Injectable } from '@angular/core';
import { StateService } from '@bx-client/ngrx';
import { select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { CurrencySelectors } from '../currency.selectors';
import { Currency } from '../models';

@Injectable()
export class CurrencyStateService extends StateService {
    currenciesLoaded$: Observable<boolean> = this.store.pipe(select(CurrencySelectors.getLoaded));

    currencies$: Observable<Currency[]> = this.store.pipe(select(CurrencySelectors.getCollection));
}
