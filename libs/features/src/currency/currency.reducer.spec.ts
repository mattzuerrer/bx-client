import { CurrencyActions } from './currency.actions';
import { currencies } from './fixtures';

import { reducer } from './currency.reducer';

describe('Currencies reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual({ collection: [], loaded: false });
    });

    it('should return existing state on load', () => {
        const state = { collection: [], loaded: false };
        const action = new CurrencyActions.LoadAction();
        expect(reducer(state, action)).toBe(state);
    });

    it('should return loaded true on load fail', () => {
        const state = { collection: [], loaded: false };
        const action = new CurrencyActions.LoadFailAction();
        expect(reducer(state, action)).toEqual({ collection: [], loaded: true });
    });

    it('should return new state on load success', () => {
        const state = { collection: [], loaded: false };
        const action = new CurrencyActions.LoadSuccessAction(currencies);
        expect(reducer(state, action)).toEqual({ collection: currencies, loaded: true });
    });
});
