import { CurrencyActions } from './currency.actions';
import { Currency } from './models';

export interface State {
    collection: Currency[];
    loaded: boolean;
}

const initialState: State = {
    collection: [],
    loaded: false
};

export function reducer(state: State = initialState, action: CurrencyActions.Actions): State {
    switch (action.type) {
        case CurrencyActions.LOAD_SUCCESS:
            return { collection: action.payload, loaded: true };
        case CurrencyActions.LOAD_FAIL:
            return { ...state, loaded: true };
        default:
            return state;
    }
}
