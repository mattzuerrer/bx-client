### Regular Button
```html
<button mat-raised-button color="primary">
    <span>Primary</span>
</button>
```

### Icon Button
```html
<button type="button" mat-icon-button
        color="primary"
        class="icon-button-delete">
    <mat-icon fontSet="fa" fontIcon="fa-times"></mat-icon>
</button>
```

### Progress Button
```html
<button mat-raised-button color="primary" data-status="progress">
    <span>Primary</span>
    <mat-progress-spinner mode="indeterminate"></mat-progress-spinner>
</button>
```

### Color properties used
- primary
- secondary or accent
- special or warn
