### Simple Menu
```html
<button type="button" tabindex="-1" mat-icon-button [matMenuTriggerFor]="simpleMenu">
    <!-- For the use of custom icons at Bexio until material guys will document how to use mat-icon with custom src -->
    <img src="assets/icons/contextmenu-icon.svg" alt="menu-trigger">
</button>
<mat-menu #simpleMenu="matMenu" [overlapTrigger]="false">
    <button mat-menu-item>
        <span>Simple Menu Item</span>
    </button>
    <button mat-menu-item disabled>
        <span>Disabled Menu Item</span>
    </button>
    <button mat-menu-item>
        <span>Simple Menu Item with a very very long text</span>
    </button>
</mat-menu>
```

### Contextual Menu
```html
<button type="button" tabindex="-1" mat-icon-button [matMenuTriggerFor]="contextualMenu">
    <!-- For the use of custom icons at Bexio until material guys will document how to use mat-icon with custom src -->
    <img src="assets/icons/contextmenu-icon.svg" alt="menu-trigger">
</button>
<mat-menu #contextualMenu="matMenu" class="context-menu-panel" [overlapTrigger]="false">
    <div class="context-menu-item">
        <div class="main-action">
            <button mat-menu-item>
                Simple contextual Menu Item
            </button>
        </div>
        <div class="sub-action">
            <button mat-icon-button>
                <mat-icon fontSet="fa" fontIcon="fa-trash"></mat-icon>
            </button>
        </div>
    </div>
    <div class="context-menu-item">
        <div class="main-action">
            <button mat-menu-item disabled>
                Disabled contextual Menu Item
            </button>
        </div>
        <div class="sub-action">
            <button mat-icon-button disabled>
                <mat-icon fontSet="fa" fontIcon="fa-trash"></mat-icon>
            </button>
        </div>
    </div>
    <div class="context-menu-item">
        <div class="main-action">
            <button mat-menu-item>
                Contextual Menu Item with a very very long text
            </button>
        </div>
        <div class="sub-action">
            <button mat-icon-button>
                <mat-icon fontSet="fa" fontIcon="fa-trash"></mat-icon>
            </button>
        </div>
    </div>
</mat-menu>
```
