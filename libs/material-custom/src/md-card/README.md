### Regular Card
```html
<mat-card></mat-card>
```

### Sections Card
```html
<mat-card>
    <mat-card-header>
        <mat-card-title><h3>Card header section</h3></mat-card-title>
        <mat-card-subtitle>Card subheader section</mat-card-subtitle>
    </mat-card-header>
    <mat-card-content>
        <p>
            Card content section
        </p>
    </mat-card-content>
    <mat-card-actions>
        <button mat-button>Card action section</button>
    </mat-card-actions>
    <mat-card-footer>
        Card Footer section
    </mat-card-footer>
</mat-card>
```
