properties([
  buildDiscarder(logRotator(numToKeepStr: '50'))
])

def runShellCommand(String command) {
    def status = sh(returnStatus: true, script: command + " > output.txt")

    if (status != 0) {
        return null
    }

    return readFile("output.txt").trim()
}

def createAndPushPackage(app_name, packageData, deployContainer) {
    def ARTIFACT_NAME = "${app_name}.tar.gz"
    def VERSION_PLACEHOLDER = "_BEXIO_FE_DEPLOYMENT_REPLACED_VERSION_"

    deployContainer.inside("--entrypoint='' --privileged --user 0") {
        sh "mkdir -p ./tar/${app_name}"
        sh "cp -r ./dist/apps/${app_name}/* ./tar/${app_name}/"
        sh "grep -rl ${VERSION_PLACEHOLDER} ./tar/${app_name} | xargs sed -i s@${VERSION_PLACEHOLDER}@${packageData.path}@g"
        sh "echo ${packageData} >> ./tar/${app_name}/REVISION"
        sh "tar czf ${ARTIFACT_NAME} --owner=0 --group=0 --exclude='.git' -C ./tar/${app_name}/ ."
        googleStorageUpload pattern: "${ARTIFACT_NAME}", bucket: "gs://bx-artifacts/frontend/${app_name}/${packageData.path}/", credentialsId: 'bexio-build'
    }
}

def getPackageGitMetaData(String hash, String branch, String tag = null) {
    def formattedBranch = branch.replaceAll("/", "-")

    Object name = formattedBranch
    Object path = formattedBranch
    if (tag != null && tag.length() > 0) {
        path = tag
    }

    return [
        branch: formattedBranch,
        hash  : hash,
        tag   : tag,
        path  : path
    ]
}

def doCreatePackageForBranch(String branchName) {
    return branch_name == "master" ||
        branch_name =~ "production" ||
        branch_name =~ "hotfix/*" ||
        branch_name =~ "preview/*" ||
        branch_name =~ "feature/*" ||
        branch_name =~ "release/*" ||
        branch_name =~ /d*\.\d*\.\d*/
}

def hasCommitsInChangeset(Object app, String changeSet) {
    if (app.includeRootPathChanges) {
        changeSet.split("\n").each { line ->
            if (-1 == line.indexOf("/")) {
                app.doBuild = true
            }
        }
    }

    if (!app.doBuild) {
        app.includePaths.each { path ->
            if (-1 < changeSet.indexOf(path)) {
                app.doBuild = true
            }
        }
    }
}

node("docker") {
    try {
        def gitInfo = checkout scm

        def packageApps = []
        def commitChangeset = sh(returnStdout: true, script: "git diff-tree --no-commit-id --name-status -r HEAD").trim()
        def globalBuild = [name: "build all", includePaths: ["libs/"], includeRootPathChanges: true, doBuild: false]
        def apps = [
            [name: "accounting_reports", includePaths: ["apps/accounting_reports/"], includeRootPath: false, doBuild: false],
            [name: "accountant_portal", includePaths: ["apps/accountant_portal/"], includeRootPath: false, doBuild: false],
            [name: "billing", includePaths: ["apps/billing/"], includeRootPath: false, doBuild: false],
            [name: "manual_entries", includePaths: ["apps/manual_entries/"], includeRootPath: false, doBuild: false]
        ]

        def environment = docker.image "eu.gcr.io/bexio-build/bx-node10-yarn-chrome:master"
        docker.withRegistry("https://eu.gcr.io", "gcr:bexio-build") {
            environment.pull()
        }

        stage("Checking which apps to build...") {
            hasCommitsInChangeset(globalBuild, commitChangeset)
            apps.each { app ->
                if (globalBuild.doBuild) {
                    packageApps.push(app)
                } else {
                    hasCommitsInChangeset(app, commitChangeset)
                    if (app.doBuild) {
                        packageApps.push(app)
                    }
                }
            }

            if (packageApps.size() < 1) {
                echo "... no apps to build.. finishing"
                return
            }
        }

        stage("Building applications") {
            def gitData = [
                branch: gitInfo.GIT_BRANCH,
                commit: runShellCommand("git rev-parse --verify --short HEAD"),
                tag   : runShellCommand("git describe --exact-match --tags")
            ]
            def gitPackageData = getPackageGitMetaData(gitData.commit, gitData.branch, gitData.tag)

            echo "USING ${gitData.commit} as tag for Docker containers"
            echo "Build following apps:"
            packageApps.each { buildApp ->
                println buildApp.name
            }

            stage("Installing dependencies") {
                environment.inside {
                    sh "rm -rf node_modules && yarn cache clean"
                    sh "yarn install"
                    sh "yarn add node-sass"
                }
            }

            stage("Running tests") {
                environment.inside {
                    sh "yarn affected:test --all --parallel --no-watch"
                    sh "yarn lint-all"
                }
            }

            stage("Packaging applications") {
                if (doCreatePackageForBranch(gitData.branch)) {
                    packageApps.each { buildApp ->
                        environment.inside {
                            sh "yarn build:${ buildApp.name } --deploy-url=_BEXIO_FE_DEPLOYMENT_REPLACED_BASE_URI_"
                        }

                        echo "Building and push '${ buildApp.name }' artifacts for '${ gitData.branch }'"
                        createAndPushPackage(buildApp.name, gitPackageData, environment)
                    }
                } else {
                    echo "Branch name pattern '${ gitData.branch }' unknown, skipping image building and package deployment."
                }
            }
        }
    } finally {
        bexio_cleanWs()
    }
}
