FROM bitnami/nginx:1.14

ARG SOURCE_DIR

COPY dist/apps/${SOURCE_DIR} /app
