import { Injectable } from '@angular/core';
import { LocalStorageConfigInterface } from 'libs/local-storage/index';

@Injectable()
export class LocalStorageConfig implements LocalStorageConfigInterface {
    appPrefix = 'accounting-reports';
    keys = {
        filter: 'filter',
        pageSize: 'page-size',
        onlyAccountsWithTransactions: 'show-only-accounts-with-transactions'
    };
}
