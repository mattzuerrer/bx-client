import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    { path: 'account_ledger', loadChildren: './account-ledger/account-ledger.module#AccountLedgerModule' },
    { path: 'journal', loadChildren: './journal/journal.module#JournalModule' },
    { path: '**', redirectTo: 'account_ledger' }
];

export const routing = RouterModule.forRoot(routes);
