import { I18nPluralPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { CommonModule as BxCommonModule } from '@bx-client/common';
import { CoreModule } from '@bx-client/core';
import { apiHttpInterceptorProvider, httpApiProviderToken } from '@bx-client/http';
import { BxLocalStorageModule, localStorageConfigToken } from '@bx-client/local-storage';
import { MessageModule } from '@bx-client/message';
import { PipesModule } from '@bx-client/pipes';

import { version } from '../config';

import { AccountLedgerModule } from './account-ledger';
import { LocalStorageConfig as localStorageConfig } from './account-ledger/services';
import { AccountLedgerHttpApiProviderService } from './account-ledger/services/account-ledger-http-api-provider.service';
import { AppComponent } from './app.component';
import { routing } from './app.routing';

@NgModule({
    imports: [
        routing,
        CoreModule.forRoot(version),
        MessageModule.forRoot(),
        PipesModule,
        AccountLedgerModule,
        BxCommonModule,
        BxLocalStorageModule.forRoot()
    ],
    declarations: [AppComponent],
    bootstrap: [AppComponent],
    providers: [
        I18nPluralPipe,
        apiHttpInterceptorProvider,
        { provide: httpApiProviderToken, useClass: AccountLedgerHttpApiProviderService },
        { provide: localStorageConfigToken, useClass: localStorageConfig }
    ]
})
export class AppModule {}
