import { PayloadAction } from '@bx-client/ngrx';

export interface State {
    startDate: Date;
    endDate: Date;
}

const initialState: State = {
    startDate: new Date('2018-01-01'),
    endDate: new Date('2018-12-31')
};

export function reducer(state: State = initialState, action: PayloadAction): State {
    switch (action.type) {
        default:
            return state;
    }
}
