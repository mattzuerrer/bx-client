import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { featureName } from './config';
import { DateRangeEffects } from './date-range.effects';
import { reducer } from './date-range.reducer';
import { DateRangeStateService } from './services';

@NgModule({
    imports: [StoreModule.forFeature(featureName, reducer), EffectsModule.forFeature([DateRangeEffects])],
    providers: [DateRangeStateService]
})
export class DateRangeModule {}
