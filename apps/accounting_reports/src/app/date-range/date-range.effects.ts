import { Injectable } from '@angular/core';
import { EffectsService } from '@bx-client/ngrx';

@Injectable()
export class DateRangeEffects extends EffectsService {}
