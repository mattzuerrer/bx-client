import { createFeatureSelector, createSelector } from '@ngrx/store';

import { featureName } from './config';
import { State } from './date-range.reducer';

export namespace DateRangeSelectors {
    export const getState = createFeatureSelector<State>(featureName);

    export const getStartDate = createSelector(getState, (state: State) => state.startDate);

    export const getEndDate = createSelector(getState, (state: State) => state.endDate);

    export const getStartYear = createSelector(getStartDate, (date: Date) => date.getFullYear());

    export const getEndYear = createSelector(getEndDate, (date: Date) => date.getFullYear());
}
