import { Injectable } from '@angular/core';
import { StateService } from '@bx-client/ngrx';
import { select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { DateRangeSelectors } from '../date-range.selectors';

@Injectable()
export class DateRangeStateService extends StateService {
    startDate$: Observable<Date> = this.store.pipe(select(DateRangeSelectors.getStartDate));

    endDate$: Observable<Date> = this.store.pipe(select(DateRangeSelectors.getEndDate));

    startYear$: Observable<number> = this.store.pipe(select(DateRangeSelectors.getStartYear));

    endYear$: Observable<number> = this.store.pipe(select(DateRangeSelectors.getEndYear));
}
