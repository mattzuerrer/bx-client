import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CommonModule as BxCommonModule } from '@bx-client/common';
import { MockComponent } from 'ng2-mock-component';

import { GlobalizeServiceMockProvider } from '../../../../libs/i18n';

import { AccountLedgerStateService } from './account-ledger/services';
import { AppComponent } from './app.component';

const RouterOutletMockComponent = MockComponent({
    selector: 'router-outlet',
    template: '<ng-content></ng-content>',
    inputs: []
});

describe('AppComponent', () => {
    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;

    beforeEach(
        async(() => {
            TestBed.configureTestingModule({
                declarations: [AppComponent, RouterOutletMockComponent],
                imports: [BxCommonModule],
                providers: [{ ...GlobalizeServiceMockProvider }, { provide: AccountLedgerStateService, useValue: {} }]
            }).compileComponents();
        })
    );

    beforeEach(() => {
        fixture = TestBed.createComponent(AppComponent);
        component = fixture.debugElement.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
