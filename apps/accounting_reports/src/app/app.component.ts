import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { GlobalizeService } from '../../../../libs/i18n';

import { AccountLedgerStateService } from './account-ledger/services';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
    loading$: Observable<boolean>;

    constructor(private accountLedgerStateService: AccountLedgerStateService, private readonly globalize: GlobalizeService) {
        moment.locale(this.globalize.getLocale());
    }

    ngOnInit(): void {
        this.loading$ = this.accountLedgerStateService.statesLoaded$.pipe(map(isLoaded => !isLoaded));
    }
}
