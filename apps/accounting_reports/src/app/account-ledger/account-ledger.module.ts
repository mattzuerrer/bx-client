import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AutocompleteModule } from '@bx-client/common';
import { CoreModule } from '@bx-client/core';
import { AccountAutocompleteModule } from '@bx-client/features/account';
import { TaxModule } from '@bx-client/features/tax';
import { matDatePickerFormatProvider, matPaginatorTranslateProvider } from '@bx-client/i18n';
import { PipesModule } from '@bx-client/pipes';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { DateRangeModule } from '../date-range';
import { EntryTypesModule } from '../entry-types';

import { routing } from './account-ledger.routes';
import { components } from './components';
import { featureName } from './config';
import { containers } from './containers';
import { AccountLedgerListDataSource } from './datasources';
import { TooltipModule } from './directives/tooltip.module';
import { effects } from './effects';
import { entryComponents } from './entry-components';
import { appGuards } from './guards';
import { MaterialModule } from './material.module';
import { reducers } from './reducers';
import { AccountingReportsHttpInterceptorServiceHttp, sharedServices } from './services';
import { LedgerAccountModule } from './services/account';

@NgModule({
    imports: [
        routing,
        CoreModule,
        LedgerAccountModule,
        AutocompleteModule,
        AccountAutocompleteModule,
        TooltipModule,
        TaxModule,
        DateRangeModule,
        EntryTypesModule,
        MaterialModule,
        StoreModule.forFeature(featureName, reducers),
        EffectsModule.forFeature(effects),
        PipesModule
    ],
    declarations: [...components, ...containers, ...entryComponents],
    providers: [
        ...appGuards,
        ...sharedServices,
        matPaginatorTranslateProvider,
        matDatePickerFormatProvider,
        AccountLedgerListDataSource,
        { provide: HTTP_INTERCEPTORS, useClass: AccountingReportsHttpInterceptorServiceHttp, multi: true }
    ],
    entryComponents: [...entryComponents]
})
export class AccountLedgerModule {}
