import { createFeatureSelector, createSelector } from '@ngrx/store';

import { featureName } from './config';
import { State } from './ledgerAccount.reducer';

export namespace LedgerAccountSelectors {
    export const getState = createFeatureSelector<State>(featureName);

    export const getCollection = createSelector(getState, (state: State) => state.collection);

    export const getGroupCollection = createSelector(getState, (state: State) => state.groupCollection);

    export const getLoaded = createSelector(getState, (state: State) => state.loaded);

    export const getLoadedGroups = createSelector(getState, (state: State) => state.loadedGroups);
}
