import { Action } from '@ngrx/store';

import { LedgerAccountGroup } from './models';

export namespace LedgerAccountActions {
    export const LOAD = '[LedgerAccounts] Load';
    export const LOAD_SUCCESS = '[LedgerAccounts] Load Success';
    export const LOAD_GROUPS = '[LedgerAccountsGroups] Load Account Groups';
    export const LOAD_GROUPS_SUCCESS = '[LedgerAccountsGroups] Load Account Groups Success';
    export const LOAD_FAIL = '[LedgerAccounts] Load Fail';
    export const LOAD_GROUPS_FAIL = '[LedgerAccountsGroups] Load Fail';

    export class LoadAction implements Action {
        readonly type = LOAD;
    }

    export class LoadSuccessAction implements Action {
        readonly type = LOAD_SUCCESS;

        constructor(public payload: any) {}
    }

    export class LoadGroupsSuccessAction implements Action {
        readonly type = LOAD_GROUPS_SUCCESS;

        constructor(public payload: LedgerAccountGroup[]) {}
    }

    export class LoadGroups implements Action {
        readonly type = LOAD_GROUPS;
    }

    export class LoadFailAction implements Action {
        readonly type = LOAD_FAIL;

        constructor(public payload: number) {}
    }

    export class LoadGroupsFailAction implements Action {
        readonly type = LOAD_GROUPS_FAIL;

        constructor(public payload: number) {}
    }

    export type Actions = LoadAction | LoadSuccessAction | LoadFailAction | LoadGroups | LoadGroupsSuccessAction | LoadGroupsFailAction;
}
