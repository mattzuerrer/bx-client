import { inject, TestBed } from '@angular/core/testing';
import { MessageActions } from '@bx-client/message';
import { StateService } from '@bx-client/ngrx';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { TranslateService } from '@ngx-translate/core';
import { cold } from 'jasmine-marbles';
import { noop, Observable, of, throwError } from 'rxjs';

import { ledgerAccounts } from './fixtures';
import { LedgerAccountActions } from './ledgerAccount.actions';
import { LedgerAccountEffects } from './ledgerAccount.effects';
import { LedgerAccountApiService } from './services';

const apiServiceStub = {
    getLedgerAccounts: () => noop(),
    getLedgerAccountGroups: () => noop()
};

const translateServiceStub = {
    instant: () => noop()
};

const stateServiceStub = {};

const initialActions = [new LedgerAccountActions.LoadAction()];

describe('Account effects', () => {
    let accountEffects: LedgerAccountEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<LedgerAccountEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                LedgerAccountEffects,
                { provide: LedgerAccountApiService, useValue: apiServiceStub },
                { provide: StateService, useValue: stateServiceStub },
                { provide: TranslateService, useValue: translateServiceStub },
                provideMockActions(() => actions)
            ]
        });

        accountEffects = TestBed.get(LedgerAccountEffects);
        metadata = getEffectsMetadata(accountEffects);
    });

    it('should have initial actions', () => {
        expect(accountEffects.initActions).toEqual(initialActions);
    });

    it(
        'should return LOAD_SUCCESS if LOAD is successful',
        inject([LedgerAccountApiService], apiService => {
            actions = cold('a', { a: new LedgerAccountActions.LoadAction() });
            spyOn(apiService, 'getLedgerAccounts').and.returnValue(of(ledgerAccounts));
            const expected = cold('b', { b: new LedgerAccountActions.LoadSuccessAction(ledgerAccounts) });
            expect(accountEffects.fetch$).toBeObservable(expected);
            expect(apiService.getLedgerAccounts).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_FAIL if LOAD fails',
        inject([LedgerAccountApiService], apiService => {
            const thrownError = { status: 404 };
            actions = cold('a', { a: new LedgerAccountActions.LoadAction() });
            spyOn(apiService, 'getLedgerAccounts').and.returnValue(throwError(thrownError));
            const expected = cold('b', { b: new LedgerAccountActions.LoadFailAction(thrownError.status) });
            expect(accountEffects.fetch$).toBeObservable(expected);
            expect(apiService.getLedgerAccounts).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_GROUPS on LOAD_SUCCESS',
        inject([StateService], stateService => {
            actions = cold('a', { a: new LedgerAccountActions.LoadSuccessAction(ledgerAccounts) });
            const expected = cold('b', { b: new LedgerAccountActions.LoadGroups() });
            expect(accountEffects.onLoadSuccess$).toBeObservable(expected);
        })
    );

    it(
        'should return LOAD_GROUP_FAIL if LOAD fails',
        inject([LedgerAccountApiService], apiService => {
            const thrownError = { status: 404 };
            actions = cold('a', { a: new LedgerAccountActions.LoadGroups() });
            spyOn(apiService, 'getLedgerAccountGroups').and.returnValue(throwError(thrownError));
            const expected = cold('b', { b: new LedgerAccountActions.LoadGroupsFailAction(thrownError.status) });
            expect(accountEffects.fetchAccountGroups$).toBeObservable(expected);
            expect(apiService.getLedgerAccountGroups).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return ERROR_MSG on LOAD_FAIL action',
        inject([TranslateService], translateService => {
            actions = cold('a', { a: new LedgerAccountActions.LoadFailAction(404) });
            spyOn(translateService, 'instant').and.returnValue('error');
            const payload = { messageKey: 'error' };
            const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
            expect(accountEffects.listLoadFail$).toBeObservable(expected);
        })
    );

    it(
        'should return ERROR_MSG on LOAD_GROUPS_FAIL action',
        inject([TranslateService], translateService => {
            actions = cold('a', { a: new LedgerAccountActions.LoadGroupsFailAction(404) });
            spyOn(translateService, 'instant').and.returnValue('error');
            const payload = { messageKey: 'error' };
            const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
            expect(accountEffects.listGroupLoadFail$).toBeObservable(expected);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetch$).toEqual({ dispatch: true });
        expect(metadata.fetchAccountGroups$).toEqual({ dispatch: true });
        expect(metadata.listLoadFail$).toEqual({ dispatch: true });
        expect(metadata.listGroupLoadFail$).toEqual({ dispatch: true });
    });
});
