import { Component, EventEmitter, forwardRef, Input, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { noop } from 'rxjs';

const autocompleteValueAccessor = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => AccountAutocompleteMockComponent),
    multi: true
};

@Component({
    selector: 'app-account-autocomplete',
    template: '<div></div>',
    providers: [autocompleteValueAccessor]
})
export class AccountAutocompleteMockComponent implements ControlValueAccessor {
    @Output() change: EventEmitter<any> = new EventEmitter<any>();

    @Input() readonly = false;

    @Input() disabled = false;

    @Input() tabindex = 0;

    @Input() placeholder = '';

    @Input() accounts: any[] = [];

    @Input() value: any;

    @Input() isValid: any;

    @Input() accountGroupsLoaded: boolean;

    @Input() config: any = {};

    onChange(): void {
        noop();
    }

    onTouched(): void {
        noop();
    }

    writeValue(value: any): void {
        this.value = value;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }
}
