import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'highlight' })
export class HighlightMockPipe implements PipeTransform {
    transform(value: any, args?: any): any {
        return value;
    }
}
