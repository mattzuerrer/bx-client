import { applyChanges } from '@bx-client/utils';

import { LedgerAccountActions } from './ledgerAccount.actions';
import { LedgerAccount, LedgerAccountGroup } from './models';

export interface State {
    collection: LedgerAccount[];
    groupCollection: LedgerAccountGroup[];
    loaded: boolean;
    loadedGroups: boolean;
}

const initialState: State = {
    collection: [],
    groupCollection: [],
    loaded: false,
    loadedGroups: false
};

const sortAccounts = (a: LedgerAccount, b: LedgerAccount): number => a.number.localeCompare(b.number);

export function reducer(state: State = initialState, action: LedgerAccountActions.Actions): State {
    switch (action.type) {
        case LedgerAccountActions.LOAD_SUCCESS:
            const entryCollection: LedgerAccount[] = action.payload.accounts.map(payload => {
                return new LedgerAccount({
                    accountGroupId: payload.accountGroupId,
                    parentAccountGroups: [payload.accountGroupId],
                    name: payload.name,
                    number: payload.number,
                    accountNo: payload.number,
                    id: payload.number
                });
            });

            return applyChanges(state, {
                collection: entryCollection.sort(sortAccounts),
                loaded: true
            });
        case LedgerAccountActions.LOAD_GROUPS_SUCCESS:
            return applyChanges(state, {
                groupCollection: action.payload,
                loadedGroups: true
            });
        case LedgerAccountActions.LOAD_FAIL:
            return { ...state, loaded: true };
        default:
            return state;
    }
}
