import { Injectable } from '@angular/core';
import { HttpService } from '@bx-client/http';
import { Observable } from 'rxjs';

import { LedgerAccount, LedgerAccountGroup } from '../models';

@Injectable()
export class LedgerAccountApiService {
    constructor(private httpService: HttpService) {}

    /**
     * Get ledger accounts.
     *
     * @returns {Observable<LedgerAccount[]>}
     */
    getLedgerAccounts(): Observable<LedgerAccount[]> {
        const limit = 500;
        const offset = 0;
        const url = `accounts?limit=${limit}&offset=${offset}`;

        return this.httpService.get(LedgerAccount, url);
    }

    /**
     * Get ledger account groups.
     *
     * @returns {Observable<LedgerAccountGroup[]>}
     */
    getLedgerAccountGroups(): Observable<LedgerAccountGroup[]> {
        return this.httpService.get(LedgerAccountGroup, 'account_groups');
    }
}
