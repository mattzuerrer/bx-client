import { Injectable } from '@angular/core';
import { StateService } from '@bx-client/ngrx';
import { select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { LedgerAccountSelectors } from '../ledgerAccount.selectors';
import { LedgerAccount, LedgerAccountGroup } from '../models';

@Injectable()
export class LedgerAccountStateService extends StateService {
    ledgerAccountGroupsLoaded$: Observable<boolean> = this.store.pipe(select(LedgerAccountSelectors.getLoadedGroups));

    ledgerAccounts$: Observable<LedgerAccount[]> = this.store.pipe(select(LedgerAccountSelectors.getCollection));

    ledgerAccountGroups$: Observable<LedgerAccountGroup[]> = this.store.pipe(select(LedgerAccountSelectors.getGroupCollection));

    ledgerAccountsLoaded$: Observable<boolean> = this.store.pipe(select(LedgerAccountSelectors.getLoaded));
}
