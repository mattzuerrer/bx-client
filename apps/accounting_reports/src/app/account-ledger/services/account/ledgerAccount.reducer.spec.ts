import { ledgerAccountGroups, ledgerAccounts } from './fixtures';
import { LedgerAccountActions } from './ledgerAccount.actions';

import { reducer } from './ledgerAccount.reducer';

const initialState = {
    collection: [],
    groupCollection: [],
    loaded: false,
    loadedGroups: false
};

describe('Account reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return existing state on load', () => {
        const action = new LedgerAccountActions.LoadAction();
        expect(reducer(initialState, action)).toBe(initialState);
    });

    it('should return loaded true on load fail', () => {
        const action = new LedgerAccountActions.LoadFailAction(404);
        expect(reducer(initialState, action)).toEqual({
            collection: [],
            groupCollection: [],
            loaded: true,
            loadedGroups: false
        });
    });

    it('should return ledgerAccounts', () => {
        const action = new LedgerAccountActions.LoadSuccessAction(ledgerAccounts);
        expect(reducer(initialState, action)).toEqual({
            collection: ledgerAccounts.accounts,
            groupCollection: [],
            loaded: true,
            loadedGroups: false
        });
    });

    it('should return ledgerAccountGroups', () => {
        const action = new LedgerAccountActions.LoadGroupsSuccessAction(ledgerAccountGroups['account-groups']);
        expect(reducer(initialState, action)).toEqual({
            collection: [],
            groupCollection: ledgerAccountGroups['account-groups'],
            loaded: false,
            loadedGroups: true
        });
    });
});
