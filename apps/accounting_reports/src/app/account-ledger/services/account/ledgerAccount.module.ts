import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { featureName } from './config';
import { LedgerAccountEffects } from './ledgerAccount.effects';
import { reducer } from './ledgerAccount.reducer';
import { LedgerAccountApiService, LedgerAccountStateService } from './services';

@NgModule({
    imports: [StoreModule.forFeature(featureName, reducer), EffectsModule.forFeature([LedgerAccountEffects])],
    providers: [LedgerAccountApiService, LedgerAccountStateService]
})
export class LedgerAccountModule {}
