export class LedgerAccount {
    accountGroupId: number;
    name: string;
    number: string;
    accountNo: string;
    id: string;
    parentAccountGroups: number[] = [];

    constructor(init?: Partial<LedgerAccount>) {
        Object.assign(this, init);
    }
}
