export class LedgerAccountGroup {
    id: number;

    name: string;

    number: string;

    parentId: number;
}
