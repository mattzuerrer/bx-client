import { plainToClass } from 'class-transformer';

import { LedgerAccount } from '../models';

export const ledgerAccounts = {
    accounts: plainToClass(LedgerAccount, [
        {
            id: '1000',
            accountGroupId: 1,
            name: 'lib.model.account.ch.1000',
            number: '1000',
            accountNo: '1000',
            parentAccountGroups: [1]
        },
        {
            id: '1020',
            accountGroupId: 3,
            name: 'Bankguthaben',
            number: '1020',
            accountNo: '1020',
            parentAccountGroups: [3]
        }
    ])
};
