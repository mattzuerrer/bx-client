import { plainToClass } from 'class-transformer';

import { LedgerAccountGroup } from '../models';

export const ledgerAccountGroups = {
    'account-groups': plainToClass(LedgerAccountGroup, [
        {
            id: 22,
            number: '24',
            name: 'Langfristiges Fremdkapital',
            parent_id: 16
        },
        {
            id: 26,
            number: '28',
            name: 'Eigenkapital ',
            parent_id: 16
        }
    ])
};
