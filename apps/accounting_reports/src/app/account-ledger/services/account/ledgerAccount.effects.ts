import { Injectable } from '@angular/core';
import { MessageActions } from '@bx-client/message';
import { EffectsService, StateService } from '@bx-client/ngrx';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap, switchMap } from 'rxjs/operators';

import { LedgerAccountActions } from './ledgerAccount.actions';
import { LedgerAccountApiService } from './services';

@Injectable()
export class LedgerAccountEffects extends EffectsService {
    @Effect()
    fetch$: Observable<any> = this.actions$.pipe(
        ofType<LedgerAccountActions.LoadAction>(LedgerAccountActions.LOAD),
        switchMap(() =>
            this.apiService
                .getLedgerAccounts()
                .pipe(
                    map(accounts => new LedgerAccountActions.LoadSuccessAction(accounts)),
                    catchError(response => of(new LedgerAccountActions.LoadFailAction(response.status)))
                )
        )
    );

    @Effect()
    fetchAccountGroups$: Observable<any> = this.actions$.pipe(
        ofType<LedgerAccountActions.LoadGroups>(LedgerAccountActions.LOAD_GROUPS),
        mergeMap(() =>
            this.apiService
                .getLedgerAccountGroups()
                .pipe(
                    map(accountGroups => new LedgerAccountActions.LoadGroupsSuccessAction(accountGroups['account-groups'])),
                    catchError(response => of(new LedgerAccountActions.LoadGroupsFailAction(response.status)))
                )
        )
    );

    @Effect()
    onLoadSuccess$: Observable<any> = this.actions$.pipe(
        ofType<LedgerAccountActions.LoadSuccessAction>(LedgerAccountActions.LOAD_SUCCESS),
        map(() => new LedgerAccountActions.LoadGroups())
    );

    @Effect()
    readonly listLoadFail$: Observable<any> = this.actions$.pipe(
        ofType<LedgerAccountActions.LoadFailAction>(LedgerAccountActions.LOAD_FAIL),
        map(
            action =>
                new MessageActions.ErrorMessage({
                    messageKey: this.translateService.instant('accounting_reports.message.error_fetch_model', {
                        httpState: action.payload,
                        modelName: this.translateService.instant('accounting_reports.message.model_accounts')
                    })
                })
        )
    );

    @Effect()
    readonly listGroupLoadFail$: Observable<any> = this.actions$.pipe(
        ofType<LedgerAccountActions.LoadFailAction>(LedgerAccountActions.LOAD_GROUPS_FAIL),
        map(
            action =>
                new MessageActions.ErrorMessage({
                    messageKey: this.translateService.instant('accounting_reports.message.error_fetch_model', {
                        httpState: action.payload,
                        modelName: this.translateService.instant('accounting_reports.message.model_account_groups')
                    })
                })
        )
    );

    initActions: Action[] = [new LedgerAccountActions.LoadAction()];

    constructor(
        private actions$: Actions,
        private apiService: LedgerAccountApiService,
        private translateService: TranslateService,
        protected stateService: StateService
    ) {
        super(stateService);
    }
}
