import { Inject, Injectable } from '@angular/core';
import { apiToken, apiVersionToken, appEnvironmentToken, csnToken, openIdToken } from '@bx-client/env';
import { BaseHttpApiProviderService } from '@bx-client/http';

const API_VERSION = '1.0.0';

@Injectable()
export class AccountLedgerHttpApiProviderService extends BaseHttpApiProviderService {
    constructor(
        @Inject(appEnvironmentToken) environment: AppEnvironment,
        @Inject(apiToken) baseApi: string,
        @Inject(apiVersionToken) apiVersion: string,
        @Inject(csnToken) csn: string,
        @Inject(openIdToken) openId: string
    ) {
        super(baseApi);
        this.apiSpecificHeaders = this.apiSpecificHeaders.set('Authorization', 'Bearer ' + openId);
        this.apiSpecificHeaders = this.apiSpecificHeaders.set('X-Accept-Version', API_VERSION);
        this.api = baseApi;
    }
}
