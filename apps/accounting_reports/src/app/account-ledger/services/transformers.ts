export const transformClientInfo = clientInfo => ({
    currency: clientInfo.currency_name,
    has_vat: clientInfo.has_vat
});
