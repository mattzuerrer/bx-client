import { formatDate } from '@angular/common';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { saveAs } from 'file-saver';
import { Observable, of, throwError } from 'rxjs';

import { DocumentType, ExportType, FileSuffix } from '../config';
import { AccountNumberRange } from '../models';

@Injectable()
export class AccountLedgerSaveFileService {
    constructor(private translateService: TranslateService) {}

    saveFile(file: Blob, exportType: ExportType, exportAllAccounts: boolean, accountNumbers: AccountNumberRange): Observable<boolean> {
        let fileSuffix: string;
        let documentType: string;

        switch (exportType) {
            case ExportType.excel:
                fileSuffix = FileSuffix.excel;
                documentType = DocumentType.excel;
                break;
            case ExportType.pdf:
                fileSuffix = FileSuffix.pdf;
                documentType = DocumentType.pdf;
                break;
            default:
                return throwError(`Type ${exportType} is not a supported export type`);
        }

        const blob = new Blob([file], { type: documentType });
        saveAs(
            blob,
            `${this.getFileName(
                exportAllAccounts,
                accountNumbers.fromAccount,
                accountNumbers.toAccount ? accountNumbers.toAccount : null
            )}.${fileSuffix}`
        );

        return of(true);
    }

    private getFileName(exportAllAccounts: boolean, fromAccount: string, toAccount: string): string {
        let fileNamePrefix: string;
        const accountLedger: string = this.translateService.instant('accounting_reports.export.account_ledger');
        const allAccounts: string = this.translateService.instant('accounting_reports.export.all_accounts');
        const dateNow = new Date();
        const formatedDate: string = formatDate(dateNow, 'dd_MM_yyyy', 'en-GB');

        if (exportAllAccounts) {
            fileNamePrefix = allAccounts;
        } else {
            fileNamePrefix = `${accountLedger}_${fromAccount}${toAccount ? '-' + toAccount : ''}`;
        }

        return `${fileNamePrefix}_${formatedDate}`;
    }
}
