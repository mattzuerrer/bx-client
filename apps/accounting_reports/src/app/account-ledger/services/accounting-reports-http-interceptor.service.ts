import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EMPTY, Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AccountingReportsHttpInterceptorServiceHttp implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            catchError(response => {
                if (response.status === 401 || response.status === 403) {
                    window.location.replace('/user/logout');
                    return EMPTY;
                }

                return throwError(response);
            })
        );
    }
}
