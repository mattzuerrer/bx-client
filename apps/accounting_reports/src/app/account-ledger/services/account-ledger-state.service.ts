import { Injectable } from '@angular/core';
import { Sort } from '@angular/material/sort';
import { StateService } from '@bx-client/ngrx';
import { identity } from '@bx-client/utils';
import { select } from '@ngrx/store';
import { combineLatest, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { ExportType } from '../config';
import {
    AccountLedgerEntry,
    AccountLedgerFilter,
    AccountNumberRange,
    BusinessYear,
    Carryover,
    ClientInfo,
    CurrencyTotal,
    DateRange,
    EntriesTotals,
    ExportOptions,
    GenerateExportStatus
} from '../models';
import {
    AccountLedgerExportSelectors,
    AccountLedgerFilterSelectors,
    AccountLedgerListPaginationSelectors,
    AccountLedgerListSelectors,
    BusinessYearsSelectors,
    ClientInfoSelectors,
    FilterMap
} from '../selectors';

import { LedgerAccountSelectors } from './account';

@Injectable()
export class AccountLedgerStateService extends StateService {
    readonly listAccounts$: Observable<string[]> = this.store.pipe(select(AccountLedgerListSelectors.getAccounts));

    readonly selectedAccountNo$: Observable<string> = this.store.pipe(select(AccountLedgerListSelectors.getSelectedAccountNo));

    readonly listEntries$: Observable<AccountLedgerEntry[]> = this.store.pipe(select(AccountLedgerListSelectors.getEntries));

    readonly listCarryover$: Observable<Carryover> = this.store.pipe(select(AccountLedgerListSelectors.getCarryover));

    readonly listTotals$: Observable<EntriesTotals> = this.store.pipe(select(AccountLedgerListSelectors.getTotals));

    readonly listCurrenciesTotals$: Observable<CurrencyTotal[]> = this.store.pipe(select(AccountLedgerListSelectors.getCurrenciesTotals));

    readonly listEntriesLoaded$: Observable<boolean> = this.store.pipe(select(AccountLedgerListSelectors.getLoaded));

    readonly listEntriesLoading$: Observable<boolean> = this.store.pipe(select(AccountLedgerListSelectors.getLoading));

    readonly listPaginationLength$: Observable<number> = this.store.pipe(select(AccountLedgerListPaginationSelectors.getLength));

    readonly listSort$: Observable<Sort> = this.store.pipe(select(AccountLedgerListSelectors.getSort));

    readonly listPaginationPageIndex$: Observable<number> = this.store.pipe(select(AccountLedgerListPaginationSelectors.getPageIndex));

    readonly listPaginationPageSize$: Observable<number> = this.store.pipe(select(AccountLedgerListPaginationSelectors.getPageSize));

    readonly listPaginationPageSizeOptions$: Observable<number[]> = this.store.pipe(
        select(AccountLedgerListPaginationSelectors.getPageSizeOptions)
    );

    readonly listPaginationOptions$: Observable<any> = this.store.pipe(select(AccountLedgerListPaginationSelectors.getPaginationOptions));

    readonly filterValues$: Observable<AccountLedgerFilter> = this.store.pipe(select(AccountLedgerFilterSelectors.getValues));

    readonly advancedFiltersUsed$: Observable<string[]> = this.store.pipe(select(AccountLedgerFilterSelectors.getAdvancedFiltersUsed));

    readonly isFilterActive$: Observable<boolean> = this.store.pipe(select(AccountLedgerFilterSelectors.isFilterActive));

    readonly filters$: Observable<FilterMap> = this.store.pipe(select(AccountLedgerFilterSelectors.getFilters));

    readonly filterAccountNumbers$: Observable<AccountNumberRange> = this.store.pipe(
        select(AccountLedgerFilterSelectors.getFilterAccountNumbers)
    );

    readonly filterDateRange$: Observable<DateRange> = this.store.pipe(select(AccountLedgerFilterSelectors.getFilterDateRange));

    readonly filterDecimalPlaces$: Observable<number> = this.store.pipe(select(AccountLedgerFilterSelectors.getFilterDecimalPlaces));

    readonly exportOptions$: Observable<ExportOptions> = this.store.pipe(select(AccountLedgerExportSelectors.getExportOptions));

    readonly exportType$: Observable<ExportType> = this.store.pipe(select(AccountLedgerExportSelectors.getExportType));

    readonly exportGenerate$: Observable<GenerateExportStatus> = this.store.pipe(select(AccountLedgerExportSelectors.getExportGenerate));

    readonly searchQuery$: Observable<string> = this.store.pipe(select(AccountLedgerFilterSelectors.getSearchQuery));

    readonly isSearchActive$: Observable<boolean> = this.store.pipe(select(AccountLedgerFilterSelectors.getIsSearchActive));

    readonly baseCurrency$: Observable<string> = this.store.pipe(select(ClientInfoSelectors.getCurrency));

    readonly clientInfo$: Observable<ClientInfo> = this.store.pipe(select(ClientInfoSelectors.getClientInfo));

    readonly clientHasVat$: Observable<boolean> = this.store.pipe(select(ClientInfoSelectors.getHasVat));

    readonly currentBusinessYear$: Observable<BusinessYear> = this.store.pipe(select(BusinessYearsSelectors.getCurrentBusinessYear));

    readonly allBusinessYears$: Observable<BusinessYear[]> = this.store.pipe(select(BusinessYearsSelectors.getAllBusinessYears));

    readonly inBusinessDateRange$: Observable<DateRange> = this.store.pipe(select(BusinessYearsSelectors.getInBusinessDateRange));

    readonly chippableActiveFilters$: Observable<string[]> = this.store.pipe(
        select(AccountLedgerFilterSelectors.getChippableActiveFilters)
    );

    readonly hasVatRecords$: Observable<boolean> = this.store.pipe(select(AccountLedgerListSelectors.getHasVatRecords));

    readonly showBalance$: Observable<boolean> = this.store.pipe(select(AccountLedgerListSelectors.getShowBalance));

    readonly showOnlyAccountsWithTransactions$: Observable<boolean> = this.store.pipe(
        select(AccountLedgerFilterSelectors.getShowOnlyAccountsWithTransactions)
    );

    readonly showAccountDescription$: Observable<boolean> = this.store.pipe(select(AccountLedgerFilterSelectors.getShowAccountDescription));

    readonly cancelRequest$: Subject<void> = new Subject<void>();

    readonly isIncludeCarryoverChecked$: Observable<boolean> = this.store.pipe(
        select(AccountLedgerFilterSelectors.getIsIncludeCarryoverChecked$)
    );

    readonly statesLoaded$: Observable<boolean> = combineLatest(
        this.store.pipe(select(LedgerAccountSelectors.getLoaded)),
        this.store.pipe(select(LedgerAccountSelectors.getLoadedGroups)),
        this.store.pipe(select(BusinessYearsSelectors.getAllBusinessYearsLoaded)),
        this.store.pipe(select(BusinessYearsSelectors.getCurrentBusinessYearLoaded))
    ).pipe(map((isLoadedStates: boolean[]) => isLoadedStates.every(identity)));

    readonly businessYearPeriods$ = this.store.pipe(select(AccountLedgerFilterSelectors.getBussinessYearPeriods));
}
