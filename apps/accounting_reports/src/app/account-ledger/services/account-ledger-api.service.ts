import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Sort } from '@angular/material/sort';
import { HttpOptions, HttpService, Paginated, setQueryParamsFromMap } from '@bx-client/http';
import { camelToSnakeCase } from '@bx-client/utils';
import * as HTTPStatus from 'http-status';
import { Observable } from 'rxjs';
import { debounceTime, repeatWhen, skipWhile, take } from 'rxjs/operators';

import { DELEAY_RETRY, MAX_RETRIES } from '../../../config';
import { AccountLedgerCollection, BusinessYear, ClientInfo, ExportOptions, GenerateExportStatus } from '../models';

import { transformClientInfo } from './transformers';

/** Mapping between differing column names of `AccountLedgerListComponent` and sort attributes of the API */
const sortIdMapping = new Map<string, string>([
    ['reference', 'reference_name'],
    ['debit', 'debit_amount'],
    ['credit', 'credit_amount'],
    ['changeRate', 'currency_exchange_rate'],
    ['vat', 'vat_name'],
    ['currency', 'foreign_currency']
]);

const RETRY_OPTIONS = {
    attempts: MAX_RETRIES,
    delay: DELEAY_RETRY,
    retryHttpStates: [HTTPStatus.ACCEPTED]
};

@Injectable()
export class AccountLedgerApiService {
    constructor(private httpService: HttpService) {}

    /**
     * Get all accounting entries or, if `accountNo` is passed, specific entries of an account.
     *
     * @param {string | null} accountNo
     * @param {Map<string, string | string[]> | null} queryParams
     * @param {string | null} search
     * @param {Sort | null} sort
     * @param {HttpOptions} options
     *
     * @returns {Observable<Paginated<AccountLedgerCollection>>}
     */
    getAccountLedgerCollection(
        accountNo: string | null,
        queryParams: Map<string, string | string[]> | null,
        search: string | null,
        sort: Sort | null,
        options: HttpOptions = {}
    ): Observable<Paginated<AccountLedgerCollection>> {
        const url = 'account_ledger_records';
        if (!options.params) {
            options.params = new HttpParams();
        }

        if (accountNo) {
            options.params = options.params.set('account-number', accountNo);
        }

        if (search) {
            options.params = options.params.set('search-term', search);
        }

        if (sort && sort.direction) {
            options.params = options.params.set('sort', sortIdMapping.get(sort.active) || camelToSnakeCase(sort.active));
            options.params = options.params.set('order', sort.direction);
        }

        if (queryParams) {
            options.params = setQueryParamsFromMap(queryParams, options.params);
        }

        return this.httpService.retryGet(AccountLedgerCollection, url, options, RETRY_OPTIONS);
    }

    /**
     * Get export generate started
     *
     * @param exportType
     * @param {ExportOptions} exportOptions
     * @param {Map<string, string | string[]>} queryParams
     * @param {Sort | null} sort
     * @param {HttpOptions} options
     *
     * @returns {Observable<GenerateExportStatus>}
     */
    getExportGenerate(
        exportType: string,
        exportOptions: ExportOptions,
        queryParams: Map<string, string | string[]>,
        sort: Sort | null,
        options: HttpOptions = {}
    ): Observable<GenerateExportStatus> {
        const url = `export/account_ledger/${exportType}/generate`;
        const checkForNewEntriesOnBackend = 0;

        if (!options.params) {
            options.params = new HttpParams();
        }

        if (exportOptions && exportOptions.paperOrientation) {
            options.params = options.params.set('paper-orientation', exportOptions.paperOrientation);
        }

        if (exportOptions && exportOptions.forcePageBreak) {
            options.params = options.params.set('force-page-break', exportOptions.forcePageBreak.toString());
        }

        if (sort && sort.direction) {
            options.params = options.params.set('sort', sortIdMapping.get(sort.active) || camelToSnakeCase(sort.active));
            options.params = options.params.set('order', sort.direction);
        }

        if (queryParams) {
            options.params = setQueryParamsFromMap(queryParams, options.params);
        }

        if (exportOptions && exportOptions.exportAllAccounts) {
            options.params = options.params.delete('account-number-from');
            options.params = options.params.delete('account-number-to');
        }

        options.params = options.params.set('load', checkForNewEntriesOnBackend.toString());

        return this.httpService.retryGet(GenerateExportStatus, url, options, RETRY_OPTIONS);
    }

    /**
     * Get (poll) status of generated export
     *
     * @param {string} fileId
     *
     * @returns {Observable<GenerateExportStatus>}
     */
    getExportGenerateStatus(fileId: string): Observable<GenerateExportStatus> {
        const url = `export/account_ledger/${fileId}/status`;

        return this.httpService
            .get(GenerateExportStatus, url)
            .pipe(repeatWhen(call => call.pipe(debounceTime(DELEAY_RETRY))), skipWhile(resp => resp.status !== 'ready'), take(1));
    }

    /**
     * Get file to download
     *
     * @param {string} fileId
     *
     * @returns {Observable<Blob>}
     */
    getGenerateExportFile(fileId: string): Observable<Blob> {
        return this.httpService.get(null, `export/account_ledger/${fileId}/download`, { responseType: 'blob' });
    }

    /**
     * Get client info
     *
     * @return {Observable<ClientInfo>}
     */
    getClientInfo(): Observable<ClientInfo> {
        return this.httpService.get(ClientInfo, 'client', {}, transformClientInfo);
    }

    /**
     * Get company's business years or
     * if date is provided, get business year which contains that date
     *
     * @param {Date} date
     * @return {Observable<any>}
     */
    getBusinessYears(date?: string): Observable<any> {
        const url = date ? `business_years?date=${date}` : 'business_years';
        return this.httpService.get(BusinessYear, url, {});
    }
}
