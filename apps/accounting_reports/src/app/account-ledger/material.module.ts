import { NgModule } from '@angular/core';
import {
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatIconModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule
} from '@angular/material';

import { MatMomentDateModule } from '@angular/material-moment-adapter';

@NgModule({
    imports: [
        MatButtonModule,
        MatCardModule,
        MatDialogModule,
        MatIconModule,
        MatTableModule,
        MatTooltipModule,
        MatPaginatorModule,
        MatExpansionModule,
        MatSelectModule,
        MatSortModule,
        MatCheckboxModule,
        MatTabsModule,
        MatProgressSpinnerModule,
        MatDatepickerModule,
        MatMomentDateModule,
        MatRadioModule,
        MatChipsModule
    ],
    exports: [
        MatButtonModule,
        MatCardModule,
        MatDialogModule,
        MatIconModule,
        MatTableModule,
        MatTooltipModule,
        MatPaginatorModule,
        MatExpansionModule,
        MatSelectModule,
        MatSortModule,
        MatCheckboxModule,
        MatTabsModule,
        MatProgressSpinnerModule,
        MatDatepickerModule,
        MatMomentDateModule,
        MatRadioModule,
        MatChipsModule
    ]
})
export class MaterialModule {}
