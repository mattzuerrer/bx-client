import { AccountLedgerComponent } from './account-ledger/account-ledger.component';

export { AccountLedgerComponent } from './account-ledger/account-ledger.component';

export const containers = [AccountLedgerComponent];
