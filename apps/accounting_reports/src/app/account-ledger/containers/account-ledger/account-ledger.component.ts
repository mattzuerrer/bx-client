import { AfterViewInit, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Sort } from '@angular/material/sort';
import { TaxStateService } from '@bx-client/features/tax';
import { PaginationEvent } from '@bx-client/http';
import { GlobalizeService } from '@bx-client/i18n';
import { BxLocalStorageService, localStorageConfigToken } from '@bx-client/local-storage';
import * as moment from 'moment';
import { Observable, of, Subject, Subscription } from 'rxjs';
import { combineLatest, map } from 'rxjs/operators';

import { DateRangeStateService } from '../../../date-range';
import { EntryTypesStateService } from '../../../entry-types';
import { AccountLedgerFilterActions, AccountLedgerListActions, AccountLedgerListPaginationActions } from '../../actions';
import { AccountLedgerListDataSource } from '../../datasources';
import { AccountLedgerFilter, AccountNumberRange, DateRange, FROM_DATE, TO_DATE } from '../../models';
import { SelectPeriod } from '../../models/Periods';
import { AccountLedgerStateService } from '../../services/';
import { LedgerAccount, LedgerAccountStateService } from '../../services/account';

@Component({
    selector: 'app-account-ledger',
    templateUrl: 'account-ledger.component.html'
})
export class AccountLedgerComponent implements OnInit, OnDestroy, AfterViewInit {
    resetFilterField$: Subject<string> = new Subject();

    listAccounts$: Observable<LedgerAccount[]>;

    chipsToShow$: Observable<string[]>;

    storedFilter$: Observable<any>;

    businessYear: DateRange = { fromDate: null, toDate: null };

    filterDateRange: DateRange = { fromDate: null, toDate: null };

    dateFormat: string;

    periods$: Observable<SelectPeriod[]>;

    // TODO Remove this when BX-9430 is done
    showOnlyAccountsWithTransactions: boolean;

    // TODO Remove this when BX-9430 is done
    firstCall = true;

    private subscriptions: Subscription[] = [];

    constructor(
        public accountLedgerListDataSource: AccountLedgerListDataSource,
        public ledgerAccountState: LedgerAccountStateService,
        public taxState: TaxStateService,
        public dateRangeState: DateRangeStateService,
        public accountLedgerState: AccountLedgerStateService,
        public entryTypesState: EntryTypesStateService,
        private localStorageService: BxLocalStorageService,
        private changeDetector: ChangeDetectorRef,
        private globalizeService: GlobalizeService,
        @Inject(localStorageConfigToken) private localStorageConfig: any
    ) {}

    ngOnInit(): void {
        this.dateFormat = this.globalizeService.getFormatFromMatDatepicker();

        this.subscriptions.push(
            this.accountLedgerState.currentBusinessYear$.subscribe(businessYear => {
                this.businessYear.fromDate = businessYear.start;
                this.businessYear.toDate = businessYear.end;
            })
        );

        this.subscriptions.push(
            this.accountLedgerState.filterDateRange$.subscribe(dateRange => {
                this.filterDateRange.fromDate = dateRange.fromDate;
                this.filterDateRange.toDate = dateRange.toDate;
            })
        );

        this.listAccounts$ = this.accountLedgerState.listAccounts$.pipe(
            combineLatest(
                this.ledgerAccountState.ledgerAccounts$,
                this.accountLedgerState.filterAccountNumbers$,
                this.accountLedgerState.showOnlyAccountsWithTransactions$,
                this.accountLedgerState.showAccountDescription$,
                this.getAccountsToShow // project ("mapper") function
            )
        );

        // TODO Remove this when BX-9430 is done
        this.subscriptions.push(
            this.accountLedgerState.listAccounts$.subscribe(accounts => {
                if (accounts.length && this.showOnlyAccountsWithTransactions && this.firstCall) {
                    this.accountLedgerState.dispatch(new AccountLedgerListActions.LoadAction(accounts[0]));
                    this.firstCall = false;
                }
            })
        );

        // TODO Remove this when BX-9430 is done
        this.subscriptions.push(
            this.accountLedgerState.showOnlyAccountsWithTransactions$.subscribe(result => (this.showOnlyAccountsWithTransactions = result))
        );

        this.chipsToShow$ = this.accountLedgerState.chippableActiveFilters$.pipe(
            map(fields =>
                fields.filter(field => {
                    switch (field) {
                        case FROM_DATE:
                            return !moment(this.businessYear.fromDate).isSame(this.filterDateRange.fromDate, 'day');
                        case TO_DATE:
                            return !moment(this.businessYear.toDate).isSame(this.filterDateRange.toDate, 'day');
                        default:
                            return true;
                    }
                })
            )
        );

        this.periods$ = this.accountLedgerState.businessYearPeriods$;

        this.storedFilter$ = of(
            Object.assign({}, this.localStorageService.get(this.localStorageConfig.keys.filter), {
                showOnlyAccountsWithTransactions: this.localStorageService.get(this.localStorageConfig.keys.onlyAccountsWithTransactions)
            })
        );
    }

    ngAfterViewInit(): void {
        this.changeDetector.detectChanges();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    onSort(sortEvent: Sort): void {
        this.accountLedgerState.dispatch(new AccountLedgerListActions.SortAction(sortEvent));
    }

    onPagination(paginationEvent: PaginationEvent): void {
        this.accountLedgerState.dispatch(new AccountLedgerListPaginationActions.ChangeAction(paginationEvent));
    }

    onApplyFilter(values: AccountLedgerFilter): void {
        if (values.includeCarryover) {
            this.accountLedgerState.dispatch(new AccountLedgerListActions.ResetSortAction());
        }

        this.accountLedgerState.dispatch(new AccountLedgerFilterActions.FilterAction(values));
    }

    onResetFilter(): void {
        this.accountLedgerState.dispatch(new AccountLedgerFilterActions.ResetFilterAction());
        // TODO Remove this when BX-9430 is done
        this.firstCall = true;
    }

    onSelectAccount(accountNo: string): void {
        this.accountLedgerState.dispatch(new AccountLedgerListActions.SelectAccountAction(accountNo));
    }

    onInstantFilter(payload: any): void {
        if (payload.hasOwnProperty('showOnlyAccountsWithTransactions')) {
            this.localStorageService.set(
                this.localStorageConfig.keys.onlyAccountsWithTransactions,
                payload.showOnlyAccountsWithTransactions
            );
        }

        if (payload.hasOwnProperty('includeCarryover') && payload.includeCarryover) {
            this.accountLedgerState.dispatch(new AccountLedgerListActions.ResetSortAction());
        }

        this.accountLedgerState.dispatch(new AccountLedgerFilterActions.InstantFilterAction(payload));
    }

    onSearch(searchString: string): void {
        this.accountLedgerState.dispatch(new AccountLedgerFilterActions.SearchAction(searchString));
    }

    onCancelSingleFilter(field: string): void {
        let valueToSet;
        switch (field) {
            case FROM_DATE:
                valueToSet = this.businessYear.fromDate;
                break;
            case TO_DATE:
                valueToSet = this.businessYear.toDate;
                break;
            default:
                valueToSet = null;
        }
        // TODO Remove this when BX-9430 is done
        this.firstCall = true;
        this.resetFilterField$.next(field);
        this.accountLedgerState.dispatch(new AccountLedgerFilterActions.CancelSingleFilterAction({ field, valueToSet }));
    }

    private getAccountsToShow(
        listAccounts: string[],
        allAccounts: LedgerAccount[],
        filterAccountNos: AccountNumberRange,
        onlyAccWithTransactions: boolean
    ): LedgerAccount[] {
        if (onlyAccWithTransactions) {
            return allAccounts.filter(account => listAccounts.includes(account.number));
        }

        if (filterAccountNos.fromAccount && !filterAccountNos.toAccount) {
            // If only `fromAccount` is set it returns this single account only
            return allAccounts.filter(account => account.number === filterAccountNos.fromAccount);
        } else if (filterAccountNos.fromAccount && filterAccountNos.toAccount) {
            const from = allAccounts.findIndex(account => account.number === filterAccountNos.fromAccount);
            const to = allAccounts.findIndex(account => account.number === filterAccountNos.toAccount);

            if (from > -1 && to > -1) {
                return allAccounts.slice(from, to + 1); // +1 because 'to' should be included
            }
        }

        return [];
    }
}
