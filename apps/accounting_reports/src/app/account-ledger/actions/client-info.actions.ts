import { Action } from '@ngrx/store';

import { ClientInfo } from '../models';

export namespace ClientInfoActions {
    export const LOAD = '[ClientInfo] Load';
    export const LOAD_SUCCESS = '[ClientInfo] Load Success';
    export const LOAD_FAIL = '[ClientInfo] Load Fail';

    export class LoadAction implements Action {
        readonly type = LOAD;
    }

    export class LoadSuccessAction implements Action {
        readonly type = LOAD_SUCCESS;

        constructor(public payload: ClientInfo) {}
    }

    export class LoadFailAction implements Action {
        readonly type = LOAD_FAIL;

        constructor(public payload: number) {}
    }

    export type Actions = LoadAction | LoadSuccessAction | LoadFailAction;
}
