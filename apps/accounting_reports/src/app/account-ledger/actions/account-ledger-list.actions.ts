import { Sort } from '@angular/material/sort';
import { Paginated } from '@bx-client/http';
import { Action } from '@ngrx/store';

import { AccountLedgerCollection } from '../models';

export namespace AccountLedgerListActions {
    export const LOAD = '[AccountLedgerList] Load';
    export const LOAD_SUCCESS = '[AccountLedgerList] Load Success';
    export const LOAD_FAIL = '[AccountLedgerList] Load Fail';
    export const SELECT_ACCOUNT = '[AccountLedgerList] Select Account';
    export const SORT = '[AccountLedgerList] Sort';
    export const RESET_SORT = '[AccountLedgerList] Reset sorting';

    export class LoadAction implements Action {
        readonly type = LOAD;

        constructor(public payload: string) {}
    }

    export class LoadSuccessAction implements Action {
        readonly type = LOAD_SUCCESS;

        constructor(public payload: { collection: Paginated<AccountLedgerCollection>; selectedAccountNo: string }) {}
    }

    export class LoadFailAction implements Action {
        readonly type = LOAD_FAIL;

        constructor(public payload: number) {}
    }

    export class SelectAccountAction implements Action {
        readonly type = SELECT_ACCOUNT;

        constructor(public payload: string) {}
    }

    export class SortAction implements Action {
        readonly type = SORT;

        constructor(public payload: Sort) {}
    }

    export class ResetSortAction implements Action {
        readonly type = RESET_SORT;
    }

    export type Actions = LoadAction | LoadSuccessAction | LoadFailAction | SelectAccountAction | SortAction | ResetSortAction;
}
