import { Action } from '@ngrx/store';

import { ExportType } from '../config';
import { ExportFile, ExportOptions } from '../models';

export namespace AccountLedgerExportActions {
    export const INIT_EXPORT_OPTIONS = '[AccountLedgerExport] Init Export Options';
    export const GENERATE_EXPORT = '[AccountLedgerExport] Generate Export';
    export const GENERATE_EXPORT_SUCCESS = '[AccountLedgerExport] Generate Export Success';
    export const GENERATE_EXPORT_FAIL = '[AccountLedgerExport] Generate Export Fail';
    export const GENERATE_EXPORT_STATUS_SUCCESS = '[AccountLedgerExport] Generate Export Status Success';
    export const GENERATE_EXPORT_STATUS_FAIL = '[AccountLedgerExport] Generate Export Status Fail';
    export const GENERATE_EXPORT_FILE_SUCCESS = '[AccountLedgerExport] Generate Export File Success';
    export const GENERATE_EXPORT_FILE_FAIL = '[AccountLedgerExport] Generate Export File Fail';
    export const SAVE_FILE_SUCCESS = '[AccountLedgerExport] Save File Success';
    export const SAVE_FILE_FAIL = '[AccountLedgerExport] Save File Fail';

    export class InitExportOptionsAction implements Action {
        readonly type = INIT_EXPORT_OPTIONS;
    }

    export class GenerateExportAction implements Action {
        readonly type = GENERATE_EXPORT;

        constructor(public payload: { exportType: ExportType; exportOptions: ExportOptions }) {}
    }

    export class GenerateExportSuccessAction implements Action {
        readonly type = GENERATE_EXPORT_SUCCESS;

        constructor(public payload: string) {}
    }

    export class GenerateExportFailAction implements Action {
        readonly type = GENERATE_EXPORT_FAIL;

        constructor(public payload: number) {}
    }

    export class GenerateExportStatusSuccessAction implements Action {
        readonly type = GENERATE_EXPORT_STATUS_SUCCESS;

        constructor(public payload: string) {}
    }

    export class GenerateExportStatusFailAction implements Action {
        readonly type = GENERATE_EXPORT_STATUS_FAIL;

        constructor(public payload: number) {}
    }

    export class GenerateExportFileSuccessAction implements Action {
        readonly type = GENERATE_EXPORT_FILE_SUCCESS;

        constructor(public payload: ExportFile) {}
    }

    export class GenerateExportFileFailAction implements Action {
        readonly type = GENERATE_EXPORT_FILE_FAIL;

        constructor(public payload: number) {}
    }

    export class SaveFileSuccessAction implements Action {
        readonly type = SAVE_FILE_SUCCESS;
    }

    export class SaveFileFailAction implements Action {
        readonly type = SAVE_FILE_FAIL;

        constructor(public payload: string) {}
    }

    export type Actions =
        | InitExportOptionsAction
        | GenerateExportAction
        | GenerateExportSuccessAction
        | GenerateExportFailAction
        | GenerateExportStatusSuccessAction
        | GenerateExportStatusFailAction
        | GenerateExportFileSuccessAction
        | GenerateExportFileFailAction
        | SaveFileSuccessAction
        | SaveFileFailAction;
}
