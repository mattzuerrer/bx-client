import { Action } from '@ngrx/store';

import { AccountLedgerFilter } from '../models';

export namespace AccountLedgerFilterActions {
    export const FILTER = '[AccountLedgerFilter] Filter';
    export const INSTANT_FILTER = '[AccountLedgerFilter] Instant filter';
    export const SEARCH = '[AccountLedgerFilter] Search';
    export const CANCEL_SINGLE_FILTER = '[AccountLedgerFilter] Cancel single filter';
    export const RESET_FILTER = '[AccountLedgerFilter] Reset filter';

    export class FilterAction implements Action {
        readonly type = FILTER;

        constructor(public payload: AccountLedgerFilter) {}
    }

    export class InstantFilterAction implements Action {
        readonly type = INSTANT_FILTER;

        constructor(public payload: any) {}
    }

    export class SearchAction implements Action {
        readonly type = SEARCH;

        constructor(public payload: string) {}
    }

    export class CancelSingleFilterAction implements Action {
        readonly type = CANCEL_SINGLE_FILTER;

        constructor(public payload: { field: string; valueToSet: Date | null }) {}
    }

    export class ResetFilterAction implements Action {
        readonly type = RESET_FILTER;
    }

    export type Actions = FilterAction | InstantFilterAction | SearchAction | CancelSingleFilterAction | ResetFilterAction;
}
