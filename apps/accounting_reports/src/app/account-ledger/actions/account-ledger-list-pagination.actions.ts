import { PaginationEvent } from '@bx-client/http';
import { Action } from '@ngrx/store';

export namespace AccountLedgerListPaginationActions {
    export const CHANGE = '[AccountLedgerListPagination] Change';
    export const LOAD_PAGE_SIZE = '[AccountLedgerListPagination] Load Page Size';
    export const LOAD_PAGE_SIZE_SUCCESS = '[AccountLedgerListPagination] Load Page Size Success';

    export class ChangeAction implements Action {
        readonly type = CHANGE;

        constructor(public payload: PaginationEvent) {}
    }

    export class LoadPageSizeAction implements Action {
        readonly type = LOAD_PAGE_SIZE;
    }

    export class LoadPageSizeSuccessAction implements Action {
        readonly type = LOAD_PAGE_SIZE_SUCCESS;

        constructor(public payload: number) {}
    }

    export type Actions = ChangeAction | LoadPageSizeAction | LoadPageSizeSuccessAction;
}
