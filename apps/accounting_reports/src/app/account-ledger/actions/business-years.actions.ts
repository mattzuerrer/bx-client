import { Action } from '@ngrx/store';

import { BusinessYear } from '../models';

export namespace BusinessYearsActions {
    export const LOAD_CURRENT = '[BusinessYears] Load Current';
    export const LOAD_CURRENT_SUCCESS = '[BusinessYears] Load Success Current';
    export const LOAD_CURRENT_FAIL = '[BusinessYears] Load Fail Current';
    export const LOAD_ALL = '[BusinessYears] Load All';
    export const LOAD_ALL_SUCCESS = '[BusinessYears] Load Success All';
    export const LOAD_ALL_FAIL = '[BusinessYears] Load Fail All';

    export class LoadCurrentAction implements Action {
        readonly type = LOAD_CURRENT;
    }

    export class LoadCurrentSuccessAction implements Action {
        readonly type = LOAD_CURRENT_SUCCESS;

        constructor(public payload: BusinessYear) {}
    }

    export class LoadCurrentFailAction implements Action {
        readonly type = LOAD_CURRENT_FAIL;

        constructor(public payload: number) {}
    }

    export class LoadAllAction implements Action {
        readonly type = LOAD_ALL;
    }

    export class LoadAllSuccessAction implements Action {
        readonly type = LOAD_ALL_SUCCESS;

        constructor(public payload: BusinessYear[]) {}
    }

    export class LoadAllFailAction implements Action {
        readonly type = LOAD_ALL_FAIL;

        constructor(public payload: number) {}
    }

    export type Actions =
        | LoadCurrentAction
        | LoadCurrentSuccessAction
        | LoadCurrentFailAction
        | LoadAllAction
        | LoadAllSuccessAction
        | LoadAllFailAction;
}
