import { PayloadAction } from '@bx-client/ngrx';
import { applyChanges } from '@bx-client/utils';

import { AccountLedgerExportActions } from '../actions';
import { ExportType } from '../config';
import { ExportOptions, GenerateExportStatus } from '../models';

export interface State {
    exportType: ExportType;
    exportOptions: ExportOptions;
    exportGenerate: GenerateExportStatus;
}

const initialState: State = {
    exportType: null,
    exportOptions: new ExportOptions(),
    exportGenerate: new GenerateExportStatus()
};

export function reducer(state: State = initialState, action: PayloadAction): State {
    switch (action.type) {
        case AccountLedgerExportActions.INIT_EXPORT_OPTIONS:
            return applyChanges(state, {
                exportOptions: applyChanges(state.exportOptions, {
                    exportAllAccounts: true,
                    paperOrientation: 'landscape',
                    forcePageBreak: true
                }),
                exportGenerate: applyChanges(state.exportGenerate, { fileId: '', status: '' })
            });
        case AccountLedgerExportActions.GENERATE_EXPORT:
            return applyChanges(state, {
                exportType: action.payload.exportType,
                exportOptions: action.payload.exportOptions,
                exportGenerate: applyChanges(state.exportGenerate, { fileId: '', status: '' })
            });
        case AccountLedgerExportActions.GENERATE_EXPORT_SUCCESS:
            return applyChanges(state, {
                exportGenerate: applyChanges(state.exportGenerate, { fileId: action.payload })
            });
        case AccountLedgerExportActions.GENERATE_EXPORT_STATUS_SUCCESS:
            return applyChanges(state, {
                exportGenerate: applyChanges(state.exportGenerate, { status: action.payload })
            });
        default:
            return state;
    }
}
