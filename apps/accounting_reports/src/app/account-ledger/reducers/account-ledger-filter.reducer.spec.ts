import { AccountLedgerFilterActions } from '../actions';
import { AccountLedgerFilter } from '../models';

import { reducer } from './account-ledger-filter.reducer';

const initialState = {
    values: {
        fromAccount: null,
        toAccount: null,
        taxId: null,
        entryTypes: [],
        fromAmount: null,
        toAmount: null,
        fromDate: null,
        toDate: null,
        vatDigits: [],
        referenceNo: null,
        description: null,
        includeCarryover: false,
        showZeroAmountEntries: false,
        showOnlyAccountsWithTransactions: false,
        showAccountDescription: false,
        decimalPlaces: 2,
        checkForNewEntriesOnBackend: 0
    },
    isFilterActive: false,
    searchString: null
};

const loadedState = {
    values: {
        fromAccount: 1,
        toAccount: 2,
        taxId: 19,
        entryTypes: ['entry_type'],
        fromAmount: 1,
        toAmount: 9999,
        fromDate: new Date('1.1.2018'),
        toDate: new Date('11.11.2018'),
        vatDigits: [200],
        referenceNo: 'RE-1234',
        description: 'desc',
        includeCarryover: false,
        showZeroAmountEntries: true,
        showOnlyAccountsWithTransactions: true,
        showAccountDescription: false,
        decimalPlaces: 6,
        checkForNewEntriesOnBackend: 0
    },
    isFilterActive: true,
    searchString: null
};

const filterPayload: AccountLedgerFilter = {
    fromAccount: 1,
    toAccount: 2,
    taxId: 19,
    entryTypes: ['entry_type'],
    fromAmount: 1,
    toAmount: 9999,
    fromDate: new Date('1.1.2018'),
    toDate: new Date('11.11.2018'),
    vatDigits: [200],
    referenceNo: 'RE-1234',
    description: 'desc',
    includeCarryover: true,
    showZeroAmountEntries: true,
    showOnlyAccountsWithTransactions: true,
    showAccountDescription: false,
    decimalPlaces: 6,
    checkForNewEntriesOnBackend: 0
};

const filterAfterReset = {
    values: {
        fromAccount: null,
        toAccount: null,
        taxId: null,
        entryTypes: [],
        fromAmount: null,
        toAmount: null,
        fromDate: null,
        toDate: null,
        vatDigits: [],
        referenceNo: null,
        description: null,
        includeCarryover: false,
        showZeroAmountEntries: true,
        showOnlyAccountsWithTransactions: true,
        showAccountDescription: false,
        decimalPlaces: 6,
        checkForNewEntriesOnBackend: 0
    },
    isFilterActive: false,
    searchString: null
};

const instantFilterPayload = { decimalPlaces: 6 };

describe('Account Ledger Filter reducer', () => {
    it('should return initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return new filter state on FILTER', () => {
        const state = initialState;
        const action = new AccountLedgerFilterActions.FilterAction(filterPayload);
        expect(reducer(state, action)).toEqual(loadedState);
    });

    it('should return new filter state on INSTANT_FILTER', () => {
        const state = initialState;
        const action = new AccountLedgerFilterActions.InstantFilterAction(instantFilterPayload);
        expect(reducer(state, action).values).toEqual(Object.assign({}, initialState.values, instantFilterPayload));
    });

    it('should return new state on SEARCH', () => {
        const state = initialState;
        const action = new AccountLedgerFilterActions.SearchAction('searchString');
        expect(reducer(state, action)).toEqual(Object.assign({}, initialState, { searchString: 'searchString' }));
    });

    it('should reset values (except instant filter part) on RESET_FILTER', () => {
        const state = loadedState;
        const action = new AccountLedgerFilterActions.ResetFilterAction();
        expect(reducer(state, action)).toEqual(filterAfterReset);
    });
});
