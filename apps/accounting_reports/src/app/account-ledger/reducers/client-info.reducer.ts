import { applyChanges } from '@bx-client/utils';

import { ClientInfoActions } from '../actions';

export interface State {
    currency: string;
    hasVat: boolean;
}

const initialState: State = {
    currency: null,
    hasVat: null
};

export function reducer(state: State = initialState, action: ClientInfoActions.Actions): State {
    switch (action.type) {
        case ClientInfoActions.LOAD_SUCCESS:
            return applyChanges(state, { currency: action.payload.currency, hasVat: action.payload.hasVat });
        default:
            return state;
    }
}
