import { AccountLedgerExportActions } from '../actions';
import { ExportType } from '../config';
import { ExportOptions, GenerateExportStatus } from '../models';

import { reducer } from './account-ledger-export.reducer';

const exportPayload = {
    exportType: ExportType.pdf,
    exportOptions: { exportAllAccounts: true, paperOrientation: 'landscape', forcePageBreak: true }
};

const exportOptions = { exportAllAccounts: true, paperOrientation: 'landscape', forcePageBreak: true };

const exportGenerate = { fileId: '', status: '' };

const initialState = {
    exportType: null,
    exportOptions: new ExportOptions(),
    exportGenerate: new GenerateExportStatus()
};

const loadedExportOptions = {
    exportType: null,
    exportOptions,
    exportGenerate
};

const loadedState = {
    exportType: ExportType.pdf,
    exportOptions,
    exportGenerate
};

describe('Account Ledger Options reducer', () => {
    it('should return initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return new state on INIT_PDF_OPTIONS', () => {
        const state = initialState;
        const action = new AccountLedgerExportActions.InitExportOptionsAction();
        expect(reducer(state, action)).toEqual(loadedExportOptions);
    });

    it('should return new state on GENERATE_PDF', () => {
        const state = initialState;
        const action = new AccountLedgerExportActions.GenerateExportAction(exportPayload);
        expect(reducer(state, action)).toEqual(loadedState);
    });

    it('should return new state on GENERATE_PDF_SUCCESS containing fileId', () => {
        const state = initialState;
        const addedFileId = Object.assign(initialState, { exportGenerate: { fileId: 'fxZxy' } });
        const action = new AccountLedgerExportActions.GenerateExportSuccessAction('fxZxy');
        expect(reducer(state, action)).toEqual(addedFileId);
    });

    it('should return new state on GENERATE_PDF_STATUS_SUCCESS containing status', () => {
        const state = initialState;
        const addedStatus = Object.assign(initialState, { exportGenerate: { status: 'ready' } });
        const action = new AccountLedgerExportActions.GenerateExportStatusSuccessAction('ready');
        expect(reducer(state, action)).toEqual(addedStatus);
    });
});
