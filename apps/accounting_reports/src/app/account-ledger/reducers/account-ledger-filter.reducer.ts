import { PayloadAction } from '@bx-client/ngrx';
import { applyChanges, isArray } from '@bx-client/utils';

import { AccountLedgerFilterActions, AccountLedgerListActions } from '../actions';
import { AccountLedgerFilter, DESCRIPTION, ENTRY_TYPES, FROM_AMOUNT, REFERENCE_NO, TAX_ID, TO_AMOUNT, VAT_DIGITS } from '../models';

export interface State {
    values: AccountLedgerFilter;
    isFilterActive: boolean;
    searchString: string;
}

const initialState: State = {
    values: {
        fromAccount: null,
        toAccount: null,
        taxId: null,
        entryTypes: [],
        fromAmount: null,
        toAmount: null,
        fromDate: null,
        toDate: null,
        vatDigits: [],
        referenceNo: null,
        description: null,
        includeCarryover: false,
        showZeroAmountEntries: false,
        showOnlyAccountsWithTransactions: false,
        showAccountDescription: false,
        decimalPlaces: 2,
        checkForNewEntriesOnBackend: 0
    },
    isFilterActive: false,
    searchString: null
};

const resetStateValues = {
    fromAccount: null,
    toAccount: null,
    taxId: null,
    entryTypes: [],
    fromAmount: null,
    toAmount: null,
    fromDate: null,
    toDate: null,
    vatDigits: [],
    referenceNo: null,
    description: null
};

export function reducer(state: State = initialState, action: PayloadAction): State {
    const hasAdavencedFilters = (): boolean => {
        return (
            action.payload[FROM_AMOUNT] ||
            action.payload[TO_AMOUNT] ||
            action.payload[TAX_ID] ||
            (isArray(action.payload[VAT_DIGITS]) && action.payload[VAT_DIGITS].length) ||
            (isArray(action.payload[ENTRY_TYPES]) && action.payload[ENTRY_TYPES].length) ||
            action.payload[REFERENCE_NO] ||
            action.payload[DESCRIPTION]
        );
    };

    switch (action.type) {
        case AccountLedgerListActions.SORT:
        case AccountLedgerListActions.SELECT_ACCOUNT:
            return applyChanges(state, {
                values: applyChanges(state.values, { checkForNewEntriesOnBackend: 0 })
            });
        case AccountLedgerFilterActions.FILTER:
            if (hasAdavencedFilters()) {
                return applyChanges(state, { values: { ...action.payload, includeCarryover: false }, isFilterActive: true });
            }
            return applyChanges(state, { values: action.payload, isFilterActive: true });
        case AccountLedgerFilterActions.INSTANT_FILTER:
            return applyChanges(state, {
                values: applyChanges(state.values, action.payload.valueOf())
            });
        case AccountLedgerFilterActions.SEARCH:
            return applyChanges(state, { searchString: action.payload });
        case AccountLedgerFilterActions.CANCEL_SINGLE_FILTER:
            const resetFilter = {};
            resetFilter[action.payload.field] = action.payload.valueToSet;
            return applyChanges(state, {
                values: applyChanges(state.values, resetFilter)
            });
        case AccountLedgerFilterActions.RESET_FILTER:
            return applyChanges(state, { values: applyChanges(state.values, resetStateValues), isFilterActive: false });
        default:
            return state;
    }
}
