import { ActionReducerMap } from '@ngrx/store';

import * as fromOptions from './account-ledger-export.reducer';
import * as fromFilter from './account-ledger-filter.reducer';
import * as fromListPagination from './account-ledger-list-pagination.reducer';
import * as fromList from './account-ledger-list.reducer';
import * as fromBusinessYears from './business-years.reducer';
import * as fromClientInfo from './client-info.reducer';

export interface State {
    filter: fromFilter.State;
    list: fromList.State;
    listPagination: fromListPagination.State;
    options: fromOptions.State;
    clientInfo: fromClientInfo.State;
    businessYears: fromBusinessYears.State;
}

export const reducers: ActionReducerMap<State> = {
    filter: fromFilter.reducer,
    list: fromList.reducer,
    listPagination: fromListPagination.reducer,
    options: fromOptions.reducer,
    clientInfo: fromClientInfo.reducer,
    businessYears: fromBusinessYears.reducer
};
