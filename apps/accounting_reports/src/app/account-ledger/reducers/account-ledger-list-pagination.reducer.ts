import { applyChanges } from '@bx-client/utils';

import { AccountLedgerFilterActions, AccountLedgerListActions, AccountLedgerListPaginationActions } from '../actions';
import { defaultPaginationPageSize, paginationPageSizeOptions } from '../config';

export interface State {
    pageSize: number;
    pageIndex: number;
    length: number;
    pageSizeOptions: number[];
}

const initialState: State = {
    pageSize: defaultPaginationPageSize,
    pageIndex: 0,
    length: 0,
    pageSizeOptions: paginationPageSizeOptions
};

export function reducer(
    state: State = initialState,
    action: AccountLedgerListPaginationActions.Actions | AccountLedgerListActions.Actions | AccountLedgerFilterActions.FilterAction
): State {
    switch (action.type) {
        case AccountLedgerListActions.SELECT_ACCOUNT:
            return applyChanges(state, {
                pageIndex: 0
            });
        case AccountLedgerListActions.LOAD_SUCCESS:
            return applyChanges(state, {
                pageSize: action.payload.collection.pageSize,
                pageIndex: action.payload.collection.pageIndex,
                length: action.payload.collection.length
            });
        case AccountLedgerListPaginationActions.CHANGE:
            return applyChanges(state, {
                pageSize: action.payload.pageSize,
                pageIndex: action.payload.pageIndex
            });
        case AccountLedgerFilterActions.FILTER:
            return applyChanges(state, {
                pageIndex: 0,
                length: 0
            });
        case AccountLedgerListPaginationActions.LOAD_PAGE_SIZE_SUCCESS:
            return applyChanges(state, {
                pageSize: action.payload
            });
        default:
            return state;
    }
}
