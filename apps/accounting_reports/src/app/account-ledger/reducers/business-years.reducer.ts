import { applyChanges } from '@bx-client/utils';

import { BusinessYearsActions } from '../actions';
import { BusinessYear } from '../models';

export interface State {
    currentBusinessYear: BusinessYear;
    allBusinessYears: BusinessYear[];
    loadedCurrentBusinessYear: boolean;
    loadedAllBusinessYears: boolean;
}

const initialState: State = {
    currentBusinessYear: {
        id: null,
        start: null,
        end: null
    },
    allBusinessYears: [],
    loadedCurrentBusinessYear: false,
    loadedAllBusinessYears: false
};

export function reducer(state: State = initialState, action: BusinessYearsActions.Actions): State {
    switch (action.type) {
        case BusinessYearsActions.LOAD_CURRENT_SUCCESS:
            return applyChanges(state, { currentBusinessYear: action.payload, loadedCurrentBusinessYear: true });
        case BusinessYearsActions.LOAD_ALL_SUCCESS:
            return applyChanges(state, { allBusinessYears: action.payload, loadedAllBusinessYears: true });
        default:
            return state;
    }
}
