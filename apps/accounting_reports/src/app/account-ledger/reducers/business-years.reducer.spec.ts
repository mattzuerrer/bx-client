import { BusinessYearsActions } from '../actions';

import { reducer } from './business-years.reducer';

const initialState = {
    currentBusinessYear: {
        id: null,
        start: null,
        end: null
    },
    allBusinessYears: [],
    loadedCurrentBusinessYear: false,
    loadedAllBusinessYears: false
};

const currentLoadedState = {
    currentBusinessYear: {
        id: 1,
        start: new Date('2018-01-01'),
        end: new Date('2018-12-31')
    },
    allBusinessYears: [],
    loadedCurrentBusinessYear: true,
    loadedAllBusinessYears: false
};

const businessYearPayload = {
    id: 1,
    start: new Date('2018-01-01'),
    end: new Date('2018-12-31')
};

const allLoadedState = {
    currentBusinessYear: {
        id: null,
        start: null,
        end: null
    },
    allBusinessYears: [businessYearPayload],
    loadedCurrentBusinessYear: false,
    loadedAllBusinessYears: true
};

describe('Business years reducer', () => {
    it('should return initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return new state on LOAD_CURRENT_SUCCESS', () => {
        const state = initialState;
        const action = new BusinessYearsActions.LoadCurrentSuccessAction(businessYearPayload);
        expect(reducer(state, action)).toEqual(currentLoadedState);
    });

    it('should return new state on LOAD_ALL_SUCCESS', () => {
        const state = initialState;
        const action = new BusinessYearsActions.LoadAllSuccessAction([businessYearPayload]);
        expect(reducer(state, action)).toEqual(allLoadedState);
    });
});
