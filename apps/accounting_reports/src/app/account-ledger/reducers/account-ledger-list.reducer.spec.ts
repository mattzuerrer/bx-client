import { Sort } from '@angular/material/sort';
import { Paginated } from '@bx-client/http';

import { AccountLedgerFilterActions, AccountLedgerListActions } from '../actions';
import { accountLedgerCollection } from '../fixtures';
import { AccountLedgerCollection } from '../models';

import { DEFAULT_SORT_DIRECTION, DEFAULT_SORT_FIELD, reducer, State } from './account-ledger-list.reducer';

const initialState: State = {
    entries: [],
    accounts: [],
    carryOver: null,
    selectedAccountNo: null,
    totals: {
        creditAmount: null,
        debitAmount: null,
        balanceAmount: null,
        currencies: []
    },
    hasVatRecords: null,
    showBalance: true,
    loaded: false,
    loading: false,
    sort: { active: 'date', direction: 'asc' }
};

const loadedState: State = {
    entries: accountLedgerCollection.accountingRecords,
    accounts: accountLedgerCollection.aggregations.accounts,
    carryOver: accountLedgerCollection.carryOver,
    selectedAccountNo: '0',
    totals: accountLedgerCollection.aggregations.totals,
    hasVatRecords: accountLedgerCollection.aggregations.hasVatRecords,
    showBalance: true,
    loaded: true,
    loading: false,
    sort: initialState.sort
};

const sortPayload: Sort = {
    active: 'description',
    direction: 'desc'
};

const paginatePayload: Paginated<AccountLedgerCollection> = {
    items: accountLedgerCollection,
    length: 58,
    pageIndex: 0,
    pageSize: 50
};

describe('Account Ledger List reducer', () => {
    it('should return initial state', () => {
        const state = undefined;
        const action = { type: null, payload: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return new state on load', () => {
        const newState = reducer(initialState, new AccountLedgerListActions.LoadAction('1'));
        expect(newState.loaded).toBe(false);
        expect(newState.loading).toBe(true);
    });

    it('should return new state on load success', () => {
        const state = initialState;
        const action = new AccountLedgerListActions.LoadSuccessAction({
            collection: paginatePayload,
            selectedAccountNo: paginatePayload.items.selectedAccountNumber
        });
        expect(reducer(state, action)).toEqual(loadedState);
    });

    it('should return loaded false and loading false on load fail', () => {
        const thrownError = { status: 404 };
        const newState = reducer(initialState, new AccountLedgerListActions.LoadFailAction(thrownError.status));
        expect(newState.loaded).toBe(true);
        expect(newState.loading).toBe(false);
    });

    it('should return new sort state on sort', () => {
        const newState = reducer(initialState, new AccountLedgerListActions.SortAction(sortPayload));
        expect(newState.sort).toEqual(sortPayload);
    });

    it('should reset state to initial state on RESET_FILTER', () => {
        const state = loadedState;
        const action = new AccountLedgerFilterActions.ResetFilterAction();
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should set showBalance on sort date to true and else to false', () => {
        const state = loadedState;
        let action = new AccountLedgerListActions.SortAction(initialState.sort);
        expect(reducer(state, action).showBalance).toBe(true);

        action = new AccountLedgerListActions.SortAction(sortPayload);
        expect(reducer(state, action).showBalance).toBe(false);
    });

    it('should reset state to initial state on RESET_SORT', () => {
        const state = loadedState;
        state.sort = { active: 'contact', direction: 'desc' };

        const action = new AccountLedgerListActions.ResetSortAction();
        expect(reducer(state, action).sort.active).toBe(DEFAULT_SORT_FIELD);
        expect(reducer(state, action).sort.direction).toBe(DEFAULT_SORT_DIRECTION);
    });
});
