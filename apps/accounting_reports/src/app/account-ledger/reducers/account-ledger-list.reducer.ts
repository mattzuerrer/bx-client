import { Sort } from '@angular/material/sort';
import { applyChanges } from '@bx-client/utils';

import { AccountLedgerFilterActions, AccountLedgerListActions } from '../actions';
import { AccountLedgerEntry, Carryover, DATE, EntriesTotals } from '../models';

export const DEFAULT_SORT_FIELD = 'date';
export const DEFAULT_SORT_DIRECTION = 'asc';

export interface State {
    entries: AccountLedgerEntry[];
    accounts: string[];
    carryOver: Carryover;
    selectedAccountNo: string;
    totals: EntriesTotals;
    hasVatRecords: boolean;
    showBalance: boolean;
    loaded: boolean;
    loading: boolean;
    sort: Sort;
}

const initialState: State = {
    entries: [],
    accounts: [],
    carryOver: null,
    selectedAccountNo: null,
    totals: {
        creditAmount: null,
        debitAmount: null,
        balanceAmount: null,
        currencies: []
    },
    hasVatRecords: null,
    showBalance: true,
    loaded: false,
    loading: false,
    sort: { active: DEFAULT_SORT_FIELD, direction: DEFAULT_SORT_DIRECTION }
};

export function reducer(
    state: State = initialState,
    action: AccountLedgerListActions.Actions | AccountLedgerFilterActions.ResetFilterAction
): State {
    switch (action.type) {
        case AccountLedgerListActions.LOAD:
            return applyChanges(state, { loaded: false, loading: true });
        case AccountLedgerListActions.LOAD_SUCCESS:
            const unfrozenAccounts = Object.assign([] as string[], action.payload.collection.items.aggregations.accounts);

            // sanity check, if we have no accounts we have no carry over, here because of bugs on BE side
            let payloadCarryOver = action.payload.collection.items.carryOver;
            if (unfrozenAccounts.length < 1) {
                payloadCarryOver = null;
            }

            // sanity check, if the account number is different one than selected, choose no entries
            let entries = action.payload.collection.items.accountingRecords;
            if (action.payload.collection.items.selectedAccountNumber !== action.payload.selectedAccountNo) {
                entries = [];
            }

            return applyChanges(state, {
                selectedAccountNo: action.payload.selectedAccountNo,
                entries,
                accounts: unfrozenAccounts.sort(),
                carryOver: payloadCarryOver,
                totals: action.payload.collection.items.aggregations.totals,
                hasVatRecords: action.payload.collection.items.aggregations.hasVatRecords,
                loaded: true,
                loading: false
            });
        case AccountLedgerListActions.LOAD_FAIL:
            return applyChanges(state, { loaded: true, loading: false });
        case AccountLedgerListActions.SORT:
            return applyChanges(state, { sort: action.payload, showBalance: action.payload.active === DATE });
        case AccountLedgerListActions.RESET_SORT:
            return applyChanges(state, { sort: { active: DEFAULT_SORT_FIELD, direction: DEFAULT_SORT_DIRECTION } });
        case AccountLedgerFilterActions.RESET_FILTER:
            return applyChanges(state, initialState);
        default:
            return state;
    }
}
