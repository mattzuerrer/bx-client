import { Paginated, PaginationEvent } from '@bx-client/http';
import { applyChanges } from '@bx-client/utils';

import { AccountLedgerListActions, AccountLedgerListPaginationActions } from '../actions';
import { accountLedgerCollection } from '../fixtures';
import { AccountLedgerCollection } from '../models';

import { reducer } from './account-ledger-list-pagination.reducer';

const initialState = {
    pageSize: 50,
    pageIndex: 0,
    length: 0,
    pageSizeOptions: [10, 25, 50, 100, 200]
};

const paginatedPayload: Paginated<AccountLedgerCollection> = {
    items: accountLedgerCollection,
    length: 58,
    pageIndex: 0,
    pageSize: 50
};

const paginationEvent: PaginationEvent = {
    pageSize: 10,
    pageIndex: 1
};

const loadSuccessPayload = {
    collection: paginatedPayload,
    selectedAccountNo: '0'
};

describe('Account Ledger List pagination reducer', () => {
    it('should return initial state', () => {
        const state = undefined;
        const action = { type: null, payload: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should set pageIndex to initial state', () => {
        const state = applyChanges(initialState, { pageIndex: 22 });
        const action = new AccountLedgerListActions.SelectAccountAction('1');
        expect(reducer(state, action).pageIndex).toBe(0);
    });

    it('should add length, pageIndex and pageSize to state on load success', () => {
        const state = initialState;
        const action = new AccountLedgerListActions.LoadSuccessAction(loadSuccessPayload);
        expect(reducer(state, action).length).toBe(58);
        expect(reducer(state, action).pageIndex).toBe(0);
        expect(reducer(state, action).pageSize).toBe(50);
    });

    it('should add length, pageIndex and pageSize to state on load account success', () => {
        const state = initialState;
        const action = new AccountLedgerListActions.LoadSuccessAction(loadSuccessPayload);
        expect(reducer(state, action).length).toBe(58);
        expect(reducer(state, action).pageIndex).toBe(0);
        expect(reducer(state, action).pageSize).toBe(50);
    });

    it('should update pageSize and pageIndex on change', () => {
        const state = initialState;
        const action = new AccountLedgerListPaginationActions.ChangeAction(paginationEvent);
        expect(reducer(state, action).pageSize).toBe(10);
        expect(reducer(state, action).pageIndex).toBe(1);
    });

    it('should update pageSize on LOAD_PAGE_SIZE_SUCCESS action', () => {
        const state = initialState;
        const action = new AccountLedgerListPaginationActions.LoadPageSizeSuccessAction(123);
        expect(reducer(state, action).pageSize).toBe(123);
    });
});
