import { ClientInfoActions } from '../actions';

import { reducer } from './client-info.reducer';

const initialState = {
    currency: null,
    hasVat: null
};

const loadedState = {
    currency: 'CHF',
    hasVat: true
};

describe('Client info reducer', () => {
    it('should return initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return new state on LOAD_SUCCESS', () => {
        const state = initialState;
        const action = new ClientInfoActions.LoadSuccessAction({ currency: 'CHF', hasVat: true });
        expect(reducer(state, action)).toEqual(loadedState);
    });
});
