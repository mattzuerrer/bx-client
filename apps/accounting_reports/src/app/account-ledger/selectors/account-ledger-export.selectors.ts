import { createSelector } from '@ngrx/store';

import { AccountLedgerSelectors } from './account-ledger.selectors';

export namespace AccountLedgerExportSelectors {
    export const getExportOptions = createSelector(AccountLedgerSelectors.getOptionsState, state => state.exportOptions);

    export const getExportGenerate = createSelector(AccountLedgerSelectors.getOptionsState, state => state.exportGenerate);

    export const getExportType = createSelector(AccountLedgerSelectors.getOptionsState, state => state.exportType);
}
