import { applyChanges, byId, convertToISO8601String as dateToString } from '@bx-client/utils';
import { createSelector } from '@ngrx/store';
import * as moment from 'moment';

import {
    AccountLedgerFilter,
    AccountNumberRange,
    BusinessYear,
    DateRange,
    DESCRIPTION,
    ENTRY_TYPES,
    FROM_AMOUNT,
    FROM_DATE,
    REFERENCE_NO,
    TAX_ID,
    TO_AMOUNT,
    TO_DATE,
    VAT_DIGITS
} from '../models';
import { SelectPeriod } from '../models/Periods';
import { LedgerAccount, LedgerAccountSelectors } from '../services/account';

import { AccountLedgerSelectors } from './account-ledger.selectors';
import { BusinessYearsSelectors } from './business-year.selectors';

export type FilterMap = Map<string, string | string[]>;

const getFilterMap = (accounts: LedgerAccount[], values: AccountLedgerFilter): FilterMap => {
    const filters: FilterMap = new Map();

    const hasAdvancedFilters = (): boolean => {
        return (
            !!values[FROM_AMOUNT] &&
            !!values[TO_AMOUNT] &&
            !!values[TAX_ID] &&
            !!values[VAT_DIGITS] &&
            !!values[ENTRY_TYPES] &&
            !!values[REFERENCE_NO] &&
            !!values[DESCRIPTION]
        );
    };

    filters.set('load', values.checkForNewEntriesOnBackend.toString());

    if (values.fromAccount) {
        filters.set('account-number-from', accounts.find(byId(values.fromAccount)).number);
    }

    if (values.toAccount) {
        filters.set('account-number-to', accounts.find(byId(values.toAccount)).number);
    }

    if (values.fromAmount) {
        filters.set('min-amount', values.fromAmount.toString());
    }

    if (values.toAmount) {
        filters.set('max-amount', values.toAmount.toString());
    }

    if (values.taxId) {
        filters.set('vat-ids[]', values.taxId.toString());
    }

    if (values.vatDigits && values.vatDigits.length > 0) {
        filters.set('vat-digits[]', values.vatDigits.map(digit => digit.toString()));
    }

    if (values.entryTypes && values.entryTypes.length > 0) {
        filters.set('accounting-record-types[]', values.entryTypes.map(type => type.toString()));
    }

    if (values.fromDate) {
        filters.set('date-from', dateToString(values.fromDate));
    }

    if (values.toDate) {
        filters.set('date-to', dateToString(values.toDate));
    }

    if (values.decimalPlaces) {
        filters.set('amount-precision', values.decimalPlaces.toString());
    }

    if (values.includeCarryover && !hasAdvancedFilters()) {
        filters.set('include-carry-over', true.toString());
    }

    if (!values.showZeroAmountEntries) {
        filters.set('exclude-zero-amount', false.toString());
    }

    filters.set('include-accounts-without-transactions', 'false');
    if (values.showOnlyAccountsWithTransactions !== null) {
        filters.set('include-accounts-without-transactions', (!values.showOnlyAccountsWithTransactions).toString());
    }

    filters.set('show-contra-account-description', 'false');
    if (values.showAccountDescription !== null) {
        filters.set('show-contra-account-description', values.showAccountDescription.toString());
    }

    if (values.referenceNo) {
        filters.set('q[reference_number]', values.referenceNo.trim());
    }

    if (values.description) {
        filters.set('q[description]', values.description.trim());
    }

    return filters;
};

const getAccountNumbers = (accounts: LedgerAccount[], values: AccountLedgerFilter) => {
    let accountsNumbers: AccountNumberRange = {};
    if (values.fromAccount) {
        accountsNumbers = applyChanges(accountsNumbers, {
            fromAccount: accounts.find(byId(values.fromAccount)).number
        });
    }
    if (values.toAccount) {
        accountsNumbers = applyChanges(accountsNumbers, {
            toAccount: accounts.find(byId(values.toAccount)).number
        });
    }

    return accountsNumbers;
};

const getAdvancedFiltersActive = (values: AccountLedgerFilter) => {
    const advancedFilters = [FROM_AMOUNT, TO_AMOUNT, TAX_ID, VAT_DIGITS, ENTRY_TYPES, REFERENCE_NO, DESCRIPTION];
    const advancedFiltersActive: string[] = [];
    for (const key of advancedFilters) {
        if (isFilterUsed(values, key)) {
            advancedFiltersActive.push(key);
        }
    }
    return advancedFiltersActive;
};

const getDateRange = (values: AccountLedgerFilter): DateRange => {
    return { fromDate: values.fromDate, toDate: values.toDate };
};

const searchQueryBuilder = (searchString: string) => {
    return searchString && searchString.length > 0 ? searchString : null;
};

const isFilterUsed = (values: AccountLedgerFilter, filter: string) => {
    return (values[filter] instanceof Array && values[filter].length > 0) || (!(values[filter] instanceof Array) && values[filter]);
};

const getChippableActive = (values: AccountLedgerFilter) => {
    const chippableFilters = [FROM_DATE, TO_DATE, FROM_AMOUNT, TO_AMOUNT, TAX_ID, VAT_DIGITS, ENTRY_TYPES, REFERENCE_NO, DESCRIPTION];
    const chippableActive: string[] = [];
    for (const key of chippableFilters) {
        if (isFilterUsed(values, key)) {
            chippableActive.push(key);
        }
    }
    return chippableActive;
};

const getBussinessPeriods = (current: BusinessYear, all: BusinessYear[]): SelectPeriod[] => {
    // The value -1 means take the quarter after current one
    // Zero means the current quarter
    // 1 and 2 means one and two quarters in the past accordingly
    const quarters: SelectPeriod[] = [-1, 0, 1, 2]
        .map(value => moment().subtract(value, 'Q'))
        .filter(quarter =>
            all.find(year => {
                const start = moment(year.start).startOf('quarter');
                const end = moment(year.end).endOf('quarter');
                const isBetween = moment(quarter)
                    .startOf('quarter')
                    .isBetween(start, end, null, '[]');
                return isBetween;
            })
        )
        .map((quarter, index, array) => ({
            start: quarter.startOf('quarter').toDate(),
            end: quarter.endOf('quarter').toDate(),
            viewValue: quarter.format('[Q]Q-Y'),
            separator: index === array.length - 1
        }));

    // sort descending
    let years: SelectPeriod[] = all
        .slice()
        .sort((a, b) => moment(b.start).valueOf() - moment(a.start).valueOf())
        .map(y => ({
            start: new Date(y.start),
            end: new Date(y.end),
            id: y.id,
            viewValue: `${moment(y.start).format('L')} - ${moment(y.end).format('L')}`,
            isCurrent: y.start === current.start && y.end === current.end
        }));
    const currentIndex = years.findIndex(y => y.isCurrent);
    const now = new Date();

    years = years.splice(currentIndex > 0 ? currentIndex - 1 : 0, 4);

    return [
        {
            start: now,
            end: now,
            viewValue: 'accounting_reports.filter.today', // it will be passed to translate pipe
            separator: true
        },
        ...quarters,
        ...years
    ];
};

export namespace AccountLedgerFilterSelectors {
    export const getValues = createSelector(AccountLedgerSelectors.getFilterState, state => state.values);

    export const getFilters = createSelector(LedgerAccountSelectors.getCollection, getValues, getFilterMap);

    export const getFilterAccountNumbers = createSelector(LedgerAccountSelectors.getCollection, getValues, getAccountNumbers);

    export const getAdvancedFiltersUsed = createSelector(getValues, getAdvancedFiltersActive);

    export const isFilterActive = createSelector(AccountLedgerSelectors.getFilterState, state => state.isFilterActive);

    export const getFilterDateRange = createSelector(getValues, getDateRange);

    export const getFilterDecimalPlaces = createSelector(getValues, values => values.decimalPlaces);

    export const getSearchString = createSelector(AccountLedgerSelectors.getFilterState, state => state.searchString);

    export const getSearchQuery = createSelector(getSearchString, searchQueryBuilder);

    export const getIsSearchActive = createSelector(getSearchString, searchString => searchString && searchString.length > 0);

    export const getIsIncludeCarryoverChecked$ = createSelector(
        AccountLedgerSelectors.getFilterState,
        state => state.values.includeCarryover
    );

    export const getChippableActiveFilters = createSelector(getValues, getChippableActive);

    export const getShowOnlyAccountsWithTransactions = createSelector(
        AccountLedgerSelectors.getFilterState,
        state => state.values.showOnlyAccountsWithTransactions
    );

    export const getShowAccountDescription = createSelector(
        AccountLedgerSelectors.getFilterState,
        state => state.values.showAccountDescription
    );

    export const getBussinessYearPeriods = createSelector(
        BusinessYearsSelectors.getCurrentBusinessYear,
        BusinessYearsSelectors.getAllBusinessYears,
        getBussinessPeriods
    );
}
