import * as moment from 'moment';

import {
    allBusinessYears,
    allSelectPeriods,
    businessYear2019,
    currentBusinessYear2019,
    currentBusinessYearAtTheEndOf2018,
    currentBusinessYearInTheMiddleOf2019,
    currentDay,
    dateFormatLocale,
    selectBusinessYear2018Partial,
    selectBusinessYear2019,
    selectQ1_2019,
    selectQ2_2019,
    selectQ3_2019,
    selectQ4_2018,
    selectToday
} from '../fixtures/periods.fixture';

import { AccountLedgerFilterSelectors } from './account-ledger-filter.selectors';

describe('AccountLedgerFilter selector ', () => {
    const selector = AccountLedgerFilterSelectors.getBussinessYearPeriods;

    beforeAll(() => {
        moment.locale(dateFormatLocale);
        jasmine.clock().install();
        jasmine.clock().mockDate(new Date(currentDay));
    });

    afterAll(() => {
        jasmine.clock().uninstall();
    });

    it('Should return business years and periods', () => {
        const options = selector.projector(currentBusinessYear2019, allBusinessYears);
        expect(options).toEqual(allSelectPeriods);
    });

    it('Should return options with only one business year', () => {
        const options = selector.projector(currentBusinessYear2019, [{ ...businessYear2019 }]);
        expect(options).toEqual([
            { ...selectToday },
            { ...selectQ3_2019 },
            { ...selectQ2_2019 },
            { ...selectQ1_2019, separator: true },
            { ...selectBusinessYear2019, isCurrent: true }
        ]);
    });

    it("Should return only today if there' no business year defined", () => {
        const options = selector.projector(currentBusinessYear2019, []);
        expect(options).toEqual([{ ...selectToday }]);
    });

    it('Should return options when Business Year starts in the middle of the year', () => {
        const options = selector.projector(currentBusinessYearInTheMiddleOf2019, [{ ...businessYear2019 }]);
        expect(options).toEqual([
            { ...selectToday },
            { ...selectQ3_2019 },
            { ...selectQ2_2019 },
            { ...selectQ1_2019, separator: true },
            { ...selectBusinessYear2019 }
        ]);
    });

    it('Should return options when Business Year starts at the end of the year', () => {
        const options = selector.projector(currentBusinessYearAtTheEndOf2018, [{ ...currentBusinessYearAtTheEndOf2018 }]);
        expect(options).toEqual([
            { ...selectToday },
            { ...selectQ4_2018, separator: true },
            { ...selectBusinessYear2018Partial, isCurrent: true }
        ]);
    });
});
