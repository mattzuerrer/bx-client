import { paginationOptions } from '@bx-client/http';
import { createSelector } from '@ngrx/store';

import { AccountLedgerSelectors } from './account-ledger.selectors';

export namespace AccountLedgerListPaginationSelectors {
    export const getLength = createSelector(AccountLedgerSelectors.getListPaginationState, state => state.length);

    export const getPageSize = createSelector(AccountLedgerSelectors.getListPaginationState, state => state.pageSize);

    export const getPageIndex = createSelector(AccountLedgerSelectors.getListPaginationState, state => state.pageIndex);

    export const getPageSizeOptions = createSelector(AccountLedgerSelectors.getListPaginationState, state => state.pageSizeOptions);

    export const getPaginationOptions = createSelector(getPageIndex, getPageSize, paginationOptions);
}
