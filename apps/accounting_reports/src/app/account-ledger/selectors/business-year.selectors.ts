import { createSelector } from '@ngrx/store';

import { BusinessYear, DateRange } from '../models';

import { AccountLedgerSelectors } from './account-ledger.selectors';

const inBusinessDateRange = (businessYears: BusinessYear[]) => {
    const dateRange: DateRange = { fromDate: null, toDate: null };

    dateRange.fromDate = Math.min.apply(null, businessYears.map(year => new Date(year.start)));
    dateRange.toDate = Math.max.apply(null, businessYears.map(year => new Date(year.end)));

    return dateRange;
};

export namespace BusinessYearsSelectors {
    export const getCurrentBusinessYear = createSelector(AccountLedgerSelectors.getBusinessYears, state => state.currentBusinessYear);

    export const getAllBusinessYears = createSelector(AccountLedgerSelectors.getBusinessYears, state => state.allBusinessYears);

    export const getAllBusinessYearsLoaded = createSelector(AccountLedgerSelectors.getBusinessYears, state => state.loadedAllBusinessYears);

    export const getCurrentBusinessYearLoaded = createSelector(
        AccountLedgerSelectors.getBusinessYears,
        state => state.loadedCurrentBusinessYear
    );

    export const getInBusinessDateRange = createSelector(getAllBusinessYears, inBusinessDateRange);
}
