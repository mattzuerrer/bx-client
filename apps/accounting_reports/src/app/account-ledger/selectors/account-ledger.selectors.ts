import { createFeatureSelector, createSelector } from '@ngrx/store';

import { featureName } from '../config';
import { State } from '../reducers';

export namespace AccountLedgerSelectors {
    export const getState = createFeatureSelector<State>(featureName);

    export const getFilterState = createSelector(getState, state => state.filter);

    export const getListState = createSelector(getState, state => state.list);

    export const getListPaginationState = createSelector(getState, state => state.listPagination);

    export const getOptionsState = createSelector(getState, state => state.options);

    export const getClientInfo = createSelector(getState, state => state.clientInfo);

    export const getBusinessYears = createSelector(getState, state => state.businessYears);
}
