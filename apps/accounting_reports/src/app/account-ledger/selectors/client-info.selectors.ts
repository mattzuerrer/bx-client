import { createSelector } from '@ngrx/store';

import { AccountLedgerSelectors } from './account-ledger.selectors';

export namespace ClientInfoSelectors {
    export const getClientInfo = createSelector(AccountLedgerSelectors.getClientInfo, state => state);

    export const getCurrency = createSelector(AccountLedgerSelectors.getClientInfo, state => state.currency);

    export const getHasVat = createSelector(AccountLedgerSelectors.getClientInfo, state => state.hasVat);
}
