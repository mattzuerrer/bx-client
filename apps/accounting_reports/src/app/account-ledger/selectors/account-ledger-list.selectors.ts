import { createSelector } from '@ngrx/store';

import { CurrencyTotal, EntriesTotals } from '../models';

import { AccountLedgerSelectors } from './account-ledger.selectors';

const getCurrenciesTotalsArray: (totals: EntriesTotals) => CurrencyTotal[] = (totals: EntriesTotals) => {
    return totals.currencies ? Object.keys(totals.currencies).map(key => ({ currency: key, amount: totals.currencies[key] })) : [];
};

export namespace AccountLedgerListSelectors {
    export const getEntries = createSelector(AccountLedgerSelectors.getListState, state => state.entries);

    export const getLoaded = createSelector(AccountLedgerSelectors.getListState, state => state.loaded);

    export const getLoading = createSelector(AccountLedgerSelectors.getListState, state => state.loading);

    export const getAccounts = createSelector(AccountLedgerSelectors.getListState, state => state.accounts);

    export const getCarryover = createSelector(AccountLedgerSelectors.getListState, state => state.carryOver);

    export const getSelectedAccountNo = createSelector(AccountLedgerSelectors.getListState, state => state.selectedAccountNo);

    export const getTotals = createSelector(AccountLedgerSelectors.getListState, state => state.totals);

    export const getCurrenciesTotals = createSelector(getTotals, getCurrenciesTotalsArray);

    export const getSort = createSelector(AccountLedgerSelectors.getListState, state => state.sort);

    export const getHasVatRecords = createSelector(AccountLedgerSelectors.getListState, state => state.hasVatRecords);

    export const getShowBalance = createSelector(AccountLedgerSelectors.getListState, state => state.showBalance);
}
