import { plainToClass } from 'class-transformer';

import { ClientInfo } from '../models';

export const clientInfo = plainToClass(ClientInfo, { currency: 'EUR', hasVat: true });
