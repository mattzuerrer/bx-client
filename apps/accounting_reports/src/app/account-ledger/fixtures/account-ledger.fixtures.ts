import { plainToClass } from 'class-transformer';

import { AccountLedgerCollection } from '../models';

export const accountLedgerCollection: AccountLedgerCollection = plainToClass(AccountLedgerCollection, {
    selectedAccountNumber: '0',
    aggregations: {
        accounts: [1000, 1020, 1021],
        totals: {
            debitAmount: 100000,
            creditAmount: 89123,
            balanceAmount: 234232,
            currencies: {
                EUR: 54200,
                CHF: 45800
            }
        },
        hasVatRecords: true
    },
    accountingRecords: [
        {
            id: 1,
            date: '2018-01-29',
            account: 1000,
            contraAccount: 1020,
            reference: { number: '1', name: 'Rechnung RE-000004-00072', link: 'http://www.bexio.com' },
            contact: 'Ackermann School Consulting GmbH',
            description: 'Test2',
            entryType: 'fake_type',
            tax: 'UN80',
            debitAmount: 0,
            creditAmount: 122,
            balance: 122,
            currency: 'CHF',
            currencyExchangeRate: 99.9,
            foreignCurrencyAmount: 1111.15,
            hasAttachement: true,
            attachments: [
                {
                    id: 1000,
                    extension: 'pdf',
                    mime_type: 'application/pdf',
                    name: 'Beleg-001',
                    link: 'http://office.bexio.bx/preview/previewFmFile/id/3'
                },
                {
                    id: 1001,
                    extension: 'pdf',
                    mime_type: 'application/pdf',
                    name: 'Beleg-002',
                    link: 'http://office.bexio.bx/preview/previewFmFile/id/4'
                }
            ]
        },
        {
            id: 2,
            date: '2018-01-29',
            account: 1100,
            contraAccount: 1000,
            reference: {
                number: '2',
                name: 'reference bexio some very very long reference',
                link: 'http://www.bexio.com'
            },
            contact: 'Jon Doe',
            description: 'Test2',
            entryType: 'fake_type',
            tax: null,
            debitAmount: 0,
            creditAmount: 122,
            balance: 122,
            currency: 'CHF',
            currencyExchangeRate: 101.1,
            foreignCurrencyAmount: 100.25,

            hasAttachement: true,
            attachments: [
                {
                    id: 1000,
                    extension: 'pdf',
                    mime_type: 'application/pdf',
                    name: 'Beleg-001',
                    link: 'http://office.bexio.bx/preview/previewFmFile/id/3'
                }
            ]
        },
        {
            id: 3,
            reference: { number: '3', name: 'Rechnung RE-000004-00003', link: 'http://www.bexio.com' },
            contact: 'Ackermann School Consulting GmbH',
            description: 'Some descriptive description',
            entryType: 'fake_type',
            tax: 'UN80',
            debitAmount: 0,
            creditAmount: 12000000.000005,
            balance: 12000000.000005,
            currency: 'CHF',
            currencyExchangeRate: 1.05,
            foreignCurrencyAmount: 12600000.0000058989898989,

            hasAttachement: false,
            attachments: []
        }
    ],
    carryOver: {
        date: '2017-01-01',
        account: 2154,
        type: 'temporary',
        debitAmount: 78800.5,
        creditAmount: 0,
        reference: {}
    },
    total: 3
});
