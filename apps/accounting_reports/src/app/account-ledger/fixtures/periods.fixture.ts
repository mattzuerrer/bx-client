import * as moment from 'moment';

export const dateFormatLocale = 'de-AT';
export const currentDay = '2019-04-01';

export const currentBusinessYear2019 = {
    id: 3,
    start: '2019-01-01',
    end: '2019-12-31'
};

export const currentBusinessYearInTheMiddleOf2019 = {
    id: 4,
    start: '2019-05-28',
    end: '2019-12-31'
};

export const currentBusinessYearAtTheEndOf2018 = {
    id: 5,
    start: '2018-10-15',
    end: '2018-12-31'
};

export const businessYear2020 = {
    id: 1,
    start: '2020-01-01',
    end: '2020-12-31'
};

export const businessYear2019 = {
    id: 2,
    start: '2019-01-01',
    end: '2019-12-31'
};

export const businessYear2018 = {
    id: 3,
    start: '2018-01-01',
    end: '2018-12-31'
};

export const businessYear2017 = {
    id: 4,
    start: '2017-01-01',
    end: '2017-12-31'
};

export const allBusinessYears = [{ ...businessYear2020 }, { ...businessYear2019 }, { ...businessYear2018 }, { ...businessYear2017 }];

export const selectToday = {
    start: new Date(currentDay),
    end: new Date(currentDay),
    viewValue: 'accounting_reports.filter.today',
    separator: true
};

export const selectQ3_2019 = {
    start: moment('2019-07-01')
        .startOf('quarter')
        .toDate(),
    end: moment('2019-09-30')
        .endOf('quarter')
        .toDate(),
    separator: false,
    viewValue: 'Q3-2019'
};

export const selectQ2_2019 = {
    start: moment('2019-04-01')
        .startOf('quarter')
        .toDate(),
    end: moment('2019-06-30')
        .endOf('quarter')
        .toDate(),
    separator: false,
    viewValue: 'Q2-2019'
};

export const selectQ1_2019 = {
    start: moment('2019-01-01')
        .startOf('quarter')
        .toDate(),
    end: moment('2019-03-31')
        .endOf('quarter')
        .toDate(),
    separator: false,
    viewValue: 'Q1-2019'
};

export const selectQ4_2018 = {
    start: moment('2018-10-01')
        .startOf('quarter')
        .toDate(),
    end: moment('2018-12-31')
        .endOf('quarter')
        .toDate(),
    separator: false,
    viewValue: 'Q4-2018'
};

export const selectBusinessYear2020 = {
    id: 1,
    start: new Date('2020-01-01'),
    end: new Date('2020-12-31'),
    viewValue: '01.01.2020 - 31.12.2020',
    isCurrent: false
};

export const selectBusinessYear2019 = {
    id: 2,
    start: new Date('2019-01-01'),
    end: new Date('2019-12-31'),
    viewValue: '01.01.2019 - 31.12.2019',
    isCurrent: false
};

export const selectBusinessYear2018 = {
    id: 3,
    start: new Date('2018-01-01'),
    end: new Date('2018-12-31'),
    viewValue: '01.01.2018 - 31.12.2018',
    isCurrent: false
};

export const selectBusinessYear2018Partial = {
    id: 5,
    start: new Date('2018-10-15'),
    end: new Date('2018-12-31'),
    viewValue: '15.10.2018 - 31.12.2018',
    isCurrent: false
};

export const selectBusinessYear2017 = {
    id: 4,
    start: new Date('2017-01-01'),
    end: new Date('2017-12-31'),
    viewValue: '01.01.2017 - 31.12.2017',
    isCurrent: false
};

export const allSelectPeriods = [
    { ...selectToday },
    { ...selectQ3_2019 },
    { ...selectQ2_2019 },
    { ...selectQ1_2019 },
    { ...selectQ4_2018, separator: true },
    { ...selectBusinessYear2020 },
    { ...selectBusinessYear2019, isCurrent: true },
    { ...selectBusinessYear2018 },
    { ...selectBusinessYear2017 }
];
