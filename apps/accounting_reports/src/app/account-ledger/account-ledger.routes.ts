import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AccountLedgerComponent } from './containers';
import { StatesLoadedGuard } from './guards';

const routes: Routes = [
    {
        path: '',
        canActivate: [StatesLoadedGuard],
        component: AccountLedgerComponent
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
