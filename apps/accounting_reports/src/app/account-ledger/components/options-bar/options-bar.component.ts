import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MatDialogConfig } from '@angular/material/typings/dialog';

import { ExcelDialogComponent, PdfDialogComponent } from '../../entry-components';
import { DESCRIPTION, ENTRY_TYPES, FROM_AMOUNT, FROM_DATE, REFERENCE_NO, TAX_ID, TO_AMOUNT, TO_DATE, VAT_DIGITS } from '../../models';

const DIALOG_CONFIG: MatDialogConfig = { width: '480px' };

@Component({
    selector: 'app-options-bar',
    templateUrl: './options-bar.component.html',
    styleUrls: ['./options-bar.component.scss']
})
export class OptionsBarComponent {
    @Input() isSearchActive = false;

    @Input() isFilterActive = false;

    @Input() chippableFilters: string[] = [];

    @Output() search: EventEmitter<string> = new EventEmitter<string>();

    @Output() cancelSingleFilter: EventEmitter<string> = new EventEmitter<string>();

    constructor(private matDialog: MatDialog) {}

    onPdfExport(): void {
        this.matDialog.open(PdfDialogComponent, DIALOG_CONFIG);
    }

    onExcelExport(): void {
        this.matDialog.open(ExcelDialogComponent, DIALOG_CONFIG);
    }

    onSearch(searchString: string): void {
        this.search.emit(searchString);
    }

    remove(filter: string): void {
        this.cancelSingleFilter.emit(filter);
    }

    getTranslationKey(field: string): string {
        switch (field) {
            case FROM_DATE:
                return 'accounting_reports.filter.date_from';
            case TO_DATE:
                return 'accounting_reports.filter.date_to';
            case FROM_AMOUNT:
                return 'accounting_reports.filter.amount_from';
            case TO_AMOUNT:
                return 'accounting_reports.filter.amount_to';
            case TAX_ID:
                return 'accounting_reports.account_ledger.vat_rate_filter_label';
            case VAT_DIGITS:
                return 'accounting_reports.account_ledger.vat_digit_filter_label';
            case ENTRY_TYPES:
                return 'accounting_reports.account_ledger.entry_type_filter_label';
            case REFERENCE_NO:
                return 'accounting_reports.filter.reference-no';
            case DESCRIPTION:
                return 'accounting_reports.filter.description';
        }
    }
}
