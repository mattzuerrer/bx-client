import { Component, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {
    ariaInvalidFn,
    ariaLiveFn,
    isFormGroupEmpty,
    makeAriaInvalidFn,
    makeAriaLiveFn,
    Validators as CustomValidators
} from '@bx-client/common';
import { IndagiaService } from '@bx-client/common/src/services';
import { AccountAutocompleteConfig } from '@bx-client/features/account';
import { Tax } from '@bx-client/features/tax';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

import { ACCOUNT_RANGE, AccountLedgerFilter, BusinessYear, ClientInfo, DATE_RANGE, DateRange, FROM_DATE, TO_DATE } from '../../models';
import { SelectPeriod } from '../../models/Periods';
import { LedgerAccount, LedgerAccountGroup } from '../../services/account';

const ENTER = 13;

@Component({
    selector: 'app-report-filter',
    templateUrl: './report-filter.component.html',
    styleUrls: ['./report-filter.component.scss']
})
export class ReportFilterComponent implements OnInit, OnDestroy {
    @Input()
    set accountsInput(accounts: LedgerAccount[]) {
        this.accounts = accounts;
        this.form.get('accountRange').setValidators([CustomValidators.accountRange(this.accounts)]);
    }

    @Input()
    set accountGroupsInput(accountsGroups: LedgerAccountGroup[]) {
        this.accountGroups = accountsGroups;
        this.accounts = this.combineAccountsAndGroups();
    }

    @Input() startYear: number = null;

    @Input() endYear: number = null;

    @Input() entryTypes: string[];

    @Input()
    set taxes(taxes: Tax[]) {
        const allTax = Object.assign(new Tax(), { id: '', name: this.allVatDisplayName });
        const noTax = Object.assign(new Tax(), { id: 'null', name: this.noVatDisplayName });
        // TODO: We have to show only taxes that belong to date range or already selected tax. This should be tackled with BX-7273
        // in validator we check again if date range is valid
        const filteredTaxes = taxes.filter(tax => this.isTaxYearRangeValid(tax) || this.getTaxIdField().value === tax.id);
        const sortVatDigits = (a: number, b: number): number => (a > b ? 1 : -1);

        this.filteredTaxes = [allTax, noTax, ...filteredTaxes];
        this.vatDigits = taxes
            .map(tax => tax.digit)
            .filter((val, ind, array) => val && array.indexOf(val) === ind)
            .sort(sortVatDigits);
    }

    @Input() accountGroupsLoaded = false;

    @Input() isFilterActive = false;

    @Input() clientInfo: ClientInfo;

    @Input() resetFilterField;

    @Input()
    set periods(periods: SelectPeriod[]) {
        const currentPeriod = periods.find(period => period.isCurrent);
        this.selectPeriods = periods;
        this.dateRangeSubform.controls.periods.setValue(currentPeriod);
    }

    @Input()
    set storedFilter(filter: any) {
        if (filter) {
            this.setFilterValues(filter);
            this.instantFilterForm.patchValue(filter);
        }
    }

    @Input()
    set advancedFiltersUsed(filtersUsed: string[]) {
        const carryOverCheckbox = this.instantFilterForm.get('includeCarryover');
        if (filtersUsed.length > 0) {
            carryOverCheckbox.patchValue(false);
            carryOverCheckbox.disable();
        } else {
            carryOverCheckbox.enable();
        }

        if (!this.form.valid) {
            carryOverCheckbox.disable();
        }
    }

    @Input()
    set currentBusinessYear(businessYear: BusinessYear) {
        this.businessYear = businessYear;
        if (!this.dateRangeSubform.controls.fromDate.value) {
            this.dateRangeSubform.controls.fromDate.setValue(moment(this.businessYear.start));
        }
        if (!this.dateRangeSubform.controls.toDate.value) {
            this.dateRangeSubform.controls.toDate.setValue(moment(this.businessYear.end));
        }
    }

    @Input()
    set inBusinessDateRange(dateRange: DateRange) {
        this.minDate = new Date(dateRange.fromDate);
        this.maxDate = new Date(dateRange.toDate);
    }

    @Output() applyFilter: EventEmitter<AccountLedgerFilter> = new EventEmitter<AccountLedgerFilter>();

    @Output() resetFilter: EventEmitter<null> = new EventEmitter<null>();

    @Output() instantFilter: EventEmitter<any> = new EventEmitter<any>();

    accounts: LedgerAccount[];

    accountGroups: LedgerAccountGroup[];

    businessYear: BusinessYear;

    decimalPlacesOptions = [0, 1, 2, 3, 4, 5, 6];

    isAriaInvalid: ariaInvalidFn;

    isAccountAriaInvalid: ariaInvalidFn;

    isDateAriaInvalid: ariaInvalidFn;

    ariaLive: ariaLiveFn;

    accountAriaLive: ariaLiveFn;

    dateAriaLive: ariaLiveFn;

    form: FormGroup;

    accountRangeSubform: FormGroup;

    dateRangeSubform: FormGroup;

    instantFilterForm: FormGroup;

    filteredTaxes: Tax[] = [];

    vatDigits: number[] = [];

    noVatDisplayName: string | any = this.translateService.instant('accounting_reports.account_ledger.vat_rate_filter_no_vat');

    allVatDisplayName: string | any = this.translateService.instant('accounting_reports.account_ledger.vat_rate_filter_all_vat');

    minDate: Date;

    maxDate: Date;

    preselectedVatDigits: number[];

    preselectedEntryTypes: string[];

    resetFieldSubscription: Subscription;

    datepickersSubscription: Subscription;

    accountAutocompleteConfig: AccountAutocompleteConfig = {
        inputAccountNo: true,
        inputAccountName: true,
        listAccountNo: true,
        listAccountName: true,
        listAccountAmount: false
    };

    selectPeriods: SelectPeriod[] = [];

    isIndagiaActive = false;

    constructor(
        private formBuilder: FormBuilder,
        private indagiaService: IndagiaService,
        private translateService: TranslateService
    ) {
        this.initForm();
        this.initInstantFilterForm();
    }

    ngOnInit(): void {
        this.resetFieldSubscription = this.resetFilterField.subscribe(field => this.form.patchValue(this.getResetField(field)));
        this.isAriaInvalid = makeAriaInvalidFn(this.form);
        this.ariaLive = makeAriaLiveFn(this.form);

        this.isAccountAriaInvalid = makeAriaInvalidFn(this.accountRangeSubform);
        this.accountAriaLive = makeAriaLiveFn(this.accountRangeSubform);

        this.isDateAriaInvalid = makeAriaInvalidFn(this.dateRangeSubform);
        this.dateAriaLive = makeAriaLiveFn(this.dateRangeSubform);

        this.handleIndagia();
        this.subscribeChangesInDatepickers();
    }

    ngOnDestroy(): void {
        this.resetFieldSubscription.unsubscribe();
        this.datepickersSubscription.unsubscribe();
    }

    onFilterSubmitButton(): void {
        this.onFilterSubmit(1);
    }

    onFilterSubmit(checkForNewEntriesOnBackend: number = 0): void {
        this.form.get('accountRange.fromAccount').markAsTouched();
        if (this.form.valid) {
            this.handleIndagia();
            let values = this.form.getRawValue();

            values = this.convertDateRange(values);
            values = this.convertAccountRange(values);

            if (values.fromAmount && values.toAmount) {
                values = this.fixAmountFields(values);
            }
            const filterValues = Object.assign({}, values, this.instantFilterForm.getRawValue(), { checkForNewEntriesOnBackend });

            this.applyFilter.emit(filterValues);
            this.form.markAsPristine();
            this.instantFilterForm.enable();
        }
    }

    onFilterReset(): void {
        this.form.reset({ dateRange: { fromDate: moment(this.businessYear.start), toDate: moment(this.businessYear.end) } });
        this.instantFilterForm.disable();
        this.resetFilter.emit();
    }

    selectAllAccounts(): void {
        if (this.accounts.length) {
            const fromAccount = this.accounts[0].number;
            const toAccount = this.accounts[this.accounts.length - 1].number;
            this.form.patchValue({ accountRange: { fromAccount, toAccount } });
            this.form.markAsDirty();
        }
    }

    onSelectAccount(account: LedgerAccount, formField: string): void {
        const accountId = null !== account ? account.id : null;
        this.accountRangeSubform.get(formField).patchValue(accountId);
        this.form.markAsDirty();
    }

    onPeriodChanges(period: SelectPeriod): void {
        this.dateRangeSubform.patchValue({
            fromDate: moment(period.start),
            toDate: moment(period.end)
        });
    }

    onInstantFilterValueChange(value: any, checkForNewEntriesOnBackend: number = 0): void {
        value = Object.assign({}, value, { checkForNewEntriesOnBackend });
        this.instantFilter.emit(value);
    }

    doSubmitFormOnReturn(event: KeyboardEvent): void {
        if (event.keyCode === ENTER) {
            const input = <HTMLInputElement>event.target;
            input.blur();
            input.focus();
            this.onFilterSubmitButton();
        }
    }

    comparePeriods(option1: SelectPeriod, option2: SelectPeriod): boolean {
        return option1 && option2 && moment(option1.start).isSame(option2.start) && moment(option1.end).isSame(option2.end);
    }

    isFormInDefaultState(): boolean {
        const dateRange = this.form.controls.dateRange.value;
        const isDateRangeInDefaultState =
            dateRange.fromDate &&
            dateRange.toDate &&
            dateRange.fromDate.isSame(moment().startOf('year'), 'day') &&
            dateRange.toDate.isSame(moment(), 'day');
        return isFormGroupEmpty(this.form, ['dateRange']) && isDateRangeInDefaultState;
    }

    @HostListener('document:keyup', ['$event'])
    submitForm(event: KeyboardEvent): void {
        this.doSubmitFormOnReturn(event);
    }

    datepickerRequiredAndEmpty(controlName: string): string {
        return this.dateRangeSubform.get(controlName).hasError('required') &&
        !this.dateRangeSubform.get(controlName).hasError('matDatepickerParse')
            ? 'assertive'
            : 'off';
    }

    private initForm(): void {
        this.accountRangeSubform = this.formBuilder.group({
            fromAccount: [null, [Validators.required]],
            toAccount: [null]
        });

        this.dateRangeSubform = this.formBuilder.group(
            {
                fromDate: new FormControl(null, [Validators.required]),
                toDate: new FormControl(null, [Validators.required]),
                periods: new FormControl(null, [Validators.nullValidator])
            },
            { validator: CustomValidators.dateRange }
        );

        this.form = new FormGroup(
            {
                accountRange: this.accountRangeSubform,
                taxId: new FormControl(null, [control => this.isValidTaxId(control)]),
                entryTypes: new FormControl(null),
                fromAmount: new FormControl(null),
                toAmount: new FormControl(null),
                dateRange: this.dateRangeSubform,
                vatDigits: new FormControl(null),
                referenceNo: new FormControl(null),
                description: new FormControl(null)
            },
            { updateOn: 'blur' }
        );
    }

    private initInstantFilterForm(): void {
        this.instantFilterForm = this.formBuilder.group({
            includeCarryover: [{ value: false, disabled: true }],
            showZeroAmountEntries: [{ value: false, disabled: true }],
            showOnlyAccountsWithTransactions: [{ value: true, disabled: true }],
            showAccountDescription: [{ value: false, disabled: true }],
            decimalPlaces: [{ value: 2, disabled: true }]
        });
    }

    private isValidTaxId(control: any): { [key: string]: any } {
        // TODO: Validate that tax is in date range, otherwise show error. This should be tackled with BX-7273
        return null;
    }

    private getTaxIdField(): FormControl {
        return <FormControl>this.form.get('taxId');
    }

    private getDateRangeField(): FormControl {
        return <FormControl>this.form.get('dateRange');
    }

    private getDecimalPlacesField(): FormControl {
        return <FormControl>this.instantFilterForm.get('decimalPlaces');
    }

    private isTaxYearRangeValid(tax: Tax): boolean {
        return this.isTaxStartYearValid(tax) && this.isTaxEndYearValid(tax);
    }

    private isTaxStartYearValid(tax: Tax): boolean {
        return !tax.startYear || !this.startYear || tax.startYear <= this.startYear;
    }

    private isTaxEndYearValid(tax: Tax): boolean {
        return !tax.endYear || !this.endYear || tax.endYear >= this.endYear;
    }

    private fixAmountFields(values: AccountLedgerFilter): AccountLedgerFilter {
        const fromAmount = values.fromAmount <= values.toAmount ? values.fromAmount : values.toAmount;
        const toAmount = values.fromAmount <= values.toAmount ? values.toAmount : values.fromAmount;
        return Object.assign(values, { fromAmount, toAmount });
    }

    private convertAccountRange(values: any): AccountLedgerFilter {
        const fromAccount = values.accountRange.fromAccount;
        const toAccount = values.accountRange.toAccount;
        delete values.accountRange;
        return Object.assign(values, { fromAccount, toAccount });
    }

    private convertDateRange(values: any): AccountLedgerFilter {
        const fromDate = values.dateRange.fromDate ? values.dateRange.fromDate.toDate() : null;
        const toDate = values.dateRange.toDate ? values.dateRange.toDate.toDate() : null;
        delete values.dateRange;
        return Object.assign({}, values, { fromDate, toDate });
    }

    private combineAccountsAndGroups(): LedgerAccount[] {
        const combinedAccounts = [];
        const _this = this;

        this.accounts.forEach(account => {
            account = Object.assign({}, account, { parentAccountGroups: _this.getFlattenedAccountGroups(account.parentAccountGroups) });
            combinedAccounts.push(account);
        });

        return combinedAccounts;
    }

    private getFlattenedAccountGroups(parentAccountGroup: number[]): number[] {
        if (parentAccountGroup.length < 1) {
            return [];
        }

        let parentId = parentAccountGroup[0];
        const parentAccountGroups = [];

        let hasParents = true;
        while (hasParents === true) {
            const parentGroup = this.accountGroups.filter(accountGroup => accountGroup.id === parentId);
            if (parentGroup.length > 0 && parentGroup[0].parentId > 0) {
                parentId = parentGroup[0].parentId;
                parentAccountGroups.push(parentGroup[0].number);
            } else {
                hasParents = false;
            }
        }

        return parentAccountGroups;
    }

    private getResetField(field: string): any {
        const resetField: any = {};
        const helperObject = {};
        switch (field) {
            case FROM_DATE:
                helperObject[field] = moment(this.businessYear.start);
                resetField.dateRange = helperObject;
                break;
            case TO_DATE:
                helperObject[field] = moment(this.businessYear.end);
                resetField.dateRange = helperObject;
                break;
            default:
                resetField[field] = null;
        }
        return resetField;
    }

    private setFilterValues(filterValues: AccountLedgerFilter): void {
        const filterValuesStructured = {};
        if (filterValues.fromDate && filterValues.toDate) {
            filterValuesStructured[DATE_RANGE] = { fromDate: moment(filterValues.fromDate), toDate: moment(filterValues.toDate) };

            this.refreshPeriodBySelectedDates(
                filterValuesStructured[DATE_RANGE].fromDate,
                filterValuesStructured[DATE_RANGE].toDate
            );
        }

        filterValuesStructured[ACCOUNT_RANGE] = { fromAccount: filterValues.fromAccount, toAccount: filterValues.toAccount };
        Object.keys(filterValues).forEach(controlName => {
            if (controlName !== DATE_RANGE && controlName !== ACCOUNT_RANGE) {
                filterValuesStructured[controlName] = filterValues[controlName];
            }
        });
        this.form.patchValue(filterValuesStructured);
        if (filterValues.vatDigits) {
            this.preselectedVatDigits = filterValues.vatDigits;
        }
        if (filterValues.entryTypes) {
            this.preselectedEntryTypes = filterValues.entryTypes;
        }
    }

    private handleIndagia(): void {
        this.isIndagiaActive = this.indagiaService.indagiaActivationDate(this.getDateRangeField().value.toDate);

        if (this.isIndagiaActive) {
            this.getDecimalPlacesField().patchValue(2);
        }
    }

    private subscribeChangesInDatepickers(): void {
        this.datepickersSubscription = this.getDateRangeField()
            .valueChanges
            .pipe(
                distinctUntilChanged((prev, curr) => this.distinctUntilDatepickersChanged(prev, curr))
            )
            .subscribe(dates => this.refreshPeriodBySelectedDates(dates.fromDate, dates.toDate));
    }

    private distinctUntilDatepickersChanged(prev: { [key: string]: moment.Moment }, curr: { [key: string]: moment.Moment }): boolean {
        return prev.fromDate.isSame(curr.fromDate) && prev.toDate.isSame(curr.toDate);
    }

    private refreshPeriodBySelectedDates(fromDate: moment.Moment, toDate: moment.Moment): void {
        const period = this.getPeriodBySelectedDates(fromDate, toDate);

        this.dateRangeSubform.controls.periods.setValue(period);
    }

    private getPeriodBySelectedDates(fromDate: moment.Moment, toDate: moment.Moment): SelectPeriod {
        for (const period of this.selectPeriods) {
            const isSame = fromDate.isSame(period.start, 'day') && toDate.isSame(period.end, 'day');

            if (isSame) {
                return period;
            }
        }

        return null;
    }
}
