import { animate, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { PageEvent, Sort } from '@angular/material';

import { AccountLedgerListDataSource } from '../../datasources';
import { AccountLedgerEntry, Carryover, CurrencyTotal, DateRange, EntriesTotals } from '../../models';
import { TooltipPlacement } from '../../models/Tooltip';
import { LedgerAccount } from '../../services/account/models';

@Component({
    selector: 'app-account-ledger-list',
    templateUrl: './account-ledger-list.component.html',
    styleUrls: ['./account-ledger-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('spinnerTextTrigger', [
            transition(':enter', [
                style({ opacity: 0 }),
                animate('500ms 3s ease-in-out', style({ opacity: 1 }))
            ])
        ])
    ]
})
export class AccountLedgerListComponent {
    baseColumns = ['attachment', 'date', 'contraAccount', 'reference', 'contact', 'description', 'vat', 'debit', 'credit', 'balance'];

    carryoverColumns = [
        'carryoverEmptyCell',
        'carryoverDate',
        'carryoverType',
        'carryoverReference',
        'carryoverDebit',
        'carryoverCredit',
        'carryoverEmptySpace'
    ];

    foreignCurrencyColumns = ['currency', 'currencyExchangeRate', 'foreignCurrencyAmount'];
    tooltipPlacement = TooltipPlacement;

    displayedColumns = [];

    totalBaseColumns = ['totalDate', 'totalDebit', 'totalCredit', 'totalBalance'];

    totalCurrenciesColumns = ['totalCurrency', 'totalAmount'];

    accountDescription = 'contraAccountDescription';

    totalDisplayedColumns = [];

    entriesList: AccountLedgerEntry[] = [];

    areAllEntriesInBaseCurrency = false;

    hasVat = true;

    @Input() listEntriesLoaded: boolean;

    @Input() dataSource: AccountLedgerListDataSource;

    @Input() listEntriesLoading: boolean;

    @Input() listAccounts: LedgerAccount[] = [];

    @Input() carryover: Carryover;

    @Input() listTotals: EntriesTotals;

    @Input() currenciesTotals: CurrencyTotal[];

    @Input() selectedAccount: string;

    @Input() listSort: Sort;

    @Input() listPaginationLength: number;

    @Input() listPaginationPageSize: number;

    @Input() listPaginationPageSizeOptions: number;

    @Input() listPaginationPageIndex: number;

    @Input() isFilterActive: boolean;

    @Input() filteredDateRange: DateRange;

    @Input() decimalPlaces: number;

    @Input() baseCurrency: string;

    @Input() dateFormat: string;

    @Input() isIncludeCarryoverChecked: boolean;

    @Input() showAccountDescriptionChecked: boolean;

    @Input() showBalance: boolean;

    @Input()
    set hasVatRecords(value: boolean) {
        this.hasVat = value;
        this.setColumnsToDisplay();
    }

    @Input()
    set entries(entries: AccountLedgerEntry[]) {
        this.entriesList = entries;
        this.setColumnsToDisplay();
    }

    @Output() selectAccount: EventEmitter<string> = new EventEmitter<string>();

    @Output() page: EventEmitter<PageEvent> = new EventEmitter<PageEvent>();

    @Output() sort: EventEmitter<Sort> = new EventEmitter<Sort>();

    get showListAccounts(): boolean {
        return this.isFilterActive && this.listAccounts.length > 0;
    }

    get hasListEntries(): boolean {
        return this.entriesList.length > 0;
    }

    get hasCarryover(): boolean {
        return this.carryover !== null && Object.keys(this.carryover).length !== 0;
    }

    get hasListEntriesOrCarryover(): boolean {
        return this.hasListEntries || this.hasCarryover;
    }

    get isOverlaySpinnerShown(): boolean {
        return this.listEntriesLoading && !this.listEntriesLoaded && this.hasListEntriesOrCarryover;
    }

    getListAccountIndex(account: string): number {
        return this.listAccounts.findIndex(listAcc => listAcc.number === account);
    }

    onSort(sortEvent: Sort): void {
        this.sort.emit(sortEvent);
    }

    onPagination(pageEvent: PageEvent): void {
        this.page.emit(pageEvent);
    }

    onSelectIndex(index: number): void {
        this.selectAccount.emit(this.getListAccount(index));
    }

    onOpenAttachment(link: string): void {
        window.open(link, '_blank');
    }

    doJournalRedirect(entry: AccountLedgerEntry): void {
        if ('collectionLink' in entry.reference) {
            window.open(entry.reference.collectionLink, '_blank');
        }
    }

    get numberFormat(): string {
        return `1.${this.decimalPlaces}-${this.decimalPlaces}`;
    }

    get carryOverTotalDateColSpan(): number {
        let cols = 6;

        [this.hasVat, this.showAccountDescriptionChecked].forEach(value => {
            if (value) {
                cols++;
            }
        });

        return cols;
    }

    get totalsForForeignCurrencies(): CurrencyTotal[] {
        return this.currenciesTotals.filter(total => total.currency !== this.baseCurrency);
    }

    private getListAccount(index: number): string {
        return this.listAccounts[index].number;
    }

    private isBaseCurrency(currency: string): boolean {
        return currency === null;
    }

    private hasOnlyBaseCurrency(): boolean {
        const notHasForeignCurrencyTotals = this.currenciesTotals.length < 1;
        const notHasEntriesWithForeignCurrency = this.entriesList.map(entry => entry.currency).every(currency => {
            return this.isBaseCurrency(currency);
        });

        return notHasEntriesWithForeignCurrency && notHasForeignCurrencyTotals;
    }

    private setColumnsToDisplay(): void {
        this.areAllEntriesInBaseCurrency = this.hasOnlyBaseCurrency();
        if (this.areAllEntriesInBaseCurrency) {
            this.displayedColumns = Object.assign([], this.baseColumns);
            this.totalDisplayedColumns = Object.assign([], this.totalBaseColumns);
        } else {
            this.displayedColumns = this.baseColumns.concat(this.foreignCurrencyColumns);
            this.totalDisplayedColumns = this.totalBaseColumns.concat(this.totalCurrenciesColumns);
        }
        if (!this.hasVat) {
            const vatColumnIndex = this.displayedColumns.indexOf('vat');
            if (vatColumnIndex !== -1) {
                this.displayedColumns.splice(vatColumnIndex, 1);
            }
        }
        if (this.showAccountDescriptionChecked) {
            this.displayedColumns.splice(3, 0, this.accountDescription);
        }
    }
}
