import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
    animations: [
        trigger('triggerAnimation', [
            state(
                'inactive',
                style({
                    width: '0',
                    border: 'none'
                })
            ),
            state(
                'active',
                style({
                    width: '150px',
                    border: '1px solid #00a1df'
                })
            ),
            transition('inactive => active', animate('240ms 0ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
            transition('active => inactive', animate('240ms 0ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
        ])
    ]
})
export class SearchComponent implements OnInit {
    @Input() isSearchActive = false;

    @Output() search: EventEmitter<string> = new EventEmitter<string>();

    openSearch = false;

    searchElement: HTMLInputElement;

    animationState = 'inactive';

    ngOnInit(): void {
        this.searchElement = <HTMLInputElement>document.getElementById('search-input');
    }

    toggleSearch(): void {
        this.openSearch = !this.openSearch;
        this.toggleAnimationState();
        if (!this.openSearch) {
            this.searchElement.blur();
        } else {
            this.searchElement.focus();
        }
    }

    toggleAnimationState(): void {
        this.animationState = this.animationState === 'active' ? 'inactive' : 'active';
    }

    checkOnBlur(value: string): void {
        if (this.openSearch && !(value.length > 0)) {
            this.toggleSearch();
        }
    }

    @HostListener('document: keydown.escape')
    close(): void {
        if (this.openSearch) {
            this.isSearchActive ? this.cancelSearch() : this.toggleSearch();
        }
    }

    @HostListener('document: keydown.enter')
    onSearch(): void {
        if (this.openSearch) {
            this.search.emit(this.searchElement.value);
        }
    }

    cancelSearch(): void {
        this.searchElement.value = '';
        if (this.openSearch) {
            this.toggleSearch();
        }
        this.search.emit('');
    }
}
