import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, take } from 'rxjs/operators';

import { AccountLedgerStateService } from '../services';

@Injectable()
export class StatesLoadedGuard implements CanActivate {
    constructor(private accountLedgerStateService: AccountLedgerStateService) {}

    canActivate(): Observable<boolean> {
        return this.accountLedgerStateService.statesLoaded$.pipe(filter(isLoaded => isLoaded), take(1));
    }
}
