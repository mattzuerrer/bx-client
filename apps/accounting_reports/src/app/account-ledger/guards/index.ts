import { StatesLoadedGuard } from './states-loaded.guard';

export { StatesLoadedGuard } from './states-loaded.guard';

export const appGuards = [StatesLoadedGuard];
