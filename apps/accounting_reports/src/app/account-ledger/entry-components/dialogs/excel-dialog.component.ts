import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

import { AccountLedgerExportActions } from '../../actions';
import { ExportType } from '../../config';
import { ExportOptions } from '../../models';
import { AccountLedgerStateService } from '../../services';

@Component({
    selector: 'app-excel-dialog',
    templateUrl: 'excel-dialog.component.html',
    styleUrls: ['excel-dialog.component.scss']
})
export class ExcelDialogComponent implements OnInit, OnDestroy {
    fromAccount: string;

    toAccount: string;

    exportOptions: ExportOptions = new ExportOptions();

    exportAllAccounts = false;

    form: FormGroup;

    selectedAccountsForExcel: string;

    isExportPending: boolean;

    private accountsArrangement = '';

    private subscriptions: Subscription[] = [];

    constructor(
        private dialogRef: MatDialogRef<ExcelDialogComponent>,
        private formBuilder: FormBuilder,
        private stateService: AccountLedgerStateService,
        private translateService: TranslateService
    ) {}

    ngOnInit(): void {
        this.stateService.dispatch(new AccountLedgerExportActions.InitExportOptionsAction());
        this.subscriptions.push(this.stateService.exportOptions$.subscribe(options => (this.exportOptions = options)));

        this.subscriptions.push(
            this.stateService.filterAccountNumbers$.subscribe(values => {
                this.fromAccount = values.fromAccount;
                this.toAccount = values.toAccount;

                if (!this.fromAccount) {
                    this.exportAllAccounts = true;
                } else {
                    this.accountsArrangement =
                        this.fromAccount && !this.toAccount ? this.fromAccount : `${this.fromAccount} - ${this.toAccount}`;
                }
            })
        );

        this.selectedAccountsForExcel = this.translateService.instant('accounting_reports.account_ledger.selected_accounts_for_excel', {
            accountRange: this.accountsArrangement
        });

        this.form = this.formBuilder.group({
            exportAllAccounts: [this.exportAllAccounts]
        });
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());

        this.dialogRef.afterClosed().subscribe(() => {
            if (this.isExportPending) {
                this.stateService.cancelRequest$.next();
            }
        });
    }

    waitForExcelFileDownload(): void {
        this.isExportPending = true;
        this.stateService.dispatch(
            new AccountLedgerExportActions.GenerateExportAction({
                exportType: ExportType.excel,
                exportOptions: this.form.getRawValue()
            })
        );

        this.subscriptions.push(
            this.stateService.exportGenerate$.subscribe(exportGenerate => {
                if (exportGenerate.status === 'ready') {
                    this.isExportPending = false;
                    this.dialogRef.close();
                }
            })
        );
    }
}
