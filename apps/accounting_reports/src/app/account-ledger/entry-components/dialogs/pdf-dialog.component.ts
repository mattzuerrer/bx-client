import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { Subscription } from 'rxjs';

import { AccountLedgerExportActions } from '../../actions';
import { ExportType } from '../../config';
import { ExportOptions } from '../../models';
import { AccountLedgerStateService } from '../../services';

@Component({
    selector: 'app-pdf-dialog',
    templateUrl: 'pdf-dialog.component.html',
    styleUrls: ['pdf-dialog.component.scss']
})
export class PdfDialogComponent implements OnInit, OnDestroy {
    fromAccount: string;

    toAccount: string;

    exportOptions: ExportOptions = new ExportOptions();

    exportAllAccounts = false;

    form: FormGroup;

    isExportPending: boolean;

    private subscriptions: Subscription[] = [];

    constructor(
        private dialogRef: MatDialogRef<PdfDialogComponent>,
        private formBuilder: FormBuilder,
        private stateService: AccountLedgerStateService
    ) {}

    ngOnInit(): void {
        this.stateService.dispatch(new AccountLedgerExportActions.InitExportOptionsAction());
        this.subscriptions.push(this.stateService.exportOptions$.subscribe(options => (this.exportOptions = options)));

        this.subscriptions.push(
            this.stateService.filterAccountNumbers$.subscribe(values => {
                this.fromAccount = values.fromAccount;
                this.toAccount = values.toAccount;

                if (!this.fromAccount) {
                    this.exportAllAccounts = true;
                }
            })
        );

        this.form = this.formBuilder.group({
            exportAllAccounts: [this.exportAllAccounts],
            paperOrientation: [this.exportOptions.paperOrientation],
            forcePageBreak: [this.exportOptions.forcePageBreak]
        });
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());

        this.dialogRef.afterClosed().subscribe(() => {
            if (this.isExportPending) {
                this.stateService.cancelRequest$.next();
            }
        });
    }

    waitForPdfFileDownload(): void {
        this.isExportPending = true;
        this.stateService.dispatch(
            new AccountLedgerExportActions.GenerateExportAction({
                exportType: ExportType.pdf,
                exportOptions: this.form.getRawValue()
            })
        );

        this.subscriptions.push(
            this.stateService.exportGenerate$.subscribe(exportGenerate => {
                if (exportGenerate.status === 'ready') {
                    this.isExportPending = false;
                    this.dialogRef.close();
                }
            })
        );
    }
}
