import { PdfDialogComponent } from './dialogs/pdf-dialog.component';

export { PdfDialogComponent } from './dialogs/pdf-dialog.component';

import { ExcelDialogComponent } from './dialogs/excel-dialog.component';

export { ExcelDialogComponent } from './dialogs/excel-dialog.component';

export const entryComponents = [PdfDialogComponent, ExcelDialogComponent];
