import { Injectable } from '@angular/core';
import { MessageActions } from '@bx-client/message';
import { EffectsService, PayloadAction } from '@bx-client/ngrx';
import { convertToISO8601String as dateToString } from '@bx-client/utils';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { BusinessYearsActions } from '../actions';
import { AccountLedgerApiService, AccountLedgerStateService } from '../services';

@Injectable()
export class BusinessYearsEffects extends EffectsService {
    @Effect()
    fetchCurrent$: Observable<any> = this.actions$.pipe(
        ofType<BusinessYearsActions.LoadCurrentAction>(BusinessYearsActions.LOAD_CURRENT),
        switchMap(() =>
            this.apiService
                .getBusinessYears(dateToString(new Date()))
                .pipe(
                    map(currentBusinessYear => new BusinessYearsActions.LoadCurrentSuccessAction(currentBusinessYear.businessYears[0])),
                    catchError(response => of(new BusinessYearsActions.LoadCurrentFailAction(response.status)))
                )
        )
    );

    @Effect()
    fetchAll$: Observable<any> = this.actions$.pipe(
        ofType<BusinessYearsActions.LoadAllAction>(BusinessYearsActions.LOAD_ALL),
        switchMap(() =>
            this.apiService
                .getBusinessYears()
                .pipe(
                    map(businessYears => new BusinessYearsActions.LoadAllSuccessAction(businessYears.businessYears)),
                    catchError(response => of(new BusinessYearsActions.LoadAllFailAction(response.status)))
                )
        )
    );

    @Effect()
    readonly loadFail$: Observable<any> = this.actions$.pipe(
        ofType<PayloadAction>(BusinessYearsActions.LOAD_CURRENT_FAIL, BusinessYearsActions.LOAD_ALL_FAIL),
        map(
            action =>
                new MessageActions.ErrorMessage({
                    messageKey: this.translateService.instant('accounting_reports.message.error_fetch_model', {
                        httpState: action.payload,
                        modelName: this.translateService.instant('accounting_reports.message.model_business_years')
                    })
                })
        )
    );

    initActions: Action[] = [new BusinessYearsActions.LoadCurrentAction(), new BusinessYearsActions.LoadAllAction()];

    constructor(
        private actions$: Actions,
        private apiService: AccountLedgerApiService,
        private translateService: TranslateService,
        accountLedgerStateService: AccountLedgerStateService
    ) {
        super(accountLedgerStateService);
    }
}
