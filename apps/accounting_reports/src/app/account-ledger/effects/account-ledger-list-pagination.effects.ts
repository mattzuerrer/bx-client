import { Inject, Injectable } from '@angular/core';
import { BxLocalStorageService, localStorageConfigToken } from '@bx-client/local-storage';
import { EffectsService } from '@bx-client/ngrx';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, tap, withLatestFrom } from 'rxjs/operators';

import { AccountLedgerListActions, AccountLedgerListPaginationActions } from '../actions';
import { defaultPaginationPageSize } from '../config';
import { AccountLedgerStateService } from '../services';

@Injectable()
export class AccountLedgerListPaginationEffects extends EffectsService {
    @Effect()
    change$: Observable<any> = this.actions$.pipe(
        ofType<AccountLedgerListPaginationActions.ChangeAction>(AccountLedgerListPaginationActions.CHANGE),
        tap(action => {
            this.localStorageService.set(this.localStorageConfig.keys.pageSize, action.payload.pageSize);
        }),
        withLatestFrom(this.accountLedgerStateService.selectedAccountNo$),
        map(([_, accountNo]) => new AccountLedgerListActions.LoadAction(accountNo))
    );

    @Effect()
    loadPageSize$: Observable<any> = this.actions$.pipe(
        ofType<AccountLedgerListPaginationActions.LoadPageSizeAction>(AccountLedgerListPaginationActions.LOAD_PAGE_SIZE),
        map(() => this.localStorageService.get(this.localStorageConfig.keys.pageSize)),
        map(
            pageSize =>
                new AccountLedgerListPaginationActions.LoadPageSizeSuccessAction(
                    <number>pageSize ? <number>pageSize : defaultPaginationPageSize
                )
        )
    );

    initActions: Action[] = [new AccountLedgerListPaginationActions.LoadPageSizeAction()];

    constructor(
        private actions$: Actions,
        private accountLedgerStateService: AccountLedgerStateService,
        private localStorageService: BxLocalStorageService,
        @Inject(localStorageConfigToken) private localStorageConfig: any
    ) {
        super(accountLedgerStateService);
    }
}
