import { Injectable } from '@angular/core';
import { Sort } from '@angular/material/sort';
import { MessageActions } from '@bx-client/message';
import { EffectsService } from '@bx-client/ngrx';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';

import { AccountLedgerListActions } from '../actions';
import { FilterMap } from '../selectors';
import { AccountLedgerApiService, AccountLedgerStateService } from '../services';

@Injectable()
export class AccountLedgerListEffects extends EffectsService {
    @Effect()
    readonly fetch$: Observable<any> = this.actions$.pipe(
        ofType<AccountLedgerListActions.LoadAction>(AccountLedgerListActions.LOAD),
        withLatestFrom(
            this.accountLedgerStateService.filters$,
            this.accountLedgerStateService.searchQuery$,
            this.accountLedgerStateService.listSort$,
            this.accountLedgerStateService.listPaginationOptions$,
            (action, filters, search, sort, options): [AccountLedgerListActions.LoadAction, FilterMap, string, Sort, any] =>
                /* Important: Clone the options object to avoid permanent mutation of its `param` property.
                 * This would cause previous search parameters to still exist in subsequent requests. */
                [action, filters, search, sort, { ...options }]
        ),
        switchMap(([action, filters, search, sort, options]) =>
            this.apiService.getAccountLedgerCollection(action.payload, filters, search, sort, options).pipe(
                map(
                    accountLedgerCollection => {
                        const accountNumber = action.payload ? action.payload : accountLedgerCollection.items.selectedAccountNumber;
                        return new AccountLedgerListActions.LoadSuccessAction({
                            collection: accountLedgerCollection,
                            selectedAccountNo: accountNumber
                        });
                    }
                ),
                catchError(response => of(new AccountLedgerListActions.LoadFailAction(response.status)))
            )
        )
    );

    @Effect()
    readonly selectAccount$: Observable<AccountLedgerListActions.LoadAction> = this.actions$.pipe(
        ofType<AccountLedgerListActions.SelectAccountAction>(AccountLedgerListActions.SELECT_ACCOUNT),
        map(action => new AccountLedgerListActions.LoadAction(action.payload))
    );

    @Effect()
    readonly sort$: Observable<AccountLedgerListActions.LoadAction> = this.actions$.pipe(
        ofType<AccountLedgerListActions.SortAction>(AccountLedgerListActions.SORT),
        withLatestFrom(this.accountLedgerStateService.selectedAccountNo$),
        map(([_, accountNo]) => new AccountLedgerListActions.LoadAction(accountNo))
    );

    @Effect()
    readonly listLoadFail$: Observable<any> = this.actions$.pipe(
        ofType<AccountLedgerListActions.LoadFailAction>(AccountLedgerListActions.LOAD_FAIL),
        map(
            action =>
                new MessageActions.ErrorMessage({
                    messageKey: this.translateService.instant('accounting_reports.message.error_list', { httpState: action.payload })
                })
        )
    );

    constructor(
        private actions$: Actions,
        private apiService: AccountLedgerApiService,
        private translateService: TranslateService,
        private accountLedgerStateService: AccountLedgerStateService
    ) {
        super(accountLedgerStateService);
    }
}
