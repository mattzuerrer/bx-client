import { Inject, Injectable } from '@angular/core';
import { TaxActions } from '@bx-client/features/src/tax';
import { BxLocalStorageService, localStorageConfigToken } from '@bx-client/local-storage';
import { MessageActions } from '@bx-client/message';
import { EffectsService } from '@bx-client/ngrx';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';

import { Observable } from 'rxjs';
import { map, tap, withLatestFrom } from 'rxjs/operators';

import { AccountLedgerFilterActions, AccountLedgerListActions } from '../actions';
import { DECIMAL_PLACES, INCLUDE_CARRYOVER, ZERO_AMOUNT_ENTRIES } from '../models';
import { AccountLedgerStateService } from '../services';

@Injectable()
export class AccountLedgerFilterEffects extends EffectsService {
    @Effect()
    filter$: Observable<any> = this.actions$.pipe(
        ofType<AccountLedgerFilterActions.FilterAction>(AccountLedgerFilterActions.FILTER),
        tap(action => {
            this.localStorageService.set(this.localStorageConfig.keys.filter, action.payload);
        }),
        map(() => new AccountLedgerListActions.LoadAction(null))
    );

    @Effect()
    instantFilter$: Observable<any> = this.actions$.pipe(
        ofType<AccountLedgerFilterActions.InstantFilterAction>(AccountLedgerFilterActions.INSTANT_FILTER),
        withLatestFrom(this.accountLedgerStateService.filterValues$),
        map(([_, filterState]) => new AccountLedgerFilterActions.FilterAction(filterState))
    );

    @Effect()
    search$: Observable<any> = this.actions$.pipe(
        ofType<AccountLedgerFilterActions.SearchAction>(AccountLedgerFilterActions.SEARCH),
        withLatestFrom(this.accountLedgerStateService.filterValues$),
        map(([, filterState]) => new AccountLedgerFilterActions.FilterAction(filterState))
    );

    @Effect()
    cancelSingleFilter$: Observable<any> = this.actions$.pipe(
        ofType<AccountLedgerFilterActions.CancelSingleFilterAction>(AccountLedgerFilterActions.CANCEL_SINGLE_FILTER),
        withLatestFrom(this.accountLedgerStateService.filterValues$),
        map(([_, filterState]) => new AccountLedgerFilterActions.FilterAction(filterState))
    );

    @Effect()
    readonly listGroupLoadFail$: Observable<any> = this.actions$.pipe(
        ofType<TaxActions.LoadFailAction>(TaxActions.LOAD_FAIL),
        map(
            action =>
                new MessageActions.ErrorMessage({
                    messageKey: this.translateService.instant('accounting_reports.message.error_fetch_model', {
                        httpState: action.payload,
                        modelName: this.translateService.instant('accounting_reports.message.model_taxes')
                    })
                })
        )
    );

    @Effect({ dispatch: false })
    readonly resetLocalStorage$: Observable<any> = this.actions$.pipe(
        ofType<AccountLedgerFilterActions.ResetFilterAction>(AccountLedgerFilterActions.RESET_FILTER),
        tap(_ => {
            const storedValues = this.localStorageService.get(this.localStorageConfig.keys.filter);
            const instantFilterValues = {};
            instantFilterValues[INCLUDE_CARRYOVER] = storedValues[INCLUDE_CARRYOVER];
            instantFilterValues[ZERO_AMOUNT_ENTRIES] = storedValues[ZERO_AMOUNT_ENTRIES];
            instantFilterValues[DECIMAL_PLACES] = storedValues[DECIMAL_PLACES];
            this.localStorageService.set(this.localStorageConfig.keys.filter, instantFilterValues);
        })
    );

    constructor(
        private actions$: Actions,
        private accountLedgerStateService: AccountLedgerStateService,
        private translateService: TranslateService,
        private localStorageService: BxLocalStorageService,
        @Inject(localStorageConfigToken) private localStorageConfig: any
    ) {
        super(accountLedgerStateService);
    }
}
