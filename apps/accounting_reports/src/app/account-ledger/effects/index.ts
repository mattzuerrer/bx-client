import { AccountLedgerExportEffects } from './account-ledger-export.effects';
import { AccountLedgerFilterEffects } from './account-ledger-filter.effects';
import { AccountLedgerListPaginationEffects } from './account-ledger-list-pagination.effects';
import { AccountLedgerListEffects } from './account-ledger-list.effects';
import { BusinessYearsEffects } from './business-years.effects';
import { ClientInfoEffects } from './client-info.effects';

export const effects = [
    AccountLedgerFilterEffects,
    AccountLedgerListEffects,
    AccountLedgerListPaginationEffects,
    AccountLedgerExportEffects,
    ClientInfoEffects,
    BusinessYearsEffects
];
