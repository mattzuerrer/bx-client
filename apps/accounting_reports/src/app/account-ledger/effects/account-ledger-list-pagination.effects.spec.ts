import { inject, TestBed } from '@angular/core/testing';
import { csnToken } from '@bx-client/env';
import { BxLocalStorageService, localStorageConfigToken } from '@bx-client/local-storage';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { noop, Observable, of } from 'rxjs';

import { AccountLedgerListActions, AccountLedgerListPaginationActions } from '../actions';
import { accountLedgerCollection } from '../fixtures';
import { AccountLedgerApiService, AccountLedgerStateService, LocalStorageConfig } from '../services';

import { AccountLedgerListPaginationEffects } from './account-ledger-list-pagination.effects';

const apiServiceStub = {
    getAccountLedgerCollection: () => noop()
};

const stateServiceStub = {
    selectedAccountNo$: of('1')
};

const accountLedgerCollectionPaginated = {
    items: accountLedgerCollection,
    length: 58,
    pageIndex: 0,
    pageSize: 50
};

const localStorageServiceStub = {
    get: () => noop(),
    set: () => noop()
};

describe('Account ledger list pagination effects', () => {
    let accountLedgerListPaginationEffects: AccountLedgerListPaginationEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<AccountLedgerListPaginationEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                AccountLedgerListPaginationEffects,
                {
                    provide: AccountLedgerApiService,
                    useValue: apiServiceStub
                },
                {
                    provide: AccountLedgerStateService,
                    useValue: stateServiceStub
                },
                {
                    provide: BxLocalStorageService,
                    useValue: localStorageServiceStub
                },
                {
                    provide: csnToken,
                    useValue: 'CSN'
                },
                {
                    provide: localStorageConfigToken,
                    useClass: LocalStorageConfig
                },
                provideMockActions(() => actions)
            ]
        });

        accountLedgerListPaginationEffects = TestBed.get(AccountLedgerListPaginationEffects);
        metadata = getEffectsMetadata(accountLedgerListPaginationEffects);
    });

    it(
        'should trigger load action when CHANGE is dispatched',
        inject([BxLocalStorageService, localStorageConfigToken], (localStorageService, localStorageConfig) => {
            actions = cold('a', {
                a: new AccountLedgerListPaginationActions.ChangeAction(accountLedgerCollectionPaginated)
            });
            spyOn(localStorageService, 'set');
            const expected = cold('b', { b: new AccountLedgerListActions.LoadAction('1') });
            expect(accountLedgerListPaginationEffects.change$).toBeObservable(expected);
            expect(localStorageService.set).toHaveBeenCalledTimes(1);
            expect(localStorageService.set).toHaveBeenCalledWith(
                localStorageConfig.keys.pageSize,
                accountLedgerCollectionPaginated.pageSize
            );
        })
    );

    it(
        'should trigger LoadPageSizeSuccessAction when LOAD_PAGE_SIZE is dispatched',
        inject([BxLocalStorageService], localStorageService => {
            actions = cold('a', {
                a: new AccountLedgerListPaginationActions.LoadPageSizeAction()
            });
            spyOn(localStorageService, 'get').and.returnValue(10);
            const expected = cold('b', { b: new AccountLedgerListPaginationActions.LoadPageSizeSuccessAction(10) });
            expect(accountLedgerListPaginationEffects.loadPageSize$).toBeObservable(expected);
        })
    );

    it('should test metadata', () => {
        expect(metadata.change$).toEqual({ dispatch: true });
        expect(metadata.loadPageSize$).toEqual({ dispatch: true });
    });
});
