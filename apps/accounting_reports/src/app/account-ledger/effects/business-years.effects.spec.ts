import { inject, TestBed } from '@angular/core/testing';
import { MessageActions } from '@bx-client/message';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { TranslateService } from '@ngx-translate/core';
import { cold } from 'jasmine-marbles';
import { noop, Observable, of, throwError } from 'rxjs';

import { BusinessYearsActions } from '../actions';
import { AccountLedgerApiService, AccountLedgerStateService } from '../services';

import { BusinessYearsEffects } from './business-years.effects';

const apiServiceStub = {
    getBusinessYears: () => noop()
};

const translateServiceStub = {
    instant: () => noop()
};

const stateServiceStub = {};

const businessYear = {
    id: 1,
    start: new Date('2018-01-01'),
    end: new Date('2018-12-31')
};
describe('Business years effects', () => {
    let businessYearsEffects: BusinessYearsEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<BusinessYearsEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                BusinessYearsEffects,
                {
                    provide: AccountLedgerApiService,
                    useValue: apiServiceStub
                },
                {
                    provide: TranslateService,
                    useValue: translateServiceStub
                },
                {
                    provide: AccountLedgerStateService,
                    useValue: stateServiceStub
                },
                provideMockActions(() => actions)
            ]
        });

        businessYearsEffects = TestBed.get(BusinessYearsEffects);
        metadata = getEffectsMetadata(businessYearsEffects);
    });

    it('should return LOAD_CURRENT_SUCCESS if getBusinessYears is successful', () => {
        inject([AccountLedgerApiService], apiService => {
            actions = cold('a', { a: new BusinessYearsActions.LoadCurrentAction() });
            spyOn(apiService, 'getBusinessYears').and.returnValue(of(businessYear));
            const expected = cold('b', {
                b: new BusinessYearsActions.LoadCurrentSuccessAction(businessYear)
            });
            spyOn(apiService, 'getBusinessYears').and.returnValue(of({ businessYears: [businessYear] }));
            expect(businessYearsEffects.fetchCurrent$).toBeObservable(expected);
            expect(apiService.getBusinessYears).toHaveBeenCalledTimes(1);
        });
    });

    it(
        'should return LOAD_CURRENT_FAIL if getBusinessYears fails',
        inject([AccountLedgerApiService], apiService => {
            const thrownError = { status: 404 };
            actions = cold('a', { a: new BusinessYearsActions.LoadCurrentAction() });
            spyOn(apiService, 'getBusinessYears').and.returnValue(throwError(thrownError));
            const expected = cold('b', {
                b: new BusinessYearsActions.LoadCurrentFailAction(thrownError.status)
            });
            expect(businessYearsEffects.fetchCurrent$).toBeObservable(expected);
            expect(apiService.getBusinessYears).toHaveBeenCalledTimes(1);
        })
    );

    it('should return LOAD_ALL_SUCCESS if getBusinessYears is successful', () => {
        inject([AccountLedgerApiService], apiService => {
            actions = cold('a', { a: new BusinessYearsActions.LoadAllAction() });
            spyOn(apiService, 'getBusinessYears').and.returnValue(of([businessYear]));
            const expected = cold('b', {
                b: new BusinessYearsActions.LoadAllSuccessAction([businessYear])
            });
            expect(businessYearsEffects.fetchAll$).toBeObservable(expected);
            expect(apiService.getBusinessYears).toHaveBeenCalledTimes(1);
        });
    });

    it(
        'should return LOAD_ALL_FAIL if getBusinessYears fails',
        inject([AccountLedgerApiService], apiService => {
            const thrownError = { status: 404 };
            actions = cold('a', { a: new BusinessYearsActions.LoadAllAction() });
            spyOn(apiService, 'getBusinessYears').and.returnValue(throwError(thrownError));
            const expected = cold('b', {
                b: new BusinessYearsActions.LoadAllFailAction(thrownError.status)
            });
            expect(businessYearsEffects.fetchAll$).toBeObservable(expected);
            expect(apiService.getBusinessYears).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return ERROR_MSG on LOAD_CURRENT_FAIL action',
        inject([TranslateService], translateService => {
            actions = cold('a', { a: new BusinessYearsActions.LoadCurrentFailAction(404) });
            spyOn(translateService, 'instant').and.returnValue('error');
            const payload = { messageKey: 'error' };
            const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
            expect(businessYearsEffects.loadFail$).toBeObservable(expected);
        })
    );

    it(
        'should return ERROR_MSG on LOAD_ALL_FAIL action',
        inject([TranslateService], translateService => {
            actions = cold('a', { a: new BusinessYearsActions.LoadAllFailAction(404) });
            spyOn(translateService, 'instant').and.returnValue('error');
            const payload = { messageKey: 'error' };
            const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
            expect(businessYearsEffects.loadFail$).toBeObservable(expected);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetchCurrent$).toEqual({ dispatch: true });
        expect(metadata.fetchAll$).toEqual({ dispatch: true });
    });
});
