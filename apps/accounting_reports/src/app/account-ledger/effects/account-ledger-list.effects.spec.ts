import { inject, TestBed } from '@angular/core/testing';
import { Sort } from '@angular/material/sort';
import { MessageActions } from '@bx-client/message';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { TranslateService } from '@ngx-translate/core';
import { cold } from 'jasmine-marbles';
import { noop, Observable, of, throwError } from 'rxjs';

import { AccountLedgerListActions } from '../actions';
import { accountLedgerCollection } from '../fixtures';
import { FilterMap } from '../selectors';
import { AccountLedgerApiService, AccountLedgerStateService } from '../services';

import { AccountLedgerListEffects } from './account-ledger-list.effects';

const accountNo = '1000';

const searchQuery = 'search query';

const sort: Sort = { active: 'random', direction: 'desc' };

const filters: FilterMap = new Map([['queryKey', 'queryValue']]);

const options = { option: 1 };

const translateServiceStub = {
    instant: () => noop()
};

const apiServiceStub = {
    getAccountLedgerCollection: () => noop()
};

const stateServiceStub = {
    filters$: of(filters),
    listSort$: of(sort),
    listPaginationOptions$: of(options),
    selectedAccountNo$: of(accountNo),
    searchQuery$: of(searchQuery)
};

const accountLedgerCollectionPaginated = {
    items: accountLedgerCollection,
    length: 58,
    pageIndex: 0,
    pageSize: 50
};

describe('Account ledger list effects', () => {
    let accountLedgerListEffects: AccountLedgerListEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<AccountLedgerListEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                AccountLedgerListEffects,
                {
                    provide: AccountLedgerApiService,
                    useValue: apiServiceStub
                },
                {
                    provide: TranslateService,
                    useValue: translateServiceStub
                },
                {
                    provide: AccountLedgerStateService,
                    useValue: stateServiceStub
                },
                provideMockActions(() => actions)
            ]
        });

        accountLedgerListEffects = TestBed.get(AccountLedgerListEffects);
        metadata = getEffectsMetadata(accountLedgerListEffects);
    });

    it(
        'should return LOAD_SUCCESS if LOAD is successful',
        inject([AccountLedgerApiService], apiService => {
            actions = cold('a', { a: new AccountLedgerListActions.LoadAction(accountNo) });
            spyOn(apiService, 'getAccountLedgerCollection').and.returnValue(of(accountLedgerCollectionPaginated));
            const expected = cold('b', {
                b: new AccountLedgerListActions.LoadSuccessAction({
                    collection: accountLedgerCollectionPaginated,
                    selectedAccountNo: accountNo
                })
            });
            expect(accountLedgerListEffects.fetch$).toBeObservable(expected);
            expect(apiService.getAccountLedgerCollection).toHaveBeenCalledTimes(1);
            expect(apiService.getAccountLedgerCollection).toHaveBeenCalledWith(accountNo, filters, searchQuery, sort, options);
        })
    );

    it(
        'should return LOAD_FAIL if LOAD fails',
        inject([AccountLedgerApiService], apiService => {
            const thrownError = { status: 404 };
            actions = cold('a', { a: new AccountLedgerListActions.LoadAction(accountNo) });
            spyOn(apiService, 'getAccountLedgerCollection').and.returnValue(throwError(thrownError));
            const expected = cold('b', { b: new AccountLedgerListActions.LoadFailAction(thrownError.status) });
            expect(accountLedgerListEffects.fetch$).toBeObservable(expected);
            expect(apiService.getAccountLedgerCollection).toHaveBeenCalledTimes(1);
            expect(apiService.getAccountLedgerCollection).toHaveBeenCalledWith(accountNo, filters, searchQuery, sort, options);
        })
    );

    it('should return LOAD on SELECT_ACCOUNT', () => {
        actions = cold('a', { a: new AccountLedgerListActions.SelectAccountAction(accountNo) });
        const expected = cold('b', { b: new AccountLedgerListActions.LoadAction(accountNo) });
        expect(accountLedgerListEffects.selectAccount$).toBeObservable(expected);
    });

    it('should return LOAD on SORT', () => {
        actions = cold('a', { a: new AccountLedgerListActions.SortAction(null) });
        const expected = cold('b', { b: new AccountLedgerListActions.LoadAction(accountNo) });
        expect(accountLedgerListEffects.sort$).toBeObservable(expected);
    });

    it(
        'should return ERROR_MSG on LOAD_FAIL action',
        inject([TranslateService], translateService => {
            actions = cold('a', { a: new AccountLedgerListActions.LoadFailAction(404) });
            spyOn(translateService, 'instant').and.returnValue('error');
            const payload = { messageKey: 'error' };
            const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
            expect(accountLedgerListEffects.listLoadFail$).toBeObservable(expected);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetch$).toEqual({ dispatch: true });
        expect(metadata.selectAccount$).toEqual({ dispatch: true });
        expect(metadata.sort$).toEqual({ dispatch: true });
        expect(metadata.listLoadFail$).toEqual({ dispatch: true });
    });
});
