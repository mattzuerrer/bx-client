import { inject, TestBed } from '@angular/core/testing';
import { csnToken } from '@bx-client/env';
import { TaxActions } from '@bx-client/features/src/tax';
import { BxLocalStorageService, localStorageConfigToken } from '@bx-client/local-storage';
import { MessageActions } from '@bx-client/message';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { TranslateService } from '@ngx-translate/core';
import { cold } from 'jasmine-marbles';
import { noop, Observable, of } from 'rxjs';

import { AccountLedgerFilterActions, AccountLedgerListActions } from '../actions';
import { AccountLedgerFilter } from '../models';
import { AccountLedgerApiService, AccountLedgerStateService, LocalStorageConfig } from '../services';

import { AccountLedgerFilterEffects } from './account-ledger-filter.effects';

const translateServiceStub = {
    instant: () => noop()
};

const apiServiceStub = {
    getAccountLedgerCollection: () => {
        noop();
    }
};

const localStorageServiceStub = {
    get: () => {
        noop();
    },
    set: () => {
        noop();
    }
};

const filterPayload: AccountLedgerFilter = {
    fromAccount: 1,
    toAccount: 2,
    taxId: 19,
    entryTypes: ['entry_type'],
    fromAmount: 1,
    toAmount: 9999,
    fromDate: new Date('10.10.2018'),
    toDate: new Date('11.11.2018'),
    vatDigits: [200],
    referenceNo: 'RE-1234',
    description: 'desc',
    includeCarryover: true,
    showZeroAmountEntries: false,
    showOnlyAccountsWithTransactions: false,
    decimalPlaces: 2,
    checkForNewEntriesOnBackend: 1,
    showAccountDescription: false
};

const stateServiceStub = {
    selectedAccountNo$: of('1'),
    filterValues$: of(filterPayload)
};

const instantFilterPayload = { decimalPlaces: 6 };

describe('Account ledger filter effects', () => {
    let accountLedgerFilterEffects: AccountLedgerFilterEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<AccountLedgerFilterEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                AccountLedgerFilterEffects,
                {
                    provide: AccountLedgerApiService,
                    useValue: apiServiceStub
                },
                {
                    provide: TranslateService,
                    useValue: translateServiceStub
                },
                {
                    provide: AccountLedgerStateService,
                    useValue: stateServiceStub
                },
                {
                    provide: BxLocalStorageService,
                    useValue: localStorageServiceStub
                },
                {
                    provide: csnToken,
                    useValue: 'CSN'
                },
                {
                    provide: localStorageConfigToken,
                    useClass: LocalStorageConfig
                },
                provideMockActions(() => actions)
            ]
        });

        accountLedgerFilterEffects = TestBed.get(AccountLedgerFilterEffects);
        metadata = getEffectsMetadata(accountLedgerFilterEffects);
    });

    it(
        'should trigger load action when FILTER is dispatched and includeCarryOver is false',
        inject([BxLocalStorageService, localStorageConfigToken], (localStorageService, localStorageConfig) => {
            const modifiedPayload = filterPayload;
            modifiedPayload.includeCarryover = false;

            actions = cold('a', {
                a: new AccountLedgerFilterActions.FilterAction(modifiedPayload)
            });
            spyOn(localStorageService, 'set');
            const expected = cold('b', { b: new AccountLedgerListActions.LoadAction(null) });
            expect(accountLedgerFilterEffects.filter$).toBeObservable(expected);
            expect(localStorageService.set).toHaveBeenCalledTimes(1);
            expect(localStorageService.set).toHaveBeenCalledWith(localStorageConfig.keys.filter, modifiedPayload);
        })
    );

    it('should trigger FILTER action when INSTANT_FILTER is dispatched', () => {
        actions = cold('a', {
            a: new AccountLedgerFilterActions.InstantFilterAction(instantFilterPayload)
        });
        const expected = cold('b', {
            b: new AccountLedgerFilterActions.FilterAction(filterPayload)
        });
        expect(accountLedgerFilterEffects.instantFilter$).toBeObservable(expected);
    });

    it('should trigger FILTER action when SEARCH is dispatched', () => {
        actions = cold('a', {
            a: new AccountLedgerFilterActions.SearchAction('payload')
        });
        const expected = cold('b', {
            b: new AccountLedgerFilterActions.FilterAction(filterPayload)
        });
        expect(accountLedgerFilterEffects.search$).toBeObservable(expected);
    });

    it(
        'should return ERROR_MSG on LOAD_FAIL action',
        inject([TranslateService], translateService => {
            actions = cold('a', { a: new TaxActions.LoadFailAction(404) });
            spyOn(translateService, 'instant').and.returnValue('error');
            const payload = { messageKey: 'error' };
            const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
            expect(accountLedgerFilterEffects.listGroupLoadFail$).toBeObservable(expected);
        })
    );

    it('should test metadata', () => {
        expect(metadata.filter$).toEqual({ dispatch: true });
        expect(metadata.instantFilter$).toEqual({ dispatch: true });
        expect(metadata.search$).toEqual({ dispatch: true });
        expect(metadata.listGroupLoadFail$).toEqual({ dispatch: true });
    });
});
