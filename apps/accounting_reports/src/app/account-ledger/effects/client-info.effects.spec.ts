import { inject, TestBed } from '@angular/core/testing';
import { MessageActions } from '@bx-client/message';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { TranslateService } from '@ngx-translate/core';
import { cold } from 'jasmine-marbles';
import { noop, Observable, of, throwError } from 'rxjs';

import { ClientInfoActions } from '../actions';
import { AccountLedgerApiService, AccountLedgerStateService } from '../services';

import { ClientInfoEffects } from './client-info.effects';

const apiServiceStub = {
    getAccountLedgerCollection: () => noop(),
    getClientInfo: () => noop()
};

const translateServiceStub = {
    instant: () => noop()
};

const stateServiceStub = {};

describe('Client info effects', () => {
    let baseCurrencyEffects: ClientInfoEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<ClientInfoEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                ClientInfoEffects,
                {
                    provide: AccountLedgerApiService,
                    useValue: apiServiceStub
                },
                {
                    provide: TranslateService,
                    useValue: translateServiceStub
                },
                {
                    provide: AccountLedgerStateService,
                    useValue: stateServiceStub
                },
                provideMockActions(() => actions)
            ]
        });

        baseCurrencyEffects = TestBed.get(ClientInfoEffects);
        metadata = getEffectsMetadata(baseCurrencyEffects);
    });

    it(
        'should return LOAD_SUCCESS on LOAD if getClientInfo is successful',
        inject([AccountLedgerApiService], apiService => {
            actions = cold('a', { a: new ClientInfoActions.LoadAction() });
            const expected = cold('b', { b: new ClientInfoActions.LoadSuccessAction({ currency: 'CHF', hasVat: true }) });
            spyOn(apiService, 'getClientInfo').and.returnValue(of({ currency: 'CHF', hasVat: true }));
            expect(baseCurrencyEffects.fetch$).toBeObservable(expected);
            expect(apiService.getClientInfo).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_FAIL on LOAD if getClientInfo fails',
        inject([AccountLedgerApiService], apiService => {
            const thrownError = { status: 404 };
            actions = cold('a', { a: new ClientInfoActions.LoadAction() });
            spyOn(apiService, 'getClientInfo').and.returnValue(throwError(thrownError));
            const expected = cold('b', { b: new ClientInfoActions.LoadFailAction(thrownError.status) });
            expect(baseCurrencyEffects.fetch$).toBeObservable(expected);
            expect(apiService.getClientInfo).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return ERROR_MSG on LOAD_ALL_FAIL action',
        inject([TranslateService], translateService => {
            actions = cold('a', { a: new ClientInfoActions.LoadFailAction(404) });
            spyOn(translateService, 'instant').and.returnValue('error');
            const payload = { messageKey: 'error' };
            const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
            expect(baseCurrencyEffects.listLoadFail$).toBeObservable(expected);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetch$).toEqual({ dispatch: true });
        expect(metadata.listLoadFail$).toEqual({ dispatch: true });
    });
});
