import { Injectable } from '@angular/core';
import { MessageActions } from '@bx-client/message';
import { EffectsService } from '@bx-client/ngrx';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { catchError, map, mapTo, switchMap, takeUntil, withLatestFrom } from 'rxjs/operators';

import { AccountLedgerExportActions } from '../actions';
import { AccountLedgerApiService, AccountLedgerSaveFileService, AccountLedgerStateService } from '../services';

@Injectable()
export class AccountLedgerExportEffects extends EffectsService {
    @Effect()
    readonly generateExport$: Observable<any> = this.actions$.pipe(
        ofType<AccountLedgerExportActions.GenerateExportAction>(AccountLedgerExportActions.GENERATE_EXPORT),
        withLatestFrom(this.accountLedgerStateService.filters$, this.accountLedgerStateService.listSort$),
        switchMap(([action, filters, sort]) =>
            this.apiService
                .getExportGenerate(action.payload.exportType, action.payload.exportOptions, filters, sort)
                .pipe(
                    takeUntil(this.accountLedgerStateService.cancelRequest$),
                    map(payload => new AccountLedgerExportActions.GenerateExportSuccessAction(payload.fileId)),
                    catchError(response => of(new AccountLedgerExportActions.GenerateExportFailAction(response.status)))
                )
        )
    );

    @Effect()
    readonly generateExportFail$: Observable<any> = this.actions$.pipe(
        ofType<AccountLedgerExportActions.GenerateExportFailAction>(AccountLedgerExportActions.GENERATE_EXPORT_FAIL),
        map(
            action =>
                new MessageActions.ErrorMessage({
                    messageKey: this.translateService.instant('accounting_reports.message.error_export', {
                        httpState: action.payload
                    })
                })
        )
    );

    @Effect()
    readonly generateExportStatus$: Observable<any> = this.actions$.pipe(
        ofType<AccountLedgerExportActions.GenerateExportSuccessAction>(AccountLedgerExportActions.GENERATE_EXPORT_SUCCESS),
        withLatestFrom(this.accountLedgerStateService.exportGenerate$),
        switchMap(([_, generateExport]) =>
            this.apiService
                .getExportGenerateStatus(generateExport.fileId)
                .pipe(
                    takeUntil(this.accountLedgerStateService.cancelRequest$),
                    map(payload => new AccountLedgerExportActions.GenerateExportStatusSuccessAction(payload.status)),
                    catchError(response => of(new AccountLedgerExportActions.GenerateExportStatusFailAction(response.status)))
                )
        )
    );

    @Effect()
    readonly generateExportFile$: Observable<any> = this.actions$.pipe(
        ofType<AccountLedgerExportActions.GenerateExportStatusSuccessAction>(AccountLedgerExportActions.GENERATE_EXPORT_STATUS_SUCCESS),
        withLatestFrom(
            this.accountLedgerStateService.exportGenerate$,
            this.accountLedgerStateService.exportType$,
            this.accountLedgerStateService.exportOptions$,
            this.accountLedgerStateService.filterAccountNumbers$
        ),
        switchMap(([_, generateExport, exportType, exportOptions, filterAccountNumbers]) =>
            this.apiService.getGenerateExportFile(generateExport.fileId).pipe(
                map(
                    file =>
                        new AccountLedgerExportActions.GenerateExportFileSuccessAction({
                            file,
                            exportType,
                            exportOptions,
                            filterAccountNumbers
                        })
                ),
                catchError(response => of(new AccountLedgerExportActions.GenerateExportFileFailAction(response.status)))
            )
        )
    );

    @Effect()
    readonly saveFile$: Observable<any> = this.actions$.pipe(
        ofType<AccountLedgerExportActions.GenerateExportFileSuccessAction>(AccountLedgerExportActions.GENERATE_EXPORT_FILE_SUCCESS),
        switchMap(action =>
            this.accountLedgerSaveFileService
                .saveFile(
                    action.payload.file,
                    action.payload.exportType,
                    action.payload.exportOptions.exportAllAccounts,
                    action.payload.filterAccountNumbers
                )
                .pipe(
                    mapTo(new AccountLedgerExportActions.SaveFileSuccessAction()),
                    catchError(response => of(new AccountLedgerExportActions.SaveFileFailAction(response)))
                )
        )
    );

    @Effect()
    readonly generateExportStatusFail$: Observable<any> = this.actions$.pipe(
        ofType<AccountLedgerExportActions.GenerateExportStatusFailAction>(AccountLedgerExportActions.GENERATE_EXPORT_STATUS_FAIL),
        map(
            action =>
                new MessageActions.ErrorMessage({
                    messageKey: this.translateService.instant('accounting_reports.message.error_export', {
                        httpState: action.payload
                    })
                })
        )
    );

    constructor(
        private actions$: Actions,
        private apiService: AccountLedgerApiService,
        private accountLedgerStateService: AccountLedgerStateService,
        private accountLedgerSaveFileService: AccountLedgerSaveFileService,
        private translateService: TranslateService
    ) {
        super(accountLedgerStateService);
    }
}
