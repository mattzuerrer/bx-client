import { inject, TestBed } from '@angular/core/testing';
import { Sort } from '@angular/material/sort';
import { MessageActions } from '@bx-client/message';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { TranslateService } from '@ngx-translate/core';
import { cold } from 'jasmine-marbles';
import { EMPTY, noop, Observable, of, throwError } from 'rxjs';

import { AccountLedgerExportActions } from '../actions';
import { ExportType } from '../config';
import { AccountNumberRange, BusinessYear, GenerateExportStatus } from '../models';
import { FilterMap } from '../selectors';
import { AccountLedgerApiService, AccountLedgerSaveFileService, AccountLedgerStateService } from '../services';

import { AccountLedgerExportEffects } from './account-ledger-export.effects';

const apiServiceStub = {
    getExportGenerate: () => noop(),
    getExportGenerateStatus: () => noop(),
    getGenerateExportFile: () => noop()
};

const translateServiceStub = {
    instant: () => noop()
};

const saveFileServiceStub = {
    saveFile: () => of(true)
};

const sort: Sort = { active: 'random', direction: 'desc' };
const filters: FilterMap = new Map([['queryKey', 'queryValue']]);
const exportGenerate: GenerateExportStatus = { fileId: '1', status: 'ready' };
const currentBusinessYear: BusinessYear = { id: 1, start: new Date('2018-04-01'), end: new Date('2019-03-31') };
const accountNumbers: AccountNumberRange = { fromAccount: '1000', toAccount: '2000' };
const exportPayload = {
    exportType: ExportType.pdf,
    exportOptions: { exportAllAccounts: true, paperOrientation: 'landscape', forcePageBreak: true }
};
const showOnlyAccountsWithTransactions = true;
const thrownError = { status: 404 };
const thrownTypeError = 'Wrong entry type';

const stateServiceStub = {
    filters$: of(filters),
    listSort$: of(sort),
    exportGenerate$: of(exportGenerate),
    currentBusinessYear$: of(currentBusinessYear),
    showOnlyAccountsWithTransactions$: of(showOnlyAccountsWithTransactions),
    cancelRequest$: EMPTY,
    exportType$: of(exportPayload.exportType),
    exportOptions$: of(exportPayload.exportOptions),
    filterAccountNumbers$: of(accountNumbers)
};

describe('Account ledger pdf effects', () => {
    let accountLedgerExportEffects: AccountLedgerExportEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<AccountLedgerExportEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                AccountLedgerExportEffects,
                {
                    provide: AccountLedgerApiService,
                    useValue: apiServiceStub
                },
                {
                    provide: TranslateService,
                    useValue: translateServiceStub
                },
                {
                    provide: AccountLedgerStateService,
                    useValue: stateServiceStub
                },
                {
                    provide: AccountLedgerSaveFileService,
                    useValue: saveFileServiceStub
                },
                provideMockActions(() => actions)
            ]
        });

        accountLedgerExportEffects = TestBed.get(AccountLedgerExportEffects);
        metadata = getEffectsMetadata(accountLedgerExportEffects);
    });

    it(
        'should return GENERATE_PDF_SUCCESS if GENERATE_PDF is successful',
        inject([AccountLedgerApiService], apiService => {
            actions = cold('a', { a: new AccountLedgerExportActions.GenerateExportAction(exportPayload) });
            spyOn(apiService, 'getExportGenerate').and.returnValue(of(exportGenerate));
            const expected = cold('b', {
                b: new AccountLedgerExportActions.GenerateExportSuccessAction(exportGenerate.fileId)
            });
            expect(accountLedgerExportEffects.generateExport$).toBeObservable(expected);
            expect(apiService.getExportGenerate).toHaveBeenCalledTimes(1);
            expect(apiService.getExportGenerate).toHaveBeenCalledWith(exportPayload.exportType, exportPayload.exportOptions, filters, sort);
        })
    );

    it(
        'should return GENERATE_EXPORT_FAIL if GENERATE_EXPORT fails',
        inject([AccountLedgerApiService], apiService => {
            actions = cold('a', { a: new AccountLedgerExportActions.GenerateExportAction(exportPayload) });
            spyOn(apiService, 'getExportGenerate').and.returnValue(throwError(thrownError));
            const expected = cold('b', {
                b: new AccountLedgerExportActions.GenerateExportFailAction(thrownError.status)
            });
            expect(accountLedgerExportEffects.generateExport$).toBeObservable(expected);
            expect(apiService.getExportGenerate).toHaveBeenCalledTimes(1);
            expect(apiService.getExportGenerate).toHaveBeenCalledWith(exportPayload.exportType, exportPayload.exportOptions, filters, sort);
        })
    );

    it(
        'should return GENERATE_EXPORT_STATUS_SUCCESS if GENERATE_EXPORT is successful',
        inject([AccountLedgerApiService], apiService => {
            actions = cold('a', {
                a: new AccountLedgerExportActions.GenerateExportSuccessAction(exportGenerate.fileId)
            });
            spyOn(apiService, 'getExportGenerateStatus').and.returnValue(of(exportGenerate));
            const expected = cold('b', {
                b: new AccountLedgerExportActions.GenerateExportStatusSuccessAction(exportGenerate.status)
            });
            expect(accountLedgerExportEffects.generateExportStatus$).toBeObservable(expected);
            expect(apiService.getExportGenerateStatus).toHaveBeenCalledTimes(1);
            expect(apiService.getExportGenerateStatus).toHaveBeenCalledWith(exportGenerate.fileId);
        })
    );

    it(
        'should return GENERATE_EXPORT_STATUS_FAIL if GENERATE_EXPORT fails',
        inject([AccountLedgerApiService], apiService => {
            actions = cold('a', {
                a: new AccountLedgerExportActions.GenerateExportSuccessAction(exportGenerate.fileId)
            });
            spyOn(apiService, 'getExportGenerateStatus').and.returnValue(throwError(thrownError));
            const expected = cold('b', {
                b: new AccountLedgerExportActions.GenerateExportStatusFailAction(thrownError.status)
            });
            expect(accountLedgerExportEffects.generateExportStatus$).toBeObservable(expected);
            expect(apiService.getExportGenerateStatus).toHaveBeenCalledTimes(1);
            expect(apiService.getExportGenerateStatus).toHaveBeenCalledWith(exportGenerate.fileId);
        })
    );

    it(
        'should return GENERATE_EXPORT_FILE_SUCCESS if GENERATE_EXPORT_STATUS is successful',
        inject([AccountLedgerApiService], apiService => {
            actions = cold('a', {
                a: new AccountLedgerExportActions.GenerateExportStatusSuccessAction(exportGenerate.fileId)
            });
            spyOn(apiService, 'getGenerateExportFile').and.returnValue(of(new Blob()));
            const expected = cold('b', {
                b: new AccountLedgerExportActions.GenerateExportFileSuccessAction({
                    file: new Blob(),
                    exportType: exportPayload.exportType,
                    exportOptions: exportPayload.exportOptions,
                    filterAccountNumbers: accountNumbers
                })
            });
            expect(accountLedgerExportEffects.generateExportFile$).toBeObservable(expected);
            expect(apiService.getGenerateExportFile).toHaveBeenCalledTimes(1);
            expect(apiService.getGenerateExportFile).toHaveBeenCalledWith(exportGenerate.fileId);
        })
    );

    it(
        'should return GENERATE_EXPORT_FILE_FAIL if GENERATE_EXPORT_STATUS fails',
        inject([AccountLedgerApiService], apiService => {
            actions = cold('a', {
                a: new AccountLedgerExportActions.GenerateExportStatusSuccessAction(exportGenerate.fileId)
            });
            spyOn(apiService, 'getGenerateExportFile').and.returnValue(throwError(thrownError));
            const expected = cold('b', {
                b: new AccountLedgerExportActions.GenerateExportFileFailAction(thrownError.status)
            });
            expect(accountLedgerExportEffects.generateExportFile$).toBeObservable(expected);
            expect(apiService.getGenerateExportFile).toHaveBeenCalledTimes(1);
            expect(apiService.getGenerateExportFile).toHaveBeenCalledWith(exportGenerate.fileId);
        })
    );

    it(
        'should return SAVE_FILE_SUCCESS if GENERATE_EXPORT_FILE is successful',
        inject([AccountLedgerSaveFileService], saveFileService => {
            actions = cold('a', {
                a: new AccountLedgerExportActions.GenerateExportFileSuccessAction({
                    file: new Blob(),
                    exportType: exportPayload.exportType,
                    exportOptions: exportPayload.exportOptions,
                    filterAccountNumbers: accountNumbers
                })
            });
            spyOn(saveFileService, 'saveFile').and.callThrough();
            const expected = cold('b', {
                b: new AccountLedgerExportActions.SaveFileSuccessAction()
            });
            expect(accountLedgerExportEffects.saveFile$).toBeObservable(expected);
            expect(saveFileService.saveFile).toHaveBeenCalledTimes(1);
            expect(saveFileService.saveFile).toHaveBeenCalledWith(
                new Blob(),
                exportPayload.exportType,
                exportPayload.exportOptions.exportAllAccounts,
                accountNumbers
            );
        })
    );

    it(
        'should return SAVE_FILE_SUCCESS if GENERATE_EXPORT_FILE is successful with no to account set',
        inject([AccountLedgerSaveFileService], saveFileService => {
            const accountNumbersMissingToAccount: AccountNumberRange = { fromAccount: '1000', toAccount: null };
            actions = cold('a', {
                a: new AccountLedgerExportActions.GenerateExportFileSuccessAction({
                    file: new Blob(),
                    exportType: exportPayload.exportType,
                    exportOptions: exportPayload.exportOptions,
                    filterAccountNumbers: accountNumbersMissingToAccount
                })
            });
            spyOn(saveFileService, 'saveFile').and.callThrough();
            const expected = cold('b', {
                b: new AccountLedgerExportActions.SaveFileSuccessAction()
            });
            expect(accountLedgerExportEffects.saveFile$).toBeObservable(expected);
            expect(saveFileService.saveFile).toHaveBeenCalledTimes(1);
            expect(saveFileService.saveFile).toHaveBeenCalledWith(
                new Blob(),
                exportPayload.exportType,
                exportPayload.exportOptions.exportAllAccounts,
                accountNumbersMissingToAccount
            );
        })
    );

    it(
        'should return SAVE_FILE_FAIL if GENERATE_EXPORT_FILE fails',
        inject([AccountLedgerSaveFileService], saveFileService => {
            actions = cold('a', {
                a: new AccountLedgerExportActions.GenerateExportFileSuccessAction({
                    file: new Blob(),
                    exportType: exportPayload.exportType,
                    exportOptions: exportPayload.exportOptions,
                    filterAccountNumbers: accountNumbers
                })
            });
            spyOn(saveFileService, 'saveFile').and.returnValue(throwError(thrownTypeError));
            const expected = cold('b', {
                b: new AccountLedgerExportActions.SaveFileFailAction(thrownTypeError)
            });
            expect(accountLedgerExportEffects.saveFile$).toBeObservable(expected);
            expect(saveFileService.saveFile).toHaveBeenCalledTimes(1);
            expect(saveFileService.saveFile).toHaveBeenCalledWith(
                new Blob(),
                exportPayload.exportType,
                exportPayload.exportOptions.exportAllAccounts,
                accountNumbers
            );
        })
    );

    it(
        'should return ERROR_MSG on GENERATE_PDF_FAIL action',
        inject([TranslateService], translateService => {
            const errorMessage = 'error ' + thrownError.status;

            spyOn(translateService, 'instant').and.returnValue(errorMessage);
            actions = cold('a', { a: new AccountLedgerExportActions.GenerateExportFailAction(thrownError.status) });
            const expected = cold('b', { b: new MessageActions.ErrorMessage({ messageKey: errorMessage }) });

            expect(accountLedgerExportEffects.generateExportFail$).toBeObservable(expected);
        })
    );

    it(
        'should return ERROR_MSG on GENERATE_PDF_STATUS_FAIL action',
        inject([TranslateService], translateService => {
            const errorMessage = 'error ' + thrownError.status;

            spyOn(translateService, 'instant').and.returnValue(errorMessage);
            actions = cold('a', { a: new AccountLedgerExportActions.GenerateExportStatusFailAction(thrownError.status) });
            const expected = cold('b', { b: new MessageActions.ErrorMessage({ messageKey: errorMessage }) });

            expect(accountLedgerExportEffects.generateExportStatusFail$).toBeObservable(expected);
        })
    );

    it('should test metadata', () => {
        expect(metadata.generateExport$).toEqual({ dispatch: true });
        expect(metadata.generateExportFail$).toEqual({ dispatch: true });
        expect(metadata.generateExportStatus$).toEqual({ dispatch: true });
        expect(metadata.generateExportStatusFail$).toEqual({ dispatch: true });
        expect(metadata.generateExportFile$).toEqual({ dispatch: true });
        expect(metadata.saveFile$).toEqual({ dispatch: true });
    });
});
