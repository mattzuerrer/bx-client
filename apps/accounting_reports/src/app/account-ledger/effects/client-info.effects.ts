import { Injectable } from '@angular/core';
import { MessageActions } from '@bx-client/message';
import { EffectsService } from '@bx-client/ngrx';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { ClientInfoActions } from '../actions';
import { AccountLedgerApiService, AccountLedgerStateService } from '../services';

@Injectable()
export class ClientInfoEffects extends EffectsService {
    @Effect()
    fetch$: Observable<any> = this.actions$.pipe(
        ofType<ClientInfoActions.LoadAction>(ClientInfoActions.LOAD),
        switchMap(() =>
            this.apiService
                .getClientInfo()
                .pipe(
                    map(
                        clientInfo => new ClientInfoActions.LoadSuccessAction({ currency: clientInfo.currency, hasVat: clientInfo.hasVat })
                    ),
                    catchError(response => of(new ClientInfoActions.LoadFailAction(response.status)))
                )
        )
    );

    @Effect()
    readonly listLoadFail$: Observable<any> = this.actions$.pipe(
        ofType<ClientInfoActions.LoadFailAction>(ClientInfoActions.LOAD_FAIL),
        map(
            action =>
                new MessageActions.ErrorMessage({
                    messageKey: this.translateService.instant('accounting_reports.message.error_fetch_model', {
                        httpState: action.payload,
                        modelName: this.translateService.instant('accounting_reports.message.model_currency')
                    })
                })
        )
    );

    initActions: Action[] = [new ClientInfoActions.LoadAction()];

    constructor(
        private actions$: Actions,
        private apiService: AccountLedgerApiService,
        private translateService: TranslateService,
        accountLedgerStateService: AccountLedgerStateService
    ) {
        super(accountLedgerStateService);
    }
}
