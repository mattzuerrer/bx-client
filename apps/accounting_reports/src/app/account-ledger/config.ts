export const featureName = 'accountLedger';

export const defaultPaginationPageSize = 50;
export const paginationPageSizeOptions = [10, 25, 50, 100, 200];

export enum ExportType {
    excel = 'excel',
    pdf = 'pdf'
}

export enum FileSuffix {
    excel = 'xlsx',
    pdf = 'pdf'
}

export enum DocumentType {
    excel = 'application/vnd.ms-excel',
    pdf = 'application/pdf'
}
