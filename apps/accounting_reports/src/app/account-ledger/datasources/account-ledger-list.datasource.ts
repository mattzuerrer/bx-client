import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Injectable } from '@angular/core';
import { noop, Observable } from 'rxjs';

import { AccountLedgerEntry } from '../models';
import { AccountLedgerStateService } from '../services';

@Injectable()
export class AccountLedgerListDataSource implements DataSource<AccountLedgerEntry> {
    constructor(private accountLedgerStateService: AccountLedgerStateService) {}

    connect(collectionViewer: CollectionViewer): Observable<AccountLedgerEntry[]> {
        return this.accountLedgerStateService.listEntries$;
    }

    disconnect(collectionViewer: CollectionViewer): void {
        noop();
    }
}
