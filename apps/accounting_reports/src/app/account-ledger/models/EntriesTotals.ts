import { CurrencyTotal } from './CurrencyTotal';

export interface EntriesTotals {
    debitAmount: number;
    creditAmount: number;
    balanceAmount: number;
    currencies?: CurrencyTotal[];
}
