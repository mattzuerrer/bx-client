export class BusinessYear {
    id: number;
    start: Date;
    end: Date;
}
