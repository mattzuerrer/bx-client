export interface AccountNumberRange {
    fromAccount?: string;
    toAccount?: string;
}
