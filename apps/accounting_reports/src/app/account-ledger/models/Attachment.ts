export class Attachment {
    name: string;
    link: string;
}
