import { serializeType } from '@bx-client/utils';
import { Type } from 'class-transformer';

import { Reference } from './Reference';

export class Carryover {
    date: Date;
    account: number;
    type: string;
    debitAmount: number;
    creditAmount: number;

    @Type(serializeType(Reference))
    reference: Reference;

    get carryoverType(): string {
        return `accounting_reports.carryover.${this.type}`;
    }
}
