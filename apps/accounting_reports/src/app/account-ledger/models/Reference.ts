export class Reference {
    number: string;
    name: string;
    link: string;
    collectionLink: string;
}
