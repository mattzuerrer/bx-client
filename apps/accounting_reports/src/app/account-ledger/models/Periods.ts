import { BusinessYear } from './BusinessYear';

export class SelectPeriod implements Partial<BusinessYear> {
    start: Date;
    end: Date;
    id?: number;
    viewValue: string;
    isCurrent?: boolean;
    separator?: boolean;
}
