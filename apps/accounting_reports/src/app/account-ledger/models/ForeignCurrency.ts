export class ForeignCurrency {
    name: string;
    changeRate: string;
    amount: string;
}
