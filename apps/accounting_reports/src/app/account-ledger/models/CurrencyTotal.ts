export interface CurrencyTotal {
    currency: string;
    amount: number;
}
