export interface AccountLedgerFilter {
    fromAccount: number;
    toAccount: number;
    taxId: number;
    entryTypes: string[];
    fromAmount: number;
    toAmount: number;
    fromDate: Date;
    toDate: Date;
    vatDigits: number[];
    referenceNo: string;
    description: string;
    includeCarryover: boolean;
    showZeroAmountEntries: boolean;
    showOnlyAccountsWithTransactions: boolean;
    showAccountDescription: boolean;
    decimalPlaces: number;
    checkForNewEntriesOnBackend: number;
}

export const FROM_DATE = 'fromDate';
export const TO_DATE = 'toDate';
export const FROM_AMOUNT = 'fromAmount';
export const TO_AMOUNT = 'toAmount';
export const TAX_ID = 'taxId';
export const ENTRY_TYPES = 'entryTypes';
export const VAT_DIGITS = 'vatDigits';
export const REFERENCE_NO = 'referenceNo';
export const DESCRIPTION = 'description';
export const INCLUDE_CARRYOVER = 'includeCarryover';
export const ZERO_AMOUNT_ENTRIES = 'showZeroAmountEntries';
export const DECIMAL_PLACES = 'decimalPlaces';

export const DATE_RANGE = 'dateRange';
export const ACCOUNT_RANGE = 'accountRange';
