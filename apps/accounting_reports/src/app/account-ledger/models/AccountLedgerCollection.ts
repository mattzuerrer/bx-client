import { serializeType } from '@bx-client/utils';
import { Type } from 'class-transformer';

import { AccountLedgerEntry } from './AccountLedgerEntry';
import { Carryover } from './Carryover';
import { EntriesTotals } from './EntriesTotals';

class AccountLedgerAggregations {
    accounts: string[];
    totals: EntriesTotals;
    hasVatRecords: boolean;
}

export class AccountLedgerCollection {
    @Type(serializeType(AccountLedgerEntry))
    accountingRecords: AccountLedgerEntry[];

    @Type(serializeType(AccountLedgerAggregations))
    aggregations: AccountLedgerAggregations;

    @Type(serializeType(Carryover))
    carryOver: Carryover;

    selectedAccountNumber: string;
}
