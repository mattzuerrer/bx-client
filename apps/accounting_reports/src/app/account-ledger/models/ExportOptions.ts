export class ExportOptions {
    exportAllAccounts: boolean;
    paperOrientation: string;
    forcePageBreak: boolean;
}
