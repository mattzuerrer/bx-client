import { serializeType } from '@bx-client/utils';
import { Type } from 'class-transformer';

import { Attachment } from './Attachment';
import { Reference } from './Reference';
import { Vat } from './Vat';

export class AccountLedgerEntry {
    @Type(serializeType(Attachment))
    attachment: Attachment[];

    id: string;

    @Type(serializeType(Date))
    date: Date;

    account: string;

    contraAccount: number;

    contraAccountDescription: string;

    @Type(serializeType(Reference))
    reference: Reference;

    contact: string;

    description: string;

    accountingRecordType: string;

    @Type(serializeType(Vat))
    vat: Vat;

    debitAmount: number;

    creditAmount: number;

    balance: number;

    currency: string;

    currencyExchangeRate: number;

    foreignCurrencyAmount: number;
}

export const DATE = 'date';
