import { ExportType } from '../config';

import { AccountNumberRange } from './AccountNumberRange';
import { ExportOptions } from './ExportOptions';

export interface ExportFile {
    file: Blob;
    exportType: ExportType;
    exportOptions: ExportOptions;
    filterAccountNumbers: AccountNumberRange;
}
