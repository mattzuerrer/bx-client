import { ConnectedPosition } from '@angular/cdk/overlay';

export enum TooltipPlacement {
    BOTTOM = 'bottom',
    TOP = 'top',
    RIGHT = 'right',
    LEFT = 'left'
  }
export const bottomPosition: ConnectedPosition[] = [
    {
        originX: 'center',
        originY: 'top',
        overlayX: 'center',
        overlayY: 'top',
        offsetY: 50
    }
];
export const topPosition: ConnectedPosition[] = [
    {
        originX: 'center',
        originY: 'bottom',
        overlayX: 'center',
        overlayY: 'bottom',
        offsetY: -30
    }
];
export const rightPosition: ConnectedPosition[] = [
    {
        originX: 'start',
        originY: 'center',
        overlayX: 'start',
        overlayY: 'center',
        offsetX: 50
    }
];
export const leftPosition: ConnectedPosition[] = [
    {
        originX: 'end',
        originY: 'center',
        overlayX: 'end',
        overlayY: 'center',
        offsetX: -300
    }
];
