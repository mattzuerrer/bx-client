import { PayloadAction } from '@bx-client/ngrx';
import { applyChanges } from '@bx-client/utils';

import { EntryTypesActions } from './entry-types.actions';

export interface State {
    entryTypes: string[];
    loaded: boolean;
}

const initialState: State = {
    entryTypes: [],
    loaded: false
};

export function reducer(state: State = initialState, action: PayloadAction): State {
    switch (action.type) {
        case EntryTypesActions.LOAD_SUCCESS:
            return applyChanges(state, { entryTypes: action.payload, loaded: true });
        case EntryTypesActions.LOAD_FAIL:
            return applyChanges(state, { loaded: false });
        default:
            return state;
    }
}
