import { inject, TestBed } from '@angular/core/testing';
import { MessageActions } from '@bx-client/message';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { TranslateService } from '@ngx-translate/core';
import { cold } from 'jasmine-marbles';
import { noop, Observable, of, throwError } from 'rxjs';

import { EntryTypesActions } from './entry-types.actions';
import { EntryTypesEffects } from './entry-types.effects';
import { EntryTypesApiService, EntryTypesStateService } from './services';

const apiServiceStub = {
    getEntryTypes: () => noop()
};

const stateServiceStub = {};

const translateServiceStub = {
    instant: () => noop()
};

const initialAction = [new EntryTypesActions.LoadAction()];

const entryTypes: string[] = ['bill', 'expense'];

describe('Entry Types effects', () => {
    let entryTypesEffects: EntryTypesEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<EntryTypesEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                EntryTypesEffects,
                {
                    provide: EntryTypesApiService,
                    useValue: apiServiceStub
                },
                {
                    provide: TranslateService,
                    useValue: translateServiceStub
                },
                {
                    provide: EntryTypesStateService,
                    useValue: stateServiceStub
                },
                provideMockActions(() => actions)
            ]
        });

        entryTypesEffects = TestBed.get(EntryTypesEffects);
        metadata = getEffectsMetadata(entryTypesEffects);
    });

    it('should have initial action', () => {
        expect(entryTypesEffects.initActions).toEqual(initialAction);
    });

    it(
        'should return LOAD_SUCCESS if LOAD is successful',
        inject([EntryTypesApiService], apiService => {
            actions = cold('a', { a: new EntryTypesActions.LoadAction() });
            spyOn(apiService, 'getEntryTypes').and.returnValue(of(entryTypes));
            const expected = cold('b', {
                b: new EntryTypesActions.LoadSuccessAction(entryTypes)
            });
            expect(entryTypesEffects.fetch$).toBeObservable(expected);
            expect(apiService.getEntryTypes).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_FAIL if LOAD fails',
        inject([EntryTypesApiService], apiService => {
            const thrownError = { status: 404 };
            actions = cold('a', { a: new EntryTypesActions.LoadAction() });
            spyOn(apiService, 'getEntryTypes').and.returnValue(throwError(thrownError));
            const expected = cold('b', { b: new EntryTypesActions.LoadFailAction(thrownError.status) });
            expect(entryTypesEffects.fetch$).toBeObservable(expected);
            expect(apiService.getEntryTypes).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return ERROR_MSG on LOAD_FAIL action',
        inject([TranslateService], translateService => {
            actions = cold('a', { a: new EntryTypesActions.LoadFailAction(404) });
            spyOn(translateService, 'instant').and.returnValue('error');
            const payload = { messageKey: 'error' };
            const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
            expect(entryTypesEffects.listLoadFail$).toBeObservable(expected);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetch$).toEqual({ dispatch: true });
        expect(metadata.listLoadFail$).toEqual({ dispatch: true });
    });
});
