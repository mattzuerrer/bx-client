import { Action } from '@ngrx/store';

export namespace EntryTypesActions {
    export const LOAD = '[EntryTypes] Load';
    export const LOAD_SUCCESS = '[EntryTypes] Load Success';
    export const LOAD_FAIL = '[EntryTypes] Load Fail';

    export class LoadAction implements Action {
        readonly type = LOAD;
    }

    export class LoadSuccessAction implements Action {
        readonly type = LOAD_SUCCESS;

        constructor(public payload: string[]) {}
    }

    export class LoadFailAction implements Action {
        readonly type = LOAD_FAIL;

        constructor(public payload: any) {}
    }

    export type Actions = LoadAction | LoadSuccessAction | LoadFailAction;
}
