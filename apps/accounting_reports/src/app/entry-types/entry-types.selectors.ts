import { createFeatureSelector, createSelector } from '@ngrx/store';

import { featureName } from './config';
import { State } from './entry-types.reducer';

export namespace EntryTypesSelectors {
    export const getState = createFeatureSelector<State>(featureName);

    export const getEntryTypes = createSelector(getState, (state: State) => state.entryTypes);
}
