import { EntryTypesActions } from './entry-types.actions';
import { reducer } from './entry-types.reducer';

const initialState = {
    entryTypes: [],
    loaded: false
};

const loadedState = {
    entryTypes: ['bill', 'expense'],
    loaded: true
};

const failState = {
    entryTypes: [],
    loaded: false
};

describe('Entry Types reducer', () => {
    it('should return initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return loaded state on success', () => {
        const state = initialState;
        const action = new EntryTypesActions.LoadSuccessAction(['bill', 'expense']);
        expect(reducer(state, action)).toEqual(loadedState);
    });

    it('should return fail state on fail', () => {
        const state = initialState;
        const action = new EntryTypesActions.LoadFailAction(404);
        expect(reducer(state, action)).toEqual(failState);
    });
});
