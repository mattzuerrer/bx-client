export const entryTypes: string[] = [
    'invoice',
    'credit_voucher',
    'bill',
    'expense',
    'client_account_entry',
    'manual_entry',
    'opening_entry',
    'payroll_entry'
];
