import { Injectable } from '@angular/core';
import { StateService } from '@bx-client/ngrx';
import { select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { EntryTypesSelectors } from '../entry-types.selectors';

@Injectable()
export class EntryTypesStateService extends StateService {
    entryTypes$: Observable<string[]> = this.store.pipe(select(EntryTypesSelectors.getEntryTypes));
}
