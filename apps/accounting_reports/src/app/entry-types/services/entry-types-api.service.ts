import { Injectable } from '@angular/core';
import { HttpService } from '@bx-client/http';
import { Observable } from 'rxjs';

@Injectable()
export class EntryTypesApiService {
    constructor(private httpService: HttpService) {}

    /**
     * Get getEntryTypes.
     *
     * @returns {Observable<string[]>}
     */
    getEntryTypes(): Observable<string[]> {
        return this.httpService.get(null, 'accounting_types', {});
    }
}
