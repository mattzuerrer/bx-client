import { Injectable } from '@angular/core';
import { MessageActions } from '@bx-client/message';
import { EffectsService } from '@bx-client/ngrx';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { EntryTypesActions } from './entry-types.actions';
import { EntryTypesApiService, EntryTypesStateService } from './services';

@Injectable()
export class EntryTypesEffects extends EffectsService {
    @Effect()
    fetch$: Observable<any> = this.actions$.pipe(
        ofType<EntryTypesActions.LoadAction>(EntryTypesActions.LOAD),
        switchMap(() =>
            this.apiService
                .getEntryTypes()
                .pipe(
                    map(entryTypes => new EntryTypesActions.LoadSuccessAction(entryTypes)),
                    catchError(response => of(new EntryTypesActions.LoadFailAction(response.status)))
                )
        )
    );

    @Effect()
    readonly listLoadFail$: Observable<any> = this.actions$.pipe(
        ofType<EntryTypesActions.LoadFailAction>(EntryTypesActions.LOAD_FAIL),
        map(
            action =>
                new MessageActions.ErrorMessage({
                    messageKey: this.translateService.instant('accounting_reports.message.error_fetch_model', {
                        httpState: action.payload,
                        modelName: this.translateService.instant('accounting_reports.message.model_entry_types')
                    })
                })
        )
    );

    initActions: Action[] = [new EntryTypesActions.LoadAction()];

    constructor(
        private actions$: Actions,
        private apiService: EntryTypesApiService,
        private translateService: TranslateService,
        entryTypesStateService: EntryTypesStateService
    ) {
        super(entryTypesStateService);
    }
}
