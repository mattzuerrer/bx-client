import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { featureName } from './config';
import { EntryTypesEffects } from './entry-types.effects';
import { reducer } from './entry-types.reducer';
import { EntryTypesApiService, EntryTypesStateService } from './services';

@NgModule({
    imports: [StoreModule.forFeature(featureName, reducer), EffectsModule.forFeature([EntryTypesEffects])],
    providers: [EntryTypesApiService, EntryTypesStateService]
})
export class EntryTypesModule {}
