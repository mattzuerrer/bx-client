import { NgModule } from '@angular/core';
import { CoreModule } from '@bx-client/core';
import { AccountModule } from '@bx-client/features/account';

import { components } from './components';
import { containers } from './containers';
import { guards } from './guards';
import { routing } from './journal.routes';

@NgModule({
    imports: [routing, CoreModule, AccountModule],
    declarations: [...components, ...containers],
    providers: [...guards]
})
export class JournalModule {}
