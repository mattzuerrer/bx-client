import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { JournalComponent } from './containers';

const routes: Routes = [
    {
        path: '',
        component: JournalComponent
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
