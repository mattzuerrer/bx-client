import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { environment } from '@bx-client/core';

import { AppModule } from './app/app.module';

if (environment.isProduction) {
    enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule);
