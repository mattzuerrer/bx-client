import { BookingFormContainerComponent } from './booking-form-container/booking-form-container.component';
import { ManualEntriesComponent } from './manual-entries/manual-entries.component';

export { BookingFormContainerComponent } from './booking-form-container/booking-form-container.component';
export { ManualEntriesComponent } from './manual-entries/manual-entries.component';

export const appContainers = [BookingFormContainerComponent, ManualEntriesComponent];
