import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { RouterActions } from '@bx-client/router';
import { Observable } from 'rxjs';

import { ListActions } from '../../actions';
import { ColumnStatus, ManualEntryCollection } from '../../models';
import { StateService } from '../../services';

@Component({
    selector: 'app-manual-entries',
    templateUrl: 'manual-entries.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ManualEntriesComponent implements OnInit {
    listEntries$: Observable<ManualEntryCollection[]>;

    columnConfig$: Observable<ColumnStatus[]>;

    constructor(private stateService: StateService, private router: Router) {}

    ngOnInit(): void {
        this.listEntries$ = this.stateService.listEntries$;
        this.columnConfig$ = this.stateService.columnConfig$;
    }

    onSort(columnStatus: ColumnStatus): void {
        this.stateService.dispatch(new ListActions.SortAction([columnStatus]));
    }

    onSelect(manualEntryCollection: ManualEntryCollection): void {
        const id: number = manualEntryCollection.id;
        const parsedRouteId: number = parseInt(this.router.url.substr(4), 10);

        // Allows to re-select entries with same id if not already selected
        if (id === parsedRouteId && manualEntryCollection.status !== ManualEntryCollection.statusSelected) {
            this.stateService.dispatch(new ListActions.SelectAction(manualEntryCollection));
        }

        this.stateService.dispatch(new RouterActions.Go({ path: ['/id/', id] }));
    }
}
