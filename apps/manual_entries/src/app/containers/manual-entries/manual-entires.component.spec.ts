import { TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { CommonModule as BxCommonModule, getChildDebugElement, getChildInstance } from '@bx-client/common';
import { RouterActions } from '@bx-client/router';
import 'hammerjs';
import { MockComponent } from 'ng2-mock-component';

import { ListActions } from '../../actions';
import { TranslateMockPipe } from '../../mocks/pipes';
import { StateServiceMock, StateServiceMockProvider } from '../../mocks/services';
import * as stateServiceFixtures from '../../mocks/services/state.service.fixtures';
import { ColumnStatus, ManualEntryCollection } from '../../models';
import { StateService } from '../../services';

import { ManualEntriesComponent } from './manual-entries.component';

const HotkeysCheatsheetMockComponent: any = MockComponent({
    selector: 'hotkeys-cheatsheet',
    template: '<div></div>',
    inputs: ['title']
});

const ListMockComponent: any = MockComponent({
    selector: 'app-list',
    template: '<div></div>',
    inputs: ['listEntries', 'columnConfig', 'sort', 'select']
});

const BookingFormContainerMockComponent: any = MockComponent({
    selector: 'app-booking-form-container',
    template: '<div></div>',
    inputs: []
});

describe('ManualEntriesComponent', () => {
    let fixture;
    let component;
    let stateService: StateServiceMock;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                ManualEntriesComponent,
                BookingFormContainerMockComponent,
                HotkeysCheatsheetMockComponent,
                ListMockComponent,
                TranslateMockPipe
            ],
            imports: [RouterTestingModule.withRoutes([]), BxCommonModule, MatCardModule],
            providers: [StateServiceMockProvider]
        });

        fixture = TestBed.createComponent(ManualEntriesComponent);
        component = fixture.debugElement.componentInstance;
        stateService = fixture.debugElement.injector.get(StateService);
    });

    it('should create manual entires', () => {
        expect(component).toBeTruthy();
    });

    it('should pass resolved states to list', () => {
        fixture.detectChanges();
        const listComponent = getChildInstance(fixture, ListMockComponent);
        expect(listComponent.listEntries).toBe(stateServiceFixtures.listEntries);
        expect(listComponent.columnConfig).toBe(stateServiceFixtures.columnConfig);
    });

    it('should dispatch sort list action', () => {
        spyOn(stateService, 'dispatch');
        const columnStatus = new ColumnStatus();
        getChildDebugElement(fixture, ListMockComponent).triggerEventHandler('sort', columnStatus);
        expect(stateService.dispatch).toHaveBeenCalledTimes(1);
        expect(stateService.dispatch).toHaveBeenCalledWith(new ListActions.SortAction([columnStatus]));
    });

    it('should dispatch select entry action', () => {
        spyOn(stateService, 'dispatch');
        const collection = new ManualEntryCollection();
        collection.id = 5;
        getChildDebugElement(fixture, ListMockComponent).triggerEventHandler('select', collection);
        expect(stateService.dispatch).toHaveBeenCalledTimes(1);
        expect(stateService.dispatch).toHaveBeenCalledWith(
            new RouterActions.Go({
                path: ['/id/', 5]
            })
        );
    });
});
