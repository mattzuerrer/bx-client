import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { byId, identity } from '@bx-client/common';
import { Account } from '@bx-client/features/account';
import { Company } from '@bx-client/features/company';
import { RouterActions } from '@bx-client/router';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import { combineLatest, filter, map, take } from 'rxjs/operators';
import { isNumber } from 'util';

import { AutofillActions, EntryCollectionActions, FilesActions, FileUploadDialogActions, TemplatesActions } from '../../actions';
import { Currency, Dictionary, File, FormState, ManualEntryCollection, ManualEntryTemplate, Tax } from '../../models';
import { StateService } from '../../services';

const fileIdQueryParameter = 'fmFileId';

@Component({ selector: 'app-booking-form-container', templateUrl: 'booking-form-container.component.html' })
export class BookingFormContainerComponent implements OnInit {
    manualEntryCollection$: Observable<ManualEntryCollection>;

    formState$: Observable<FormState>;

    currencies$: Observable<Currency[]>;

    accounts$: Observable<Account[]>;

    accountGroupsLoaded$: Observable<boolean>;

    taxes$: Observable<Tax[]>;

    files$: Observable<File[]>;

    fileUploadDialogFiles$: Observable<File[]>;

    fileUploadDialogFilesAdded$: Observable<File[]>;

    fileUploadDialogFilesRemoved$: Observable<File[]>;

    inboxFile$: Observable<File>;

    company$: Observable<Company>;

    templates$: Observable<ManualEntryTemplate[]>;

    autofillDescriptions$: Observable<string[]>;

    constructor(private stateService: StateService, private route: ActivatedRoute, private router: Router) {}

    ngOnInit(): void {
        this.formState$ = this.stateService.formState$;
        this.manualEntryCollection$ = this.stateService.manualEntryCollection$;
        this.currencies$ = this.stateService.currencies$;
        this.taxes$ = this.stateService.taxes$;
        this.files$ = this.stateService.files$;
        this.fileUploadDialogFiles$ = this.stateService.fileUploadDialogFiles$;
        this.fileUploadDialogFilesAdded$ = this.stateService.fileUploadDialogFilesAdded$;
        this.fileUploadDialogFilesRemoved$ = this.stateService.fileUploadDialogFilesRemoved$;
        this.accounts$ = this.stateService.accountsWithCurencies$;
        this.company$ = this.stateService.company$;
        this.templates$ = this.stateService.templates$;
        this.autofillDescriptions$ = this.stateService.autofillDescriptions$;
        this.accountGroupsLoaded$ = this.stateService.accountGroupsLoaded$;
        this.inboxFile$ = this.inboxFile;
    }

    onCancelCreate(): void {
        this.stateService.dispatch(new EntryCollectionActions.ResetAction());
    }

    onCancelEdit(): void {
        this.stateService.dispatch(new EntryCollectionActions.CancelAction());
        this.stateService.dispatch(new RouterActions.Go({ path: [''] }));
    }

    onDateChange(date: Date): void {
        this.stateService.dispatch(new EntryCollectionActions.UpdateDateAction(date));
    }

    onCreate(manualEntryCollection: ManualEntryCollection): void {
        this.stateService.dispatch(new EntryCollectionActions.SaveEntryCollectionAction(manualEntryCollection));
    }

    onAddAutofill(dictionary: Dictionary<string[]>): void {
        this.stateService.dispatch(new AutofillActions.AddAutofillAction(dictionary));
    }

    onUpdate(manualEntryCollection: ManualEntryCollection): void {
        this.stateService.dispatch(new EntryCollectionActions.UpdateEntryCollectionAction(manualEntryCollection));
    }

    onUpdateFiles(manualEntryCollection: ManualEntryCollection): void {
        this.stateService.dispatch(new EntryCollectionActions.UpdateEntryCollectionFilesAction(manualEntryCollection));
    }

    onRemove(id: number): void {
        this.stateService.dispatch(new EntryCollectionActions.DeleteEntryCollectionAction(id));
    }

    onAddFileUploadDialogFile(file: File): void {
        this.stateService.dispatch(new FileUploadDialogActions.AddFileAction(file));
    }

    onRemoveFileUploadDialogFile(file: File): void {
        this.stateService.dispatch(new FileUploadDialogActions.RemoveFileAction(file));
    }

    onInitFileUploadDialog(files: File[]): void {
        this.stateService.dispatch(new FileUploadDialogActions.InitFileUploadDialog(files));
    }

    onDeleteTemplate(id: number): void {
        this.stateService.dispatch(new TemplatesActions.DeleteAction(id));
    }

    onSaveTemplate(template: ManualEntryTemplate): void {
        let action: Action;
        if (isNumber(template.id) && template.id > 0) {
            action = new TemplatesActions.UpdateAction(template);
        } else {
            action = new TemplatesActions.CreateAction(template);
        }
        this.stateService.dispatch(action);
    }

    onSelectTemplate(template: ManualEntryTemplate): void {
        this.stateService.dispatch(new TemplatesActions.SelectTemplateAction(template.collection));
    }

    get inboxFile(): Observable<File> {
        return this.route.queryParams.pipe(
            take(1),
            map(queryParams => queryParams[fileIdQueryParameter]),
            filter(identity),
            combineLatest(this.route.queryParams, this.files$, (_, queryParams, files) => {
                const fileId = +queryParams[fileIdQueryParameter];
                if (fileId) {
                    const file = files.find(byId(fileId));
                    if (!file) {
                        this.stateService.dispatch(new FilesActions.LoadFileAction(fileId));
                    } else {
                        setTimeout(() => {
                            const url: string = this.router.url.substring(0, this.router.url.indexOf('?'));
                            this.router.navigateByUrl(url);
                        }, 0);
                    }
                    return file;
                }
                return null;
            })
        );
    }
}
