import { TestBed } from '@angular/core/testing';
import { getChildDebugElement, getChildInstance } from '@bx-client/common';
import { RouterActions } from '@bx-client/router';

import { EntryCollectionActions } from '../../actions';
import { BookingFormMockComponent } from '../../mocks/components';
import { ActivatedRouteMockProvider, RouterMockProvider, StateServiceMockProvider } from '../../mocks/services';
import * as stateServiceFixtures from '../../mocks/services/state.service.fixtures';
import { ManualEntryCollection } from '../../models';
import { StateService } from '../../services';

import { BookingFormContainerComponent } from './booking-form-container.component';

describe('BookingFormContainerComponent', () => {
    let fixture;
    let component;
    let stateService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [BookingFormContainerComponent, BookingFormMockComponent],
            providers: [StateServiceMockProvider, ActivatedRouteMockProvider, RouterMockProvider]
        });

        fixture = TestBed.createComponent(BookingFormContainerComponent);
        component = fixture.debugElement.componentInstance;
        stateService = fixture.debugElement.injector.get(StateService);
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should pass resolved states to booking form', () => {
        fixture.detectChanges();
        const bookingFormComponent: BookingFormMockComponent = getChildInstance(fixture, BookingFormMockComponent);
        expect(bookingFormComponent.formState).toBe(stateServiceFixtures.formState);
        expect(bookingFormComponent.manualEntryCollection).toBe(stateServiceFixtures.manualEntryCollection);
        expect(bookingFormComponent.accounts).toBe(stateServiceFixtures.accounts);
        expect(bookingFormComponent.taxes).toBe(stateServiceFixtures.taxes);
        expect(bookingFormComponent.currencies).toBe(stateServiceFixtures.currencies);
        expect(bookingFormComponent.company).toBe(stateServiceFixtures.company);
    });

    it('should dispatch create action', () => {
        spyOn(stateService, 'dispatch');
        const collection = new ManualEntryCollection();
        getChildDebugElement(fixture, BookingFormMockComponent).triggerEventHandler('create', collection);
        expect(stateService.dispatch).toHaveBeenCalledTimes(1);
        expect(stateService.dispatch).toHaveBeenCalledWith(new EntryCollectionActions.SaveEntryCollectionAction(collection));
    });

    it('should dispatch update action', () => {
        spyOn(stateService, 'dispatch');
        const collection = new ManualEntryCollection();
        getChildDebugElement(fixture, BookingFormMockComponent).triggerEventHandler('update', collection);
        expect(stateService.dispatch).toHaveBeenCalledTimes(1);
        expect(stateService.dispatch).toHaveBeenCalledWith(new EntryCollectionActions.UpdateEntryCollectionAction(collection));
    });

    it('should dispatch cancel create action', () => {
        spyOn(stateService, 'dispatch');
        getChildDebugElement(fixture, BookingFormMockComponent).triggerEventHandler('cancelCreate');
        expect(stateService.dispatch).toHaveBeenCalledTimes(1);
        expect(stateService.dispatch).toHaveBeenCalledWith(new EntryCollectionActions.ResetAction());
    });

    it('should dispatch cancel edit action', () => {
        spyOn(stateService, 'dispatch');
        getChildDebugElement(fixture, BookingFormMockComponent).triggerEventHandler('cancelEdit');
        expect(stateService.dispatch).toHaveBeenCalledTimes(2);
        expect(stateService.dispatch).toHaveBeenCalledWith(new EntryCollectionActions.CancelAction());
        expect(stateService.dispatch).toHaveBeenCalledWith(new RouterActions.Go({ path: [''] }));
    });

    it('should dispatch date change action', () => {
        spyOn(stateService, 'dispatch');
        const date = new Date();
        getChildDebugElement(fixture, BookingFormMockComponent).triggerEventHandler('dateChange', date);
        expect(stateService.dispatch).toHaveBeenCalledTimes(1);
        expect(stateService.dispatch).toHaveBeenCalledWith(new EntryCollectionActions.UpdateDateAction(date));
    });
});
