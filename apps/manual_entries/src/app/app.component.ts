import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AppActions } from './actions';
import { AppService, StateService } from './services';

@Component({
    selector: 'app-root',
    styleUrls: ['app.component.scss'],
    templateUrl: 'app.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
    @ViewChild(ToastContainerDirective) toastContainer: ToastContainerDirective;
    loading$: Observable<boolean>;

    constructor(private appService: AppService, private stateService: StateService, private toastrService: ToastrService) {}

    ngOnInit(): void {
        this.appService.init();
        this.stateService.dispatch(new AppActions.InitAppAction());
        this.loading$ = this.stateService.statesLoaded$.pipe(map(isLoading => !isLoading));
        this.toastrService.overlayContainer = this.toastContainer;
    }
}
