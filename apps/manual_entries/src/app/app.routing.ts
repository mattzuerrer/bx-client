import { RouterModule, Routes } from '@angular/router';

export const routes: Routes = [{ path: '', loadChildren: './manual-entries.module#ManualEntriesModule' }, { path: '**', redirectTo: '' }];

export const routing = RouterModule.forRoot(routes);
