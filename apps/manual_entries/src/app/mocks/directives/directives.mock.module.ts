import { NgModule } from '@angular/core';

import { CurrencyFormatAccessorMockDirective } from './currency-format-accessor.mock.directive';

@NgModule({
    declarations: [CurrencyFormatAccessorMockDirective]
})
export class ComponentMockModule {}
