import { Directive, Input } from '@angular/core';

@Directive({
    selector: 'input[appCurrencyInput]'
})
export class CurrencyFormatAccessorMockDirective {
    @Input() customFormat: string;
}
