import { noop } from 'rxjs';

import { AppService } from '../../services';

export class AppServiceMock {
    init(): void {
        noop();
    }

    setRootViewContainerRef(): void {
        noop();
    }
}

export const AppServiceMockProvider = {
    provide: AppService,
    useClass: AppServiceMock
};
