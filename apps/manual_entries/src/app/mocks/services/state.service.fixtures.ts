import { Account } from '@bx-client/features/account';
import { Company } from '@bx-client/features/company';

import { ColumnStatus, Currency, File, FormState, ManualEntryCollection, Tax } from '../../models';

export const listEntries = [new ManualEntryCollection(), new ManualEntryCollection()];

export const statesLoaded = false;

export const accounts = [new Account(), new Account()];

export const company = new Company();

export const manualEntryCollection = new ManualEntryCollection();

export const formState = new FormState();

export const currencies = [new Currency(), new Currency()];

export const taxes = [new Tax(), new Tax()];

export const columnConfig = [new ColumnStatus(), new ColumnStatus()];

export const files = [new File(), new File()];
