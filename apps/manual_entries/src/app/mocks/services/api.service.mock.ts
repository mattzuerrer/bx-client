import { of } from 'rxjs';

import { ApiService } from '../../services';
import { collection } from '../fixtures/entry-collection.fixtures';
import { taxes } from '../fixtures/taxes.fixtures';
import { templates } from '../fixtures/templates.fixtures';

export class ApiServiceMock {
    getTaxes(date: string): any {
        return of(taxes);
    }

    getManualEntryCollections(orderBy: string): any {
        return of([collection]);
    }

    getFiles(): any {
        return of([]);
    }

    getManualEntryTemplates(): any {
        return of([]);
    }

    getNextRefNumber(): any {
        return of(`ref_${+new Date()}`);
    }

    createCollection(): any {
        return of(collection);
    }

    createManualEntryTemplate(): any {
        return of(templates[0]);
    }

    updateManualEntryTemplate(): any {
        return of(templates[0]);
    }

    deleteManualEntryTemplate(): any {
        return of(1);
    }

    getFile(): any {
        return true;
    }

    connectFile(): any {
        return true;
    }

    disconnectFile(): any {
        return true;
    }

    deleteManualEntryCollection(): any {
        return true;
    }

    updateCollection(): any {
        return true;
    }
}

export const ApiServiceMockProvider = {
    provide: ApiService,
    useClass: ApiServiceMock
};
