import { FileApiService } from '../../services/file-api/file-api.service';

export class FileApiServiceMock {}

export const FileApiServiceMockProvider = {
    provide: FileApiService,
    useClass: FileApiServiceMock
};
