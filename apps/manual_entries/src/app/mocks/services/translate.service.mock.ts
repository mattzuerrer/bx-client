import { TranslateService } from '@ngx-translate/core';
import { noop, Observable, of } from 'rxjs';

export class TranslateServiceMock {
    setDefaultLang(): void {
        noop();
    }

    use(): void {
        noop();
    }

    get(keys: string[]): Observable<any> {
        const result = {};
        keys.forEach(key => (result[key] = key));
        return of(result);
    }
}

export const TranslateServiceMockProvider = {
    provide: TranslateService,
    useClass: TranslateServiceMock
};
