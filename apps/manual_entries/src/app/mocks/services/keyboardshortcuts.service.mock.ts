import { noop } from 'rxjs';

import { KeyboardShortcutsService } from '../../services';

export class KeyboardShortcutsServiceMock {
    init(): void {
        noop();
    }
}

export const KeyboardShortcutsServiceMockProvider = {
    provide: KeyboardShortcutsService,
    useClass: KeyboardShortcutsServiceMock
};
