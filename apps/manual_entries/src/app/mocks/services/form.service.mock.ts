import { noop, Observable, of } from 'rxjs';

import { FormService } from '../../services';

export class FormServiceMock {
    onResetForm$(): Observable<void> {
        return of(null);
    }

    onResetTemplate$(): Observable<void> {
        return of(null);
    }

    onSubmitForm$(): Observable<void> {
        return of(null);
    }

    onAddRow$(): Observable<void> {
        return of(null);
    }

    onRemoveRow$(): Observable<void> {
        return of(null);
    }

    onFocusRow$(): Observable<number> {
        return of(null);
    }

    resetForm(): void {
        noop();
    }

    resetTemplate(): void {
        noop();
    }

    submitForm(): void {
        noop();
    }

    addRow(): void {
        noop();
    }

    removeRow(): void {
        noop();
    }
}

export const FormServiceMockProvider = {
    provide: FormService,
    useClass: FormServiceMock
};
