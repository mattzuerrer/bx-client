import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';

export class ActivatedRouteMock {
    queryParams: Observable<any> = of({ fileId: '4' });
}

export const ActivatedRouteMockProvider = {
    provide: ActivatedRoute,
    useClass: ActivatedRouteMock
};
