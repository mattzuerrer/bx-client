import { Account } from '@bx-client/features/account';
import { Company } from '@bx-client/features/company';
import { Action } from '@ngrx/store';
import { BehaviorSubject, noop, Observable, of } from 'rxjs';

import { ColumnStatus, Currency, File, FormState, ManualEntryCollection, Tax } from '../../models';
import { StateService } from '../../services';

import * as fixtures from './state.service.fixtures';

export class StateServiceMock {
    listEntries$: Observable<ManualEntryCollection[]> = of(fixtures.listEntries);

    statesLoaded$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(fixtures.statesLoaded);

    accountsWithCurencies$: Observable<Account[]> = of(fixtures.accounts);

    company$: Observable<Company> = of(fixtures.company);

    manualEntryCollection$: Observable<ManualEntryCollection> = of(fixtures.manualEntryCollection);

    formState$: Observable<FormState> = of(fixtures.formState);

    currencies$: Observable<Currency[]> = of(fixtures.currencies);

    taxes$: Observable<Tax[]> = of(fixtures.taxes);

    columnConfig$: Observable<ColumnStatus[]> = of(fixtures.columnConfig);

    files$: Observable<File[]> = of(fixtures.files);

    dispatch(action: Action): void {
        noop();
    }
}

export const StateServiceMockProvider = {
    provide: StateService,
    useClass: StateServiceMock
};
