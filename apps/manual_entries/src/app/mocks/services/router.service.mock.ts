import { Router } from '@angular/router';

export class RouterMock {}

export const RouterMockProvider = {
    provide: Router,
    useClass: RouterMock
};
