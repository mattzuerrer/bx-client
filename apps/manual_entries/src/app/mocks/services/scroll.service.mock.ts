import { noop } from 'rxjs';

import { ScrollService } from '../../services';

export class ScrollServiceMock {
    scrollConfig: any = {};

    scrollInstance: any = {};

    init(document: Document): void {
        noop();
    }

    scrollToTop(): void {
        noop();
    }
}

export const ScrollServiceMockProvider = {
    provide: ScrollService,
    useClass: ScrollServiceMock
};
