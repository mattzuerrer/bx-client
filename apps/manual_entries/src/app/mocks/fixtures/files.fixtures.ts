import { plainToClass } from 'class-transformer';

import { File } from '../../models';

export const files = plainToClass(File, [
    {
        id: 1,
        name: 'File 1',
        size_in_bytes: 2260,
        extension: 'png',
        mime_type: 'image/png',
        user_id: 1,
        created_at: '2017-03-22T11:14:42+00:00'
    },
    {
        id: 2,
        name: 'File 2',
        size_in_bytes: 2487,
        extension: 'png',
        mime_type: 'image/png',
        user_id: 1,
        created_at: '2017-03-22T11:14:42+00:00'
    }
]);

export const file = plainToClass(File, {
    id: 3,
    name: 'chewbacca',
    size_in_bytes: 35654,
    extension: 'png',
    mime_type: 'image/png',
    user_id: 1,
    created_at: '2018-03-23T23:01:09+00:00'
});
