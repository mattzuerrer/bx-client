import { ManualEntry, ManualEntryCollection } from '../../models';

export const collection = Object.assign(new ManualEntryCollection(), {
    id: 1,
    type: null,
    referenceNr: '123',
    entries: [Object.assign(new ManualEntry(), { id: 1, debitAccountId: 2, creditAccountId: 3, currencyId: 1, amount: 1000 })],
    date: new Date()
});
