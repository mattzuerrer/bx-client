import { ManualEntry, ManualEntryCollection, ManualEntryTemplate } from '../../models';

export const templates = [
    Object.assign(new ManualEntryTemplate(), {
        id: 1,
        name: 'test Template',
        collection: Object.assign(new ManualEntryCollection(), {
            type: null,
            referenceNr: '123',
            entries: [
                Object.assign(new ManualEntry(), {
                    id: 1,
                    debitAccountId: 2,
                    creditAccountId: 3,
                    currencyId: 1,
                    amount: 1000
                })
            ],
            date: new Date()
        })
    }),
    Object.assign(new ManualEntryTemplate(), {
        id: 2,
        name: 'test second Template',
        collection: Object.assign(new ManualEntryCollection(), {
            type: null,
            referenceNr: '67234',
            entries: [
                Object.assign(new ManualEntry(), {
                    id: 1,
                    debitAccountId: 3,
                    creditAccountId: 4,
                    currencyId: 1,
                    amount: 123
                })
            ],
            date: new Date()
        })
    })
];
