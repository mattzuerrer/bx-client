import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'toNumber' })
export class ToNumberMockPipe implements PipeTransform {
    transform(value: any): any {
        return value;
    }
}
