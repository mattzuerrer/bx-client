import { NgModule } from '@angular/core';

import { HighlightMockPipe } from './highlight.mock.pipe';
import { ToNumberMockPipe } from './to-number.mock.pipe';
import { TranslateMockPipe } from './translate.mock.pipe';

@NgModule({
    declarations: [HighlightMockPipe, ToNumberMockPipe, TranslateMockPipe]
})
export class PipeMockModule {}
