import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Account } from '@bx-client/features/account';
import { Company } from '@bx-client/features/company';

import { Currency, File, FormState, ManualEntryCollection, ManualEntryTemplate, Tax } from '../../models';

@Component({ selector: 'app-booking-form', template: '<div></div>' })
export class BookingFormMockComponent {
    @Input() formState: FormState;

    @Input() manualEntryCollection: ManualEntryCollection;

    @Input() templates: ManualEntryTemplate[];

    @Input() currencies: Currency[] = [];

    @Input() accounts: Account[] = [];

    @Input() taxes: Tax[] = [];

    @Input() inboxFile: File = null;

    @Input() files: File[] = [];

    @Input() company: Company = null;

    @Input() accountGroupsLoaded = false;

    @Input() autofillDescriptions: string[] = [];

    @Input() fileUploadDialogFiles: File[];

    @Input() fileUploadDialogFilesAdded: File[];

    @Input() fileUploadDialogFilesRemoved: File[];

    @Output() dateChange: EventEmitter<Date> = new EventEmitter<Date>();

    @Output() create: EventEmitter<ManualEntryCollection> = new EventEmitter<ManualEntryCollection>();

    @Output() update: EventEmitter<ManualEntryCollection> = new EventEmitter<ManualEntryCollection>();

    @Output() cancel: EventEmitter<void> = new EventEmitter<void>();
}
