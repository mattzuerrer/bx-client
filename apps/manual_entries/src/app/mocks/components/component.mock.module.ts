import { NgModule } from '@angular/core';

import { AutocompleteMockComponent } from './autocomplete.mock.component';
import { BookingEntryMockComponent } from './booking-entry.mock.component';
import { BookingFormMockComponent } from './booking-form.mock.component';
import { TapDateMockComponent } from './tap-date.mock.component';

@NgModule({
    declarations: [AutocompleteMockComponent, BookingEntryMockComponent, BookingFormMockComponent, TapDateMockComponent]
})
export class ComponentMockModule {}
