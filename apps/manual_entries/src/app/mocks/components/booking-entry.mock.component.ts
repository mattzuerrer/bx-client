import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Account } from '@bx-client/features/account';
import { Company } from '@bx-client/features/company';

import { Currency, Tax } from '../../models';

@Component({ selector: 'app-booking-entry', template: '<div></div>' })
export class BookingEntryMockComponent {
    @Input() currencies: Currency[] = [];

    @Input() accounts: Account[] = [];

    @Input() taxes: Tax[] = [];

    @Input() entry: FormGroup;

    @Input() date: Date;

    @Input() company: Company = null;

    @Input() type: string;

    @Input() index: number;

    @Input() isLocked: boolean;

    @Input() accountGroupsLoaded = false;

    @Input() autofillDescriptions: string[] = [];

    @Input() bookingEntryFormat: string;
}
