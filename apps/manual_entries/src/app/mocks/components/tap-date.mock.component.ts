import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';

@Component({ selector: 'app-tap-date', template: '<div></div>' })
export class TapDateMockComponent {
    @Input() label = 'Date';

    @Input() required = false;

    @Input() disabled = false;

    @Output() dateChange: EventEmitter<Date> = new EventEmitter<Date>();

    @ViewChild('dayInput') dayInput: ElementRef;
}
