import { NgModule } from '@angular/core';
import { CommonModule as BxCommonModule } from '@bx-client/common';
import { CoreModule } from '@bx-client/core';
import { HotkeysModule } from '@bx-client/hotkeys';
import { apiHttpInterceptorProvider, ApiJwtInterceptorProvider, httpApiProviderToken } from '@bx-client/http';
import { BxLocalStorageModule, localStorageConfigToken } from '@bx-client/local-storage';
import { MessageModule } from '@bx-client/message';

import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { version } from './config';
import { LocalStorageConfig } from './config/local-storage-config';
import { ManualEntriesModule } from './manual-entries.module';
import { appModules } from './modules';

@NgModule({
    declarations: [AppComponent],
    imports: [
        CoreModule.forRoot(version),
        HotkeysModule.forRoot(),
        MessageModule.forRoot(),
        routing,
        ...appModules,
        ManualEntriesModule,
        BxCommonModule,
        BxLocalStorageModule.forRoot()
    ],
    providers: [
        apiHttpInterceptorProvider,
        { provide: httpApiProviderToken, useClass: ApiJwtInterceptorProvider },
        { provide: localStorageConfigToken, useClass: LocalStorageConfig }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
