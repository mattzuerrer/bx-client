import { RouterModule, Routes } from '@angular/router';

import { ManualEntriesComponent } from './containers';
import { ManualEntryCollectionExistsGuard, StatesLoadedGuard } from './guards';

export const routes: Routes = [
    { path: '', canActivate: [StatesLoadedGuard], component: ManualEntriesComponent },
    {
        path: 'id/:id',
        canActivate: [ManualEntryCollectionExistsGuard],
        component: ManualEntriesComponent
    },
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forChild(routes);
