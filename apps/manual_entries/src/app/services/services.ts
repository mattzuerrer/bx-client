import { ApiService } from './api.service';
import { AppService } from './app.service';
import { FileApiService } from './file-api';
import { FormService } from './form.service';
import { InputCalculatorService } from './input-calculator.service';
import { KeyboardShortcutsService } from './keyboardshortcuts.service';
import { ScrollService } from './scroll.service';
import { StateService } from './state.service';

export const appServices = [
    ApiService,
    AppService,
    FileApiService,
    FormService,
    KeyboardShortcutsService,
    ScrollService,
    StateService,
    InputCalculatorService
];
