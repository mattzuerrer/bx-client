import { inject, TestBed } from '@angular/core/testing';
import { GlobalizeService, localesFormat } from '@bx-client/i18n';
import { noop } from 'rxjs';

import { InputCalculatorService } from './input-calculator.service';

export class GlobalizeServiceMock {
    getLocaleFormat(): void {
        noop();
    }
    toNumber: () => number = () => 123431.2;
}

export const GlobalizeServiceMockProvider = {
    provide: GlobalizeService,
    useClass: GlobalizeServiceMock
};

describe('Input calculator service', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [InputCalculatorService, GlobalizeServiceMockProvider]
        });
    });

    it(
        'should return number without wrong arithmetics for de-CH locale',
        inject([GlobalizeService, InputCalculatorService], (globalizeService, inputCalculatorService) => {
            const arithmeticString = "123'431.20 + ";
            spyOn(globalizeService, 'getLocaleFormat').and.returnValue(localesFormat['de-CH']);
            spyOn(globalizeService, 'toNumber').and.callThrough();

            expect(inputCalculatorService.calculate(arithmeticString)).toBe(123431.2);
        })
    );

    it(
        'should return number for de-CH locale',
        inject([GlobalizeService, InputCalculatorService], (globalizeService, inputCalculatorService) => {
            const arithmeticString = "123'431.20 + (-0.5 + 5.5 * (4.5 + 5.5) / (3 + 2)) - 1.50";

            spyOn(globalizeService, 'getLocaleFormat').and.returnValue(localesFormat['de-CH']);

            expect(inputCalculatorService.calculate(arithmeticString)).toBe(123440.2);
        })
    );

    it(
        'should return number without wrong arithmetics for en-GB locale',
        inject([GlobalizeService, InputCalculatorService], (globalizeService, inputCalculatorService) => {
            const arithmeticString = '123,431.20 + ';

            spyOn(globalizeService, 'getLocaleFormat').and.returnValue(localesFormat['en-GB']);
            spyOn(globalizeService, 'toNumber').and.callThrough();

            expect(inputCalculatorService.calculate(arithmeticString)).toBe(123431.2);
        })
    );

    it(
        'should return number for en-GB locale',
        inject([GlobalizeService, InputCalculatorService], (globalizeService, inputCalculatorService) => {
            const arithmeticString = '123,431.20 + (-0.5 + 5.5 * (4.5 + 5.5) / (3 + 2)) - 1.50';

            spyOn(globalizeService, 'getLocaleFormat').and.returnValue(localesFormat['en-GB']);

            expect(inputCalculatorService.calculate(arithmeticString)).toBe(123440.2);
        })
    );

    it(
        'should return number without wrong arithmetics for de-DE locale',
        inject([GlobalizeService, InputCalculatorService], (globalizeService, inputCalculatorService) => {
            const arithmeticString = '123.431,20 + ';

            spyOn(globalizeService, 'getLocaleFormat').and.returnValue(localesFormat['de-DE']);
            spyOn(globalizeService, 'toNumber').and.callThrough();

            expect(inputCalculatorService.calculate(arithmeticString)).toBe(123431.2);
        })
    );

    it(
        'should return number for de-DE locale',
        inject([GlobalizeService, InputCalculatorService], (globalizeService, inputCalculatorService) => {
            const arithmeticString = '123.431,20 + (-0,5 + 5,5 * (4,5 + 5,5) / (3 + 2)) - 1,50';

            spyOn(globalizeService, 'getLocaleFormat').and.returnValue(localesFormat['de-DE']);

            expect(inputCalculatorService.calculate(arithmeticString)).toBe(123440.2);
        })
    );

    it(
        'should return number without wrong arithmetics for de-AT locale',
        inject([GlobalizeService, InputCalculatorService], (globalizeService, inputCalculatorService) => {
            const arithmeticString = '123.431,20 + ';

            spyOn(globalizeService, 'getLocaleFormat').and.returnValue(localesFormat['de-AT']);
            spyOn(globalizeService, 'toNumber').and.callThrough();

            expect(inputCalculatorService.calculate(arithmeticString)).toBe(123431.2);
        })
    );

    it(
        'should return number for de-AT locale',
        inject([GlobalizeService, InputCalculatorService], (globalizeService, inputCalculatorService) => {
            const arithmeticString = '123.431,20 + (-0,5 + 5,5 * (4,5 + 5,5) / (3 + 2)) - 1,50';

            spyOn(globalizeService, 'getLocaleFormat').and.returnValue(localesFormat['de-AT']);

            expect(inputCalculatorService.calculate(arithmeticString)).toBe(123440.2);
        })
    );

    it(
        'should return number without wrong arithmetics for fr-CH locale',
        inject([GlobalizeService, InputCalculatorService], (globalizeService, inputCalculatorService) => {
            const arithmeticString = '123.431,20 + ';

            spyOn(globalizeService, 'getLocaleFormat').and.returnValue(localesFormat['fr-CH']);
            spyOn(globalizeService, 'toNumber').and.callThrough();

            expect(inputCalculatorService.calculate(arithmeticString)).toBe(123431.2);
        })
    );

    it(
        'should return for fr-CH locale',
        inject([GlobalizeService, InputCalculatorService], (globalizeService, inputCalculatorService) => {
            const arithmeticString = "123'431.20 + (-0.5 + 5.5 * (4.5 + 5.5) / (3 + 2)) - 1.50";

            spyOn(globalizeService, 'getLocaleFormat').and.returnValue(localesFormat['fr-CH']);

            expect(inputCalculatorService.calculate(arithmeticString)).toBe(123440.2);
        })
    );
});
