import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { Hotkey, HotkeysService } from 'angular2-hotkeys';

import { FormService } from './form.service';

const allowIn = ['INPUT'];

const toggleHelpPanelShortcut = '?';

const submitFormShortcut = 'shift+enter';

const addNewRowShortcut = 'mod+shift+down';

const removeRowShortcut = 'mod+shift+up';

@Injectable()
export class KeyboardShortcutsService {
    constructor(
        private formService: FormService,
        private hotkeysService: HotkeysService,
        private matDialog: MatDialog,
        private translateService: TranslateService
    ) {}

    init(): void {
        this.translateService
            .get([
                'globals.keyboard_shortcut.toggle_help_panel',
                'globals.keyboard_shortcut.submit_entry',
                'globals.keyboard_shortcut.new_row',
                'globals.keyboard_shortcut.remove_row'
            ])
            .subscribe(translations => {
                this.hotkeysService.add(
                    new Hotkey(
                        toggleHelpPanelShortcut,
                        () => {
                            this.hotkeysService.cheatSheetToggle.next({});
                            return false;
                        },
                        allowIn,
                        translations['globals.keyboard_shortcut.toggle_help_panel']
                    )
                );

                this.hotkeysService.add(
                    new Hotkey(
                        submitFormShortcut,
                        () => {
                            this.matDialog.closeAll();
                            this.formService.submitForm();
                            return false;
                        },
                        allowIn,
                        translations['globals.keyboard_shortcut.submit_entry']
                    )
                );

                this.hotkeysService.add(
                    new Hotkey(
                        addNewRowShortcut,
                        () => {
                            this.formService.addRow();
                            return false;
                        },
                        allowIn,
                        translations['globals.keyboard_shortcut.new_row']
                    )
                );

                this.hotkeysService.add(
                    new Hotkey(
                        removeRowShortcut,
                        () => {
                            this.formService.removeRow();
                            return false;
                        },
                        allowIn,
                        translations['globals.keyboard_shortcut.remove_row']
                    )
                );
            });
    }
}
