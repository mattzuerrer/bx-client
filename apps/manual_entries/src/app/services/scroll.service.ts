import { Injectable } from '@angular/core';
import { PageScrollConfig, PageScrollInstance, PageScrollService } from 'ng2-page-scroll/ng2-page-scroll';

const defaultDuration = 300;

const defaultScrollOffset = 100;

@Injectable()
export class ScrollService {
    scrollConfig: any = PageScrollConfig;

    scrollInstance: any = PageScrollInstance;

    private document: Document;

    constructor(private pageScrollService: PageScrollService) {}

    init(document: Document): void {
        this.document = document;
        this.scrollConfig.defaultDuration = defaultDuration;
        this.scrollConfig.defaultScrollOffset = defaultScrollOffset;
    }

    scrollToTop(): void {
        if (this.document) {
            setTimeout(
                () => this.pageScrollService.start(this.scrollInstance.simpleInstance(this.document, this.document.documentElement)),
                0
            );
        }
    }
}
