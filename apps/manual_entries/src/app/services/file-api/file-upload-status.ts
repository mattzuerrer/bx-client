import { FilterFunction } from 'ng2-file-upload';

import { FileUploadObject } from './file-upload-object';

/**
 * Status base class representing a file upload status.
 */
export abstract class FileUploadStatus {
    /**
     * Formats the errorValue to be displayed to the user.
     * @param file
     *   File to be uploaded
     * @param filter
     *   Criteria which is not met by file.
     * @returns {any}
     *   Formatted error value or null
     */
    static getErrorValue(file: FileUploadObject, filter: FilterFunction): any {
        if (file && filter) {
            switch (filter.name) {
                case 'fileSize':
                    const size: number = file.size / 1024 / 1024; // Bytes -> MB
                    return size.toFixed(2);
                case 'mimeType':
                    return file.type;
                default:
                    return null;
            }
        }
        return null;
    }

    /**
     * Create a new file upload status.
     *
     * @param errorType
     *   The translation key describing the error status, or null if it is no error status.
     *   Example: 'error.file_upload.server_error'
     * @param errorValue
     *   If errorType is set and translations contain variables (e. g. {{value}}) this is the actual
     *   value to replace the variable with.
     *   Example:
     *   - errorType: error.file_upload.file_invalid.fileSize
     *   - errorValue: 25.3 [file size in MB]
     */
    protected constructor(readonly errorType?: string, readonly errorValue?: any) {}

    /**
     * Whether this status represents an error.
     * @returns {boolean}
     */
    get isError(): boolean {
        return !!this.errorType;
    }
}

/**
 * Callback to notify the using component about a file upload status change, providing the status
 * information.
 */
export type FileStatusFn = (status: FileUploadStatus) => void;
