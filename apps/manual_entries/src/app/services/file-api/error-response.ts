/**
 * An error generated on the server
 */
export class ErrorMessage {
    /**
     * Error message
     */
    message: string;
}

/**
 * A list of errors
 */
interface ErrorMap {
    [key: string]: ErrorMessage;
}

/**
 * Error information received from the server
 */
export class ErrorResponse {
    /**
     * List of errors generated on the server
     */
    errors: ErrorMap;
}
