import { FileItem } from 'ng2-file-upload';

import { FileUploadStatus } from './file-upload-status';

/**
 * Represents an unknown error status and contains metadata about the uploaded file
 */
export class UnknownError extends FileUploadStatus {
    /**
     * @param file
     *   Metadata of the uploaded file
     */
    constructor(readonly file: FileItem) {
        super('error.file_upload.unknown_error');
    }
}
