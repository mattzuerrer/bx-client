/**
 * A file obect returned by ng2-file-upload. This class represents a FileLikeObject which is not
 * exported by ng2-file-upload.
 */
export interface FileUploadObject {
    lastModifiedDate: any;
    size: any;
    type: string;
    name: string;
}
