import { Inject, Injectable } from '@angular/core';
import { UtilitiesService } from '@bx-client/common';
import { apiToken, apiVersionToken } from '@bx-client/env';
import { plainToClass } from 'class-transformer';
import { FileItem, FilterFunction } from 'ng2-file-upload';

import { File } from '../../models';

import { AddFileFailure } from './add-file-failure';
import { ErrorResponse } from './error-response';
import { FileUploadObject } from './file-upload-object';
import { FileStatusFn } from './file-upload-status';
import { FileUploadSuccess } from './file-upload-success';
import { ServerError } from './server-error';
import { UnknownError } from './unknown-error';

/**
 * The FileApiService handles file uploads performed by the ng2-file-upload component. This
 * component implements uploading the files directly and cannot be integrated with state service or
 * RxJs in general. Therefore this service provides methods to build the upload url and handle
 * different callbacks. The callback prepare the raw
 * responses from the server or emitted event data from the component into @see FileUploadStatus instances and
 * notifies the host component via provided callback functions.
 */
@Injectable()
export class FileApiService {
    private static HTTP_CREATED = 201;

    constructor(
        private utilitiesService: UtilitiesService,
        @Inject(apiToken) private baseApi: string,
        @Inject(apiVersionToken) private apiVersion: AppEnvironment
    ) {}

    /**
     * Get url for uploading files into the inbox. Uploading is performed by ng2-file-upload
     *
     * @returns {string}
     */
    getInboxUploadUrl(): string {
        return `${this.baseApi}/${this.apiVersion}/files`;
    }

    /**
     * Handles failures to add a file to the upload queue, e. g. when a file is too large or its
     * type is not permitted.
     *
     * @param callback
     *   Callback funtion to notify the host component
     * @returns {(item:FileUploadObject, filter:FilterFunction)=>undefined}
     *   Event handler to assign to the ng2-file-upload component
     */
    handleAddingFileFailure(callback: FileStatusFn): (item: FileUploadObject, filter: FilterFunction) => void {
        return (item: FileUploadObject, filter: FilterFunction) => {
            if (callback) {
                callback(new AddFileFailure(item, filter));
            }
        };
    }

    /**
     * Handles a successful file upload.
     * As this service currently only supports creating new files but not overwriting/updating
     * existing
     * files on the server, any other HTTP status code than 201 (CREATED) results in an @see UnknownError.
     * If the raw response data cannot be converted into a @see File object, an UnknownError is generated as well.
     *
     * @param callback
     *   Callback funtion to notify the host component
     * @returns {(item:FileUploadObject, filter:FilterFunction)=>undefined}
     *   Event handler to assign to the ng2-file-upload component
     */
    handleFileUploadSuccess(callback: FileStatusFn): (item: FileItem, respone: string, status: number) => void {
        return (item: FileItem, response: string, status: number) => {
            if (callback) {
                if (status === FileApiService.HTTP_CREATED) {
                    try {
                        const file: File = plainToClass(File, this.utilitiesService.snakeToCamel(JSON.parse(response)[0]), {
                            ignoreDecorators: true
                        });
                        callback(new FileUploadSuccess(file));
                    } catch (err) {
                        callback(new UnknownError(item));
                    }
                } else {
                    callback(new UnknownError(item));
                }
            }
        };
    }

    /**
     * Handles upload errors returned by the server.
     * If the raw response data cannot be converted into a @see ErrorResponse object or no 'uploaded_file'
     * error is returned, an UnknownError is generated as well.
     *
     * @param callback
     *   Callback funtion to notify the host component
     * @returns {(item:FileUploadObject, filter:FilterFunction)=>undefined}
     *   Event handler to assign to the ng2-file-upload component
     */
    handleFileUploadError(callback: FileStatusFn): (item: FileItem, response: string) => void {
        return (item: FileItem, response: string) => {
            if (callback) {
                try {
                    const errorResponse: ErrorResponse = plainToClass(ErrorResponse, response);
                    if (errorResponse.errors && errorResponse.errors.uploaded_file) {
                        callback(new ServerError(item, errorResponse.errors.uploaded_file.message));
                    } else {
                        callback(new UnknownError(item));
                    }
                } catch (err) {
                    callback(new UnknownError(item));
                }
            }
        };
    }
}
