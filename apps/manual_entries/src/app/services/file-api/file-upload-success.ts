import { File } from '../../models';

import { FileUploadStatus } from './file-upload-status';

/**
 * Represents the success status of a file upload and contains metadata about the uploaded file
 */
export class FileUploadSuccess extends FileUploadStatus {
    /**
     * @param file
     *   Metadata of the uploaded file
     */
    constructor(readonly file: File) {
        super(null);
    }
}
