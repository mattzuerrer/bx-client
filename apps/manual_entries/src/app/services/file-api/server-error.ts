import { FileItem } from 'ng2-file-upload';

import { FileUploadStatus } from './file-upload-status';

/**
 * Represents an error status returned by the server and contains metadata about the uploaded file
 */
export class ServerError extends FileUploadStatus {
    /**
     * @param file
     *   Metadata of the uploaded file
     * @param serverErrorMessage
     *   Error message returned by the server
     */
    constructor(readonly file: FileItem, serverErrorMessage: string) {
        super('error.file_upload.server_error', serverErrorMessage);
    }
}
