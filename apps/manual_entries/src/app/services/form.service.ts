import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class FormService {
    private resetForm$: Subject<void> = new Subject<void>();

    private resetTemplate$: Subject<void> = new Subject<void>();

    private submitForm$: Subject<void> = new Subject<void>();

    private addRow$: Subject<void> = new Subject<void>();

    private removeRow$: Subject<void> = new Subject<void>();

    private focusRow$: Subject<number> = new Subject<number>();

    onResetForm$(): Observable<void> {
        return this.resetForm$.asObservable();
    }

    onResetTemplate$(): Observable<void> {
        return this.resetTemplate$.asObservable();
    }

    onSubmitForm$(): Observable<void> {
        return this.submitForm$.asObservable();
    }

    onAddRow$(): Observable<void> {
        return this.addRow$.asObservable();
    }

    onRemoveRow$(): Observable<void> {
        return this.removeRow$.asObservable();
    }

    onFocusRow$(): Observable<number> {
        return this.focusRow$.asObservable();
    }

    resetForm(): void {
        this.resetForm$.next();
    }

    resetTemplate(): void {
        this.resetTemplate$.next();
    }

    submitForm(): void {
        this.submitForm$.next();
    }

    addRow(): void {
        this.addRow$.next();
    }

    removeRow(): void {
        this.removeRow$.next();
    }

    /**
     * Trigger setting focus to the entry row defined by index.
     */
    triggerFocusRow(index: number): void {
        setTimeout(() => this.focusRow(index), 0);
    }

    /**
     * Set the entry row to focus. Do not call this directly as we have to wait for one UI cycle
     * until the components have been rendered and can receive events.
     */
    private focusRow(index: number): void {
        this.focusRow$.next(index);
    }
}
