import { DOCUMENT } from '@angular/common';
import { inject, TestBed } from '@angular/core/testing';
import { MessageService, MessageServiceMockProvider } from '@bx-client/message';

import {
    KeyboardShortcutsServiceMockProvider,
    ScrollServiceMockProvider,
    StateServiceMockProvider,
    TranslateServiceMockProvider
} from '../mocks/services';
import { AppService, KeyboardShortcutsService, ScrollService, StateService } from '../services';

const document = {};
const action = {
    type: 'test_action'
};

describe('App service', () => {
    const documentProvider = { provide: DOCUMENT, useValue: document };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                AppService,
                KeyboardShortcutsServiceMockProvider,
                ScrollServiceMockProvider,
                TranslateServiceMockProvider,
                MessageServiceMockProvider,
                StateServiceMockProvider,
                documentProvider
            ]
        });
    });

    it(
        'should init scroll service',
        inject([AppService, ScrollService], (appService, scrollService) => {
            spyOn(scrollService, 'init');
            appService.init();
            expect(scrollService.init).toHaveBeenCalledTimes(1);
            expect(scrollService.init).toHaveBeenCalledWith(document);
        })
    );

    it(
        'should init keyboard shortcuts service',
        inject([AppService, KeyboardShortcutsService], (appService, keyboardShortcutsService) => {
            spyOn(keyboardShortcutsService, 'init');
            appService.init();
            expect(keyboardShortcutsService.init).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should emit actions from message service',
        inject([AppService, MessageService, StateService], (appService, messageService, stateService) => {
            spyOn(stateService, 'dispatch');
            appService.init();
            messageService.action$.next(action);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(action);
        })
    );
});
