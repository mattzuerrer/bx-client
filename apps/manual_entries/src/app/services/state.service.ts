import { Injectable } from '@angular/core';
import { byId, identity } from '@bx-client/common';
import { Account, AccountSelectors } from '@bx-client/features/account';
import { Company, CompanySelectors } from '@bx-client/features/company';
import { CurrencySelectors } from '@bx-client/features/currency';
import { Action, Store } from '@ngrx/store';
import { combineLatest as observableCombineLatest, Observable } from 'rxjs';

import {
    ColumnStatus,
    Currency,
    Dictionary,
    File,
    FormState,
    ManualEntry,
    ManualEntryCollection,
    ManualEntryTemplate,
    Tax
} from '../models';
import * as fromRoot from '../reducers';

@Injectable()
export class StateService {
    listEntries$: Observable<ManualEntryCollection[]> = observableCombineLatest(
        this.store.select(fromRoot.getListEntries),
        this.store.select(AccountSelectors.getCollection),
        this.store.select(CurrencySelectors.getCollection),
        (manualEntryCollections: ManualEntryCollection[], accounts: Account[], currencies: Currency[]) =>
            manualEntryCollections.map(collection =>
                Object.assign(new ManualEntryCollection(), collection, {
                    entries: collection.entries.map((entry: ManualEntry) =>
                        Object.assign(new ManualEntry(), entry, {
                            creditAccount: accounts.find(byId(entry.creditAccountId)),
                            debitAccount: accounts.find(byId(entry.debitAccountId)),
                            currency: currencies.find(byId(entry.currencyId)),
                            baseCurrency: currencies.find(byId(entry.baseCurrencyId))
                        })
                    )
                })
            )
    );

    statesLoaded$: Observable<boolean> = observableCombineLatest(
        this.store.select(fromRoot.getListLoaded),
        this.store.select(AccountSelectors.getLoaded),
        this.store.select(CurrencySelectors.getLoaded),
        this.store.select(fromRoot.getTaxesLoaded),
        this.store.select(CompanySelectors.getLoaded),
        this.store.select(fromRoot.getEntryCollectionLoaded),
        this.store.select(fromRoot.getTemplatesLoaded),
        (...isLoadedStates: boolean[]) => isLoadedStates.every(identity)
    );

    accountGroupsLoaded$: Observable<boolean> = observableCombineLatest(
        this.store.select(AccountSelectors.getLoadedGroups),
        (...isLoadedStates: boolean[]) => isLoadedStates.every(identity)
    );

    accountsWithCurencies$: Observable<Account[]> = observableCombineLatest(
        this.store.select(AccountSelectors.getCollection),
        this.store.select(CurrencySelectors.getCollection),
        (accounts: Account[], currencies: Currency[]) =>
            accounts.map(account => Object.assign(new Account(), account, { currency: currencies.find(byId(account.currencyId)) }))
    );

    company$: Observable<Company> = this.store.select(CompanySelectors.getCompany);

    manualEntryCollection$: Observable<ManualEntryCollection> = observableCombineLatest(
        this.store.select(fromRoot.getEntryCollectionEntry),
        this.company$,
        (manualEntryCollection: ManualEntryCollection, company: Company) =>
            Object.assign(new ManualEntryCollection(), manualEntryCollection, {
                entries: manualEntryCollection.entries.map(entry =>
                    Object.assign(new ManualEntry(), entry, {
                        currencyId: entry.currencyId ? entry.currencyId : company.currencyId,
                        baseCurrencyId: entry.baseCurrencyId ? entry.baseCurrencyId : company.currencyId,
                        files: entry.files.map((file: File) => Object.assign(new File(), file))
                    })
                )
            })
    );

    formState$: Observable<FormState> = this.store.select(fromRoot.getFormState);

    currencies$: Observable<Currency[]> = this.store.select(CurrencySelectors.getCollection);

    taxes$: Observable<Tax[]> = this.store.select(fromRoot.getTaxesCollection);

    autofillState$: Observable<Dictionary<string[]>> = this.store.select(fromRoot.getAutofillState);

    autofillDescriptions$: Observable<string[]> = this.store.select(fromRoot.getAutofillCollection('description'));

    templates$: Observable<ManualEntryTemplate[]> = observableCombineLatest(
        this.store.select(fromRoot.getTemplatesCollection),
        (templates: ManualEntryTemplate[]) =>
            templates.map(template =>
                Object.assign(new ManualEntryTemplate(), template, {
                    collection: Object.assign(new ManualEntryCollection(), template.collection, {
                        entries: template.collection.entries.map((entry: ManualEntry) => Object.assign(new ManualEntry(), entry))
                    })
                })
            )
    );

    files$: Observable<File[]> = this.store.select(fromRoot.getFilesCollection);

    fileUploadDialogFiles$: Observable<File[]> = this.store.select(fromRoot.getDialogFilesState);

    fileUploadDialogFilesAdded$: Observable<File[]> = this.store.select(fromRoot.getAddedDialogFilesState);

    fileUploadDialogFilesRemoved$: Observable<File[]> = this.store.select(fromRoot.getRemovedDialogFilesState);

    columnConfig$: Observable<ColumnStatus[]> = this.store.select(fromRoot.getColumnConfigState);

    constructor(private store: Store<fromRoot.State>) {}

    dispatch(action: Action): void {
        this.store.dispatch(action);
    }
}
