import { publishLast, refCount } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { UtilitiesService } from '@bx-client/common';
import { HttpService } from '@bx-client/http';
import { classToPlain } from 'class-transformer';
import { Observable } from 'rxjs';

import { Currency, File, FileConnection, ManualEntryCollection, ManualEntryTemplate, RefNumber, Tax } from '../models';

import { excludeAllStrategy, group } from '../models/serialization';

@Injectable()
export class ApiService {
    constructor(private httpService: HttpService, private utilitiesService: UtilitiesService) {}

    /**
     * Get manual entries.
     *
     * @param {string} orderBy
     * @returns {Observable<ManualEntryCollection[]>}
     */
    getManualEntryCollections(orderBy: string): Observable<ManualEntryCollection[]> {
        const httpOptions = {
            classTransformerOptions: {
                ignoreDecorators: true
            }
        };

        return this.httpService.get(
            ManualEntryCollection,
            `accounting/manual_entries?scope=manual_entries&embed=tax,file&limit=50&order_by=${orderBy}`,
            httpOptions
        );
    }

    /**
     * Get manual entry.
     *
     * @param {number} id
     * @returns {Observable<ManualEntryCollection>}
     */
    getManualEntryCollection(id: number): Observable<ManualEntryCollection> {
        const httpOptions = {
            classTransformerOptions: {
                ignoreDecorators: true
            }
        };

        return this.httpService.get(ManualEntryCollection, `accounting/manual_entries/${id}?embed=file,tax`, httpOptions);
    }

    /**
     * Get currencies.
     *
     * @returns {Observable<Currency[]>}
     */
    getCurrencies(): Observable<Currency[]> {
        return this.httpService.get(Currency, 'currencies?embed=exchange_rate');
    }

    /**
     * Get next reference number.
     *
     * @returns {Observable<RefNumber>}
     */
    getNextRefNumber(): Observable<RefNumber> {
        return this.httpService.get(RefNumber, 'accounting/manual_entries/next_ref_nr');
    }

    /**
     * Get taxes for given date.
     *
     * @param {string} date
     * @returns {Observable<Tax[]>}
     */
    getTaxes(date: string): Observable<Tax[]> {
        return this.httpService.get(Tax, `taxes?date=${date}`).pipe(publishLast(), refCount());
    }

    /**
     * Post a ManualEntryCollection.
     *
     * @param {ManualEntryCollection} manualEntryCollection
     * @returns {Observable<ManualEntryCollection>}
     *
     */
    createCollection(manualEntryCollection: ManualEntryCollection): Observable<ManualEntryCollection> {
        const body = JSON.stringify(
            this.utilitiesService.camelToSnake(
                classToPlain(manualEntryCollection, {
                    strategy: excludeAllStrategy,
                    groups: [group.manualEntryCollection]
                })
            )
        );

        const httpOptions = {
            classTransformerOptions: {
                ignoreDecorators: true
            }
        };

        return this.httpService.post(ManualEntryCollection, 'accounting/manual_entries', body, httpOptions);
    }

    /**
     * Update a ManualEntryCollection.
     *
     * @param {ManualEntryCollection} manualEntryCollection
     * @returns {Observable<ManualEntryCollection>}
     */
    updateCollection(manualEntryCollection: ManualEntryCollection): Observable<ManualEntryCollection> {
        const body = JSON.stringify(
            this.utilitiesService.camelToSnake(
                classToPlain(manualEntryCollection, {
                    strategy: excludeAllStrategy,
                    groups: [group.manualEntryCollection]
                })
            )
        );

        const httpOptions = {
            classTransformerOptions: {
                ignoreDecorators: true
            }
        };

        return this.httpService.put(
            ManualEntryCollection,
            `accounting/manual_entries/${manualEntryCollection.id}?embed=file`,
            body,
            httpOptions
        );
    }

    /**
     * Delete manual entry.
     *
     * @param {number} id
     * @returns {Observable<number>}
     */
    deleteManualEntryCollection(id: number): Observable<number> {
        return this.httpService.remove(null, `accounting/manual_entries/${id}`);
    }

    /**
     * Get manual entry templates.
     *
     * @returns {Observable<ManualEntryTemplate[]>}
     */
    getManualEntryTemplates(): Observable<ManualEntryTemplate[]> {
        const httpOptions = {
            classTransformerOptions: {
                ignoreDecorators: true
            }
        };

        return this.httpService.get(ManualEntryTemplate, 'accounting/manual_entry_templates?order_by=name', httpOptions);
    }

    /**
     * Post a ManualEntryTemplate.
     *
     * @param {ManualEntryTemplate} template
     * @returns {Observable<ManualEntryTemplate>}
     */
    createManualEntryTemplate(template: ManualEntryTemplate): Observable<ManualEntryTemplate> {
        const body = JSON.stringify(
            this.utilitiesService.camelToSnake(
                classToPlain(template, { strategy: excludeAllStrategy, groups: [group.manualEntryTemplate] })
            )
        );

        const httpOptions = {
            classTransformerOptions: {
                ignoreDecorators: true
            }
        };

        return this.httpService.post(ManualEntryTemplate, 'accounting/manual_entry_templates', body, httpOptions);
    }

    /**
     * Update a ManualEntryTemplate.
     *
     * @param {ManualEntryTemplate} template
     * @returns {Observable<ManualEntryTemplate>}
     */
    updateManualEntryTemplate(template: ManualEntryTemplate): Observable<ManualEntryTemplate> {
        const body = JSON.stringify(
            this.utilitiesService.camelToSnake(
                classToPlain(template, { strategy: excludeAllStrategy, groups: [group.manualEntryTemplate] })
            )
        );

        const httpOptions = {
            classTransformerOptions: {
                ignoreDecorators: true
            }
        };

        return this.httpService.put(ManualEntryTemplate, `accounting/manual_entry_templates/${template.id}`, body, httpOptions);
    }

    /**
     * Delete manual entry template.
     *
     * @param {number} id
     * @returns {Observable<number>}
     */
    deleteManualEntryTemplate(id: number): Observable<number> {
        return this.httpService.remove(null, `accounting/manual_entry_templates/${id}`);
    }

    /**
     * Get files from inbox
     *
     * @returns {Observable<File[]>}
     */
    getFiles(): Observable<File[]> {
        return this.httpService.get(File, 'files?embed=user&order_by=name').pipe(publishLast(), refCount());
    }

    /**
     * Get file from inbox
     *
     * @returns {Observable<File>}
     */
    getFile(id: number): Observable<File> {
        return this.httpService.get(File, `files/${id}?embed=user`);
    }

    /**
     * Connects file with ManualEntry
     *
     * @param {FileConnection} connection
     * @returns {Observable<File>}
     */
    connectFile(connection: FileConnection): Observable<File> {
        if (!connection.entryId) {
            return this.connectFileOnCollection(connection);
        }
        return this.httpService.post(
            File,
            `accounting/manual_entries/${connection.collectionId}/entries/${connection.entryId}/files/${connection.fileId}/connect`
        );
    }

    /**
     * Disconnects file from ManualEntry
     *
     * @param {FileConnection} connection
     * @returns {Observable<File>}
     */
    disconnectFile(connection: FileConnection): Observable<File> {
        if (!connection.entryId) {
            return this.disconnectFileOnCollection(connection);
        }
        return this.httpService.remove(
            File,
            `accounting/manual_entries/${connection.collectionId}/entries/${connection.entryId}/files/${connection.fileId}/disconnect`
        );
    }

    /**
     * Download a file.
     *
     * @param {File} file
     * @returns {Observable<Blob>}
     */
    downloadFile(file: File): Observable<Blob> {
        return this.httpService.get(null, `files/${file.id}/download`, { responseType: 'blob' });
    }

    /**
     * Connect a file.
     *
     * @param {FileConnection} connection
     * @returns {Observable<File>}
     */
    private connectFileOnCollection(connection: FileConnection): Observable<File> {
        return this.httpService.post(File, `accounting/manual_entries/${connection.collectionId}/files/${connection.fileId}/connect`);
    }

    /**
     * Disconnect a file.
     *
     * @param {FileConnection} connection
     * @returns {Observable<File>}
     */
    private disconnectFileOnCollection(connection: FileConnection): Observable<File> {
        return this.httpService.remove(File, `accounting/manual_entries/${connection.collectionId}/files/${connection.fileId}/disconnect`);
    }
}
