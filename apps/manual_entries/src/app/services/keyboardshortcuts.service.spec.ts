import { inject, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material';
import { Hotkey, HotkeysService } from 'angular2-hotkeys';

import { FormServiceMockProvider, TranslateServiceMockProvider } from '../mocks/services';

import { FormService } from './form.service';
import { KeyboardShortcutsService } from './keyboardshortcuts.service';

const hotkeysServiceStub = {
    hotkeys: [],

    cheatSheetToggle: { next: () => null },

    add(hotkey: Hotkey): void {
        this.hotkeys.push(hotkey);
    }
};

describe('Keyboard shortcuts service', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [MatDialogModule],
            providers: [
                MatDialog,
                KeyboardShortcutsService,
                FormServiceMockProvider,
                TranslateServiceMockProvider,
                { provide: HotkeysService, useValue: hotkeysServiceStub }
            ]
        });
    });

    it(
        'should toggle help panel',
        inject([KeyboardShortcutsService, HotkeysService], (keyboardShortcutsService, hotkeysService) => {
            spyOn(hotkeysService.cheatSheetToggle, 'next');
            keyboardShortcutsService.init();
            const hotkey = hotkeysService.hotkeys[0];
            hotkey.callback();
            expect(hotkey.combo).toEqual(['?']);
            expect(hotkeysService.cheatSheetToggle.next).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should submit form',
        inject([KeyboardShortcutsService, HotkeysService, FormService], (keyboardShortcutsService, hotkeysService, formService) => {
            spyOn(formService, 'submitForm');
            keyboardShortcutsService.init();
            const hotkey = hotkeysService.hotkeys[1];
            hotkey.callback();
            expect(hotkey.combo).toEqual(['shift+enter']);
            expect(formService.submitForm).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should add new row',
        inject([KeyboardShortcutsService, HotkeysService, FormService], (keyboardShortcutsService, hotkeysService, formService) => {
            spyOn(formService, 'addRow');
            keyboardShortcutsService.init();
            const hotkey = hotkeysService.hotkeys[2];
            hotkey.callback();
            expect(hotkey.combo).toEqual(['mod+shift+down']);
            expect(formService.addRow).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should remove a row',
        inject([KeyboardShortcutsService, HotkeysService, FormService], (keyboardShortcutsService, hotkeysService, formService) => {
            spyOn(formService, 'removeRow');
            keyboardShortcutsService.init();
            const hotkey = hotkeysService.hotkeys[3];
            hotkey.callback();
            expect(hotkey.combo).toEqual(['mod+shift+up']);
            expect(formService.removeRow).toHaveBeenCalledTimes(1);
        })
    );
});
