import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { MessageService } from '@bx-client/message';

import { KeyboardShortcutsService } from './keyboardshortcuts.service';
import { ScrollService } from './scroll.service';
import { StateService } from './state.service';

@Injectable()
export class AppService {
    constructor(
        private scrollService: ScrollService,
        private keyboardShortcutsService: KeyboardShortcutsService,
        private messageService: MessageService,
        private stateService: StateService,
        @Inject(DOCUMENT) private document: any
    ) {}

    init(): void {
        this.initScrollService();
        this.initKeyboardShortcutsService();
        this.initMessageService();
    }

    private initScrollService(): void {
        this.scrollService.init(this.document);
    }

    private initKeyboardShortcutsService(): void {
        this.keyboardShortcutsService.init();
    }

    private initMessageService(): void {
        this.messageService.action$.subscribe(action => this.stateService.dispatch(action));
    }
}
