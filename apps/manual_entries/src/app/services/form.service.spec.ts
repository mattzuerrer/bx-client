import { noop } from 'rxjs';

import { FormService } from './form.service';

describe('Form service', () => {
    let formService: FormService;

    const callable = {
        exec(event: string): void {
            noop();
        }
    };

    beforeEach(() => {
        formService = new FormService();
    });

    it('should have observable reset form property', () => {
        spyOn(callable, 'exec');
        formService.onResetForm$().subscribe(() => callable.exec('RESET_FORM'));
        expect(callable.exec).toHaveBeenCalledTimes(0);
        formService.resetForm();
        expect(callable.exec).toHaveBeenCalledTimes(1);
        expect(callable.exec).toHaveBeenCalledWith('RESET_FORM');
    });

    it('should have observable submit form property', () => {
        spyOn(callable, 'exec');
        formService.onSubmitForm$().subscribe(() => callable.exec('SUBMIT_FORM'));
        expect(callable.exec).toHaveBeenCalledTimes(0);
        formService.submitForm();
        expect(callable.exec).toHaveBeenCalledTimes(1);
        expect(callable.exec).toHaveBeenCalledWith('SUBMIT_FORM');
    });

    it('should have observable add row property', () => {
        spyOn(callable, 'exec');
        formService.onAddRow$().subscribe(() => callable.exec('ADD_ROW'));
        expect(callable.exec).toHaveBeenCalledTimes(0);
        formService.addRow();
        expect(callable.exec).toHaveBeenCalledTimes(1);
        expect(callable.exec).toHaveBeenCalledWith('ADD_ROW');
    });

    it('should have observable remove row property', () => {
        spyOn(callable, 'exec');
        formService.onRemoveRow$().subscribe(() => callable.exec('REMOVE_ROW'));
        expect(callable.exec).toHaveBeenCalledTimes(0);
        formService.removeRow();
        expect(callable.exec).toHaveBeenCalledTimes(1);
        expect(callable.exec).toHaveBeenCalledWith('REMOVE_ROW');
    });

    it('should call observable focus row if focussing row is triggered', () => {
        jasmine.clock().uninstall();
        jasmine.clock().install();

        spyOn(callable, 'exec');
        formService.onFocusRow$().subscribe(() => callable.exec('FOCUS_ROW'));
        expect(callable.exec).toHaveBeenCalledTimes(0);
        formService.triggerFocusRow(0);
        // this executes the setTimeout call in triggerFocusRow()
        jasmine.clock().tick(0);
        expect(callable.exec).toHaveBeenCalledTimes(1);
        expect(callable.exec).toHaveBeenCalledWith('FOCUS_ROW');

        jasmine.clock().uninstall();
    });
});
