import { Injectable } from '@angular/core';
import { GlobalizeService } from '@bx-client/i18n';

// Using function constructor as alternate eval
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function
const evaluate: (fn: any) => any = fn => new Function('return ' + fn)();

@Injectable()
export class InputCalculatorService {
    constructor(private globalizeService: GlobalizeService) {}

    calculate(value: string): number {
        const sanitizedString: string = value.replace(/[^0-9\s,.+\-*\/()]/g, '');
        const validArithmeticString: string = this.normalizeArithmeticString(sanitizedString);

        try {
            return evaluate(validArithmeticString);
        } catch {
            return this.globalizeService.toNumber(value);
        }
    }

    private normalizeArithmeticString(value: string): string {
        const delimiters = this.globalizeService.getLocaleFormat().delimiters;

        return value
            .split(delimiters.thousands)
            .join('')
            .split(delimiters.decimal)
            .join('.');
    }
}
