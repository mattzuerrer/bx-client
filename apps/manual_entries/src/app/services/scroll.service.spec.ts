import { inject, TestBed } from '@angular/core/testing';
import { PageScrollService } from 'ng2-page-scroll/ng2-page-scroll';

import { ScrollService } from './scroll.service';

const pageScrollServiceMock = {
    start: () => null
};

const document = {
    documentElement: 'documentElement'
};

const scrollInstance = {
    simpleInstance: () => 'scrollSimpleInstance'
};

describe('Scroll service', () => {
    const pageScrollServiceProvider = { provide: PageScrollService, useValue: pageScrollServiceMock };

    beforeEach(() => {
        jasmine.clock().uninstall();
        jasmine.clock().install();
        TestBed.configureTestingModule({ providers: [ScrollService, pageScrollServiceProvider] });
    });

    afterEach(() => {
        jasmine.clock().uninstall();
    });

    it(
        'should init',
        inject([ScrollService], scrollService => {
            scrollService.scrollConfig = {};
            scrollService.scrollInstance = scrollInstance;
            scrollService.init(document);
            expect(scrollService.scrollConfig.defaultDuration).toBe(300);
            expect(scrollService.scrollConfig.defaultScrollOffset).toBe(100);
        })
    );

    it(
        'should create new scroll instance',
        inject([ScrollService], scrollService => {
            scrollService.scrollConfig = {};
            scrollService.scrollInstance = scrollInstance;
            scrollService.init(document);
            spyOn(scrollService.scrollInstance, 'simpleInstance');
            scrollService.scrollToTop();
            jasmine.clock().tick(1);
            expect(scrollInstance.simpleInstance).toHaveBeenCalledTimes(1);
            expect(scrollInstance.simpleInstance).toHaveBeenCalledWith(document, document.documentElement);
        })
    );

    it(
        'should call page scroll service',
        inject([ScrollService, PageScrollService], (scrollService, pageScrollService) => {
            scrollService.scrollConfig = {};
            scrollService.scrollInstance = scrollInstance;
            scrollService.init(document);
            spyOn(pageScrollService, 'start');
            scrollService.scrollToTop();
            jasmine.clock().tick(1);
            expect(pageScrollService.start).toHaveBeenCalledTimes(1);
            expect(pageScrollService.start).toHaveBeenCalledWith('scrollSimpleInstance');
        })
    );
});
