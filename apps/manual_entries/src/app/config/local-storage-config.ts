import { Injectable } from '@angular/core';
import { LocalStorageConfigInterface } from 'libs/local-storage/index';

@Injectable()
export class LocalStorageConfig implements LocalStorageConfigInterface {
    appPrefix = 'manual-entries';
    keys = {
        autofillState: 'autofill-state'
    };
}
