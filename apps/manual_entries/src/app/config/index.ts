export const version = '_BEXIO_FE_DEPLOYMENT_REPLACED_VERSION_';
export const requirements = 'requirements';
export const liabilities = 'liabilities';
export const vat = 'vat';

export const internalKeys = {
    [requirements]: ['1100'],
    [liabilities]: ['2000'],
    [vat]: ['1170', '1171', '1172', '2200', '2202']
};

export const featureName = 'manualEntries';

export const mimeTypes = [
    'image/png',
    'image/jpeg',
    'image/gif',
    'application/pdf',
    'application/x-pdf',
    'application/msword',
    'application/vnd.ms-excel',
    'application/vnd.ms-powerpoint',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
];
