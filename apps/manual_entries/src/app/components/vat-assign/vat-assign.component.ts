import { Component, Input, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import { ManualEntry } from '../../models';

@Component({
    selector: 'app-vat-assign',
    templateUrl: 'vat-assign.component.html',
    styleUrls: ['vat-assign.component.scss']
})
export class VatAssignComponent implements OnInit {
    @Input() rows: ManualEntry[];

    constructor(public dialogRef: MatDialogRef<VatAssignComponent>) {}

    ngOnInit(): void {
        this.rows.forEach(row => (row.taxAccountId = row.debitAccount.id));
    }

    closeDialog(): void {
        this.dialogRef.close(this.rows);
    }
}
