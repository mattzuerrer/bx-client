import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material';
import { Subject } from 'rxjs';

import { ManualEntryTemplate } from '../../models';

const formActionsDebounce = 200;

// Minimum length a template name must have. Check: length >= canSaveMinimumNameLength
const canSaveMinimumNameLength = 1;

@Component({
    selector: 'app-save-template',
    templateUrl: 'save-template-dialog.component.html',
    styleUrls: ['save-template-dialog.component.scss']
})
export class SaveTemplateDialogComponent implements OnInit, OnDestroy {
    static dialogWidth = '480px';

    static open(
        mdDialog: MatDialog,
        existingTemplates: ManualEntryTemplate[],
        loadedTemplate?: ManualEntryTemplate
    ): MatDialogRef<SaveTemplateDialogComponent> {
        const config: MatDialogConfig = { width: SaveTemplateDialogComponent.dialogWidth };
        const dialog = mdDialog.open(SaveTemplateDialogComponent, config);
        dialog.componentInstance.templates = existingTemplates;
        if (loadedTemplate) {
            dialog.componentInstance.templateName = loadedTemplate.name;
            dialog.componentInstance._isLoadedTemplate = true;
        }
        return dialog;
    }

    templateName = '';

    @ViewChild('nameInput') nameInput: ElementRef;
    private _isLoadedTemplate = false;
    private templates: ManualEntryTemplate[];
    private _nameExists = false;

    private templateInput: Subject<string> = new Subject<string>();

    constructor(private dialogRef: MatDialogRef<SaveTemplateDialogComponent>) {}

    ngOnInit(): void {
        this.templateInput
            .pipe(
                debounceTime(formActionsDebounce),
                distinctUntilChanged(),
                map(input => this.templates.filter(this.filterTemplates(input)).length)
            )
            .subscribe(count => (this._nameExists = count > 0));
        this.checkName(this.templateName);
    }

    ngOnDestroy(): void {
        this.templateInput.unsubscribe();
    }

    closeDialog(): void {
        let template: ManualEntryTemplate = this.templates.find(this.filterTemplates(this.templateName));
        if (template) {
            template = Object.assign(new ManualEntryTemplate(), template);
        } else {
            template = new ManualEntryTemplate();
            template.name = this.templateName;
        }
        this.dialogRef.close(template);
    }

    get canSave(): boolean {
        return this.templateName.length >= canSaveMinimumNameLength;
    }

    get isLoadedTemplate(): boolean {
        return this._isLoadedTemplate;
    }

    get nameExists(): boolean {
        return this._nameExists;
    }

    checkName(name: string): void {
        this.templateInput.next(name);
    }

    private filterTemplates(query: string): (template: ManualEntryTemplate) => boolean {
        const name = this.templateName;
        return template => template.name === name;
    }
}
