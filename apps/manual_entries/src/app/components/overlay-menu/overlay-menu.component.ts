import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material';
import { CdnService } from '@bx-client/core/src/environments/service/cdn.service';

import { version } from '../../config';
import { MenuAction } from '../../models';

@Component({ selector: 'app-overlay-menu', templateUrl: 'overlay-menu.component.html' })
export class OverlayMenuComponent {
    @Input() actions: MenuAction[];

    @Output() menuSelect: EventEmitter<MenuAction> = new EventEmitter<MenuAction>();

    @ViewChild(MatMenuTrigger) overlayMenu: MatMenuTrigger;

    assetUrl: string = this.cdn.getAssetUrl(version);

    constructor(private cdn: CdnService) {}

    get hasActions(): boolean {
        return this.actions && this.actions.length > 0;
    }

    onMenuItemClick(action: MenuAction): void {
        this.menuSelect.emit(action);
    }

    openMenu(): void {
        this.overlayMenu.openMenu();
    }
}
