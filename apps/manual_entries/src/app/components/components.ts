import { BookingEntryComponent } from './booking-entry/booking-entry.component';
import { BookingFormComponent } from './booking-form/booking-form.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { FileUploadDialogComponent } from './file-upload-dialog/file-upload-dialog.component';
import { ListSortComponent } from './list-sort/list-sort.component';
import { ListComponent } from './list/list.component';
import { OverlayMenuComponent } from './overlay-menu/overlay-menu.component';
import { SaveTemplateDialogComponent } from './save-template-dialog/save-template-dialog.component';
import { TapDateComponent } from './tap-date/tap-date.component';
import { VatAssignComponent } from './vat-assign/vat-assign.component';

export const appComponents = [
    BookingEntryComponent,
    BookingFormComponent,
    TapDateComponent,
    ListComponent,
    ListSortComponent,
    VatAssignComponent,
    ConfirmDialogComponent,
    OverlayMenuComponent,
    FileUploadDialogComponent,
    SaveTemplateDialogComponent
];

export const appDynamicComponents = [VatAssignComponent, ConfirmDialogComponent, SaveTemplateDialogComponent, FileUploadDialogComponent];
