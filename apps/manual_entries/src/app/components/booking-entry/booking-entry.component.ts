import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {
    ariaInvalidFn,
    ariaLiveFn,
    byId,
    formAriaInvalidFn,
    formAriaLiveFn,
    isActive,
    makeAriaInvalidFn,
    makeAriaLiveFn,
    makeFormAriaInvalidFn,
    makeFormAriaLiveFn,
    or,
    Validators as CustomValidators
} from '@bx-client/common';
import { IndagiaService } from '@bx-client/common/src/services';
import { Account, AccountAutocompleteComponent, AccountAutocompleteConfig } from '@bx-client/features/account';
import { Company } from '@bx-client/features/company';
import { defaultNumberFormat, GlobalizeService } from '@bx-client/i18n';
import { twoDecimalPlacesFormat } from '@bx-client/i18n/src/config/number-format';
import { Subscription } from 'rxjs';
import { filter, startWith } from 'rxjs/operators';
import { isNumber } from 'util';

import { Currency, Item, ManualEntryCollection, Tax } from '../../models';
import { FormService, InputCalculatorService } from '../../services';

@Component({
    selector: 'app-booking-entry',
    templateUrl: 'booking-entry.component.html',
    styleUrls: ['booking-entry.component.scss']
})
export class BookingEntryComponent implements OnInit, OnDestroy {
    @Input() currencies: Currency[] = [];

    @Input() accounts: Account[] = [];

    @Input() taxes: Tax[] = [];

    @Input()
    set autofillDescriptions(values: string[]) {
        if (values) {
            this.descriptions = values.map(value => ({ id: value, name: value, format: () => value }));
        }
    }

    @Input() entry: FormGroup;

    @Input() company: Company = null;

    @Input() index = 0;

    @Input() isLocked = false;

    @Input() accountGroupsLoaded = false;

    @Input() date: Date;

    @Output() amountBlur: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild('debit') debit: AccountAutocompleteComponent;

    @Input()
    set type(value: string) {
        this.entry.get('type').patchValue(value);
    }

    accountAutocompleteConfig: AccountAutocompleteConfig = {
        inputAccountNo: true,
        inputAccountName: false,
        listAccountNo: true,
        listAccountName: true,
        listAccountAmount: true
    };

    isAriaInvalid: ariaInvalidFn;

    ariaLive: ariaLiveFn;

    formAriaInvalid: formAriaInvalidFn;

    formAriaLive: formAriaLiveFn;

    descriptions: Item[] = [];

    private subscriptions: Subscription[] = [];

    constructor(
        private globalizeService: GlobalizeService,
        private formService: FormService,
        private inputCalculatorService: InputCalculatorService,
        private indagiaService: IndagiaService
    ) {}

    ngOnInit(): void {
        this.subscriptions.push(this.entry.valueChanges.pipe(startWith(null)).subscribe(() => this.handleValueChanges()));
        this.subscriptions.push(
            this.formService
                .onFocusRow$()
                .pipe(filter(index => this.index === index))
                .subscribe(() => this.debit.focus())
        );
        this.isAriaInvalid = makeAriaInvalidFn(this.entry);
        this.ariaLive = makeAriaLiveFn(this.entry);
        this.formAriaInvalid = makeFormAriaInvalidFn(this.entry);
        this.formAriaLive = makeFormAriaLiveFn(this.entry);
    }

    getActiveAccounts(selectedAccountId: number): Account[] {
        return this.accounts.filter(or(isActive, byId(selectedAccountId)));
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    onSelectCurrency(currencyId: number): void {
        const currency = this.currencies.find(byId(currencyId));
        if (currency && currency.exchangeRate) {
            const currencyFactor = currency.exchangeRate;
            const currencyFactorFormatted = this.globalizeService.toNumber(
                this.globalizeService.toCurrency(currencyFactor, defaultNumberFormat)
            );
            const amount = this.format(this.amount.value);
            const baseCurrencyAmount = amount * currencyFactorFormatted;

            this.currencyFactor.patchValue(currencyFactorFormatted);
            this.baseCurrencyAmount.patchValue(this.format(baseCurrencyAmount));
        }
    }

    onSelectAccount(account: Account): void {
        if (account) {
            const taxId = account.taxId;
            if (taxId) {
                this.entry.get('taxId').patchValue(taxId);
            }
        }
    }

    onSwitchAccounts(): void {
        this.entry.patchValue({
            debitAccountId: this.entry.get('creditAccountId').value,
            creditAccountId: this.entry.get('debitAccountId').value
        });

        this.entry.markAsDirty();
    }

    calculate(value: string): void {
        this.amount.patchValue(this.format(this.inputCalculatorService.calculate(value)));
    }

    baseCurrencyAmountFromAmount(): void {
        const amount = this.format(this.amount.value) ;
        const currencyFactor = this.currencyFactor.value;
        const baseCurrencyAmount = amount * currencyFactor;

        this.baseCurrencyAmount.patchValue(this.format(baseCurrencyAmount));
    }

    baseCurrencyAmountFromCurrency(value: string): void {
        const currencyFactor = this.globalizeService.toNumber(value);
        const amount = this.format(this.amount.value);
        const baseCurrencyAmount = amount * (isNaN(currencyFactor) ? 1 : currencyFactor);

        this.baseCurrencyAmount.patchValue(this.format(baseCurrencyAmount));
    }

    currencyFactorFromBaseCurrencyAmount(): void {
        const baseCurrencyAmount = this.format(this.baseCurrencyAmount.value);
        const amount = this.format(this.amount.value);

        const currencyFactor = baseCurrencyAmount / amount;

        if (currencyFactor && currencyFactor !== Infinity) {
            const currencyFactorFormatted = this.globalizeService.toNumber(
                this.globalizeService.toCurrency(currencyFactor, defaultNumberFormat)
            );
            this.currencyFactor.patchValue(currencyFactorFormatted);
        }
    }

    hasAccount(account: Account): boolean {
        return account && isNumber(account.id) && account.id > 0;
    }

    showCompoundLabel(key: string): boolean {
        return this.isCompoundEntryCollection && this.index === 0 && !!this.entry.get(key).value;
    }

    isAccountValid(account: FormControl, key?: string): boolean {
        if (account.pristine) {
            return true;
        }
        // if key === undefined account.hasError(key) fails, thus first check if key is defined
        return !!(!account.errors || (key && !account.hasError(key)));
    }

    getDecimalFormat(): string {
        return this.isIndagiaActive ? twoDecimalPlacesFormat : defaultNumberFormat;
    }

    get isIndagiaActive(): boolean {
        return this.indagiaService.indagiaActivationDate(this.date);
    }

    private format(value: string | number): number {
        return this.globalizeService.toNumber(this.globalizeService.toCurrency(value, this.getDecimalFormat()));
    }

    private handleValueChanges(): void {
        this.toggleCurrencyFactor();
        this.toggleDebitCreditAutocomplete();
        this.toggleTaxForCompoundEntry();
    }

    private toggleCurrencyFactor(): void {
        const quiet = { onlySelf: true, emitEvent: false };
        const currencyId = this.entry.get('currencyId').value;
        const baseCurrencyId = this.entry.get('baseCurrencyId').value;
        const currencyFactor = this.currencyFactor;
        const baseCurrencyAmount = this.baseCurrencyAmount;
        const controlsDisabled = currencyFactor.disabled && baseCurrencyAmount.disabled;
        const isDifferentCurrency = currencyId !== null && currencyId !== baseCurrencyId;

        if (!this.isLocked && controlsDisabled && isDifferentCurrency) {
            currencyFactor.enable(quiet);
            baseCurrencyAmount.enable(quiet);
            currencyFactor.setValidators(CustomValidators.largerThan(0));
        } else if (!controlsDisabled && !isDifferentCurrency) {
            currencyFactor.disable(quiet);
            baseCurrencyAmount.disable(quiet);
            currencyFactor.clearValidators();
        }
    }

    private toggleDebitCreditAutocomplete(): void {
        const quiet = { onlySelf: false, emitEvent: false };
        const debitAccount = this.entry.get('debitAccountId');
        const creditAccount = this.entry.get('creditAccountId');
        const debitDisabled = debitAccount.disabled;
        const creditDisabled = creditAccount.disabled;
        const shouldDisableDebitAccount = this.isLocked || (this.isCompoundEntryCollection && !!this.entry.get('creditAccountId').value);
        const shouldDisableCreditAccount = this.isLocked || (this.isCompoundEntryCollection && !!this.entry.get('debitAccountId').value);
        if (debitDisabled && !shouldDisableDebitAccount) {
            debitAccount.enable(quiet);
        }
        if (!debitDisabled && shouldDisableDebitAccount) {
            debitAccount.disable(quiet);
        }
        if (creditDisabled && !shouldDisableCreditAccount) {
            creditAccount.enable(quiet);
        }
        if (!creditDisabled && shouldDisableCreditAccount) {
            creditAccount.disable(quiet);
        }
    }

    private toggleTaxForCompoundEntry(): void {
        const quiet = { onlySelf: true, emitEvent: false };
        const taxId = this.entry.get('taxId');

        (this.isCompoundEntryCollection && this.index === 0) || this.isLocked ? taxId.disable(quiet) : taxId.enable(quiet);
    }

    get amount(): FormControl {
        return <FormControl>this.entry.get('amount');
    }

    get baseCurrencyAmount(): FormControl {
        return <FormControl>this.entry.get('baseCurrencyAmount');
    }

    get currencyFactor(): FormControl {
        return <FormControl>this.entry.get('currencyFactor');
    }

    get debitAccountSelect(): FormControl {
        return <FormControl>this.entry.get('debitAccountId');
    }

    get creditAccountSelect(): FormControl {
        return <FormControl>this.entry.get('creditAccountId');
    }

    get debitAccount(): Account {
        return this.accounts.find(byId(this.debitAccountSelect.value));
    }

    get creditAccount(): Account {
        return this.accounts.find(byId(this.creditAccountSelect.value));
    }

    get currency(): Currency {
        const currencyId = this.entry.get('currencyId').value;
        return this.currencies.find(byId(currencyId));
    }

    get baseCurrency(): Currency {
        const baseCurrencyId = this.entry.get('baseCurrencyId').value;
        return this.currencies.find(byId(baseCurrencyId));
    }

    get tax(): Tax {
        const taxId = this.entry.get('taxId').value;
        return this.taxes.find(byId(taxId));
    }

    get isCompoundEntryCollection(): boolean {
        return this.entryType === ManualEntryCollection.compoundEntryType;
    }

    get entryType(): string {
        return this.entry.get('type').value;
    }

    get validateIsAccount(): string {
        return 'isAccount';
    }
}
