import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MAT_DIALOG_SCROLL_STRATEGY_PROVIDER, MatIconModule, MatOptionModule, MatSelectModule, MatTooltipModule } from '@angular/material';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IndagiaService } from '@bx-client/common/src/services';
import { indagiaStartDateToken } from '@bx-client/env';
import { AccountMockModule } from '@bx-client/features/account';
import { GlobalizeService } from '@bx-client/i18n';
import 'hammerjs';

import { AutocompleteMockComponent } from '../../mocks/components';
import { CurrencyFormatAccessorMockDirective } from '../../mocks/directives/currency-format-accessor.mock.directive';
import { collection } from '../../mocks/fixtures/entry-collection.fixtures';
import { ToNumberMockPipe, TranslateMockPipe } from '../../mocks/pipes';
import { FormServiceMockProvider } from '../../mocks/services';
import { ManualEntry, ManualEntryCollection } from '../../models';
import { InputCalculatorService } from '../../services';

import { BookingEntryComponent } from './booking-entry.component';

const globalizeServiceStub = {};
const inputCalculatorServiceStub = {};

describe('BookingEntryComponent', () => {
    let component: BookingEntryComponent;
    let fixture: ComponentFixture<BookingEntryComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                AccountMockModule,
                ReactiveFormsModule,
                BrowserAnimationsModule,
                MatIconModule,
                MatSelectModule,
                MatOptionModule,
                MatTooltipModule
            ],
            declarations: [
                BookingEntryComponent,
                TranslateMockPipe,
                ToNumberMockPipe,
                AutocompleteMockComponent,
                CurrencyFormatAccessorMockDirective
            ],
            providers: [
                { provide: GlobalizeService, useValue: globalizeServiceStub },
                { provide: InputCalculatorService, useValue: inputCalculatorServiceStub },
                FormServiceMockProvider,
                MAT_DIALOG_SCROLL_STRATEGY_PROVIDER,
                IndagiaService,
                { provide: indagiaStartDateToken, useValue: '2019-12-01' }
            ]
        });

        fixture = TestBed.createComponent(BookingEntryComponent);
        component = fixture.debugElement.componentInstance;

        component.entry = new FormGroup({
            id: new FormControl(null),
            debitAccountId: new FormControl(null),
            creditAccountId: new FormControl(null),
            taxId: new FormControl(null),
            description: new FormControl(''),
            amount: new FormControl(0),
            baseCurrencyId: new FormControl(null),
            currencyId: new FormControl(null),
            currencyFactor: new FormControl(1),
            baseCurrencyAmount: new FormControl(0),
            taxAccountId: new FormControl(null),
            type: new FormControl(ManualEntryCollection.groupEntryType)
        });

        fixture.detectChanges();
    });

    it('should create the BookingEntryComponent', () => {
        expect(component).toBeTruthy();
    });

    it('should set disabled attribute when form is locked', () => {
        component.isLocked = true;
        fixture.detectChanges();
        expect(fixture.debugElement.query(By.css('.icon-button-switch')).properties.disabled).toBe(true);
    });

    it('should not disable currency input for compound entry type and enable it for the other types in the BookingEntryComponent', () => {
        component.entry.patchValue(Object.assign(new ManualEntry(), { type: ManualEntryCollection.compoundEntryType }));

        expect(component.isCompoundEntryCollection).toBe(true, 'isCompoundEntryType');
        expect(component.entry.get('currencyId').enabled).toBe(true, 'currencyIdEnabled');

        component.entry.patchValue(Object.assign(new ManualEntry(), { type: ManualEntryCollection.singleEntryType }));

        expect(component.isCompoundEntryCollection).toBe(false, 'isSingleEntryType');
        expect(component.entry.get('currencyId').enabled).toBe(true, 'currencyIdEnabled');

        component.entry.patchValue(Object.assign(new ManualEntry(), { type: ManualEntryCollection.groupEntryType }));

        expect(component.isCompoundEntryCollection).toBe(false, 'isGroupEntryType');
        expect(component.entry.get('currencyId').enabled).toBe(true, 'currencyIdEnabled');
    });

    it('should disable tax input for compound entry type and enable it for the other types in the BookingEntryComponent', () => {
        component.entry.patchValue(Object.assign(new ManualEntry(), { type: ManualEntryCollection.compoundEntryType }));

        expect(component.isCompoundEntryCollection).toBe(true, 'isCompoundEntryType');
        expect(component.entry.get('taxId').disabled).toBe(true, 'currencyIdDisabled');

        component.entry.patchValue(Object.assign(new ManualEntry(), { type: ManualEntryCollection.singleEntryType }));

        expect(component.isCompoundEntryCollection).toBe(false, 'isSingleEntryType');
        expect(component.entry.get('taxId').enabled).toBe(true, 'taxIdEnabled');

        component.entry.patchValue(Object.assign(new ManualEntry(), { type: ManualEntryCollection.groupEntryType }));

        expect(component.isCompoundEntryCollection).toBe(false, 'isGroupEntryType');
        expect(component.entry.get('taxId').enabled).toBe(true, 'taxIdEnabled');
    });

    it('should not reset currency id and exchange rate if an another account has been selected', () => {
        const singleEntryMock = Object.assign(new ManualEntry(), collection.entries[0]);
        const expectedCurrencyId = 2;
        const expectedCurrencyFactor = 1.23;

        component.entry.patchValue(singleEntryMock);
        component.entry.get('currencyId').patchValue(expectedCurrencyId);
        component.entry.get('currencyFactor').patchValue(expectedCurrencyFactor);
        component.entry.get('creditAccountId').patchValue(8);

        component.onSelectAccount(component.entry.getRawValue());

        fixture.detectChanges();

        expect(component.entry.get('currencyId').value).toBe(expectedCurrencyId);
        expect(component.entry.get('currencyFactor').value).toBe(expectedCurrencyFactor);
    });

    it('should not reset currency id and exchange rate on switching accounts', () => {
        const singleEntryMock = Object.assign(new ManualEntry(), collection.entries[0]);
        const expectedCurrencyId = 4;
        const expectedCurrencyFactor = 1.99;

        component.entry.patchValue(singleEntryMock);
        component.entry.get('currencyId').patchValue(expectedCurrencyId);
        component.entry.get('currencyFactor').patchValue(expectedCurrencyFactor);
        component.entry.get('creditAccountId').patchValue(6);

        component.onSwitchAccounts();

        fixture.detectChanges();

        expect(component.entry.get('currencyId').value).toBe(expectedCurrencyId);
        expect(component.entry.get('currencyFactor').value).toBe(expectedCurrencyFactor);
    });
});
