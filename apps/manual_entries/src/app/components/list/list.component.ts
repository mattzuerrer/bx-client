import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { GlobalizeService } from '@bx-client/i18n';

import { ColumnStatus, ManualEntryCollection } from '../../models';

@Component({
    selector: 'app-list',
    templateUrl: 'list.component.html',
    styleUrls: ['list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListComponent {
    @Input() listEntries: ManualEntryCollection[] = [];

    @Input() columnConfig: ColumnStatus[] = [];

    @Output() sort: EventEmitter<ColumnStatus> = new EventEmitter<ColumnStatus>();

    @Output() select: EventEmitter<ManualEntryCollection> = new EventEmitter<ManualEntryCollection>();

    constructor(private globalizeService: GlobalizeService) {}

    getFormatedAmount(num: number): string {
        return this.globalizeService.toCurrency(num);
    }
}
