import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { MaterialModule } from '../../material.module';
import { ColumnStatus } from '../../models';

import { ListSortComponent } from './list-sort.component';

describe('ListSortComponent', () => {
    let component: ListSortComponent;
    let fixture: ComponentFixture<ListSortComponent>;
    const columnStatus = new ColumnStatus('test');
    columnStatus.status = ColumnStatus.statusAscending;

    beforeEach(() => {
        TestBed.configureTestingModule({ declarations: [ListSortComponent], imports: [MaterialModule] });
        fixture = TestBed.createComponent(ListSortComponent);
        component = fixture.debugElement.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should set column title', () => {
        component.title = 'test title';
        fixture.detectChanges();
        expect(fixture.debugElement.query(By.css('.title')).nativeElement.textContent).toBe('test title');
    });

    it('should set data-sort attribute', () => {
        component.columnStatus = columnStatus;
        fixture.detectChanges();
        expect(fixture.debugElement.query(By.css('a')).attributes['data-sort']).toBe(columnStatus.status);
    });

    it('should set column status from column config', () => {
        const columnStatus1 = new ColumnStatus('test 1');
        const columnStatus2 = new ColumnStatus('test 2');
        component.column = 'test 2';
        component.columnConfig = [columnStatus1, columnStatus2];
        expect(component.columnStatus).toBe(columnStatus2);
        component.column = 'test 3';
        component.columnConfig = [columnStatus1, columnStatus2];
        expect(component.columnStatus).toEqual(new ColumnStatus('test 3'));
    });

    it('should emit sort event on click', () => {
        component.columnStatus = columnStatus;
        fixture.detectChanges();
        component.sort.subscribe(event => expect(event).toBe(columnStatus));
        fixture.debugElement.query(By.css('a')).triggerEventHandler('click', null);
    });

    it('should emit sort event on enter', () => {
        component.columnStatus = columnStatus;
        fixture.detectChanges();
        component.sort.subscribe(event => expect(event).toBe(columnStatus));
        fixture.debugElement.query(By.css('a')).triggerEventHandler('enter', null);
    });
});
