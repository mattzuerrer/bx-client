import { Component, Input } from '@angular/core';
import { DialogRole, MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material';
import { identity } from '@bx-client/common';
import { filter } from 'rxjs/operators';

@Component({
    selector: 'app-confirm-dialog',
    templateUrl: 'confirm-dialog.component.html',
    styleUrls: ['confirm-dialog.component.scss']
})
export class ConfirmDialogComponent {
    static dialogAriaRole: DialogRole = 'alertdialog';
    static dialogWidth = '360px';

    static open(
        matDialog: MatDialog,
        title: string,
        content: string | string[],
        submitText: string,
        switchButtons: boolean,
        onConfirmCallback: (value: any) => void,
        data?: any
    ): MatDialogRef<ConfirmDialogComponent> {
        const config: MatDialogConfig = {
            role: ConfirmDialogComponent.dialogAriaRole,
            width: ConfirmDialogComponent.dialogWidth
        };
        const dialog = matDialog.open(ConfirmDialogComponent, config);
        dialog.componentInstance.title = title;
        dialog.componentInstance.content = content;
        dialog.componentInstance.submit = submitText;
        dialog.componentInstance.switchButtons = switchButtons;
        dialog.componentInstance.data = data;
        dialog
            .afterClosed()
            .pipe(filter(identity))
            .subscribe(onConfirmCallback);
        return dialog;
    }
    @Input() title = '';
    @Input() content: string | string[] = '';
    @Input() submit = '';
    @Input() data: any = null;

    @Input() switchButtons: boolean;

    constructor(private dialogRef: MatDialogRef<ConfirmDialogComponent>) {}

    closeDialog(dialogResult: boolean): void {
        this.dialogRef.close(dialogResult);
    }

    get isContentArray(): boolean {
        return typeof this.content !== 'string';
    }
}
