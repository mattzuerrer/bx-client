import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { MatCheckboxChange, MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material';
import { CdnService } from '@bx-client/core/src/environments/service/cdn.service';
import { openIdToken } from '@bx-client/env';
import { TranslateService } from '@ngx-translate/core';
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { Ng2FilterPipe } from 'ng2-filter-pipe';

import { version } from '../../config';
import { File } from '../../models';
import { FileApiService, FileUploadStatus, FileUploadSuccess } from '../../services/file-api';

@Component({
    selector: 'app-file-upload-dialog',
    templateUrl: 'file-upload-dialog.component.html',
    styleUrls: ['file-upload-dialog.component.scss']
})
export class FileUploadDialogComponent implements OnInit {
    static dialogWidth = '480px';
    static maxSize = 12;

    static open(
        matDialog: MatDialog,
        entry: File[],
        inboxFiles: File[],
        url: string,
        accept: string,
        mimeTypes: string[],
        maxFileSize: number = FileUploadDialogComponent.maxSize
    ): MatDialogRef<FileUploadDialogComponent> {
        const config: MatDialogConfig = { width: FileUploadDialogComponent.dialogWidth };
        const dialog = matDialog.open(FileUploadDialogComponent, config);
        dialog.componentInstance.entry = entry;
        dialog.componentInstance.inboxFiles = inboxFiles;
        dialog.componentInstance.accept = accept;
        dialog.componentInstance.url = url;
        dialog.componentInstance.maxFileSize = maxFileSize;
        dialog.componentInstance.mimeTypes = mimeTypes;
        return dialog;
    }

    inboxFilesCountLimit = 15;

    @Input() entry: File[];
    @Input() inboxFiles: File[];
    @Input() accept: string;
    @Input() url: string;
    @Input() maxFileSize: number;
    @Input() mimeTypes: string[];

    @Output() addingFile: EventEmitter<File> = new EventEmitter<File>();
    @Output() removingFile: EventEmitter<File> = new EventEmitter<File>();

    filterBy: Ng2FilterPipe;

    inboxFilter: any = { name: '' };

    files: File[] = [];

    uploader: FileUploader;

    uploadError: string = null;

    assetUrl: string = this.cdn.getAssetUrl(version);

    private maxBytes = 1024;
    private _isFileOver = false;

    constructor(
        public dialogRef: MatDialogRef<FileUploadDialogComponent>,
        private fileApi: FileApiService,
        private translate: TranslateService,
        private cdn: CdnService,
        @Inject(openIdToken) private authorization: string
    ) {}

    ngOnInit(): void {
        this.initEntryFiles();
        this.initUploader();
        this.initUploaderListeners();
    }

    closeDialog(): void {
        this.dialogRef.close(this.files);
    }

    removeFile(file: File): void {
        this.files = this.files.filter(existingFile => existingFile !== file);
        this.removingFile.emit(file);
    }

    addFile(file: File): void {
        this.files.push(file);
        this.addingFile.emit(file);
    }

    onFileOver(status: boolean): void {
        this._isFileOver = status;
    }

    onRemoveFile(file: File): void {
        this.removeFile(file);
    }

    onCheckFile(event: MatCheckboxChange, file: File): void {
        event.checked ? this.addFile(file) : this.removeFile(file);
    }

    get isFileOver(): boolean {
        return this._isFileOver;
    }

    get inboxFilesCount(): number {
        return this.inboxFiles.length;
    }

    isSelected(file: File): boolean {
        const findFile = (value: File, index: number, obj: File[]) => value === file;
        return this.files.find(findFile) === file;
    }

    private initEntryFiles(): void {
        this.files.push(...this.entry);
    }

    private initUploader(): void {
        const options: FileUploaderOptions = {
            url: this.url,
            autoUpload: true,
            isHTML5: true,
            maxFileSize: this.maxFileSize * this.maxBytes * this.maxBytes, // MB -> Bytes,
            allowedMimeType: this.mimeTypes,
            authToken: `Bearer ${this.authorization}`,
            headers: [{ name: 'Accept', value: 'application/json' }]
        };

        this.uploader = new FileUploader(options);
    }

    private initUploaderListeners(): void {
        const resetErrorDisplay = file => (this.uploadError = null);

        this.uploader.onCancelItem = resetErrorDisplay;

        this.uploader.onBeforeUploadItem = fileItem => {
            resetErrorDisplay(fileItem);
            fileItem.withCredentials = false;
        };

        this.uploader.onWhenAddingFileFailed = this.fileApi.handleAddingFileFailure(status => this.showUploadStatus(status));
        this.uploader.onSuccessItem = this.fileApi.handleFileUploadSuccess(status => {
            this.showUploadStatus(status);
            this.addFile((<FileUploadSuccess>status).file);
        });
        this.uploader.onErrorItem = this.fileApi.handleFileUploadError(status => this.showUploadStatus(status));
    }

    private showUploadStatus(status: FileUploadStatus): void {
        this.uploadError = null;
        if (!status) {
            return;
        }
        if (status.isError) {
            this.translate.get(status.errorType, { value: status.errorValue }).subscribe(translation => (this.uploadError = translation));
        }
    }
}
