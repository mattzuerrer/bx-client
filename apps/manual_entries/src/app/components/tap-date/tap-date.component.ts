import { Component, ElementRef, EventEmitter, forwardRef, Input, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { formatDay, formatMonth, formatYear, Opaque } from '@bx-client/common';
import * as keycode from 'keycode';

interface TapDateErrors {
    day: { [key: string]: any };
    month: { [key: string]: any };
    year: { [key: string]: any };
}

const yearRange = 10;

const minYear: number = new Date().getFullYear() - yearRange;

const maxYear: number = new Date().getFullYear() + yearRange;

const tapDateValueAccesor = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => TapDateComponent),
    multi: true
};

const tapDateValidator = {
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => TapDateComponent),
    multi: true
};

let uniqueIdCounter = 0;

@Component({
    selector: 'app-tap-date',
    templateUrl: 'tap-date.component.html',
    styleUrls: ['tap-date.component.scss'],
    providers: [tapDateValueAccesor, tapDateValidator]
})
export class TapDateComponent implements ControlValueAccessor {
    dayInputId = `tap-date-${uniqueIdCounter++}`;

    minYear: number = minYear;

    maxYear: number = maxYear;

    errors: TapDateErrors = { day: null, month: null, year: null };

    @Input()
    set date(date: Date) {
        this.writeValue(date);
    }

    @Input() label = 'Date';

    @Input() required = false;

    @Input() disabled = false;

    @Output() dateChange: EventEmitter<Date> = new EventEmitter<Date>();

    @ViewChild('dayInput') dayInput: ElementRef;

    @ViewChild('monthInput') monthInput: ElementRef;

    @ViewChild('yearInput') yearInput: ElementRef;

    day: Opaque = new Opaque('');

    month: Opaque = new Opaque('');

    year: Opaque = new Opaque('');

    // tslint:disable:no-empty
    onChange(value: any): void {}

    onTouched(): any {}
    // tslint:enable:no-empty

    writeValue(date: Date): void {
        if (date) {
            this.clearErrors();
            this.updateFields(date);
        }
    }

    registerOnTouched(fn: () => any): void {
        this.onTouched = fn;
    }

    registerOnChange(fn: (value: any) => void): void {
        this.onChange = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    onDayChange(day: string): void {
        this.onTouched();
        const date = this.checkDate(null, null, day);
        if (!date) {
            this.updateDayField(day);
            this.errors.day = { invalidDate: true };
        } else {
            this.clearErrors();
        }
        this.propagateChange(date);
    }

    onMonthChange(month: string): void {
        this.onTouched();
        const date = this.checkDate(null, month, null);
        if (!date) {
            this.updateMonthField(month);
            this.errors.month = { invalidDate: true };
        } else {
            this.clearErrors();
        }
        this.propagateChange(date);
    }

    onYearChange(year: string): void {
        this.onTouched();
        let date = this.checkDate(year, null, null);
        const outOfBoundMin = date && date.getFullYear() < minYear;
        const outOfBoundMax = date && date.getFullYear() > maxYear;
        switch (true) {
            case outOfBoundMax:
                this.updateYearField(formatYear(year));
                this.errors.year = { yearOutOfBoundMax: true };
                date = null;
                break;
            case outOfBoundMin:
                this.updateYearField(formatYear(year));
                this.errors.year = { yearOutOfBoundMin: true };
                date = null;
                break;
            case !date:
                this.updateYearField(year);
                this.errors.year = { invalidDate: true };
                break;
            default:
                this.clearErrors();
        }
        this.propagateChange(date);
    }

    onKeyDown(event: KeyboardEvent): void {
        if (event.keyCode === keycode('.') || event.keyCode === keycode('numpad .')) {
            event.preventDefault();
            let element;
            switch (event.srcElement) {
                case this.dayInput.nativeElement:
                    element = this.monthInput;
                    break;
                case this.monthInput.nativeElement:
                    element = this.yearInput;
                    break;
            }
            this.focus(element);
        }
    }

    focus(element?: ElementRef): void {
        if (!element) {
            element = this.dayInput;
        }
        element.nativeElement.select();
    }

    validate(): { [key: string]: any } {
        let errorCount = 0;
        for (const key in this.errors) {
            if (this.errors.hasOwnProperty(key)) {
                errorCount += this.errors[key] ? 1 : 0;
            }
        }
        if (errorCount > 1) {
            return { invalidDate: true };
        }
        return this.errors.day || this.errors.month || this.errors.year;
    }

    private clearErrors(): void {
        this.errors = { day: null, month: null, year: null };
    }

    private updateFields(date: Date): void {
        this.updateDayField(formatDay(date.getDate().toString()));
        this.updateMonthField(formatMonth((date.getMonth() + 1).toString()));
        this.updateYearField(formatYear(date.getFullYear().toString()));
    }

    private updateDayField(day: string): void {
        this.day = new Opaque(day);
    }

    private updateMonthField(month: string): void {
        this.month = new Opaque(month);
    }

    private updateYearField(year: string): void {
        this.year = new Opaque(year);
    }

    private propagateChange(date: Date): void {
        if (!date || !this.isEqualDate(date)) {
            this.dateChange.emit(date);
            this.onChange(date);
        }
        this.writeValue(date);
    }

    private isEqualDate(date: Date): boolean {
        return (
            this.day.toString() === formatDay(date.getDate().toString()) &&
            this.month.toString() === formatMonth((date.getMonth() + 1).toString()) &&
            this.year.toString() === formatYear(date.getFullYear().toString())
        );
    }

    private checkDate(year: string, month: string, day: string): Date {
        day = formatDay(day !== null ? day : this.day.toString());
        month = formatMonth(month !== null ? month : this.month.toString());
        year = formatYear(year !== null ? year : this.year.toString());
        const dateString = `${year}/${month}/${day}`;
        const regexDate = /^\d{4}\/\d{2}\/\d{2}$/;
        if (!regexDate.test(dateString)) {
            return null;
        }
        const parsedDay = parseInt(day, 10);
        const parsedMonth = parseInt(month, 10) - 1;
        const parsedYear = parseInt(year, 10);
        const date = new Date(dateString);
        if (date.getFullYear() === parsedYear && date.getMonth() === parsedMonth && date.getDate() === parsedDay) {
            return date;
        } else {
            return null;
        }
    }
}
