import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import {
    ariaInvalidFn,
    ariaLiveFn,
    byId,
    byType,
    identity,
    makeAriaInvalidFn,
    makeAriaLiveFn,
    sum,
    Validators as CustomValidators
} from '@bx-client/common';
import { IndagiaService } from '@bx-client/common/src/services';
import { Account, AccountValidators } from '@bx-client/features/account';
import { Company } from '@bx-client/features/company';
import { Action } from '@ngrx/store';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, filter, map } from 'rxjs/operators';

import { FormActions } from '../../actions';
import { internalKeys, liabilities, mimeTypes, requirements, vat } from '../../config';
import { isManualEntryCollection, isManualEntryTemplate } from '../../guards';
import {
    Currency,
    Dictionary,
    File,
    FileConnection,
    FormState,
    ManualEntry,
    ManualEntryCollection,
    ManualEntryTemplate,
    MenuAction,
    Tax
} from '../../models';
import { FileApiService, FormService } from '../../services';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { FileUploadDialogComponent } from '../file-upload-dialog/file-upload-dialog.component';
import { SaveTemplateDialogComponent } from '../save-template-dialog/save-template-dialog.component';
import { TapDateComponent } from '../tap-date/tap-date.component';
import { VatAssignComponent } from '../vat-assign/vat-assign.component';

const validationDelta = 5e-7;

const closedTaxPeriod = 'closed_tax_period';

const closedBusinessYear = 'closed_business_year';

const formActionsDebounce = 200;

const menuSaveAsTemplate = new MenuAction('SAVE_AS_TEMPLATE', 'globals.template.save_as_template');
const menuUpdateOrCopyTemplate = new MenuAction('UPDATE_OR_COPY_TEMPLATE', 'globals.template.update_or_copy_template');

@Component({
    selector: 'app-booking-form',
    styleUrls: ['booking-form.component.scss'],
    templateUrl: 'booking-form.component.html',
    encapsulation: ViewEncapsulation.None
})
export class BookingFormComponent implements OnInit, OnDestroy {
    @Input()
    set formState(formState: FormState) {
        this.form.patchValue(formState);
    }

    @Input()
    set manualEntryCollection(manualEntryCollection: ManualEntryCollection) {
        delete manualEntryCollection.referenceNr;
        this.form.setControl('entries', new FormArray(manualEntryCollection.entries.map(() => this.initEntryRow())));
        manualEntryCollection.isCompoundEntry ? this.setCompoundEntryValidators() : this.setSingleEntryValidators();
        this.form.patchValue(manualEntryCollection);
        this.setMenuOptions(manualEntryCollection);
        this.isLocked ? this.disableForm() : this.enableForm();
        this.focusForm();
    }

    static maxAmount = 100000000;

    @Input()
    set inboxFile(file: File) {
        if (file) {
            this.firstEntry.get('files').patchValue([file]);
        }
    }

    @Input() templates: ManualEntryTemplate[] = [];

    @Input() currencies: Currency[] = [];

    @Input() accounts: Account[] = [];

    @Input() taxes: Tax[] = [];

    @Input() files: File[] = [];

    @Input() fileUploadDialogFiles: File[];

    @Input() fileUploadDialogFilesAdded: File[];

    @Input() fileUploadDialogFilesRemoved: File[];

    @Input() company: Company = null;

    @Input() accountGroupsLoaded = false;

    @Input() autofillDescriptions: string[] = [];

    @Output() dateChange: EventEmitter<Date> = new EventEmitter<Date>();

    @Output() create: EventEmitter<ManualEntryCollection> = new EventEmitter<ManualEntryCollection>();

    @Output() update: EventEmitter<ManualEntryCollection> = new EventEmitter<ManualEntryCollection>();

    @Output() updateFiles: EventEmitter<ManualEntryCollection> = new EventEmitter<ManualEntryCollection>();

    @Output() remove: EventEmitter<number> = new EventEmitter<number>();

    @Output() cancelCreate: EventEmitter<void> = new EventEmitter<void>();

    @Output() cancelEdit: EventEmitter<void> = new EventEmitter<void>();

    @Output() connectFiles: EventEmitter<FileConnection> = new EventEmitter<FileConnection>();

    @Output() disconnectFiles: EventEmitter<FileConnection> = new EventEmitter<FileConnection>();

    @Output() saveTemplate: EventEmitter<ManualEntryTemplate> = new EventEmitter<ManualEntryTemplate>();

    @Output() deleteTemplate: EventEmitter<number> = new EventEmitter<number>();

    @Output() selectTemplate: EventEmitter<ManualEntryTemplate> = new EventEmitter<ManualEntryTemplate>();

    @Output() addAutofill: EventEmitter<Dictionary<string[]>> = new EventEmitter<Dictionary<string[]>>();

    @Output() initFileUploadDialog: EventEmitter<File[]> = new EventEmitter<File[]>();

    @Output() addFileUploadDialogFile: EventEmitter<File> = new EventEmitter<File>();

    @Output() removeFileUploadDialogFile: EventEmitter<File> = new EventEmitter<File>();

    @ViewChild('tapDate') tapDate: TapDateComponent;

    form: FormGroup = this.initForm();

    isAriaInvalid: ariaInvalidFn;

    ariaLive: ariaLiveFn;

    compoundFormat: string;

    private actions: MenuAction[];

    private subscriptions: Subscription[] = [];

    private entriesChange$: Subject<Action> = new Subject<Action>();

    private loadedTemplate: ManualEntryTemplate = null;

    constructor(
        private matDialog: MatDialog,
        private formBuilder: FormBuilder,
        private formService: FormService,
        private fileApiService: FileApiService,
        private indagiaService: IndagiaService
    ) {}

    ngOnInit(): void {
        this.tapDate.focus();
        this.updateDecimalFormat(new Date());
        this.initFormValidators();
        this.initAriaHelpers();
        this.initFormSubscriptions();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    onDateChange(date: Date): void {
        if (date) {
            this.dateChange.emit(date);
            this.updateDecimalFormat(date);
        }
    }

    onAutoAddEntryRow(entry: FormGroup, index: number): void {
        const entryExists = index < this.entries.length;
        const isLastRow = index === this.entries.length - 1;
        if (
            entryExists &&
            this.isOneSidedEntry(entry) &&
            (this.hasNumberOfRows(1) || (isLastRow && this.isCompoundEntryCollection && !this.areEntriesBalanced))
        ) {
            this.onAddEntryRow();
        }
    }

    onAddEntryRow(): void {
        if (!this.isLocked) {
            this.entriesChange$.next(new FormActions.AddRowAction());
        }
    }

    onMenu(action: MenuAction): void {
        switch (action.type) {
            case menuSaveAsTemplate.type:
            case menuUpdateOrCopyTemplate.type:
                const dialogRef = SaveTemplateDialogComponent.open(this.matDialog, this.templates, this.loadedTemplate);
                dialogRef
                    .afterClosed()
                    .pipe(filter(identity))
                    .subscribe((template: ManualEntryTemplate) => {
                        template.collection = Object.assign(new ManualEntryCollection(), this.form.getRawValue());
                        this.saveTemplate.emit(template);
                    });
                break;
        }
    }

    onDeleteTemplate(templateId: number): void {
        const template = this.templates.find(byId(templateId));
        if (template) {
            const data = Object.assign({}, { name: template.name });
            ConfirmDialogComponent.open(
                this.matDialog,
                'entry_row-dialog.template-delete.title',
                'entry_row-dialog.template-delete.content',
                'entry_row-dialog.template-delete.submit',
                false,
                () => this.deleteTemplate.emit(template.id),
                data
            );
        }
    }

    onSelectTemplate(templateId: number): void {
        const template = this.templates.find(byId(templateId));
        if (template && template.collection) {
            this.loadedTemplate = template;
            this.selectTemplate.emit(template);
        }
    }

    onSubmit(skipSystemAccounts: boolean = false): boolean | void {
        const systemAccountTypes = this.getSystemAccountTypes();
        if (!skipSystemAccounts && systemAccountTypes.length) {
            this.openSystemAccountAssignDialog(systemAccountTypes);
            return false;
        }

        if (!this.isLoading) {
            this.handleTaxAccounts();
        }
    }

    onRemoveEntry(i: number): void {
        if (!this.isLocked) {
            this.entriesChange$.next(new FormActions.RemoveRowAction(i));
        }
    }

    getAccount(id: string): Account {
        return this.accounts.find(byId(id));
    }

    getSystemAccountTypes(): string[] {
        const keys = [];
        for (const key in internalKeys) {
            if (internalKeys.hasOwnProperty(key)) {
                const internalKeysType = internalKeys[key];

                if (this.internalKeyIsInArray(internalKeysType)) {
                    keys.push(key);
                }
            }
        }
        return keys;
    }
    openSystemAccountAssignDialog(systemAccountTypes: string[]): void {
        const systemAccountMessages: string[] = [];
        const isRequirementsSystemAccount: boolean = systemAccountTypes.indexOf(requirements) > -1;
        const isLiabilitiesSystemAccount: boolean = systemAccountTypes.indexOf(liabilities) > -1;
        const isVatSystemAccount: boolean = systemAccountTypes.indexOf(vat) > -1;
        let title: string;

        if (isRequirementsSystemAccount) {
            title = 'entry_row-dialog.collective-account-entry.title';
            systemAccountMessages.push('entry_row-dialog.system-account-entry.requirements');
        }

        if (isLiabilitiesSystemAccount) {
            title = 'entry_row-dialog.collective-account-entry.title';
            systemAccountMessages.push('entry_row-dialog.system-account-entry.liabilities');
        }

        if (isVatSystemAccount) {
            title = 'entry_row-dialog.vat-account-entry.title';
            systemAccountMessages.push('entry_row-dialog.system-account-entry.vat');
        }

        if (isVatSystemAccount && (isRequirementsSystemAccount || isLiabilitiesSystemAccount)) {
            title = '';
        }

        ConfirmDialogComponent.open(
            this.matDialog,
            title,
            systemAccountMessages,
            'entry_row-dialog.system-account-entry.submit',
            true,
            () => this.onSubmit(true)
        );
    }

    openEntryDeleteDialog(): void {
        const id = this.form.get('id').value;
        const referenceNr = this.form.get('referenceNr').value;
        const description = this.firstEntry.get('description').value;
        const referenceNrAndDescription = Object.assign({}, { referenceNr, description });

        ConfirmDialogComponent.open(
            this.matDialog,
            'entry_row-dialog.entry-delete.title',
            this.getDialogContentTranslationKey(description, referenceNr),
            'entry_row-dialog.entry-delete.submit',
            false,
            () => this.remove.emit(id),
            referenceNrAndDescription
        );
    }

    beforeDialogOpens(index: number): void {
        const entryForm = <FormGroup>this.entries.at(index);
        this.initFileUploadDialog.emit(entryForm.getRawValue().files);
    }

    openFileUploadDialog(index: number): void {
        const accept = mimeTypes.join(', ');
        const dialogRef = FileUploadDialogComponent.open(
            this.matDialog,
            this.fileUploadDialogFiles,
            this.files,
            this.fileApiService.getInboxUploadUrl(),
            accept,
            mimeTypes
        );

        dialogRef.afterClosed().subscribe(files => {
            if (files) {
                this.assignFiles(null, files, index);
            }
        });

        dialogRef.componentInstance.addingFile.subscribe(file => this.addFileUploadDialogFile.emit(file));
        dialogRef.componentInstance.removingFile.subscribe(file => this.removeFileUploadDialogFile.emit(file));
    }

    getAttachedFiles(index: number): File[] {
        return this.entries.at(index).get('files').value;
    }

    countAttachedFiles(index: number): number {
        const attachedFiles = this.getAttachedFiles(index);
        return attachedFiles ? attachedFiles.length : 0;
    }

    hasCollectiveAccountError(index: number): boolean {
        const error = this.form.get('entries').getError('firstitemunique');
        return error && error.index === index;
    }

    showAttachmentIcon(index: number): boolean {
        return !this.isCompoundEntryCollection || index === 0;
    }

    assignFiles(entry: ManualEntry, files: File[], index: number): void {
        this.entries
            .at(index)
            .get('files')
            .patchValue(this.fileUploadDialogFiles);
        this.entries
            .at(index)
            .get('addedFiles')
            .patchValue(this.fileUploadDialogFilesAdded);
        this.entries
            .at(index)
            .get('removedFiles')
            .patchValue(this.fileUploadDialogFilesRemoved);

        this.form.get('entries').markAsDirty();
    }

    onCancelCreate(): void {
        this.cancelCreate.emit();
    }

    onCancelEdit(): void {
        this.cancelEdit.emit();
    }

    isRemoveEntryDisabled(index: number): boolean {
        return this.isLocked || this.entries.length === 1 || (index === 0 && this.isCompoundEntryCollection);
    }

    private getDialogContentTranslationKey(description: string|null, referenceNr: string|null): string {
        const keyWithDescriptionWithReferenceNumber = 'entry_row-dialog.entry-delete-with-description-with-reference-number.content';
        const keyWithDescriptionWithoutReferenceNumber = 'entry_row-dialog.entry-delete-with-description-without-reference-number.content';
        const keyWithoutDescriptionWithReferenceNumber = 'entry_row-dialog.entry-delete-without-description-with-reference-number.content';
        const keyWithoutDescriptionWithoutReferenceNumber
            = 'entry_row-dialog.entry-delete-without-description-without-reference-number.content';

        if (null === referenceNr && null === description) {
            return keyWithoutDescriptionWithoutReferenceNumber;
        }

        if (null === referenceNr && null !== description) {
            return keyWithDescriptionWithoutReferenceNumber;
        }

        if (null !== referenceNr && null === description) {
            return keyWithoutDescriptionWithReferenceNumber;
        }

        if (null !== referenceNr && null !== description) {
            return keyWithDescriptionWithReferenceNumber;
        }
    }

    private updateDecimalFormat(date: Date): void {
        const isActive = this.indagiaService.indagiaActivationDate(date);
        if (isActive) {
            this.compoundFormat = '1.2-2';
        } else {
            this.compoundFormat = '1.2-6';
        }
    }

    private initFormSubscriptions(): void {
        const entriesChange$ = this.entriesChange$.pipe(debounceTime(formActionsDebounce));
        this.subscriptions.push(this.formService.onResetForm$().subscribe(() => this.resetForm()));
        this.subscriptions.push(this.formService.onResetTemplate$().subscribe(() => (this.loadedTemplate = null)));
        this.subscriptions.push(this.formService.onSubmitForm$().subscribe(() => this.triggerSubmit()));
        this.subscriptions.push(this.formService.onAddRow$().subscribe(() => this.onAddEntryRow()));
        this.subscriptions.push(this.formService.onRemoveRow$().subscribe(() => this.onRemoveEntry(this.entries.length - 1)));
        this.subscriptions.push(entriesChange$.pipe(filter(byType(FormActions.ActionTypes.ADD_ROW))).subscribe(() => this.addEntry()));
        this.subscriptions.push(
            entriesChange$
                .pipe(filter(byType(FormActions.ActionTypes.REMOVE_ROW)), map(action => action.payload))
                .subscribe(index => this.removeEntry(index))
        );
    }

    private initForm(): FormGroup {
        return this.formBuilder.group({
            id: [null],
            isLocked: [false],
            lockedInfo: [''],
            type: [ManualEntryCollection.singleEntryType, Validators.required],
            date: [new Date(), Validators.required],
            referenceNr: ['', Validators.maxLength(255)],
            entries: this.formBuilder.array([this.initEntryRow()]),
            status: [''],
            templates: [null]
        });
    }

    private initEntryRow(): FormGroup {
        return this.formBuilder.group(
            {
                id: [null],
                debitAccountId: [null, [Validators.required, AccountValidators.isAccount]],
                creditAccountId: [null, [Validators.required, AccountValidators.isAccount]],
                taxId: [null],
                description: ['', Validators.maxLength(255)],
                amount: [0, [CustomValidators.notEqual(0), CustomValidators.smallerThan(BookingFormComponent.maxAmount)]],
                baseCurrencyId: [null],
                date: [null],
                currencyId: [null, Validators.required],
                currencyFactor: [{ value: 1, disabled: true }],
                baseCurrencyAmount: [{ value: 0, disabled: true }, CustomValidators.smallerThan(BookingFormComponent.maxAmount)],
                taxAccountId: [null],
                type: [ManualEntryCollection.singleEntryType],
                files: [[]],
                addedFiles: [[]],
                removedFiles: [[]]
            },
            { validator: CustomValidators.fieldsNotEqual('debitAccountId', 'creditAccountId') }
        );
    }

    private initFormValidators(): void {
        this.form.setValidators([
            () => (!this.isLocked ? null : { formlocked: { value: this.lockedInfo } }),
            () => (this.areEntriesBalanced ? null : { entriesbalanced: { value: this.debitCreditSumDifference } })
        ]);
    }

    private initAriaHelpers(): void {
        this.isAriaInvalid = makeAriaInvalidFn(this.form);
        this.ariaLive = makeAriaLiveFn(this.form);
    }

    private addEntry(): void {
        const newEntry = this.initEntryRow();
        this.initDefaultValues(newEntry);
        this.entries.push(newEntry);
        this.checkMultiEntryType();
        this.formService.triggerFocusRow(this.entries.length - 1);
    }

    private initDefaultValues(entry: FormGroup): void {
        entry.get('currencyId').patchValue(this.lastEntry.get('baseCurrencyId').value);
        entry.get('baseCurrencyId').patchValue(this.lastEntry.get('baseCurrencyId').value);
        entry.get('description').patchValue(this.lastEntry.get('description').value);
        const difference = this.debitCreditSumDifference;
        entry.get('baseCurrencyAmount').patchValue(difference);
        entry.get('amount').patchValue(difference);
    }

    get hasTemplates(): boolean {
        return this.templates.length > 0;
    }

    private setMenuOptions(item: ManualEntryCollection | ManualEntryTemplate): void {
        if (isManualEntryCollection(item) || isManualEntryTemplate(item)) {
            this.actions = this.loadedTemplate ? [menuUpdateOrCopyTemplate] : [menuSaveAsTemplate];
        } else {
            this.actions = [];
        }
    }

    private removeEntry(i: number): void {
        if (this.entries.length > 1) {
            this.entries.removeAt(i);
            this.checkSingleEntryType();
            this.entries.markAsDirty();
        }
    }

    private triggerSubmit(): void {
        if (!this.isEntryCreateDisabled && (!this.isExistingEntryCollection || !this.isEntryUpdateDisabled)) {
            this.onSubmit();
        }
    }

    private handleTaxAccounts(): void {
        const vatAssignRows = [];
        this.entries.controls.forEach((entry, index) => {
            const debitAccount = this.getAccount(entry.get('debitAccountId').value);
            const creditAccount = this.getAccount(entry.get('creditAccountId').value);
            const tax = this.taxes.find(byId(entry.get('taxId').value));
            if (tax && debitAccount && creditAccount) {
                const hasDebitAccountTaxId = !!debitAccount.taxId;
                const hasCreditAccountTaxId = !!creditAccount.taxId;
                switch (true) {
                    case hasDebitAccountTaxId === hasCreditAccountTaxId:
                        vatAssignRows.push(Object.assign(new ManualEntry(), { debitAccount, creditAccount, tax, index }));
                        break;
                    case hasDebitAccountTaxId:
                        entry.get('taxAccountId').patchValue(debitAccount.id);
                        break;
                    case hasCreditAccountTaxId:
                        entry.get('taxAccountId').patchValue(creditAccount.id);
                        break;
                }
            }
        });
        if (vatAssignRows.filter(identity).length && !this.isLocked) {
            this.openVatAssignDialog(vatAssignRows);
        } else {
            this.saveCollection();
        }
    }

    private internalKeyIsInArray(internalKeysType: string[]): boolean {
        return this.entries.controls.some(entry => {
            const debitAccount = this.getAccount(entry.get('debitAccountId').value);
            const creditAccount = this.getAccount(entry.get('creditAccountId').value);

            return (
                (debitAccount && internalKeysType.indexOf(debitAccount.internalKey) > -1) ||
                (creditAccount && internalKeysType.indexOf(creditAccount.internalKey) > -1)
            );
        });
    }

    private saveCollection(): void {
        const rawFormValues = this.form.getRawValue();
        const rawEntries = this.entries.getRawValue();
        const manualEntryCollection = Object.assign(new ManualEntryCollection(), rawFormValues);

        this.addAutofill.emit({ description: rawEntries.map(entry => entry.description) });

        if (this.isLocked) {
            this.updateFiles.emit(manualEntryCollection);
        } else {
            this.isExistingEntryCollection ? this.update.emit(manualEntryCollection) : this.create.emit(manualEntryCollection);
        }
    }

    private openVatAssignDialog(rows: ManualEntry[]): void {
        const dialogRef = this.matDialog.open(VatAssignComponent);
        dialogRef.componentInstance.rows = rows;
        const vatAssignDialogSubscription = dialogRef.afterClosed().subscribe(results => {
            if (results) {
                results.forEach(result => {
                    this.entries.controls[result.index].get('taxAccountId').patchValue(result.taxAccountId);
                });
                this.saveCollection();
            }
            vatAssignDialogSubscription.unsubscribe();
        });
    }

    private checkSingleEntryType(): void {
        if (this.hasNumberOfRows(1)) {
            this.form.get('type').patchValue(ManualEntryCollection.singleEntryType);
            this.firstEntry.get('taxId').enable();
            this.setSingleEntryValidators();
        }
    }

    private checkMultiEntryType(): void {
        if (this.hasNumberOfRows(2)) {
            if (this.isOneSidedEntry(this.firstEntry)) {
                this.form.get('type').patchValue(ManualEntryCollection.compoundEntryType);
                const taxControl = this.firstEntry.get('taxId');
                taxControl.patchValue(null);
                taxControl.disable();
                this.setCompoundEntryValidators();
            } else {
                this.form.get('type').patchValue(ManualEntryCollection.groupEntryType);
            }
        }
    }

    private setSingleEntryValidators(): void {
        this.entries.clearValidators();
        this.entries.controls.forEach(control => {
            control.clearValidators();
            control.setValidators(CustomValidators.fieldsNotEqual('debitAccountId', 'creditAccountId'));
            control.get('debitAccountId').clearValidators();
            control.get('debitAccountId').setValidators([Validators.required, AccountValidators.isAccount]);
            control.get('creditAccountId').clearValidators();
            control.get('creditAccountId').setValidators([Validators.required, AccountValidators.isAccount]);
        });
    }

    private setCompoundEntryValidators(): void {
        this.entries.clearValidators();
        this.entries.setValidators(CustomValidators.firstItemUnique('debitAccountId', 'creditAccountId'));
        this.entries.controls.forEach(control => {
            control.clearValidators();
            control.setValidators(CustomValidators.xor('debitAccountId', 'creditAccountId'));
            control.get('debitAccountId').clearValidators();
            control.get('debitAccountId').setValidators(AccountValidators.isAccount);
            control.get('creditAccountId').clearValidators();
            control.get('creditAccountId').setValidators(AccountValidators.isAccount);
        });
    }

    private sumAccountSide(key: string): number {
        return this.entries.controls
            .filter(control => !!control.get(key).value)
            .map(control => control.get('baseCurrencyAmount').value)
            .reduce(sum, 0);
    }

    private resetForm(): void {
        this.form.reset();
    }

    private focusForm(): void {
        setTimeout(() => this.tapDate.focus(), 0);
    }

    private enableForm(): void {
        this.form.enable();
    }

    private disableForm(): void {
        this.form.disable();
    }

    private isOneSidedEntry(entry: FormGroup): boolean {
        return !!entry.get('debitAccountId').value !== !!entry.get('creditAccountId').value;
    }

    private hasNumberOfRows(n: number): boolean {
        return this.entries.length === n;
    }

    get menuActions(): MenuAction[] {
        return this.actions;
    }

    get lockedInfo(): string {
        return this.form.get('lockedInfo').value;
    }

    get isClosedTaxPeriod(): boolean {
        return this.lockedInfo === closedTaxPeriod;
    }

    get isClosedBusinessYear(): boolean {
        return this.lockedInfo === closedBusinessYear;
    }

    get isLoading(): boolean {
        return this.form.get('status').value === FormState.statusProgress;
    }

    get isLocked(): boolean {
        return this.form.get('isLocked').value;
    }

    get isExistingEntryCollection(): boolean {
        return !!this.form.get('id').value;
    }

    get isCompoundEntryCollection(): boolean {
        return this.entryType === ManualEntryCollection.compoundEntryType;
    }

    get isCreatingNewEntry(): boolean {
        return !this.isExistingEntryCollection && (this.form.dirty || !!this.loadedTemplate);
    }

    get entryType(): string {
        return this.form.get('type').value;
    }

    get entryTypeAttribute(): string {
        return this.isCompoundEntryCollection ? ManualEntryCollection.compoundEntryType : ManualEntryCollection.singleEntryType;
    }

    get isEntryCreateDisabled(): boolean {
        return this.form.invalid || !this.areEntriesBalanced;
    }

    get isEntryUpdateDisabled(): boolean {
        return !(this.form.valid && !this.form.pristine && this.areEntriesBalanced);
    }

    get entries(): FormArray {
        return <FormArray>this.form.get('entries');
    }

    get firstEntry(): FormGroup {
        return <FormGroup>this.entries.at(0);
    }

    get lastEntry(): FormGroup {
        return <FormGroup>this.entries.at(this.entries.length - 1);
    }

    get debitSum(): number {
        return this.sumAccountSide('debitAccountId');
    }

    get creditSum(): number {
        return this.sumAccountSide('creditAccountId');
    }

    get debitCreditSumDifference(): number {
        return Math.abs(this.debitSum - this.creditSum);
    }

    get isDebitCreditSumDifferenceGreaterMaxAmount(): boolean {
        return this.debitCreditSumDifference >= BookingFormComponent.maxAmount;
    }

    get areEntriesBalanced(): boolean {
        return this.debitCreditSumDifference < validationDelta;
    }

    get areEntriesValid(): boolean {
        const entries = <FormArray>this.form.get('entries');
        for (const entry of entries.controls) {
            const creditAccount = entry.get('creditAccountId').getError('isAccount');
            const debitAccount = entry.get('debitAccountId').getError('isAccount');
            if (creditAccount || debitAccount) {
                return false;
            }
        }
        return true;
    }
}
