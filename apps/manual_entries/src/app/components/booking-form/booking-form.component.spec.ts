import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { CommonModule as BxCommmonModule } from '@bx-client/common';
import { IndagiaService } from '@bx-client/common/src/services';
import { CdnServiceMockProvider } from '@bx-client/core/src/environments/service/cdn.service.mock';
import { indagiaStartDateToken } from '@bx-client/env';
import { FileUploadModule } from 'ng2-file-upload';
import { Ng2FilterPipeModule } from 'ng2-filter-pipe';

import { FilePreviewDirective } from '../../directives/file-preview.directive';
import { MaterialModule } from '../../material.module';
import { BookingEntryMockComponent } from '../../mocks/components';
import { CurrencyFormatAccessorMockDirective } from '../../mocks/directives/currency-format-accessor.mock.directive';
import { ToNumberMockPipe, TranslateMockPipe } from '../../mocks/pipes';
import { FileApiServiceMockProvider, FormServiceMockProvider } from '../../mocks/services';
import { ReversePipe } from '../../pipes/reverse.pipe';
import { SafeHtmlPipe } from '../../pipes/safe-html.pipe';
import { FileUploadDialogComponent } from '../file-upload-dialog/file-upload-dialog.component';
import { OverlayMenuComponent } from '../overlay-menu/overlay-menu.component';
import { SaveTemplateDialogComponent } from '../save-template-dialog/save-template-dialog.component';
import { TapDateComponent } from '../tap-date/tap-date.component';

import { BookingFormComponent } from './booking-form.component';

describe('BookingForm Component', () => {
    let component: BookingFormComponent;
    let fixture: ComponentFixture<BookingFormComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ReactiveFormsModule, FormsModule, BxCommmonModule, MaterialModule, FileUploadModule, Ng2FilterPipeModule],
            declarations: [
                BookingFormComponent,
                TapDateComponent,
                BookingEntryMockComponent,
                CurrencyFormatAccessorMockDirective,
                TranslateMockPipe,
                ToNumberMockPipe,
                OverlayMenuComponent,
                SaveTemplateDialogComponent,
                SafeHtmlPipe,
                FileUploadDialogComponent,
                FilePreviewDirective,
                ReversePipe
            ],
            providers: [
                CdnServiceMockProvider,
                FormServiceMockProvider,
                FileApiServiceMockProvider,
                IndagiaService,
                { provide: indagiaStartDateToken, useValue: '2019-12-01' }
            ]
        });

        fixture = TestBed.createComponent(BookingFormComponent);
        component = fixture.debugElement.componentInstance;

        fixture.detectChanges();

        component.form.patchValue({ type: 'manual_single_entry', date: new Date() });

        component.entries.at(0).patchValue({ debitAccountId: 1, creditAccountId: 2, amount: 0, currencyId: 1 });
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('should set disabled button if amount is 0', () => {
        fixture.detectChanges();
        expect(fixture.debugElement.query(By.css('#fibu\\.manual\\.form\\.submit')).nativeElement.disabled).toBe(true);
    });

    it('should set enable button if positive amount', () => {
        component.entries.at(0).patchValue({ amount: 1000 });
        fixture.detectChanges();
        expect(fixture.debugElement.query(By.css('#fibu\\.manual\\.form\\.submit')).nativeElement.disabled).toBe(false);
    });

    it('should set enable button if negative amount', () => {
        component.entries.at(0).patchValue({ amount: -1000 });
        fixture.detectChanges();
        expect(fixture.debugElement.query(By.css('#fibu\\.manual\\.form\\.submit')).nativeElement.disabled).toBe(false);
    });
});
