import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { MockComponent } from 'ng2-mock-component';

import { InputSelectValueDirective } from './input-select-value.directive';

describe('InputSelectValueDirective', () => {
    let fixture: any;

    const TestComponent: any = MockComponent({ template: '<div><input appInputSelectValue /></div>' });

    beforeEach(() => {
        TestBed.configureTestingModule({ declarations: [InputSelectValueDirective, TestComponent] });
    });

    it(
        'should handle onClick event',
        fakeAsync(() => {
            fixture = TestBed.createComponent(TestComponent);
            fixture.detectChanges();

            const directiveElement = fixture.debugElement.query(By.directive(InputSelectValueDirective));
            expect(directiveElement).not.toBeNull();

            const directiveInstance: InputSelectValueDirective = directiveElement.injector.get(InputSelectValueDirective);
            spyOn(directiveInstance, 'onFocus');
            const input = fixture.debugElement.query(By.css('input'));
            input.triggerEventHandler('focus', null);
            tick(1);
            expect(directiveInstance.onFocus).toHaveBeenCalledTimes(1);
        })
    );
});
