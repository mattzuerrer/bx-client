import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({ selector: 'input[appInputSelectValue]' })
export class InputSelectValueDirective {
    constructor(public element: ElementRef) {}

    @HostListener('focus')
    onFocus(): void {
        setTimeout(() => this.element.nativeElement.select(), 1);
    }
}
