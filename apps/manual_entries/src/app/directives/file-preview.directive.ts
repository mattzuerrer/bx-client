import { Directive, HostListener, Input } from '@angular/core';

import { File } from '../models/File';
import { ApiService } from '../services/api.service';

@Directive({ selector: '[appFilePreview]' })
export class FilePreviewDirective {
    @Input() file: File;

    constructor(private apiService: ApiService) {}

    @HostListener('click')
    downloadAndOpenFile(): void {
        const newTab = isMsSaveOrOpenBlob() ? null : window.open();

        this.apiService.downloadFile(this.file).subscribe(blob => {
            const url = window.URL.createObjectURL(blob);
            isMsSaveOrOpenBlob() ? handleMsBlob(blob, this.file.format()) : (newTab.document.location.href = url);
        });

        function isMsSaveOrOpenBlob(): boolean {
            return !!window.navigator && !!window.navigator.msSaveOrOpenBlob;
        }

        function handleMsBlob(blob: Blob, file: string): void {
            window.navigator.msSaveOrOpenBlob(blob, file);
        }
    }
}
