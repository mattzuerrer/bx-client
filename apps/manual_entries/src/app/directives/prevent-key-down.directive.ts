import { Directive, HostListener, Input } from '@angular/core';
import * as keycode from 'keycode';

@Directive({ selector: '[appPreventKeyDown]' })
export class PreventKeyDownDirective {
    @Input() appPreventKeyDown: string;

    @HostListener('keydown', ['$event'])
    onKeydown(event: KeyboardEvent): void {
        if (keycode(event) === this.appPreventKeyDown) {
            event.preventDefault();
        }
    }
}
