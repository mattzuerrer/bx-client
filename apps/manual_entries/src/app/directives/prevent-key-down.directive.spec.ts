import { TestBed } from '@angular/core/testing';
import { getChildDebugElementByCss } from '@bx-client/common';
import { MockComponent } from 'ng2-mock-component';
import { noop } from 'rxjs';

import { PreventKeyDownDirective } from './prevent-key-down.directive';

describe('PreventKeyDownDirective', () => {
    let fixture: any;

    const TestComponent: any = MockComponent({ template: '<div appPreventKeyDown="backspace"></div>' });

    beforeEach(() => {
        TestBed.configureTestingModule({ declarations: [PreventKeyDownDirective, TestComponent] });
    });

    it('should prevent event default on backspace keydown', () => {
        fixture = TestBed.createComponent(TestComponent);
        fixture.detectChanges();
        const event: any = {
            keyCode: 8,
            preventDefault: () => noop()
        };
        spyOn(event, 'preventDefault');
        getChildDebugElementByCss(fixture, 'div').triggerEventHandler('keydown', event);
        expect(event.preventDefault).toHaveBeenCalledTimes(1);
    });
});
