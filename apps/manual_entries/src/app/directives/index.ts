import { FilePreviewDirective } from './file-preview.directive';
import { InputSelectValueDirective } from './input-select-value.directive';
import { PreventKeyDownDirective } from './prevent-key-down.directive';

export const appDirectives = [PreventKeyDownDirective, InputSelectValueDirective, FilePreviewDirective];
