import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'toNumber' })
export class ToNumberPipe implements PipeTransform {
    transform(value: any): any {
        const toNumber = Number(value);
        return isNaN(toNumber) ? 0 : toNumber;
    }
}
