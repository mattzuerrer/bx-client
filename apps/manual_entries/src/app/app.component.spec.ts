import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CommonModule as BxCommonModule } from '@bx-client/common';
import { ToastsManagerServiceMockProvider } from '@bx-client/message';
import 'hammerjs';
import { MockComponent } from 'ng2-mock-component';

import { AppActions } from './actions';
import { AppComponent } from './app.component';
import { AppServiceMock, AppServiceMockProvider, StateServiceMock, StateServiceMockProvider } from './mocks/services';
import { AppService, StateService } from './services';

const RouterOutletMockComponent: any = MockComponent({
    selector: 'router-outlet',
    template: '<div></div>',
    inputs: []
});

describe('AppComponent', () => {
    let fixture;
    let component;
    let appService: AppServiceMock;
    let stateService: StateServiceMock;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [AppComponent, RouterOutletMockComponent],
            imports: [BxCommonModule, RouterTestingModule.withRoutes([])],
            providers: [AppServiceMockProvider, StateServiceMockProvider, ToastsManagerServiceMockProvider]
        });

        fixture = TestBed.createComponent(AppComponent);
        component = fixture.debugElement.componentInstance;
        appService = fixture.debugElement.injector.get(AppService);
        stateService = fixture.debugElement.injector.get(StateService);
    });

    it('should create the app', () => {
        expect(component).toBeTruthy();
    });

    it(`should init app and app service`, () => {
        spyOn(appService, 'init');
        spyOn(stateService, 'dispatch');
        fixture.detectChanges();
        expect(appService.init).toHaveBeenCalledTimes(1);
        expect(stateService.dispatch).toHaveBeenCalledWith(new AppActions.InitAppAction());
        expect(stateService.dispatch).toHaveBeenCalledTimes(1);
    });
});
