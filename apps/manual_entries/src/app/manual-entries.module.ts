import { NgModule } from '@angular/core';
import { AutocompleteModule, CommonModule } from '@bx-client/common';
import { CoreModule } from '@bx-client/core';
import { AccountModule } from '@bx-client/features/account';
import { CompanyModule } from '@bx-client/features/company';
import { CurrencyModule } from '@bx-client/features/currency';
import { HotkeysModule } from '@bx-client/hotkeys';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { FileUploadModule } from 'ng2-file-upload';
import { Ng2FilterPipeModule } from 'ng2-filter-pipe';

import { appComponents, appDynamicComponents } from './components';
import { featureName } from './config';
import { appContainers } from './containers';
import { appDirectives } from './directives';
import { appEffects } from './effects';
import { appGuards } from './guards';
import { routing } from './manual-entries.routing';
import { MaterialModule } from './material.module';
import { appPipes } from './pipes';
import { reducers } from './reducers';
import { appServices } from './services';
import { appUtil } from './util';

@NgModule({
    imports: [
        routing,
        CoreModule,
        AccountModule,
        CurrencyModule,
        CompanyModule,
        CommonModule,
        MaterialModule,
        FileUploadModule,
        Ng2FilterPipeModule,
        HotkeysModule,
        AutocompleteModule,
        StoreModule.forFeature(featureName, reducers),
        EffectsModule.forFeature(appEffects)
    ],
    declarations: [...appContainers, ...appComponents, ...appDirectives, ...appPipes],
    entryComponents: [...appDynamicComponents],
    providers: [...appServices, ...appGuards, ...appUtil]
})
export class ManualEntriesModule {}
