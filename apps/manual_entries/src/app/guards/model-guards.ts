import { ManualEntryCollection, ManualEntryTemplate } from '../models';

export function isManualEntryCollection(value: any): value is ManualEntryCollection {
    return value instanceof ManualEntryCollection;
}

export function isManualEntryTemplate(value: any): value is ManualEntryTemplate {
    return value instanceof ManualEntryTemplate;
}
