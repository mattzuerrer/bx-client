import { ManualEntryCollectionExistsGuard } from './manual-entry-collection-exists.guard';
import { StatesLoadedGuard } from './states-loaded.guard';

export const appGuards = [ManualEntryCollectionExistsGuard, StatesLoadedGuard];
