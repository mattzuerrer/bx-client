import { inject, TestBed } from '@angular/core/testing';

import { EntryCollectionActions } from '../actions';
import { StateServiceMock, StateServiceMockProvider } from '../mocks/services';
import { StateService } from '../services';

import { StatesLoadedGuard } from './states-loaded.guard';

describe('States loaded guard', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({ providers: [StatesLoadedGuard, StateServiceMockProvider] });
    });

    it(
        'should not activate route until state is loaded',
        inject([StatesLoadedGuard, StateService], (guard: StatesLoadedGuard, stateService: StateServiceMock) => {
            spyOn(stateService, 'dispatch');
            let canActivate = null;
            const subscription = guard.canActivate().subscribe(change => (canActivate = change));
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new EntryCollectionActions.ResetAction());
            stateService.statesLoaded$.next(false);
            stateService.statesLoaded$.next(false);
            expect(canActivate).toBe(null);
            stateService.statesLoaded$.next(true);
            stateService.statesLoaded$.next(false);
            expect(canActivate).toBe(true);
            subscription.unsubscribe();
        })
    );
});
