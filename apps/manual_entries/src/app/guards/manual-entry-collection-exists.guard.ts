import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { byId, identity } from '@bx-client/common';
import { Observable, of } from 'rxjs';
import { catchError, filter, map, switchMap, take, tap } from 'rxjs/operators';

import { EntryCollectionActions, ListActions } from '../actions';
import { ApiService, StateService } from '../services';

@Injectable()
export class ManualEntryCollectionExistsGuard implements CanActivate {
    constructor(private apiService: ApiService, private stateService: StateService) {}

    canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
        const id = parseInt(route.params.id, 10);
        return this.hasEntry(id).pipe(switchMap(hasId => (hasId ? this.waitForStatesLoaded() : of(false))));
    }

    private waitForStatesLoaded(): Observable<boolean> {
        return this.stateService.statesLoaded$.pipe(filter(identity), take(1));
    }

    private hasEntry(id: number): Observable<boolean> {
        return this.hasEntryInList(id).pipe(switchMap(inList => (inList ? of(true) : this.hasEntryInApi(id))));
    }

    private hasEntryInList(id: number): Observable<boolean> {
        return this.stateService.listEntries$.pipe(
            take(1),
            map(entries => entries.find(byId(id))),
            switchMap(entry => {
                const hasEntry = !!entry;
                if (hasEntry) {
                    this.stateService.dispatch(new ListActions.SelectAction(entry));
                }
                return of(hasEntry);
            })
        );
    }

    private hasEntryInApi(id: number): Observable<boolean> {
        this.stateService.dispatch(new EntryCollectionActions.LoadEntryCollectionAction());
        return this.apiService
            .getManualEntryCollection(id)
            .pipe(
                map(entry => new ListActions.SelectAction(entry)),
                tap(action => this.stateService.dispatch(action)),
                map(() => true),
                catchError(() =>
                    of(false).pipe(tap(() => this.stateService.dispatch(new EntryCollectionActions.LoadEntryCollectionFailAction(id))))
                )
            );
    }
}
