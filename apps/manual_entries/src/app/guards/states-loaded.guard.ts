import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, take } from 'rxjs/operators';

import { EntryCollectionActions } from '../actions';
import { StateService } from '../services';

@Injectable()
export class StatesLoadedGuard implements CanActivate {
    constructor(private stateService: StateService) {}

    canActivate(): Observable<boolean> {
        this.stateService.dispatch(new EntryCollectionActions.ResetAction());
        return this.waitForStatesLoaded();
    }

    private waitForStatesLoaded(): Observable<boolean> {
        return this.stateService.statesLoaded$.pipe(filter(isLoaded => isLoaded), take(1));
    }
}
