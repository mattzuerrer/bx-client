import { inject, TestBed } from '@angular/core/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { Observable } from 'rxjs';

import { EntryCollectionActions, ListActions, TemplatesActions } from '../actions';
import { collection } from '../mocks/fixtures/entry-collection.fixtures';
import { ApiServiceMockProvider, FormServiceMockProvider } from '../mocks/services';
import { FormService } from '../services';

import { FormEffects } from './form.effects';

describe('Form effects', () => {
    let formEffects: FormEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<FormEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [FormEffects, ApiServiceMockProvider, FormServiceMockProvider, provideMockActions(() => actions)]
        });

        formEffects = TestBed.get(FormEffects);
        metadata = getEffectsMetadata(formEffects);
    });

    it(
        'should resetForm on SELECT',
        inject([FormService], formService => {
            actions = cold('a', { a: new ListActions.SelectAction(collection) });
            spyOn(formService, 'resetForm').and.callThrough();
            const expected = cold('b', { b: new ListActions.SelectAction(collection) });
            expect(formEffects.reset$).toBeObservable(expected);
            expect(formService.resetForm).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should resetForm on SELECT_TEMPLATE',
        inject([FormService], formService => {
            actions = cold('a', { a: new TemplatesActions.SelectTemplateAction(collection) });
            spyOn(formService, 'resetForm').and.callThrough();
            const expected = cold('b', { b: new TemplatesActions.SelectTemplateAction(collection) });
            expect(formEffects.reset$).toBeObservable(expected);
            expect(formService.resetForm).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should resetForm on RESET',
        inject([FormService], formService => {
            actions = cold('a', { a: new EntryCollectionActions.ResetAction() });
            spyOn(formService, 'resetForm').and.callThrough();
            const expected = cold('b', { b: new EntryCollectionActions.ResetAction() });
            expect(formEffects.reset$).toBeObservable(expected);
            expect(formService.resetForm).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should resetTemplate on RESET',
        inject([FormService], formService => {
            actions = cold('a', { a: new EntryCollectionActions.ResetAction() });
            spyOn(formService, 'resetTemplate').and.callThrough();
            const expected = cold('b', { b: new EntryCollectionActions.ResetAction() });
            expect(formEffects.resetTemplate$).toBeObservable(expected);
            expect(formService.resetTemplate).toHaveBeenCalledTimes(1);
        })
    );

    it('should test metadata', () => {
        expect(metadata.reset$).toEqual({ dispatch: false });
        expect(metadata.resetTemplate$).toEqual({ dispatch: false });
    });
});
