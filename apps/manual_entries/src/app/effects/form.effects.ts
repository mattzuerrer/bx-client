import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { EntryCollectionActions, ListActions, TemplatesActions } from '../actions';
import { FormService } from '../services';

@Injectable()
export class FormEffects {
    // TODO Find a way to change this syntax to new one
    @Effect({ dispatch: false })
    reset$: Observable<any> = this.actions$
        .ofType(ListActions.SELECT, TemplatesActions.SELECT_TEMPLATE, EntryCollectionActions.RESET)
        .pipe(tap(() => this.formService.resetForm()));

    @Effect({ dispatch: false })
    resetTemplate$: Observable<any> = this.actions$.pipe(
        ofType<EntryCollectionActions.ResetAction>(EntryCollectionActions.RESET),
        tap(() => this.formService.resetTemplate())
    );

    constructor(private actions$: Actions, private formService: FormService) {}
}
