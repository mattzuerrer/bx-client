import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';

import { TemplatesActions } from '../actions';
import { ApiService } from '../services';

@Injectable()
export class TemplatesEffects {
    @Effect()
    fetch$: Observable<any> = this.actions$.pipe(
        ofType<TemplatesActions.LoadAction>(TemplatesActions.LOAD),
        startWith(new TemplatesActions.LoadAction()),
        switchMap(() =>
            this.apiService
                .getManualEntryTemplates()
                .pipe(
                    map(templates => new TemplatesActions.LoadSuccessAction(templates)),
                    catchError(() => of(new TemplatesActions.LoadFailAction()))
                )
        )
    );

    @Effect()
    create$: Observable<any> = this.actions$.pipe(
        ofType<TemplatesActions.CreateAction>(TemplatesActions.CREATE),
        map((action: TemplatesActions.CreateAction) => action.payload),
        switchMap(payload =>
            this.apiService
                .createManualEntryTemplate(payload)
                .pipe(
                    map(template => new TemplatesActions.CreateSuccessAction(template)),
                    catchError(() => of(new TemplatesActions.CreateFailAction(payload)))
                )
        )
    );

    @Effect()
    update$: Observable<any> = this.actions$.pipe(
        ofType<TemplatesActions.UpdateAction>(TemplatesActions.UPDATE),
        map((action: TemplatesActions.UpdateAction) => action.payload),
        switchMap(payload =>
            this.apiService
                .updateManualEntryTemplate(payload)
                .pipe(
                    map(template => new TemplatesActions.UpdateSuccessAction(template)),
                    catchError(() => of(new TemplatesActions.UpdateFailAction(payload)))
                )
        )
    );

    @Effect()
    delete$: Observable<any> = this.actions$.pipe(
        ofType<TemplatesActions.DeleteAction>(TemplatesActions.DELETE),
        map((action: TemplatesActions.DeleteAction) => action.payload),
        switchMap(id =>
            this.apiService
                .deleteManualEntryTemplate(id)
                .pipe(
                    map(template => new TemplatesActions.DeleteSuccessAction(id)),
                    catchError(() => of(new TemplatesActions.DeleteFailAction(id)))
                )
        )
    );

    constructor(private actions$: Actions, private apiService: ApiService) {}
}
