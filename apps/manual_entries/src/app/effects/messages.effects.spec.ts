import { TestBed } from '@angular/core/testing';
import { AccountActions } from '@bx-client/features/account';
import { CompanyActions } from '@bx-client/features/company';
import { CurrencyActions } from '@bx-client/features/currency';
import { MessageActions } from '@bx-client/message';
import { RouterActions } from '@bx-client/router';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { Observable } from 'rxjs';

import { EntryCollectionActions, ListActions, TaxesActions, TemplatesActions } from '../actions';
import { collection } from '../mocks/fixtures/entry-collection.fixtures';
import { templates as templatesFixture } from '../mocks/fixtures/templates.fixtures';
import { ApiServiceMockProvider } from '../mocks/services';

import { MessagesEffects } from './messages.effects';

describe('Messages effects', () => {
    let messagesEffects: MessagesEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<MessagesEffects>;

    const date = new Date('01.01.2000');
    const id = 1;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [MessagesEffects, ApiServiceMockProvider, provideMockActions(() => actions)]
        });

        messagesEffects = TestBed.get(MessagesEffects);
        metadata = getEffectsMetadata(messagesEffects);
    });

    it('should return ERROR_MSG on LOAD_FAIL action (AccountActions)', () => {
        actions = cold('a', { a: new AccountActions.LoadFailAction() });
        const payload = {
            messageKey: 'error.server.fetch_accounts',
            titleKey: 'globals.try_again',
            options: { data: { action: new AccountActions.LoadAction() } }
        };

        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(messagesEffects.accountsLoadFail$).toBeObservable(expected);
    });

    it('should return ERROR_MSG on LOAD_FAIL action (CurrencyActions)', () => {
        actions = cold('a', { a: new CurrencyActions.LoadFailAction() });
        const payload = {
            messageKey: 'error.server.fetch_currencies',
            titleKey: 'globals.try_again',
            options: { data: { action: new CurrencyActions.LoadAction() } }
        };

        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(messagesEffects.currenciesLoadFail$).toBeObservable(expected);
    });

    it('should return ERROR_MSG on LOAD_FAIL action (AccountActions)', () => {
        actions = cold('a', { a: new TaxesActions.LoadFailAction(date) });
        const payload = {
            messageKey: 'error.server.fetch_taxes',
            titleKey: 'globals.try_again',
            options: { data: { action: new TaxesActions.LoadAction(date) } }
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(messagesEffects.taxesLoadFail$).toBeObservable(expected);
    });

    it('should return ERROR_MSG on LOAD_FAIL action (ListActions)', () => {
        actions = cold('a', { a: new ListActions.LoadFailAction([collection]) });
        const payload = {
            messageKey: 'error.server.fetch_list',
            titleKey: 'globals.try_again',
            options: { data: { action: new ListActions.LoadAction() } }
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(messagesEffects.listLoadFail$).toBeObservable(expected);
    });

    it('should return ERROR_MSG on LOAD_FAIL action (CompanyActions)', () => {
        actions = cold('a', { a: new CompanyActions.LoadFailAction() });
        const payload = {
            messageKey: 'error.server.fetch_user',
            titleKey: 'globals.try_again',
            options: { data: { action: new CompanyActions.LoadAction() } }
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(messagesEffects.userLoadFail$).toBeObservable(expected);
    });

    it('should return ERROR_MSG on LOAD_REFERENCE_NR_FAIL action', () => {
        actions = cold('a', { a: new EntryCollectionActions.LoadReferenceNrFailAction() });
        const payload = {
            messageKey: 'error.server.fetch_reference_nr',
            titleKey: 'globals.try_again',
            options: { data: { action: new EntryCollectionActions.LoadReferenceNrAction() } }
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(messagesEffects.referenceNrLoadFail$).toBeObservable(expected);
    });

    it('should return ERROR_MSG LOAD_ENTRY_COLLECTION_FAIL action', () => {
        actions = cold('a', { a: new EntryCollectionActions.LoadEntryCollectionFailAction(id) });
        const payload = {
            messageKey: 'error.server.fetch_manual_entry',
            titleKey: 'globals.try_again',
            options: { data: { action: new RouterActions.Go({ path: ['/id/', id] }) } }
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(messagesEffects.manualEntryLoadFail$).toBeObservable(expected);
    });

    it('should return ERROR_MSG on SAVE_ENTRY_COLLECTION_FAIL action', () => {
        actions = cold('a', { a: new EntryCollectionActions.SaveEntryCollectionFailAction(collection) });
        const payload = {
            messageKey: 'error.server.save_manual_entry',
            titleKey: 'globals.try_again',
            options: { data: { action: new EntryCollectionActions.SaveEntryCollectionAction(collection) } }
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(messagesEffects.manualEntrySaveFail$).toBeObservable(expected);
    });

    it('should return ERROR_MSG on UPDATE_ENTRY_COLLECTION_FAIL action', () => {
        actions = cold('a', { a: new EntryCollectionActions.UpdateEntryCollectionFailAction(collection) });
        const payload = {
            messageKey: 'error.server.update_manual_entry',
            titleKey: 'globals.try_again',
            options: { data: { action: new EntryCollectionActions.UpdateEntryCollectionAction(collection) } }
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(messagesEffects.manualEntryUpdateFail$).toBeObservable(expected);
    });

    it('should return SUCCESS_MSG on DELETE_ENTRY_COLLECTION_SUCCESS action', () => {
        actions = cold('a', { a: new EntryCollectionActions.DeleteEntryCollectionSuccessAction(id) });
        const payload = {
            messageKey: 'success.server.delete_manual_entry'
        };
        const expected = cold('b', { b: new MessageActions.SuccessMessage(payload) });
        expect(messagesEffects.manualEntryDeleteSuccess$).toBeObservable(expected);
    });

    it('should return ERROR_MSG on DELETE_ENTRY_COLLECTION_FAIL action', () => {
        actions = cold('a', { a: new EntryCollectionActions.DeleteEntryCollectionFailAction(id) });
        const payload = {
            messageKey: 'error.server.delete_manual_entry',
            titleKey: 'globals.try_again',
            options: { data: { action: new EntryCollectionActions.DeleteEntryCollectionAction(id) } }
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(messagesEffects.manualEntryDeleteFail$).toBeObservable(expected);
    });

    it('should return SUCCESS_MSG on CREATE_SUCCESS action', () => {
        actions = cold('a', { a: new TemplatesActions.CreateSuccessAction(templatesFixture[0]) });
        const payload = {
            messageKey: 'globals.template.save.message_success',
            options: { enableHtml: true }
        };
        const expected = cold('b', { b: new MessageActions.SuccessMessage(payload) });
        expect(messagesEffects.manualEntryTemplateCreateSuccess$).toBeObservable(expected);
    });

    it('should return ERROR_MSG on CREATE_FAIL action', () => {
        actions = cold('a', { a: new TemplatesActions.CreateFailAction(templatesFixture[0]) });
        const payload = {
            messageKey: 'error.server.save_manual_entry_template',
            titleKey: 'globals.try_again',
            options: { data: { action: new TemplatesActions.CreateAction(templatesFixture[0]) } }
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(messagesEffects.manualEntryTemplateCreateFail$).toBeObservable(expected);
    });

    it('should return SUCCESS_MSG on UPDATE_SUCCESS action', () => {
        actions = cold('a', { a: new TemplatesActions.UpdateSuccessAction(templatesFixture[0]) });
        const payload = {
            messageKey: 'globals.template.save.message_success',
            options: { enableHtml: true }
        };
        const expected = cold('b', { b: new MessageActions.SuccessMessage(payload) });
        expect(messagesEffects.manualEntryTemplateUpdateSuccess$).toBeObservable(expected);
    });

    it('should return ERROR_MSG on UPDATE_FAIL action', () => {
        actions = cold('a', { a: new TemplatesActions.UpdateFailAction(templatesFixture[0]) });
        const payload = {
            messageKey: 'error.server.save_manual_entry_template',
            titleKey: 'globals.try_again',
            options: { data: { action: new TemplatesActions.UpdateAction(templatesFixture[0]) } }
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(messagesEffects.manualEntryTemplateUpdateFail$).toBeObservable(expected);
    });

    it('should return SUCCESS_MSG on DELETE_SUCCESS action', () => {
        actions = cold('a', { a: new TemplatesActions.DeleteSuccessAction(id) });
        const payload = {
            messageKey: 'globals.template.delete.message_success',
            options: { enableHtml: true }
        };
        const expected = cold('b', { b: new MessageActions.SuccessMessage(payload) });
        expect(messagesEffects.manualEntryTemplateDeleteSuccess$).toBeObservable(expected);
    });

    it('should return ERROR_MSG on DELETE_FAIL action', () => {
        actions = cold('a', { a: new TemplatesActions.DeleteFailAction(id) });
        const payload = {
            messageKey: 'error.server.delete_manual_entry_template',
            titleKey: 'globals.try_again',
            options: { data: { action: new TemplatesActions.DeleteAction(id) } }
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(messagesEffects.manualEntryTemplateDeleteFail$).toBeObservable(expected);
    });

    it('should test metadata', () => {
        expect(metadata.accountsLoadFail$).toEqual({ dispatch: true });
        expect(metadata.currenciesLoadFail$).toEqual({ dispatch: true });
        expect(metadata.taxesLoadFail$).toEqual({ dispatch: true });
        expect(metadata.listLoadFail$).toEqual({ dispatch: true });
        expect(metadata.userLoadFail$).toEqual({ dispatch: true });
        expect(metadata.referenceNrLoadFail$).toEqual({ dispatch: true });
        expect(metadata.manualEntryLoadFail$).toEqual({ dispatch: true });
        expect(metadata.manualEntrySaveFail$).toEqual({ dispatch: true });
        expect(metadata.manualEntryUpdateFail$).toEqual({ dispatch: true });
        expect(metadata.manualEntryDeleteSuccess$).toEqual({ dispatch: true });
        expect(metadata.manualEntryDeleteFail$).toEqual({ dispatch: true });
        expect(metadata.manualEntryTemplateCreateSuccess$).toEqual({ dispatch: true });
        expect(metadata.manualEntryTemplateCreateFail$).toEqual({ dispatch: true });
        expect(metadata.manualEntryTemplateUpdateSuccess$).toEqual({ dispatch: true });
        expect(metadata.manualEntryTemplateUpdateFail$).toEqual({ dispatch: true });
        expect(metadata.manualEntryTemplateDeleteSuccess$).toEqual({ dispatch: true });
        expect(metadata.manualEntryTemplateDeleteFail$).toEqual({ dispatch: true });
    });
});
