import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, startWith, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { ListActions } from '../actions';
import { ManualEntryCollection } from '../models';
import { ApiService, ScrollService, StateService } from '../services';

const assignById = (id: number, info: any) => item => (item.id === id ? Object.assign(item, info) : item);

@Injectable()
export class ListEffects {
    // TODO Find a way to change this to new syntax
    @Effect()
    fetch$: Observable<any> = this.actions$
        .ofType(ListActions.LOAD, ListActions.SORT)
        .pipe(
            startWith(new ListActions.LoadAction()),
            withLatestFrom(this.stateService.columnConfig$, (_, columnConfig) => columnConfig),
            map(columnConfig => columnConfig.join(',')),
            switchMap(orderBy =>
                this.apiService
                    .getManualEntryCollections(orderBy)
                    .pipe(
                        withLatestFrom(this.stateService.manualEntryCollection$, (collections, entry) =>
                            collections.map(assignById(entry.id, { status: ManualEntryCollection.statusSelected }))
                        ),
                        map(manualEntryCollections => new ListActions.LoadSuccessAction(manualEntryCollections)),
                        catchError(() => of(new ListActions.LoadFailAction([])))
                    )
            )
        );

    @Effect({ dispatch: false })
    scroll$: Observable<any> = this.actions$.pipe(
        ofType<ListActions.SelectAction>(ListActions.SELECT),
        tap(() => this.scrollService.scrollToTop())
    );

    constructor(
        private actions$: Actions,
        private apiService: ApiService,
        private scrollService: ScrollService,
        private stateService: StateService
    ) {}
}
