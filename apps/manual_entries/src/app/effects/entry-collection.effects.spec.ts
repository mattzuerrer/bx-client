import { inject, TestBed } from '@angular/core/testing';
import { byId } from '@bx-client/common';
import { AccountActions } from '@bx-client/features/account';
import { RouterActions } from '@bx-client/router';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { Observable, of, throwError } from 'rxjs';

import { EntryCollectionActions, TemplatesActions } from '../actions';
import { collection } from '../mocks/fixtures/entry-collection.fixtures';
import { taxes } from '../mocks/fixtures/taxes.fixtures';
import { ApiServiceMockProvider, StateServiceMockProvider } from '../mocks/services';
import { File, RefNumber } from '../models';
import { ApiService, StateService } from '../services';

import { EntryCollectionEffects } from './entry-collection.effects';

describe('Accounts effects', () => {
    let entryCollectionEffects: EntryCollectionEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<EntryCollectionEffects>;

    const refNum = Object.assign({}, new RefNumber(), { nextRefNr: 'refNr2' });
    const id = 1;
    const collectionWithTaxes = Object.assign(collection, {
        entries: collection.entries.map(entry => Object.assign(entry, { tax: taxes.find(byId(entry.taxId)), files: [new File()] }))
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [EntryCollectionEffects, ApiServiceMockProvider, StateServiceMockProvider, provideMockActions(() => actions)]
        });

        const stateService = TestBed.get(StateService);
        stateService.taxes$ = of(taxes);
        entryCollectionEffects = TestBed.get(EntryCollectionEffects);
        metadata = getEffectsMetadata(entryCollectionEffects);
    });

    it(
        'should return LOAD_REFERENCE_NR_SUCCESS if RESET is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new EntryCollectionActions.ResetAction() });
            spyOn(apiService, 'getNextRefNumber').and.returnValue(of(refNum));
            const expected = cold('b', {
                b: new EntryCollectionActions.LoadReferenceNrSuccessAction(refNum.nextRefNr)
            });
            expect(entryCollectionEffects.fetch$).toBeObservable(expected);
            expect(apiService.getNextRefNumber).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_REFERENCE_NR_SUCCESS if LOAD_REFERENCE_NR is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new EntryCollectionActions.LoadReferenceNrAction() });
            spyOn(apiService, 'getNextRefNumber').and.returnValue(of(refNum));
            const expected = cold('b', {
                b: new EntryCollectionActions.LoadReferenceNrSuccessAction(refNum.nextRefNr)
            });
            expect(entryCollectionEffects.fetch$).toBeObservable(expected);
            expect(apiService.getNextRefNumber).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_REFERENCE_NR_SUCCESS if SELECT_TEMPLATE is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new TemplatesActions.SelectTemplateAction(collection) });
            spyOn(apiService, 'getNextRefNumber').and.returnValue(of(refNum));
            const expected = cold('b', {
                b: new EntryCollectionActions.LoadReferenceNrSuccessAction(refNum.nextRefNr)
            });
            expect(entryCollectionEffects.fetch$).toBeObservable(expected);
            expect(apiService.getNextRefNumber).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_REFERENCE_NR_FAIL if RESET fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new EntryCollectionActions.ResetAction() });
            spyOn(apiService, 'getNextRefNumber').and.returnValue(throwError(null));
            const expected = cold('b', { b: new EntryCollectionActions.LoadReferenceNrFailAction() });
            expect(entryCollectionEffects.fetch$).toBeObservable(expected);
            expect(apiService.getNextRefNumber).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return SAVE_ENTRY_COLLECTION_SUCCESS if SAVE_ENTRY_COLLECTION is successful',
        inject([ApiService, StateService], (apiService, stateService) => {
            actions = cold('a', { a: new EntryCollectionActions.SaveEntryCollectionAction(collection) });
            spyOn(apiService, 'createCollection').and.returnValue(of(collection));
            spyOn(stateService, 'dispatch').and.callThrough();
            const expected = cold('b', {
                b: new EntryCollectionActions.SaveEntryCollectionSuccessAction(collectionWithTaxes)
            });
            expect(entryCollectionEffects.save$).toBeObservable(expected);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(apiService.createCollection).toHaveBeenCalledTimes(1);
            expect(apiService.createCollection).toHaveBeenCalledWith(collection);
        })
    );

    it(
        'should return SAVE_ENTRY_COLLECTION_SUCCESS if SAVE_ENTRY_COLLECTION fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new EntryCollectionActions.SaveEntryCollectionAction(collection) });
            spyOn(apiService, 'createCollection').and.returnValue(throwError(null));
            const expected = cold('b', { b: new EntryCollectionActions.SaveEntryCollectionFailAction(collection) });
            expect(entryCollectionEffects.save$).toBeObservable(expected);
            expect(apiService.createCollection).toHaveBeenCalledTimes(1);
            expect(apiService.createCollection).toHaveBeenCalledWith(collection);
        })
    );

    it(
        'should return UPDATE_ENTRY_COLLECTION_SUCCESS if UPDATE_ENTRY_COLLECTION is successful',
        inject([ApiService, StateService], (apiService, stateService) => {
            actions = cold('a', { a: new EntryCollectionActions.UpdateEntryCollectionAction(collection) });
            spyOn(apiService, 'updateCollection').and.returnValue(of(collection));
            spyOn(stateService, 'dispatch').and.callThrough();
            const expected = cold('b', {
                b: new EntryCollectionActions.UpdateEntryCollectionSuccessAction(collectionWithTaxes)
            });
            expect(entryCollectionEffects.update$).toBeObservable(expected);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new EntryCollectionActions.UpdateEntryCollectionFilesAction(collection));
            expect(apiService.updateCollection).toHaveBeenCalledTimes(1);
            expect(apiService.updateCollection).toHaveBeenCalledWith(collection);
        })
    );

    it(
        'should return UPDATE_ENTRY_COLLECTION_FAIL if UPDATE_ENTRY_COLLECTION fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new EntryCollectionActions.UpdateEntryCollectionAction(collection) });
            spyOn(apiService, 'updateCollection').and.returnValue(throwError(null));
            const expected = cold('b', { b: new EntryCollectionActions.UpdateEntryCollectionFailAction(collection) });
            expect(entryCollectionEffects.update$).toBeObservable(expected);
            expect(apiService.updateCollection).toHaveBeenCalledTimes(1);
            expect(apiService.updateCollection).toHaveBeenCalledWith(collection);
        })
    );

    it(
        'should return UPDATE_ENTRY_COLLECTION_SUCCESS if UPDATE_ENTRY_COLLECTION_FILES is successful',
        inject([StateService], stateService => {
            actions = cold('a', { a: new EntryCollectionActions.UpdateEntryCollectionFilesAction(collection) });
            spyOn(stateService, 'dispatch').and.callThrough();
            const expected = cold('b', { b: collection });
            expect(entryCollectionEffects.updateFiles$).toBeObservable(expected);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return DELETE_ENTRY_COLLECTION_SUCCESS if DELETE_ENTRY_COLLECTION is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new EntryCollectionActions.DeleteEntryCollectionAction(id) });
            spyOn(apiService, 'deleteManualEntryCollection').and.returnValue(of(collection));
            const expected = cold('b', { b: new EntryCollectionActions.DeleteEntryCollectionSuccessAction(id) });
            expect(entryCollectionEffects.delete$).toBeObservable(expected);
            expect(apiService.deleteManualEntryCollection).toHaveBeenCalledTimes(1);
            expect(apiService.deleteManualEntryCollection).toHaveBeenCalledWith(id);
        })
    );

    it(
        'should return DELETE_ENTRY_COLLECTION_FAIL if DELETE_ENTRY_COLLECTION fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new EntryCollectionActions.DeleteEntryCollectionAction(id) });
            spyOn(apiService, 'deleteManualEntryCollection').and.returnValue(throwError(null));
            const expected = cold('b', { b: new EntryCollectionActions.DeleteEntryCollectionFailAction(id) });
            expect(entryCollectionEffects.delete$).toBeObservable(expected);
            expect(apiService.deleteManualEntryCollection).toHaveBeenCalledTimes(1);
            expect(apiService.deleteManualEntryCollection).toHaveBeenCalledWith(id);
        })
    );

    it(
        'should return RESET on SAVE_ENTRY_COLLECTION_SUCCESS',
        inject([StateService], stateService => {
            actions = cold('a', { a: new EntryCollectionActions.SaveEntryCollectionSuccessAction(collection) });
            spyOn(stateService, 'dispatch').and.callThrough();
            const expected = cold('b', { b: new EntryCollectionActions.SaveEntryCollectionSuccessAction(collection) });
            expect(entryCollectionEffects.saveSuccess$).toBeObservable(expected);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new EntryCollectionActions.ResetAction());
        })
    );

    it(
        'should return GO on DELETE_ENTRY_COLLECTION_SUCCESS',
        inject([StateService], stateService => {
            actions = cold('a', { a: new EntryCollectionActions.DeleteEntryCollectionSuccessAction(id) });
            spyOn(stateService, 'dispatch').and.callThrough();
            const expected = cold('b', { b: new EntryCollectionActions.DeleteEntryCollectionSuccessAction(id) });
            expect(entryCollectionEffects.refreshSuccess$).toBeObservable(expected);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new RouterActions.Go({ path: [''] }));
        })
    );

    it(
        'should return GO on UPDATE_ENTRY_COLLECTION_SUCCESS',
        inject([StateService], stateService => {
            actions = cold('a', { a: new EntryCollectionActions.UpdateEntryCollectionSuccessAction(collection) });
            spyOn(stateService, 'dispatch').and.callThrough();
            const expected = cold('b', {
                b: new EntryCollectionActions.UpdateEntryCollectionSuccessAction(collection)
            });
            expect(entryCollectionEffects.refreshSuccess$).toBeObservable(expected);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new RouterActions.Go({ path: [''] }));
        })
    );

    it(
        'should return LOAD_BALANCE on SAVE_ENTRY_COLLECTION_SUCCESS',
        inject([StateService], stateService => {
            actions = cold('a', { a: new EntryCollectionActions.SaveEntryCollectionSuccessAction(collection) });
            const expected = cold('b', { b: new AccountActions.LoadBalance() });
            expect(entryCollectionEffects.fetchAccountGroups$).toBeObservable(expected);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetch$).toEqual({ dispatch: true });
        expect(metadata.save$).toEqual({ dispatch: true });
        expect(metadata.update$).toEqual({ dispatch: true });
        expect(metadata.updateFiles$).toEqual({ dispatch: true });
        expect(metadata.delete$).toEqual({ dispatch: true });
        expect(metadata.saveSuccess$).toEqual({ dispatch: false });
        expect(metadata.refreshSuccess$).toEqual({ dispatch: false });
    });
});
