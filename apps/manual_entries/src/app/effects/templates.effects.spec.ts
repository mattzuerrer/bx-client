import { inject, TestBed } from '@angular/core/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { Observable, of, throwError } from 'rxjs';

import { TemplatesActions } from '../actions';
import { templates as templatesFixture } from '../mocks/fixtures/templates.fixtures';
import { ApiServiceMockProvider } from '../mocks/services';
import { ManualEntryTemplate } from '../models';
import { ApiService } from '../services';

import { TemplatesEffects } from './templates.effects';

describe('Templates effects', () => {
    let templatesEffects: TemplatesEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<TemplatesEffects>;

    const id = 1;
    const templates = Object.assign(new ManualEntryTemplate(), templatesFixture);

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [TemplatesEffects, ApiServiceMockProvider, provideMockActions(() => actions)]
        });

        templatesEffects = TestBed.get(TemplatesEffects);
        metadata = getEffectsMetadata(templatesEffects);
    });

    it(
        'should return LOAD_SUCCESS on LOAD if fetch$ is successful (auto)',
        inject([ApiService], apiService => {
            actions = cold('');
            spyOn(apiService, 'getManualEntryTemplates').and.returnValue(of(templates));
            const expected = cold('b', { b: new TemplatesActions.LoadSuccessAction(templates) });
            expect(templatesEffects.fetch$).toBeObservable(expected);
            expect(apiService.getManualEntryTemplates).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_SUCCESS on LOAD if fetch$ is successful',
        inject([ApiService], apiService => {
            actions = cold('-a', { a: new TemplatesActions.LoadAction() });
            spyOn(apiService, 'getManualEntryTemplates').and.returnValue(of(templates));
            const expected = cold('bb', { b: new TemplatesActions.LoadSuccessAction(templates) });
            expect(templatesEffects.fetch$).toBeObservable(expected);
            expect(apiService.getManualEntryTemplates).toHaveBeenCalledTimes(2);
        })
    );

    it(
        'should return LOAD_FAIL on LOAD if fetch$ fails',
        inject([ApiService], apiService => {
            actions = cold('');
            spyOn(apiService, 'getManualEntryTemplates').and.returnValue(throwError(null));
            const expected = cold('b', { b: new TemplatesActions.LoadFailAction() });
            expect(templatesEffects.fetch$).toBeObservable(expected);
            expect(apiService.getManualEntryTemplates).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return CREATE_SUCCESS on CREATE if create$ is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new TemplatesActions.CreateAction(templates[0]) });
            spyOn(apiService, 'createManualEntryTemplate').and.returnValue(of(templates[0]));
            const expected = cold('b', { b: new TemplatesActions.CreateSuccessAction(templates[0]) });
            expect(templatesEffects.create$).toBeObservable(expected);
            expect(apiService.createManualEntryTemplate).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return CREATE_FAIL on CREATE if create$ fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new TemplatesActions.CreateAction(templates[0]) });
            spyOn(apiService, 'createManualEntryTemplate').and.returnValue(throwError(null));
            const expected = cold('b', { b: new TemplatesActions.CreateFailAction(templates[0]) });
            expect(templatesEffects.create$).toBeObservable(expected);
            expect(apiService.createManualEntryTemplate).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return UPDATE_SUCCESS on UPDATE if update$ is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new TemplatesActions.UpdateAction(templates[0]) });
            spyOn(apiService, 'updateManualEntryTemplate').and.returnValue(of(templates[0]));
            const expected = cold('b', { b: new TemplatesActions.UpdateSuccessAction(templates[0]) });
            expect(templatesEffects.update$).toBeObservable(expected);
            expect(apiService.updateManualEntryTemplate).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return UPDATE_FAIL on UPDATE if update$ fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new TemplatesActions.UpdateAction(templates[0]) });
            spyOn(apiService, 'updateManualEntryTemplate').and.returnValue(throwError(null));
            const expected = cold('b', { b: new TemplatesActions.UpdateFailAction(templates[0]) });
            expect(templatesEffects.update$).toBeObservable(expected);
            expect(apiService.updateManualEntryTemplate).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return DELETE_SUCCESS on DELETE if delete$ is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new TemplatesActions.DeleteAction(id) });
            spyOn(apiService, 'deleteManualEntryTemplate').and.returnValue(of(id));
            const expected = cold('b', { b: new TemplatesActions.DeleteSuccessAction(id) });
            expect(templatesEffects.delete$).toBeObservable(expected);
            expect(apiService.deleteManualEntryTemplate).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return DELETE_FAIL on DELETE if delete$ fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new TemplatesActions.DeleteAction(id) });
            spyOn(apiService, 'deleteManualEntryTemplate').and.returnValue(throwError(null));
            const expected = cold('b', { b: new TemplatesActions.DeleteFailAction(id) });
            expect(templatesEffects.delete$).toBeObservable(expected);
            expect(apiService.deleteManualEntryTemplate).toHaveBeenCalledTimes(1);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetch$).toEqual({ dispatch: true });
        expect(metadata.create$).toEqual({ dispatch: true });
        expect(metadata.update$).toEqual({ dispatch: true });
        expect(metadata.delete$).toEqual({ dispatch: true });
    });
});
