import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap, startWith, switchMap, tap } from 'rxjs/operators';

import { FilesActions } from '../actions';
import { ApiService, StateService } from '../services';

@Injectable()
export class FilesEffects {
    @Effect()
    fetch$: Observable<any> = this.actions$.pipe(
        ofType<FilesActions.LoadAction>(FilesActions.LOAD),
        startWith(new FilesActions.LoadAction()),
        switchMap(() =>
            this.apiService
                .getFiles()
                .pipe(map(files => new FilesActions.LoadSuccessAction(files)), catchError(() => of(new FilesActions.LoadFailAction())))
        )
    );

    @Effect()
    fetchFile$: Observable<any> = this.actions$.pipe(
        ofType<FilesActions.LoadFileAction>(FilesActions.LOAD_FILE),
        map((action: FilesActions.LoadFileAction) => action.payload),
        mergeMap(id =>
            this.apiService
                .getFile(id)
                .pipe(
                    map(file => new FilesActions.LoadFileSuccessAction(file)),
                    catchError(() => of(new FilesActions.LoadFileFailAction(id)))
                )
        )
    );

    @Effect()
    connect$: Observable<any> = this.actions$.pipe(
        ofType<FilesActions.ConnectFilesAction>(FilesActions.CONNECT),
        map((action: FilesActions.ConnectFilesAction) => action.payload),
        mergeMap(payload =>
            this.apiService
                .connectFile(payload)
                .pipe(
                    tap(file => this.stateService.dispatch(new FilesActions.RemoveInboxFilesAction(file))),
                    map(file => new FilesActions.ConnectFilesSuccessAction(Object.assign({}, payload, { file }))),
                    catchError(() => of(new FilesActions.ConnectFilesFailAction(payload)))
                )
        )
    );

    @Effect()
    disconnect$: Observable<any> = this.actions$.pipe(
        ofType<FilesActions.DisconnectFilesAction>(FilesActions.DISCONNECT),
        map((action: FilesActions.DisconnectFilesAction) => action.payload),
        mergeMap(payload =>
            this.apiService
                .disconnectFile(payload)
                .pipe(
                    map(() => new FilesActions.DisconnectFilesSuccessAction(payload)),
                    catchError(() => of(new FilesActions.DisconnectFilesFailAction(payload)))
                )
        )
    );

    constructor(private actions$: Actions, private apiService: ApiService, private stateService: StateService) {}
}
