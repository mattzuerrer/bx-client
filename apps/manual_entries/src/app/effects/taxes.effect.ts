import { Injectable } from '@angular/core';
import { byId, identity, isActive, or } from '@bx-client/common';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { EntryCollectionActions, ListActions, TaxesActions, TemplatesActions } from '../actions';
import { ManualEntry, Tax } from '../models';
import { ApiService, StateService } from '../services';

// tslint:disable-next-line:no-var-requires memoizee is a CommonJS module and cannot be imported using ES6 syntax
const memoize: any = require('memoizee');

const dateToIso = (date: Date): string => date.toISOString().split('T')[0];

export const today = () => new Date(dateToIso(new Date()));

let cachedTaxes: any;

const loadTaxes$ = (observable$: Observable<{ date: Date; entries: ManualEntry[] }>): Observable<any> =>
    observable$.pipe(
        map(({ date, entries }) => ({
            date: dateToIso(date),
            taxIds: entries.map(entry => entry.taxId).filter(identity)
        })),
        switchMap(({ date, taxIds }) =>
            cachedTaxes(date).pipe(
                map((taxes: Tax[]) => taxes.filter(or(isActive, byId(...taxIds)))),
                map((taxes: Tax[]) => new TaxesActions.LoadSuccessAction(taxes)),
                catchError(() =>
                    of(new Date(date)).pipe(tap(payload => cachedTaxes.clear()), map(payload => new TaxesActions.LoadFailAction(payload)))
                )
            )
        )
    );

@Injectable()
export class TaxesEffects {
    @Effect()
    updateDate$: Observable<any> = this.actions$.ofType(EntryCollectionActions.UPDATE_DATE, TaxesActions.LOAD).pipe(
        map((action: EntryCollectionActions.UpdateDateAction | TaxesActions.LoadAction) => action.payload),
        withLatestFrom(this.stateService.manualEntryCollection$, (date, collection) => ({
            date,
            entries: collection.entries
        })),
        loadTaxes$
    );

    @Effect()
    selectManualEntry$: Observable<any> = this.actions$.pipe(
        ofType<ListActions.SelectAction>(ListActions.SELECT),
        map((action: ListActions.SelectAction) => action.payload),
        map(collection => ({ date: collection.date, entries: collection.entries })),
        loadTaxes$
    );

    @Effect()
    reset$: Observable<any> = this.actions$
        .ofType(EntryCollectionActions.RESET, TemplatesActions.SELECT_TEMPLATE)
        .pipe(map(() => ({ date: today(), entries: [] })), loadTaxes$);

    constructor(private actions$: Actions, private apiService: ApiService, private stateService: StateService) {
        cachedTaxes = memoize(this.apiService.getTaxes.bind(this.apiService));
    }
}
