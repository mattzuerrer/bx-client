import { inject, TestBed } from '@angular/core/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { Observable, of, throwError } from 'rxjs';

import { ListActions } from '../actions';
import { collection } from '../mocks/fixtures/entry-collection.fixtures';
import { ApiServiceMockProvider, ScrollServiceMockProvider, StateServiceMockProvider } from '../mocks/services';
import { ColumnStatus, ManualEntryCollection } from '../models';
import { ApiService, ScrollService, StateService } from '../services';

import { ListEffects } from './list.effects';

describe('List effects', () => {
    let listEffects: ListEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<ListEffects>;
    let stateService: StateService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                ListEffects,
                ApiServiceMockProvider,
                ScrollServiceMockProvider,
                StateServiceMockProvider,
                provideMockActions(() => actions)
            ]
        });

        stateService = TestBed.get(StateService);
        stateService.columnConfig$ = of([new ColumnStatus('test1'), new ColumnStatus('test2')]);
        stateService.manualEntryCollection$ = of(new ManualEntryCollection());
        listEffects = TestBed.get(ListEffects);
        metadata = getEffectsMetadata(listEffects);
    });

    it(
        'should return LOAD_SUCCESS on LOAD if fetch$ is successful (auto)',
        inject([ApiService], apiService => {
            actions = cold('');
            spyOn(apiService, 'getManualEntryCollections').and.returnValue(of([collection]));
            const expected = cold('b', { b: new ListActions.LoadSuccessAction([collection]) });
            expect(listEffects.fetch$).toBeObservable(expected);
            expect(apiService.getManualEntryCollections).toHaveBeenCalledWith('test1_desc,test2_desc');
            expect(apiService.getManualEntryCollections).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_SUCCESS on LOAD if fetch$ is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new ListActions.LoadAction() });
            spyOn(apiService, 'getManualEntryCollections').and.returnValue(of([collection]));
            const expected = cold('(bb)', { b: new ListActions.LoadSuccessAction([collection]) });
            expect(listEffects.fetch$).toBeObservable(expected);
            expect(apiService.getManualEntryCollections).toHaveBeenCalledWith('test1_desc,test2_desc');
            expect(apiService.getManualEntryCollections).toHaveBeenCalledTimes(2);
        })
    );

    it(
        'should return LOAD_SUCCESS on SORT if fetch$ is successful',
        inject([ApiService], apiService => {
            actions = cold('a', {
                a: new ListActions.SortAction([new ColumnStatus('test1'), new ColumnStatus('test2')])
            });
            spyOn(apiService, 'getManualEntryCollections').and.returnValue(of([collection]));
            const expected = cold('(bb)', { b: new ListActions.LoadSuccessAction([collection]) });
            expect(listEffects.fetch$).toBeObservable(expected);
            expect(apiService.getManualEntryCollections).toHaveBeenCalledWith('test1_desc,test2_desc');
            expect(apiService.getManualEntryCollections).toHaveBeenCalledTimes(2);
        })
    );

    it(
        'should return LOAD_FAIL on LOAD if fetch$ is successful (auto)',
        inject([ApiService], apiService => {
            actions = cold('');
            spyOn(apiService, 'getManualEntryCollections').and.returnValue(throwError(null));
            const expected = cold('b', { b: new ListActions.LoadFailAction([]) });
            expect(listEffects.fetch$).toBeObservable(expected);
            expect(apiService.getManualEntryCollections).toHaveBeenCalledWith('test1_desc,test2_desc');
            expect(apiService.getManualEntryCollections).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should scroll to top on SELECT',
        inject([ScrollService], scrollService => {
            actions = cold('a', { a: new ListActions.SelectAction(collection) });
            spyOn(scrollService, 'scrollToTop').and.callThrough();
            const expected = cold('b', { b: new ListActions.SelectAction(collection) });
            expect(listEffects.scroll$).toBeObservable(expected);
            expect(scrollService.scrollToTop).toHaveBeenCalledTimes(1);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetch$).toEqual({ dispatch: true });
        expect(metadata.scroll$).toEqual({ dispatch: false });
    });
});
