import { filter, map, mergeMap, tap } from 'rxjs/operators';

import { Inject, Injectable } from '@angular/core';
import { identity } from '@bx-client/common';
import { BxLocalStorageService, localStorageConfigToken } from '@bx-client/local-storage';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';

import { AppActions, AutofillActions } from '../actions';
import { Dictionary } from '../models';
import { StateService } from '../services';

@Injectable()
export class AutofillEffects {
    @Effect({ dispatch: false })
    add$: Observable<any> = this.actions$.pipe(
        ofType<AutofillActions.AddAutofillAction>(AutofillActions.ADD),
        mergeMap(() => this.stateService.autofillState$),
        tap(autofillState => this.localStorageService.set(this.localStorageConfig.keys.autofillState, autofillState, false))
    );

    @Effect()
    init$: Observable<any> = this.actions$.pipe(
        ofType<AppActions.InitAppAction>(AppActions.INIT),
        map(() => this.localStorageService.get(this.localStorageConfig.keys.autofillState, false)),
        filter(identity),
        map((autofillState: Dictionary<string[]>) => new AutofillActions.InitAutofillAction(autofillState))
    );

    constructor(
        private actions$: Actions,
        private stateService: StateService,
        private localStorageService: BxLocalStorageService,
        @Inject(localStorageConfigToken) private localStorageConfig: any
    ) {}
}
