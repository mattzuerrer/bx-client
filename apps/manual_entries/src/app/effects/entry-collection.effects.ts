import { Injectable } from '@angular/core';
import { byId } from '@bx-client/common';
import { AccountActions } from '@bx-client/features/account';
import { RouterActions } from '@bx-client/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { EntryCollectionActions, FilesActions, TemplatesActions } from '../actions';
import { ApiService, StateService } from '../services';

const assignTax = (collection, taxes) =>
    Object.assign(collection, {
        entries: collection.entries.map(entry => Object.assign(entry, { tax: taxes.find(byId(entry.taxId)) }))
    });

@Injectable()
export class EntryCollectionEffects {
    @Effect()
    fetch$: Observable<any> = this.actions$
        .ofType(EntryCollectionActions.RESET, EntryCollectionActions.LOAD_REFERENCE_NR, TemplatesActions.SELECT_TEMPLATE)
        .pipe(
            switchMap(() =>
                this.apiService
                    .getNextRefNumber()
                    .pipe(
                        map(referenceNumber => referenceNumber.nextRefNr),
                        map(nextRefNr => new EntryCollectionActions.LoadReferenceNrSuccessAction(nextRefNr)),
                        catchError(() => of(new EntryCollectionActions.LoadReferenceNrFailAction()))
                    )
            )
        );

    @Effect()
    save$: Observable<any> = this.actions$.pipe(
        ofType<EntryCollectionActions.SaveEntryCollectionAction>(EntryCollectionActions.SAVE_ENTRY_COLLECTION),
        map((action: EntryCollectionActions.SaveEntryCollectionAction) => action.payload),
        switchMap(payload =>
            this.apiService.createCollection(payload).pipe(
                withLatestFrom(this.stateService.taxes$, assignTax),
                tap(collection => {
                    collection.entries.forEach((entry, index) => {
                        payload.entries[index].files.forEach(file =>
                            this.stateService.dispatch(
                                new FilesActions.ConnectFilesAction({
                                    collectionId: collection.id,
                                    entryId: collection.isCompoundEntry ? null : entry.id,
                                    fileId: file.id
                                })
                            )
                        );
                    });
                }),
                map(collection => new EntryCollectionActions.SaveEntryCollectionSuccessAction(collection)),
                catchError(() => of(new EntryCollectionActions.SaveEntryCollectionFailAction(payload)))
            )
        )
    );

    @Effect()
    update$: Observable<any> = this.actions$.pipe(
        ofType<EntryCollectionActions.UpdateEntryCollectionAction>(EntryCollectionActions.UPDATE_ENTRY_COLLECTION),
        map((action: EntryCollectionActions.UpdateEntryCollectionAction) => action.payload),
        switchMap(payload =>
            this.apiService
                .updateCollection(payload)
                .pipe(
                    withLatestFrom(this.stateService.taxes$, assignTax),
                    tap(collection => this.stateService.dispatch(new EntryCollectionActions.UpdateEntryCollectionFilesAction(payload))),
                    map(collection => new EntryCollectionActions.UpdateEntryCollectionSuccessAction(collection)),
                    catchError(() => of(new EntryCollectionActions.UpdateEntryCollectionFailAction(payload)))
                )
        )
    );

    @Effect()
    updateFiles$: Observable<any> = this.actions$.pipe(
        ofType<EntryCollectionActions.UpdateEntryCollectionFilesAction>(EntryCollectionActions.UPDATE_ENTRY_COLLECTION_FILES),
        map((action: EntryCollectionActions.UpdateEntryCollectionFilesAction) => action.payload),
        tap(payload =>
            payload.entries.forEach(entry => {
                entry.addedFiles.forEach(file =>
                    this.stateService.dispatch(
                        new FilesActions.ConnectFilesAction({
                            collectionId: payload.id,
                            entryId: payload.isCompoundEntry ? null : entry.id,
                            fileId: file.id
                        })
                    )
                );
                entry.removedFiles.forEach(file =>
                    this.stateService.dispatch(
                        new FilesActions.DisconnectFilesAction({
                            collectionId: payload.id,
                            entryId: payload.isCompoundEntry ? null : entry.id,
                            fileId: file.id
                        })
                    )
                );
                this.stateService.dispatch(new EntryCollectionActions.UpdateEntryCollectionSuccessAction(payload));
            })
        )
    );

    @Effect()
    delete$: Observable<any> = this.actions$.pipe(
        ofType<EntryCollectionActions.DeleteEntryCollectionAction>(EntryCollectionActions.DELETE_ENTRY_COLLECTION),
        map((action: EntryCollectionActions.DeleteEntryCollectionAction) => action.payload),
        switchMap(id =>
            this.apiService
                .deleteManualEntryCollection(id)
                .pipe(
                    map(collection => new EntryCollectionActions.DeleteEntryCollectionSuccessAction(id)),
                    catchError(() => of(new EntryCollectionActions.DeleteEntryCollectionFailAction(id)))
                )
        )
    );

    @Effect({ dispatch: false })
    saveSuccess$: Observable<any> = this.actions$.pipe(
        ofType<EntryCollectionActions.SaveEntryCollectionSuccessAction>(EntryCollectionActions.SAVE_ENTRY_COLLECTION_SUCCESS),
        tap(() => this.stateService.dispatch(new EntryCollectionActions.ResetAction()))
    );

    @Effect({ dispatch: false })
    refreshSuccess$: Observable<any> = this.actions$
        .ofType(EntryCollectionActions.UPDATE_ENTRY_COLLECTION_SUCCESS, EntryCollectionActions.DELETE_ENTRY_COLLECTION_SUCCESS)
        .pipe(tap(() => this.stateService.dispatch(new RouterActions.Go({ path: [''] }))));

    @Effect()
    fetchAccountGroups$: Observable<any> = this.actions$.pipe(
        ofType<EntryCollectionActions.SaveEntryCollectionSuccessAction>(EntryCollectionActions.SAVE_ENTRY_COLLECTION_SUCCESS),
        map(() => new AccountActions.LoadBalance())
    );

    constructor(private actions$: Actions, private apiService: ApiService, private stateService: StateService) {}
}
