import { inject, TestBed } from '@angular/core/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { Observable, of, throwError } from 'rxjs';

import { EntryCollectionActions, ListActions, TaxesActions, TemplatesActions } from '../actions';
import { ApiServiceMockProvider, StateServiceMockProvider } from '../mocks/services';
import { ManualEntry, ManualEntryCollection, Tax } from '../models';
import { ApiService, StateService } from '../services';

import { TaxesEffects, today } from './taxes.effect';

describe('Taxes effect', () => {
    let taxesEffects: TaxesEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<TaxesEffects>;

    const tax1 = Object.assign(new Tax(), { id: 1 });
    const tax2 = Object.assign(new Tax(), { id: 2 });
    const entry = Object.assign(new ManualEntry(), { taxId: 1 });
    const dateString = '2017-12-01';
    const date = new Date(dateString);
    const dateNow = today()
        .toISOString()
        .split('T')[0];
    const collection = Object.assign(new ManualEntryCollection(), { entries: [entry], date });

    describe('success api', () => {
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [],
                providers: [TaxesEffects, ApiServiceMockProvider, StateServiceMockProvider, provideMockActions(() => actions)]
            });

            const stateService = TestBed.get(StateService);
            stateService.manualEntryCollection$ = of(collection);

            const apiService = TestBed.get(ApiService);
            spyOn(apiService, 'getTaxes').and.returnValue(of([tax1, tax2]));

            taxesEffects = TestBed.get(TaxesEffects);
            metadata = getEffectsMetadata(taxesEffects);
        });

        it(
            'should return LOAD_SUCCESS on LOAD',
            inject([ApiService], apiService => {
                actions = cold('a', { a: new TaxesActions.LoadAction(date) });
                const expected = cold('b', { b: new TaxesActions.LoadSuccessAction([tax1]) });
                expect(taxesEffects.updateDate$).toBeObservable(expected);
                expect(apiService.getTaxes).toHaveBeenCalledWith(dateString);
                expect(apiService.getTaxes).toHaveBeenCalledTimes(1);
            })
        );

        it(
            'should return LOAD_SUCCESS on UPDATE_DATE',
            inject([ApiService], apiService => {
                actions = cold('a', { a: new EntryCollectionActions.UpdateDateAction(date) });
                const expected = cold('b', { b: new TaxesActions.LoadSuccessAction([tax1]) });
                expect(taxesEffects.updateDate$).toBeObservable(expected);
                expect(apiService.getTaxes).toHaveBeenCalledWith(dateString);
                expect(apiService.getTaxes).toHaveBeenCalledTimes(1);
            })
        );

        it(
            'should return LOAD_SUCCESS on SELECT',
            inject([ApiService], apiService => {
                actions = cold('a', { a: new ListActions.SelectAction(collection) });
                const expected = cold('b', { b: new TaxesActions.LoadSuccessAction([tax1]) });
                expect(taxesEffects.selectManualEntry$).toBeObservable(expected);
                expect(apiService.getTaxes).toHaveBeenCalledWith(dateString);
                expect(apiService.getTaxes).toHaveBeenCalledTimes(1);
            })
        );

        it(
            'should return LOAD_SUCCESS on RESET',
            inject([ApiService], apiService => {
                actions = cold('a', { a: new EntryCollectionActions.ResetAction() });
                const expected = cold('b', { b: new TaxesActions.LoadSuccessAction([]) });
                expect(taxesEffects.reset$).toBeObservable(expected);
                expect(apiService.getTaxes).toHaveBeenCalledWith(dateNow);
                expect(apiService.getTaxes).toHaveBeenCalledTimes(1);
            })
        );

        it(
            'should return LOAD_SUCCESS on SELECT_TEMPLATE',
            inject([ApiService], apiService => {
                actions = cold('a', { a: new TemplatesActions.SelectTemplateAction(collection) });
                const expected = cold('b', { b: new TaxesActions.LoadSuccessAction([]) });
                expect(taxesEffects.reset$).toBeObservable(expected);
                expect(apiService.getTaxes).toHaveBeenCalledWith(dateNow);
                expect(apiService.getTaxes).toHaveBeenCalledTimes(1);
            })
        );

        it(
            'should memoize call to api service get taxes',
            inject([ApiService], apiService => {
                actions = cold('aa', { a: new TaxesActions.LoadAction(date) });
                const expected = cold('bb', { b: new TaxesActions.LoadSuccessAction([tax1]) });
                expect(taxesEffects.updateDate$).toBeObservable(expected);
                expect(apiService.getTaxes).toHaveBeenCalledWith(dateString);
                expect(apiService.getTaxes).toHaveBeenCalledTimes(1);
            })
        );

        it('should test metadata', () => {
            expect(metadata.updateDate$).toEqual({ dispatch: true });
            expect(metadata.selectManualEntry$).toEqual({ dispatch: true });
            expect(metadata.reset$).toEqual({ dispatch: true });
        });
    });

    describe('fail api', () => {
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [],
                providers: [TaxesEffects, ApiServiceMockProvider, StateServiceMockProvider, provideMockActions(() => actions)]
            });

            const stateService = TestBed.get(StateService);
            stateService.manualEntryCollection$ = of(collection);

            const apiService = TestBed.get(ApiService);
            spyOn(apiService, 'getTaxes').and.returnValue(throwError('api error'));

            taxesEffects = TestBed.get(TaxesEffects);
        });

        it(
            'should return LOAD_FAIL on LOAD on getTaxes error',
            inject([ApiService], apiService => {
                actions = cold('a', { a: new TaxesActions.LoadAction(date) });
                const expected = cold('b', { b: new TaxesActions.LoadFailAction(date) });
                expect(taxesEffects.updateDate$).toBeObservable(expected);
                expect(apiService.getTaxes).toHaveBeenCalledWith(dateString);
                expect(apiService.getTaxes).toHaveBeenCalledTimes(1);
            })
        );
    });
});
