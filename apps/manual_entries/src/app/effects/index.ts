import { AutofillEffects } from './autofill.effects';
import { EntryCollectionEffects } from './entry-collection.effects';
import { FilesEffects } from './files.effects';
import { FormEffects } from './form.effects';
import { ListEffects } from './list.effects';
import { MessagesEffects } from './messages.effects';
import { TaxesEffects } from './taxes.effect';
import { TemplatesEffects } from './templates.effects';

export const appEffects = [
    AutofillEffects,
    EntryCollectionEffects,
    FormEffects,
    ListEffects,
    MessagesEffects,
    TaxesEffects,
    FilesEffects,
    TemplatesEffects
];
