import { inject, TestBed } from '@angular/core/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { Observable, of, throwError } from 'rxjs';

import { FilesActions } from '../actions';
import { ApiServiceMockProvider, StateServiceMockProvider } from '../mocks/services';
import { File } from '../models';
import { ApiService } from '../services';

import { FilesEffects } from './files.effects';

describe('Files effects', () => {
    let filesEffects: FilesEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<FilesEffects>;

    const file = new File();
    const fileConnection = { collectionId: 1, entryId: 2, fileId: 3 };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [FilesEffects, ApiServiceMockProvider, StateServiceMockProvider, provideMockActions(() => actions)]
        });

        filesEffects = TestBed.get(FilesEffects);
        metadata = getEffectsMetadata(filesEffects);
    });

    it(
        'should return LOAD_SUCCESS if SELECT is successful (auto)',
        inject([ApiService], apiService => {
            actions = cold('');
            spyOn(apiService, 'getFiles').and.returnValue(of([file]));
            const expected = cold('b', { b: new FilesActions.LoadSuccessAction([file]) });
            expect(filesEffects.fetch$).toBeObservable(expected);
            expect(apiService.getFiles).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_SUCCESS if SELECT is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new FilesActions.LoadAction() });
            spyOn(apiService, 'getFiles').and.returnValue(of([file]));
            const expected = cold('(bb)', { b: new FilesActions.LoadSuccessAction([file]) });
            expect(filesEffects.fetch$).toBeObservable(expected);
            expect(apiService.getFiles).toHaveBeenCalledTimes(2);
        })
    );

    it(
        'should return LOAD_SUCCESS if SELECT fails (auto)',
        inject([ApiService], apiService => {
            actions = cold('');
            spyOn(apiService, 'getFiles').and.returnValue(throwError(null));
            const expected = cold('b', { b: new FilesActions.LoadFailAction() });
            expect(filesEffects.fetch$).toBeObservable(expected);
            expect(apiService.getFiles).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_FILE_SUCCESS if LOAD_FILE is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new FilesActions.LoadFileAction(1) });
            spyOn(apiService, 'getFile').and.returnValue(of(file));
            const expected = cold('b', { b: new FilesActions.LoadFileSuccessAction(file) });
            expect(filesEffects.fetchFile$).toBeObservable(expected);
            expect(apiService.getFile).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_FILE_SUCCESS if LOAD_FILE fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new FilesActions.LoadFileAction(1) });
            spyOn(apiService, 'getFile').and.returnValue(throwError(null));
            const expected = cold('b', { b: new FilesActions.LoadFileFailAction(1) });
            expect(filesEffects.fetchFile$).toBeObservable(expected);
            expect(apiService.getFile).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return CONNECT_SUCCESS if CONNECT is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new FilesActions.ConnectFilesAction(fileConnection) });
            spyOn(apiService, 'connectFile').and.returnValue(of(file));
            const expected = cold('b', {
                b: new FilesActions.ConnectFilesSuccessAction(Object.assign({}, fileConnection, { file }))
            });
            expect(filesEffects.connect$).toBeObservable(expected);
            expect(apiService.connectFile).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return CONNECT_FAIL if CONNECT fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new FilesActions.ConnectFilesAction(fileConnection) });
            spyOn(apiService, 'connectFile').and.returnValue(throwError(null));
            const expected = cold('b', { b: new FilesActions.ConnectFilesFailAction(fileConnection) });
            expect(filesEffects.connect$).toBeObservable(expected);
            expect(apiService.connectFile).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return DISCONNECT_SUCCESS if DISCONNECT is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new FilesActions.DisconnectFilesAction(fileConnection) });
            spyOn(apiService, 'disconnectFile').and.returnValue(of(fileConnection));
            const expected = cold('b', { b: new FilesActions.DisconnectFilesSuccessAction(fileConnection) });
            expect(filesEffects.disconnect$).toBeObservable(expected);
            expect(apiService.disconnectFile).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return DISCONNECT_FAIL if DISCONNECT fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new FilesActions.DisconnectFilesAction(fileConnection) });
            spyOn(apiService, 'disconnectFile').and.returnValue(throwError(null));
            const expected = cold('b', { b: new FilesActions.DisconnectFilesFailAction(fileConnection) });
            expect(filesEffects.disconnect$).toBeObservable(expected);
            expect(apiService.disconnectFile).toHaveBeenCalledTimes(1);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetch$).toEqual({ dispatch: true });
        expect(metadata.fetchFile$).toEqual({ dispatch: true });
        expect(metadata.connect$).toEqual({ dispatch: true });
        expect(metadata.disconnect$).toEqual({ dispatch: true });
    });
});
