import { inject, TestBed } from '@angular/core/testing';
import { BxLocalStorageService, localStorageConfigToken } from '@bx-client/local-storage';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { noop, Observable, of } from 'rxjs';

import { AppActions, AutofillActions } from '../actions';
import { LocalStorageConfig } from '../config/local-storage-config';
import { ApiServiceMockProvider, StateServiceMockProvider } from '../mocks/services';
import { StateService } from '../services';

import { AutofillEffects } from './autofill.effects';

const localStorageStub = {
    set: () => noop(),
    get: () => noop()
};

const localStorageProvider = {
    provide: BxLocalStorageService,
    useValue: localStorageStub
};

describe('Autofill effects', () => {
    let autofillEffects: AutofillEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<AutofillEffects>;
    let stateService: StateService;

    const dictionary = { reference: ['r1', 'r2'], description: ['d1', 'd2'] };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                AutofillEffects,
                ApiServiceMockProvider,
                StateServiceMockProvider,
                localStorageProvider,
                {
                    provide: localStorageConfigToken,
                    useClass: LocalStorageConfig
                },
                provideMockActions(() => actions)
            ]
        });

        stateService = TestBed.get(StateService);
        stateService.autofillState$ = of(dictionary);
        autofillEffects = TestBed.get(AutofillEffects);
        metadata = getEffectsMetadata(autofillEffects);
    });

    it(
        'should set localStorage if ADD is successful',
        inject([BxLocalStorageService], localStorageService => {
            actions = cold('a', { a: new AutofillActions.AddAutofillAction(dictionary) });
            spyOn(localStorageService, 'set').and.callThrough();
            const expected = cold('b', { b: dictionary });
            expect(autofillEffects.add$).toBeObservable(expected);
            expect(localStorageService.set).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return INIT if INIT (appActions) is successful',
        inject([BxLocalStorageService], localStorageService => {
            actions = cold('a', { a: new AppActions.InitAppAction() });
            spyOn(localStorageService, 'get').and.returnValue(dictionary);
            const expected = cold('b', { b: new AutofillActions.InitAutofillAction(dictionary) });
            expect(autofillEffects.init$).toBeObservable(expected);
            expect(localStorageService.get).toHaveBeenCalledTimes(1);
        })
    );

    it('should test metadata', () => {
        expect(metadata.add$).toEqual({ dispatch: false });
        expect(metadata.init$).toEqual({ dispatch: true });
    });
});
