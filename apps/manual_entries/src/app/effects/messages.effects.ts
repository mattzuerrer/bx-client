import { Injectable } from '@angular/core';
import { AccountActions } from '@bx-client/features/account';
import { CompanyActions } from '@bx-client/features/company';
import { CurrencyActions } from '@bx-client/features/currency';
import { MessageActions } from '@bx-client/message';
import { RouterActions } from '@bx-client/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { EntryCollectionActions, ListActions, TaxesActions, TemplatesActions } from '../actions';

@Injectable()
export class MessagesEffects {
    @Effect({ dispatch: true })
    accountsLoadFail$: Observable<any> = this.actions$.pipe(
        ofType<AccountActions.LoadFailAction>(AccountActions.LOAD_FAIL),
        map(
            () =>
                new MessageActions.ErrorMessage({
                    messageKey: 'error.server.fetch_accounts',
                    titleKey: 'globals.try_again',
                    options: { data: { action: new AccountActions.LoadAction() } }
                })
        )
    );

    @Effect({ dispatch: true })
    currenciesLoadFail$: Observable<any> = this.actions$.pipe(
        ofType<CurrencyActions.LoadFailAction>(CurrencyActions.LOAD_FAIL),
        map(
            () =>
                new MessageActions.ErrorMessage({
                    messageKey: 'error.server.fetch_currencies',
                    titleKey: 'globals.try_again',
                    options: { data: { action: new CurrencyActions.LoadAction() } }
                })
        )
    );

    @Effect({ dispatch: true })
    taxesLoadFail$: Observable<any> = this.actions$.pipe(
        ofType<TaxesActions.LoadFailAction>(TaxesActions.LOAD_FAIL),
        map(
            (action: TaxesActions.LoadFailAction) =>
                new MessageActions.ErrorMessage({
                    messageKey: 'error.server.fetch_taxes',
                    titleKey: 'globals.try_again',
                    options: { data: { action: new TaxesActions.LoadAction(action.payload) } }
                })
        )
    );

    @Effect({ dispatch: true })
    listLoadFail$: Observable<any> = this.actions$.pipe(
        ofType<ListActions.LoadFailAction>(ListActions.LOAD_FAIL),
        map(
            () =>
                new MessageActions.ErrorMessage({
                    messageKey: 'error.server.fetch_list',
                    titleKey: 'globals.try_again',
                    options: { data: { action: new ListActions.LoadAction() } }
                })
        )
    );

    @Effect({ dispatch: true })
    userLoadFail$: Observable<any> = this.actions$.pipe(
        ofType<CompanyActions.LoadFailAction>(CompanyActions.LOAD_FAIL),
        map(
            () =>
                new MessageActions.ErrorMessage({
                    messageKey: 'error.server.fetch_user',
                    titleKey: 'globals.try_again',
                    options: { data: { action: new CompanyActions.LoadAction() } }
                })
        )
    );

    @Effect({ dispatch: true })
    referenceNrLoadFail$: Observable<any> = this.actions$.pipe(
        ofType<EntryCollectionActions.LoadReferenceNrFailAction>(EntryCollectionActions.LOAD_REFERENCE_NR_FAIL),
        map(
            () =>
                new MessageActions.ErrorMessage({
                    messageKey: 'error.server.fetch_reference_nr',
                    titleKey: 'globals.try_again',
                    options: { data: { action: new EntryCollectionActions.LoadReferenceNrAction() } }
                })
        )
    );

    @Effect({ dispatch: true })
    manualEntryLoadFail$: Observable<any> = this.actions$.pipe(
        ofType<EntryCollectionActions.LoadEntryCollectionFailAction>(EntryCollectionActions.LOAD_ENTRY_COLLECTION_FAIL),
        map(
            (action: EntryCollectionActions.LoadEntryCollectionFailAction) =>
                new MessageActions.ErrorMessage({
                    messageKey: 'error.server.fetch_manual_entry',
                    titleKey: 'globals.try_again',
                    options: { data: { action: new RouterActions.Go({ path: ['/id/', action.payload] }) } }
                })
        )
    );

    @Effect({ dispatch: true })
    manualEntrySaveFail$: Observable<any> = this.actions$.pipe(
        ofType<EntryCollectionActions.SaveEntryCollectionFailAction>(EntryCollectionActions.SAVE_ENTRY_COLLECTION_FAIL),
        map(
            (action: EntryCollectionActions.SaveEntryCollectionFailAction) =>
                new MessageActions.ErrorMessage({
                    messageKey: 'error.server.save_manual_entry',
                    titleKey: 'globals.try_again',
                    options: { data: { action: new EntryCollectionActions.SaveEntryCollectionAction(action.payload) } }
                })
        )
    );

    @Effect({ dispatch: true })
    manualEntryUpdateFail$: Observable<any> = this.actions$.pipe(
        ofType<EntryCollectionActions.UpdateEntryCollectionFailAction>(EntryCollectionActions.UPDATE_ENTRY_COLLECTION_FAIL),
        map(
            (action: EntryCollectionActions.UpdateEntryCollectionFailAction) =>
                new MessageActions.ErrorMessage({
                    messageKey: 'error.server.update_manual_entry',
                    titleKey: 'globals.try_again',
                    options: {
                        data: { action: new EntryCollectionActions.UpdateEntryCollectionAction(action.payload) }
                    }
                })
        )
    );

    @Effect({ dispatch: true })
    manualEntryDeleteSuccess$: Observable<any> = this.actions$.pipe(
        ofType<EntryCollectionActions.DeleteEntryCollectionSuccessAction>(EntryCollectionActions.DELETE_ENTRY_COLLECTION_SUCCESS),
        map(
            () =>
                new MessageActions.SuccessMessage({
                    messageKey: 'success.server.delete_manual_entry'
                })
        )
    );

    @Effect({ dispatch: true })
    manualEntryDeleteFail$: Observable<any> = this.actions$.pipe(
        ofType<EntryCollectionActions.DeleteEntryCollectionFailAction>(EntryCollectionActions.DELETE_ENTRY_COLLECTION_FAIL),
        map(
            (action: EntryCollectionActions.DeleteEntryCollectionFailAction) =>
                new MessageActions.ErrorMessage({
                    messageKey: 'error.server.delete_manual_entry',
                    titleKey: 'globals.try_again',
                    options: {
                        data: { action: new EntryCollectionActions.DeleteEntryCollectionAction(action.payload) }
                    }
                })
        )
    );

    @Effect({ dispatch: true })
    manualEntryTemplateCreateSuccess$: Observable<any> = this.actions$.pipe(
        ofType<TemplatesActions.CreateSuccessAction>(TemplatesActions.CREATE_SUCCESS),
        map(
            () =>
                new MessageActions.SuccessMessage({
                    messageKey: 'globals.template.save.message_success',
                    options: { enableHtml: true }
                })
        )
    );

    @Effect({ dispatch: true })
    manualEntryTemplateCreateFail$: Observable<any> = this.actions$.pipe(
        ofType<TemplatesActions.CreateFailAction>(TemplatesActions.CREATE_FAIL),
        map(
            (action: TemplatesActions.CreateFailAction) =>
                new MessageActions.ErrorMessage({
                    messageKey: 'error.server.save_manual_entry_template',
                    titleKey: 'globals.try_again',
                    options: { data: { action: new TemplatesActions.CreateAction(action.payload) } }
                })
        )
    );

    @Effect({ dispatch: true })
    manualEntryTemplateUpdateSuccess$: Observable<any> = this.actions$.pipe(
        ofType<TemplatesActions.UpdateSuccessAction>(TemplatesActions.UPDATE_SUCCESS),
        map(
            () =>
                new MessageActions.SuccessMessage({
                    messageKey: 'globals.template.save.message_success',
                    options: { enableHtml: true }
                })
        )
    );

    @Effect({ dispatch: true })
    manualEntryTemplateUpdateFail$: Observable<any> = this.actions$.pipe(
        ofType<TemplatesActions.UpdateFailAction>(TemplatesActions.UPDATE_FAIL),
        map(
            (action: TemplatesActions.UpdateFailAction) =>
                new MessageActions.ErrorMessage({
                    messageKey: 'error.server.save_manual_entry_template',
                    titleKey: 'globals.try_again',
                    options: { data: { action: new TemplatesActions.UpdateAction(action.payload) } }
                })
        )
    );

    @Effect({ dispatch: true })
    manualEntryTemplateDeleteSuccess$: Observable<any> = this.actions$.pipe(
        ofType<TemplatesActions.DeleteSuccessAction>(TemplatesActions.DELETE_SUCCESS),
        map(
            () =>
                new MessageActions.SuccessMessage({
                    messageKey: 'globals.template.delete.message_success',
                    options: { enableHtml: true }
                })
        )
    );

    @Effect({ dispatch: true })
    manualEntryTemplateDeleteFail$: Observable<any> = this.actions$.pipe(
        ofType<TemplatesActions.DeleteFailAction>(TemplatesActions.DELETE_FAIL),
        map(
            (action: TemplatesActions.DeleteFailAction) =>
                new MessageActions.ErrorMessage({
                    messageKey: 'error.server.delete_manual_entry_template',
                    titleKey: 'globals.try_again',
                    options: { data: { action: new TemplatesActions.DeleteAction(action.payload) } }
                })
        )
    );

    constructor(private actions$: Actions) {}
}
