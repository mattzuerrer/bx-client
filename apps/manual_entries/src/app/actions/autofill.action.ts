import { Action } from '@ngrx/store';

import { Dictionary } from '../models';

export namespace AutofillActions {
    export const INIT = '[Autofill] Init';
    export const ADD = '[Autofill] Add';

    export class InitAutofillAction implements Action {
        readonly type = INIT;

        constructor(public payload: Dictionary<string[]>) {}
    }

    export class AddAutofillAction implements Action {
        readonly type = ADD;

        constructor(public payload: Dictionary<string[]>) {}
    }

    export type Actions = InitAutofillAction | AddAutofillAction;
}
