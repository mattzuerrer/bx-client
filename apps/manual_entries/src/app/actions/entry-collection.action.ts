import { Action } from '@ngrx/store';

import { ManualEntryCollection } from '../models';

export namespace EntryCollectionActions {
    export const RESET = '[EntryCollection] Reset';
    export const CANCEL = '[EntryCollection] Cancel';
    export const LOAD_ENTRY_COLLECTION = '[EntryCollection] Load entry';
    export const LOAD_ENTRY_COLLECTION_SUCCESS = '[EntryCollection] Load entry success';
    export const LOAD_ENTRY_COLLECTION_FAIL = '[EntryCollection] Loading entry fail';
    export const LOAD_REFERENCE_NR = '[EntryCollection] Load Ref Number';
    export const LOAD_REFERENCE_NR_SUCCESS = '[EntryCollection] Load Reference Number Success';
    export const LOAD_REFERENCE_NR_FAIL = '[EntryCollection] Load Reference Number Fail';
    export const UPDATE_DATE = '[EntryCollection] Update Date';
    export const SAVE_ENTRY_COLLECTION = '[EntryCollection] Add new entry';
    export const SAVE_ENTRY_COLLECTION_SUCCESS = '[EntryCollection] Add new entry success';
    export const SAVE_ENTRY_COLLECTION_FAIL = '[EntryCollection] Add new entry fail';
    export const UPDATE_ENTRY_COLLECTION = '[EntryCollection] Update entry';
    export const UPDATE_ENTRY_COLLECTION_FILES = '[EntryCollection] Update entry files';
    export const UPDATE_ENTRY_COLLECTION_SUCCESS = '[EntryCollection] Update entry success';
    export const UPDATE_ENTRY_COLLECTION_FAIL = '[EntryCollection] Update new entry fail';
    export const DELETE_ENTRY_COLLECTION = '[EntryCollection] Delete entry';
    export const DELETE_ENTRY_COLLECTION_SUCCESS = '[EntryCollection] Delete entry success';
    export const DELETE_ENTRY_COLLECTION_FAIL = '[EntryCollection] Delete entry fail';

    export class ResetAction implements Action {
        readonly type = RESET;
    }

    export class CancelAction implements Action {
        readonly type = CANCEL;
    }

    export class LoadEntryCollectionAction implements Action {
        readonly type = LOAD_ENTRY_COLLECTION;
    }

    export class LoadEntryCollectionSuccessAction implements Action {
        readonly type = LOAD_ENTRY_COLLECTION_SUCCESS;
    }

    export class LoadEntryCollectionFailAction implements Action {
        readonly type = LOAD_ENTRY_COLLECTION_FAIL;

        constructor(public payload: number) {}
    }

    export class LoadReferenceNrAction implements Action {
        readonly type = LOAD_REFERENCE_NR;
    }

    export class LoadReferenceNrSuccessAction implements Action {
        readonly type = LOAD_REFERENCE_NR_SUCCESS;

        constructor(public payload: string) {}
    }

    export class LoadReferenceNrFailAction implements Action {
        readonly type = LOAD_REFERENCE_NR_FAIL;
    }

    export class UpdateDateAction implements Action {
        readonly type = UPDATE_DATE;

        constructor(public payload: Date) {}
    }

    export class SaveEntryCollectionAction implements Action {
        readonly type = SAVE_ENTRY_COLLECTION;

        constructor(public payload: ManualEntryCollection) {}
    }

    export class SaveEntryCollectionSuccessAction implements Action {
        readonly type = SAVE_ENTRY_COLLECTION_SUCCESS;

        constructor(public payload: ManualEntryCollection) {}
    }

    export class SaveEntryCollectionFailAction implements Action {
        readonly type = SAVE_ENTRY_COLLECTION_FAIL;

        constructor(public payload: ManualEntryCollection) {}
    }

    export class UpdateEntryCollectionAction implements Action {
        readonly type = UPDATE_ENTRY_COLLECTION;

        constructor(public payload: ManualEntryCollection) {}
    }

    export class UpdateEntryCollectionFilesAction implements Action {
        readonly type = UPDATE_ENTRY_COLLECTION_FILES;

        constructor(public payload: ManualEntryCollection) {}
    }

    export class UpdateEntryCollectionSuccessAction implements Action {
        readonly type = UPDATE_ENTRY_COLLECTION_SUCCESS;

        constructor(public payload: ManualEntryCollection) {}
    }

    export class UpdateEntryCollectionFailAction implements Action {
        readonly type = UPDATE_ENTRY_COLLECTION_FAIL;

        constructor(public payload: ManualEntryCollection) {}
    }

    export class DeleteEntryCollectionAction implements Action {
        readonly type = DELETE_ENTRY_COLLECTION;

        constructor(public payload: number) {}
    }

    export class DeleteEntryCollectionSuccessAction implements Action {
        readonly type = DELETE_ENTRY_COLLECTION_SUCCESS;

        constructor(public payload: number) {}
    }

    export class DeleteEntryCollectionFailAction implements Action {
        readonly type = DELETE_ENTRY_COLLECTION_FAIL;

        constructor(public payload: number) {}
    }

    export type Actions =
        | ResetAction
        | CancelAction
        | LoadEntryCollectionAction
        | LoadEntryCollectionSuccessAction
        | LoadEntryCollectionFailAction
        | LoadReferenceNrAction
        | LoadReferenceNrSuccessAction
        | LoadReferenceNrFailAction
        | UpdateDateAction
        | SaveEntryCollectionAction
        | SaveEntryCollectionSuccessAction
        | SaveEntryCollectionFailAction
        | UpdateEntryCollectionAction
        | UpdateEntryCollectionSuccessAction
        | UpdateEntryCollectionFilesAction
        | UpdateEntryCollectionFailAction
        | DeleteEntryCollectionAction
        | DeleteEntryCollectionSuccessAction
        | DeleteEntryCollectionFailAction;
}
