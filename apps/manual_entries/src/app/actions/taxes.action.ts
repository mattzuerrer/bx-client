import { Action } from '@ngrx/store';

import { Tax } from '../models';

export namespace TaxesActions {
    export const LOAD = '[Taxes] Load';
    export const LOAD_SUCCESS = '[Taxes] Load Success';
    export const LOAD_FAIL = '[Taxes] Load Fail';

    export class LoadAction implements Action {
        readonly type = LOAD;

        constructor(public payload: Date) {}
    }

    export class LoadSuccessAction implements Action {
        readonly type = LOAD_SUCCESS;

        constructor(public payload: Tax[]) {}
    }

    export class LoadFailAction implements Action {
        readonly type = LOAD_FAIL;

        constructor(public payload: Date) {}
    }

    export type Actions = LoadAction | LoadSuccessAction | LoadFailAction;
}
