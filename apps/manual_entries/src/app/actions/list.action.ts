import { Action } from '@ngrx/store';

import { ColumnStatus, ManualEntryCollection } from '../models';

export namespace ListActions {
    export const LOAD = '[List] Load';
    export const LOAD_SUCCESS = '[List] Load Success';
    export const LOAD_FAIL = '[List] Load Fail';
    export const SORT = '[List] Sort';
    export const SELECT = '[List] Select';

    export class LoadAction implements Action {
        readonly type = LOAD;
    }

    export class LoadSuccessAction implements Action {
        readonly type = LOAD_SUCCESS;

        constructor(public payload: ManualEntryCollection[]) {}
    }

    export class LoadFailAction implements Action {
        readonly type = LOAD_FAIL;

        constructor(public payload: ManualEntryCollection[]) {}
    }

    export class SortAction implements Action {
        readonly type = SORT;

        constructor(public payload: ColumnStatus[]) {}
    }

    export class SelectAction implements Action {
        readonly type = SELECT;

        constructor(public payload: ManualEntryCollection) {}
    }

    export type Actions = LoadAction | LoadSuccessAction | LoadFailAction | SortAction | SelectAction;
}
