import { Action } from '@ngrx/store';

export namespace AppActions {
    export const INIT = '[App] Init';

    export class InitAppAction implements Action {
        readonly type = INIT;
    }

    export type Actions = InitAppAction;
}
