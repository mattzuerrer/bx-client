import { Action } from '@ngrx/store';

import { File } from '../models';

export namespace FileUploadDialogActions {
    export const INIT_FILE_UPLOAD_DIALOG = '[File Upload Dialog] Init file upload dialog';
    export const ADD_FILE = '[File Upload Dialog] Add file';
    export const REMOVE_FILE = '[File Upload Dialog] Remove file';

    export class InitFileUploadDialog implements Action {
        readonly type = INIT_FILE_UPLOAD_DIALOG;

        constructor(public payload: File[]) {}
    }

    export class AddFileAction implements Action {
        readonly type = ADD_FILE;

        constructor(public payload: File) {}
    }

    export class RemoveFileAction implements Action {
        readonly type = REMOVE_FILE;

        constructor(public payload: File) {}
    }

    export type Actions = InitFileUploadDialog | AddFileAction | RemoveFileAction;
}
