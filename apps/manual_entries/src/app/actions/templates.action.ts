import { Action } from '@ngrx/store';

import { ManualEntryCollection, ManualEntryTemplate } from '../models';

export namespace TemplatesActions {
    export const LOAD = '[Templates] Load';
    export const LOAD_SUCCESS = '[Templates] Load Success';
    export const LOAD_FAIL = '[Templates] Load Fail';
    export const CREATE = '[Templates] Create';
    export const CREATE_SUCCESS = '[Templates] Create Success';
    export const CREATE_FAIL = '[Templates] Create Fail';
    export const UPDATE = '[Templates] Update';
    export const UPDATE_SUCCESS = '[Templates] Update Success';
    export const UPDATE_FAIL = '[Templates] Update Fail';
    export const DELETE = '[Templates] Delete';
    export const DELETE_SUCCESS = '[Templates] Delete Success';
    export const DELETE_FAIL = '[Templates] Delete Fail';
    export const SELECT_TEMPLATE = '[Templates] Select Template';

    export class LoadAction implements Action {
        readonly type = LOAD;
    }

    export class LoadSuccessAction implements Action {
        readonly type = LOAD_SUCCESS;

        constructor(public payload: ManualEntryTemplate[]) {}
    }

    export class LoadFailAction implements Action {
        readonly type = LOAD_FAIL;
    }

    export class CreateAction implements Action {
        readonly type = CREATE;

        constructor(public payload: ManualEntryTemplate) {}
    }

    export class CreateSuccessAction implements Action {
        readonly type = CREATE_SUCCESS;

        constructor(public payload: ManualEntryTemplate) {}
    }

    export class CreateFailAction implements Action {
        readonly type = CREATE_FAIL;

        constructor(public payload: ManualEntryTemplate) {}
    }

    export class UpdateAction implements Action {
        readonly type = UPDATE;

        constructor(public payload: ManualEntryTemplate) {}
    }

    export class UpdateSuccessAction implements Action {
        readonly type = UPDATE_SUCCESS;

        constructor(public payload: ManualEntryTemplate) {}
    }

    export class UpdateFailAction implements Action {
        readonly type = UPDATE_FAIL;

        constructor(public payload: ManualEntryTemplate) {}
    }

    export class DeleteAction implements Action {
        readonly type = DELETE;

        constructor(public payload: number) {}
    }

    export class DeleteSuccessAction implements Action {
        readonly type = DELETE_SUCCESS;

        constructor(public payload: number) {}
    }

    export class DeleteFailAction implements Action {
        readonly type = DELETE_FAIL;

        constructor(public payload: number) {}
    }

    export class SelectTemplateAction implements Action {
        readonly type = SELECT_TEMPLATE;

        constructor(public payload: ManualEntryCollection) {}
    }

    export type Actions =
        | LoadAction
        | LoadSuccessAction
        | LoadFailAction
        | CreateAction
        | CreateSuccessAction
        | CreateFailAction
        | UpdateAction
        | UpdateSuccessAction
        | UpdateFailAction
        | DeleteAction
        | DeleteSuccessAction
        | DeleteFailAction
        | SelectTemplateAction;
}
