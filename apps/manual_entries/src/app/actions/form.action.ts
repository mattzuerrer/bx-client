import { Action } from '@ngrx/store';

export namespace FormActions {
    export const ActionTypes = {
        ADD_ROW: '[Form] Add Row',
        REMOVE_ROW: '[Form] Remove Row'
    };

    export class AddRowAction implements Action {
        type = ActionTypes.ADD_ROW;
    }

    export class RemoveRowAction implements Action {
        type = ActionTypes.REMOVE_ROW;

        constructor(public payload: number) {}
    }

    export type Actions = AddRowAction | RemoveRowAction;
}
