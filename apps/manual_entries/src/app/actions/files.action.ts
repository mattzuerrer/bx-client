import { Action } from '@ngrx/store';

import { File, FileConnection } from '../models';

export namespace FilesActions {
    export const LOAD = '[Files] Load';
    export const LOAD_SUCCESS = '[Files] Load Success';
    export const LOAD_FAIL = '[Files] Load Fail';
    export const LOAD_FILE = '[Files] Load File';
    export const LOAD_FILE_SUCCESS = '[Files] Load File Success';
    export const LOAD_FILE_FAIL = '[Files] Load File Fail';
    export const CONNECT = '[Files] Connect';
    export const CONNECT_SUCCESS = '[Files] Connect Success';
    export const CONNECT_FAIL = '[Files] Connect Fail';
    export const DISCONNECT = '[Files] Disconnect';
    export const DISCONNECT_SUCCESS = '[Files] Disconnect Success';
    export const DISCONNECT_FAIL = '[Files] Disconnect Fail';
    export const ADD_INBOX_FILE = '[Files] Add file to inbox';
    export const REMOVE_INBOX_FILE = '[Files] Remove file from inbox';

    export class LoadAction implements Action {
        readonly type = LOAD;
    }

    export class LoadSuccessAction implements Action {
        readonly type = LOAD_SUCCESS;

        constructor(public payload: File[]) {}
    }

    export class LoadFailAction implements Action {
        readonly type = LOAD_FAIL;
    }

    export class LoadFileAction implements Action {
        readonly type = LOAD_FILE;

        constructor(public payload: number) {}
    }

    export class LoadFileSuccessAction implements Action {
        readonly type = LOAD_FILE_SUCCESS;

        constructor(public payload: File) {}
    }

    export class LoadFileFailAction implements Action {
        readonly type = LOAD_FILE_FAIL;

        constructor(public payload: number) {}
    }

    export class ConnectFilesAction implements Action {
        readonly type = CONNECT;

        constructor(public payload: FileConnection) {}
    }

    export class ConnectFilesSuccessAction implements Action {
        readonly type = CONNECT_SUCCESS;

        constructor(public payload: FileConnection) {}
    }

    export class ConnectFilesFailAction implements Action {
        readonly type = CONNECT_FAIL;

        constructor(public payload: FileConnection) {}
    }

    export class DisconnectFilesAction implements Action {
        readonly type = DISCONNECT;

        constructor(public payload: FileConnection) {}
    }

    export class DisconnectFilesSuccessAction implements Action {
        readonly type = DISCONNECT_SUCCESS;

        constructor(public payload: FileConnection) {}
    }

    export class DisconnectFilesFailAction implements Action {
        readonly type = DISCONNECT_FAIL;

        constructor(public payload: FileConnection) {}
    }

    export class AddInboxFilesAction implements Action {
        readonly type = ADD_INBOX_FILE;

        constructor(public payload: File) {}
    }

    export class RemoveInboxFilesAction implements Action {
        readonly type = REMOVE_INBOX_FILE;

        constructor(public payload: File) {}
    }

    export type Actions =
        | LoadAction
        | LoadSuccessAction
        | LoadFailAction
        | LoadFileAction
        | LoadFileSuccessAction
        | LoadFileFailAction
        | ConnectFilesAction
        | ConnectFilesSuccessAction
        | ConnectFilesFailAction
        | DisconnectFilesAction
        | DisconnectFilesSuccessAction
        | DisconnectFilesFailAction
        | AddInboxFilesAction
        | RemoveInboxFilesAction;
}
