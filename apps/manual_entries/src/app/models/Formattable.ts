/**
 * Formattable data item. Formats the item for display to not rely on the name property.
 */
export interface Formattable {
    /**
     * Format the item display
     */
    format(): string;
}
