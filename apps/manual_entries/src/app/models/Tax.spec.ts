import { Tax } from './Tax';

describe('Tax model', () => {
    it('should return tax code on format() if code is specified', () => {
        const tax = new Tax();
        tax.displayName = 'Mat. DL 19.00%';
        tax.code = 'ZOLLM';

        expect(tax.format()).toBe('ZOLLM');
    });

    it('should return tax name on format() if code is not specified', () => {
        const tax = new Tax();
        tax.displayName = 'Mat. DL 19.00%';

        expect(tax.format()).toBe('Mat. DL 19.00%');
    });

    it('should return tax name on format() if code is null', () => {
        const tax = new Tax();
        tax.displayName = 'Mat. DL 19.00%';
        tax.code = null;

        expect(tax.format()).toBe('Mat. DL 19.00%');
    });
});
