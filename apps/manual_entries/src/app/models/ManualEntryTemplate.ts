import { Expose, Type } from 'class-transformer';

import { Sort } from '../util/sort';

import { ManualEntryCollection } from './ManualEntryCollection';
import { serializeType } from './serialization';

export class ManualEntryTemplate {
    static compareByName(a: ManualEntryTemplate, b: ManualEntryTemplate): number {
        return Sort.compareString(a.name, b.name);
    }

    @Expose() id: number;

    type: string;

    @Expose() name: string;

    createdByUserId: number;

    editedByUserId: number;

    @Expose()
    @Type(serializeType(ManualEntryCollection))
    collection: ManualEntryCollection;
}
