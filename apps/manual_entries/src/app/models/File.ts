import { User } from '@bx-client/features/user';
import { Type } from 'class-transformer';

import { Item } from './Item';
import { serializeType } from './serialization';

export class File implements Item {
    id: number;

    name: string;

    sizeInBytes: number;

    extension: string;

    mimeType: string;

    userId: number;

    createdAt: string;

    uploaderEmail: string;

    @Type(serializeType(User))
    user: User;

    uploader(): string {
        if (!this.user && this.uploaderEmail) {
            return this.uploaderEmail;
        }

        if (this.user) {
            return this.user.format();
        }

        return null;
    }

    format(): string {
        return `${this.name || ''}.${this.extension || ''}`;
    }
}
