import { File } from './File';

describe('File model', () => {
    it('should return file name with extension on format()', () => {
        const file = new File();
        file.name = 'I am a super cool file name';
        file.extension = 'jpg';

        expect(file.format()).toBe('I am a super cool file name.jpg');
    });
});
