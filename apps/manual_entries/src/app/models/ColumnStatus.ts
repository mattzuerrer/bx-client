const statusNone = 'none';
const statusAscending = 'asc';
const statusDescending = 'desc';
const statusLoading = 'loading';

export class ColumnStatus {
    static get statusNone(): string {
        return statusNone;
    }

    static get statusAscending(): string {
        return statusAscending;
    }

    static get statusDescending(): string {
        return statusDescending;
    }

    static get statusLoading(): string {
        return statusLoading;
    }

    column: string;

    status: string;

    reversed = false;

    constructor(column: string = '') {
        this.column = column;
        this.status = statusNone;
    }

    get direction(): string {
        return this.reversed ? statusAscending : statusDescending;
    }

    toString(): string {
        return `${this.column}_${this.direction}`;
    }
}
