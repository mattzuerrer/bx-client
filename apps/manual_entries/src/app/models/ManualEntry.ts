import { Account } from '@bx-client/features/account';
import { Expose, Type } from 'class-transformer';

import { Currency } from './Currency';
import { File } from './File';
import { group, serializeType } from './serialization';
import { Tax } from './Tax';

export class ManualEntry {
    @Expose({ groups: [group.manualEntryCollection] })
    id: number;

    @Expose({ groups: [group.manualEntryCollection, group.manualEntryTemplate] })
    debitAccountId: number;

    @Expose({ groups: [group.manualEntryCollection, group.manualEntryTemplate] })
    creditAccountId: number;

    @Expose({ groups: [group.manualEntryCollection, group.manualEntryTemplate] })
    taxId: number;

    @Expose({ groups: [group.manualEntryCollection, group.manualEntryTemplate] })
    description: string;

    @Expose({ groups: [group.manualEntryCollection, group.manualEntryTemplate] })
    amount: number;

    @Expose({ groups: [group.manualEntryCollection, group.manualEntryTemplate] })
    currencyId: number;

    @Expose({ groups: [group.manualEntryCollection, group.manualEntryTemplate] })
    currencyFactor: number;

    @Expose({ groups: [group.manualEntryCollection] })
    taxAccountId: number;

    @Expose({ groups: [group.manualEntryCollection, group.manualEntryTemplate] })
    date: string;

    baseCurrencyId: number;

    @Expose({ groups: [group.manualEntryCollection, group.manualEntryTemplate] })
    baseCurrencyAmount: number;

    createdByUserId: number;

    editedByUserId: number;

    @Type(serializeType(File))
    files: File[] = [];

    @Type(serializeType(File))
    addedFiles: File[] = [];

    @Type(serializeType(File))
    removedFiles: File[] = [];

    index: number;

    @Type(serializeType(Currency))
    baseCurrency: Currency;

    @Type(serializeType(Currency))
    currency: Currency;

    @Type(serializeType(Account))
    creditAccount: Account;

    @Type(serializeType(Account))
    debitAccount: Account;

    @Type(serializeType(Tax))
    tax: Tax;

    get isDifferentCurrency(): boolean {
        return this.baseCurrencyId !== this.currencyId;
    }

    get bothAccountsHaveTax(): boolean {
        return !!this.debitAccount.taxId && !!this.creditAccount.taxId;
    }
}
