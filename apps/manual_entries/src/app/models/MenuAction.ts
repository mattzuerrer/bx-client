export class MenuAction {
    constructor(readonly type: string, readonly displayName: string) {}
}
