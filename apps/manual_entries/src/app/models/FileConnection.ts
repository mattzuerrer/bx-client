import { File } from './File';

export interface FileConnection {
    collectionId: number;
    entryId: number;
    fileId: number;
    file?: File;
}
