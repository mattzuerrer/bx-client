import { Account } from '@bx-client/features/account';
import { Expose, Type } from 'class-transformer';

import { Currency } from './Currency';
import { ManualEntry } from './ManualEntry';
import { group, serializeType } from './serialization';

const singleEntryType = 'manual_single_entry';

const groupEntryType = 'manual_group_entry';

const compoundEntryType = 'manual_compound_entry';

const statusNone = '';

const statusUpdated = 'updated';

const statusSelected = 'selected';

export class ManualEntryCollection {
    static get singleEntryType(): string {
        return singleEntryType;
    }

    static get groupEntryType(): string {
        return groupEntryType;
    }

    static get compoundEntryType(): string {
        return compoundEntryType;
    }

    static get statusNone(): string {
        return statusNone;
    }

    static get statusUpdated(): string {
        return statusUpdated;
    }

    static get statusSelected(): string {
        return statusSelected;
    }

    @Expose({ groups: [group.manualEntryCollection] })
    id: number;

    @Expose({ groups: [group.manualEntryCollection, group.manualEntryTemplate] })
    type: string = singleEntryType;

    @Expose({ groups: [group.manualEntryCollection] })
    referenceNr: string;

    @Expose({ groups: [group.manualEntryCollection, group.manualEntryTemplate] })
    @Type(serializeType(ManualEntry))
    entries: ManualEntry[] = [];

    @Type(serializeType(Date))
    date: Date = new Date();

    description: string;

    createdByUserId: number;

    editedByUserId: number;

    isLocked: boolean;

    lockedInfo: string;

    files: string[] = [];

    status: string = statusNone;

    @Type(serializeType(Currency))
    currency: Currency;

    @Type(serializeType(Account))
    account: Account;

    get isSingleEntry(): boolean {
        return this.type === singleEntryType;
    }

    get isGroupEntry(): boolean {
        return this.type === groupEntryType;
    }

    get isCompoundEntry(): boolean {
        return this.type === compoundEntryType;
    }

    @Expose({ name: 'date', groups: [group.manualEntryCollection] })
    get formattedDate(): string {
        return `${this.date.getFullYear()}-${this.date.getMonth() + 1}-${this.date.getDate()}`;
    }

    @Expose({ groups: [group.manualEntryCollection, group.manualEntryTemplate] })
    get currencyId(): number {
        return this.currency ? this.currency.id : null;
    }
}
