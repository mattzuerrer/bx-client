import { Item } from './Item';

export class Tax implements Item {
    id: number;

    accountId: number;

    code: string;

    name: string;

    displayName: string;

    startYear: number;

    endYear: number;

    isActive: boolean;

    taxSetElementType: string;

    type: string;

    value: number;

    format(): string {
        return this.code || this.displayName;
    }
}
