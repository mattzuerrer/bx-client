import { Formattable } from './Formattable';

/**
 * A data item with an ID and a name
 */
export interface Item extends Formattable {
    /**
     * ID of the data item
     */
    id: number | string;

    /**
     * Name of the data item
     */
    name: string;
}
