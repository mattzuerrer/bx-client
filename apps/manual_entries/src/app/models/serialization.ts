type serializationStrategy = 'exposeAll' | 'excludeAll';
export const excludeAllStrategy: serializationStrategy = 'excludeAll';
export const exposeAllStrategy: serializationStrategy = 'exposeAll';

export const group = {
    manualEntryCollection: 'manual_entry_collection',
    manualEntryTemplate: 'manual_entry_template'
};

export function serializeType<T>(object: T): () => T {
    return (): T => object;
}
