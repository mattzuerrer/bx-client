import { byId } from '@bx-client/common';

import { EntryCollectionActions, ListActions } from '../actions';
import * as listFixtures from '../mocks/fixtures/list.fixtures';
import { ManualEntryCollection } from '../models';

import { reducer } from './list.reducer';

const initialState = {
    entries: [],
    loaded: false
};

const loadedState = {
    entries: listFixtures.listEntries,
    loaded: true
};

const byStatusNotNone = (collection: ManualEntryCollection) => collection.status !== ManualEntryCollection.statusNone;

describe('List reducer', () => {
    it('should return initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return new state on load success', () => {
        const state = initialState;
        const action = new ListActions.LoadSuccessAction(listFixtures.listEntries);
        expect(reducer(state, action).loaded).toBe(true);
        expect(reducer(state, action).entries).toBe(listFixtures.listEntries);
    });

    it('should return status with selected value on select', () => {
        const state = loadedState;
        const action = new ListActions.SelectAction(listFixtures.selectedEntry);
        expect(reducer(state, action).loaded).toBe(true);
        expect(reducer(state, action).entries.filter(byStatusNotNone)).toEqual([
            Object.assign(listFixtures.selectedEntry, { status: ManualEntryCollection.statusSelected })
        ]);
    });

    it('should add saved item with status updated to the beginning of the list array and remove status from the rest of the list', () => {
        const state = loadedState;
        const action = new EntryCollectionActions.SaveEntryCollectionSuccessAction(listFixtures.selectedEntry);
        expect(reducer(state, action).loaded).toBe(state.loaded);
        expect(reducer(state, action).entries[0]).toEqual(Object.assign(listFixtures.selectedEntry, { status: 'updated' }));
        expect(reducer(state, action).entries.filter(byStatusNotNone).length).toBe(1);
    });

    it('should update item and set status updated on that item while removing status from the rest of the list', () => {
        const state = loadedState;
        const action = new EntryCollectionActions.UpdateEntryCollectionSuccessAction(listFixtures.selectedEntry);
        expect(reducer(state, action).loaded).toBe(state.loaded);
        expect(reducer(state, action).entries.find(byId(listFixtures.selectedEntry.id))).toEqual(
            Object.assign(listFixtures.selectedEntry, { status: 'updated' })
        );
        expect(reducer(state, action).entries.filter(byStatusNotNone).length).toBe(1);
    });

    it('should return state with removed status values on cancel', () => {
        const state = loadedState;
        const action = new EntryCollectionActions.CancelAction();
        expect(reducer(state, action).loaded).toBe(state.loaded);
        expect(reducer(state, action).entries.filter(byStatusNotNone).length).toBe(0);
    });

    it('should return loaded true on load fail', () => {
        const state = initialState;
        const action = new ListActions.LoadFailAction(listFixtures.listEntries);
        expect(reducer(state, action)).toEqual({ entries: [], loaded: true });
    });
});
