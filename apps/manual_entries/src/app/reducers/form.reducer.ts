import { PayloadAction } from '@bx-client/ngrx';

import { EntryCollectionActions, ListActions } from '../actions';
import { FormState } from '../models';

export type State = FormState;

const initialState: State = Object.assign(new FormState(), { referenceNr: '', status: FormState.statusNone });

export function reducer(state: State = initialState, action: PayloadAction): State {
    switch (action.type) {
        case EntryCollectionActions.RESET:
            return initialState;
        case EntryCollectionActions.LOAD_REFERENCE_NR_SUCCESS:
            return Object.assign({}, state, { referenceNr: action.payload });
        case ListActions.SELECT:
            return Object.assign({}, state, { referenceNr: action.payload.referenceNr });
        case EntryCollectionActions.SAVE_ENTRY_COLLECTION:
        case EntryCollectionActions.UPDATE_ENTRY_COLLECTION:
            return Object.assign({}, state, { status: FormState.statusProgress });
        case EntryCollectionActions.SAVE_ENTRY_COLLECTION_SUCCESS:
        case EntryCollectionActions.UPDATE_ENTRY_COLLECTION_SUCCESS:
            return Object.assign({}, state, { status: FormState.statusNone });
        case EntryCollectionActions.SAVE_ENTRY_COLLECTION_FAIL:
        case EntryCollectionActions.UPDATE_ENTRY_COLLECTION_FAIL:
            return Object.assign({}, state, { status: FormState.statusNone });
        default:
            return state;
    }
}

export const getReferenceNr = (state: State) => state.referenceNr;

export const getStatus = (state: State) => state.status;
