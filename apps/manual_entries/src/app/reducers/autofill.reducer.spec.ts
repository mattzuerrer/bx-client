import { AutofillActions } from '../actions';

import { reducer } from './autofill.reducer';

describe('Autofill reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null, payload: null };
        expect(reducer(state, action)).toEqual({});
    });

    it('should return new state on init', () => {
        const state = { description: ['d1', 'd2', 'd3'] };
        const initPayload = { reference: ['r1', 'r2'] };
        const action = new AutofillActions.InitAutofillAction(initPayload);
        expect(reducer(state, action)).toBe(initPayload);
    });

    it('should merge existing state on add', () => {
        const state = { description: ['d1', 'd2', 'd3'] };
        const action = new AutofillActions.AddAutofillAction({ reference: ['r1', 'r2'] });
        expect(reducer(state, action)).toEqual({ description: ['d1', 'd2', 'd3'], reference: ['r1', 'r2'] });
    });

    it('should not merge empty values on add', () => {
        const state = { description: ['d1', 'd2', 'd3'] };
        const action = new AutofillActions.AddAutofillAction({ reference: ['r1', '', 'r2', ''] });
        expect(reducer(state, action)).toEqual({ description: ['d1', 'd2', 'd3'], reference: ['r1', 'r2'] });
    });

    it('should merge existing state and remove duplicates', () => {
        const state = { description: ['d1', 'd2', 'd3'], reference: ['r1', 'r2', 'r3'] };
        const action = new AutofillActions.AddAutofillAction({
            reference: ['r1', 'r2', 'r4', 'r5'],
            description: ['d1', 'd2', 'd4', 'd5']
        });
        expect(reducer(state, action)).toEqual({
            description: ['d1', 'd2', 'd3', 'd4', 'd5'],
            reference: ['r1', 'r2', 'r3', 'r4', 'r5']
        });
    });

    it('should merge existing state up to latest 10 autofill entries per key', () => {
        const state = { description: ['d1', 'd2', 'd3', 'd4', 'd5', 'd6', 'd7', 'd8'] };
        const action = new AutofillActions.AddAutofillAction({ description: ['d9', 'd10', 'd11', 'd12'] });
        expect(reducer(state, action)).toEqual({
            description: ['d3', 'd4', 'd5', 'd6', 'd7', 'd8', 'd9', 'd10', 'd11', 'd12']
        });
    });
});
