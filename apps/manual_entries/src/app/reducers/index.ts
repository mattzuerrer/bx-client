import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

import { featureName } from '../config';

import * as fromAutofill from './autofill.reducer';
import * as fromColumnConfig from './column-config.reducer';
import * as fromEntryCollection from './entry-collection.reducer';
import * as fromFileUploadDialog from './file-upload-dialog.reducer';
import * as fromFiles from './files.reducer';
import * as fromForm from './form.reducer';
import * as fromList from './list.reducer';
import * as fromTaxes from './taxes.reducer';
import * as fromTemplate from './templates.reducer';

export interface State {
    autofill: fromAutofill.State;
    columnConfig: fromColumnConfig.State;
    entryCollection: fromEntryCollection.State;
    form: fromForm.State;
    list: fromList.State;
    taxes: fromTaxes.State;
    files: fromFiles.State;
    fileUploadDialog: fromFileUploadDialog.State;
    templates: fromTemplate.State;
}

export const reducers: ActionReducerMap<State> = {
    autofill: fromAutofill.reducer,
    columnConfig: fromColumnConfig.reducer,
    entryCollection: fromEntryCollection.reducer,
    form: fromForm.reducer,
    list: fromList.reducer,
    taxes: fromTaxes.reducer,
    files: fromFiles.reducer,
    fileUploadDialog: fromFileUploadDialog.reducer,
    templates: fromTemplate.reducer
};

export const getState = createFeatureSelector<State>(featureName);

/**
 * Autofill
 */
export const getAutofillState = createSelector(getState, (state: State) => state.autofill);

export const getAutofillCollection = key => createSelector(getAutofillState, fromAutofill.getCollection(key));

/**
 * Column Config
 */
export const getColumnConfigState = createSelector(getState, (state: State) => state.columnConfig);

/**
 * Files
 */
export const getFilesState = createSelector(getState, (state: State) => state.files);

export const getFilesCollection = createSelector(getFilesState, fromFiles.getCollection);

export const getFilesLoaded = createSelector(getFilesState, fromFiles.getLoaded);

/**
 * File Upload Dialog
 */
export const getFileDialogState = createSelector(getState, (state: State) => state.fileUploadDialog);

export const getDialogFilesState = createSelector(getFileDialogState, fromFileUploadDialog.getFiles);

export const getAddedDialogFilesState = createSelector(getFileDialogState, fromFileUploadDialog.getAddedFiles);

export const getRemovedDialogFilesState = createSelector(getFileDialogState, fromFileUploadDialog.getRemovedFiles);

/**
 * Templates
 */
export const getTemplatesState = createSelector(getState, (state: State) => state.templates);

export const getTemplatesCollection = createSelector(getTemplatesState, fromTemplate.getCollection);

export const getTemplatesLoaded = createSelector(getTemplatesState, fromTemplate.getLoaded);

/**
 * Entry Collection
 */
export const getEntryCollectionState = createSelector(getState, (state: State) => state.entryCollection);

export const getEntryCollectionEntry = createSelector(getEntryCollectionState, fromEntryCollection.getEntry);

export const getEntryCollectionLoaded = createSelector(getEntryCollectionState, fromEntryCollection.getLoaded);

/**
 * Form
 */
export const getFormState = createSelector(getState, (state: State) => state.form);

export const getFormStatus = createSelector(getFormState, fromForm.getStatus);

export const getFormReferenceNr = createSelector(getFormState, fromForm.getReferenceNr);

/**
 * List
 */
export const getListState = createSelector(getState, (state: State) => state.list);

export const getListEntries = createSelector(getListState, fromList.getEntries);

export const getListLoaded = createSelector(getListState, fromList.getLoaded);

/**
 * Taxes
 */
export const getTaxesState = createSelector(getState, (state: State) => state.taxes);

export const getTaxesCollection = createSelector(getTaxesState, fromTaxes.getCollection);

export const getTaxesLoaded = createSelector(getTaxesState, fromTaxes.getLoaded);
