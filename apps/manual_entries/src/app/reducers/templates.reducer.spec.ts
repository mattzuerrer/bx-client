import { TemplatesActions } from '../actions';
import * as templateReducerFixtures from '../mocks/fixtures/templates.fixtures';
import { ManualEntryTemplate } from '../models';

import { reducer } from './templates.reducer';

describe('Template reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual({ collection: [], loaded: false });
    });

    it('should return existing state on load', () => {
        const state = { collection: [], loaded: false };
        const action = new TemplatesActions.LoadAction();
        expect(reducer(state, action)).toBe(state);
    });

    it('should return loaded true on load fail', () => {
        const state = { collection: [], loaded: false };
        const action = new TemplatesActions.LoadFailAction();
        expect(reducer(state, action)).toEqual({ collection: [], loaded: true });
    });

    it('should return new state on load success', () => {
        const state = { collection: [], loaded: false };
        const action = new TemplatesActions.LoadSuccessAction(templateReducerFixtures.templates);
        expect(reducer(state, action)).toEqual({ collection: templateReducerFixtures.templates, loaded: true });
    });

    it('should return collection containing template on create success', () => {
        const template1 = new ManualEntryTemplate();
        template1.id = 1;
        template1.name = 'Template 1';
        const template2 = new ManualEntryTemplate();
        template2.id = 2;
        template2.name = 'Template 2';
        const state = { collection: [template1], loaded: false };
        const action = new TemplatesActions.CreateSuccessAction(template2);
        expect(reducer(state, action).collection.length).toBe(2);
        expect(reducer(state, action).collection.find(template => template.name === template1.name)).toBe(template1);
        expect(reducer(state, action).collection.find(template => template.name === template2.name)).toBe(template2);
    });

    it('should return loaded true on create fail', () => {
        const template = new ManualEntryTemplate();
        template.name = 'Template 1';
        template.id = 1;
        const state = { collection: [], loaded: false };
        const action = new TemplatesActions.CreateFailAction(template);
        expect(reducer(state, action)).toEqual({ collection: [], loaded: true });
    });

    it('should return existing collection on create fail', () => {
        const template1 = new ManualEntryTemplate();
        template1.name = 'Template 1';
        template1.id = 1;
        const template2 = new ManualEntryTemplate();
        template2.name = 'Template 2';
        template2.id = 2;
        const state = { collection: [template1], loaded: false };
        const action = new TemplatesActions.CreateFailAction(template2);
        expect(reducer(state, action)).toEqual({ collection: [template1], loaded: true });
    });

    it('should return collection containing template on update success', () => {
        const template1 = new ManualEntryTemplate();
        template1.id = 1;
        template1.name = 'Template 1';
        const template2 = new ManualEntryTemplate();
        template2.id = 2;
        template2.name = 'Template 2';
        const state = { collection: [template1], loaded: false };
        const action = new TemplatesActions.UpdateSuccessAction(template2);
        expect(reducer(state, action).collection.length).toBe(2);
        expect(reducer(state, action).collection.find(template => template.name === template1.name)).toBe(template1);
        expect(reducer(state, action).collection.find(template => template.name === template2.name)).toBe(template2);
    });

    it('should return loaded true on update fail', () => {
        const template = new ManualEntryTemplate();
        template.name = 'Template 1';
        template.id = 1;
        const state = { collection: [], loaded: false };
        const action = new TemplatesActions.UpdateFailAction(template);
        expect(reducer(state, action)).toEqual({ collection: [], loaded: true });
    });

    it('should return existing collection on update fail', () => {
        const template1 = new ManualEntryTemplate();
        template1.name = 'Template 1';
        template1.id = 1;
        const template2 = new ManualEntryTemplate();
        template2.name = 'Template 2';
        template2.id = 2;
        const state = { collection: [template1], loaded: false };
        const action = new TemplatesActions.UpdateFailAction(template2);
        expect(reducer(state, action)).toEqual({ collection: [template1], loaded: true });
    });

    it('should return loaded true on delete success', () => {
        const template = new ManualEntryTemplate();
        template.name = 'Template 1';
        template.id = 1;
        const state = { collection: [template], loaded: true };
        const action = new TemplatesActions.DeleteSuccessAction(template.id);
        expect(reducer(state, action)).toEqual({ collection: [], loaded: true });
    });

    it('should return existing collection without deleted template on delete success', () => {
        const template1 = new ManualEntryTemplate();
        template1.name = 'Template 1';
        template1.id = 1;
        const template2 = new ManualEntryTemplate();
        template2.name = 'Template 2';
        template2.id = 2;
        const state = { collection: [template1, template2], loaded: true };
        const action = new TemplatesActions.DeleteSuccessAction(template1.id);
        expect(reducer(state, action)).toEqual({ collection: [template2], loaded: true });
    });

    it('should return existing state on delete fail', () => {
        const template = new ManualEntryTemplate();
        template.name = 'Template 1';
        template.id = 1;
        const state = { collection: [template], loaded: true };
        const action = new TemplatesActions.DeleteFailAction(template.id);
        expect(reducer(state, action)).toBe(state);
    });
});
