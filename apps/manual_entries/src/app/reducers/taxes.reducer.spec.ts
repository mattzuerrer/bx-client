import { TaxesActions } from '../actions';
import * as taxesReducerFixtures from '../mocks/fixtures/taxes.fixtures';

import { reducer } from './taxes.reducer';

describe('Taxes reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null, payload: null };
        expect(reducer(state, action)).toEqual({ collection: [], loaded: false });
    });

    it('should return existing state on load', () => {
        const state = { collection: [], loaded: false };
        const action = new TaxesActions.LoadAction(new Date());
        expect(reducer(state, action)).toBe(state);
    });

    it('should return loaded true on load fail', () => {
        const state = { collection: [], loaded: false };
        const action = new TaxesActions.LoadFailAction(null);
        expect(reducer(state, action)).toEqual({ collection: [], loaded: true });
    });

    it('should return new state on load success', () => {
        const state = { collection: [], loaded: false };
        const action = new TaxesActions.LoadSuccessAction(taxesReducerFixtures.taxes);
        expect(reducer(state, action)).toEqual({ collection: taxesReducerFixtures.taxes, loaded: true });
    });
});
