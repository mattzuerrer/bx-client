import { PayloadAction } from '@bx-client/ngrx';

import { AutofillActions } from '../actions';
import { Dictionary } from '../models';

export type State = Dictionary<string[]>;

const initialState: State = {};

const maxAutofillItems = 10;

const unique = arr => arr.filter((item, index) => item && arr.indexOf(item) === index);

const mergeDictionaries = (oldDictionary, newDictionary) =>
    Object.assign(
        {},
        oldDictionary,
        Object.keys(newDictionary).reduce((prev, key) => {
            const oldValues = oldDictionary[key] || [];
            const newValues = newDictionary[key];
            prev[key] = unique([...oldValues, ...newValues]).slice(-maxAutofillItems);
            return prev;
        }, {})
    );

export function reducer(state: State = initialState, action: PayloadAction): State {
    switch (action.type) {
        case AutofillActions.INIT:
            return action.payload;
        case AutofillActions.ADD:
            return mergeDictionaries(state, action.payload);
        default:
            return state;
    }
}

export const getCollection = key => (state: State) => state[key] || [];
