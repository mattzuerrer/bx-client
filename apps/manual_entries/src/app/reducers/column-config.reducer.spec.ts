import { ListActions } from '../actions';
import { ColumnStatus } from '../models';

import { reducer } from './column-config.reducer';

const setStatus = status => column => Object.assign(new ColumnStatus(), column, { status });

const setLoading = setStatus(ColumnStatus.statusLoading);

const dateColumn = new ColumnStatus('date');

const idColumn = new ColumnStatus('id');

const initialState = [dateColumn, idColumn];

const loadingState = initialState.map(setLoading);

describe('Column config reducer', () => {
    it('should have initial state of date and id', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should set status loading on list load', () => {
        const state = initialState;
        const action = new ListActions.LoadAction();
        expect(reducer(state, action)).toEqual(loadingState);
    });

    it('should set state sorted columns and add id column on list sort', () => {
        const state = undefined;
        const action = new ListActions.SortAction([dateColumn]);
        expect(reducer(state, action)).toEqual([Object.assign(setLoading(dateColumn), { reversed: true }), new ColumnStatus('id')]);
    });

    it('should set status direction on list load success', () => {
        const state = loadingState;
        const action = new ListActions.LoadSuccessAction(null);
        expect(reducer(state, action)).toEqual(initialState.map(setStatus(ColumnStatus.statusDescending)));
    });

    it('should set status direction on list load fail', () => {
        const state = loadingState;
        const action = new ListActions.LoadSuccessAction(null);
        expect(reducer(state, action)).toEqual(initialState.map(setStatus(ColumnStatus.statusDescending)));
    });
});
