import { PayloadAction } from '@bx-client/ngrx';

import { FilesActions } from '../actions';
import { File } from '../models';

export interface State {
    collection: File[];
    loaded: boolean;
}

const initialState: State = {
    collection: [],
    loaded: false
};

export function reducer(state: State = initialState, action: PayloadAction): State {
    switch (action.type) {
        case FilesActions.LOAD_SUCCESS:
            return { collection: action.payload, loaded: true };
        case FilesActions.LOAD_FILE_SUCCESS:
            return Object.assign({}, state, { collection: [...state.collection, action.payload] });
        case FilesActions.LOAD_FAIL:
            return Object.assign({}, state, { loaded: true });
        case FilesActions.ADD_INBOX_FILE:
            return Object.assign({}, state, { collection: state.collection.concat([action.payload]) });
        case FilesActions.REMOVE_INBOX_FILE:
            return Object.assign({}, state, {
                collection: state.collection.filter(existingFile => existingFile.id !== action.payload.id)
            });
        default:
            return state;
    }
}

export const getCollection = (state: State) => state.collection;

export const getLoaded = (state: State) => state.loaded;
