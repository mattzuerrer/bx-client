import { PayloadAction } from '@bx-client/ngrx';

import { TemplatesActions } from '../actions';
import { ManualEntryTemplate } from '../models';

export interface State {
    collection: ManualEntryTemplate[];
    loaded: boolean;
}

const initialState: State = {
    collection: [],
    loaded: false
};

export function reducer(state: State = initialState, action: PayloadAction): State {
    let templates: ManualEntryTemplate[];
    switch (action.type) {
        case TemplatesActions.LOAD_SUCCESS:
            return { collection: action.payload, loaded: true };
        case TemplatesActions.LOAD_FAIL:
            return Object.assign({}, state, { loaded: true });
        case TemplatesActions.CREATE_SUCCESS:
        case TemplatesActions.UPDATE_SUCCESS:
            templates = [...state.collection.filter(template => template.id !== action.payload.id), action.payload];
            templates = templates.sort(ManualEntryTemplate.compareByName);
            return { collection: templates, loaded: true };
        case TemplatesActions.CREATE_FAIL:
        case TemplatesActions.UPDATE_FAIL:
            return Object.assign({}, state, { loaded: true });
        case TemplatesActions.DELETE_SUCCESS:
            templates = state.collection.filter(template => template.id !== action.payload);
            return { collection: templates, loaded: true };
        default:
            return state;
    }
}

export const getCollection = (state: State) => state.collection;

export const getLoaded = (state: State) => state.loaded;
