import { PayloadAction } from '@bx-client/ngrx';

import { EntryCollectionActions, ListActions, TemplatesActions } from '../actions';
import { ManualEntry, ManualEntryCollection } from '../models';

export interface State {
    entry: ManualEntryCollection;
    previousDate: Date;
    loaded: boolean;
}

const initialState = (date: Date = new Date()): State => ({
    entry: Object.assign(new ManualEntryCollection(), { entries: [new ManualEntry()] }, { date }),
    previousDate: date,
    loaded: true
});
export function reducer(state: State = initialState(), action: PayloadAction): State {
    switch (action.type) {
        case ListActions.SELECT:
            return Object.assign({}, state, { entry: action.payload, loaded: true });
        case TemplatesActions.SELECT_TEMPLATE:
            const collection = Object.assign(new ManualEntryCollection(), action.payload, { date: new Date() });
            return Object.assign({}, state, { entry: collection, loaded: true });
        case EntryCollectionActions.RESET:
            return initialState(state.previousDate);
        case EntryCollectionActions.SAVE_ENTRY_COLLECTION_SUCCESS:
            return Object.assign({}, state, { previousDate: action.payload.date });
        case EntryCollectionActions.LOAD_ENTRY_COLLECTION:
            return Object.assign({}, state, { loaded: false });
        default:
            return state;
    }
}
export const getEntry = (state: State) => state.entry;
export const getLoaded = (state: State) => state.loaded;
