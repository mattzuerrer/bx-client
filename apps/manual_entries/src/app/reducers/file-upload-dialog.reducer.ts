import { PayloadAction } from '@bx-client/ngrx';

import { FileUploadDialogActions } from '../actions';
import { File } from '../models';

export interface State {
    files: File[];
    addedFiles: File[];
    removedFiles: File[];
}

const initialState: State = {
    files: [],
    addedFiles: [],
    removedFiles: []
};

export function reducer(state: State = initialState, action: PayloadAction): State {
    switch (action.type) {
        case FileUploadDialogActions.INIT_FILE_UPLOAD_DIALOG:
            return Object.assign({
                files: action.payload,
                addedFiles:
                    state.files === action.payload ? state.addedFiles.filter(existingFile => existingFile.id !== action.payload.id) : [],
                removedFiles:
                    state.files === action.payload ? state.removedFiles.filter(existingFile => existingFile.id !== action.payload.id) : []
            });
        case FileUploadDialogActions.ADD_FILE:
            return Object.assign({}, state, {
                files: state.files.concat(action.payload),
                addedFiles: state.addedFiles.concat(action.payload),
                removedFiles: state.removedFiles.filter(existingFile => existingFile.id !== action.payload.id)
            });
        case FileUploadDialogActions.REMOVE_FILE:
            return Object.assign({}, state, {
                files: state.files.filter(existingFile => existingFile.id !== action.payload.id),
                removedFiles: state.removedFiles.concat(action.payload),
                addedFiles: state.addedFiles.filter(existingFile => existingFile.id !== action.payload.id)
            });
        default:
            return state;
    }
}

export const getFiles = (state: State) => state.files;

export const getAddedFiles = (state: State) => state.addedFiles;

export const getRemovedFiles = (state: State) => state.removedFiles;
