import { PayloadAction } from '@bx-client/ngrx';

import { EntryCollectionActions, FilesActions, ListActions, TemplatesActions } from '../actions';
import { ManualEntry, ManualEntryCollection } from '../models';
import { File } from '../models/File';
import { FileConnection } from '../models/FileConnection';

export interface State {
    entries: ManualEntryCollection[];
    loaded: boolean;
}

const initialState: State = {
    entries: [],
    loaded: false
};

const assignInfo = (collection: ManualEntryCollection, info: any) => Object.assign(new ManualEntryCollection(), collection, info);

const assignEntryById = (manualEntryCollection: ManualEntryCollection, info: any) => (collection: ManualEntryCollection) =>
    collection.id === manualEntryCollection.id ? assignInfo(manualEntryCollection, info) : collection;

const addFileToEntry = (connection: FileConnection) => (collection: ManualEntryCollection) => {
    if (collection.id === connection.collectionId) {
        const newCollection = Object.assign(new ManualEntryCollection(), collection, {
            status: ManualEntryCollection.statusSelected
        });
        newCollection.entries = [];
        collection.entries.forEach((entry: ManualEntry) => {
            if (entry.id === connection.entryId) {
                const newEntry = Object.assign(new ManualEntry(), entry);
                newEntry.files = [];
                entry.files.forEach((file: File) => newEntry.files.push(file));
                newEntry.files.push(connection.file);
                newCollection.entries.push(Object.assign(new ManualEntry(), newEntry));
            } else {
                newCollection.entries.push(entry);
            }
        });
        return newCollection;
    } else {
        return collection;
    }
};

const removeFileFromEntry = (connection: FileConnection) => (collection: ManualEntryCollection) => {
    if (collection.id === connection.collectionId) {
        const newCollection = Object.assign(new ManualEntryCollection(), collection, {
            status: ManualEntryCollection.statusSelected
        });
        newCollection.entries = [];
        collection.entries.forEach((entry: ManualEntry) => {
            if (entry.id === connection.entryId) {
                const newEntry = Object.assign(new ManualEntry(), entry);
                newEntry.files = [];
                entry.files.forEach((file: File) => {
                    if (file.id !== connection.fileId) {
                        newEntry.files.push(file);
                    }
                });
                newCollection.entries.push(Object.assign(new ManualEntry(), newEntry));
            } else {
                newCollection.entries.push(entry);
            }
        });
        return newCollection;
    } else {
        return collection;
    }
};

const removeStatus = (collection: ManualEntryCollection) =>
    collection.status !== ManualEntryCollection.statusNone
        ? assignInfo(collection, { status: ManualEntryCollection.statusNone })
        : collection;

const entriesReducer = (state, action) => {
    switch (action.type) {
        case ListActions.SELECT:
            return state.map(removeStatus).map(
                assignEntryById(action.payload, {
                    status: ManualEntryCollection.statusSelected
                })
            );
        case TemplatesActions.SELECT_TEMPLATE:
            return state.map(removeStatus);
        case ListActions.LOAD_SUCCESS:
            return action.payload;
        case EntryCollectionActions.SAVE_ENTRY_COLLECTION_SUCCESS:
            return [assignInfo(action.payload, { status: ManualEntryCollection.statusUpdated }), ...state.map(removeStatus)];
        case EntryCollectionActions.UPDATE_ENTRY_COLLECTION_SUCCESS:
            return state.map(removeStatus).map(
                assignEntryById(action.payload, {
                    status: ManualEntryCollection.statusUpdated
                })
            );
        case FilesActions.CONNECT_SUCCESS:
            return state.map(removeStatus).map(addFileToEntry(action.payload));
        case FilesActions.DISCONNECT_SUCCESS:
            return state.map(removeStatus).map(removeFileFromEntry(action.payload));
        case EntryCollectionActions.CANCEL:
            return state.map(removeStatus);
        case EntryCollectionActions.DELETE_ENTRY_COLLECTION_SUCCESS:
            const id = action.payload;
            return state.map(removeStatus).filter(manualEntryCollection => manualEntryCollection.id !== id);
    }
};

export function reducer(state: State = initialState, action: PayloadAction): State {
    switch (action.type) {
        case ListActions.LOAD_SUCCESS:
        case TemplatesActions.SELECT_TEMPLATE:
            return { entries: entriesReducer(state.entries, action), loaded: true };
        case ListActions.SELECT:
        case EntryCollectionActions.SAVE_ENTRY_COLLECTION_SUCCESS:
        case EntryCollectionActions.UPDATE_ENTRY_COLLECTION_SUCCESS:
        case EntryCollectionActions.CANCEL:
        case EntryCollectionActions.DELETE_ENTRY_COLLECTION_SUCCESS:
        case FilesActions.CONNECT_SUCCESS:
        case FilesActions.DISCONNECT_SUCCESS:
            return Object.assign({}, state, { entries: entriesReducer(state.entries, action) });
        case ListActions.LOAD_FAIL:
            return Object.assign({}, state, { loaded: true });
        default:
            return state;
    }
}

export const getEntries = (state: State) => state.entries;

export const getLoaded = (state: State) => state.loaded;
