import { FilesActions } from '../actions';
import * as filesReducerFixtures from '../mocks/fixtures/files.fixtures';

import { reducer } from './files.reducer';

describe('Files reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual({ collection: [], loaded: false });
    });

    it('should return existing state on load', () => {
        const state = { collection: [], loaded: false };
        const action = new FilesActions.LoadAction();
        expect(reducer(state, action)).toBe(state);
    });

    it('should return loaded true on load fail', () => {
        const state = { collection: [], loaded: false };
        const action = new FilesActions.LoadFailAction();
        expect(reducer(state, action)).toEqual({ collection: [], loaded: true });
    });

    it('should return new state on load success', () => {
        const state = { collection: [], loaded: false };
        const action = new FilesActions.LoadSuccessAction(filesReducerFixtures.files);
        expect(reducer(state, action)).toEqual({ collection: filesReducerFixtures.files, loaded: true });
    });
});
