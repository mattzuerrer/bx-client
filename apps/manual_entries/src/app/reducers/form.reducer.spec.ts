import { EntryCollectionActions, ListActions } from '../actions';
import { FormState, ManualEntryCollection } from '../models';

import { reducer } from './form.reducer';

const initialState = Object.assign(new FormState(), { referenceNr: '', status: '' });
const progressState = Object.assign(new FormState(), { referenceNr: '', status: 'progress' });

describe('Form reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return initial state on reset', () => {
        const state = progressState;
        const action = new EntryCollectionActions.ResetAction();
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return selected reference number', () => {
        const state = initialState;
        const action = new ListActions.SelectAction(Object.assign(new ManualEntryCollection(), { referenceNr: '123AB' }));
        expect(reducer(state, action).referenceNr).toBe('123AB');
        expect(reducer(state, action).status).toBe(state.status);
    });

    it('should return reference number on success', () => {
        const state = initialState;
        const action = new EntryCollectionActions.LoadReferenceNrSuccessAction('123');
        expect(reducer(state, action).referenceNr).toBe('123');
        expect(reducer(state, action).status).toBe(state.status);
    });

    it('should return initial state on load reference number fail', () => {
        const state = initialState;
        const action = new EntryCollectionActions.LoadReferenceNrFailAction();
        expect(reducer(state, action)).toBe(state);
    });

    it('should return status with progress value on save or update', () => {
        const state = initialState;
        const saveAction = new EntryCollectionActions.SaveEntryCollectionAction(new ManualEntryCollection());
        const updateAction = new EntryCollectionActions.UpdateEntryCollectionAction(new ManualEntryCollection());
        expect(reducer(state, saveAction).referenceNr).toBe(state.referenceNr);
        expect(reducer(state, saveAction).status).toBe('progress');
        expect(reducer(state, updateAction).referenceNr).toBe(state.referenceNr);
        expect(reducer(state, updateAction).status).toBe('progress');
    });

    it('should remove progress value from status on save success or update success', () => {
        const state = progressState;
        const saveSuccessAction = new EntryCollectionActions.SaveEntryCollectionSuccessAction(new ManualEntryCollection());
        const updateSuccessAction = new EntryCollectionActions.UpdateEntryCollectionSuccessAction(new ManualEntryCollection());
        expect(reducer(state, saveSuccessAction).referenceNr).toBe(state.referenceNr);
        expect(reducer(state, saveSuccessAction).status).toBe('');
        expect(reducer(state, updateSuccessAction).referenceNr).toBe(state.referenceNr);
        expect(reducer(state, updateSuccessAction).status).toBe('');
    });

    it('should remove progress value from status on save fail or update fail', () => {
        const state = progressState;
        const saveFailAction = new EntryCollectionActions.SaveEntryCollectionFailAction(null);
        const updateFailAction = new EntryCollectionActions.UpdateEntryCollectionFailAction(null);
        expect(reducer(state, saveFailAction).referenceNr).toBe(state.referenceNr);
        expect(reducer(state, saveFailAction).status).toBe('');
        expect(reducer(state, updateFailAction).referenceNr).toBe(state.referenceNr);
        expect(reducer(state, updateFailAction).status).toBe('');
    });
});
