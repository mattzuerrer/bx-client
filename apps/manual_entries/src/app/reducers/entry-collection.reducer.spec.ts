import { EntryCollectionActions, ListActions } from '../actions';
import * as entryCollectionFixtures from '../mocks/fixtures/entry-collection.fixtures';
import { ManualEntry, ManualEntryCollection } from '../models';

import { reducer } from './entry-collection.reducer';

const today = new Date();

const enteredDate = new Date('2001-01-01');

const initialState = {
    entry: Object.assign(new ManualEntryCollection(), { entries: [new ManualEntry()] }, { date: today }),
    previousDate: today,
    loaded: true
};

const loadedState = {
    entry: entryCollectionFixtures.collection,
    previousDate: today,
    loaded: true
};

const notLoadedState = {
    entry: initialState.entry,
    previousDate: enteredDate,
    loaded: false
};

describe('Entry Collection reducer', () => {
    beforeEach(() => {
        jasmine.clock().uninstall();
        jasmine.clock().install();
        jasmine.clock().mockDate(today);
    });

    afterEach(() => {
        jasmine.clock().uninstall();
    });

    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return new state on load', () => {
        const state = notLoadedState;
        const action = new ListActions.SelectAction(entryCollectionFixtures.collection);
        expect(reducer(state, action)).toEqual({
            entry: entryCollectionFixtures.collection,
            previousDate: state.previousDate,
            loaded: true
        });
    });

    it('should reset to initial state', () => {
        const state = notLoadedState;
        const action = new EntryCollectionActions.ResetAction();
        expect(reducer(state, action)).toEqual(
            Object.assign(initialState, {
                entry: Object.assign(initialState.entry, { date: state.previousDate }),
                previousDate: state.previousDate
            })
        );
    });

    it('should set previous date on save success', () => {
        const state = loadedState;
        const action = new EntryCollectionActions.SaveEntryCollectionSuccessAction(
            Object.assign(new ManualEntryCollection(), { date: enteredDate })
        );
        expect(reducer(state, action)).toEqual(Object.assign(state, { previousDate: enteredDate }));
    });

    it('should set loaded to false while loading', () => {
        const state = loadedState;
        const action = new EntryCollectionActions.LoadEntryCollectionAction();
        expect(reducer(state, action)).toEqual(Object.assign(state, { loaded: false }));
    });
});
