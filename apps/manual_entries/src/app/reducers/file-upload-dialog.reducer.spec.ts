import { FileUploadDialogActions } from '../actions';
import { file, files } from '../mocks/fixtures/files.fixtures';

import { reducer, State } from './file-upload-dialog.reducer';

const initialState: State = {
    files: [],
    addedFiles: [],
    removedFiles: []
};

const loadedState: State = {
    files,
    addedFiles: [file],
    removedFiles: [file]
};

describe('File upload dialog reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null, payload: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return state with files on init file upload dialog', () => {
        const state = initialState;
        const action = new FileUploadDialogActions.InitFileUploadDialog(files);
        expect(reducer(state, action)).toEqual(Object.assign({}, initialState, { files }));
    });

    it('should return addedFiles or removedFiles which are not already in files', () => {
        const state = loadedState;
        const action = new FileUploadDialogActions.InitFileUploadDialog(files);
        expect(reducer(state, action).addedFiles).toContain(file);
        expect(reducer(state, action).removedFiles).toContain(file);
    });

    it('should return empty state for addedFiles and removedFiles when files differ', () => {
        const state = initialState;
        const action = new FileUploadDialogActions.InitFileUploadDialog(files);
        expect(reducer(state, action).addedFiles).toEqual(initialState.addedFiles);
        expect(reducer(state, action).removedFiles).toEqual(initialState.removedFiles);
    });

    it('should add file to files and addedFiles and remove it from removedFiles', () => {
        const state = Object.assign({}, initialState, { files: [...files], addedFiles: [], removedFiles: [file] });
        const action = new FileUploadDialogActions.AddFileAction(file);
        expect(reducer(state, action)).toEqual(Object.assign({}, state, { files: [...files, file], addedFiles: [file], removedFiles: [] }));
    });

    it('should remove file from files and addedFiles and add it to removedFiles', () => {
        const state = Object.assign({}, initialState, {
            files: [...files, file],
            addedFiles: [file],
            removedFiles: []
        });
        const action = new FileUploadDialogActions.RemoveFileAction(file);
        expect(reducer(state, action)).toEqual(Object.assign({}, state, { files: [...files], addedFiles: [], removedFiles: [file] }));
    });
});
