import { PayloadAction } from '@bx-client/ngrx';

import { TaxesActions } from '../actions';
import { Tax } from '../models';

export interface State {
    collection: Tax[];
    loaded: boolean;
}

const initialState: State = {
    collection: [],
    loaded: false
};

export function reducer(state: State = initialState, action: PayloadAction): State {
    switch (action.type) {
        case TaxesActions.LOAD_SUCCESS:
            return { collection: action.payload, loaded: true };
        case TaxesActions.LOAD_FAIL:
            return Object.assign({}, state, { loaded: true });
        default:
            return state;
    }
}

export const getCollection = (state: State) => state.collection;

export const getLoaded = (state: State) => state.loaded;
