import { PayloadAction } from '@bx-client/ngrx';

import { ListActions } from '../actions';
import { ColumnStatus } from '../models';

export type State = ColumnStatus[];

const initialState: State = [new ColumnStatus('date'), new ColumnStatus('id')];

const setStatus = status => column => Object.assign(new ColumnStatus(), column, { status });

const setLoading = setStatus(ColumnStatus.statusLoading);

export function reducer(state: State = initialState, action: PayloadAction): State {
    switch (action.type) {
        case ListActions.LOAD:
            return state.map(setLoading);
        case ListActions.SORT:
            return [
                ...action.payload.map(columnStatus =>
                    Object.assign(new ColumnStatus(), columnStatus, {
                        status: ColumnStatus.statusLoading,
                        reversed: !columnStatus.reversed
                    })
                ),
                new ColumnStatus('id')
            ];
        case ListActions.LOAD_SUCCESS:
        case ListActions.LOAD_FAIL:
            return state.map(columnStatus => Object.assign(new ColumnStatus(), columnStatus, { status: columnStatus.direction }));
        default:
            return state;
    }
}
