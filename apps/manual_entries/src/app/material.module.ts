import { NgModule } from '@angular/core';
import {
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatTooltipModule
} from '@angular/material';

@NgModule({
    imports: [
        MatAutocompleteModule,
        MatButtonModule,
        MatCardModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        MatListModule,
        MatRadioModule,
        MatCheckboxModule,
        MatIconModule,
        MatSlideToggleModule,
        MatTooltipModule,
        MatProgressBarModule,
        MatSelectModule,
        MatMenuModule
    ],
    exports: [
        MatAutocompleteModule,
        MatButtonModule,
        MatCardModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        MatListModule,
        MatRadioModule,
        MatCheckboxModule,
        MatIconModule,
        MatSlideToggleModule,
        MatTooltipModule,
        MatProgressBarModule,
        MatSelectModule,
        MatMenuModule
    ]
})
export class MaterialModule {}
