import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    { path: 'user_manager', loadChildren: './user-manager/user-manager.module#UserManagerModule' },
    {
        path: 'addon_manager',
        loadChildren: './addon-manager/addon-manager.module#AddonManagerModule'
    },
    { path: 'checkout', loadChildren: './checkout/checkout.module#CheckoutModule' },
    { path: 'overview', loadChildren: './overview/overview.module#OverviewModule' },
    { path: '**', redirectTo: 'overview' }
];

export const routing = RouterModule.forRoot(routes);
