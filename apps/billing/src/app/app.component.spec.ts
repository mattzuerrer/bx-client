import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { CommonModule as BxCommonModule } from '@bx-client/common';
import { MessageService, MessageServiceMockProvider } from '@bx-client/message';
import { of } from 'rxjs';

import { AppComponent } from './app.component';
import { DomService, PingService, StateService } from './shared';
import { DomServiceMockProvider, PingServiceMockProvider, StateServiceMockProvider } from './shared/mocks';
import { RouterOutletMockComponent } from './shared/mocks/components';

describe('App Container', () => {
    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [AppComponent, RouterOutletMockComponent],
            imports: [BxCommonModule],
            providers: [DomServiceMockProvider, StateServiceMockProvider, MessageServiceMockProvider, PingServiceMockProvider]
        });
    });

    it('should create the component', () => {
        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
        expect(component).toBeTruthy();
    });

    it(
        'should init ping requests',
        inject([PingService, StateService], (pingService, stateService) => {
            stateService.pageLoaded$ = of(true);
            spyOn(pingService, 'ping').and.callThrough();
            fixture = TestBed.createComponent(AppComponent);
            fixture.detectChanges();
            expect(pingService.ping).toHaveBeenCalled();
        })
    );

    it(
        'should subscribe state service to message service actions',
        inject([MessageService, StateService], (messageService, stateService) => {
            stateService.pageLoaded$ = of(true);
            fixture = TestBed.createComponent(AppComponent);
            fixture.detectChanges();
            spyOn(stateService, 'dispatch').and.callThrough();
            const action = { type: 'ACTION' };
            messageService.action$.next(action);
            expect(stateService.dispatch).toHaveBeenCalled();
            expect(stateService.dispatch).toHaveBeenCalledWith(action);
        })
    );

    it(
        'should call dom service dispatch init and after view init events',
        inject([DomService, StateService], (domService, stateService) => {
            jasmine.clock().uninstall();
            jasmine.clock().install();
            stateService.pageLoaded$ = of(true);
            spyOn(domService, 'dispatchInitEvent').and.callThrough();
            spyOn(domService, 'dispatchAfterViewInitEvent').and.callThrough();
            spyOn(domService, 'dispatchAfterViewCheckedEvent').and.callThrough();
            fixture = TestBed.createComponent(AppComponent);
            fixture.detectChanges();
            expect(domService.dispatchInitEvent).toHaveBeenCalled();
            expect(domService.dispatchAfterViewInitEvent).toHaveBeenCalled();
            jasmine.clock().tick(0);
            expect(domService.dispatchAfterViewCheckedEvent).toHaveBeenCalled();
            jasmine.clock().uninstall();
        })
    );

    it(
        'should show spinner while loading route',
        inject([StateService], stateService => {
            stateService.pageLoaded$ = of(false);
            fixture = TestBed.createComponent(AppComponent);
            fixture.detectChanges();
            expect(fixture.debugElement.query(By.css('bx-spinner'))).toBeTruthy();
        })
    );

    it(
        'should hide spinner when route is loaded',
        inject([StateService], stateService => {
            stateService.pageLoaded$ = of(true);
            fixture = TestBed.createComponent(AppComponent);
            fixture.detectChanges();
            expect(fixture.debugElement.query(By.css('bx-spinner'))).toBeFalsy();
        })
    );
});
