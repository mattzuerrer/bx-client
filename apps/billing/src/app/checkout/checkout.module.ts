import { NgModule } from '@angular/core';

import { SharedModule } from '../shared';

import { routing } from './checkout.routes';
import { components } from './components';
import { containers } from './containers';
import { guards } from './guards';

@NgModule({
    imports: [routing, SharedModule],
    declarations: [...components, ...containers],
    providers: [...guards]
})
export class CheckoutModule {}
