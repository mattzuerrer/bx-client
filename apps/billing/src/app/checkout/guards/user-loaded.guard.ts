import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { StatesLoadedGuard } from '../../shared';
import * as userActions from '../../shared/actions/user.action';
import { GuardConfig } from '../../shared/models';
import { StateService } from '../../shared/services/state.service';

@Injectable()
export class UserLoadedGuard extends StatesLoadedGuard implements CanActivate {
    protected config: GuardConfig[] = [
        {
            stream: this.stateService.currentUserLoaded$,
            required: true,
            action: new userActions.FetchCurrentAction()
        }
    ];

    constructor(protected stateService: StateService) {
        super(stateService);
    }
}
