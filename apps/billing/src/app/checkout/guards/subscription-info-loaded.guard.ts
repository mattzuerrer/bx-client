import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { StatesLoadedGuard } from '../../shared';
import * as subscriptionInfoActions from '../../shared/actions/subscription-info.action';
import { GuardConfig } from '../../shared/models';

@Injectable()
export class SubscriptionInfoLoadedGuard extends StatesLoadedGuard implements CanActivate {
    protected config: GuardConfig[] = [
        {
            stream: this.stateService.subscriptionInfoLoaded$,
            required: true,
            action: new subscriptionInfoActions.LoadAction()
        }
    ];
}
