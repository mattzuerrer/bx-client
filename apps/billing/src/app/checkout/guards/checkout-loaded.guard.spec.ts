import { inject, TestBed } from '@angular/core/testing';
import { RouterActions } from '@bx-client/router';
import { of } from 'rxjs';

import { StateService } from '../../shared';
import * as companyActions from '../../shared/actions/company.action';
import * as countriesActions from '../../shared/actions/countries.action';
import * as pageActions from '../../shared/actions/page.action';
import * as productCatalogActions from '../../shared/actions/product-catalog.action';
import * as subscriptionInfoActions from '../../shared/actions/subscription-info.action';
import * as userActions from '../../shared/actions/user.action';
import { StateServiceMockProvider } from '../../shared/mocks';

import { CheckoutLoadedGuard } from './checkout-loaded.guard';

describe('Checkout Loaded Guard', () => {
    let guard: CheckoutLoadedGuard;

    beforeEach(() => TestBed.configureTestingModule({ providers: [StateServiceMockProvider, CheckoutLoadedGuard] }));

    it(
        'should dispatch actions to load states if states are not loaded',
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();

            stateService.subscriptionInfoLoaded$ = of(false);
            stateService.catalogLoaded$ = of(false);
            stateService.usersLoaded$ = of(false);
            stateService.currentUserLoaded$ = of(false);
            stateService.companyLoaded$ = of(false);
            stateService.countriesLoaded$ = of(false);

            guard = new CheckoutLoadedGuard(stateService);

            guard.canActivate();

            expect(stateService.dispatch).toHaveBeenCalledTimes(7);
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.LoadingAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new subscriptionInfoActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new productCatalogActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new userActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new userActions.FetchCurrentAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new companyActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new countriesActions.LoadAction());
        })
    );

    it(
        'should not dispatch actions to load states if states are loaded',
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();

            stateService.subscriptionInfoLoaded$ = of(true);
            stateService.catalogLoaded$ = of(true);
            stateService.usersLoaded$ = of(true);
            stateService.currentUserLoaded$ = of(true);
            stateService.companyLoaded$ = of(true);
            stateService.countriesLoaded$ = of(true);

            guard = new CheckoutLoadedGuard(stateService);

            guard.canActivate();

            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.LoadingAction());
        })
    );

    it(
        'should wait for required states to load and return true if user is trial',
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();

            let canActivate = null;

            stateService.subscriptionInfoLoaded$ = of(true);
            stateService.catalogLoaded$ = of(true);
            stateService.usersLoaded$ = of(true);
            stateService.currentUserLoaded$ = of(true);
            stateService.companyLoaded$ = of(false);
            stateService.countriesLoaded$ = of(false);
            stateService.subscriptionInfoInstanceIsTrial$ = of(true);

            guard = new CheckoutLoadedGuard(stateService);

            guard.canActivate().subscribe(result => (canActivate = result));

            expect(stateService.dispatch).toHaveBeenCalledTimes(4);
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.LoadingAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new companyActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new countriesActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.IdleAction());
            expect(canActivate).toBeTruthy();
        })
    );

    it(
        'should wait for required states to load and redirect to overview if user is not trial',
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();

            let canActivate = null;

            stateService.subscriptionInfoLoaded$ = of(true);
            stateService.catalogLoaded$ = of(true);
            stateService.usersLoaded$ = of(true);
            stateService.currentUserLoaded$ = of(true);
            stateService.companyLoaded$ = of(false);
            stateService.countriesLoaded$ = of(false);
            stateService.subscriptionInfoInstanceIsTrial$ = of(false);

            guard = new CheckoutLoadedGuard(stateService);

            guard.canActivate().subscribe(result => (canActivate = result));

            expect(stateService.dispatch).toHaveBeenCalledTimes(5);
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.LoadingAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new companyActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new countriesActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.IdleAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(
                new RouterActions.Go({
                    path: ['overview']
                })
            );
            expect(canActivate).toBeTruthy();
        })
    );
});
