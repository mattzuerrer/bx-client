import { inject, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { StateService } from '../../shared';
import * as pageActions from '../../shared/actions/page.action';
import * as userActions from '../../shared/actions/user.action';
import { StateServiceMockProvider } from '../../shared/mocks';

import { UserLoadedGuard } from './user-loaded.guard';

describe('User Loaded Guard', () => {
    let guard: UserLoadedGuard;

    beforeEach(() => TestBed.configureTestingModule({ providers: [StateServiceMockProvider, UserLoadedGuard] }));

    it(
        'should dispatch actions to load states if states are not loaded',
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();
            stateService.currentUserLoaded$ = of(false);

            guard = new UserLoadedGuard(stateService);
            guard.canActivate();

            expect(stateService.dispatch).toHaveBeenCalledTimes(2);
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.LoadingAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new userActions.FetchCurrentAction());
        })
    );

    it(
        'should not dispatch actions to load states if states are loaded',
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();
            stateService.currentUserLoaded$ = of(true);

            guard = new UserLoadedGuard(stateService);

            expect(stateService.dispatch).toHaveBeenCalledTimes(0);
        })
    );
});
