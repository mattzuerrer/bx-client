import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { RouterActions } from '@bx-client/router';
import { Observable, of } from 'rxjs';
import { switchMap, take, tap } from 'rxjs/operators';

import { StateService, StatesLoadedGuard } from '../../shared';
import * as companyActions from '../../shared/actions/company.action';
import * as countriesActions from '../../shared/actions/countries.action';
import * as productCatalogActions from '../../shared/actions/product-catalog.action';
import * as subscriptionInfoActions from '../../shared/actions/subscription-info.action';
import * as userActions from '../../shared/actions/user.action';
import { GuardConfig } from '../../shared/models';

@Injectable()
export class CheckoutLoadedGuard extends StatesLoadedGuard implements CanActivate {
    protected config: GuardConfig[] = [
        {
            stream: this.stateService.subscriptionInfoLoaded$,
            required: true,
            action: new subscriptionInfoActions.LoadAction()
        },
        {
            stream: this.stateService.catalogLoaded$,
            required: true,
            action: new productCatalogActions.LoadAction()
        },
        {
            stream: this.stateService.usersLoaded$,
            required: true,
            action: new userActions.LoadAction()
        },
        {
            stream: this.stateService.currentUserLoaded$,
            required: true,
            action: new userActions.FetchCurrentAction()
        },
        {
            stream: this.stateService.companyLoaded$,
            required: false,
            action: new companyActions.LoadAction()
        },
        {
            stream: this.stateService.countriesLoaded$,
            required: false,
            action: new countriesActions.LoadAction()
        }
    ];

    constructor(protected stateService: StateService) {
        super(stateService);
    }

    canActivate(): Observable<boolean> {
        return super.canActivate().pipe(switchMap(() => this.checkIsTrial()));
    }

    private checkIsTrial(): Observable<boolean> {
        return this.stateService.subscriptionInfoInstanceIsTrial$.pipe(
            take(1),
            switchMap(isTrial =>
                isTrial
                    ? of(true)
                    : of(true).pipe(tap(() => this.stateService.dispatch(new RouterActions.Go({ path: ['overview'] }))))
            )
        );
    }
}
