import { inject, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { StateService } from '../../shared';
import * as pageActions from '../../shared/actions/page.action';
import * as subscriptionInfoActions from '../../shared/actions/subscription-info.action';
import { StateServiceMockProvider } from '../../shared/mocks';

import { CheckoutLoadedGuard } from './checkout-loaded.guard';
import { SubscriptionInfoLoadedGuard } from './subscription-info-loaded.guard';

describe('SubscriptionInfo Loaded Guard', () => {
    let guard: SubscriptionInfoLoadedGuard;

    beforeEach(() => TestBed.configureTestingModule({ providers: [StateServiceMockProvider, CheckoutLoadedGuard] }));

    it(
        'should dispatch actions to load states if states are not loaded',
        inject([StateService], (stateService: StateService) => {
            spyOn(stateService, 'dispatch').and.callThrough();

            stateService.subscriptionInfoLoaded$ = of(false);

            guard = new SubscriptionInfoLoadedGuard(stateService);
            guard.canActivate();

            expect(stateService.dispatch).toHaveBeenCalledTimes(2);
            expect(stateService.dispatch).toHaveBeenCalledWith(new subscriptionInfoActions.LoadAction());
        })
    );

    it(
        'should not dispatch actions to load states if states are loaded',
        inject([StateService], (stateService: StateService) => {
            spyOn(stateService, 'dispatch').and.callThrough();

            stateService.subscriptionInfoLoaded$ = of(true);

            guard = new SubscriptionInfoLoadedGuard(stateService);
            guard.canActivate();

            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.LoadingAction());
        })
    );
});
