import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageGuard } from '../shared';

import { title } from './config';
import { CheckoutComponent, ThankYouComponent } from './containers';
import { CheckoutLoadedGuard, SubscriptionInfoLoadedGuard, UserLoadedGuard } from './guards';

export const checkoutRouteData = {
    title: title.checkout
};

export const checkoutThankYouRouteData = {
    title: title.thankYou
};

const routes: Routes = [
    {
        path: '',
        component: CheckoutComponent,
        data: checkoutRouteData,
        canActivate: [PageGuard, CheckoutLoadedGuard]
    },
    {
        path: 'thankyou',
        component: ThankYouComponent,
        data: checkoutThankYouRouteData,
        canActivate: [PageGuard, UserLoadedGuard, SubscriptionInfoLoadedGuard]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
