import { CheckoutComponent } from './checkout/checkout.component';
import { ThankYouComponent } from './thank-you/thank-you.component';

export { CheckoutComponent } from './checkout/checkout.component';
export { ThankYouComponent } from './thank-you/thank-you.component';

export const containers = [CheckoutComponent, ThankYouComponent];
