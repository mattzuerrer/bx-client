import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, OnInit, Renderer2, ViewEncapsulation } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { StateService } from '../../../shared';
import * as checkoutActions from '../../../shared/actions/checkout.action';
import * as creditCardActions from '../../../shared/actions/credit-card.action';
import * as rsaSignatureActions from '../../../shared/actions/rsa-signature.action';
import {
    Addon,
    AddonAttributeCollection,
    CheckoutSummary,
    CompanyProfile,
    Country,
    PaymentMethod,
    Plan,
    RsaSignature,
    User,
    Voucher
} from '../../../shared/models';

@Component({
    selector: 'app-checkout',
    styleUrls: ['checkout.component.scss'],
    templateUrl: 'checkout.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None
})
export class CheckoutComponent implements OnInit, AfterViewInit {
    step = 1;

    countries$: Observable<Country[]>;

    checkoutPlan$: Observable<Plan>;

    checkoutProductOptions$: Observable<string[]>;

    isCheckoutProductOptionsSelectCountValid$: Observable<boolean>;

    checkoutPreview$: Observable<CheckoutSummary>;

    checkoutPreviewLoaded$: Observable<boolean>;

    users$: Observable<User[]>;

    plans$: Observable<Plan[]>;

    paidAddons$: Observable<Addon[]>;

    activeAddons$: Observable<Addon[]>;

    currentUser$: Observable<User>;

    rsaSignature$: Observable<RsaSignature>;

    checkoutPaymentMethod$: Observable<PaymentMethod>;

    checkoutBillingInfo$: Observable<CompanyProfile>;

    companyProfile$: Observable<CompanyProfile>;

    voucher$: Observable<Voucher>;

    creditCardFormSubmit$: Subject<void> = new Subject<void>();

    creditCardFormLoaded = false;

    creditCardFormSubmitted = false;

    billingInfoAddressValid: boolean;

    checkoutRequestProcessing: Observable<boolean>;

    addons$: Observable<Addon[]>;

    checkoutProductOptionsAttributes$: Observable<AddonAttributeCollection>;

    paymentMethodChanged$: Subject<void> = new Subject<void>();

    constructor(private stateService: StateService, private renderer: Renderer2, private elementRef: ElementRef) {}

    ngOnInit(): void {
        this.countries$ = this.stateService.countriesCollection$;
        this.checkoutPlan$ = this.stateService.checkoutInstancePlan$;
        this.checkoutProductOptions$ = this.stateService.checkoutInstanceProductOptions$;
        this.checkoutPreview$ = this.stateService.checkoutPreview$;
        this.checkoutPreviewLoaded$ = this.stateService.checkoutPreviewLoaded$;
        this.isCheckoutProductOptionsSelectCountValid$ = this.stateService.isCheckoutProductOptionsSelectCountValid$;
        this.users$ = this.stateService.usersCollection$;
        this.plans$ = this.stateService.catalogPlans$;
        this.paidAddons$ = this.stateService.catalogPaidAddons$;
        this.activeAddons$ = this.stateService.subscriptionInfoInstanceAddons$;
        this.currentUser$ = this.stateService.currentUserInstance$;
        this.rsaSignature$ = this.stateService.rsaSignatureInstance$;
        this.checkoutPaymentMethod$ = this.stateService.checkoutInstancePaymentMethod$;
        this.checkoutBillingInfo$ = this.stateService.checkoutInstanceBillingInfo$;
        this.companyProfile$ = this.stateService.companyProfile$;
        this.voucher$ = this.stateService.checkoutInstanceVoucher$;
        this.checkoutRequestProcessing = this.stateService.checkoutRequestProcessing$;
        this.addons$ = this.stateService.catalogAddons$;
        this.checkoutProductOptionsAttributes$ = this.stateService.checkoutInstanceProductOptionsAtributes$;
    }

    ngAfterViewInit(): void {
        // FIX: EDGE 14 renderer not showing checkout page
        setTimeout(() => this.renderer.setStyle(this.elementRef.nativeElement, 'display', 'block'), 0);
    }

    onVoucherChange(code: string): void {
        const voucher = Object.assign(new Voucher(), { code });
        this.stateService.dispatch(new checkoutActions.CheckoutChangeVoucherAction(voucher));
    }

    onFetchSignature(): void {
        this.stateService.dispatch(new rsaSignatureActions.LoadAction());
    }

    onCreditCardClear(): void {
        this.stateService.dispatch(new rsaSignatureActions.RemoveAction());
    }

    onCreditCardChange(refId: string): void {
        this.stateService.dispatch(new checkoutActions.CheckoutChangeCreditCardAction(refId));
    }

    onCreditCardError(): void {
        this.creditCardFormSubmitted = false;
    }

    onCreditCardLoadError(errorMessage: string): void {
        this.stateService.dispatch(new creditCardActions.CreditCardError(errorMessage));
    }

    onPlanChange(plan: Plan): void {
        this.stateService.dispatch(new checkoutActions.CheckoutChangePlanAction(plan));
    }

    onCreditCardFormLoaded(): void {
        this.creditCardFormLoaded = true;
    }

    onOrderNow(): void {
        if (this.isCreditCardStep) {
            this.creditCardFormSubmitted = true;
            this.creditCardFormSubmit$.next();
        } else {
            this.stateService.dispatch(new checkoutActions.CheckoutAction());
        }
    }

    onPaymentMethodChange(paymentMethod: PaymentMethod): void {
        this.stateService.dispatch(new checkoutActions.CheckoutChangePaymentMethodAction(paymentMethod));
        this.paymentMethodChanged$.next();
    }

    onBillingInfoChange(billingInfo: CompanyProfile): void {
        this.stateService.dispatch(new checkoutActions.CheckoutChangeBillingInfoAction(billingInfo));
    }

    onBillingInfoChangeValidity(valid: boolean): void {
        this.billingInfoAddressValid = valid;
    }

    onStepChange(increment: number): void {
        this.step += increment;
    }

    onCheckoutStartStepLoaded(): void {
        this.stateService.dispatch(new checkoutActions.Step1LoadedAction());
        this.stateService.dispatch(new checkoutActions.PreviewAction());
    }

    onChoosePaymentMethodStepLoaded(): void {
        this.stateService.dispatch(new checkoutActions.Step2LoadedAction());
    }

    onCreditCardStepLoaded(): void {
        this.stateService.dispatch(new checkoutActions.Step3LoadedAction());
    }

    onCheckProductOption(addonKey: string): void {
        this.stateService.dispatch(new checkoutActions.CheckoutCheckProductOptionAction(addonKey));
    }

    onUncheckProductOption(addonKey: string): void {
        this.stateService.dispatch(new checkoutActions.CheckoutUncheckProductOptionAction(addonKey));
    }

    onPayrollAttributesEndingUnitChange(payrollEmployeesNumber: number): void {
        this.stateService.dispatch(new checkoutActions.ChangePayrollEndingUnitAttribute(payrollEmployeesNumber));
    }

    get isCheckoutStartStep(): boolean {
        return this.step === 1;
    }

    get isChoosePaymentMethodStep(): boolean {
        return this.step === 2;
    }

    get isCreditCardStep(): boolean {
        return this.step === 3;
    }
}
