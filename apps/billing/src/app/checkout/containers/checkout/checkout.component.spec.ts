import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material';
import { getChildDebugElement, getChildInstance } from '@bx-client/common';
import { of } from 'rxjs';

import { StateService } from '../../../shared';
import * as checkoutActions from '../../../shared/actions/checkout.action';
import * as creditCardActions from '../../../shared/actions/credit-card.action';
import * as rsaSignatureActions from '../../../shared/actions/rsa-signature.action';
import * as fixtures from '../../../shared/fixtures';
import { StateServiceMockProvider } from '../../../shared/mocks';
import {
    AppAddonsCheckoutMockComponent,
    AppCheckoutActionsComponent,
    AppCheckoutStep1MockComponent,
    AppCheckoutStep2MockComponent,
    AppCreditCardMockComponent,
    AppPlanOptionsMockComponent,
    AppSummaryListMockComponent,
    AppVoucherMockComponent
} from '../../../shared/mocks/components';
import { TranslateMockPipe } from '../../../shared/mocks/pipes';
import { CompanyProfile, PaymentMethod, Plan, Voucher } from '../../../shared/models';

import { CheckoutComponent } from './checkout.component';

describe('Checkout Container', () => {
    let component: CheckoutComponent;
    let fixture: ComponentFixture<CheckoutComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppCheckoutStep1MockComponent,
                AppCheckoutStep2MockComponent,
                AppCreditCardMockComponent,
                AppCheckoutActionsComponent,
                AppCreditCardMockComponent,
                AppAddonsCheckoutMockComponent,
                AppVoucherMockComponent,
                AppSummaryListMockComponent,
                AppPlanOptionsMockComponent,
                CheckoutComponent,
                TranslateMockPipe
            ],
            providers: [StateServiceMockProvider],
            imports: [MatCardModule]
        });
    });

    it('should create the component', () => {
        fixture = TestBed.createComponent(CheckoutComponent);
        component = fixture.componentInstance;
        expect(component).toBeTruthy();
    });

    it('should show checkout component step 1 on first step', () => {
        fixture = TestBed.createComponent(CheckoutComponent);
        fixture.componentInstance.step = 1;
        fixture.detectChanges();
        expect(getChildDebugElement(fixture, AppCheckoutStep1MockComponent)).toBeTruthy();
        expect(getChildDebugElement(fixture, AppCheckoutStep2MockComponent)).toBeFalsy();
        expect(getChildDebugElement(fixture, AppCreditCardMockComponent)).toBeFalsy();
    });

    it('should reset display block on checkout component to prevent rendering bug on edge v14', done => {
        fixture = TestBed.createComponent(CheckoutComponent);
        fixture.componentInstance.step = 1;
        fixture.detectChanges();
        setTimeout(() => {
            expect(fixture.debugElement.nativeElement.style.display).toBe('block');
            done();
        }, 0);
    });

    it('should show checkout component step 2 on second step', () => {
        fixture = TestBed.createComponent(CheckoutComponent);
        fixture.componentInstance.step = 2;
        fixture.detectChanges();
        expect(getChildDebugElement(fixture, AppCheckoutStep1MockComponent)).toBeFalsy();
        expect(getChildDebugElement(fixture, AppCheckoutStep2MockComponent)).toBeTruthy();
        expect(getChildDebugElement(fixture, AppCreditCardMockComponent)).toBeFalsy();
    });

    it('should show checkout credit card component on third step', () => {
        fixture = TestBed.createComponent(CheckoutComponent);
        fixture.componentInstance.step = 3;
        fixture.detectChanges();
        expect(getChildDebugElement(fixture, AppCheckoutStep1MockComponent)).toBeFalsy();
        expect(getChildDebugElement(fixture, AppCheckoutStep2MockComponent)).toBeFalsy();
        expect(getChildDebugElement(fixture, AppCreditCardMockComponent)).toBeTruthy();
    });

    it('should change step for the given increment', () => {
        fixture = TestBed.createComponent(CheckoutComponent);
        fixture.componentInstance.step = 2;
        fixture.detectChanges();
        getChildDebugElement(fixture, AppCheckoutActionsComponent).triggerEventHandler('stepChange', 2);
        expect(fixture.componentInstance.step).toBe(4);
        getChildDebugElement(fixture, AppCheckoutActionsComponent).triggerEventHandler('stepChange', -3);
        expect(fixture.componentInstance.step).toBe(1);
    });

    it(
        'should pass resolved states to checkout step 1',
        inject([StateService], stateService => {
            stateService.checkoutInstancePlan$ = of(fixtures.subscriptionInfo.plan);
            stateService.catalogPlans$ = of(fixtures.productCatalog.plans);
            stateService.usersCollection$ = of(fixtures.users);
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.detectChanges();
            const checkoutStep1 = getChildInstance(fixture, AppCheckoutStep1MockComponent);
            expect(checkoutStep1.activePlan).toBe(fixtures.subscriptionInfo.plan);
            expect(checkoutStep1.plans).toBe(fixtures.productCatalog.plans);
            expect(checkoutStep1.users).toBe(fixtures.users);
        })
    );

    it(
        'should pass resolved states to checkout addons',
        inject([StateService], stateService => {
            stateService.checkoutInstancePlan$ = of(fixtures.subscriptionInfo.plan);
            stateService.subscriptionInfoInstanceAddons$ = of(fixtures.subscriptionInfo.addons);
            stateService.catalogPaidAddons$ = of(fixtures.productCatalog.addons);
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.detectChanges();
            const addonsCheckout = getChildInstance(fixture, AppAddonsCheckoutMockComponent);
            expect(addonsCheckout.activePlan).toBe(fixtures.subscriptionInfo.plan);
            expect(addonsCheckout.activeAddons).toBe(fixtures.subscriptionInfo.addons);
            expect(addonsCheckout.paidAddons).toBe(fixtures.productCatalog.addons);
        })
    );

    it(
        'should pass resolved states to voucher',
        inject([StateService], stateService => {
            stateService.checkoutPreviewLoaded$ = of(true);
            stateService.checkoutPreview$ = of(fixtures.checkoutPreviewMonth);
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.detectChanges();
            const voucherCheckout = getChildInstance(fixture, AppVoucherMockComponent);
            expect(voucherCheckout.loaded).toBe(true);
            expect(voucherCheckout.preview).toBe(fixtures.checkoutPreviewMonth);
        })
    );

    it(
        'should pass resolved states to plan options',
        inject([StateService], stateService => {
            stateService.checkoutInstancePlan$ = of(fixtures.subscriptionInfo.plan);
            stateService.catalogAddons$ = of(fixtures.productCatalog.addons);
            stateService.companyProfile$ = of(fixtures.companyProfile);
            stateService.checkoutInstanceProductOptions$ = of(fixtures.checkoutRequest.productOptions);
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.detectChanges();
            const checkoutOptions = getChildInstance(fixture, AppPlanOptionsMockComponent);
            expect(checkoutOptions.activePlan).toBe(fixtures.subscriptionInfo.plan);
            expect(checkoutOptions.addons).toBe(fixtures.productCatalog.addons);
            expect(checkoutOptions.activePlan).toBe(fixtures.subscriptionInfo.plan);
            expect(checkoutOptions.companyProfile).toBe(fixtures.companyProfile);
            expect(checkoutOptions.activatedProductOptions).toBe(fixtures.checkoutRequest.productOptions);
            expect(checkoutOptions.scope).toBe('checkout');
        })
    );

    it(
        'should pass resolved states to summary list',
        inject([StateService], stateService => {
            stateService.checkoutPreviewLoaded$ = of(true);
            stateService.checkoutPreview$ = of(fixtures.checkoutPreviewMonth);
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.detectChanges();
            const summaryListCheckout = getChildInstance(fixture, AppSummaryListMockComponent);
            expect(summaryListCheckout.previewLoaded).toBe(true);
            expect(summaryListCheckout.preview).toBe(fixtures.checkoutPreviewMonth);
        })
    );

    it(
        'should pass resolved states to checkout step 2',
        inject([StateService], stateService => {
            stateService.checkoutInstancePaymentMethod$ = of(fixtures.subscriptionInfo.paymentMethod);
            stateService.checkoutInstanceBillingInfo$ = of(fixtures.companyProfile);
            stateService.countriesCollection$ = of(fixtures.countries);
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.componentInstance.step = 2;
            fixture.detectChanges();
            const checkoutStep2 = getChildInstance(fixture, AppCheckoutStep2MockComponent);
            expect(checkoutStep2.paymentMethod).toBe(fixtures.subscriptionInfo.paymentMethod);
            expect(checkoutStep2.checkoutBillingInfo).toBe(fixtures.companyProfile);
            expect(checkoutStep2.countries).toBe(fixtures.countries);
        })
    );

    it(
        'should pass resolved states to summary list on checkout step 2',
        inject([StateService], stateService => {
            stateService.isCheckoutPreviewLoaded$ = of(true);
            stateService.checkoutPreview$ = of(fixtures.checkoutPreviewMonth);
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.componentInstance.step = 2;
            fixture.detectChanges();
            const summaryListCheckout = getChildInstance(fixture, AppSummaryListMockComponent);
            expect(summaryListCheckout.previewLoaded).toBe(true);
            expect(summaryListCheckout.preview).toBe(fixtures.checkoutPreviewMonth);
        })
    );

    it(
        'should pass resolved states to credit card on checkout step 3',
        inject([StateService], stateService => {
            stateService.rsaSignatureInstance$ = of(fixtures.rsaSignature);
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.componentInstance.step = 3;
            fixture.detectChanges();
            const creditCardCheckout = getChildInstance(fixture, AppCreditCardMockComponent);
            expect(creditCardCheckout.rsaSignature$).toBe(stateService.rsaSignatureInstance$);
            expect(creditCardCheckout.creditCardFormSubmit$).toBe(fixture.componentInstance.creditCardFormSubmit$);
        })
    );

    it(
        'should pass resolved states to checkout actions',
        inject([StateService], stateService => {
            stateService.checkoutInstancePaymentMethod$ = of(fixtures.subscriptionInfo.paymentMethod);
            stateService.checkoutInstancePlan$ = of(fixtures.subscriptionInfo.plan);
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.componentInstance.step = 3;
            fixture.componentInstance.creditCardFormLoaded = true;
            fixture.componentInstance.creditCardFormSubmitted = true;
            fixture.detectChanges();
            const actionsCheckout = getChildInstance(fixture, AppCheckoutActionsComponent);
            expect(actionsCheckout.step).toBe(3);
            expect(actionsCheckout.creditCardFormLoaded).toBe(true);
            expect(actionsCheckout.creditCardFormSubmitted).toBe(true);
            expect(actionsCheckout.paymentMethod).toBe(fixtures.subscriptionInfo.paymentMethod);
        })
    );

    it(
        'should dispatch checkout plan change action',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            const plan = new Plan();
            getChildDebugElement(fixture, AppCheckoutStep1MockComponent).triggerEventHandler('planChange', plan);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new checkoutActions.CheckoutChangePlanAction(plan));
        })
    );

    it(
        'should dispatch checkout change voucher action',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            getChildDebugElement(fixture, AppVoucherMockComponent).triggerEventHandler('voucherChange', 'test-voucher-code');
            const voucher = Object.assign(new Voucher(), { code: 'test-voucher-code' });
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new checkoutActions.CheckoutChangeVoucherAction(voucher));
        })
    );

    it(
        'should dispatch checkout check addon action',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.detectChanges();
            const addonKey = 'addon_payroll';
            spyOn(stateService, 'dispatch');
            getChildDebugElement(fixture, AppPlanOptionsMockComponent).triggerEventHandler('checkProductOption', addonKey);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new checkoutActions.CheckoutCheckProductOptionAction(addonKey));
        })
    );

    it(
        'should dispatch checkout uncheck addon action',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.detectChanges();
            const addonKey = 'addon_payroll';
            spyOn(stateService, 'dispatch');
            getChildDebugElement(fixture, AppPlanOptionsMockComponent).triggerEventHandler('uncheckProductOption', addonKey);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new checkoutActions.CheckoutUncheckProductOptionAction(addonKey));
        })
    );

    it(
        'should dispatch checkout action on order now',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.componentInstance.step = 2;
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            getChildDebugElement(fixture, AppCheckoutActionsComponent).triggerEventHandler('orderNow', null);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new checkoutActions.CheckoutAction());
        })
    );

    it(
        'should dispatch payment method change',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.componentInstance.step = 2;
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            const paymentMethod = new PaymentMethod();
            getChildDebugElement(fixture, AppCheckoutStep2MockComponent).triggerEventHandler('paymentMethodChange', paymentMethod);
            getChildDebugElement(fixture, AppCheckoutActionsComponent).triggerEventHandler('paymentMethodChange', true);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new checkoutActions.CheckoutChangePaymentMethodAction(paymentMethod));
        })
    );

    it(
        'should dispatch billing info change',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.componentInstance.step = 2;
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            const billingInfo = new CompanyProfile();
            getChildDebugElement(fixture, AppCheckoutStep2MockComponent).triggerEventHandler('addressOptionChange', billingInfo);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new checkoutActions.CheckoutChangeBillingInfoAction(billingInfo));
        })
    );

    it(
        'should change validity on billing info validity change',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.componentInstance.step = 2;
            fixture.componentInstance.billingInfoAddressValid = false;
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            getChildDebugElement(fixture, AppCheckoutStep2MockComponent).triggerEventHandler('billingInfoChangeValidity', true);
            expect(fixture.componentInstance.billingInfoAddressValid).toBeTruthy();
        })
    );

    it(
        'should dispatch step 1 loaded',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.componentInstance.step = 1;
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            getChildDebugElement(fixture, AppCheckoutStep1MockComponent).triggerEventHandler('componentLoaded');
            expect(stateService.dispatch).toHaveBeenCalledTimes(2);
            expect(stateService.dispatch.calls.allArgs()).toEqual([
                [new checkoutActions.Step1LoadedAction()],
                [new checkoutActions.PreviewAction()]
            ]);
        })
    );

    it(
        'should dispatch step 2 loaded',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.componentInstance.step = 2;
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            getChildDebugElement(fixture, AppCheckoutStep2MockComponent).triggerEventHandler('componentLoaded');
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new checkoutActions.Step2LoadedAction());
        })
    );

    it(
        'should dispatch step 3 loaded',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.componentInstance.step = 3;
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            getChildDebugElement(fixture, AppCreditCardMockComponent).triggerEventHandler('componentLoaded');
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new checkoutActions.Step3LoadedAction());
        })
    );

    it(
        'should dispatch rsa signature load action',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.componentInstance.step = 3;
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            getChildDebugElement(fixture, AppCreditCardMockComponent).triggerEventHandler('fetchSignature', null);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new rsaSignatureActions.LoadAction());
        })
    );

    it(
        'should dispatch rsa signature remove action',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.componentInstance.step = 3;
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            getChildDebugElement(fixture, AppCreditCardMockComponent).triggerEventHandler('creditCardClear', null);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new rsaSignatureActions.RemoveAction());
        })
    );

    it(
        'should dispatch credit card load error action',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.componentInstance.step = 3;
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            getChildDebugElement(fixture, AppCreditCardMockComponent).triggerEventHandler('creditCardLoadError', 'credit_card_load_error');
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new creditCardActions.CreditCardError('credit_card_load_error'));
        })
    );

    it(
        'should dispatch credit card change action',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(CheckoutComponent);
            fixture.componentInstance.step = 3;
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            getChildDebugElement(fixture, AppCreditCardMockComponent).triggerEventHandler('creditCardChange', 'test-reference-id');
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new checkoutActions.CheckoutChangeCreditCardAction('test-reference-id'));
        })
    );

    it('should set credit card form submitted to true and dispatch submit event on order now', () => {
        fixture = TestBed.createComponent(CheckoutComponent);
        fixture.componentInstance.step = 3;
        fixture.componentInstance.creditCardFormSubmitted = false;
        fixture.detectChanges();
        spyOn(fixture.componentInstance.creditCardFormSubmit$, 'next').and.returnValue(null);
        getChildDebugElement(fixture, AppCheckoutActionsComponent).triggerEventHandler('orderNow', null);
        expect(fixture.componentInstance.creditCardFormSubmitted).toBeTruthy();
        expect(fixture.componentInstance.creditCardFormSubmit$.next).toHaveBeenCalled();
    });

    it('should set credit card form submitted to false on credit card error', () => {
        fixture = TestBed.createComponent(CheckoutComponent);
        fixture.componentInstance.step = 3;
        fixture.componentInstance.creditCardFormSubmitted = true;
        fixture.detectChanges();
        getChildDebugElement(fixture, AppCreditCardMockComponent).triggerEventHandler('creditCardError', null);
        expect(fixture.componentInstance.creditCardFormSubmitted).toBeFalsy();
    });

    it('should set credit card form loaded to true on credit card form loaded', () => {
        fixture = TestBed.createComponent(CheckoutComponent);
        fixture.componentInstance.step = 3;
        fixture.componentInstance.creditCardFormLoaded = false;
        fixture.detectChanges();
        getChildDebugElement(fixture, AppCreditCardMockComponent).triggerEventHandler('creditCardFormLoaded', null);
        expect(fixture.componentInstance.creditCardFormLoaded).toBeTruthy();
    });
});
