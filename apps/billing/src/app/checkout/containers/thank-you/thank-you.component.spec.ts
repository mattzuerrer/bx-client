import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';

import * as checkoutActions from '../../../shared/actions/checkout.action';
import { StateServiceMockProvider, TranslateMockPipe } from '../../../shared/mocks';
import { StateService } from '../../../shared/services';

import { ThankYouComponent } from './thank-you.component';

describe('Thank You Component', () => {
    let component: ThankYouComponent;
    let fixture: ComponentFixture<ThankYouComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ThankYouComponent, TranslateMockPipe],
            providers: [StateServiceMockProvider],
            imports: [MatCardModule, RouterTestingModule]
        });
    });

    it('should create the component', () => {
        fixture = TestBed.createComponent(ThankYouComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        expect(component).toBeTruthy();
    });

    it('should show links', () => {
        fixture = TestBed.createComponent(ThankYouComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        const links = fixture.debugElement.queryAll(By.css('a'));
        expect(links.length).toBe(4);
        expect(links[0].nativeElement.href).toContain('/admin/apps');
        expect(links[0].nativeElement.textContent).toContain('billing.addons.to_addons');

        expect(links[1].nativeElement.href).toContain('/user_manager');
        expect(links[1].nativeElement.textContent).toContain('billing.thanks.add_user');

        expect(links[2].nativeElement.href).toContain('mailto:support@bexio.com');
        expect(links[2].nativeElement.textContent).toContain('support@bexio.com');

        expect(links[3].nativeElement.href).toContain('www.bexio.com');
        expect(links[3].nativeElement.textContent).toContain('billing.thanks.sign_up_for_newsletter');
    });

    it(
        'should dispatch page loaded action',
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();
            fixture = TestBed.createComponent(ThankYouComponent);
            component = fixture.componentInstance;
            fixture.detectChanges();
            expect(stateService.dispatch).toHaveBeenCalledWith(new checkoutActions.ThankYouPageLoadedAction());
        })
    );
});
