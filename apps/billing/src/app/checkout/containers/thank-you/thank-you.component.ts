import { AfterViewInit, ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import * as checkoutActions from '../../../shared/actions/checkout.action';
import { Plan, User } from '../../../shared/models';
import { StateService } from '../../../shared/services';

@Component({
    selector: 'app-thank-you',
    templateUrl: 'thank-you.component.html',
    styleUrls: ['thank-you.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThankYouComponent implements OnInit, AfterViewInit {
    currentUser$: Observable<User>;
    subscriptionPlan$: Observable<Plan>;

    constructor(private stateService: StateService) {}

    ngOnInit(): void {
        this.currentUser$ = this.stateService.currentUserInstance$;
        this.subscriptionPlan$ = this.stateService.subscriptionInfoInstancePlan$;
    }

    ngAfterViewInit(): void {
        this.stateService.dispatch(new checkoutActions.ThankYouPageLoadedAction());
    }
}
