import { debounceTime, filter } from 'rxjs/operators';

import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject, Subscription } from 'rxjs';

import { CheckoutSummary, Voucher } from '../../../shared/models';

@Component({
    selector: 'app-voucher',
    templateUrl: 'voucher-code.component.html',
    styleUrls: ['voucher-code.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class VoucherCodeComponent implements OnInit, OnDestroy {
    @Input() enteredVoucher: Voucher;

    @Input() preview: CheckoutSummary;

    @Input() loaded: boolean;

    @Output() voucherChange: EventEmitter<string> = new EventEmitter<string>();

    voucherInputChange$: Subject<string> = new Subject<string>();

    previousCodeValue = '';

    private subscription: Subscription;

    ngOnInit(): void {
        this.previousCodeValue = this.enteredVoucher.code;
        this.subscription = this.voucherInputChange$
            .pipe(debounceTime(1200), filter(code => code !== this.previousCodeValue))
            .subscribe(code => {
                this.previousCodeValue = code;
                this.voucherChange.emit(code);
            });
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
