import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';

import { PaymentMethod, Plan } from '../../../shared/models';

@Component({
    selector: 'app-checkout-actions',
    templateUrl: 'checkout-actions.component.html',
    styleUrls: ['checkout-actions.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckoutActionsComponent implements OnInit {
    @Input() step = 1;

    @Input() activePlan: Plan = new Plan();

    @Input() paymentMethod: PaymentMethod = new PaymentMethod();

    @Input() creditCardFormLoaded = false;

    @Input() creditCardFormSubmitted = false;

    @Input() isCheckoutProductOptionsSelectCountValid: boolean;

    @Input() billingInfoAddressValid = false;

    @Input() checkoutRequestProcessing = false;

    @Input() paymentMethodChange: Subject<boolean>;

    @Output() stepChange: EventEmitter<number> = new EventEmitter<number>();

    @Output() orderNow: EventEmitter<void> = new EventEmitter<void>();

    areTermsAccepted = false;

    ngOnInit(): void {
        this.paymentMethodChange.subscribe(() => {
            this.areTermsAccepted = false;
        });
    }

    get showPreviousStepBtn(): boolean {
        return !this.isCheckoutStartStep;
    }

    get showNextStepBtn(): boolean {
        return this.isCheckoutStartStep || (this.isChoosePaymentMethodStep && this.paymentMethod.isCard);
    }

    get showOrderNowBtn(): boolean {
        return this.isCreditCardStep || (this.isChoosePaymentMethodStep && this.paymentMethod.isInvoice);
    }

    changeStep(i: number): void {
        this.stepChange.emit(i);
    }

    onOrderNow(): void {
        this.orderNow.emit();
    }

    get disableOrderNow(): boolean {
        return (
            (this.isCreditCardStep && (!this.creditCardFormLoaded || this.creditCardFormSubmitted)) ||
            (this.isChoosePaymentMethodStep && !this.billingInfoAddressValid) ||
            (this.activePlan.isAccountantPlan && !this.isCheckoutProductOptionsSelectCountValid)
        );
    }

    get isCheckoutStartStep(): boolean {
        return this.step === 1;
    }

    get isChoosePaymentMethodStep(): boolean {
        return this.step === 2;
    }

    get isCreditCardStep(): boolean {
        return this.step === 3;
    }

    get submitStatus(): string {
        return this.checkoutRequestProcessing || this.creditCardFormSubmitted ? 'progress' : '';
    }
}
