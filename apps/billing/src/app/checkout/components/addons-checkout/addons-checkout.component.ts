import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { by, byId } from '@bx-client/common';

import { Addon, Plan } from '../../../shared/models';

const byBillingPeriod = by('billingPeriod');

@Component({
    selector: 'app-addons-checkout',
    templateUrl: 'addons-checkout.component.html',
    styleUrls: ['addons-checkout.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddonsCheckoutComponent {
    @Input() paidAddons: Addon[] = [];

    @Input() activeAddons: Addon[] = [];

    @Input() activePlan: Plan = new Plan();

    get filteredAddons(): Addon[] {
        return this.paidAddons
            .filter(byId(...this.activeAddons.map(addon => addon.key)))
            .filter(byBillingPeriod(this.activePlan.billingPeriod));
    }
}
