import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Addon, CheckoutSummary, Discount, Plan, Product } from '../../../shared/models';

@Component({
    selector: 'app-summary-list',
    templateUrl: 'summary-list.component.html',
    styleUrls: ['summary-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SummaryListComponent {
    @Input() preview: CheckoutSummary = new CheckoutSummary();

    @Input() previewLoaded = false;

    get plan(): Plan {
        return this.preview.plan;
    }

    get users(): Product {
        return this.preview.users;
    }

    get addons(): Addon[] {
        return this.preview.addons;
    }

    get total(): Product {
        return this.preview.total;
    }

    get addonsPriceWithoutDiscount(): number {
        return this.preview.addons.map(addon => addon.price).reduce((a, b) => a + b, 0);
    }

    get addonsTotalWithoutDiscount(): number {
        return this.preview.addons.map(addon => addon.totalPrice).reduce((a, b) => a + b, 0);
    }

    get discounts(): Discount[] {
        return this.preview.discounts;
    }

    getDiscountKey(discount: string): string {
        switch (discount) {
            case 'Discount - bexio - Partner - UBS Fee':
                return 'billing.checkout.discount.ubs_discount';
            default:
                return 'billing.checkout.discount';
        }
    }
}
