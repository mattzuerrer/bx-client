import { AddonsCheckoutComponent } from './addons-checkout/addons-checkout.component';
import { CheckoutActionsComponent } from './checkout-actions/checkout-actions.component';
import { CheckoutStep1Component } from './checkout-step-1/checkout-step-1.component';
import { CheckoutStep2Component } from './checkout-step-2/checkout-step-2.component';
import { SummaryListComponent } from './summary-list/summary-list.component';
import { VoucherCodeComponent } from './voucher-code/voucher-code.component';

export const components = [
    AddonsCheckoutComponent,
    CheckoutActionsComponent,
    CheckoutStep1Component,
    CheckoutStep2Component,
    SummaryListComponent,
    VoucherCodeComponent
];
