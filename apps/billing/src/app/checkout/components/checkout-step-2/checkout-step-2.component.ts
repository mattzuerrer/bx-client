import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { byId } from '@bx-client/common';
import { CdnService } from '@bx-client/core/src/environments/service/cdn.service';

import { version } from '../../../../config';
import { CompanyProfile, Country, PaymentMethod, Plan } from '../../../shared/models';

@Component({
    selector: 'app-checkout-step-2',
    templateUrl: 'checkout-step-2.component.html',
    styleUrls: ['checkout-step-2.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckoutStep2Component implements AfterViewInit {
    @Input() paymentMethod: PaymentMethod = new PaymentMethod();

    @Input() checkoutBillingInfo: CompanyProfile = new CompanyProfile();

    @Input() countries: Country[] = [];

    @Input() plan: Plan = new Plan();

    @Output() paymentMethodChange: EventEmitter<PaymentMethod> = new EventEmitter<PaymentMethod>();

    @Output() addressOptionChange: EventEmitter<CompanyProfile> = new EventEmitter<CompanyProfile>();

    @Output() billingInfoChangeValidity: EventEmitter<boolean> = new EventEmitter<boolean>();

    @Output() componentLoaded: EventEmitter<void> = new EventEmitter<void>();

    assetUrl: string = this.cdn.getAssetUrl(version);

    constructor(private cdn: CdnService) {}

    ngAfterViewInit(): void {
        setTimeout(() => this.componentLoaded.emit(), 0);
    }

    changePaymentMethod(value: string): void {
        this.paymentMethodChange.emit(Object.assign(new PaymentMethod(), { type: value }));
    }

    onBillingInfoChange(billingInfo: CompanyProfile): void {
        this.addressOptionChange.emit(billingInfo);
    }

    onBillingInfoChangeValidity(valid: boolean): void {
        this.billingInfoChangeValidity.emit(valid);
    }

    getCountryName(countryCode: string): string {
        const country = this.countries.find(byId(countryCode));
        return country ? country.name : '';
    }
}
