import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { by, byId } from '@bx-client/common';

import { planProKey, planProPlusKey, planStarterKey } from '../../../../config';
import { Addon, Plan, User } from '../../../shared/models';

const byBillingPeriod = by('billingPeriod');

const byProductVersion = by('productVersion');

const byIdentifier = by('identifier');

const byKey = by('key');

@Component({
    selector: 'app-checkout-step-1',
    templateUrl: 'checkout-step-1.component.html',
    styleUrls: ['checkout-step-1.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckoutStep1Component implements AfterViewInit {
    @Input() activePlan: Plan = new Plan();

    @Input() plans: Plan[] = [];

    @Input() users: User[] = [];

    @Input() activeAddons: Addon[] = [];

    @Input() paidAddons: Addon[] = [];

    @Output() planChange: EventEmitter<Plan> = new EventEmitter<Plan>();

    @Output() componentLoaded: EventEmitter<void> = new EventEmitter<void>();

    ngAfterViewInit(): void {
        setTimeout(() => this.componentLoaded.emit(), 0);
    }

    onBillingPeriodChange(billingPeriod: string): void {
        const relatedPlan = Object.assign(new Plan(), this.activePlan, { billingPeriod });
        this.onPlanChange(relatedPlan.identifier);
    }

    onPlanChange(identifier: string): void {
        const plan = this.plans.find(byIdentifier(identifier));
        this.planChange.emit(plan);
    }

    get filteredPlans(): Plan[] {
        const plans = this.plans
            .filter(byProductVersion(this.productVersion))
            .filter(byBillingPeriod(this.billingPeriod))
            .filter(plan => !plan.isAccountantPlan);

        if (this.activePlan.isMiniPlan) {
            return plans.filter(plan => plan.isMiniPlan);
        }

        return plans.filter(plan => !plan.isMiniPlan);
    }

    get billingPeriod(): string {
        return this.activePlan.billingPeriod;
    }

    get productVersion(): string {
        return this.activePlan.productVersion;
    }

    arePlanConditionsSatisfied(plan: Plan): boolean {
        return this.isProductVersionV1 || this.isMaxNumberOfUsersSatisfied(plan);
    }

    isMaxNumberOfUsersSatisfied(plan: Plan): boolean {
        const maxNumberOfUsers = plan ? plan.maxNumberOfUsers : 0;
        return maxNumberOfUsers >= this.numberOfUsers;
    }

    get starterPlan(): Plan {
        return this.plans.find(byKey(planStarterKey));
    }

    get proPlan(): Plan {
        return this.plans.find(byKey(planProKey));
    }

    get numberOfUsers(): number {
        return this.users.length;
    }

    get isProductVersionV1(): boolean {
        return this.activePlan.isProductVersionV1;
    }

    get isProductVersionV2(): boolean {
        return this.activePlan.isProductVersionV2;
    }

    get isProPlan(): boolean {
        return this.activePlan.key === planProKey;
    }

    get isProPlusPlan(): boolean {
        return this.activePlan.key === planProPlusKey;
    }

    get removeNumberOfUsersForPro(): number {
        return this.numberOfUsers - this.proPlan.maxNumberOfUsers;
    }

    get removeNumberOfUsersForStarter(): number {
        return this.numberOfUsers - this.starterPlan.maxNumberOfUsers;
    }

    get showOptions(): boolean {
        return !this.activePlan.isMiniPlan;
    }

    get showActiveAddons(): boolean {
        if (this.activePlan.isAccountantPlan || this.activePlan.isMiniPlan) {
            return false;
        }

        const paidActiveAddons = this.paidAddons
            .filter(byId(...this.activeAddons.map(addon => addon.key)))
            .filter(byBillingPeriod(this.activePlan.billingPeriod));

        return paidActiveAddons.length > 0;
    }
}
