import { OverviewLoadedGuard } from './overview-loaded.guard';

export { OverviewLoadedGuard } from './overview-loaded.guard';

export const guards = [OverviewLoadedGuard];
