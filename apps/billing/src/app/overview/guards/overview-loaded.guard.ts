import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { RouterActions } from '@bx-client/router';
import { Observable, of } from 'rxjs';
import { switchMap, take, tap } from 'rxjs/operators';

import { StateService, StatesLoadedGuard } from '../../shared';
import * as countriesActions from '../../shared/actions/countries.action';
import * as paymentHistoryActions from '../../shared/actions/payment-history.action';
import * as productCatalogActions from '../../shared/actions/product-catalog.action';
import * as subscriptionInfoActions from '../../shared/actions/subscription-info.action';
import * as userActions from '../../shared/actions/user.action';
import { GuardConfig } from '../../shared/models';

@Injectable()
export class OverviewLoadedGuard extends StatesLoadedGuard implements CanActivate {
    protected config: GuardConfig[] = [
        {
            stream: this.stateService.usersLoaded$,
            required: true,
            action: new userActions.LoadAction()
        },
        {
            stream: this.stateService.subscriptionInfoLoaded$,
            required: true,
            action: new subscriptionInfoActions.LoadAction()
        },
        {
            stream: this.stateService.catalogLoaded$,
            required: true,
            action: new productCatalogActions.LoadAction()
        },
        {
            stream: this.stateService.countriesLoaded$,
            required: false,
            action: new countriesActions.LoadAction()
        },
        {
            stream: this.stateService.paymentHistoryLoaded$,
            required: false,
            action: new paymentHistoryActions.LoadAction()
        }
    ];

    constructor(protected stateService: StateService) {
        super(stateService);
    }

    checkIsNotTrial(): Observable<boolean> {
        return this.stateService.subscriptionInfoInstanceIsTrial$.pipe(
            take(1),
            switchMap(isTrial =>
                isTrial
                    ? of(true).pipe(tap(() => this.stateService.dispatch(new RouterActions.Go({ path: ['checkout'] }))))
                    : of(true)
            )
        );
    }

    canActivate(): Observable<boolean> {
        return super.canActivate().pipe(switchMap(() => this.checkIsNotTrial()));
    }
}
