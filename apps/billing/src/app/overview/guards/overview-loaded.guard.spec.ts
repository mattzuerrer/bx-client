import { inject, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { StateService } from '../../shared';
import * as countriesActions from '../../shared/actions/countries.action';
import * as pageActions from '../../shared/actions/page.action';
import * as paymentHistoryActions from '../../shared/actions/payment-history.action';
import * as productCatalogActions from '../../shared/actions/product-catalog.action';
import * as subscriptionInfoActions from '../../shared/actions/subscription-info.action';
import * as userActions from '../../shared/actions/user.action';
import { StateServiceMockProvider } from '../../shared/mocks';

import { OverviewLoadedGuard } from './overview-loaded.guard';

describe('Overview Loaded Guard', () => {
    let guard: OverviewLoadedGuard;

    beforeEach(() => TestBed.configureTestingModule({ providers: [StateServiceMockProvider, OverviewLoadedGuard] }));

    it(
        'should dispatch actions to load states if states are not loaded',
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();

            stateService.usersLoaded$ = of(false);
            stateService.subscriptionInfoLoaded$ = of(false);
            stateService.catalogLoaded$ = of(false);
            stateService.paymentHistoryLoaded$ = of(false);
            stateService.countriesLoaded$ = of(false);
            stateService.paymentHistoryLoaded$ = of(false);

            guard = new OverviewLoadedGuard(stateService);

            guard.canActivate();

            expect(stateService.dispatch).toHaveBeenCalledTimes(6);
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.LoadingAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new userActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new subscriptionInfoActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new productCatalogActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new countriesActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new paymentHistoryActions.LoadAction());
        })
    );

    it(
        'should not dispatch actions to load states if states are loaded',
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();

            stateService.usersLoaded$ = of(true);
            stateService.subscriptionInfoLoaded$ = of(true);
            stateService.catalogLoaded$ = of(true);
            stateService.paymentHistoryLoaded$ = of(true);
            stateService.countriesLoaded$ = of(true);

            guard = new OverviewLoadedGuard(stateService);

            guard.canActivate();

            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.LoadingAction());
        })
    );

    it(
        'should wait for required states to load and return true if user is not trial',
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();

            let canActivate = null;

            stateService.usersLoaded$ = of(true);
            stateService.subscriptionInfoLoaded$ = of(true);
            stateService.catalogLoaded$ = of(true);
            stateService.paymentHistoryLoaded$ = of(false);
            stateService.countriesLoaded$ = of(false);
            stateService.subscriptionInfoInstanceIsTrial$ = of(false);

            guard = new OverviewLoadedGuard(stateService);

            guard.canActivate().subscribe(result => (canActivate = result));

            expect(stateService.dispatch).toHaveBeenCalledTimes(4);
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.LoadingAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new paymentHistoryActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new countriesActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.IdleAction());
            expect(canActivate).toBeTruthy();
        })
    );

    it(
        'should wait for required states to load and redirect to checkout if user is trial',
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();

            let canActivate = null;

            stateService.usersLoaded$ = of(true);
            stateService.subscriptionInfoLoaded$ = of(true);
            stateService.catalogLoaded$ = of(true);
            stateService.paymentHistoryLoaded$ = of(false);
            stateService.countriesLoaded$ = of(false);
            stateService.subscriptionInfoInstanceIsTrial$ = of(true);

            guard = new OverviewLoadedGuard(stateService);

            guard.canActivate().subscribe(result => (canActivate = result));

            expect(stateService.dispatch).toHaveBeenCalledTimes(5);
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.LoadingAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new paymentHistoryActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new countriesActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.IdleAction());
            expect(canActivate).toBeTruthy();
        })
    );
});
