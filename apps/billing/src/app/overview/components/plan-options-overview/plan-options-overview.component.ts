import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { by } from '@bx-client/common';

import { addonBankingKey, addonPayrollKey } from '../../../../config';
import { Addon, AddonAttributeCollection, PayrollInfo, Plan, ScalePrice } from '../../../shared/models';

const byKey = by('key');

const byIdentifier = by('identifier');

@Component({
    selector: 'app-plan-options-overview',
    templateUrl: 'plan-options-overview.component.html',
    styleUrls: ['plan-options-overview.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlanOptionsOverviewComponent {
    @Input() activePlan: Plan = new Plan();

    @Input() addons: Addon[] = [];

    @Input() activeAddons: Addon[] = [];

    @Input() payrollInfo: PayrollInfo = new PayrollInfo();

    @Input() activatedProductOptionsAttributes: AddonAttributeCollection = new AddonAttributeCollection();

    get payrollChecked(): boolean {
        return this.payrollActivated || this.planHasPayroll;
    }

    get payrollActivated(): boolean {
        return this.activeAddons.some(byKey(addonPayrollKey));
    }

    get planHasPayroll(): boolean {
        return this.activePlan.hasPayroll;
    }

    get payrollAddon(): Addon {
        const addon = Object.assign(new Addon(), { key: addonPayrollKey, billingPeriod: this.billingPeriod });
        return this.addons.find(byIdentifier(addon.identifier));
    }

    get bankingAddon(): Addon {
        const addon = Object.assign(new Addon(), { key: addonBankingKey, billingPeriod: this.billingPeriod });
        return this.addons.find(byIdentifier(addon.identifier));
    }

    get billingPeriod(): string {
        return this.activePlan.billingPeriod;
    }

    get maxNumberOfPayrollEmployees(): number {
        return this.activePlan.maxNumberOfPayrollEmployees;
    }

    get activePayrollAddon(): Addon {
        return this.activeAddons.find(byKey(addonPayrollKey));
    }

    showTrialDaysLeft(addon: Addon): boolean {
        return addon && addon.isTrial && addon.trialDaysLeft > 0;
    }

    get numberOfPayrollEmployees(): number {
        const attributes = this.activatedProductOptionsAttributes.get(addonPayrollKey);
        return attributes && attributes.endingUnit ? attributes.endingUnit : ScalePrice.DEFAULT_NB_PAYROLL_USERS;
    }
}
