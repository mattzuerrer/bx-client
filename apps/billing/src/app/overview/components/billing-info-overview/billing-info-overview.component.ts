import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { byId } from '@bx-client/common';

import { BillingAddressFormComponent } from '../../../shared/components';
import { CompanyProfile, Country } from '../../../shared/models';

@Component({
    selector: 'app-billing-info-overview',
    templateUrl: 'billing-info-overview.component.html',
    styleUrls: ['billing-info-overview.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BillingInfoOverviewComponent {
    showBillingForm = false;

    @Input() billingInfo: CompanyProfile = new CompanyProfile();

    @Input() countries: Country[] = [];

    @Input() billingInfoFormValidity = false;

    @Output() changeBillingAddress: EventEmitter<CompanyProfile> = new EventEmitter<CompanyProfile>();

    @ViewChild(BillingAddressFormComponent) private formComponent: BillingAddressFormComponent;

    onSaveBillingAddress(): void {
        const billingInfo = this.billingInfoForm.getRawValue();
        this.changeBillingAddress.emit(Object.assign(new CompanyProfile(), billingInfo));
        this.showBillingForm = false;
    }

    get billingInfoForm(): FormGroup {
        return this.formComponent ? this.formComponent.form : new FormGroup({});
    }

    toggleBillingForm(): void {
        this.showBillingForm = !this.showBillingForm;
    }

    getCountryName(countryCode: string): string {
        const country = this.countries.find(byId(countryCode));
        return country ? country.name : '';
    }

    onChangeFormValidity(valid: boolean): void {
        this.billingInfoFormValidity = valid;
    }
}
