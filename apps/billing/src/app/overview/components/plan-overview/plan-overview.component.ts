import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { by } from '@bx-client/common';

import { planProKey, planProPlusKey, planStarterKey } from '../../../../config';
import { Addon, CheckoutSummary, Plan, Product } from '../../../shared/models';
import { SwitchPlanDialogComponent } from '../../entry-components';

const byBillingPeriod = by('billingPeriod');

const byProductVersion = by('productVersion');

const byIdentifier = by('identifier');

const byKey = by('key');

@Component({
    selector: 'app-plan-overview',
    templateUrl: 'plan-overview.component.html',
    styleUrls: ['plan-overview.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlanOverviewComponent implements OnInit {
    @Input() activePlan: Plan = new Plan();

    @Input() activeAddons: Addon[] = [];

    @Input() selectedPlan: Plan = new Plan();

    @Input() plans: Plan[] = [];

    @Input() numberOfUsers: number;

    @Input() subscriptionChanged: boolean;

    @Input() params: any = {};

    @Input() preview: CheckoutSummary = new CheckoutSummary();

    @Input() previewLoaded = false;

    @Input() isOverviewProductOptionsSelectCountValid: boolean;

    @Output() initOptions: EventEmitter<void> = new EventEmitter<void>();

    @Output() changeSubscriptionPlan: EventEmitter<Plan> = new EventEmitter<Plan>();

    @Output() redirect: EventEmitter<string> = new EventEmitter<string>();

    editMode = false;

    constructor(private matDialog: MatDialog) {}

    ngOnInit(): void {
        this.initOptions.emit();
        if (this.upgradePlanParam) {
            this.editMode = true;
            this.changeSubscriptionPlan.emit(this.upgradePlan);
        } else {
            this.changeSubscriptionPlan.emit(this.activePlan);
        }
    }

    enterEditMode(): void {
        this.initOptions.emit();
        this.editMode = true;
    }

    exitEditMode(): void {
        this.changeSubscriptionPlan.emit(this.activePlan);
        this.editMode = false;
        if (this.redirectToParam) {
            this.redirect.emit(this.redirectToParam);
        }
    }

    get disableChangePlanSubmit(): boolean {
        return (this.activePlan.isAccountantPlan && !this.isOverviewProductOptionsSelectCountValid) || !this.subscriptionChanged;
    }

    get filteredPlans(): Plan[] {
        const plans = this.plans
            .filter(byProductVersion(this.productVersion))
            .filter(byBillingPeriod(this.selectedPlan.billingPeriod))
            .filter(plan => !plan.isAccountantPlan);

        if (this.activePlan.isMiniPlan) {
            return plans.filter(plan => plan.isMiniPlan);
        }

        return plans.filter(plan => !plan.isMiniPlan);
    }

    getPlanByBillingPeriod(billingPeriod: string): Plan {
        const relatedPlan = Object.assign(new Plan(), this.selectedPlan, { billingPeriod });
        return this.plans.find(byIdentifier(relatedPlan.identifier));
    }

    onPlanChange(identifier: string): void {
        const plan = this.plans.find(byIdentifier(identifier));
        this.changeSubscriptionPlan.emit(plan);
    }

    onBillingPeriodChange(billingPeriod: string): void {
        const plan = this.getPlanByBillingPeriod(billingPeriod);
        this.changeSubscriptionPlan.emit(plan);
    }

    openSwitchPlanModal(): void {
        const monthPlan = this.getPlanByBillingPeriod(Product.billingPeriodMonth);
        const annualPlan = this.getPlanByBillingPeriod(Product.billingPeriodAnnual);
        const dialogRef = this.matDialog.open(SwitchPlanDialogComponent, { width: '416px' });
        dialogRef.componentInstance.plans = [monthPlan, annualPlan];
        dialogRef.componentInstance.activePlan = this.activePlan;
    }

    get productVersion(): string {
        return this.activePlan.productVersion;
    }

    get upgradePlan(): Plan {
        let key = this.activePlan.key;
        switch (key) {
            case planStarterKey:
                key = planProKey;
                break;
            case planProKey:
                key = planProPlusKey;
                break;
        }
        const relatedPlan = Object.assign(new Plan(), this.activePlan, { key });
        return this.plans.find(byIdentifier(relatedPlan.identifier));
    }

    arePlanConditionsSatisfied(plan: Plan): boolean {
        return this.isProductVersionV1 || this.isMaxNumberOfUsersSatisfied(plan);
    }

    isMaxNumberOfUsersSatisfied(plan: Plan): boolean {
        const maxNumberOfUsers = plan ? plan.maxNumberOfUsers : 0;
        return maxNumberOfUsers >= this.numberOfUsers;
    }

    get addonsTotalWithoutDiscount(): number {
        return this.activeAddons.map(addon => addon.totalPrice).reduce((a, b) => a + b, 0);
    }

    get isProductVersionV1(): boolean {
        return this.activePlan.isProductVersionV1;
    }

    get isProductVersionV2(): boolean {
        return this.activePlan.isProductVersionV2;
    }

    get upgradePlanParam(): boolean {
        return this.params.upgrade_plan || false;
    }

    get redirectToParam(): string {
        return this.params.redirect_to || '';
    }

    get starterPlan(): Plan {
        return this.plans.find(byKey(planStarterKey));
    }

    get proPlan(): Plan {
        return this.plans.find(byKey(planProKey));
    }

    get isProPlan(): boolean {
        return this.selectedPlan.key === planProKey;
    }

    get isProPlusPlan(): boolean {
        return this.selectedPlan.key === planProPlusKey;
    }

    get removeNumberOfUsersForPro(): number {
        return this.numberOfUsers - this.proPlan.maxNumberOfUsers;
    }

    get removeNumberOfUsersForStarter(): number {
        return this.numberOfUsers - this.starterPlan.maxNumberOfUsers;
    }
}
