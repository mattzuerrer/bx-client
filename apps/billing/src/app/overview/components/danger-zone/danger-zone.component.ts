import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-danger-zone',
    templateUrl: 'danger-zone.component.html',
    styleUrls: ['danger-zone.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DangerZoneComponent {}
