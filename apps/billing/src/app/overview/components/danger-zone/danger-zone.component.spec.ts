import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material';
import { By } from '@angular/platform-browser';

import { TranslateMockPipe } from '../../../shared/mocks/pipes/translate.mock.pipe';

import { DangerZoneComponent } from './danger-zone.component';

describe('Danger zone component', () => {
    let component: DangerZoneComponent;
    let fixture: ComponentFixture<DangerZoneComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DangerZoneComponent, TranslateMockPipe],
            imports: [MatCardModule]
        });
    });

    it('should create the component', () => {
        fixture = TestBed.createComponent(DangerZoneComponent);
        component = fixture.componentInstance;
        expect(component).toBeTruthy();
    });

    it('should set link ', () => {
        fixture = TestBed.createComponent(DangerZoneComponent);
        fixture.detectChanges();
        const link = fixture.debugElement.query(By.css('.card-content a'));
        expect(link.nativeElement.textContent).toBe('billing.overview.cancel_subscription');
        expect(link.nativeElement.href).toContain('/license_manager/delete');
    });
});
