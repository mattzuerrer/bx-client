import { AddonsOverviewComponent } from './addons-overview/addons-overview.component';
import { BillingInfoOverviewComponent } from './billing-info-overview/billing-info-overview.component';
import { DangerZoneComponent } from './danger-zone/danger-zone.component';
import { PaymentHistoryComponent } from './payment-history/payment-history.component';
import { PaymentMethodComponent } from './payment-method-overview/payment-method.component';
import { PlanOptionsOverviewComponent } from './plan-options-overview/plan-options-overview.component';
import { PlanOverviewComponent } from './plan-overview/plan-overview.component';
import { UsersOverviewComponent } from './users-overview/users-overview.component';

export const components = [
    PlanOverviewComponent,
    AddonsOverviewComponent,
    BillingInfoOverviewComponent,
    DangerZoneComponent,
    PaymentHistoryComponent,
    PaymentMethodComponent,
    UsersOverviewComponent,
    PlanOptionsOverviewComponent
];
