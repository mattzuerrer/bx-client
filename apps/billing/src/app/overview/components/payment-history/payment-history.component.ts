import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MessageActions } from '@bx-client/message';
import { StateService } from '@bx-client/ngrx';
import saveAs from 'file-saver';

import { Invoice } from '../../../shared/models';
import { ApiService } from '../../../shared/services';

@Component({
    selector: 'app-payment-history',
    templateUrl: 'payment-history.component.html',
    styleUrls: ['payment-history.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaymentHistoryComponent {
    @Input() invoices: Invoice[] = [];

    @Input() invoicesLoaded = false;

    constructor(private apiService: ApiService, private stateService: StateService) {}

    downloadInvoice(invoice: Invoice): void {
        this.apiService
            .getPaymentInvoice(invoice.id)
            .subscribe(
                response => {
                    const blob = new Blob([response.body], { type: 'application/pdf' });
                    const fileName = `${invoice.number}.pdf`;
                    saveAs(blob, fileName);
                },
                error => {
                    this.stateService.dispatch(new MessageActions.ErrorMessage({
                        messageKey: 'billing.download_invoice.error'
                    }));
                }
            );
    }
}
