import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CdnService } from '@bx-client/core/src/environments/service/cdn.service';

import { version } from '../../../../config';
import { CompanyProfile, CreditCard, PaymentMethod, Plan } from '../../../shared/models';
import { CreditCardDialogComponent } from '../../entry-components';

@Component({
    selector: 'app-payment-method',
    templateUrl: 'payment-method.component.html',
    styleUrls: ['payment-method.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaymentMethodComponent {
    @Input() creditCard: CreditCard = new CreditCard();

    @Input() paymentMethod: PaymentMethod = new PaymentMethod();

    @Input() billingInfo: CompanyProfile = new CompanyProfile();

    @Input() plan: Plan = new Plan();

    @Output() paymentMethodChange: EventEmitter<PaymentMethod> = new EventEmitter<PaymentMethod>();

    assetUrl: string = this.cdn.getAssetUrl(version);

    constructor(private matDialog: MatDialog, private cdn: CdnService) {}

    onPaymentTypeChange(type: string): void {
        const paymentMethod = Object.assign(new PaymentMethod(), { type, creditCard: this.creditCard });
        this.paymentMethodChange.emit(paymentMethod);
    }

    openCreditCardDialog(): void {
        this.matDialog.open(CreditCardDialogComponent);
    }

    get creditCardLogo(): string {
        return `assets/images/credit-cards/${this.creditCard.type.toLowerCase() || 'unknown'}.svg`;
    }
}
