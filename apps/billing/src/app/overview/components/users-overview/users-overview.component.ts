import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Product } from '../../../shared/models';

@Component({
    selector: 'app-users-overview',
    templateUrl: 'users-overview.component.html',
    styleUrls: ['users-overview.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UsersOverviewComponent {
    @Input()
    set numberOfUsers(numberOfUsers: number) {
        this.numberOfAdditionalUsers = numberOfUsers > 1 ? numberOfUsers - 1 : 0;
    }

    @Input() user: Product;

    numberOfAdditionalUsers = 0;
}
