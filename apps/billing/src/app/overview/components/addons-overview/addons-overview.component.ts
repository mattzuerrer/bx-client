import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { by } from '@bx-client/common';

import { Addon, CheckoutSummary } from '../../../shared/models';

const byIsProductOption = by('isProductOption');

@Component({
    selector: 'app-addons-overview',
    templateUrl: 'addons-overview.component.html',
    styleUrls: ['addons-overview.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddonsOverviewComponent {
    @Input() addons: Addon[] = [];

    @Input() preview: CheckoutSummary = new CheckoutSummary();

    @Input() previewLoaded = false;

    get filteredAddons(): Addon[] {
        return this.addons.filter(byIsProductOption(false));
    }

    get addonsTotal(): number {
        return this.filteredAddons.reduce((sum, addon) => sum + addon.price, 0);
    }
}
