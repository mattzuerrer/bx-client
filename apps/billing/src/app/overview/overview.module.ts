import { NgModule } from '@angular/core';

import { SharedModule } from '../shared';

import { components } from './components';
import { containers } from './containers';
import { entryComponents } from './entry-components';
import { guards } from './guards';
import { routing } from './overview.routes';

@NgModule({
    entryComponents: [...entryComponents],
    imports: [routing, SharedModule],
    declarations: [...components, ...entryComponents, ...containers],
    providers: [...guards]
})
export class OverviewModule {}
