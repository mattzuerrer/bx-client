export const breadcrumbs = [
    { translationKey: 'breadcrumbs.settings', url: '/admin' },
    { translationKey: 'breadcrumbs.settings.plans', url: '/admin/packageAndUserSetting' },
    { translationKey: 'breadcrumbs.settings.overview' }
];
