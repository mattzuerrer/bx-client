import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageGuard } from '../shared';

import { breadcrumbs, title } from './config';
import { OverviewComponent } from './containers';
import { OverviewLoadedGuard } from './guards';

const routes: Routes = [
    {
        path: '',
        component: OverviewComponent,
        data: { breadcrumbs, title },
        canActivate: [PageGuard, OverviewLoadedGuard]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
