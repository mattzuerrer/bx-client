import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { getChildDebugElement, getChildInstance } from '@bx-client/common';
import { RouterActions } from '@bx-client/router';
import { of } from 'rxjs';

import { StateService } from '../../../shared';
import * as overviewActions from '../../../shared/actions/overview.actions';
import * as subscriptionInfoActions from '../../../shared/actions/subscription-info.action';
import * as fixtures from '../../../shared/fixtures';
import { StateServiceMockProvider } from '../../../shared/mocks';
import {
    AppAddonsOverviewMockComponent,
    AppBillingInfoOverviewMockComponent,
    AppBreadcrumbsMockComponent,
    AppDangerZoneMockComponent,
    AppPaymentHistoryMockComponent,
    AppPaymentMethodMockComponent,
    AppPlanOptionsMockComponent,
    AppPlanOptionsOverviewMockComponent,
    AppPlanOverviewMockComponent,
    AppUsersOverviewMockComponent
} from '../../../shared/mocks/components';
import { CompanyProfile, PaymentMethod, Plan } from '../../../shared/models';

import { OverviewComponent } from './overview.component';

const queryParams = {
    redirect_to: 'user_manager',
    upgrade_plan: true
};

const activatedRouteProvider = {
    provide: ActivatedRoute,
    useValue: { queryParams: of(queryParams) }
};

describe('Overview Container', () => {
    let component: OverviewComponent;
    let fixture: ComponentFixture<OverviewComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppBreadcrumbsMockComponent,
                AppPlanOverviewMockComponent,
                AppUsersOverviewMockComponent,
                AppAddonsOverviewMockComponent,
                AppPlanOptionsOverviewMockComponent,
                AppPaymentMethodMockComponent,
                AppPaymentHistoryMockComponent,
                AppBillingInfoOverviewMockComponent,
                AppDangerZoneMockComponent,
                AppPlanOptionsMockComponent,
                OverviewComponent
            ],
            providers: [StateServiceMockProvider, activatedRouteProvider],
            imports: [MatCardModule]
        });
    });

    it('should create the component', () => {
        fixture = TestBed.createComponent(OverviewComponent);
        component = fixture.componentInstance;
        expect(component).toBeTruthy();
    });

    it(
        'should pass resolved states to payment history',
        inject([StateService], stateService => {
            stateService.paymentHistoryLoaded$ = of(true);
            stateService.paymentHistoryCollection$ = of(fixtures.invoices);
            fixture = TestBed.createComponent(OverviewComponent);
            fixture.detectChanges();
            const paymentHistory = getChildInstance(fixture, AppPaymentHistoryMockComponent);
            expect(paymentHistory.invoicesLoaded).toBe(true);
            expect(paymentHistory.invoices).toBe(fixtures.invoices);
        })
    );

    it(
        'should pass resolved states to billing info overview',
        inject([StateService], stateService => {
            stateService.subscriptionInfoInstanceBillingInfo$ = of(fixtures.companyProfile);
            stateService.countriesCollection$ = of(fixtures.countries);
            fixture = TestBed.createComponent(OverviewComponent);
            fixture.detectChanges();
            const billingInfoOverview = getChildInstance(fixture, AppBillingInfoOverviewMockComponent);
            expect(billingInfoOverview.billingInfo).toBe(fixtures.companyProfile);
            expect(billingInfoOverview.countries).toBe(fixtures.countries);
        })
    );

    it(
        'should pass resolved states to plan options overview',
        inject([StateService], stateService => {
            stateService.subscriptionInfoInstancePlan$ = of(fixtures.subscriptionInfo.plan);
            stateService.catalogAddons$ = of(fixtures.productCatalog.addons);
            stateService.subscriptionInfoInstanceAddons$ = of(fixtures.subscriptionInfo.addons);
            stateService.subscriptionInfoInstancePayrollInfo$ = of(fixtures.subscriptionInfo.payrollInfo);
            fixture = TestBed.createComponent(OverviewComponent);
            fixture.detectChanges();
            const planOptionsOverview = getChildInstance(fixture, AppPlanOptionsOverviewMockComponent);
            expect(planOptionsOverview.activePlan).toBe(fixtures.subscriptionInfo.plan);
            expect(planOptionsOverview.addons).toBe(fixtures.productCatalog.addons);
            expect(planOptionsOverview.activeAddons).toBe(fixtures.subscriptionInfo.addons);
            expect(planOptionsOverview.payrollInfo).toBe(fixtures.subscriptionInfo.payrollInfo);
        })
    );

    it(
        'should dispatch change plan action',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(OverviewComponent);
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            const plan = new Plan();
            getChildDebugElement(fixture, AppPlanOverviewMockComponent).triggerEventHandler('changeSubscriptionPlan', plan);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new overviewActions.ChangePlanAction(plan));
        })
    );

    it(
        'should dispatch change payment method action',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(OverviewComponent);
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            const paymentMethod = new PaymentMethod();
            getChildDebugElement(fixture, AppPaymentMethodMockComponent).triggerEventHandler('paymentMethodChange', paymentMethod);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new subscriptionInfoActions.UpdatePaymentMethodAction(paymentMethod));
        })
    );

    it(
        'should dispatch change billing address action',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(OverviewComponent);
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            const billingAddress = new CompanyProfile();
            getChildDebugElement(fixture, AppBillingInfoOverviewMockComponent).triggerEventHandler('changeBillingAddress', billingAddress);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new subscriptionInfoActions.UpdateBillingAddressAction(billingAddress));
        })
    );

    it(
        'should dispatch go action on redirect',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(OverviewComponent);
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            const path = 'user_manager';
            getChildDebugElement(fixture, AppPlanOverviewMockComponent).triggerEventHandler('redirect', path);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new RouterActions.Go({ path: [path] }));
        })
    );

    it(
        'should dispatch enter edit mode action',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(OverviewComponent);
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            getChildDebugElement(fixture, AppPlanOverviewMockComponent).triggerEventHandler('initOptions');
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new overviewActions.InitOptionsAction());
        })
    );

    it(
        'should pass resolved states to plan overview',
        inject([StateService], stateService => {
            stateService.subscriptionInfoInstancePlan$ = of(fixtures.subscriptionInfo.plan);
            stateService.overviewPlan$ = of(fixtures.subscriptionInfo.plan);
            stateService.catalogPlans$ = of(fixtures.productCatalog.plans);
            stateService.usersCollection$ = of(fixtures.users);
            stateService.overviewSubscriptionChanged$ = of(true);
            stateService.overviewPreview$ = of(fixtures.checkoutPreviewMonth);
            stateService.overviewPreviewLoaded$ = of(true);
            fixture = TestBed.createComponent(OverviewComponent);
            fixture.detectChanges();
            const planOverview = getChildInstance(fixture, AppPlanOverviewMockComponent);
            expect(planOverview.activePlan).toBe(fixtures.subscriptionInfo.plan);
            expect(planOverview.selectedPlan).toBe(fixtures.subscriptionInfo.plan);
            expect(planOverview.plans).toBe(fixtures.productCatalog.plans);
            expect(planOverview.numberOfUsers).toBe(fixtures.users.length);
            expect(planOverview.subscriptionChanged).toBe(true);
            expect(planOverview.preview).toBe(fixtures.checkoutPreviewMonth);
            expect(planOverview.previewLoaded).toBe(true);
            expect(planOverview.params).toEqual(queryParams);
        })
    );

    it(
        'should pass resolved states to users overview',
        inject([StateService], stateService => {
            stateService.usersCollection$ = of(fixtures.users);
            stateService.subscriptionInfoInstancePlanUser$ = of(fixtures.user);
            fixture = TestBed.createComponent(OverviewComponent);
            fixture.detectChanges();
            const usersOverview = getChildInstance(fixture, AppUsersOverviewMockComponent);
            expect(usersOverview.numberOfUsers).toBe(fixtures.users.length);
            expect(usersOverview.user).toBe(fixtures.user);
        })
    );

    it(
        'should pass resolved states to addons overview',
        inject([StateService], stateService => {
            stateService.subscriptionInfoInstancePaidAddons$ = of(fixtures.subscriptionInfo.addons);
            stateService.overviewPreview$ = of(fixtures.checkoutPreviewMonth);
            stateService.overviewPreviewLoaded$ = of(true);
            fixture = TestBed.createComponent(OverviewComponent);
            fixture.detectChanges();
            const addonsOverview = getChildInstance(fixture, AppAddonsOverviewMockComponent);
            expect(addonsOverview.addons).toEqual(fixtures.subscriptionInfo.addons);
            expect(addonsOverview.preview).toBe(fixtures.checkoutPreviewMonth);
            expect(addonsOverview.previewLoaded).toBe(true);
        })
    );

    it(
        'should pass resolved states to plan options',
        inject([StateService], stateService => {
            stateService.overviewPlan$ = of(fixtures.subscriptionInfo.plan);
            stateService.catalogAddons$ = of(fixtures.productCatalog.addons);
            stateService.subscriptionInfoInstanceAddons$ = of(fixtures.subscriptionInfo.addons);
            stateService.overviewProductOptions$ = of(fixtures.checkoutRequest.productOptions);
            fixture = TestBed.createComponent(OverviewComponent);
            fixture.detectChanges();
            const planOptions = getChildInstance(fixture, AppPlanOptionsMockComponent);
            expect(planOptions.activePlan).toBe(fixtures.subscriptionInfo.plan);
            expect(planOptions.addons).toBe(fixtures.productCatalog.addons);
            expect(planOptions.activeAddons).toBe(fixtures.subscriptionInfo.addons);
            expect(planOptions.activatedProductOptions).toBe(fixtures.checkoutRequest.productOptions);
            expect(planOptions.scope).toBe('overview');
        })
    );

    it(
        'should dispatch check product option action',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(OverviewComponent);
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            getChildDebugElement(fixture, AppPlanOptionsMockComponent).triggerEventHandler('checkProductOption', 'addon_payroll');
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new overviewActions.CheckProductOptionAction('addon_payroll'));
        })
    );

    it(
        'should dispatch uncheck product option action',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(OverviewComponent);
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            getChildDebugElement(fixture, AppPlanOptionsMockComponent).triggerEventHandler('uncheckProductOption', 'addon_payroll');
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new overviewActions.UncheckProductOptionAction('addon_payroll'));
        })
    );

    it(
        'should pass resolved states to payment method',
        inject([StateService], stateService => {
            stateService.subscriptionInfoInstancePaymentMethodCreditCard$ = of(fixtures.subscriptionInfo.paymentMethod.creditCard);
            stateService.overviewPaymentMethod$ = of(fixtures.subscriptionInfo.paymentMethod);
            stateService.subscriptionInfoInstanceBillingInfo$ = of(fixtures.subscriptionInfo.billingInfo);
            fixture = TestBed.createComponent(OverviewComponent);
            fixture.detectChanges();
            const paymentMethod = getChildInstance(fixture, AppPaymentMethodMockComponent);
            expect(paymentMethod.creditCard).toBe(fixtures.subscriptionInfo.paymentMethod.creditCard);
            expect(paymentMethod.paymentMethod).toBe(fixtures.subscriptionInfo.paymentMethod);
            expect(paymentMethod.billingInfo).toBe(fixtures.subscriptionInfo.billingInfo);
        })
    );

    it(
        'should dispatch change payroll ending unit attribute action',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(OverviewComponent);
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            getChildDebugElement(fixture, AppPlanOptionsMockComponent).triggerEventHandler('payrollAttributesEndingUnitChange', 99);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new overviewActions.ChangePayrollEndingUnitAttribute(99));
        })
    );
});
