import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterActions } from '@bx-client/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { StateService } from '../../../shared';
import * as overviewActions from '../../../shared/actions/overview.actions';
import * as subscriptionInfoActions from '../../../shared/actions/subscription-info.action';
import {
    Addon,
    AddonAttributeCollection,
    CheckoutSummary,
    CompanyProfile,
    Country,
    CreditCard,
    Invoice,
    PaymentMethod,
    PayrollInfo,
    Plan,
    Product
} from '../../../shared/models';

@Component({
    selector: 'app-overview',
    styleUrls: ['overview.component.scss'],
    templateUrl: 'overview.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None
})
export class OverviewComponent implements OnInit {
    numberOfUsers$: Observable<number>;

    activePlan$: Observable<Plan>;

    selectedPlan$: Observable<Plan>;

    billingInfo$: Observable<CompanyProfile>;

    creditCard$: Observable<CreditCard>;

    paymentMethod$: Observable<PaymentMethod>;

    plans$: Observable<Plan[]>;

    planUser$: Observable<Product>;

    invoices$: Observable<Invoice[]>;

    invoicesLoaded$: Observable<boolean>;

    activePaidAddons$: Observable<Addon[]>;

    activeAddons$: Observable<Addon[]>;

    countries$: Observable<Country[]>;

    addons$: Observable<Addon[]>;

    payrollInfo$: Observable<PayrollInfo>;

    subscriptionChanged$: Observable<boolean>;

    isOverviewProductOptionsSelectCountValid$: Observable<boolean>;

    preview$: Observable<CheckoutSummary>;

    previewLoaded$: Observable<boolean>;

    overviewProductOptions$: Observable<string[]>;

    overviewProductOptionsAttributes$: Observable<AddonAttributeCollection>;

    queryParams$: Observable<any>;

    constructor(private stateService: StateService, private activatedRoute: ActivatedRoute) {}

    ngOnInit(): void {
        this.numberOfUsers$ = this.stateService.usersCollection$.pipe(map(users => users.length));
        this.activePlan$ = this.stateService.subscriptionInfoInstancePlan$;
        this.selectedPlan$ = this.stateService.overviewPlan$;
        this.plans$ = this.stateService.catalogPlans$;
        this.billingInfo$ = this.stateService.subscriptionInfoInstanceBillingInfo$;
        this.creditCard$ = this.stateService.subscriptionInfoInstancePaymentMethodCreditCard$;
        this.paymentMethod$ = this.stateService.overviewPaymentMethod$;
        this.planUser$ = this.stateService.subscriptionInfoInstancePlanUser$;
        this.invoices$ = this.stateService.paymentHistoryCollection$;
        this.invoicesLoaded$ = this.stateService.paymentHistoryLoaded$;
        this.activePaidAddons$ = this.stateService.subscriptionInfoInstancePaidAddons$;
        this.activeAddons$ = this.stateService.subscriptionInfoInstanceAddons$;
        this.countries$ = this.stateService.countriesCollection$;
        this.addons$ = this.stateService.catalogAddons$;
        this.payrollInfo$ = this.stateService.subscriptionInfoInstancePayrollInfo$;
        this.subscriptionChanged$ = this.stateService.overviewSubscriptionChanged$;
        this.preview$ = this.stateService.overviewPreview$;
        this.previewLoaded$ = this.stateService.overviewPreviewLoaded$;
        this.overviewProductOptions$ = this.stateService.overviewProductOptions$;
        this.isOverviewProductOptionsSelectCountValid$ = this.stateService.isOverviewProductOptionsSelectCountValid$;
        this.overviewProductOptionsAttributes$ = this.stateService.overviewProductOptionsAttributes$;
        this.queryParams$ = this.activatedRoute.queryParams;
    }

    onInitOptions(): void {
        this.stateService.dispatch(new overviewActions.InitOptionsAction());
    }

    onChangeSubscriptionPlan(plan: Plan): void {
        this.stateService.dispatch(new overviewActions.ChangePlanAction(plan));
    }

    onChangeBillingAddress(billingAddress: CompanyProfile): void {
        this.stateService.dispatch(new subscriptionInfoActions.UpdateBillingAddressAction(billingAddress));
    }

    onChangePaymentMethod(paymentMethod: PaymentMethod): void {
        this.stateService.dispatch(new subscriptionInfoActions.UpdatePaymentMethodAction(paymentMethod));
    }

    onRedirect(path: string): void {
        this.stateService.dispatch(new RouterActions.Go({ path: [path] }));
    }

    onCheckProductOption(productOption: string): void {
        this.stateService.dispatch(new overviewActions.CheckProductOptionAction(productOption));
    }

    onUncheckProductOption(productOption: string): void {
        this.stateService.dispatch(new overviewActions.UncheckProductOptionAction(productOption));
    }

    onPayrollAttributesEndingUnitChange(payrollEmployeesNumber: number): void {
        this.stateService.dispatch(new overviewActions.ChangePayrollEndingUnitAttribute(payrollEmployeesNumber));
    }
}
