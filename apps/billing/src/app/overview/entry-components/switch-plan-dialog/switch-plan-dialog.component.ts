import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { by } from '@bx-client/common';
import { Observable } from 'rxjs';

import { productOptionsKeys } from '../../../../config';
import { StateService } from '../../../shared';
import * as subscriptionInfoActions from '../../../shared/actions/subscription-info.action';
import { Addon, CheckoutSummary, Plan } from '../../../shared/models';

const byKey = by('key');

@Component({
    selector: 'app-switch-plan-dialog',
    templateUrl: './switch-plan-dialog.component.html',
    styleUrls: ['./switch-plan-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwitchPlanDialogComponent implements OnInit {
    @Input() plans: Plan[] = [];

    @Input() activePlan: Plan = new Plan();

    loaded$: Observable<boolean>;

    preview$: Observable<CheckoutSummary>;

    constructor(private dialogRef: MatDialogRef<SwitchPlanDialogComponent>, private stateService: StateService) {}

    ngOnInit(): void {
        this.loaded$ = this.stateService.overviewPreviewLoaded$;
        this.preview$ = this.stateService.overviewPreview$;
    }

    closeModal(): void {
        this.stateService.dispatch(new subscriptionInfoActions.UpdatePlanAction());
        this.dialogRef.close();
    }

    addonsTotal(addons: Addon[]): number {
        return addons.filter(addon => productOptionsKeys.indexOf(addon.key) === -1).reduce((sum, addon) => sum + addon.price, 0);
    }

    hasAddons(addons: Addon[]): boolean {
        return addons.filter(addon => productOptionsKeys.indexOf(addon.key) === -1).length > 0;
    }

    getProductOptions(addons: Addon[]): Addon[] {
        return addons.filter(addon => byKey(productOptionsKeys));
    }
}
