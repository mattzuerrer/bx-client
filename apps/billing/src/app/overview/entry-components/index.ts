import { SwitchPlanDialogComponent } from './/switch-plan-dialog/switch-plan-dialog.component';
import { CreditCardDialogComponent } from './credit-card-dialog/credit-card-dialog.component';

export { SwitchPlanDialogComponent } from './/switch-plan-dialog/switch-plan-dialog.component';
export { CreditCardDialogComponent } from './credit-card-dialog/credit-card-dialog.component';

export const entryComponents = [CreditCardDialogComponent, SwitchPlanDialogComponent];
