import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Observable, Subject } from 'rxjs';

import { StateService } from '../../../shared';
import * as creditCardActions from '../../../shared/actions/credit-card.action';
import * as rsaSignatureActions from '../../../shared/actions/rsa-signature.action';
import * as subscriptionInfoActions from '../../../shared/actions/subscription-info.action';
import { CreditCard, PaymentMethod, RsaSignature } from '../../../shared/models';

@Component({
    selector: 'app-credit-card-dialog',
    templateUrl: './credit-card-dialog.component.html',
    styleUrls: ['./credit-card-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreditCardDialogComponent implements OnInit {
    rsaSignature$: Observable<RsaSignature>;

    creditCardFormLoaded = false;

    creditCardFormSubmitted = false;

    creditCardFormSubmit$: Subject<void> = new Subject<void>();

    constructor(private dialogRef: MatDialogRef<CreditCardDialogComponent>, private stateService: StateService) {}

    ngOnInit(): void {
        this.rsaSignature$ = this.stateService.rsaSignatureInstance$;
    }

    onFetchSignature(): void {
        this.stateService.dispatch(new rsaSignatureActions.LoadAction());
    }

    onCreditCardClear(): void {
        this.stateService.dispatch(new rsaSignatureActions.RemoveAction());
    }

    onCreditCardChange(refId: string): void {
        const creditCard = Object.assign(new CreditCard(), { refId });
        const type = PaymentMethod.card;
        const paymentMethod = Object.assign(new PaymentMethod(), { type, creditCard });
        this.stateService.dispatch(new subscriptionInfoActions.UpdatePaymentMethodAction(paymentMethod));
        this.dialogRef.close();
    }

    onCreditCardError(errorMessage: any): void {
        this.creditCardFormSubmitted = false;
    }

    onCreditCardLoadError(errorMessage: string): void {
        this.stateService.dispatch(new creditCardActions.CreditCardError(errorMessage));
        this.dialogRef.close();
    }

    onCreditCardFormLoaded(): void {
        this.creditCardFormLoaded = true;
    }

    submitCreditCard(): void {
        this.creditCardFormSubmitted = true;
        this.creditCardFormSubmit$.next();
    }

    get creditCardFormSubmitStatus(): string {
        return this.creditCardFormSubmitted ? 'progress' : '';
    }
}
