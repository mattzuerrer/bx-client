import { Subject } from 'rxjs';

export class DialogReference {
    result: Subject<any> = new Subject<any>();

    componentInstance: any = {};

    afterClosed(): any {
        return this.result;
    }
}
