import { noop, Observable, of } from 'rxjs';

import {
    Addon,
    CheckoutRequest,
    CheckoutSummary,
    ColumnStatus,
    CompanyProfile,
    Country,
    PaymentMethod,
    Plan,
    RsaSignature,
    SubscriptionInfo,
    User
} from '../../models';
import { StateService } from '../../services';

export class StateServiceMock {
    usersColumnConfig$: Observable<ColumnStatus[]> = of([new ColumnStatus('test_1'), new ColumnStatus('test_2')]);

    usersCollection$: Observable<User[]> = of([]);

    checkoutInstance$: Observable<CheckoutRequest> = of(new CheckoutRequest());

    checkoutInstancePlan$: Observable<Plan> = of(new Plan());

    subscriptionInfoInstanceAddons$: Observable<Addon[]> = of([]);

    checkoutInstancePaymentMethod$: Observable<PaymentMethod> = of(new PaymentMethod());

    usersLoaded$: Observable<boolean> = of(true);

    currentUserLoaded$: Observable<boolean> = of(true);

    companyLoaded$: Observable<boolean> = of(true);

    subscriptionInfoLoaded$: Observable<boolean> = of(true);

    catalogLoaded$: Observable<boolean> = of(true);

    paymentHistoryLoaded$: Observable<boolean> = of(true);

    countriesLoaded$: Observable<boolean> = of(true);

    checkoutInstanceVoucher$: Observable<string> = of('');

    subscriptionInfoInstancePlan$: Observable<Plan> = of(new Plan());

    catalogAddons$: Observable<Addon[]> = of([]);

    subscriptionInfoInstance$: Observable<SubscriptionInfo> = of(new SubscriptionInfo());

    subscriptionInfoInstanceIsTrial$: Observable<boolean> = of(true);

    countriesCollection$: Observable<Country[]> = of([]);

    checkoutPreview$: Observable<CheckoutSummary> = of(new CheckoutSummary());

    checkoutPreviewLoaded$: Observable<boolean> = of(true);

    catalogPlans$: Observable<Plan[]> = of([]);

    currentUserInstance$: Observable<User> = of(new User());

    rsaSignatureInstance$: Observable<RsaSignature> = of(new RsaSignature());

    subscriptionInfoInstanceBillingInfo$: Observable<CompanyProfile> = of(new CompanyProfile());

    checkoutInstanceBillingInfo$: Observable<CompanyProfile> = of(new CompanyProfile());

    overviewPlan$: Observable<Plan> = of(new Plan());

    overviewProductOptions$: Observable<string[]> = of([]);

    dispatch(): void {
        noop();
    }
}

export const StateServiceMockProvider = {
    provide: StateService,
    useClass: StateServiceMock
};
