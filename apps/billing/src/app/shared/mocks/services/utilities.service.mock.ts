import { UtilitiesService } from '@bx-client/common';

export class UtilitiesServiceMock {
    snakeToCamelArray(t: any): any {
        return t;
    }

    camelToSnakeArray(t: any): any {
        return t;
    }

    snakeToCamel(t: any): any {
        return t;
    }

    camelToSnake(t: any): any {
        return t;
    }
}

export const UtilitiesServiceMockProvider = {
    provide: UtilitiesService,
    useClass: UtilitiesServiceMock
};
