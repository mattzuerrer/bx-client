import { noop } from 'rxjs';

import { DomService } from '../../services';

export class DomServiceMock {
    dispatchInitEvent(): void {
        noop();
    }

    dispatchAfterViewInitEvent(): void {
        noop();
    }

    dispatchAfterViewCheckedEvent(): void {
        noop();
    }

    dispatchLoadingEvent(): void {
        noop();
    }

    dispatchIdleEvent(): void {
        noop();
    }
}

export const DomServiceMockProvider = {
    provide: DomService,
    useClass: DomServiceMock
};
