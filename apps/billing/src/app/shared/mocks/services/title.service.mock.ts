import { Title } from '@angular/platform-browser';
import { noop } from 'rxjs';

export class TitleServiceMock {
    setTitle(): void {
        noop();
    }
}

export const TitleServiceMockProvider = {
    provide: Title,
    useClass: TitleServiceMock
};
