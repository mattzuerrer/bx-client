import { Observable, of } from 'rxjs';

import * as fixtures from '../../fixtures';
import {
    CheckoutSummary,
    CompanyProfile,
    Country,
    CreditCard,
    Invoice,
    ProductCatalog,
    RsaSignature,
    SubscriptionInfo,
    User
} from '../../models';
import { ApiService } from '../../services';

export class ApiServiceMock {
    getCompanyProfile(): Observable<CompanyProfile> {
        return of(Object.assign(new CompanyProfile(), fixtures.companyProfile));
    }

    saveCompanyProfile(): Observable<CompanyProfile> {
        return of(Object.assign(new CompanyProfile(), fixtures.companyProfile));
    }

    getProductCatalog(): Observable<ProductCatalog> {
        return of(Object.assign(new ProductCatalog(), fixtures.productCatalog));
    }

    getSubscriptionInfo(): Observable<SubscriptionInfo> {
        return of(Object.assign(new SubscriptionInfo(), fixtures.subscriptionInfo));
    }

    getCountries(): Observable<Country[]> {
        return of([Object.assign(new Country(), fixtures.country)]);
    }

    getRsaSignature(): Observable<RsaSignature> {
        return of(Object.assign(new RsaSignature(), fixtures.rsaSignature));
    }

    /* user effects mocks */
    getUsers(): Observable<User[]> {
        return of(fixtures.users.map(user => Object.assign(new User(), user)));
    }

    getCurrentUser(): Observable<User> {
        return of(Object.assign(new User(), fixtures.user));
    }

    inviteUser(): Observable<User> {
        return of(Object.assign(new User(), fixtures.user));
    }

    removeUser(id: number): Observable<boolean> {
        return of(true);
    }

    getCheckoutPreview(): Observable<CheckoutSummary> {
        return of(Object.assign(new CheckoutSummary(), fixtures.checkoutPreviewMonth));
    }

    sendCompletedCheckout(): Observable<any> {
        return of(null);
    }

    changeSubscriptionPlan(): Observable<SubscriptionInfo> {
        return of(Object.assign(new SubscriptionInfo(), fixtures.subscriptionInfo));
    }

    getPaymentHistory(): Observable<Invoice[]> {
        return of(fixtures.invoices.map(invoice => Object.assign(new Invoice(), invoice)));
    }

    changeBillingAddress(): Observable<CompanyProfile> {
        return of(Object.assign(new CompanyProfile(), fixtures.companyProfile));
    }

    changePaymentMethod(): Observable<SubscriptionInfo> {
        return of(Object.assign(new SubscriptionInfo(), fixtures.subscriptionInfo));
    }

    activateAddon(): Observable<boolean> {
        return of(true);
    }

    deactivateAddon(): Observable<boolean> {
        return of(true);
    }

    getCreditCardInfo(): Observable<CreditCard> {
        return of(new CreditCard());
    }
}

export const ApiServiceMockProvider = {
    provide: ApiService,
    useClass: ApiServiceMock
};
