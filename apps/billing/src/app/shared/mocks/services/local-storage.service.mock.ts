import { BxLocalStorageService } from '@bx-client/local-storage';
import { noop } from 'rxjs';

export class LocalStorageServiceMock {
    set(): void {
        noop();
    }

    get(): void {
        noop();
    }

    remove(): void {
        noop();
    }
}

export const LocalStorageServiceMockProvider = {
    provide: BxLocalStorageService,
    useClass: LocalStorageServiceMock
};
