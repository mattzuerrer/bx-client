import { Observable, of } from 'rxjs';

import { PingService } from '../../services';

export class PingServiceMock {
    ping(): Observable<any> {
        return of('pong');
    }

    pingOnce(): Observable<any> {
        return of('pong');
    }
}

export const PingServiceMockProvider = {
    provide: PingService,
    useClass: PingServiceMock
};
