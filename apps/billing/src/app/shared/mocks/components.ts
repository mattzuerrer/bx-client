import { MockComponent } from 'ng2-mock-component';

export const AppAddonsListMockComponent = MockComponent({
    selector: 'app-addons-list',
    inputs: ['isTrial', 'addons', 'activePlan', 'activeAddons', 'addonsLoadingList'],
    outputs: ['addonActivate', 'addonDeactivate']
});

export const AppUserListMockComponent = MockComponent({
    selector: 'app-user-list',
    inputs: ['users', 'currentUser', 'columnConfig', 'companyProfile'],
    outputs: ['sort', 'remove']
});

export const AppUserControlsMockComponent = MockComponent({
    selector: 'app-user-controls',
    inputs: ['users', 'plan', 'companyProfile'],
    outputs: ['addUser']
});

export const AppBreadcrumbsMockComponent = MockComponent({ selector: 'app-breadcrumbs' });

export const RouterOutletMockComponent = MockComponent({
    selector: 'router-outlet',
    template: '<ng-content></ng-content>'
});

export const AppPlanOverviewMockComponent = MockComponent({
    selector: 'app-plan-overview',
    template: '<ng-content></ng-content>',
    inputs: [
        'activePlan',
        'activeAddons',
        'selectedPlan',
        'plans',
        'numberOfUsers',
        'subscriptionChanged',
        'preview',
        'previewLoaded',
        'params',
        'isOverviewProductOptionsSelectCountValid'
    ],
    outputs: ['changeSubscriptionPlan', 'initOptions', 'redirect']
});

export const AppUsersOverviewMockComponent = MockComponent({
    selector: 'app-users-overview',
    inputs: ['numberOfUsers', 'user']
});

export const AppAddonsOverviewMockComponent = MockComponent({
    selector: 'app-addons-overview',
    inputs: ['addons', 'preview', 'previewLoaded']
});

export const AppPaymentMethodMockComponent = MockComponent({
    selector: 'app-payment-method',
    inputs: ['creditCard', 'paymentMethod', 'billingInfo', 'plan'],
    outputs: ['paymentMethodChange']
});

export const AppPaymentHistoryMockComponent = MockComponent({
    selector: 'app-payment-history',
    inputs: ['invoices', 'invoicesLoaded']
});

export const AppBillingInfoOverviewMockComponent = MockComponent({
    selector: 'app-billing-info-overview',
    inputs: ['billingInfo', 'countries'],
    outputs: ['changeBillingAddress']
});

export const AppDangerZoneMockComponent = MockComponent({ selector: 'app-danger-zone' });

export const AppCheckoutStep1MockComponent = MockComponent({
    selector: 'app-checkout-step-1',
    template: '<ng-content></ng-content>',
    inputs: ['activePlan', 'plans', 'users', 'activeAddons', 'paidAddons'],
    outputs: ['planChange', 'componentLoaded']
});

export const AppCheckoutStep2MockComponent = MockComponent({
    selector: 'app-checkout-step-2',
    template: '<ng-content></ng-content>',
    inputs: ['paymentMethod', 'checkoutBillingInfo', 'countries', 'plan'],
    outputs: ['paymentMethodChange', 'addressOptionChange', 'componentLoaded']
});

export const AppCreditCardMockComponent = MockComponent({
    selector: 'app-credit-card',
    inputs: ['rsaSignature$', 'creditCardFormSubmit$'],
    outputs: [
        'fetchSignature',
        'creditCardClear',
        'creditCardChange',
        'creditCardError',
        'creditCardLoadError',
        'creditCardFormLoaded',
        'componentLoaded'
    ]
});

export const AppCheckoutActionsComponent = MockComponent({
    selector: 'app-checkout-actions',
    inputs: [
        'step',
        'activePlan',
        'paymentMethod',
        'creditCardFormLoaded',
        'creditCardFormSubmitted',
        'billingInfoAddressValid',
        'checkoutRequestProcessing',
        'isCheckoutProductOptionsSelectCountValid',
        'paymentMethodChange'
    ],
    outputs: ['planChange', 'stepChange', 'orderNow']
});

export const AppAddonsCheckoutMockComponent = MockComponent({
    selector: 'app-addons-checkout',
    inputs: ['activeAddons', 'paidAddons', 'activePlan']
});

export const AppVoucherMockComponent = MockComponent({
    selector: 'app-voucher',
    inputs: ['preview', 'loaded', 'enteredVoucher'],
    outputs: ['voucherChange']
});

export const AppPlanOptionsMockComponent = MockComponent({
    selector: 'app-plan-options',
    inputs: [
        'activePlan',
        'addons',
        'activatedProductOptions',
        'activatedProductOptionsAttributes',
        'activeAddons',
        'scope',
        'companyProfile'
    ],
    outputs: ['checkProductOption', 'uncheckProductOption', 'payrollAttributesEndingUnitChange']
});

export const AppPlanOptionsOverviewMockComponent = MockComponent({
    selector: 'app-plan-options-overview',
    inputs: ['activePlan', 'addons', 'activeAddons', 'payrollInfo', 'activatedProductOptionsAttributes'],
    outputs: []
});

export const AppPlanCardwMockComponent = MockComponent({
    selector: 'app-plan-card',
    inputs: ['checked', 'plan', 'disabled'],
    outputs: []
});

export const AppSummaryListMockComponent = MockComponent({
    selector: 'app-summary-list',
    inputs: ['preview', 'previewLoaded']
});

export const LearnMoreMockComponent = MockComponent({ selector: 'app-learn-more' });
