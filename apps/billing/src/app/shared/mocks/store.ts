import { Store } from '@ngrx/store';
import { noop, of } from 'rxjs';

class StoreMock {
    private state: any = {};

    select(mapFn: any): any {
        return of(mapFn(this.state));
    }

    dispatch(): void {
        noop();
    }

    setState(state: any): void {
        this.state = state;
    }
}

export const StoreMockProvider = {
    provide: Store,
    useClass: StoreMock
};
