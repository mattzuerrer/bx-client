import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'pluralize' })
export class PluralizeMockPipe implements PipeTransform {
    transform(value: any, args?: any): string {
        return 'pluralized';
    }
}
