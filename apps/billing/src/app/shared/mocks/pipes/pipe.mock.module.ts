import { NgModule } from '@angular/core';

import { BillingPeriodTotalMockPipe, PerBillingPeriodDescriptiveMockPipe, PerBillingPeriodMockPipe } from './billing-period.mock.pipe';
import { I18nPluralMockPipe } from './i18n-plural.mock.pipe';
import { PluralizeMockPipe } from './pluralize.mock.pipe';
import { SafeHtmlMockPipe } from './safe-html.mock.pipe';
import { TranslateMockPipe } from './translate.mock.pipe';

@NgModule({
    declarations: [
        BillingPeriodTotalMockPipe,
        PerBillingPeriodDescriptiveMockPipe,
        PerBillingPeriodMockPipe,
        I18nPluralMockPipe,
        TranslateMockPipe,
        PluralizeMockPipe,
        SafeHtmlMockPipe
    ]
})
export class PipeMockModule {}
