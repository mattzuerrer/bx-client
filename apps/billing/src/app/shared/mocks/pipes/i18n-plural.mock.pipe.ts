import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'translate' })
export class I18nPluralMockPipe implements PipeTransform {
    transform(value: any, args?: any): string {
        return 'plural_key';
    }
}
