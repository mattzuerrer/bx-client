import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'billing_period_total' })
export class BillingPeriodTotalMockPipe implements PipeTransform {
    transform(value: any, args?: any): any {
        return 'billing_period';
    }
}

@Pipe({ name: 'per_billing_period_descriptive' })
export class PerBillingPeriodDescriptiveMockPipe implements PipeTransform {
    transform(value: any, args?: any): any {
        return 'billing_period';
    }
}

@Pipe({ name: 'per_billing_period' })
export class PerBillingPeriodMockPipe implements PipeTransform {
    transform(value: any, args?: any): any {
        return 'billing_period';
    }
}
