import { Pipe, PipeTransform } from '@angular/core';
import { by } from '@bx-client/common';

import { addonAccountantAccountingKey, addonAccountantPayrollKey } from '../../../config';
import { Addon } from '../models';

const byKey = by('key');

@Pipe({ name: 'accountant_product_options', pure: true })
export class AccountantProductOptionsPipe implements PipeTransform {
    transform(options: Addon[]): string {
        const hasAccountingActivated = options.some(byKey(addonAccountantAccountingKey));
        const hasPayrollActivated = options.some(byKey(addonAccountantPayrollKey));

        if (hasAccountingActivated && hasPayrollActivated) {
            return 'billing.overview.accountant_options.payroll_accounting';
        } else if (hasAccountingActivated) {
            return 'billing.overview.accountant_options.accounting';
        } else if (hasPayrollActivated) {
            return 'billing.overview.accountant_options.payroll';
        }
        return '';
    }
}
