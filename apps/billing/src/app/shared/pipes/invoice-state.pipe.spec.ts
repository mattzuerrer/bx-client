import { inject, TestBed } from '@angular/core/testing';

import { InvoiceStatePipe } from './invoice-state.pipe';

describe('Invoice state pipe', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({ providers: [InvoiceStatePipe] });
    });

    it(
        'should return open translation key',
        inject([InvoiceStatePipe], invoiceStatePipe => {
            expect(invoiceStatePipe.transform('open')).toBe('billing.payments.status_open');
        })
    );

    it(
        'should return paid translation key',
        inject([InvoiceStatePipe], invoiceStatePipe => {
            expect(invoiceStatePipe.transform('paid')).toBe('billing.payments.status_paid');
        })
    );

    it(
        'should return same string if invoice state is unknown',
        inject([InvoiceStatePipe], invoiceStatePipe => {
            expect(invoiceStatePipe.transform('unknown')).toBe('unknown');
        })
    );
});
