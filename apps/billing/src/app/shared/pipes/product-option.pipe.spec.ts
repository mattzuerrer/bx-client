import { inject, TestBed } from '@angular/core/testing';

import { addonAccountantAccountingKey, addonAccountantPayrollKey } from '../../../config';
import { Addon } from '../models';

import { AccountantProductOptionsPipe } from './product-option.pipe';

describe('Product option pipe', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({ providers: [AccountantProductOptionsPipe] });
    });

    describe('accountant product option', () => {
        const addonPayroll = new Addon();
        addonPayroll.key = addonAccountantPayrollKey;
        const addonAccounting = new Addon();
        addonAccounting.key = addonAccountantAccountingKey;

        it(
            'should return payroll translation key',
            inject([AccountantProductOptionsPipe], accountantProductOptionPipe => {
                expect(accountantProductOptionPipe.transform([addonPayroll])).toBe('billing.overview.accountant_options.payroll');
            })
        );

        it(
            'should return accounting translation key',
            inject([AccountantProductOptionsPipe], accountantProductOptionPipe => {
                expect(accountantProductOptionPipe.transform([addonAccounting])).toBe('billing.overview.accountant_options.accounting');
            })
        );

        it(
            'should return both translation key',
            inject([AccountantProductOptionsPipe], accountantProductOptionPipe => {
                expect(accountantProductOptionPipe.transform([addonAccounting, addonPayroll])).toBe(
                    'billing.overview.accountant_options.payroll_accounting'
                );
            })
        );

        it(
            'should return empty string if no options activated',
            inject([AccountantProductOptionsPipe], accountantProductOptionPipe => {
                expect(accountantProductOptionPipe.transform([])).toBe('');
            })
        );
    });
});
