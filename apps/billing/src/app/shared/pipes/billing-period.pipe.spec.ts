import { inject, TestBed } from '@angular/core/testing';

import { BillingPeriodTotalPipe, PaidBillingPeriodPipe, PerBillingPeriodPipe } from './billing-period.pipe';

describe('Billing period pipe', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [BillingPeriodTotalPipe, PerBillingPeriodPipe, PaidBillingPeriodPipe]
        });
    });

    describe('total', () => {
        it(
            'should return month translation key',
            inject([BillingPeriodTotalPipe], billingPeriodTotalPipe => {
                expect(billingPeriodTotalPipe.transform('month')).toBe('billing.checkout_total_month');
            })
        );

        it(
            'should return annual translation key',
            inject([BillingPeriodTotalPipe], billingPeriodTotalPipe => {
                expect(billingPeriodTotalPipe.transform('annual')).toBe('billing.checkout_total_year');
            })
        );

        it(
            'should return empty string if billing period is unknown',
            inject([BillingPeriodTotalPipe], billingPeriodTotalPipe => {
                expect(billingPeriodTotalPipe.transform('quarter')).toBe('');
            })
        );
    });

    describe('per billing period', () => {
        it(
            'should return month translation key',
            inject([PerBillingPeriodPipe], perBillingPeriodPipe => {
                expect(perBillingPeriodPipe.transform('month')).toBe('billing.period.per_month');
            })
        );

        it(
            'should return annual translation key',
            inject([PerBillingPeriodPipe], perBillingPeriodPipe => {
                expect(perBillingPeriodPipe.transform('annual')).toBe('billing.period.per_year');
            })
        );

        it(
            'should return empty string if billing period is unknown',
            inject([PerBillingPeriodPipe], perBillingPeriodPipe => {
                expect(perBillingPeriodPipe.transform('quarter')).toBe('');
            })
        );
    });

    describe('paid billing period', () => {
        it(
            'should return month translation key',
            inject([PaidBillingPeriodPipe], paidBillingPeriodPipe => {
                expect(paidBillingPeriodPipe.transform('month')).toBe('billing.overview.paid_monthly');
            })
        );

        it(
            'should return annual translation key',
            inject([PaidBillingPeriodPipe], paidBillingPeriodPipe => {
                expect(paidBillingPeriodPipe.transform('annual')).toBe('billing.overview.paid_yearly');
            })
        );

        it(
            'should return empty string if billing period is unknown',
            inject([PaidBillingPeriodPipe], paidBillingPeriodPipe => {
                expect(paidBillingPeriodPipe.transform('quarter')).toBe('');
            })
        );
    });
});
