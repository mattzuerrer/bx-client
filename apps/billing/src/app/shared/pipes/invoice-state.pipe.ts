import { Pipe, PipeTransform } from '@angular/core';

import { Invoice } from '../models';

@Pipe({ name: 'invoice_state', pure: true })
export class InvoiceStatePipe implements PipeTransform {
    transform(invoiceState: string): string {
        switch (invoiceState) {
            case Invoice.stateOpen:
                return 'billing.payments.status_open';
            case Invoice.statePaid:
                return 'billing.payments.status_paid';
            default:
                return invoiceState;
        }
    }
}
