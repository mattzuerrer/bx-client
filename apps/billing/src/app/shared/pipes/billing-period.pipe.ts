import { Pipe, PipeTransform } from '@angular/core';

import { Product } from '../models';

@Pipe({ name: 'billing_period_total', pure: true })
export class BillingPeriodTotalPipe implements PipeTransform {
    transform(billingPeriod: string): string {
        switch (billingPeriod) {
            case Product.billingPeriodMonth:
                return 'billing.checkout_total_month';
            case Product.billingPeriodAnnual:
                return 'billing.checkout_total_year';
            default:
                return '';
        }
    }
}

@Pipe({ name: 'per_billing_period', pure: true })
export class PerBillingPeriodPipe implements PipeTransform {
    transform(billingPeriod: string): string {
        switch (billingPeriod) {
            case Product.billingPeriodMonth:
                return 'billing.period.per_month';
            case Product.billingPeriodAnnual:
                return 'billing.period.per_year';
            default:
                return '';
        }
    }
}

@Pipe({ name: 'paid_billing_period', pure: true })
export class PaidBillingPeriodPipe implements PipeTransform {
    transform(billingPeriod: string): string {
        switch (billingPeriod) {
            case Product.billingPeriodMonth:
                return 'billing.overview.paid_monthly';
            case Product.billingPeriodAnnual:
                return 'billing.overview.paid_yearly';
            default:
                return '';
        }
    }
}
