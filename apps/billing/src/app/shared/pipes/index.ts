import { BillingPeriodTotalPipe, PaidBillingPeriodPipe, PerBillingPeriodPipe } from './billing-period.pipe';
import { InvoiceStatePipe } from './invoice-state.pipe';
import { AccountantProductOptionsPipe } from './product-option.pipe';

export const sharedPipes = [
    BillingPeriodTotalPipe,
    InvoiceStatePipe,
    PerBillingPeriodPipe,
    PaidBillingPeriodPipe,
    AccountantProductOptionsPipe
];
