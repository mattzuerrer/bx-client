import * as userActions from '../actions/user.action';
import { ColumnStatus, User } from '../models';

export interface State {
    collection: User[];
    columnConfig: ColumnStatus[];
    loaded: boolean;
    invitingUser: boolean;
    removingUser: boolean;
}

const setStatus = status => column => Object.assign(new ColumnStatus(), column, { status });

const setLoading = setStatus(ColumnStatus.statusLoading);

const initialState: State = {
    collection: [],
    columnConfig: [new ColumnStatus('lastname'), new ColumnStatus('id')],
    loaded: false,
    invitingUser: false,
    removingUser: false
};

const columnsReducer = (state, action: userActions.Actions): ColumnStatus[] => {
    switch (action.type) {
        case userActions.LOAD:
            return state.map(setLoading);
        case userActions.SORT:
            return [
                ...action.payload.map(columnStatus =>
                    Object.assign(new ColumnStatus(), columnStatus, {
                        status: ColumnStatus.statusLoading,
                        reversed: !columnStatus.reversed
                    })
                ),
                new ColumnStatus('id')
            ];
        case userActions.LOAD_SUCCESS:
        case userActions.LOAD_FAIL:
            return state.map(columnStatus => Object.assign(new ColumnStatus(), columnStatus, { status: columnStatus.direction }));
    }
};

export function reducer(state: State = initialState, action: userActions.Actions): State {
    switch (action.type) {
        case userActions.LOAD:
        case userActions.SORT:
            return Object.assign({}, state, { columnConfig: columnsReducer(state.columnConfig, action) });
        case userActions.LOAD_SUCCESS:
            return Object.assign({}, state, {
                collection: action.payload,
                columnConfig: columnsReducer(state.columnConfig, action),
                loaded: true
            });
        case userActions.LOAD_FAIL:
            return Object.assign({}, state, { loaded: true, columnConfig: columnsReducer(state.columnConfig, action) });
        case userActions.INVITE:
            return Object.assign({}, state, { invitingUser: true });
        case userActions.INVITE_SUCCESS:
            return Object.assign({}, state, { collection: [...state.collection, action.payload], invitingUser: false });
        case userActions.INVITE_FAIL:
            return Object.assign({}, state, { invitingUser: false });
        case userActions.REMOVE:
            return Object.assign({}, state, { removingUser: true });
        case userActions.REMOVE_FAIL:
            return Object.assign({}, state, { removingUser: false });
        case userActions.REMOVE_SUCCESS:
            return Object.assign({}, state, {
                collection: state.collection.filter(user => user.id !== action.payload),
                removingUser: false
            });
        default:
            return state;
    }
}

export const getCollection = (state: State) => state.collection;

export const getColumnConfig = (state: State) => state.columnConfig;

export const getLoaded = (state: State) => state.loaded;

export const getInvitingUser = (state: State) => state.invitingUser;

export const getRemovingUser = (state: State) => state.removingUser;
