import * as paymentHistoryActions from '../actions/payment-history.action';
import { Invoice } from '../models';

export interface State {
    collection: Invoice[];
    loaded: boolean;
}

const initialState: State = {
    collection: [],
    loaded: false
};

export function reducer(state: State = initialState, action: paymentHistoryActions.Actions): State {
    switch (action.type) {
        case paymentHistoryActions.LOAD:
            return Object.assign({}, state, { loaded: false });
        case paymentHistoryActions.LOAD_SUCCESS:
            return { collection: action.payload, loaded: true };
        case paymentHistoryActions.LOAD_FAIL:
            return Object.assign({}, state, { loaded: true });
        default:
            return state;
    }
}

export const getCollection = (state: State) => state.collection;

export const getLoaded = (state: State) => state.loaded;
