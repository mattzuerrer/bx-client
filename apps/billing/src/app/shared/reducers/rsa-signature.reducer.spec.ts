import * as rsaSignatureActions from '../actions/rsa-signature.action';
import { RsaSignature } from '../models';

import { getInstance, getLoaded, reducer } from './rsa-signature.reducer';

const initialState = {
    instance: null,
    loaded: false
};

const formParams = {
    style: 'inline',
    submitEnabled: 'true'
};

describe('RSA signature reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return existing state by default', () => {
        const action = new rsaSignatureActions.LoadAction();
        expect(reducer(initialState, action)).toBe(initialState);
    });

    it('should return new state on LOAD_SUCCESS', () => {
        const signature = new RsaSignature();
        const resultSignature = Object.assign(new RsaSignature(), signature, formParams);
        const action = new rsaSignatureActions.LoadSuccessAction(signature);
        expect(reducer(initialState, action).instance).toEqual(resultSignature);
        expect(reducer(initialState, action).loaded).toBe(true);
    });

    it('should return initial state on REMOVE', () => {
        const signature = new RsaSignature();
        const action = new rsaSignatureActions.RemoveAction();
        expect(reducer({ instance: signature, loaded: true }, action)).toEqual(initialState);
    });

    it('should return initial state on LOAD_FAIL', () => {
        const signature = new RsaSignature();
        const action = new rsaSignatureActions.LoadFailAction();
        expect(reducer({ instance: signature, loaded: true }, action)).toEqual(initialState);
    });

    it('should have substates', () => {
        expect(getInstance(initialState)).toBe(initialState.instance);
        expect(getLoaded(initialState)).toBe(initialState.loaded);
    });
});
