import * as addonActions from '../actions/addon.action';
import * as checkoutActions from '../actions/checkout.action';
import * as subscriptionInfoActions from '../actions/subscription-info.action';
import { Addon, CompanyProfile, CreditCard, PaymentMethod, Plan, SubscriptionInfo } from '../models';

import {
    getInstance,
    getInstanceAddons,
    getInstanceBillingInfo,
    getInstanceIsTrial,
    getInstancePaymentMethod,
    getInstancePlan,
    getInstancePlanUser,
    getLoaded,
    reducer
} from './subscription-info.reducer';

const initialState = {
    instance: new SubscriptionInfo(),
    loaded: false
};

const mailXpertAddon = Object.assign(new Addon(), { key: 'addon_mailxpert' });

const mailXpertAddonMapped = Object.assign(new Addon(), mailXpertAddon, {
    logoUrl: 'assets/images/addons/mailxpert.png',
    instructionsUrl: {
        de: 'https://support.bexio.com/hc/de/articles/206488638-Was-muss-ich-%C3%BCber-mailXpert-wissen-',
        en: 'https://support.bexio.com/hc/en-us/articles/206488638-What-do-I-need-to-know-about-mailXpert-',
        fr: 'https://support.bexio.com/hc/fr/articles/206488638-Que-dois-je-savoir-sur-mailXpert-'
    }
});

const subscriptionInfo = new SubscriptionInfo();

const subscriptionInfoPro = Object.assign(new SubscriptionInfo(), {
    plan: Object.assign(new Plan(), { key: 'plan_pro' })
});

const subscriptionInfoProPlus = Object.assign(new SubscriptionInfo(), {
    plan: Object.assign(new Plan(), { key: 'plan_pro_plus' })
});

const subscriptionInfoAddons = Object.assign(new SubscriptionInfo(), { addons: [mailXpertAddon] });

describe('SubscriptionInfo reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return initial state on LOADING', () => {
        const action = new subscriptionInfoActions.LoadAction();
        expect(reducer(initialState, action).loaded).toBe(initialState.loaded);
    });

    it('should set subscription info on LOAD_SUCCESS', () => {
        const action = new subscriptionInfoActions.LoadSuccessAction(subscriptionInfo);
        expect(reducer(initialState, action).loaded).toBeTruthy();
        expect(reducer(initialState, action).instance).toEqual(subscriptionInfo);
    });

    it('should apply plan mappings on LOAD_SUCCESS', () => {
        let action = new subscriptionInfoActions.LoadSuccessAction(subscriptionInfoPro);
        expect(reducer(initialState, action).instance.plan.includedProductOptions).toEqual(['addon_banking']);
        action = new subscriptionInfoActions.LoadSuccessAction(subscriptionInfoProPlus);
        expect(reducer(initialState, action).instance.plan.includedProductOptions).toEqual(['addon_banking', 'addon_payroll']);
    });

    it('should apply addons mappings on LOAD_SUCCESS', () => {
        const action = new subscriptionInfoActions.LoadSuccessAction(subscriptionInfoAddons);
        expect(reducer(initialState, action).instance.addons).toEqual([mailXpertAddonMapped]);
    });

    it('should set subscription info on UPDATE_PLAN_SUCCESS', () => {
        const action = new subscriptionInfoActions.UpdatePlanSuccessAction(subscriptionInfo);
        expect(reducer(initialState, action).loaded).toBeTruthy();
        expect(reducer(initialState, action).instance).toEqual(subscriptionInfo);
    });

    it('should apply plan mappings on UPDATE_PLAN_SUCCESS', () => {
        let action = new subscriptionInfoActions.UpdatePlanSuccessAction(subscriptionInfoPro);
        expect(reducer(initialState, action).instance.plan.includedProductOptions).toEqual(['addon_banking']);
        action = new subscriptionInfoActions.UpdatePlanSuccessAction(subscriptionInfoProPlus);
        expect(reducer(initialState, action).instance.plan.includedProductOptions).toEqual(['addon_banking', 'addon_payroll']);
    });

    it('should set billing address on UPDATE_BILLING_ADDRESS_SUCCESS', () => {
        const billingInfo = new CompanyProfile();
        const action = new subscriptionInfoActions.UpdateBillingAddressSuccessAction(billingInfo);
        expect(reducer(initialState, action).loaded).toBeFalsy();
        expect(reducer(initialState, action).instance.billingInfo).toBe(billingInfo);
    });

    it('should add addon on ACTIVATE', () => {
        const addon = new Addon();
        const action = new addonActions.ActivateAction(addon);
        expect(reducer(initialState, action).loaded).toBe(initialState.loaded);
        expect(reducer(initialState, action).instance.addons).toEqual([addon]);
    });

    it('should remove addon on DEACTIVATE', () => {
        const addon1 = Object.assign(new Addon(), { key: 'addon1' });
        const addon2 = Object.assign(new Addon(), { key: 'addon2' });
        const addons = [addon1, addon2];
        const instance = Object.assign(new SubscriptionInfo(), { addons });
        const state = { instance, loaded: true };
        const action = new addonActions.DeactivateAction(addon1);
        expect(reducer(state, action).loaded).toBe(state.loaded);
        expect(reducer(state, action).instance.addons).toEqual([addon2]);
    });

    it('should set credit card info on LOAD_CREDIT_CARD_SUCCESS', () => {
        const creditCard = new CreditCard();
        const action = new subscriptionInfoActions.LoadCreditCardSuccessAction(creditCard);
        expect(reducer(initialState, action).loaded).toBe(initialState.loaded);
        expect(reducer(initialState, action).instance.paymentMethod.creditCard).toBe(creditCard);
    });

    it('should set payment method on UPDATE_PAYMENT_METHOD_SUCCESS but keep credit card', () => {
        const creditCard = Object.assign(new CreditCard(), { refId: 'abc123' });
        const paymentMethod = new PaymentMethod();
        const action = new subscriptionInfoActions.UpdatePaymentMethodSuccessAction(paymentMethod);
        const instance = Object.assign(new SubscriptionInfo(), {
            paymentMethod: Object.assign(new PaymentMethod(), { creditCard })
        });
        const state = Object.assign({}, initialState, { instance });
        expect(reducer(state, action).loaded).toBe(state.loaded);
        expect(reducer(state, action).instance.paymentMethod).toEqual(Object.assign(new PaymentMethod(), paymentMethod, { creditCard }));
    });

    it('should set subscription info on CHECKOUT_SUCCESS', () => {
        const action = new checkoutActions.CheckoutSuccessAction(subscriptionInfo);
        expect(reducer(initialState, action).loaded).toBe(initialState.loaded);
        expect(reducer(initialState, action).instance).toEqual(subscriptionInfo);
    });

    it('should have substates', () => {
        expect(getInstance(initialState)).toBe(initialState.instance);
        expect(getLoaded(initialState)).toBe(initialState.loaded);
        expect(getInstanceIsTrial(initialState.instance)).toBe(initialState.instance.isTrial);
        expect(getInstancePlan(initialState.instance)).toBe(initialState.instance.plan);
        expect(getInstanceAddons(initialState.instance)).toBe(initialState.instance.addons);
        expect(getInstanceBillingInfo(initialState.instance)).toBe(initialState.instance.billingInfo);
        expect(getInstancePaymentMethod(initialState.instance)).toBe(initialState.instance.paymentMethod);
        expect(getInstancePlanUser(initialState.instance.plan)).toBe(initialState.instance.plan.user);
    });
});
