import * as catalogActions from '../actions/product-catalog.action';
import { Addon, Plan, ProductCatalog } from '../models';

import { getCatalogAddons, getCatalogPlans, getInstance, getLoaded, reducer } from './catalog.reducer';

const initialState = {
    instance: new ProductCatalog(),
    loaded: false
};

const planPro = Object.assign(new Plan(), { key: 'plan_pro' });

const planProMapped = Object.assign(new Plan(), planPro, { includedProductOptions: ['addon_banking'] });

const planProPlus = Object.assign(new Plan(), { key: 'plan_pro_plus' });

const planProPlusMapped = Object.assign(new Plan(), planProPlus, {
    includedProductOptions: ['addon_banking', 'addon_payroll']
});

const planCustom = Object.assign(new Plan(), { key: 'plan_custom' });

const mailXpertAddon = Object.assign(new Addon(), { key: 'addon_mailxpert' });

const mailXpertAddonMapped = Object.assign(new Addon(), mailXpertAddon, {
    logoUrl: 'assets/images/addons/mailxpert.png',
    instructionsUrl: {
        de: 'https://support.bexio.com/hc/de/articles/206488638-Was-muss-ich-%C3%BCber-mailXpert-wissen-',
        en: 'https://support.bexio.com/hc/en-us/articles/206488638-What-do-I-need-to-know-about-mailXpert-',
        fr: 'https://support.bexio.com/hc/fr/articles/206488638-Que-dois-je-savoir-sur-mailXpert-'
    }
});

describe('Product catalog reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return existing state by default', () => {
        const action = new catalogActions.LoadAction();
        expect(reducer(initialState, action)).toBe(initialState);
    });

    it('should return loaded true on LOAD_FAIL', () => {
        const catalog = new ProductCatalog();
        const action = new catalogActions.LoadFailAction();
        expect(reducer({ instance: catalog, loaded: false }, action).instance).toBe(catalog);
        expect(reducer({ instance: catalog, loaded: false }, action).loaded).toBe(true);
    });

    it('should return new state on LOAD_SUCCESS', () => {
        const catalog = new ProductCatalog();
        const action = new catalogActions.LoadSuccessAction(catalog);
        expect(reducer(initialState, action).instance).toEqual(catalog);
        expect(reducer(initialState, action).loaded).toBe(true);
    });

    it('should filter out custom plan, apply plan mappings and sort plans on LOAD_SUCCESS', () => {
        const catalog = Object.assign(new ProductCatalog(), { plans: [planProPlus, planPro, planCustom] });
        const action = new catalogActions.LoadSuccessAction(catalog);
        expect(reducer(initialState, action).instance.plans).toEqual([planProMapped, planProPlusMapped]);
    });

    it('should apply addons mappings on LOAD_SUCCESS', () => {
        const catalog = Object.assign(new ProductCatalog(), { addons: [mailXpertAddon] });
        const action = new catalogActions.LoadSuccessAction(catalog);
        expect(reducer(initialState, action).instance.addons).toEqual([mailXpertAddonMapped]);
    });

    it('should have substates', () => {
        expect(getInstance(initialState)).toBe(initialState.instance);
        expect(getLoaded(initialState)).toBe(initialState.loaded);
        expect(getCatalogPlans(initialState.instance)).toBe(initialState.instance.plans);
        expect(getCatalogAddons(initialState.instance)).toBe(initialState.instance.addons);
    });
});
