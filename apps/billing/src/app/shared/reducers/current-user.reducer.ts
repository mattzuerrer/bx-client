import * as userActions from '../actions/user.action';
import { User } from '../models';

export interface State {
    instance: User;
    loaded: boolean;
}

const initialState: State = {
    instance: new User(),
    loaded: false
};

export function reducer(state: State = initialState, action: userActions.Actions): State {
    switch (action.type) {
        case userActions.FETCH_CURRENT_SUCCESS:
            return { instance: action.payload, loaded: true };
        case userActions.FETCH_CURRENT_FAIL:
            return Object.assign({}, state, { loaded: true });
        default:
            return state;
    }
}

export const getInstance = (state: State) => state.instance;

export const getLoaded = (state: State) => state.loaded;
