import * as pageActions from '../actions/page.action';
import { Breadcrumb } from '../models';

import { getBreadcrumbs, getLoaded, reducer } from './page.reducer';

const initialState = {
    loaded: true,
    forceLoading: false,
    breadcrumbs: []
};

const forceLoadingState = {
    loaded: true,
    forceLoading: true,
    breadcrumbs: [new Breadcrumb()]
};

describe('Routing reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return initial state on LOADING', () => {
        const action = new pageActions.LoadingAction();
        expect(reducer(initialState, action).loaded).toBeFalsy();
    });

    it('should set loaded to true on IDLE', () => {
        const action = new pageActions.IdleAction();
        expect(reducer(initialState, action).loaded).toBeTruthy();
    });

    it('should set force loading to true on FORCE_LOADING', () => {
        const action = new pageActions.ForceLoadingAction();
        expect(reducer(initialState, action).forceLoading).toBeTruthy();
    });

    it('should set breadcrumbs SET_BREADCRUMBS', () => {
        const breadCrumbs = [];
        const action = new pageActions.SetBreadcrumbsAction(breadCrumbs);
        expect(reducer(initialState, action).breadcrumbs).toBe(breadCrumbs);
    });

    it('should reset breadcrumbs state on CLEAR_BREADCRUMBS', () => {
        const action = new pageActions.ClearBreadcrumbsAction();
        expect(reducer(forceLoadingState, action).breadcrumbs).toEqual([]);
    });

    it('should have substates', () => {
        expect(getLoaded(initialState)).toBeTruthy();
        expect(getLoaded(forceLoadingState)).toBeFalsy();
        expect(getBreadcrumbs(forceLoadingState)).toEqual(forceLoadingState.breadcrumbs);
    });
});
