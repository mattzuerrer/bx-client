import * as paymentHistoryActions from '../actions/payment-history.action';

import { getCollection, getLoaded, reducer } from './payment-history.reducer';

const initialState = {
    collection: [],
    loaded: false
};

describe('Payment history reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return loaded false on LOAD', () => {
        const action = new paymentHistoryActions.LoadAction();
        expect(reducer(initialState, action).collection).toBe(initialState.collection);
        expect(reducer(initialState, action).loaded).toBe(false);
    });

    it('should return loaded true on LOAD_FAIL', () => {
        const action = new paymentHistoryActions.LoadFailAction();
        expect(reducer(initialState, action).collection).toBe(initialState.collection);
        expect(reducer(initialState, action).loaded).toBe(true);
    });

    it('should return new state on LOAD_SUCCESS', () => {
        const collection = [];
        const action = new paymentHistoryActions.LoadSuccessAction(collection);
        expect(reducer(initialState, action).collection).toBe(collection);
        expect(reducer(initialState, action).loaded).toBe(true);
    });

    it('should have substates', () => {
        expect(getCollection(initialState)).toBe(initialState.collection);
        expect(getLoaded(initialState)).toBe(initialState.loaded);
    });
});
