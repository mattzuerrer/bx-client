import { routerReducer, RouterReducerState } from '@ngrx/router-store';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

import { featureName } from '../config';

import * as fromAddons from './addons.reducer';
import * as fromCatalog from './catalog.reducer';
import * as fromCheckout from './checkout.reducer';
import * as fromCompany from './company.reducer';
import * as fromCountries from './countries.reducer';
import * as fromCurrentUser from './current-user.reducer';
import * as fromOverview from './overview.reducer';
import * as fromPage from './page.reducer';
import * as fromPaymentHistory from './payment-history.reducer';
import * as fromRsaSignature from './rsa-signature.reducer';
import * as fromSubscriptionInfo from './subscription-info.reducer';
import * as fromUsers from './users.reducer';

export interface State {
    router: RouterReducerState;
    page: fromPage.State;
    users: fromUsers.State;
    currentUser: fromCurrentUser.State;
    catalog: fromCatalog.State;
    company: fromCompany.State;
    countries: fromCountries.State;
    checkout: fromCheckout.State;
    rsaSignature: fromRsaSignature.State;
    subscriptionInfo: fromSubscriptionInfo.State;
    paymentHistory: fromPaymentHistory.State;
    overview: fromOverview.State;
    addons: fromAddons.State;
}

export const reducers: ActionReducerMap<State> = {
    router: routerReducer,
    page: fromPage.reducer,
    users: fromUsers.reducer,
    currentUser: fromCurrentUser.reducer,
    catalog: fromCatalog.reducer,
    company: fromCompany.reducer,
    countries: fromCountries.reducer,
    checkout: fromCheckout.reducer,
    rsaSignature: fromRsaSignature.reducer,
    subscriptionInfo: fromSubscriptionInfo.reducer,
    paymentHistory: fromPaymentHistory.reducer,
    overview: fromOverview.reducer,
    addons: fromAddons.reducer
};

export const getState = createFeatureSelector<State>(featureName);

/**
 * Overview
 */
export const getOverviewState = createSelector(getState, (state: State) => state.overview);

export const getOverviewPreview = createSelector(getOverviewState, fromOverview.getPreview);

export const getOverviewPreviewLoaded = createSelector(getOverviewState, fromOverview.getPreviewLoaded);

export const getOverviewPaymentMethod = createSelector(getOverviewState, fromOverview.getPaymentMethod);

export const getOverviewPlan = createSelector(getOverviewState, fromOverview.getPlan);

export const getOverviewProductOptions = createSelector(getOverviewState, fromOverview.getProductOptions);

export const getIsOverviewProductOptionsSelectCountValid = createSelector(
    getOverviewState,
    fromOverview.getIsOverviewProductOptionsSelectCountValid
);

export const getOverviewProductOptionsAttributes = createSelector(getOverviewState, fromOverview.getProductOptionsAttributes);

export const getOverviewSubscriptionChanged = createSelector(getOverviewState, fromOverview.getSubscriptionChanged);

export const getOverviewChangePlanLoading = createSelector(getOverviewState, fromOverview.getPlanChangeLoading);

/**
 * Page
 */
export const getPageState = createSelector(getState, (state: State) => state.page);

export const getPageLoaded = createSelector(getPageState, fromPage.getLoaded);

export const getPageBreadcrumbs = createSelector(getPageState, fromPage.getBreadcrumbs);

/**
 * Users
 */
export const getUsersState = createSelector(getState, (state: State) => state.users);

export const getUsersCollection = createSelector(getUsersState, fromUsers.getCollection);

export const getUsersLoaded = createSelector(getUsersState, fromUsers.getLoaded);

export const getUsersInvitingUser = createSelector(getUsersState, fromUsers.getInvitingUser);

export const getUsersRemovingUser = createSelector(getUsersState, fromUsers.getRemovingUser);

export const getUsersColumnConfig = createSelector(getUsersState, fromUsers.getColumnConfig);

/**
 * Current user
 */
export const getCurrentUserState = createSelector(getState, (state: State) => state.currentUser);

export const getCurrentUserInstance = createSelector(getCurrentUserState, fromCurrentUser.getInstance);

export const getCurrentUserLoaded = createSelector(getCurrentUserState, fromCurrentUser.getLoaded);

/**
 * Catalog
 */
export const getCatalogState = createSelector(getState, (state: State) => state.catalog);

export const getCatalogInstance = createSelector(getCatalogState, fromCatalog.getInstance);

export const getCatalogLoaded = createSelector(getCatalogState, fromCatalog.getLoaded);

export const getCatalogPlans = createSelector(getCatalogInstance, fromCatalog.getCatalogPlans);

export const getCatalogAddons = createSelector(getCatalogInstance, fromCatalog.getCatalogAddons);

export const getCatalogPaidAddons = createSelector(getCatalogAddons, fromCatalog.getPaidAddons);

/**
 * Company
 */
export const getCompanyState = createSelector(getState, (state: State) => state.company);

export const getCompanyProfile = createSelector(getCompanyState, fromCompany.getProfile);

export const getCompanyLoaded = createSelector(getCompanyState, fromCompany.getLoaded);

/**
 * Countries
 */
export const getCountriesState = createSelector(getState, (state: State) => state.countries);

export const getCountriesCollection = createSelector(getCountriesState, fromCountries.getCollection);

export const getCountriesLoaded = createSelector(getCountriesState, fromCountries.getLoaded);

/**
 * Checkout
 */
export const getCheckoutState = createSelector(getState, (state: State) => state.checkout);

export const getCheckoutInstance = createSelector(getCheckoutState, fromCheckout.getInstance);

export const getCheckoutInstancePlan = createSelector(getCheckoutInstance, fromCheckout.getInstancePlan);

export const getCheckoutInstanceBillingInfo = createSelector(getCheckoutInstance, fromCheckout.getInstanceBillingInfo);

export const getCheckoutInstancePaymentMethod = createSelector(getCheckoutInstance, fromCheckout.getInstancePaymentMethod);

export const getCheckoutInstanceVoucher = createSelector(getCheckoutInstance, fromCheckout.getInstanceVoucher);

export const getCheckoutInstanceProductOptions = createSelector(getCheckoutInstance, fromCheckout.getInstanceProductOptions);

export const getCheckoutInstanceProductOptionsAtributes = createSelector(
    getCheckoutInstance,
    fromCheckout.getInstanceProductOptionsAttributes
);

export const getIsCheckoutProductOptionsSelectCountValid = createSelector(
    getCheckoutInstance,
    fromCheckout.getIsCheckoutProductOptionsSelectCountValid
);

export const getCheckoutPreview = createSelector(getCheckoutState, fromCheckout.getPreview);

export const getCheckoutPreviewLoaded = createSelector(getCheckoutState, fromCheckout.getPreviewLoaded);

export const getCheckoutRequestProcessing = createSelector(getCheckoutState, fromCheckout.getRequestProcessing);

/**
 * RSA signature
 */
export const getRsaSignatureState = createSelector(getState, (state: State) => state.rsaSignature);

export const getRsaSignatureInstance = createSelector(getRsaSignatureState, fromRsaSignature.getInstance);

export const getRsaSignatureLoaded = createSelector(getRsaSignatureState, fromRsaSignature.getLoaded);

/**
 * SubscriptionInfo
 */
export const getSubscriptionInfoState = createSelector(getState, (state: State) => state.subscriptionInfo);

export const getSubscriptionInfoInstance = createSelector(getSubscriptionInfoState, fromSubscriptionInfo.getInstance);

export const getSubscriptionInfoLoaded = createSelector(getSubscriptionInfoState, fromSubscriptionInfo.getLoaded);

export const getSubscriptionInfoInstanceIsTrial = createSelector(getSubscriptionInfoInstance, fromSubscriptionInfo.getInstanceIsTrial);

export const getSubscriptionInfoInstancePlan = createSelector(getSubscriptionInfoInstance, fromSubscriptionInfo.getInstancePlan);

export const getSubscriptionInfoInstanceAddons = createSelector(getSubscriptionInfoInstance, fromSubscriptionInfo.getInstanceAddons);

export const getSubscriptionInfoInstancePaidAddons = createSelector(getSubscriptionInfoInstanceAddons, fromSubscriptionInfo.getPaidAddons);

export const getSubscriptionInfoInstanceBillingInfo = createSelector(
    getSubscriptionInfoInstance,
    fromSubscriptionInfo.getInstanceBillingInfo
);

export const getSubscriptionInfoInstancePaymentMethod = createSelector(
    getSubscriptionInfoInstance,
    fromSubscriptionInfo.getInstancePaymentMethod
);

export const getSubscriptionInfoInstancePayrollInfo = createSelector(
    getSubscriptionInfoInstance,
    fromSubscriptionInfo.getInstancePayrollInfo
);

export const getSubscriptionInfoInstancePlanUser = createSelector(
    getSubscriptionInfoInstancePlan,
    fromSubscriptionInfo.getInstancePlanUser
);

export const getSubscriptionInfoInstancePaymentMethodCreditCard = createSelector(
    getSubscriptionInfoInstancePaymentMethod,
    fromSubscriptionInfo.getInstancePaymentMethodCreditCard
);

/**
 * Payment history
 */

export const getPaymentHistoryState = createSelector(getState, (state: State) => state.paymentHistory);

export const getPaymentHistoryCollection = createSelector(getPaymentHistoryState, fromPaymentHistory.getCollection);

export const getPaymentHistoryLoaded = createSelector(getPaymentHistoryState, fromPaymentHistory.getLoaded);

/**
 * Addons reducer
 */
export const getAddonsState = createSelector(getState, (state: State) => state.addons);

export const getAddonsLoadingList = createSelector(getAddonsState, fromAddons.getLoadingList);
