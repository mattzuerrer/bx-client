import * as countriesActions from '../actions/countries.action';
import * as fixtures from '../fixtures';
import { Country } from '../models/Country';

import { getCollection, getLoaded, reducer } from './countries.reducer';

const initialState = {
    collection: [],
    loaded: false
};

describe('Countries reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return existing state by default', () => {
        const action = new countriesActions.LoadAction();
        expect(reducer(initialState, action)).toBe(initialState);
    });

    it('should return loaded true on LOAD_FAIL', () => {
        const action = new countriesActions.LoadFailAction();
        expect(reducer(initialState, action).collection).toBe(initialState.collection);
        expect(reducer(initialState, action).loaded).toBe(true);
    });

    it('should return new state on LOAD_SUCCESS', () => {
        const collection = fixtures.countriesTransformed.map(country => Object.assign(new Country(), country));
        const filteredCollection = collection.filter(country => ['US', 'CA'].indexOf(country.id) === -1);
        const action = new countriesActions.LoadSuccessAction(collection);
        expect(reducer(initialState, action).collection).toEqual(filteredCollection);
        expect(reducer(initialState, action).loaded).toBe(true);
    });

    it('should have substates', () => {
        expect(getCollection(initialState)).toBe(initialState.collection);
        expect(getLoaded(initialState)).toBe(initialState.loaded);
    });
});
