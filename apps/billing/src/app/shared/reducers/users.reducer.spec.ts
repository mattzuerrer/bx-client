import * as userActions from '../actions/user.action';
import { ColumnStatus, User } from '../models';

import { getCollection, getColumnConfig, getLoaded, reducer } from './users.reducer';

const setStatus = status => column => Object.assign(new ColumnStatus(), column, { status });

const setLoading = setStatus(ColumnStatus.statusLoading);

const initialColumnConfig = [new ColumnStatus('lastname'), new ColumnStatus('id')];

const loadingLastnameColumnConfig = initialColumnConfig.map(setLoading);

const initialState = {
    collection: [],
    columnConfig: initialColumnConfig,
    loaded: false,
    invitingUser: false,
    removingUser: false
};

describe('Users reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should set column state to loading on LOAD', () => {
        const action = new userActions.LoadAction();
        expect(reducer(initialState, action).collection).toBe(initialState.collection);
        expect(reducer(initialState, action).columnConfig).toEqual(loadingLastnameColumnConfig);
        expect(reducer(initialState, action).loaded).toBe(initialState.loaded);
    });

    it('should set sorted column state to loading on SORT', () => {
        const emailColumn = new ColumnStatus('email');
        const action = new userActions.SortAction([emailColumn]);
        expect(reducer(initialState, action).collection).toBe(initialState.collection);
        expect(reducer(initialState, action).columnConfig).toEqual([
            Object.assign(setLoading(emailColumn), { reversed: true }),
            new ColumnStatus('id')
        ]);
        expect(reducer(initialState, action).loaded).toBe(initialState.loaded);
    });

    it('should set new state on LOAD_SUCCESS', () => {
        const collection = [];
        const action = new userActions.LoadSuccessAction(collection);
        const columnConfig = [
            Object.assign(new ColumnStatus('lastname'), { status: 'desc' }),
            Object.assign(new ColumnStatus('id'), { status: 'desc' })
        ];
        expect(reducer(initialState, action).collection).toBe(collection);
        expect(reducer(initialState, action).columnConfig).toEqual(columnConfig);
        expect(reducer(initialState, action).loaded).toBe(true);
    });

    it('should reset column states status on LOAD_FAIL', () => {
        const action = new userActions.LoadFailAction();
        const columnConfig = [
            Object.assign(new ColumnStatus('lastname'), { status: 'desc' }),
            Object.assign(new ColumnStatus('id'), { status: 'desc' })
        ];
        expect(reducer(initialState, action).collection).toBe(initialState.collection);
        expect(reducer(initialState, action).columnConfig).toEqual(columnConfig);
        expect(reducer(initialState, action).loaded).toBe(true);
    });

    it('should set invitingUser to true on INVITE', () => {
        const action = new userActions.InviteAction('');
        expect(reducer(initialState, action).collection).toBe(initialState.collection);
        expect(reducer(initialState, action).columnConfig).toBe(initialState.columnConfig);
        expect(reducer(initialState, action).loaded).toBe(initialState.loaded);
        expect(reducer(initialState, action).invitingUser).toBe(true);
    });

    it('should set invitingUser to false on INVITE_FAIL', () => {
        const action = new userActions.InviteFailAction({ reason: '', retryAction: null });
        expect(reducer(initialState, action).collection).toBe(initialState.collection);
        expect(reducer(initialState, action).columnConfig).toBe(initialState.columnConfig);
        expect(reducer(initialState, action).loaded).toBe(initialState.loaded);
        expect(reducer(initialState, action).invitingUser).toBe(false);
    });

    it('should add new user on INVITE_SUCCESS', () => {
        const user = new User();
        const action = new userActions.InviteSuccessAction(user);
        expect(reducer(initialState, action).collection).toEqual([...initialState.collection, user]);
        expect(reducer(initialState, action).columnConfig).toBe(initialState.columnConfig);
        expect(reducer(initialState, action).loaded).toBe(initialState.loaded);
        expect(reducer(initialState, action).invitingUser).toBe(false);
    });

    it('should set removingUser to true on REMOVE', () => {
        const action = new userActions.RemoveAction(101);
        expect(reducer(initialState, action).collection).toBe(initialState.collection);
        expect(reducer(initialState, action).columnConfig).toBe(initialState.columnConfig);
        expect(reducer(initialState, action).loaded).toBe(initialState.loaded);
        expect(reducer(initialState, action).removingUser).toBe(true);
    });

    it('should set removingUser to false on REMOVE_FAIL', () => {
        const action = new userActions.RemoveFailAction(101);
        expect(reducer(initialState, action).collection).toBe(initialState.collection);
        expect(reducer(initialState, action).columnConfig).toBe(initialState.columnConfig);
        expect(reducer(initialState, action).loaded).toBe(initialState.loaded);
        expect(reducer(initialState, action).removingUser).toBe(false);
    });

    it('should remove user on REMOVE_SUCCESS', () => {
        const collection = [Object.assign(new User(), { id: 1 }), Object.assign(new User(), { id: 2 })];
        const action = new userActions.RemoveSuccessAction(1);
        const filledState = Object.assign({}, initialState, { collection });
        expect(reducer(filledState, action).collection).toEqual([Object.assign(new User(), { id: 2 })]);
        expect(reducer(filledState, action).columnConfig).toBe(filledState.columnConfig);
        expect(reducer(filledState, action).loaded).toBe(filledState.loaded);
        expect(reducer(initialState, action).removingUser).toBe(false);
    });

    it('should have substates', () => {
        expect(getCollection(initialState)).toBe(initialState.collection);
        expect(getColumnConfig(initialState)).toBe(initialState.columnConfig);
        expect(getLoaded(initialState)).toBe(initialState.loaded);
    });
});
