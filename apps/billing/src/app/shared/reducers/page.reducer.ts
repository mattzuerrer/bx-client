import * as pageActions from '../actions/page.action';
import { Breadcrumb } from '../models';

export interface State {
    loaded: boolean;
    forceLoading: boolean;
    breadcrumbs: Breadcrumb[];
}

const initialState: State = {
    loaded: true,
    forceLoading: false,
    breadcrumbs: []
};

export function reducer(state: State = initialState, action: pageActions.Actions): State {
    switch (action.type) {
        case pageActions.SET_BREADCRUMBS:
            return Object.assign({}, state, { breadcrumbs: action.payload });
        case pageActions.CLEAR_BREADCRUMBS:
            return Object.assign({}, state, { breadcrumbs: [] });
        case pageActions.LOADING:
            return Object.assign({}, state, { loaded: false });
        case pageActions.IDLE:
            return Object.assign({}, state, { loaded: true });
        case pageActions.FORCE_LOADING:
            return Object.assign({}, state, { loaded: false, forceLoading: true });
        default:
            return state;
    }
}

export const getLoaded = (state: State) => state.loaded && !state.forceLoading;

export const getBreadcrumbs = (state: State) => state.breadcrumbs;
