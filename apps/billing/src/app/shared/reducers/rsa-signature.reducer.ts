import * as rsaSignatureActions from '../actions/rsa-signature.action';
import { RsaSignature } from '../models';

export interface State {
    instance: RsaSignature;
    loaded: boolean;
}

const initialState: State = {
    instance: null,
    loaded: false
};

const formParams = {
    style: 'inline',
    submitEnabled: 'true'
};

export function reducer(state: State = initialState, action: rsaSignatureActions.Actions): State {
    switch (action.type) {
        case rsaSignatureActions.LOAD_SUCCESS:
            const instance = Object.assign(new RsaSignature(), action.payload, formParams);
            return { instance, loaded: true };
        case rsaSignatureActions.LOAD_FAIL:
        case rsaSignatureActions.REMOVE:
            return initialState;
        default:
            return state;
    }
}

export const getInstance = (state: State) => state.instance;

export const getLoaded = (state: State) => state.loaded;
