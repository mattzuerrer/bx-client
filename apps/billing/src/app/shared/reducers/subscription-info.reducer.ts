import { addonsMapping, plansMapping } from '../../../config';
import * as addonActions from '../actions/addon.action';
import * as checkoutActions from '../actions/checkout.action';
import * as subscriptionInfoActions from '../actions/subscription-info.action';
import { Addon, PaymentMethod, Plan, SubscriptionInfo } from '../models';

export interface State {
    instance: SubscriptionInfo;
    loaded: boolean;
}

const initialState: State = {
    instance: new SubscriptionInfo(),
    loaded: false
};

const applyPlanConfig = plan => Object.assign(new Plan(), plan, plansMapping[plan.key]);

const applyAddonConfig = addon => Object.assign(new Addon(), addon, addonsMapping.find(addonMapping => addonMapping.key === addon.key));

const instanceReducer = (state, action: subscriptionInfoActions.Actions | addonActions.Actions): SubscriptionInfo => {
    switch (action.type) {
        case subscriptionInfoActions.UPDATE_BILLING_ADDRESS_SUCCESS:
            const billingInfo = action.payload;
            return Object.assign(new SubscriptionInfo(), state, { billingInfo });
        case addonActions.ACTIVATE:
        case addonActions.DEACTIVATE_FAIL:
            return Object.assign(new SubscriptionInfo(), state, { addons: [...state.addons, action.payload] });
        case addonActions.DEACTIVATE:
        case addonActions.ACTIVATE_FAIL:
            return Object.assign(new SubscriptionInfo(), state, {
                addons: state.addons.filter(addon => addon.key !== action.payload.key)
            });
        case subscriptionInfoActions.LOAD_CREDIT_CARD_SUCCESS:
            return Object.assign(new SubscriptionInfo(), state, {
                paymentMethod: Object.assign(new PaymentMethod(), state.paymentMethod, { creditCard: action.payload })
            });
        case subscriptionInfoActions.UPDATE_PAYMENT_METHOD_SUCCESS:
            const paymentMethod = Object.assign(new PaymentMethod(), action.payload, {
                creditCard: state.paymentMethod.creditCard
            });
            return Object.assign(new SubscriptionInfo(), state, { paymentMethod });
    }
};

export function reducer(
    state: State = initialState,
    action: subscriptionInfoActions.Actions | addonActions.Actions | checkoutActions.Actions
): State {
    switch (action.type) {
        case subscriptionInfoActions.LOAD_SUCCESS:
        case subscriptionInfoActions.UPDATE_PLAN_SUCCESS:
            const subscriptionInfo = action.payload;
            return {
                instance: Object.assign(new SubscriptionInfo(), action.payload, {
                    plan: applyPlanConfig(subscriptionInfo.plan),
                    addons: subscriptionInfo.addons.map(applyAddonConfig)
                }),
                loaded: true
            };
        case subscriptionInfoActions.UPDATE_BILLING_ADDRESS_SUCCESS:
        case addonActions.ACTIVATE:
        case addonActions.DEACTIVATE:
        case addonActions.ACTIVATE_FAIL:
        case addonActions.DEACTIVATE_FAIL:
        case subscriptionInfoActions.LOAD_CREDIT_CARD_SUCCESS:
        case subscriptionInfoActions.UPDATE_PAYMENT_METHOD_SUCCESS:
            return Object.assign({}, state, { instance: instanceReducer(state.instance, action) });
        case subscriptionInfoActions.LOAD:
            return Object.assign({}, state, { loaded: false });
        case checkoutActions.CHECKOUT_SUCCESS:
            return Object.assign({}, state, { instance: action.payload });
        default:
            return state;
    }
}

export const getInstance = (state: State) => state.instance;

export const getLoaded = (state: State) => state.loaded;

export const getInstanceIsTrial = (subscriptionInfo: SubscriptionInfo) => subscriptionInfo.isTrial;

export const getInstancePlan = (subscriptionInfo: SubscriptionInfo) => subscriptionInfo.plan;

export const getInstanceAddons = (subscriptionInfo: SubscriptionInfo) => subscriptionInfo.addons;

export const getInstanceBillingInfo = (subscriptionInfo: SubscriptionInfo) => subscriptionInfo.billingInfo;

export const getInstancePaymentMethod = (subscriptionInfo: SubscriptionInfo) => subscriptionInfo.paymentMethod;

export const getInstancePlanUser = (plan: Plan) => plan.user;

export const getInstancePaymentMethodCreditCard = (paymentMethod: PaymentMethod) => paymentMethod.creditCard;

export const getInstancePayrollInfo = (subscriptionInfo: SubscriptionInfo) => subscriptionInfo.payrollInfo;

export const getPaidAddons = (addons: Addon[]) => addons.filter(addon => addon.price > 0);
