import * as addonActions from '../actions/addon.action';

export interface State {
    loadingList: string[];
}

const initialState: State = {
    loadingList: []
};

export function reducer(state: State = initialState, action: addonActions.Actions): State {
    switch (action.type) {
        case addonActions.ACTIVATE:
        case addonActions.DEACTIVATE:
            return { loadingList: [...state.loadingList, action.payload.key] };
        case addonActions.ACTIVATE_SUCCESS:
        case addonActions.ACTIVATE_FAIL:
        case addonActions.DEACTIVATE_SUCCESS:
        case addonActions.DEACTIVATE_FAIL:
            return { loadingList: state.loadingList.filter(key => key !== action.payload.key) };
        default:
            return state;
    }
}

export const getLoadingList = (state: State) => state.loadingList;
