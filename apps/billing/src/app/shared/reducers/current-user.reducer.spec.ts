import * as userActions from '../actions/user.action';
import { User } from '../models';

import { getInstance, getLoaded, reducer } from './current-user.reducer';

const initialState = {
    instance: new User(),
    loaded: false
};

describe('Current user reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return existing state by default', () => {
        const action = new userActions.FetchCurrentAction();
        expect(reducer(initialState, action)).toBe(initialState);
    });

    it('should return loaded true on FETCH_CURRENT_FAIL', () => {
        const user = new User();
        const action = new userActions.FetchCurrentFailAction();
        expect(reducer({ instance: user, loaded: false }, action).instance).toBe(user);
        expect(reducer({ instance: user, loaded: false }, action).loaded).toBe(true);
    });

    it('should return new state on load FETCH_CURRENT_SUCCESS', () => {
        const user = new User();
        const action = new userActions.FetchCurrentSuccessAction(user);
        expect(reducer(initialState, action).instance).toBe(user);
        expect(reducer(initialState, action).loaded).toBe(true);
    });

    it('should have substates', () => {
        expect(getInstance(initialState)).toBe(initialState.instance);
        expect(getLoaded(initialState)).toBe(initialState.loaded);
    });
});
