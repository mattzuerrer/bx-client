import * as overviewActions from '../actions/overview.actions';
import * as subscriptionInfoActions from '../actions/subscription-info.action';
import { AddonAttribute, AddonAttributeCollection, CheckoutSummary, PaymentMethod, Plan, SubscriptionInfo } from '../models';

import {
    getIsOverviewProductOptionsSelectCountValid,
    getPaymentMethod,
    getPreview,
    getPreviewLoaded,
    getProductOptionsAttributes,
    getSubscriptionChanged,
    reducer
} from './overview.reducer';

const addonAttributes: any[] & { addon_payroll?: AddonAttribute } = [];
addonAttributes.addon_payroll = new AddonAttribute();

const originalState = Object.assign({
    plan: new Plan(),
    productOptions: ['addon_payroll', 'addon_banking'],
    productOptionsAttributes: new AddonAttributeCollection(addonAttributes)
});

const initialState = {
    preview: new CheckoutSummary(),
    previewLoaded: false,
    paymentMethod: new PaymentMethod(),
    plan: new Plan(),
    productOptions: [],
    productOptionsAttributes: new AddonAttributeCollection(),
    planChangeLoading: false,
    originalState: null
};

const loadedState = {
    preview: new CheckoutSummary(),
    previewLoaded: true,
    paymentMethod: new PaymentMethod(),
    plan: new Plan(),
    productOptions: ['addon_payroll'],
    productOptionsAttributes: new AddonAttributeCollection(addonAttributes),
    planChangeLoading: false,
    originalState
};

describe('Overview reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null, payload: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should set loaded to false on PREVIEW', () => {
        const action = new overviewActions.PreviewAction();
        expect(reducer(loadedState, action).preview).toBe(loadedState.preview);
        expect(reducer(loadedState, action).previewLoaded).toBe(false);
        expect(reducer(loadedState, action).paymentMethod).toBe(loadedState.paymentMethod);
        expect(reducer(loadedState, action).planChangeLoading).toBe(loadedState.planChangeLoading);
    });

    it('should return new state on PREVIEW_SUCCESS', () => {
        const preview = new CheckoutSummary();
        const action = new overviewActions.PreviewSuccessAction(preview);
        expect(reducer(initialState, action).preview).toBe(preview);
        expect(reducer(initialState, action).previewLoaded).toBe(true);
        expect(reducer(initialState, action).paymentMethod).toBe(initialState.paymentMethod);
        expect(reducer(loadedState, action).planChangeLoading).toBe(loadedState.planChangeLoading);
    });

    it('should change payment method on UPDATE_PAYMENT_METHOD', () => {
        const paymentMethod = new PaymentMethod();
        const action = new subscriptionInfoActions.UpdatePaymentMethodAction(paymentMethod);
        expect(reducer(initialState, action).preview).toBe(initialState.preview);
        expect(reducer(initialState, action).previewLoaded).toBe(initialState.previewLoaded);
        expect(reducer(initialState, action).paymentMethod).toBe(paymentMethod);
        expect(reducer(loadedState, action).planChangeLoading).toBe(loadedState.planChangeLoading);
    });

    it('should set payment method on LOAD_SUCCESS', () => {
        const subscriptionInfo = new SubscriptionInfo();
        const action = new subscriptionInfoActions.LoadSuccessAction(subscriptionInfo);
        expect(reducer(initialState, action).preview).toBe(initialState.preview);
        expect(reducer(initialState, action).previewLoaded).toBe(initialState.previewLoaded);
        expect(reducer(initialState, action).paymentMethod).toBe(subscriptionInfo.paymentMethod);
        expect(reducer(loadedState, action).planChangeLoading).toBe(loadedState.planChangeLoading);
    });

    it('should set payment method on UPDATE_PAYMENT_METHOD_SUCCESS', () => {
        const paymentMethod = new PaymentMethod();
        const action = new subscriptionInfoActions.UpdatePaymentMethodSuccessAction(paymentMethod);
        expect(reducer(initialState, action).preview).toBe(initialState.preview);
        expect(reducer(initialState, action).previewLoaded).toBe(initialState.previewLoaded);
        expect(reducer(initialState, action).paymentMethod).toBe(paymentMethod);
        expect(reducer(loadedState, action).planChangeLoading).toBe(loadedState.planChangeLoading);
    });

    it('should set planChangeLoading to true on UPDATE_PLAN', () => {
        const action = new subscriptionInfoActions.UpdatePlanAction();
        expect(reducer(loadedState, action).preview).toBe(loadedState.preview);
        expect(reducer(loadedState, action).previewLoaded).toBe(loadedState.previewLoaded);
        expect(reducer(loadedState, action).paymentMethod).toBe(loadedState.paymentMethod);
        expect(reducer(loadedState, action).planChangeLoading).toBe(true);
    });

    it('should set planChangeLoading to true on UPDATE_PLAN_FAIL', () => {
        const action = new subscriptionInfoActions.UpdatePlanFailAction();
        expect(reducer(loadedState, action).preview).toBe(loadedState.preview);
        expect(reducer(loadedState, action).previewLoaded).toBe(loadedState.previewLoaded);
        expect(reducer(loadedState, action).paymentMethod).toBe(loadedState.paymentMethod);
        expect(reducer(loadedState, action).planChangeLoading).toBe(false);
    });

    it('should set plan and change subscription on CHANGE_PLAN', () => {
        const plan = new Plan();
        const action = new overviewActions.ChangePlanAction(plan);
        expect(reducer(loadedState, action).preview).toBe(loadedState.preview);
        expect(reducer(loadedState, action).previewLoaded).toBe(loadedState.previewLoaded);
        expect(reducer(loadedState, action).paymentMethod).toBe(loadedState.paymentMethod);
        expect(reducer(loadedState, action).plan).toBe(plan);
        expect(reducer(loadedState, action).productOptions).toBe(loadedState.productOptions);
        expect(getSubscriptionChanged(loadedState)).toBe(true);
    });

    it('should have previous product options available in the original state on CHANGE_PLAN', () => {
        const plan = new Plan();
        const action = new overviewActions.ChangePlanAction(plan);
        const previousProductOptions = originalState.productOptions;
        expect(reducer(loadedState, action).originalState.productOptions).toBe(previousProductOptions);
    });

    it('should init product options on INIT_PRODUCT_OPTIONS', () => {
        const action = new overviewActions.InitProductOptionsAction(['addon_payroll', 'addon_banking']);
        expect(reducer(loadedState, action).preview).toBe(loadedState.preview);
        expect(reducer(loadedState, action).previewLoaded).toBe(loadedState.previewLoaded);
        expect(reducer(loadedState, action).paymentMethod).toBe(loadedState.paymentMethod);
        expect(reducer(loadedState, action).plan).toBe(loadedState.plan);
        expect(reducer(loadedState, action).productOptions).toEqual(['addon_payroll', 'addon_banking']);
    });

    it('should add product option and change subscription on CHECK_PRODUCT_OPTION', () => {
        const action = new overviewActions.CheckProductOptionAction('addon_banking');
        expect(reducer(loadedState, action).preview).toBe(loadedState.preview);
        expect(reducer(loadedState, action).previewLoaded).toBe(loadedState.previewLoaded);
        expect(reducer(loadedState, action).paymentMethod).toBe(loadedState.paymentMethod);
        expect(reducer(loadedState, action).plan).toBe(loadedState.plan);
        expect(reducer(loadedState, action).productOptions).toEqual(['addon_payroll', 'addon_banking']);
        expect(getSubscriptionChanged(loadedState)).toBe(true);
    });

    it('should remove product option on UNCHECK_PRODUCT_OPTION', () => {
        const action = new overviewActions.UncheckProductOptionAction('addon_payroll');
        expect(reducer(loadedState, action).preview).toBe(loadedState.preview);
        expect(reducer(loadedState, action).previewLoaded).toBe(loadedState.previewLoaded);
        expect(reducer(loadedState, action).paymentMethod).toBe(loadedState.paymentMethod);
        expect(reducer(loadedState, action).plan).toBe(loadedState.plan);
        expect(reducer(loadedState, action).productOptions).toEqual([]);
    });

    it('should increase addon or product option select count on CHECK_PRODUCT_OPTION', () => {
        const action = new overviewActions.CheckProductOptionAction('addon_payroll');

        expect(reducer(initialState, action).productOptions.length).toBe(1);
    });

    it('should decrease addon or product option select count on UNCHECK_PRODUCT_OPTIONN', () => {
        const action = new overviewActions.UncheckProductOptionAction('addon_payroll');

        expect(reducer(initialState, action).productOptions.length).toBe(0);
    });

    it('should set plan and subscription changed to null on INIT_PLAN', () => {
        const plan = new Plan();
        const action = new overviewActions.InitPlanAction(plan);
        expect(reducer(loadedState, action).preview).toBe(loadedState.preview);
        expect(reducer(loadedState, action).previewLoaded).toBe(loadedState.previewLoaded);
        expect(reducer(loadedState, action).paymentMethod).toBe(loadedState.paymentMethod);
        expect(reducer(loadedState, action).plan).toBe(plan);
        expect(reducer(loadedState, action).productOptions).toEqual([]);
        expect(getSubscriptionChanged(initialState)).toBe(null);
    });

    it('should set product options attributes on INIT_PRODUCT_OPTIONS_ATTRIBUTES', () => {
        const testAddonAttributes: any[] & { addon_payroll?: AddonAttribute } = [];
        testAddonAttributes.addon_payroll = new AddonAttribute();
        const productOptionsAttributes = new AddonAttributeCollection(testAddonAttributes);
        const action = new overviewActions.InitProductOptionsAttributesAction(productOptionsAttributes);
        expect(reducer(loadedState, action).productOptionsAttributes).toEqual(productOptionsAttributes);
    });

    it('should have previous product options attributes available in the original state on INIT_PRODUCT_OPTIONS_ATTRIBUTES', () => {
        const testAddonAttributes: any[] & { addon_payroll?: AddonAttribute } = [];
        testAddonAttributes.addon_payroll = new AddonAttribute(1);
        const productOptionsAttributes = new AddonAttributeCollection(testAddonAttributes);
        const action = new overviewActions.InitProductOptionsAttributesAction(productOptionsAttributes);
        expect(reducer(loadedState, action).originalState.productOptionsAttributes).toEqual(productOptionsAttributes);
    });

    it('should set payroll ending unit on CHANGE_PAYROLL_ENDING_UNIT_ATTRIBUTE', () => {
        const action = new overviewActions.ChangePayrollEndingUnitAttribute(101);
        expect(reducer(loadedState, action).productOptionsAttributes.get('addon_payroll').endingUnit).toEqual(101);
    });

    it('should have substates', () => {
        expect(getPreview(initialState)).toBe(initialState.preview);
        expect(getPreviewLoaded(initialState)).toBe(initialState.previewLoaded);
        expect(getPaymentMethod(initialState)).toBe(initialState.paymentMethod);
        expect(getSubscriptionChanged(initialState)).toBe(null);
        expect(getIsOverviewProductOptionsSelectCountValid(initialState)).toBe(false);
        expect(getProductOptionsAttributes(initialState)).toBe(initialState.productOptionsAttributes);
    });
});
