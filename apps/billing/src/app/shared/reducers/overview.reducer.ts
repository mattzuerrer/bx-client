import { addonPayrollKey, planStarterKey } from '../../../config';
import * as overviewActions from '../actions/overview.actions';
import * as productCatalogActions from '../actions/product-catalog.action';
import * as subscriptionInfoActions from '../actions/subscription-info.action';
import { AddonAttribute, AddonAttributeCollection, CheckoutSummary, PaymentMethod, Plan } from '../models';
import { arraysEqual } from '../utils';

interface OriginalState {
    plan: Plan;
    productOptions: string[];
    productOptionsAttributes: AddonAttributeCollection;
}

export interface State {
    preview: CheckoutSummary;
    previewLoaded: boolean;
    paymentMethod: PaymentMethod;
    plan: Plan;
    productOptions: string[];
    productOptionsAttributes: AddonAttributeCollection;
    planChangeLoading: boolean;
    originalState: OriginalState;
}

export const initialState: State = {
    preview: new CheckoutSummary(),
    previewLoaded: false,
    paymentMethod: new PaymentMethod(),
    plan: new Plan(),
    productOptions: [],
    productOptionsAttributes: new AddonAttributeCollection(),
    planChangeLoading: false,
    originalState: null
};

export function reducer(state: State = initialState, action: any): State {
    switch (action.type) {
        case overviewActions.PREVIEW:
            return Object.assign({}, state, { previewLoaded: false });
        case overviewActions.PREVIEW_SUCCESS:
            const preview = action.payload;
            return Object.assign({}, state, { preview, previewLoaded: true });
        case subscriptionInfoActions.UPDATE_PAYMENT_METHOD:
            return Object.assign({}, state, { paymentMethod: action.payload });
        case subscriptionInfoActions.LOAD_SUCCESS:
            const subscriptionInfo = action.payload;
            return Object.assign({}, state, { paymentMethod: subscriptionInfo.paymentMethod });
        case subscriptionInfoActions.UPDATE_PAYMENT_METHOD_SUCCESS:
            return Object.assign({}, state, { paymentMethod: action.payload });
        case subscriptionInfoActions.UPDATE_PLAN:
            return Object.assign({}, state, { planChangeLoading: true });
        case subscriptionInfoActions.UPDATE_PLAN_FAIL:
            return Object.assign({}, state, { planChangeLoading: false });
        case overviewActions.INIT_PLAN:
            const addonAttributes = [];
            addonAttributes[addonPayrollKey] = new AddonAttribute(action.payload.maxNumberOfPayrollEmployees);
            const originalState = {
                plan: action.payload,
                productOptions: [],
                productOptionsAttributes: new AddonAttributeCollection(addonAttributes)
            };
            return Object.assign({}, state, { ...originalState, originalState });
        case overviewActions.CHANGE_PLAN:
            return Object.assign({}, state, {
                plan: action.payload,
                productOptionsAttributes: state.productOptionsAttributes
            });
        case overviewActions.INIT_PRODUCT_OPTIONS:
            const originalProductOptionState = Object.assign({}, state.originalState, {
                productOptions: action.payload
            });

            return Object.assign({}, state, { productOptions: action.payload }, { originalState: originalProductOptionState });
        case overviewActions.INIT_PRODUCT_OPTIONS_ATTRIBUTES:
            return Object.assign(
                {},
                state,
                { originalState: Object.assign({}, state.originalState) },
                { productOptionsAttributes: state.productOptionsAttributes }
            );
        case overviewActions.CHECK_PRODUCT_OPTION:
            return Object.assign({}, state, { productOptions: [...state.productOptions, action.payload] });
        case overviewActions.UNCHECK_PRODUCT_OPTION:
            return Object.assign({}, state, {
                productOptions: state.productOptions.filter(key => key !== action.payload)
            });
        default:
            return state;
        case overviewActions.CHANGE_PAYROLL_ENDING_UNIT_ATTRIBUTE:
            const attributes = [];
            attributes[addonPayrollKey] = new AddonAttribute(action.payload);
            return Object.assign({}, state, { productOptionsAttributes: new AddonAttributeCollection(attributes) });
        case productCatalogActions.LOAD_FAIL:
        case overviewActions.PREVIEW_FAIL:
            return Object.assign({ planChangeLoading: true, previewLoaded: false });
    }
}

export const getPreview = (state: State) => state.preview;

export const getPreviewLoaded = (state: State) => state.previewLoaded;

export const getPaymentMethod = (state: State) => state.paymentMethod;

export const getPlan = (state: State) => state.plan;

export const getProductOptions = (state: State) => state.productOptions;

export const getIsOverviewProductOptionsSelectCountValid = (state: State) => state.productOptions.length > 0;

export const getProductOptionsAttributes = (state: State) => state.productOptionsAttributes;

export const getSubscriptionChanged = (state: State) => {
    return (
        state.originalState &&
        (!state.originalState.plan.isEqual(state.plan) ||
            !arraysEqual(state.originalState.productOptions, state.productOptions) ||
            ((state.plan.isProductVersionV1 || state.plan.key === planStarterKey) &&
                !state.originalState.productOptionsAttributes.isEqual(state.productOptionsAttributes)))
    );
};

export const getPlanChangeLoading = (state: State) => state.planChangeLoading;
