import { addonPayrollKey } from '../../../config';
import * as checkoutActions from '../actions/checkout.action';
import { AddonAttribute, AddonAttributeCollection, CheckoutRequest, CheckoutSummary, CreditCard, PaymentMethod } from '../models';

const initialPaymentMethod = Object.assign(new PaymentMethod(), { type: PaymentMethod.card });

const initialCheckoutRequest = Object.assign(new CheckoutRequest(), { paymentMethod: initialPaymentMethod });

export interface State {
    instance: CheckoutRequest;
    preview: CheckoutSummary;
    previewLoaded: boolean;
    requestProcessing: boolean;
}

const initialState: State = {
    instance: initialCheckoutRequest,
    preview: new CheckoutSummary(),
    previewLoaded: false,
    requestProcessing: false
};

const paymentMethodReducer = (state, action: checkoutActions.Actions): PaymentMethod => {
    switch (action.type) {
        case checkoutActions.CHANGE_CREDIT_CARD:
            const refId = action.payload;
            const creditCard = Object.assign(new CreditCard(), { refId });
            return Object.assign(new PaymentMethod(), state, { creditCard });
    }
};

const checkoutRequestReducer = (state, action: checkoutActions.Actions): CheckoutRequest => {
    switch (action.type) {
        case checkoutActions.INIT:
            return Object.assign(new CheckoutRequest(), state, action.payload);
        case checkoutActions.CHANGE_CREDIT_CARD:
            const paymentMethod = paymentMethodReducer(state.paymentMethod, action);
            return Object.assign(new CheckoutRequest(), state, { paymentMethod });
        case checkoutActions.CHANGE_PLAN:
            return Object.assign(new CheckoutRequest(), state, { plan: action.payload });
        case checkoutActions.CHANGE_PAYMENT_METHOD:
            return Object.assign(new CheckoutRequest(), state, { paymentMethod: action.payload });
        case checkoutActions.CHANGE_VOUCHER_CODE:
            return Object.assign(new CheckoutRequest(), state, { voucher: action.payload });
        case checkoutActions.CHANGE_BILLING_INFO:
            return Object.assign(new CheckoutRequest(), state, { billingInfo: action.payload });
        case checkoutActions.CHECK_PRODUCT_OPTION:
            return Object.assign(new CheckoutRequest(), state, {
                productOptions: [...state.productOptions, action.payload]
            });
        case checkoutActions.UNCHECK_PRODUCT_OPTION:
            return Object.assign(new CheckoutRequest(), state, {
                productOptions: state.productOptions.filter(key => key !== action.payload)
            });
        case checkoutActions.CHANGE_PAYROLL_ENDING_UNIT_ATTRIBUTE:
            const attributes = [];
            attributes[addonPayrollKey] = new AddonAttribute(action.payload);
            return Object.assign(new CheckoutRequest(), state, { productOptionsAttributes: new AddonAttributeCollection(attributes) });
    }
};

export function reducer(state: State = initialState, action: checkoutActions.Actions): State {
    switch (action.type) {
        case checkoutActions.PREVIEW:
            return Object.assign({}, state, { previewLoaded: false });
        case checkoutActions.PREVIEW_SUCCESS:
            return Object.assign({}, state, { preview: action.payload, previewLoaded: true });
        case checkoutActions.INIT:
        case checkoutActions.CHANGE_PAYMENT_METHOD:
        case checkoutActions.CHANGE_PLAN:
        case checkoutActions.CHANGE_BILLING_INFO:
        case checkoutActions.CHANGE_CREDIT_CARD:
        case checkoutActions.CHANGE_VOUCHER_CODE:
        case checkoutActions.CHECK_PRODUCT_OPTION:
        case checkoutActions.UNCHECK_PRODUCT_OPTION:
        case checkoutActions.CHANGE_PAYROLL_ENDING_UNIT_ATTRIBUTE:
            const instance = checkoutRequestReducer(state.instance, action);
            return Object.assign({}, state, { instance });
        case checkoutActions.CHECKOUT:
            return Object.assign({}, state, { requestProcessing: true });
        case checkoutActions.CHECKOUT_SUCCESS:
        case checkoutActions.CHECKOUT_FAIL:
            return Object.assign({}, state, { requestProcessing: false });
        default:
            return state;
    }
}

export const getInstance = (state: State) => state.instance;

export const getInstancePlan = (instance: CheckoutRequest) => instance.plan;

export const getInstanceBillingInfo = (instance: CheckoutRequest) => instance.billingInfo;

export const getInstancePaymentMethod = (instance: CheckoutRequest) => instance.paymentMethod;

export const getInstanceVoucher = (instance: CheckoutRequest) => instance.voucher;

export const getInstanceProductOptions = (instance: CheckoutRequest) => instance.productOptions;

export const getInstanceProductOptionsAttributes = (instance: CheckoutRequest) => instance.productOptionsAttributes;

export const getIsCheckoutProductOptionsSelectCountValid = (instance: CheckoutRequest) => instance.productOptions.length > 0;

export const getPreview = (state: State) => state.preview;

export const getPreviewLoaded = (state: State) => state.previewLoaded;

export const getRequestProcessing = (state: State) => state.requestProcessing;
