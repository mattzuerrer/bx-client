import * as companyActions from '../actions/company.action';
import { CompanyProfile } from '../models';

import { getLoaded, getProfile, reducer } from './company.reducer';

const initialState = {
    profile: new CompanyProfile(),
    loaded: false
};

describe('Company reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return existing state by default', () => {
        const action = new companyActions.LoadAction();
        expect(reducer(initialState, action)).toBe(initialState);
    });

    it('should return loaded true on LOAD_FAIL', () => {
        const companyProfile = new CompanyProfile();
        const action = new companyActions.LoadFailAction();
        expect(reducer({ profile: companyProfile, loaded: false }, action).profile).toBe(companyProfile);
        expect(reducer({ profile: companyProfile, loaded: false }, action).loaded).toBe(true);
    });

    it('should return new state on LOAD_SUCCESS', () => {
        const companyProfile = new CompanyProfile();
        const action = new companyActions.LoadSuccessAction(companyProfile);
        expect(reducer(initialState, action).profile).toBe(companyProfile);
        expect(reducer(initialState, action).loaded).toBe(true);
    });

    it('should return new state on SAVE_SUCCESS', () => {
        const companyProfile = new CompanyProfile();
        const action = new companyActions.SaveSuccessAction(companyProfile);
        expect(reducer(initialState, action).profile).toBe(companyProfile);
        expect(reducer(initialState, action).loaded).toBe(true);
    });

    it('should have substates', () => {
        expect(getProfile(initialState)).toBe(initialState.profile);
        expect(getLoaded(initialState)).toBe(initialState.loaded);
    });
});
