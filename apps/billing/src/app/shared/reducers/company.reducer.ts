import * as companyActions from '../actions/company.action';
import { CompanyProfile } from '../models';

export interface State {
    profile: CompanyProfile;
    loaded: boolean;
}

const initialState: State = {
    profile: new CompanyProfile(),
    loaded: false
};

export function reducer(state: State = initialState, action: companyActions.Actions): State {
    switch (action.type) {
        case companyActions.LOAD_SUCCESS:
            return { profile: action.payload, loaded: true };
        case companyActions.LOAD_FAIL:
            return Object.assign({}, state, { loaded: true });
        case companyActions.SAVE_SUCCESS:
            return { profile: action.payload, loaded: true };
        default:
            return state;
    }
}

export const getProfile = (state: State) => state.profile;

export const getLoaded = (state: State) => state.loaded;
