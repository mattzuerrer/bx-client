import * as checkoutActions from '../actions/checkout.action';
import { CheckoutRequest, CheckoutSummary, CompanyProfile, CreditCard, PaymentMethod, Plan, SubscriptionInfo, Voucher } from '../models';

import {
    getInstance,
    getInstanceBillingInfo,
    getInstancePaymentMethod,
    getInstancePlan,
    getInstanceProductOptions,
    getInstanceProductOptionsAttributes,
    getInstanceVoucher,
    getIsCheckoutProductOptionsSelectCountValid,
    getPreview,
    getPreviewLoaded,
    reducer
} from './checkout.reducer';

const initialPaymentMethod = Object.assign(new PaymentMethod(), { type: PaymentMethod.card });

const initialCheckoutRequest = Object.assign(new CheckoutRequest(), { paymentMethod: initialPaymentMethod });

const initialState = {
    instance: initialCheckoutRequest,
    preview: new CheckoutSummary(),
    previewLoaded: false,
    requestProcessing: false
};

describe('Checkout reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should set loaded to false on PREVIEW', () => {
        const action = new checkoutActions.PreviewAction();
        expect(reducer(initialState, action).instance).toBe(initialState.instance);
        expect(reducer(initialState, action).preview).toBe(initialState.preview);
        expect(reducer(initialState, action).previewLoaded).toBe(false);
    });

    it('should return new preview and loaded true on PREVIEW_SUCCESS', () => {
        const checkoutSummary = new CheckoutSummary();
        const action = new checkoutActions.PreviewSuccessAction(checkoutSummary);
        expect(reducer(initialState, action).instance).toBe(initialState.instance);
        expect(reducer(initialState, action).preview).toBe(checkoutSummary);
        expect(reducer(initialState, action).previewLoaded).toBe(true);
    });

    it('should set checkout request on INIT', () => {
        const checkoutRequest = new CheckoutRequest();
        const action = new checkoutActions.InitAction(checkoutRequest);

        expect(reducer(initialState, action).instance).toEqual(action.payload);
        expect(reducer(initialState, action).preview).toBe(initialState.preview);
        expect(reducer(initialState, action).previewLoaded).toBe(initialState.previewLoaded);
    });

    it('should set checkout request plan on UPDATE_PLAN', () => {
        const plan = new Plan();
        const action = new checkoutActions.CheckoutChangePlanAction(plan);

        expect(reducer(initialState, action).instance).toEqual(Object.assign(new CheckoutRequest(), initialCheckoutRequest, { plan }));
        expect(reducer(initialState, action).preview).toBe(initialState.preview);
        expect(reducer(initialState, action).previewLoaded).toBe(initialState.previewLoaded);
    });

    it('should set checkout request plan on UPDATE_PAYMENT_METHOD', () => {
        const paymentMethod = new PaymentMethod();
        const action = new checkoutActions.CheckoutChangePaymentMethodAction(paymentMethod);

        expect(reducer(initialState, action).instance).toEqual(Object.assign(new CheckoutRequest(), { paymentMethod }));
        expect(reducer(initialState, action).preview).toBe(initialState.preview);
        expect(reducer(initialState, action).previewLoaded).toBe(initialState.previewLoaded);
    });

    it('should set checkout request plan on CHANGE_CREDIT_CARD', () => {
        const refId = 'refId';
        const creditCard = Object.assign(new CreditCard(), { refId });
        const action = new checkoutActions.CheckoutChangeCreditCardAction(refId);
        const paymentMethod = Object.assign(new PaymentMethod(), { creditCard, type: 'card' });

        expect(reducer(initialState, action).instance).toEqual(Object.assign(new CheckoutRequest(), { paymentMethod }));
        expect(reducer(initialState, action).preview).toBe(initialState.preview);
        expect(reducer(initialState, action).previewLoaded).toBe(initialState.previewLoaded);
    });

    it('should set checkout request voucher on CHANGE_VOUCHER_CODE', () => {
        const voucher = Object.assign(new Voucher(), { code: 'code_test' });
        const action = new checkoutActions.CheckoutChangeVoucherAction(voucher);

        expect(reducer(initialState, action).instance).toEqual(Object.assign(new CheckoutRequest(), initialCheckoutRequest, { voucher }));
        expect(reducer(initialState, action).preview).toBe(initialState.preview);
        expect(reducer(initialState, action).previewLoaded).toBe(initialState.previewLoaded);
    });

    it('should add addon on CHECK_PRODUCT_OPTION', () => {
        const addonKey = 'addon_payroll';
        const action = new checkoutActions.CheckoutCheckProductOptionAction(addonKey);

        expect(reducer(initialState, action).instance.productOptions).toEqual([addonKey]);
    });

    it('should increase addon or product option select count on CHECK_PRODUCT_OPTION', () => {
        const addonKey = 'addon_accounting_payroll';
        const action = new checkoutActions.CheckoutCheckProductOptionAction(addonKey);

        expect(reducer(initialState, action).instance.productOptions.length).toBe(1);
    });

    it('should decrease addon or product option select count on UNCHECK_PRODUCT_OPTIONN', () => {
        const addonKey = 'addon_accounting_payroll';
        const action = new checkoutActions.CheckoutUncheckProductOptionAction(addonKey);

        expect(reducer(initialState, action).instance.productOptions.length).toBe(0);
    });

    it('should remove addon on UNCHECK_PRODUCT_OPTION', () => {
        const addonKey1 = 'addon_payroll';
        const addonKey2 = 'addon_banking';
        const action = new checkoutActions.CheckoutUncheckProductOptionAction(addonKey1);

        const state = Object.assign({}, initialState, {
            instance: Object.assign(new CheckoutRequest(), initialCheckoutRequest, {
                productOptions: [addonKey1, addonKey2]
            })
        });

        expect(reducer(state, action).instance.productOptions).toEqual([addonKey2]);
    });

    it('should set checkout request on CHANGE_BILLING_INFO', () => {
        const billingInfo = Object.assign(new CompanyProfile(), { countryCode: 'CH' });
        const action = new checkoutActions.CheckoutChangeBillingInfoAction(billingInfo);
        const checkoutRequest = Object.assign(new CheckoutRequest(), { billingInfo });
        expect(reducer(Object.assign({}, initialState, { instance: checkoutRequest }), action).instance).toEqual(
            Object.assign(new CheckoutRequest(), { billingInfo })
        );
        expect(reducer(initialState, action).preview).toBe(initialState.preview);
        expect(reducer(initialState, action).previewLoaded).toBe(initialState.previewLoaded);
    });

    it('should set request processing to true on CHECKOUT', () => {
        const action = new checkoutActions.CheckoutAction();
        expect(reducer(initialState, action).instance).toBe(initialState.instance);
        expect(reducer(initialState, action).preview).toBe(initialState.preview);
        expect(reducer(initialState, action).previewLoaded).toBe(false);
        expect(reducer(initialState, action).requestProcessing).toBe(true);
    });

    it('should set request processing to false on CHECKOUT_SUCCESS', () => {
        const action = new checkoutActions.CheckoutSuccessAction(new SubscriptionInfo());
        expect(reducer(initialState, action).instance).toBe(initialState.instance);
        expect(reducer(initialState, action).preview).toBe(initialState.preview);
        expect(reducer(initialState, action).previewLoaded).toBe(false);
        expect(reducer(initialState, action).requestProcessing).toBe(false);
    });

    it('should set request processing to false on CHECKOUT_FAIL', () => {
        const action = new checkoutActions.CheckoutFailAction();
        expect(reducer(initialState, action).instance).toBe(initialState.instance);
        expect(reducer(initialState, action).preview).toBe(initialState.preview);
        expect(reducer(initialState, action).previewLoaded).toBe(false);
        expect(reducer(initialState, action).requestProcessing).toBe(false);
    });

    it('should have substates', () => {
        expect(getInstance(initialState)).toBe(initialState.instance);
        expect(getInstancePlan(initialState.instance)).toBe(initialState.instance.plan);
        expect(getInstanceBillingInfo(initialState.instance)).toBe(initialState.instance.billingInfo);
        expect(getInstancePaymentMethod(initialState.instance)).toBe(initialState.instance.paymentMethod);
        expect(getInstanceVoucher(initialState.instance)).toBe(initialState.instance.voucher);
        expect(getInstanceProductOptions(initialState.instance)).toBe(initialState.instance.productOptions);
        expect(getInstanceProductOptionsAttributes(initialState.instance)).toBe(initialState.instance.productOptionsAttributes);
        expect(getIsCheckoutProductOptionsSelectCountValid(initialState.instance)).toBe(false);
        expect(getPreview(initialState)).toBe(initialState.preview);
        expect(getPreviewLoaded(initialState)).toBe(initialState.previewLoaded);
    });
});
