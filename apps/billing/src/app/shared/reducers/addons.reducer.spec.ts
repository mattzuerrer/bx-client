import * as addonActions from '../actions/addon.action';
import { Addon } from '../models/Addon';

import { getLoadingList, reducer } from './addons.reducer';

const initialState = {
    loadingList: []
};

const testAddonKey = 'addon_test';
const testAddon = Object.assign(new Addon(), { key: testAddonKey });

const stateWithAddon = {
    loadingList: [testAddonKey]
};

describe('Addons reducer', () => {
    it('should have initial state', () => {
        const state = undefined;
        const action = { type: null, payload: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should add addon key to list on ACTIVATE', () => {
        const action = new addonActions.ActivateAction(testAddon);
        expect(reducer(initialState, action).loadingList).toContain(testAddonKey);
    });

    it('should add addon key to list on DEACTIVATE', () => {
        const action = new addonActions.DeactivateAction(testAddon);
        expect(reducer(initialState, action).loadingList).toContain(testAddonKey);
    });

    it('should remove addon key from list on ACTIVATE_SUCCESS', () => {
        const action = new addonActions.ActivateSuccessAction(testAddon);
        expect(reducer(stateWithAddon, action).loadingList).not.toContain(testAddonKey);
    });

    it('should remove addon key from list on ACTIVATE_SFAIL', () => {
        const action = new addonActions.ActivateFailAction(testAddon);
        expect(reducer(stateWithAddon, action).loadingList).not.toContain(testAddonKey);
    });

    it('should remove addon key from list on DEACTIVATE_SUCCESS', () => {
        const action = new addonActions.DeactivateSuccessAction(testAddon);
        expect(reducer(stateWithAddon, action).loadingList).not.toContain(testAddonKey);
    });

    it('should remove addon key from list on ACTIVATE_SUCCESS', () => {
        const action = new addonActions.DeactivateFailAction(testAddon);
        expect(reducer(stateWithAddon, action).loadingList).not.toContain(testAddonKey);
    });

    it('should have substates', () => {
        expect(getLoadingList(initialState)).toBe(initialState.loadingList);
    });
});
