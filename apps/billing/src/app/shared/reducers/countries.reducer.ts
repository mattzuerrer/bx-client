import { excludedCountries } from '../../../config/countries';
import * as countriesActions from '../actions/countries.action';
import { Country } from '../models';

export interface State {
    collection: Country[];
    loaded: boolean;
}

const initialState: State = {
    collection: [],
    loaded: false
};

const filterCountries = countries => countries.filter(country => excludedCountries.indexOf(country.id) === -1);

export function reducer(state: State = initialState, action: countriesActions.Actions): State {
    switch (action.type) {
        case countriesActions.LOAD_SUCCESS:
            return { collection: filterCountries(action.payload), loaded: true };
        case countriesActions.LOAD_FAIL:
            return Object.assign({}, state, { loaded: true });
        default:
            return state;
    }
}

export const getCollection = (state: State) => state.collection;

export const getLoaded = (state: State) => state.loaded;
