import { addonsMapping, planCustomKey, plansMapping, plansOrder } from '../../../config';
import * as productCatalogActions from '../actions/product-catalog.action';
import { Addon, Plan, ProductCatalog } from '../models';

export interface State {
    instance: ProductCatalog;
    loaded: boolean;
}

const initialState: State = {
    instance: new ProductCatalog(),
    loaded: false
};

const applyPlanConfig = plan => Object.assign(new Plan(), plan, plansMapping[plan.key]);

const applyAddonConfig = addon => Object.assign(new Addon(), addon, addonsMapping.find(addonMapping => addonMapping.key === addon.key));

const filterCustomPlan = plan => plan.key !== planCustomKey;

export const sortPlans = (a, b) => (plansOrder.indexOf(a.key) >= plansOrder.indexOf(b.key) ? 1 : -1);

const applyCatalogConfig = catalog =>
    Object.assign(new ProductCatalog(), {
        plans: catalog.plans
            .filter(filterCustomPlan)
            .map(applyPlanConfig)
            .sort(sortPlans),
        addons: catalog.addons.map(applyAddonConfig)
    });

export function reducer(state: State = initialState, action: productCatalogActions.Actions): State {
    switch (action.type) {
        case productCatalogActions.LOAD_SUCCESS:
            return { instance: applyCatalogConfig(action.payload), loaded: true };
        case productCatalogActions.LOAD_FAIL:
            return Object.assign({}, state, { loaded: true });
        default:
            return state;
    }
}

export const getInstance = (state: State) => state.instance;

export const getLoaded = (state: State) => state.loaded;

export const getCatalogPlans = (catalog: ProductCatalog) => catalog.plans;

export const getCatalogAddons = (catalog: ProductCatalog) => catalog.addons;

export const getPaidAddons = (addons: Addon[]) => addons.filter(addon => addon.price > 0);
