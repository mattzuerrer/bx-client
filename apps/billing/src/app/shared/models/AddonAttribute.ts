import { ScalePrice } from './ScalePrice';

export class AddonAttribute {
    endingUnit: number;

    constructor(endingUnit: number = ScalePrice.DEFAULT_NB_PAYROLL_USERS) {
        this.endingUnit = endingUnit;
    }

    isEqual(other: AddonAttribute): boolean {
        return this.endingUnit === other.endingUnit;
    }
}
