const stateOpen = 'open';

const statePaid = 'paid';

const typeInvoice = 'invoice';

const typeCreditNote = 'credit_note';

export class Invoice {
    id: string;
    number: string;
    type: string;
    state: string;
    amount: number;
    date: string;
    currency: string;

    static get stateOpen(): string {
        return stateOpen;
    }

    static get statePaid(): string {
        return statePaid;
    }

    get isInvoiceType(): boolean {
        return this.type === typeInvoice;
    }

    get isCreditNoteType(): boolean {
        return this.type === typeCreditNote;
    }

    get absAmount(): number {
        return Math.abs(this.amount);
    }
}
