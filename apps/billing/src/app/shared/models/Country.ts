export class Country {
    id: number;
    name: string;

    format(): string {
        return this.name;
    }
}
