const statusNone = '';

const statusProgress = 'progress';

export class FormState {
    static get statusNone(): string {
        return statusNone;
    }

    static get statusProgress(): string {
        return statusProgress;
    }

    status: string;
}
