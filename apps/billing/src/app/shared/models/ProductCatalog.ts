import { Type } from 'class-transformer';

import { Addon } from './Addon';
import { Plan } from './Plan';
import { serializeType } from './serialization';

export class ProductCatalog {
    @Type(serializeType(Plan))
    plans: Plan[] = [];
    @Type(serializeType(Addon))
    addons: Addon[] = [];
}
