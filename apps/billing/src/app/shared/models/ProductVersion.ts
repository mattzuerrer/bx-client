const v1 = 'v1';

const v2 = 'v2';

export class ProductVersion {
    static get V1(): string {
        return v1;
    }

    static get V2(): string {
        return v2;
    }
}
