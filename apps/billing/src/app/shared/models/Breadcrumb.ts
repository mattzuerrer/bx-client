export class Breadcrumb {
    translationKey: string;
    url: string;
}
