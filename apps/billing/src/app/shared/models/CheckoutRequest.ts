import { Type } from 'class-transformer';

import { AddonAttributeCollection } from './AddonAttributeCollection';
import { CompanyProfile } from './CompanyProfile';
import { PaymentMethod } from './PaymentMethod';
import { Plan } from './Plan';
import { serializeType } from './serialization';
import { Voucher } from './Voucher';

export class CheckoutRequest {
    @Type(serializeType(Plan))
    plan: Plan = new Plan();

    @Type(serializeType(PaymentMethod))
    paymentMethod: PaymentMethod = new PaymentMethod();

    @Type(serializeType(CompanyProfile))
    billingInfo: CompanyProfile = new CompanyProfile();

    @Type(serializeType(Voucher))
    voucher: Voucher = new Voucher();

    productOptions: string[] = [];

    productOptionsAttributes: AddonAttributeCollection = new AddonAttributeCollection();
}
