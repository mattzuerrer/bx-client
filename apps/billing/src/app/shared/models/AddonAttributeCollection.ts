import { AddonAttribute } from './AddonAttribute';

export class AddonAttributeCollection {
    attributes: AddonAttribute[];

    constructor(attributes: AddonAttribute[] = []) {
        this.attributes = attributes;
    }

    get(key: string): AddonAttribute {
        return this.attributes[key];
    }

    isEqual(other: AddonAttributeCollection): boolean {
        const keys = Object.keys(this.attributes);
        const otherKeys = Object.keys(other.attributes);

        return keys.length === otherKeys.length && keys.every(key => this.get(key).isEqual(other.get(key)));
    }
}
