import { Type } from 'class-transformer';

import { addonBankingKey, addonPayrollKey, planAccountantKey, planCustomKey, planMiniKey, planProPlusKey } from '../../../config';

import { Product } from './Product';
import { serializeType } from './serialization';
import { UserProduct } from './UserProduct';

const chfCurrency = 'CHF';

export class Plan extends Product {
    category: string = Product.categoryPlan;

    maxNumberOfUsers = 1;

    maxNumberOfPayrollEmployees = 1;

    includedProductOptions: string[] = [];

    @Type(serializeType(UserProduct))
    user: UserProduct = new UserProduct();

    get isLocalCurrency(): boolean {
        return this.currency === chfCurrency;
    }

    get hasBanking(): boolean {
        return this.includedProductOptions.indexOf(addonBankingKey) > -1;
    }

    get hasPayroll(): boolean {
        return this.includedProductOptions.indexOf(addonPayrollKey) > -1;
    }

    get isCustomPlan(): boolean {
        return this.key === planCustomKey;
    }

    get isAccountantPlan(): boolean {
        return this.key === planAccountantKey;
    }

    get isProPlusPlan(): boolean {
        return this.key === planProPlusKey;
    }

    get isMiniPlan(): boolean {
        return this.key === planMiniKey;
    }

    isEqual(other: Plan): boolean {
        return this.key === other.key && this.billingPeriod === other.billingPeriod;
    }
}
