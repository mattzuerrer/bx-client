export class RsaSignature {
    tenantId: string;
    id: string;
    signature: string;
    token: string;
    key: string;
    url: string;
    paymentGateway: string;
}
