export class CreditCard {
    refId = '';

    expirationMonth = '';

    expirationYear = '';

    name = '';

    number = '';

    type = '';

    get hasDetails(): boolean {
        return this.number !== '' && this.name !== '';
    }

    get hasRefId(): boolean {
        return this.refId !== '';
    }

    get forrmatedCardNumber(): string {
        return this.number
            .replace(/\*/g, 'x')
            .match(/.{1,4}/g)
            .join(' ');
    }
}
