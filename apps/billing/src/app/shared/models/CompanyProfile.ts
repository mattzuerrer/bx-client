import { Type } from 'class-transformer';

import { Country } from './Country';
import { serializeType } from './serialization';

const domesticCountryCode = 'CH';

export class CompanyProfile {
    id: number;
    name: string;
    address: string;
    addressNr: number;
    postcode: number;
    city: string;
    mail: string;
    countryCode: string;
    phoneFixed: string;
    phoneMobile: string;
    fax: string;
    url: string;
    description: string;
    firstname: string;
    lastname: string;
    accountantCompanyName: string;

    @Type(serializeType(Country))
    country: Country = new Country();

    get isDomestic(): boolean {
        return this.countryCode === domesticCountryCode;
    }

    static get domesticCountryCode(): string {
        return domesticCountryCode;
    }
}
