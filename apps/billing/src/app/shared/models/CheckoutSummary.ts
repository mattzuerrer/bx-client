import { Type } from 'class-transformer';

import { Addon } from './Addon';
import { Discount } from './Discount';
import { Plan } from './Plan';
import { Product } from './Product';
import { serializeType } from './serialization';
import { Voucher } from './Voucher';

const voucherValidityNone = 'none';

const voucherValidityValid = 'valid';

const voucherValidityInvalid = 'invalid';

const voucherValidityNoResponse = 'no_response';

export class CheckoutSummary {
    @Type(serializeType(Plan))
    plan: Plan = new Plan();

    @Type(serializeType(Product))
    users: Product = new Product();

    @Type(serializeType(Addon))
    addons: Addon[] = [];

    @Type(serializeType(Voucher))
    voucher: Voucher = new Voucher();

    @Type(serializeType(Discount))
    discounts: Discount[] = [];

    @Type(serializeType(Product))
    total: Product = new Product();

    @Type(serializeType(Product))
    netTotal: Product = new Product();

    voucherValidity: string = voucherValidityNone;

    get isVoucherValid(): boolean {
        return this.voucherValidity === voucherValidityValid;
    }

    get isVoucherInvalid(): boolean {
        return this.voucherValidity === voucherValidityInvalid;
    }

    get hasNoResponse(): boolean {
        return this.voucherValidity === voucherValidityNoResponse;
    }

    get isVoucherNotAvailable(): boolean {
        return this.isVoucherInvalid || this.hasNoResponse;
    }
}
