import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';

export interface GuardConfig {
    stream: Observable<boolean>;
    required: boolean;
    action: Action;
}
