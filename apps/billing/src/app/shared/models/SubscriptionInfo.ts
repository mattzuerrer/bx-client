import { Type } from 'class-transformer';

import { productOptionsKeys } from '../../../config';

import { Addon } from './Addon';
import { CompanyProfile } from './CompanyProfile';
import { PaymentMethod } from './PaymentMethod';
import { PayrollInfo } from './PayrollInfo';
import { Plan } from './Plan';
import { serializeType } from './serialization';

export class SubscriptionInfo {
    isTrial: boolean;
    @Type(serializeType(Plan))
    plan: Plan = new Plan();
    @Type(serializeType(CompanyProfile))
    billingInfo: CompanyProfile = new CompanyProfile();
    @Type(serializeType(PaymentMethod))
    paymentMethod: PaymentMethod = new PaymentMethod();
    @Type(serializeType(Addon))
    addons: Addon[] = [];
    @Type(serializeType(PayrollInfo))
    payrollInfo: PayrollInfo = new PayrollInfo();

    get productOptions(): Addon[] {
        return this.addons.filter(addon => productOptionsKeys.indexOf(addon.key) > -1);
    }
}
