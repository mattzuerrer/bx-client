import { Product } from './Product';

export class UserProduct extends Product {
    category: string = Product.categoryUser;
}
