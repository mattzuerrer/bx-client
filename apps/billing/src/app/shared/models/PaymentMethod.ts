import { Type } from 'class-transformer';

import { CreditCard } from './CreditCard';
import { serializeType } from './serialization';

const invoice = 'invoice';
const card = 'card';

export class PaymentMethod {
    type: string = invoice;

    @Type(serializeType(CreditCard))
    creditCard: CreditCard = new CreditCard();

    static get invoice(): string {
        return invoice;
    }

    static get card(): string {
        return card;
    }

    get isInvoice(): boolean {
        return this.type === invoice;
    }

    get isCard(): boolean {
        return this.type === card;
    }

    get hasCreditCardRef(): boolean {
        return this.creditCard.hasRefId;
    }
}
