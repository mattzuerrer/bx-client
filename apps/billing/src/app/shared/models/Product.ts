import { ProductVersion } from './ProductVersion';

const billingPeriodMonth = 'month';

const billingPeriodAnnual = 'annual';

const categoryPlan = 'plan';

const categoryAddon = 'addon';

const categoryUser = 'user';

export class Product {
    key = '';
    billingPeriod = '';
    currency = '';
    quantity = 1;
    price = 0;
    periodPrice = 0;
    totalPrice = 0;
    category = '';
    productVersion: string = ProductVersion.V1;

    get id(): string {
        return this.key;
    }

    get name(): string {
        return `billing.${this.key}.name`;
    }

    get description(): string {
        return `billing.${this.key}.description`;
    }

    get identifier(): string {
        return `${this.key}_${this.billingPeriod}`;
    }

    get isPaid(): boolean {
        return this.price > 0;
    }

    static get billingPeriodMonth(): string {
        return billingPeriodMonth;
    }

    static get billingPeriodAnnual(): string {
        return billingPeriodAnnual;
    }

    static get categoryPlan(): string {
        return categoryPlan;
    }

    static get categoryAddon(): string {
        return categoryAddon;
    }

    static get categoryUser(): string {
        return categoryUser;
    }

    get isBillingPeriodMonth(): boolean {
        return this.billingPeriod === billingPeriodMonth;
    }

    get isBillingPeriodAnnual(): boolean {
        return this.billingPeriod === billingPeriodAnnual;
    }

    get isProductVersionV1(): boolean {
        return this.productVersion === ProductVersion.V1;
    }

    get isProductVersionV2(): boolean {
        return this.productVersion === ProductVersion.V2;
    }
}
