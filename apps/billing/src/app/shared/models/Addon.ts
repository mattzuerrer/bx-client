import { Type } from 'class-transformer';

import { planProKey, planProPlusKey, planStarterKey } from '../../../config/plansMapping';

import { AddonAttribute } from './AddonAttribute';
import { Plan } from './Plan';
import { Product } from './Product';
import { ScalePrice } from './ScalePrice';
import { serializeType } from './serialization';

export interface InstructionsUrl {
    de?: string;
    en?: string;
    fr?: string;
}

export class Addon extends Product {
    logoUrl = '';
    instructionsUrl: InstructionsUrl = {};
    isDeprecated = false;
    isProductOption = false;
    hasScalePrices = false;
    @Type(serializeType(ScalePrice))
    scalePrices: ScalePrice[] = [];
    isTrial = false;
    trialDaysLeft = 0;

    customAttributes: AddonAttribute = new AddonAttribute();

    category: string = Product.categoryAddon;

    getScalePrice(numberOfUnits: number): ScalePrice {
        const foundScalePrice = this.scalePrices.find(
            scalePrice => scalePrice.startingUnit <= numberOfUnits && scalePrice.endingUnit >= numberOfUnits
        );
        return foundScalePrice ? foundScalePrice : new ScalePrice();
    }

    getScalePriceByPlan(plan: Plan): ScalePrice {
        let index = 0;
        switch (plan.key) {
            case planStarterKey:
                index = 0;
                break;
            case planProKey:
                index = 1;
                break;
            case planProPlusKey:
                index = 2;
        }
        return this.scalePrices[index];
    }
}
