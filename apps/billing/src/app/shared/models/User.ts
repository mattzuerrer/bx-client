export class User {
    id: number;
    salutationType: string;
    firstname: string;
    lastname: string;
    email: string;
    isSuperadmin: boolean;
    isAccountant: boolean;

    get fullName(): string {
        const firstName = this.firstname || '';
        const lastName = this.lastname || '';
        return `${firstName} ${lastName}`;
    }

    get alias(): string {
        return this.fullName.trim() ? this.fullName : this.email;
    }
}
