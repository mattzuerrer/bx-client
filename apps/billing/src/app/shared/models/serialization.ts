export function serializeType<T>(object: T): () => T {
    return (): T => object;
}
