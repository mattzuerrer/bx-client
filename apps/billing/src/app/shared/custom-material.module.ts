import { NgModule } from '@angular/core';
import {
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatIconModule,
    MatListModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSlideToggleModule,
    MatTooltipModule
} from '@angular/material';

@NgModule({
    imports: [
        MatAutocompleteModule,
        MatButtonModule,
        MatCardModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        MatListModule,
        MatRadioModule,
        MatCheckboxModule,
        MatIconModule,
        MatSlideToggleModule,
        MatTooltipModule,
        MatProgressBarModule
    ],
    exports: [
        MatAutocompleteModule,
        MatButtonModule,
        MatCardModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        MatListModule,
        MatRadioModule,
        MatCheckboxModule,
        MatIconModule,
        MatSlideToggleModule,
        MatTooltipModule,
        MatProgressBarModule
    ]
})
export class CustomMaterialModule {}
