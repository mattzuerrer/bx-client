import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';

export { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';

export const sharedEntryComponents = [ConfirmDialogComponent];
