import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { StateService } from '../../services';

@Component({
    selector: 'app-confirm-dialog',
    templateUrl: 'confirm-dialog.component.html',
    styleUrls: ['confirm-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmDialogComponent implements OnInit, OnDestroy {
    @Input() title = '';

    @Input() content = '';

    @Input() data: any = null;

    @Input() loadingStreamName = '';

    @Input() confirmButtonText = 'globals.ok';

    @Output() result: EventEmitter<any> = new EventEmitter<any>();

    loading = false;

    private subscriptions: Subscription[] = [];

    constructor(private dialogRef: MatDialogRef<ConfirmDialogComponent>, private stateService: StateService) {}

    ngOnInit(): void {
        if (this.isAsync) {
            this.subscriptions.push(
                this.stateService[this.loadingStreamName]
                    .pipe(filter(loading => this.loading !== loading))
                    .subscribe(() => this.dialogRef.close())
            );
        }
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    closeModal(result: boolean): void {
        if (this.isAsync && result) {
            this.loading = true;
            this.result.emit(true);
        } else {
            this.dialogRef.close(result);
        }
    }

    get loadingStatus(): string {
        return this.loading ? 'progress' : '';
    }

    get isAsync(): boolean {
        return this.loadingStreamName !== '';
    }
}
