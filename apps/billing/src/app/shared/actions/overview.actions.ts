import { Action } from '@ngrx/store';

import { AddonAttributeCollection, CheckoutSummary, Plan } from '../models';

export const PREVIEW = '[Overview] Preview';
export const PREVIEW_SUCCESS = '[Overview] Preview Success';
export const PREVIEW_FAIL = '[Overview] Preview Fail';
export const INIT_PLAN = '[Overview] Init Plan';
export const CHANGE_PLAN = '[Overview] Change Plan';
export const INIT_PRODUCT_OPTIONS = '[Overview] Init Product Options';
export const INIT_PRODUCT_OPTIONS_ATTRIBUTES = '[Overview] Init Product Options Attributes';
export const CHECK_PRODUCT_OPTION = '[Overview] Check Product Option';
export const UNCHECK_PRODUCT_OPTION = '[Overview] Uncheck Product Option';
export const CHANGE_PAYROLL_ENDING_UNIT_ATTRIBUTE = '[Overview] Change Payroll Ending Unit Attribute';
export const INIT_OPTIONS = '[Overview] Init Options';

export class PreviewAction implements Action {
    readonly type = PREVIEW;
}

export class PreviewSuccessAction implements Action {
    readonly type = PREVIEW_SUCCESS;

    constructor(public payload: CheckoutSummary) {}
}

export class PreviewFailAction implements Action {
    readonly type = PREVIEW_FAIL;
}

export class InitPlanAction implements Action {
    readonly type = INIT_PLAN;

    constructor(public payload: Plan) {}
}

export class InitProductOptionsAction implements Action {
    readonly type = INIT_PRODUCT_OPTIONS;

    constructor(public payload: string[]) {}
}

export class InitProductOptionsAttributesAction implements Action {
    readonly type = INIT_PRODUCT_OPTIONS_ATTRIBUTES;

    constructor(public payload: AddonAttributeCollection) {}
}

export class InitOptionsAction implements Action {
    readonly type = INIT_OPTIONS;
}

export class ChangePlanAction implements Action {
    readonly type = CHANGE_PLAN;

    constructor(public payload: Plan) {}
}

export class CheckProductOptionAction implements Action {
    readonly type = CHECK_PRODUCT_OPTION;

    constructor(public payload: string) {}
}

export class UncheckProductOptionAction implements Action {
    readonly type = UNCHECK_PRODUCT_OPTION;

    constructor(public payload: string) {}
}

export class ChangePayrollEndingUnitAttribute implements Action {
    readonly type = CHANGE_PAYROLL_ENDING_UNIT_ATTRIBUTE;

    constructor(public payload: number) {}
}

export type Actions =
    | PreviewAction
    | PreviewSuccessAction
    | PreviewFailAction
    | ChangePlanAction
    | InitPlanAction
    | InitProductOptionsAction
    | InitProductOptionsAttributesAction
    | InitOptionsAction
    | CheckProductOptionAction
    | UncheckProductOptionAction
    | ChangePayrollEndingUnitAttribute;
