import { Action } from '@ngrx/store';

import { CompanyProfile, CreditCard, PaymentMethod, SubscriptionInfo } from '../models';

export const LOAD = '[SubscriptionInfo] Load';
export const LOAD_SUCCESS = '[SubscriptionInfo] Load Success';
export const LOAD_FAIL = '[SubscriptionInfo] Load Fail';
export const UPDATE_PLAN = '[SubscriptionInfo] Update Plan';
export const UPDATE_PLAN_SUCCESS = '[SubscriptionInfo] Update Plan Success';
export const UPDATE_PLAN_FAIL = '[SubscriptionInfo] Update Plan Fail';
export const UPDATE_BILLING_ADDRESS = '[SubscriptionInfo] Update Billing Address';
export const UPDATE_BILLING_ADDRESS_SUCCESS = '[SubscriptionInfo] Update Billing Address Success';
export const UPDATE_BILLING_ADDRESS_FAIL = '[SubscriptionInfo] Update Billing Address Fail';
export const UPDATE_PAYMENT_METHOD = '[SubscriptionInfo] Update Payment Method';
export const UPDATE_PAYMENT_METHOD_SUCCESS = '[SubscriptionInfo] Update Payment Method Success';
export const UPDATE_PAYMENT_METHOD_FAIL = '[SubscriptionInfo] Update Payment Method Fail';
export const LOAD_CREDIT_CARD = '[SubscriptionInfo] Load Credit Card';
export const LOAD_CREDIT_CARD_SUCCESS = '[SubscriptionInfo] Load Credit Card Success';
export const LOAD_CREDIT_CARD_FAIL = '[SubscriptionInfo] Load Credit Card Fail';

export class LoadAction implements Action {
    readonly type = LOAD;
}

export class LoadSuccessAction implements Action {
    readonly type = LOAD_SUCCESS;

    constructor(public payload: SubscriptionInfo) {}
}

export class LoadFailAction implements Action {
    readonly type = LOAD_FAIL;

    constructor(public payload: string) {}
}

export class UpdatePlanAction implements Action {
    readonly type = UPDATE_PLAN;
}

export class UpdatePlanSuccessAction implements Action {
    readonly type = UPDATE_PLAN_SUCCESS;

    constructor(public payload: SubscriptionInfo) {}
}

export class UpdatePlanFailAction implements Action {
    readonly type = UPDATE_PLAN_FAIL;
}

export class UpdateBillingAddressAction implements Action {
    readonly type = UPDATE_BILLING_ADDRESS;

    constructor(public payload: CompanyProfile) {}
}

export class UpdateBillingAddressSuccessAction implements Action {
    readonly type = UPDATE_BILLING_ADDRESS_SUCCESS;

    constructor(public payload: CompanyProfile) {}
}

export class UpdateBillingAddressFailAction implements Action {
    readonly type = UPDATE_BILLING_ADDRESS_FAIL;

    constructor(public payload: CompanyProfile) {}
}

export class UpdatePaymentMethodAction implements Action {
    readonly type = UPDATE_PAYMENT_METHOD;

    constructor(public payload: PaymentMethod) {}
}

export class UpdatePaymentMethodSuccessAction implements Action {
    readonly type = UPDATE_PAYMENT_METHOD_SUCCESS;

    constructor(public payload: PaymentMethod) {}
}

export class UpdatePaymentMethodFailAction implements Action {
    readonly type = UPDATE_PAYMENT_METHOD_FAIL;

    constructor(public payload: PaymentMethod) {}
}

export class LoadCreditCardAction implements Action {
    readonly type = LOAD_CREDIT_CARD;
}

export class LoadCreditCardSuccessAction implements Action {
    readonly type = LOAD_CREDIT_CARD_SUCCESS;

    constructor(public payload: CreditCard) {}
}

export class LoadCreditCardFailAction implements Action {
    readonly type = LOAD_CREDIT_CARD_FAIL;
}

export type Actions =
    | LoadAction
    | LoadSuccessAction
    | LoadFailAction
    | UpdatePlanAction
    | UpdatePlanSuccessAction
    | UpdatePlanFailAction
    | UpdateBillingAddressAction
    | UpdateBillingAddressSuccessAction
    | UpdateBillingAddressFailAction
    | UpdatePaymentMethodAction
    | UpdatePaymentMethodSuccessAction
    | UpdatePaymentMethodFailAction
    | LoadCreditCardAction
    | LoadCreditCardSuccessAction
    | LoadCreditCardFailAction;
