import { NavigationExtras } from '@angular/router';
import { Action } from '@ngrx/store';

export const GO = '[Router] Go';
export const BACK = '[Router] Back';
export const FORWARD = '[Router] Forward';

interface Navigation {
    path: any[];
    query?: object;
    extras?: NavigationExtras;
}

export class Go implements Action {
    readonly type: any = GO;

    constructor(public payload: Navigation) {}
}

export class Back implements Action {
    readonly type: any = BACK;
}

export class Forward implements Action {
    readonly type: any = FORWARD;
}

export type Actions = Go | Back | Forward;
