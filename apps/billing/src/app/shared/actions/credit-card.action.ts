import { Action } from '@ngrx/store';

export const CREDIT_CARD_ERROR = '[Checkout] Credit Card Error';

export class CreditCardError implements Action {
    readonly type = CREDIT_CARD_ERROR;

    constructor(public payload: string) {}
}

export type Actions = CreditCardError;
