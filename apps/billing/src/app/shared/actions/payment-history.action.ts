import { Action } from '@ngrx/store';

import { Invoice } from '../models';

export const LOAD = '[PaymentHistory] Load';
export const LOAD_SUCCESS = '[PaymentHistory] Load Success';
export const LOAD_FAIL = '[PaymentHistory] Load Fail';

export class LoadAction implements Action {
    readonly type = LOAD;
}

export class LoadSuccessAction implements Action {
    readonly type = LOAD_SUCCESS;

    constructor(public payload: Invoice[]) {}
}

export class LoadFailAction implements Action {
    readonly type = LOAD_FAIL;
}

export type Actions = LoadAction | LoadSuccessAction | LoadFailAction;
