import { Action } from '@ngrx/store';

import { ProductCatalog } from '../models';

export const LOAD = '[Catalog] Load';
export const LOAD_SUCCESS = '[Catalog] Load Success';
export const LOAD_FAIL = '[Catalog] Load Fail';

export class LoadAction implements Action {
    readonly type = LOAD;
}

export class LoadSuccessAction implements Action {
    readonly type = LOAD_SUCCESS;

    constructor(public payload: ProductCatalog) {}
}

export class LoadFailAction implements Action {
    readonly type = LOAD_FAIL;
}

export type Actions = LoadAction | LoadSuccessAction | LoadFailAction;
