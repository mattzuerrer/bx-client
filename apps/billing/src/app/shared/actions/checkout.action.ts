import { Action } from '@ngrx/store';

import { CheckoutRequest, CheckoutSummary, CompanyProfile, PaymentMethod, Plan, SubscriptionInfo, Voucher } from '../models';

export const INIT = '[Checkout] Init';
export const PREVIEW = '[Checkout] Preview';
export const PREVIEW_SUCCESS = '[Checkout] Preview Success';
export const PREVIEW_FAIL = '[Checkout] Preview Fail';
export const CHECKOUT = '[Checkout]';
export const CHECKOUT_SUCCESS = '[Checkout] Success';
export const CHECKOUT_FAIL = '[Checkout] Fail';
export const CHANGE_PLAN = '[Checkout] Change Plan';
export const CHANGE_BILLING_INFO = '[Checkout] Change Billing Info';
export const CHANGE_PAYMENT_METHOD = '[Checkout] Change Payment Method';
export const CHANGE_CREDIT_CARD = '[Checkout] Change Credit Card';
export const CHANGE_VOUCHER_CODE = '[Checkout] Change Voucher Code';
export const STEP_1_LOADED = '[Checkout] Step 1 Loaded';
export const STEP_2_LOADED = '[Checkout] Step 2 Loaded';
export const STEP_3_LOADED = '[Checkout] Step 3 Loaded';
export const THANK_YOU_PAGE_LOADED = '[Checkout] Thank You Page Loaded';
export const CHECK_PRODUCT_OPTION = '[Checkout] Check Product Option';
export const UNCHECK_PRODUCT_OPTION = '[Checkout] Uncheck Product Option';
export const CHANGE_PAYROLL_ENDING_UNIT_ATTRIBUTE = '[Checkout] Change Payroll Ending Unit Attribute';

export class InitAction implements Action {
    readonly type = INIT;

    constructor(public payload: CheckoutRequest) {}
}

export class PreviewAction implements Action {
    readonly type = PREVIEW;
}

export class PreviewSuccessAction implements Action {
    readonly type = PREVIEW_SUCCESS;

    constructor(public payload: CheckoutSummary) {}
}

export class PreviewFailAction implements Action {
    readonly type = PREVIEW_FAIL;
}

export class CheckoutAction implements Action {
    readonly type = CHECKOUT;
}

export class CheckoutSuccessAction implements Action {
    readonly type = CHECKOUT_SUCCESS;

    constructor(public payload: SubscriptionInfo) {}
}

export class CheckoutFailAction implements Action {
    readonly type = CHECKOUT_FAIL;
}

export class CheckoutChangePlanAction implements Action {
    readonly type = CHANGE_PLAN;

    constructor(public payload: Plan) {}
}

export class CheckoutChangeBillingInfoAction implements Action {
    readonly type = CHANGE_BILLING_INFO;

    constructor(public payload: CompanyProfile) {}
}

export class CheckoutChangePaymentMethodAction implements Action {
    readonly type = CHANGE_PAYMENT_METHOD;

    constructor(public payload: PaymentMethod) {}
}

export class CheckoutChangeCreditCardAction implements Action {
    readonly type = CHANGE_CREDIT_CARD;

    constructor(public payload: string) {}
}

export class CheckoutChangeVoucherAction implements Action {
    readonly type = CHANGE_VOUCHER_CODE;

    constructor(public payload: Voucher) {}
}

export class Step1LoadedAction implements Action {
    readonly type = STEP_1_LOADED;
}

export class Step2LoadedAction implements Action {
    readonly type = STEP_2_LOADED;
}

export class Step3LoadedAction implements Action {
    readonly type = STEP_3_LOADED;
}

export class ThankYouPageLoadedAction implements Action {
    readonly type = THANK_YOU_PAGE_LOADED;
}

export class CheckoutCheckProductOptionAction implements Action {
    readonly type = CHECK_PRODUCT_OPTION;

    constructor(public payload: string) {}
}

export class CheckoutUncheckProductOptionAction implements Action {
    readonly type = UNCHECK_PRODUCT_OPTION;

    constructor(public payload: string) {}
}

export class ChangePayrollEndingUnitAttribute implements Action {
    readonly type = CHANGE_PAYROLL_ENDING_UNIT_ATTRIBUTE;

    constructor(public payload: number) {}
}

export type Actions =
    | InitAction
    | CheckoutAction
    | CheckoutSuccessAction
    | CheckoutFailAction
    | PreviewAction
    | PreviewSuccessAction
    | PreviewFailAction
    | CheckoutChangePlanAction
    | CheckoutChangeBillingInfoAction
    | CheckoutChangePaymentMethodAction
    | CheckoutChangeCreditCardAction
    | Step1LoadedAction
    | Step2LoadedAction
    | Step3LoadedAction
    | CheckoutChangeVoucherAction
    | ThankYouPageLoadedAction
    | CheckoutCheckProductOptionAction
    | CheckoutUncheckProductOptionAction
    | ChangePayrollEndingUnitAttribute;
