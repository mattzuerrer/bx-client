import { Action } from '@ngrx/store';

import { RsaSignature } from '../models';

export const LOAD = '[rsa-signature] Load';
export const LOAD_SUCCESS = '[rsa-signature] Load Success';
export const LOAD_FAIL = '[rsa-signature] Load Fail';
export const REMOVE = '[rsa-signature] Remove';

export class LoadAction implements Action {
    readonly type = LOAD;
}

export class LoadSuccessAction implements Action {
    readonly type = LOAD_SUCCESS;

    constructor(public payload: RsaSignature) {}
}

export class LoadFailAction implements Action {
    readonly type = LOAD_FAIL;
}

export class RemoveAction implements Action {
    readonly type = REMOVE;
}

export type Actions = LoadAction | LoadSuccessAction | LoadFailAction | RemoveAction;
