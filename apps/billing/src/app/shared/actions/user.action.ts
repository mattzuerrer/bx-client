import { Action } from '@ngrx/store';

import { ColumnStatus, User } from '../models';

export const LOAD = '[User] Load';
export const LOAD_SUCCESS = '[User] Load Success';
export const LOAD_FAIL = '[User] Load Fail';
export const SORT = '[User] Sort';
export const INVITE = '[User] Invite';
export const INVITE_SUCCESS = '[User] Invite Success';
export const INVITE_FAIL = '[User] Invite Fail';
export const REMOVE = '[User] Remove';
export const REMOVE_SUCCESS = '[User] Remove Success';
export const REMOVE_FAIL = '[User] Remove Fail';
export const FETCH_CURRENT = '[User] Fetch Current';
export const FETCH_CURRENT_SUCCESS = '[User] Fetch Current Success';
export const FETCH_CURRENT_FAIL = '[User] Fetch Current Fail';

export class LoadAction implements Action {
    readonly type = LOAD;
}

export class LoadSuccessAction implements Action {
    readonly type = LOAD_SUCCESS;

    constructor(public payload: User[]) {}
}

export class LoadFailAction implements Action {
    readonly type = LOAD_FAIL;
}

export class SortAction implements Action {
    readonly type = SORT;

    constructor(public payload: ColumnStatus[]) {}
}

export class InviteAction implements Action {
    readonly type = INVITE;

    constructor(public payload: string) {}
}

export class InviteSuccessAction implements Action {
    readonly type = INVITE_SUCCESS;

    constructor(public payload: User) {}
}

export class InviteFailAction implements Action {
    readonly type = INVITE_FAIL;

    constructor(public payload: { reason: string; retryAction: Action }) {}
}

export class RemoveAction implements Action {
    readonly type = REMOVE;

    constructor(public payload: number) {}
}

export class RemoveSuccessAction implements Action {
    readonly type = REMOVE_SUCCESS;

    constructor(public payload: number) {}
}

export class RemoveFailAction implements Action {
    readonly type = REMOVE_FAIL;

    constructor(public payload: number) {}
}

export class FetchCurrentAction implements Action {
    readonly type = FETCH_CURRENT;
}

export class FetchCurrentSuccessAction implements Action {
    readonly type = FETCH_CURRENT_SUCCESS;

    constructor(public payload: User) {}
}

export class FetchCurrentFailAction implements Action {
    readonly type = FETCH_CURRENT_FAIL;
}

export type Actions =
    | LoadAction
    | LoadSuccessAction
    | LoadFailAction
    | SortAction
    | InviteAction
    | InviteSuccessAction
    | InviteFailAction
    | RemoveAction
    | RemoveSuccessAction
    | RemoveFailAction
    | FetchCurrentAction
    | FetchCurrentSuccessAction
    | FetchCurrentFailAction;
