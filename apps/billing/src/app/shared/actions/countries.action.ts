import { Action } from '@ngrx/store';

import { Country } from '../models';

export const LOAD = '[Countries] Load';
export const LOAD_SUCCESS = '[Countries] Load Success';
export const LOAD_FAIL = '[Countries] Load Fail';

export class LoadAction implements Action {
    readonly type = LOAD;
}

export class LoadSuccessAction implements Action {
    readonly type = LOAD_SUCCESS;

    constructor(public payload: Country[]) {}
}

export class LoadFailAction implements Action {
    readonly type = LOAD_FAIL;
}

export type Actions = LoadAction | LoadSuccessAction | LoadFailAction;
