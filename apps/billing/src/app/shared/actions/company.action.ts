import { Action } from '@ngrx/store';

import { CompanyProfile } from '../models';

export const LOAD = '[CompanyProfile] Load';
export const LOAD_SUCCESS = '[CompanyProfile] Load Success';
export const LOAD_FAIL = '[CompanyProfile] Load Fail';
export const SAVE = '[CompanyProfile] Save';
export const SAVE_SUCCESS = '[CompanyProfile] Save Success';
export const SAVE_FAIL = '[CompanyProfile] Save Fail';

export class LoadAction implements Action {
    readonly type = LOAD;
}

export class LoadSuccessAction implements Action {
    readonly type = LOAD_SUCCESS;

    constructor(public payload: CompanyProfile) {}
}

export class LoadFailAction implements Action {
    readonly type = LOAD_FAIL;
}

export class SaveAction implements Action {
    readonly type = SAVE;

    constructor(public payload: CompanyProfile) {}
}

export class SaveSuccessAction implements Action {
    readonly type = SAVE_SUCCESS;

    constructor(public payload: CompanyProfile) {}
}

export class SaveFailAction implements Action {
    readonly type = SAVE_FAIL;

    constructor(public payload: CompanyProfile) {}
}

export type Actions = LoadAction | LoadSuccessAction | LoadFailAction | SaveAction | SaveSuccessAction | SaveFailAction;
