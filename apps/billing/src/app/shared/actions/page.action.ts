import { Action } from '@ngrx/store';

import { Breadcrumb } from '../models';

export const SET_BREADCRUMBS = '[Page] Set breadcrumbs';
export const CLEAR_BREADCRUMBS = '[Page] Clear breadcrumbs';
export const SET_PAGE_TITLE = '[Page] Set page title';
export const RELOAD = '[Page] Reload';
export const LOADING = '[Routing] Loading';
export const FORCE_LOADING = '[Routing] Force Loading';
export const IDLE = '[Routing] Idle';

export class SetBreadcrumbsAction implements Action {
    readonly type = SET_BREADCRUMBS;

    constructor(public payload: Breadcrumb[]) {}
}

export class ClearBreadcrumbsAction implements Action {
    readonly type = CLEAR_BREADCRUMBS;
}

export class SetPageTitleAction implements Action {
    readonly type = SET_PAGE_TITLE;

    constructor(public payload: string) {}
}

export class ReloadAction implements Action {
    readonly type = RELOAD;
}

export class LoadingAction implements Action {
    readonly type = LOADING;
}

export class ForceLoadingAction implements Action {
    readonly type = FORCE_LOADING;
}

export class IdleAction implements Action {
    readonly type = IDLE;
}

export type Actions =
    | SetBreadcrumbsAction
    | ClearBreadcrumbsAction
    | SetPageTitleAction
    | ReloadAction
    | LoadingAction
    | ForceLoadingAction
    | IdleAction;
