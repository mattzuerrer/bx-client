import { Action } from '@ngrx/store';

import { Addon } from '../models';

export const ACTIVATE = '[Addon] Activate';
export const ACTIVATE_SUCCESS = '[Addon] Activate Success';
export const ACTIVATE_FAIL = '[Addon] Activate Fail';
export const DEACTIVATE = '[Addon] Deactivate';
export const DEACTIVATE_SUCCESS = '[Addon] Deactivate Success';
export const DEACTIVATE_FAIL = '[Addon] Deactivate Fail';

export class ActivateAction implements Action {
    readonly type = ACTIVATE;

    constructor(public payload: Addon) {}
}

export class ActivateSuccessAction implements Action {
    readonly type = ACTIVATE_SUCCESS;

    constructor(public payload: Addon) {}
}

export class ActivateFailAction implements Action {
    readonly type = ACTIVATE_FAIL;

    constructor(public payload: Addon) {}
}

export class DeactivateAction implements Action {
    readonly type = DEACTIVATE;

    constructor(public payload: Addon) {}
}

export class DeactivateSuccessAction implements Action {
    readonly type = DEACTIVATE_SUCCESS;

    constructor(public payload: Addon) {}
}

export class DeactivateFailAction implements Action {
    readonly type = DEACTIVATE_FAIL;

    constructor(public payload: Addon) {}
}

export type Actions =
    | ActivateAction
    | ActivateSuccessAction
    | ActivateFailAction
    | DeactivateAction
    | DeactivateSuccessAction
    | DeactivateFailAction;
