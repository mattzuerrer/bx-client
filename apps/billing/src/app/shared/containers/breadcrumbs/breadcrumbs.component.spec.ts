import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';

import { StateServiceMockProvider, TranslateMockPipe } from '../../mocks';
import { StateService } from '../../services';

import { BreadcrumbsComponent } from './breadcrumbs.component';

describe('Breadcrumbs Container', () => {
    let component: BreadcrumbsComponent;
    let fixture: ComponentFixture<BreadcrumbsComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [BreadcrumbsComponent, TranslateMockPipe],
            providers: [StateServiceMockProvider]
        });
    });

    it('should create the component', () => {
        fixture = TestBed.createComponent(BreadcrumbsComponent);
        component = fixture.componentInstance;
        expect(component).toBeTruthy();
    });

    it(
        'should show breadcrumbs',
        inject([StateService], stateService => {
            stateService.pageBreadcrumbs$ = of([
                { translationKey: 'breadcrumb_1', url: 'breadcrumb_url_1' },
                { translationKey: 'breadcrumb_2', url: 'breadcrumb_url_2' },
                { translationKey: 'breadcrumb_3', url: 'breadcrumb_url_3' }
            ]);
            fixture = TestBed.createComponent(BreadcrumbsComponent);
            component = fixture.componentInstance;
            fixture.detectChanges();
            const items = fixture.debugElement.queryAll(By.css('.breadcrumbs-item'));
            expect(items.length).toBe(3);
            expect(items[0].query(By.css('a')).nativeElement.href).toContain('breadcrumb_url_1');
            expect(items[0].query(By.css('a')).nativeElement.textContent).toBe('breadcrumb_1');
            expect(items[0].query(By.css('.breadcrumbs-divider'))).toBeTruthy();
            expect(items[1].query(By.css('a')).nativeElement.href).toContain('breadcrumb_url_2');
            expect(items[1].query(By.css('a')).nativeElement.textContent).toBe('breadcrumb_2');
            expect(items[1].query(By.css('.breadcrumbs-divider'))).toBeTruthy();
            expect(items[2]).toBe(fixture.debugElement.query(By.css('.breadcrumbs-active-item')));
            expect(items[2].nativeElement.textContent).toContain('breadcrumb_3');
        })
    );
});
