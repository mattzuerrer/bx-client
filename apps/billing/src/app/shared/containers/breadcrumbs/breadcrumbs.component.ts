import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Breadcrumb } from '../../models';
import { StateService } from '../../services';

@Component({
    selector: 'app-breadcrumbs',
    templateUrl: 'breadcrumbs.component.html',
    styleUrls: ['breadcrumbs.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BreadcrumbsComponent implements OnInit {
    breadcrumbs$: Observable<Breadcrumb[]>;

    constructor(private stateService: StateService) {}

    ngOnInit(): void {
        this.breadcrumbs$ = this.stateService.pageBreadcrumbs$;
    }
}
