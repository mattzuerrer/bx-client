import { combineLatest as observableCombineLatest, Observable } from 'rxjs';

import { filter, take, tap } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { identity, negation } from '@bx-client/common';

import * as pageActions from '../../shared/actions/page.action';
import { GuardConfig } from '../models/GuardConfig';
import { StateService } from '../services';

@Injectable()
export abstract class StatesLoadedGuard implements CanActivate {
    protected config: GuardConfig[] = [];

    constructor(protected stateService: StateService) {}

    canActivate(): Observable<boolean> {
        this.loadStates();
        return this.waitForStatesLoaded().pipe(tap(() => this.stateService.dispatch(new pageActions.IdleAction())));
    }

    private waitForStatesLoaded(): Observable<boolean> {
        return observableCombineLatest(
            ...this.config.filter(guardConfig => guardConfig.required).map(guardConfig => guardConfig.stream),
            (...loadedStates: boolean[]) => loadedStates.every(identity)
        ).pipe(filter(identity), take(1));
    }

    private loadStates(): void {
        this.stateService.dispatch(new pageActions.LoadingAction());
        this.config.forEach(guardConfig =>
            guardConfig.stream.pipe(take(1), filter(negation)).subscribe(() => this.stateService.dispatch(guardConfig.action))
        );
    }
}
