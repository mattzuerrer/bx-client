import { PageGuard } from './page.guard';

export { PageGuard } from './page.guard';
export { StatesLoadedGuard } from './states-loaded.guard';

export const sharedGuards = [PageGuard];
