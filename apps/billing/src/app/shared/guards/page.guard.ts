import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';

import * as pageActions from '../actions/page.action';
import { StateService } from '../services';

@Injectable()
export class PageGuard implements CanActivate {
    constructor(private stateService: StateService) {}

    canActivate(route: ActivatedRouteSnapshot): boolean {
        const breadcrumbs = route.data.breadcrumbs;
        const title = route.data.title;
        const breadcrumbsAction = breadcrumbs
            ? new pageActions.SetBreadcrumbsAction(breadcrumbs)
            : new pageActions.ClearBreadcrumbsAction();
        this.stateService.dispatch(breadcrumbsAction);
        this.stateService.dispatch(new pageActions.SetPageTitleAction(title));
        return true;
    }
}
