import { inject, TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot } from '@angular/router';

import * as pageActions from '../actions/page.action';
import { StateServiceMockProvider } from '../mocks';
import { Breadcrumb } from '../models';
import { StateService } from '../services';

import { PageGuard } from './page.guard';

describe('Page Guard', () => {
    let guard: PageGuard;

    beforeEach(() => TestBed.configureTestingModule({ providers: [StateServiceMockProvider, PageGuard] }));

    beforeEach(
        inject([PageGuard], pageGuard => {
            guard = pageGuard;
        })
    );

    it(
        'should dispatch CLEAR_BREADCRUMBS and SET_PAGE_TITLE action',
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();
            const route = Object.assign(new ActivatedRouteSnapshot(), { data: { title: 'page_title' } });
            expect(guard.canActivate(route)).toBeTruthy();
            expect(stateService.dispatch).toHaveBeenCalledTimes(2);
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.ClearBreadcrumbsAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.SetPageTitleAction('page_title'));
        })
    );

    it(
        'should dispatch SET_BREADCRUMBS and SET_PAGE_TITLE action',
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();
            const breadcrumbs = [new Breadcrumb(), new Breadcrumb()];
            const route = Object.assign(new ActivatedRouteSnapshot(), { data: { title: 'page_title', breadcrumbs } });
            expect(guard.canActivate(route)).toBeTruthy();
            expect(stateService.dispatch).toHaveBeenCalledTimes(2);
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.SetBreadcrumbsAction(breadcrumbs));
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.SetPageTitleAction('page_title'));
        })
    );
});
