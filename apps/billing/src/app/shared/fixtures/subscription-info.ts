export const subscriptionInfo = {
    isTrial: false,
    plan: {
        key: 'plan_custom',
        billingPeriod: 'month',
        currency: 'CHF',
        price: 122,
        maxNumberOfUsers: 22,
        maxNumberOfPayrollEmployees: 55,
        totalPrice: 380,
        productVersion: 'v2',
        user: { key: 'user_bexio', currency: 'CHF', price: 15, billing_period: 'month' }
    },
    addons: [
        {
            key: 'addon_dropbox',
            currency: 'CHF',
            price: 14.99,
            billingPeriod: 'month',
            isDeprecated: false
        },
        {
            key: 'addon_kickshops',
            currency: 'CHF',
            price: 11.99,
            billingPeriod: 'month',
            isDeprecated: false
        },
        {
            key: 'addon_file_manager',
            currency: 'CHF',
            price: 22.99,
            billingPeriod: 'month',
            isDeprecated: true
        },
        {
            key: 'addon_payroll',
            price: 33.33,
            total_price: 355.66,
            currency: 'CHF',
            is_trial: false,
            trial_start_date: null,
            trial_end_date: null,
            billing_period: 'annual',
            remaining_trial_days: 0,
            custom_attributes: { ending_unit: 55 }
        },
        {
            key: 'addon_banking',
            price: 10,
            total_price: 120,
            currency: 'CHF',
            is_trial: false,
            trial_start_date: null,
            trial_end_date: null,
            billing_period: 'annual',
            remaining_trial_days: 7
        }
    ],
    billingInfo: {
        name: 'Edge Garden Services v4',
        address: 'street 77',
        postcode: '8645',
        city: 'Rapperswil5',
        mail: 'foo@bar.com',
        phoneMobile: '044 536 00 15',
        firstname: 'Heiri',
        lastname: 'M\u00fcller',
        countryCode: 'DE',
        accountantCompanyName: 'testAccountantCompany'
    },
    paymentMethod: {
        type: 'card',
        creditCard: {
            refId: '',
            type: 'visa',
            expirationMonth: '03',
            expirationYear: '2018',
            name: 'Juan Salarez',
            number: 'xxxxxxxxxxxx4389'
        }
    },
    payrollInfo: { numberOfUsers: 44 }
};

export const subscriptionInfoResponse = {
    is_trial: true,
    plan: {
        key: 'plan_all_in_one',
        price: 54,
        currency: 'CHF',
        billing_period: 'month',
        user: {
            key: 'user_bexio',
            price: 15,
            currency: 'CHF',
            quantity: 0,
            billing_period: 'month'
        }
    },
    addons: [
        {
            key: 'addon_google_authenticator',
            price: 0,
            currency: 'CHF',
            billing_period: 'month',
            is_deprecated: false
        },
        {
            key: 'addon_kickshops',
            price: 0,
            currency: 'CHF',
            billing_period: 'month',
            is_deprecated: false
        },
        {
            key: 'addon_mailchimp',
            price: 0,
            currency: 'CHF',
            billing_period: 'month',
            is_deprecated: false
        },
        {
            key: 'addon_mailxpert',
            price: 39,
            currency: 'CHF',
            billing_period: 'month',
            is_deprecated: false
        },
        {
            key: 'addon_pingen',
            price: 0,
            currency: 'CHF',
            billing_period: 'month',
            is_deprecated: false
        },
        {
            key: 'addon_phpeppershop_webshop',
            price: 0,
            currency: 'CHF',
            billing_period: 'month',
            is_deprecated: false
        }
    ],
    features: [],
    payment_information: {
        payment_type: 'credit_card',
        company_name: 'Edge Garden Services v4',
        email: 'foo@bar.com',
        firstname: 'Heiri',
        lastname: 'M\u00fcller',
        address: 'street 77',
        zip: '8645',
        city: 'Rapperswil5',
        phone_number: '044 536 00 15',
        country: 'DE',
        accountant_company_name: 'testAccountantCompany'
    },
    user_information: { user_max: 15, user_quantity: 1, payroll_employee_max: 4 }
};

export const subscriptionInfoTransformedResponse = {
    is_trial: true,
    plan: {
        key: 'plan_all_in_one',
        price: 54,
        currency: 'CHF',
        billing_period: 'month',
        max_number_of_payroll_employees: 4,
        max_number_of_users: 15,
        user: {
            key: 'user_bexio',
            price: 15,
            currency: 'CHF',
            quantity: 0,
            billing_period: 'month'
        }
    },
    addons: [
        {
            key: 'addon_google_authenticator',
            price: 0,
            currency: 'CHF',
            billing_period: 'month',
            is_deprecated: false
        },
        {
            key: 'addon_kickshops',
            price: 0,
            currency: 'CHF',
            billing_period: 'month',
            is_deprecated: false
        },
        {
            key: 'addon_mailchimp',
            price: 0,
            currency: 'CHF',
            billing_period: 'month',
            is_deprecated: false
        },
        {
            key: 'addon_mailxpert',
            price: 39,
            currency: 'CHF',
            billing_period: 'month',
            is_deprecated: false
        },
        {
            key: 'addon_pingen',
            price: 0,
            currency: 'CHF',
            billing_period: 'month',
            is_deprecated: false
        },
        {
            key: 'addon_phpeppershop_webshop',
            price: 0,
            currency: 'CHF',
            billing_period: 'month',
            is_deprecated: false
        }
    ],
    features: [],
    billing_info: {
        name: 'Edge Garden Services v4',
        address: 'street 77',
        postcode: '8645',
        city: 'Rapperswil5',
        mail: 'foo@bar.com',
        phone_mobile: '044 536 00 15',
        firstname: 'Heiri',
        lastname: 'M\u00fcller',
        country_code: 'DE',
        accountant_company_name: 'testAccountantCompany'
    },
    payment_method: { type: 'card' },
    payroll_info: { number_of_users: 4 }
};
