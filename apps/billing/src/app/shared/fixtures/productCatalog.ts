export const productCatalog = {
    plans: [
        {
            key: 'plan_standard',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 29,
            totalPrice: 290,
            productVersion: 'v1',
            user: {
                key: 'user_standard',
                currency: 'CHF',
                price: 10,
                billingPeriod: 'annual'
            }
        },
        {
            key: 'plan_standard',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 32,
            totalPrice: 32,
            productVersion: 'v1',
            user: { key: 'user_standard', currency: 'CHF', price: 10, billingPeriod: 'month' }
        },
        {
            key: 'plan_service',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 39,
            totalPrice: 390,
            productVersion: 'v1',
            user: { key: 'user_bexio', currency: 'CHF', price: 15, billingPeriod: 'annual' }
        },
        {
            key: 'plan_service',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 43,
            totalPrice: 43,
            productVersion: 'v1',
            user: { key: 'user_bexio', currency: 'CHF', price: 15, billingPeriod: 'month' }
        },
        {
            key: 'plan_trade',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 39,
            totalPrice: 390,
            productVersion: 'v1',
            user: { key: 'user_bexio', currency: 'CHF', price: 15, billingPeriod: 'annual' }
        },
        {
            key: 'plan_trade',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 43,
            totalPrice: 43,
            productVersion: 'v1',
            user: { key: 'user_bexio', currency: 'CHF', price: 15, billingPeriod: 'month' }
        },
        {
            key: 'plan_all_in_one',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 49,
            totalPrice: 490,
            productVersion: 'v1',
            user: { key: 'user_bexio', currency: 'CHF', price: 15, billingPeriod: 'annual' }
        },
        {
            key: 'plan_all_in_one',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 54,
            totalPrice: 54,
            productVersion: 'v1',
            user: { key: 'user_bexio', currency: 'CHF', price: 15, billingPeriod: 'month' }
        },
        {
            key: 'plan_starter',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 15,
            totalPrice: 150,
            productVersion: 'v2',
            maxNumberOfUsers: 1,
            freeSupport: false,
            user: { key: 'user_bexio', currency: 'CHF', price: 15, billingPeriod: 'annual' }
        },
        {
            key: 'plan_starter',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 17,
            totalPrice: 17,
            productVersion: 'v2',
            maxNumberOfUsers: 1,
            freeSupport: false,
            user: { key: 'user_bexio', currency: 'CHF', price: 15, billingPeriod: 'month' }
        },
        {
            key: 'plan_pro',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 25,
            totalPrice: 250,
            productVersion: 'v2',
            maxNumberOfUsers: 3,
            freeSupport: true,
            user: { key: 'user_bexio', currency: 'CHF', price: 15, billingPeriod: 'annual' }
        },
        {
            key: 'plan_pro',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 28,
            totalPrice: 28,
            productVersion: 'v2',
            maxNumberOfUsers: 3,
            freeSupport: true,
            user: { key: 'user_bexio', currency: 'CHF', price: 15, billingPeriod: 'month' }
        },
        {
            key: 'plan_pro_plus',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 35,
            totalPrice: 350,
            productVersion: 'v2',
            maxNumberOfUsers: 25,
            freeSupport: true,
            user: { key: 'user_bexio', currency: 'CHF', price: 15, billingPeriod: 'annual' }
        },
        {
            key: 'plan_pro_plus',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 38,
            totalPrice: 38,
            productVersion: 'v2',
            maxNumberOfUsers: 25,
            freeSupport: true,
            user: { key: 'user_bexio', currency: 'CHF', price: 15, billingPeriod: 'month' }
        }
    ],
    addons: [
        {
            key: 'addon_box_net',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: true
        },
        {
            key: 'addon_box_net',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: true
        },
        {
            key: 'addon_dropbox',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: true
        },
        {
            key: 'addon_dropbox',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: true
        },
        {
            key: 'addon_file_sharer',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: true
        },
        {
            key: 'addon_file_sharer',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: true
        },
        {
            key: 'addon_google_authenticator',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: false
        },
        {
            key: 'addon_google_authenticator',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: false
        },
        {
            key: 'addon_google_drive',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: true
        },
        {
            key: 'addon_google_drive',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: true
        },
        {
            key: 'addon_kickshops',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: false
        },
        {
            key: 'addon_kickshops',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: false
        },
        {
            key: 'addon_lead_management',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 10,
            totalPrice: 10,
            is_deprecated: true
        },
        {
            key: 'addon_lead_management',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 10,
            totalPrice: 100,
            is_deprecated: true
        },
        {
            key: 'addon_mailchimp',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: false
        },
        {
            key: 'addon_mailchimp',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: false
        },
        {
            key: 'addon_mailxpert',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 39,
            totalPrice: 39,
            is_deprecated: false
        },
        {
            key: 'addon_mailxpert',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 39,
            totalPrice: 390,
            is_deprecated: false
        },
        {
            key: 'addon_pingen',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: false
        },
        {
            key: 'addon_pingen',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: false
        },
        {
            key: 'addon_phpeppershop_pos',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: false
        },
        {
            key: 'addon_phpeppershop_pos',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: false
        },
        {
            key: 'addon_phpeppershop_webshop',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: false
        },
        {
            key: 'addon_phpeppershop_webshop',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 0,
            totalPrice: 0,
            is_deprecated: false
        },
        {
            key: 'addon_text_editor',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 4,
            totalPrice: 4,
            is_deprecated: true
        },
        {
            key: 'addon_text_editor',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 4,
            totalPrice: 40,
            is_deprecated: true
        },
        {
            key: 'addon_file_manager',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 6,
            totalPrice: 6,
            is_deprecated: true
        },
        {
            key: 'addon_file_manager',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 6,
            totalPrice: 60,
            is_deprecated: true
        },
        {
            key: 'addon_payroll',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 7,
            totalPrice: 7,
            is_deprecated: false,
            hasScalePrices: true,
            scale_prices: [
                {
                    starting_unit: 0,
                    ending_unit: 1,
                    price: 22,
                    total_price: 22
                },
                {
                    starting_unit: 5,
                    ending_unit: 25,
                    price: 55,
                    total_price: 55
                }
            ]
        },
        {
            key: 'addon_payroll',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 7,
            totalPrice: 70,
            is_deprecated: false,
            hasScalePrices: true,
            scale_prices: [
                {
                    starting_unit: 0,
                    ending_unit: 1,
                    price: 22,
                    total_price: 264
                },
                {
                    starting_unit: 5,
                    ending_unit: 25,
                    price: 55,
                    total_price: 660
                }
            ]
        },
        {
            key: 'addon_banking',
            billingPeriod: 'month',
            currency: 'CHF',
            price: 18,
            totalPrice: 18,
            is_deprecated: false
        },
        {
            key: 'addon_banking',
            billingPeriod: 'annual',
            currency: 'CHF',
            price: 18,
            totalPrice: 180,
            is_deprecated: false
        }
    ]
};

export const productCatalogResponse = {
    plans: [
        {
            key: 'plan_pro',
            product_version: 'v2',
            billing_period: 'annual',
            currency: 'CHF',
            price: 59,
            additional_user_max: 2,
            user_max: 3,
            payroll_employee_max: 4,
            user: null,
            total_price: 708
        },
        {
            key: 'plan_pro',
            product_version: 'v2',
            billing_period: 'month',
            currency: 'CHF',
            price: 65,
            additional_user_max: 2,
            user_max: 3,
            payroll_employee_max: 4,
            user: null,
            total_price: 65
        },
        {
            key: 'plan_pro_plus',
            product_version: 'v2',
            billing_period: 'month',
            currency: 'CHF',
            price: 109,
            additional_user_max: 24,
            user_max: 25,
            payroll_employee_max: 25,
            user: null,
            total_price: 109
        },
        {
            key: 'plan_pro_plus',
            product_version: 'v2',
            billing_period: 'annual',
            currency: 'CHF',
            price: 99,
            additional_user_max: 24,
            user_max: 25,
            payroll_employee_max: 25,
            user: null,
            total_price: 1188
        }
    ],
    addons: [
        {
            key: 'addon_phpeppershop_pos',
            billing_period: 'month',
            currency: 'CHF',
            price: 0,
            total_price: 0,
            trial_days: 0,
            is_deprecated: true
        },
        {
            key: 'addon_phpeppershop_pos',
            billing_period: 'annual',
            currency: 'CHF',
            price: 0,
            total_price: 0,
            trial_days: 0,
            is_deprecated: true
        },
        {
            key: 'addon_text_editor',
            billing_period: 'month',
            currency: 'CHF',
            price: 4,
            total_price: 4,
            trial_days: 0,
            is_deprecated: true
        },
        {
            key: 'addon_text_editor',
            billing_period: 'annual',
            currency: 'CHF',
            price: 4,
            total_price: 48,
            trial_days: 0,
            is_deprecated: true
        }
    ]
};

export const productCatalogResponseTransformed = {
    plans: [
        {
            key: 'plan_pro',
            product_version: 'v2',
            billing_period: 'annual',
            currency: 'CHF',
            price: 59,
            max_number_of_users: 3,
            max_number_of_payroll_employees: 4,
            user: null,
            total_price: 708
        },
        {
            key: 'plan_pro',
            product_version: 'v2',
            billing_period: 'month',
            currency: 'CHF',
            price: 65,
            max_number_of_users: 3,
            max_number_of_payroll_employees: 4,
            user: null,
            total_price: 65
        },
        {
            key: 'plan_pro_plus',
            product_version: 'v2',
            billing_period: 'month',
            currency: 'CHF',
            price: 109,
            max_number_of_users: 25,
            max_number_of_payroll_employees: 25,
            user: null,
            total_price: 109
        },
        {
            key: 'plan_pro_plus',
            product_version: 'v2',
            billing_period: 'annual',
            currency: 'CHF',
            price: 99,
            max_number_of_users: 25,
            max_number_of_payroll_employees: 25,
            user: null,
            total_price: 1188
        }
    ],
    addons: [
        {
            key: 'addon_phpeppershop_pos',
            billing_period: 'month',
            currency: 'CHF',
            price: 0,
            total_price: 0,
            trial_days: 0,
            is_deprecated: true
        },
        {
            key: 'addon_phpeppershop_pos',
            billing_period: 'annual',
            currency: 'CHF',
            price: 0,
            total_price: 0,
            trial_days: 0,
            is_deprecated: true
        },
        {
            key: 'addon_text_editor',
            billing_period: 'month',
            currency: 'CHF',
            price: 4,
            total_price: 4,
            trial_days: 0,
            is_deprecated: true
        },
        {
            key: 'addon_text_editor',
            billing_period: 'annual',
            currency: 'CHF',
            price: 4,
            total_price: 48,
            trial_days: 0,
            is_deprecated: true
        }
    ]
};
