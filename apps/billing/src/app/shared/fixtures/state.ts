export const state = {
    billing: {
        router: { path: '/overview' },
        page: {
            loaded: true,
            forceLoading: false,
            breadcrumbs: [{ translationKey: 'breadcrumbs.settings', url: '/admin' }, { translationKey: 'breadcrumbs.settings.overview' }]
        },
        users: {
            collection: [
                {
                    id: 5,
                    salutationType: 'male',
                    firstname: 'Derrick',
                    lastname: 'Montgomery',
                    email: 'derick.montgomery@124accounting.com',
                    isSuperadmin: false,
                    isAccountant: true
                },
                {
                    id: 1,
                    salutationType: 'male',
                    firstname: 'Ines',
                    lastname: 'Klug',
                    email: 'devtesting1@bexio.com',
                    isSuperadmin: true,
                    isAccountant: false
                },
                {
                    id: 2,
                    salutationType: 'male',
                    firstname: 'Manfred',
                    lastname: 'Kalafkalsch',
                    email: 'devtesting2@bexio.com',
                    isSuperadmin: false,
                    isAccountant: false
                },
                {
                    id: 4,
                    salutationType: 'male',
                    firstname: 'Lester',
                    lastname: 'Bryant',
                    email: 'lester.bryant@124accounting.com',
                    isSuperadmin: false,
                    isAccountant: true
                },
                {
                    id: 6,
                    salutationType: null,
                    firstname: null,
                    lastname: null,
                    email: 'supermega@bexio.com',
                    isSuperadmin: false,
                    isAccountant: false
                }
            ],
            columnConfig: [{ reversed: false, column: 'lastname', status: 'desc' }, { reversed: false, column: 'id', status: 'desc' }],
            loaded: false,
            invitingUser: false,
            removingUser: false
        },
        currentUser: {
            instance: {
                id: 1,
                salutationType: 'male',
                firstname: 'Ines',
                lastname: 'Klug',
                email: 'devtesting1@bexio.com',
                isSuperadmin: true,
                isAccountant: false
            },
            loaded: false
        },
        catalog: {
            instance: {
                plans: [
                    {
                        key: 'plan_standard',
                        billing_period: 'annual',
                        currency: 'CHF',
                        price: 29,
                        user: {
                            key: 'user_standard',
                            currency: 'CHF',
                            price: 10,
                            billing_period: 'annual'
                        }
                    },
                    {
                        key: 'plan_standard',
                        billing_period: 'month',
                        currency: 'CHF',
                        price: 32,
                        user: {
                            key: 'user_standard',
                            currency: 'CHF',
                            price: 10,
                            billing_period: 'month'
                        }
                    },
                    {
                        key: 'plan_service',
                        billing_period: 'annual',
                        currency: 'CHF',
                        price: 39,
                        user: {
                            key: 'user_bexio',
                            currency: 'CHF',
                            price: 15,
                            billing_period: 'annual'
                        }
                    },
                    {
                        key: 'plan_service',
                        billing_period: 'month',
                        currency: 'CHF',
                        price: 43,
                        user: {
                            key: 'user_bexio',
                            currency: 'CHF',
                            price: 15,
                            billing_period: 'month'
                        }
                    },
                    {
                        key: 'plan_trade',
                        billing_period: 'annual',
                        currency: 'CHF',
                        price: 39,
                        user: {
                            key: 'user_bexio',
                            currency: 'CHF',
                            price: 15,
                            billing_period: 'annual'
                        }
                    },
                    {
                        key: 'plan_trade',
                        billing_period: 'month',
                        currency: 'CHF',
                        price: 43,
                        user: {
                            key: 'user_bexio',
                            currency: 'CHF',
                            price: 15,
                            billing_period: 'month'
                        }
                    },
                    {
                        key: 'plan_all_in_one',
                        billing_period: 'annual',
                        currency: 'CHF',
                        price: 49,
                        user: {
                            key: 'user_bexio',
                            currency: 'CHF',
                            price: 15,
                            billing_period: 'annual'
                        }
                    },
                    {
                        key: 'plan_all_in_one',
                        billing_period: 'month',
                        currency: 'CHF',
                        price: 54,
                        user: {
                            key: 'user_bexio',
                            currency: 'CHF',
                            price: 15,
                            billing_period: 'month'
                        }
                    }
                ],
                addons: [
                    {
                        key: 'addon_box_net',
                        billing_period: 'month',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: true
                    },
                    {
                        key: 'addon_box_net',
                        billing_period: 'annual',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: true
                    },
                    {
                        key: 'addon_dropbox',
                        billing_period: 'month',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: true
                    },
                    {
                        key: 'addon_dropbox',
                        billing_period: 'annual',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: true
                    },
                    {
                        key: 'addon_file_sharer',
                        billing_period: 'month',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: true
                    },
                    {
                        key: 'addon_file_sharer',
                        billing_period: 'annual',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: true
                    },
                    {
                        key: 'addon_google_authenticator',
                        billing_period: 'month',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: false
                    },
                    {
                        key: 'addon_google_authenticator',
                        billing_period: 'annual',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: false
                    },
                    {
                        key: 'addon_google_drive',
                        billing_period: 'month',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: true
                    },
                    {
                        key: 'addon_google_drive',
                        billing_period: 'annual',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: true
                    },
                    {
                        key: 'addon_kickshops',
                        billing_period: 'month',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: false
                    },
                    {
                        key: 'addon_kickshops',
                        billing_period: 'annual',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: false
                    },
                    {
                        key: 'addon_lead_management',
                        billing_period: 'month',
                        currency: 'CHF',
                        price: 10,
                        is_deprecated: true
                    },
                    {
                        key: 'addon_lead_management',
                        billing_period: 'annual',
                        currency: 'CHF',
                        price: 10,
                        is_deprecated: true
                    },
                    {
                        key: 'addon_mailchimp',
                        billing_period: 'month',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: false
                    },
                    {
                        key: 'addon_mailchimp',
                        billing_period: 'annual',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: false
                    },
                    {
                        key: 'addon_mailxpert',
                        billing_period: 'month',
                        currency: 'CHF',
                        price: 39,
                        is_deprecated: false
                    },
                    {
                        key: 'addon_mailxpert',
                        billing_period: 'annual',
                        currency: 'CHF',
                        price: 39,
                        is_deprecated: false
                    },
                    {
                        key: 'addon_pingen',
                        billing_period: 'month',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: false
                    },
                    {
                        key: 'addon_pingen',
                        billing_period: 'annual',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: false
                    },
                    {
                        key: 'addon_phpeppershop_pos',
                        billing_period: 'month',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: false
                    },
                    {
                        key: 'addon_phpeppershop_pos',
                        billing_period: 'annual',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: false
                    },
                    {
                        key: 'addon_phpeppershop_webshop',
                        billing_period: 'month',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: false
                    },
                    {
                        key: 'addon_phpeppershop_webshop',
                        billing_period: 'annual',
                        currency: 'CHF',
                        price: 0,
                        is_deprecated: false
                    },
                    {
                        key: 'addon_text_editor',
                        billing_period: 'month',
                        currency: 'CHF',
                        price: 4,
                        is_deprecated: true
                    },
                    {
                        key: 'addon_text_editor',
                        billing_period: 'annual',
                        currency: 'CHF',
                        price: 4,
                        is_deprecated: true
                    },
                    {
                        key: 'addon_file_manager',
                        billing_period: 'month',
                        currency: 'CHF',
                        price: 6,
                        is_deprecated: true
                    },
                    {
                        key: 'addon_file_manager',
                        billing_period: 'annual',
                        currency: 'CHF',
                        price: 6,
                        is_deprecated: true
                    }
                ]
            },
            loaded: false
        },
        company: {
            profile: {
                id: 1,
                name: 'Edge Garden Services adasdasdasd',
                address: 'Gerbiweg 10',
                addressNr: '',
                postcode: '8143',
                city: 'Buechenegg',
                countryCode: 'CH',
                mail: '',
                phoneFixed: '044 227 92 91',
                phoneMobile: '',
                fax: '',
                url: '',
                skypeName: '',
                facebookName: '',
                twitterName: '',
                description: '',
                firstname: 'Ines',
                lastname: 'Klug',
                legalForm: null,
                ustIdnr: '',
                mwstNr: '',
                tradeRegisterNr: ''
            },
            loaded: false
        },
        countries: { collection: [], loaded: false },
        checkout: {
            instance: {
                plan: {
                    key: 'plan_service',
                    billing_period: 'annual',
                    currency: 'CHF',
                    price: 39,
                    user: {
                        key: 'user_bexio',
                        currency: 'CHF',
                        price: 15,
                        billing_period: 'annual'
                    }
                },
                billingInfo: {},
                paymentMethod: { type: 'card' },
                voucher: { code: 'bexio2018' },
                productOptions: ['addon_accountant_payroll'],
                productOptionsAttributes: { endingUnit: 5 }
            },
            preview: { plan: { user: {} }, users: {}, addons: [], total: {} },
            previewLoaded: false,
            requestProcessing: false
        },
        rsaSignature: { instance: null, loaded: false },
        subscriptionInfo: {
            instance: {
                isTrial: true,
                plan: {
                    key: 'plan_service',
                    currency: 'CHF',
                    price: 65.99,
                    billingPeriod: 'month',
                    user: { key: 'user_bexio', currency: 'CHF', price: 1.99, billingPeriod: 'month' }
                },
                addons: [
                    {
                        key: 'addon_dropbox',
                        currency: 'CHF',
                        price: 14.99,
                        billingPeriod: 'month',
                        isDeprecated: false
                    },
                    {
                        key: 'addon_kickshops',
                        currency: 'CHF',
                        price: 11.99,
                        billingPeriod: 'month',
                        isDeprecated: false
                    },
                    {
                        key: 'addon_file_manager',
                        currency: 'CHF',
                        price: 22.99,
                        billingPeriod: 'month',
                        isDeprecated: true
                    }
                ],
                billingInfo: {
                    id: 1,
                    name: 'Edge Garden Services v4',
                    address: 'Gerbiweg 10',
                    addressNr: '',
                    postcode: '8143',
                    city: 'Buechenegg',
                    countryCode: 'CH',
                    mail: 'gerb@garden.com',
                    phoneFixed: '044 227 92 91',
                    phoneMobile: '044 536 00 15',
                    fax: '',
                    url: '',
                    skypeName: '',
                    facebookName: '',
                    twitterName: '',
                    description: '',
                    firstname: 'Ines',
                    lastname: 'Klug',
                    legalForm: null,
                    ustIdnr: '',
                    mwstNr: '',
                    tradeRegisterNr: ''
                },
                paymentMethod: {
                    type: 'card',
                    creditCard: {
                        refId: '',
                        type: 'visa',
                        expirationMonth: '03',
                        expirationYear: '2018',
                        name: 'Juan Salarez',
                        number: 'xxxx xxxx xxxx 4389'
                    }
                }
            },
            loaded: false
        },
        paymentHistory: { collection: [], loaded: false },
        overview: {
            preview: { plan: { user: {} }, users: {}, addons: [], total: {} },
            previewLoaded: false,
            paymentMethod: { type: 'invoice', creditCard: {} },
            plan: {
                key: 'plan_all_in_one',
                billing_period: 'annual',
                currency: 'CHF',
                price: 49,
                user: { key: 'user_bexio', currency: 'CHF', price: 15, billing_period: 'annual' }
            },
            productOptions: ['addon_payroll'],
            productOptionsAttributes: {},
            planChangeLoading: false,
            originalState: null
        },
        addons: { loadingList: [] }
    }
};
