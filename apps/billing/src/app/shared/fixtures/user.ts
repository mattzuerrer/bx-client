export const users = [
    {
        id: 123,
        salutationType: 'Mr.',
        firstname: 'Stan',
        lastname: 'Smith',
        email: 'test@gmail.com',
        isSuperadmin: false,
        isAccountant: true
    },
    {
        id: 124,
        salutationType: 'Mr.',
        firstname: 'Stan',
        lastname: 'Smith',
        email: 'test@gmail.com',
        isSuperadmin: false,
        isAccountant: true
    }
];

export const user = {
    id: 123,
    salutationType: 'Mr.',
    firstname: 'Stan',
    lastname: 'Smith',
    email: 'test@gmail.com',
    isSuperadmin: false,
    isAccountant: true
};
