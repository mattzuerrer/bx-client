// tslint:disable:max-line-length

export const rsaSignature = {
    signature:
        'CYnJKH3hqCf9a6InchoirRpmeb5/KqgfeldDGRd7U1csZFKpgaG+EMx2DR7Z5Xaq+B2bynEFLUneJ8Omflc+hL7QE9SVDE3agIxDsRT+GJw2vIY4zPeBELG9yQL/hgXL1/yqsyH1yO1siXgfZjZfyaI9aIUMeuJw0PP6KHRCVqDJLwa+jqil+xP+mSVSxanQ71PonAaI+vYc9mtb0l0nzrOJvtM68tA7fxH0I6zpKdn6Z7JnB3oJ2eyqe0LJK+6QAkM12PYbT7lKggWDAS1FbcpysHj/yWIuFW3cN3ncYB48EOC48anVAbYT1Aq5RRgkdFu/crAm18XhW62Yz0xo2w==',
    token: 'JcBzaXqHVbVWJIlX1FYFDtmzVhj7iz8L',
    tenantId: '16025',
    locale: 'de_CH',
    url: 'https://apisandbox.zuora.com/apps/PublicHostedPageLite.do',
    paymentGateway: 'Stripe',
    id: '2c92c0f85ae13660015aebc298f118c8',
    key:
        'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhsCSC4DVlvtRDMgmEiebaHz6vw6wxZAQkqA8ELGynDUXD/ll/C3ioDM8jvTWUQZ/3wfSzCviVNgXQwABjnHmEpMF0kCN0mhmhxux1cdLBXKfjcQ2VJlsw+FFIRQ87X6zqf3QlmweoEykJh4Udn/43gLKvWCMDPS16rI5N1srJuGUbOG6hrSkSKJYBgfYcFfeJUIRVM9Wr0S+oNAj1a8wVOzDejU1gihPeG6oGSMnO5uTKz1fjDGr+awxh3JIq3k3DCC9QwjKChB5F9PF+ew3hBkF8zmeOX+oJUrCju0vtfhj4Gjbhi1TKn2/3CYNx0upycXAVueurtWcSJ+dv3QKdQIDAQAB'
};
