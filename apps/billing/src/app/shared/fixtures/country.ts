export const countries = [
    { id: 1, name: 'Switzerland', name_short: 'CH', iso_3166_alpha2: 'CH' },
    { id: 2, name: 'Germany', name_short: 'D', iso_3166_alpha2: 'DE' },
    { id: 3, name: 'Austria', name_short: 'A', iso_3166_alpha2: 'AT' },
    { id: 4, name: 'Brazil', name_short: 'BR', iso_3166_alpha2: 'BR' },
    { id: 5, name: 'Canada', name_short: 'CDN', iso_3166_alpha2: 'CA' },
    { id: 6, name: 'China', name_short: 'CN', iso_3166_alpha2: 'CN' },
    { id: 7, name: 'Czech Republic', name_short: 'CZ', iso_3166_alpha2: 'CZ' },
    { id: 8, name: 'France', name_short: 'F', iso_3166_alpha2: 'FR' },
    { id: 9, name: 'Great Britian', name_short: 'GB', iso_3166_alpha2: 'GB' },
    { id: 10, name: 'Italy', name_short: 'I', iso_3166_alpha2: 'IT' },
    { id: 11, name: 'Liechtenstein', name_short: 'FL', iso_3166_alpha2: 'LI' },
    { id: 12, name: 'Hong Kong', name_short: 'HK', iso_3166_alpha2: 'HK' },
    { id: 13, name: 'Portugal', name_short: 'PT', iso_3166_alpha2: 'PT' },
    { id: 14, name: 'Egypt', name_short: 'EG', iso_3166_alpha2: 'EG' },
    { id: 15, name: 'USA', name_short: 'USA', iso_3166_alpha2: 'US' },
    { id: 16, name: 'Spain', name_short: 'ES', iso_3166_alpha2: 'ES' },
    { id: 17, name: 'Australia', name_short: 'AU', iso_3166_alpha2: 'AU' },
    { id: 18, name: 'Argentina', name_short: 'AR', iso_3166_alpha2: 'AR' },
    { id: 19, name: 'Maldives', name_short: 'MV', iso_3166_alpha2: 'MV' },
    { id: 20, name: 'Bulgaria', name_short: 'BG', iso_3166_alpha2: 'BG' },
    { id: 21, name: 'Romania', name_short: 'RO', iso_3166_alpha2: 'RO' },
    { id: 22, name: 'Luxembourg', name_short: 'LU', iso_3166_alpha2: 'LU' },
    { id: 23, name: 'Turkey', name_short: 'TR', iso_3166_alpha2: 'TR' },
    { id: 24, name: 'Israel', name_short: 'ISR', iso_3166_alpha2: 'IL' },
    { id: 25, name: 'Netherlands', name_short: 'NL', iso_3166_alpha2: 'NL' },
    { id: 26, name: 'United Arab Emirates', name_short: 'VAE', iso_3166_alpha2: 'AE' },
    { id: 27, name: 'Belgium', name_short: 'B', iso_3166_alpha2: 'BE' },
    { id: 28, name: 'Singapore', name_short: 'SIN', iso_3166_alpha2: 'SG' },
    { id: 29, name: 'Montenegro', name_short: 'MTN', iso_3166_alpha2: 'ME' },
    { id: 30, name: 'Qatar', name_short: 'KAT', iso_3166_alpha2: 'QA' },
    { id: 31, name: 'Denmark', name_short: 'DK', iso_3166_alpha2: 'DK' },
    { id: 32, name: 'Sweden', name_short: 'SE', iso_3166_alpha2: 'SE' },
    { id: 33, name: 'Malaysia', name_short: 'MY', iso_3166_alpha2: 'MY' },
    { id: 34, name: 'Ireland', name_short: 'IRL', iso_3166_alpha2: 'IE' },
    { id: 35, name: 'Norway', name_short: 'NO', iso_3166_alpha2: 'NO' },
    { id: 36, name: 'Finland', name_short: 'FI', iso_3166_alpha2: 'FI' },
    { id: 37, name: 'Hungary', name_short: 'HU', iso_3166_alpha2: 'HU' }
];

export const countriesTransformed = [
    { name: 'Switzerland', id: 'CH' },
    { name: 'Germany', id: 'DE' },
    { name: 'Austria', id: 'AT' },
    { name: 'Brazil', id: 'BR' },
    { name: 'Canada', id: 'CA' },
    { name: 'China', id: 'CN' },
    { name: 'Czech Republic', id: 'CZ' },
    { name: 'France', id: 'FR' },
    { name: 'Great Britian', id: 'GB' },
    { name: 'Italy', id: 'IT' },
    { name: 'Liechtenstein', id: 'LI' },
    { name: 'Hong Kong', id: 'HK' },
    { name: 'Portugal', id: 'PT' },
    { name: 'Egypt', id: 'EG' },
    { name: 'USA', id: 'US' },
    { name: 'Spain', id: 'ES' },
    { name: 'Australia', id: 'AU' },
    { name: 'Argentina', id: 'AR' },
    { name: 'Maldives', id: 'MV' },
    { name: 'Bulgaria', id: 'BG' },
    { name: 'Romania', id: 'RO' },
    { name: 'Luxembourg', id: 'LU' },
    { name: 'Turkey', id: 'TR' },
    { name: 'Israel', id: 'IL' },
    { name: 'Netherlands', id: 'NL' },
    { name: 'United Arab Emirates', id: 'AE' },
    { name: 'Belgium', id: 'BE' },
    { name: 'Singapore', id: 'SG' },
    { name: 'Montenegro', id: 'ME' },
    { name: 'Qatar', id: 'QA' },
    { name: 'Denmark', id: 'DK' },
    { name: 'Sweden', id: 'SE' },
    { name: 'Malaysia', id: 'MY' },
    { name: 'Ireland', id: 'IE' },
    { name: 'Norway', id: 'NO' },
    { name: 'Finland', id: 'FI' },
    { name: 'Hungary', id: 'HU' }
];

export const country = {
    id: 1,
    name: 'Switzerland',
    nameShort: 'CH'
};
