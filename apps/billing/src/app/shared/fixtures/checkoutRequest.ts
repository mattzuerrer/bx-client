import { addonPayrollKey } from '../../../config/addonsMapping';
import { AddonAttribute, AddonAttributeCollection } from '../models';

export const checkoutRequest = {
    plan: {
        key: 'plan_standard',
        billingPeriod: 'annual',
        currency: 'CHF',
        price: 29,
        includedProductOptions: ['addon_payroll'],
        user: { key: 'user_standard', currency: 'CHF', price: 10, billingPeriod: 'annual' }
    },
    paymentMethod: {
        type: 'card',
        creditCard: { refId: 'oweriutv98muw345uokejrw9834oibjlkrthjm98u3o4h5kj34bkj5g3' }
    },
    billingInfo: {
        name: 'Edge Garden Services v4',
        address: 'Gerbiweg 10',
        postcode: '8143',
        city: 'Buechenegg',
        countryCode: 'CH',
        mail: 'gerb@garden.com',
        phoneFixed: '044 227 92 91',
        phoneMobile: '044 536 00 15',
        firstname: 'Ines',
        lastname: 'Klug',
        accountantCompanyName: 'testAccountantCompany'
    },
    voucher: { code: 'Xmas 2018' },
    productOptions: ['addon_payroll', 'addon_banking']
};

export const checkoutRequestTransformed = {
    plan: { key: 'plan_standard', billing_period: 'annual' },
    payment_method: { payment_type: 'invoice', payment_id: '', language: 'DE' },
    billing_info: {
        company_name: 'Edge Garden Services v4',
        email: 'gerb@garden.com',
        firstname: 'Ines',
        lastname: 'Klug',
        address: 'Gerbiweg 10',
        zip: '8143',
        city: 'Buechenegg',
        phone_number: '044 536 00 15',
        country: 'CH',
        accountant_company_name: 'testAccountantCompany'
    },
    voucher: { code: 'Xmas 2018' },
    addons: [{ key: 'addon_banking' }]
};

export const checkoutPreviewTransformed = {
    plan: { key: 'plan_standard', billing_period: 'annual' },
    voucher: { code: 'Xmas 2018' },
    addons: [{ key: 'addon_banking' }]
};

export const checkoutRequestPreview = {
    plan: { key: 'plan_standard', billing_period: 'annual' },
    voucher: { code: 'Xmas 2018' },
    addons: [{ key: 'addon_banking' }]
};

export const voucherRequest = {
    key: '',
    code: 'Xmas 2018',
    billingPeriod: ''
};

export const voucherTransformedRequest = {
    code: 'Xmas 2018'
};

export const planRequest = {
    key: 'plan_standard',
    billingPeriod: 'annual',
    currency: 'CHF',
    price: 29,
    user: { key: 'user_standard', currency: 'CHF', price: 10, billingPeriod: 'annual' }
};

export const planTransformedRequest = {
    key: 'plan_standard',
    billing_period: 'annual'
};

export const addonAttributes = [];
addonAttributes[addonPayrollKey] = new AddonAttribute(99);

export const checkoutRequestChangePlan = {
    plan: {
        key: 'plan_pro_plus',
        productVersion: 'v2',
        billingPeriod: 'annual',
        includedProductOptions: ['addon_banking']
    },
    productOptions: ['addon_banking', 'addon_payroll'],
    productOptionsAttributes: new AddonAttributeCollection(addonAttributes)
};

export const checkoutRequestChangePlanTransformed = {
    key: 'plan_pro_plus',
    billing_period: 'annual',
    addons: [{ key: 'addon_payroll', custom_attributes: { ending_unit: 99 } }]
};
