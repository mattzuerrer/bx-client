export const addon = {
    key: 'addon_dropbox',
    billing_period: 'month',
    currency: 'CHF',
    price: 0,
    is_deprecated: true
};
