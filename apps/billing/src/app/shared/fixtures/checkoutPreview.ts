export const checkoutPreviewMonth = {
    plan: {
        key: 'plan_service',
        billing_period: 'month',
        currency: 'CHF',
        price: 43,
        user: { key: 'user_bexio', currency: 'CHF', price: 15, billing_period: 'month' }
    },
    addons: [
        {
            key: 'addon_dropbox',
            billing_period: 'month',
            currency: 'CHF',
            price: 5,
            is_deprecated: true
        },
        {
            key: 'addon_kickshops',
            billing_period: 'month',
            currency: 'CHF',
            price: 10.99,
            is_deprecated: false
        },
        {
            key: 'addon_payroll',
            billing_period: 'month',
            currency: 'CHF',
            price: 29.99,
            is_deprecated: false
        },
        {
            key: 'addon_banking',
            billing_period: 'month',
            currency: 'CHF',
            price: 19.99,
            is_deprecated: false
        }
    ],
    users: {
        key: 'extra_users',
        price: 5.15,
        billingPeriod: 'month',
        currency: 'CHF',
        quantity: 3,
        totalPrice: 15.45
    },
    discounts: [
        { name: 'Discount 1', price: -20, currency: 'CHF', quantity: 1 },
        { name: 'Discount 2', price: -10, currency: 'CHF', quantity: 1 }
    ],

    voucherValidity: 'valid',

    total: { price: 168.8, total_price: 430, currency: 'CHF', billingPeriod: 'month' },

    netTotal: { price: 180.5, totalPrice: 450, currency: 'CHF', billingPeriod: 'month' }
};
