export const creditCard = {
    type: 'visa',
    expirationMonth: '03',
    expirationYear: '2018',
    name: 'Juan Salarez',
    number: 'xxxx xxxx xxxx 4389'
};

export const creditCardResponse = {
    type: 'visa',
    expiration_month: '03',
    expiration_year: '2018',
    name: 'Juan Salarez',
    number: 'xxxx xxxx xxxx 4389',
    payment_id: 'abcdef123456'
};

export const creditCardTransformedResponse = {
    type: 'visa',
    expiration_month: '03',
    expiration_year: '2018',
    name: 'Juan Salarez',
    number: 'xxxx xxxx xxxx 4389',
    ref_id: 'abcdef123456'
};
