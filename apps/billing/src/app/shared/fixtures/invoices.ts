export const invoices = [
    {
        id: '2c92c0945b422be4015b4791fca00a96',
        number: 'INV00143112',
        type: 'credit_note',
        state: 'open',
        amount: -5.3,
        date: '2017-04-18',
        currency: 'CHF'
    },
    {
        id: '2c92c0fb5b424173015b4761f88263d9',
        number: 'INV00143111',
        type: 'invoice',
        state: 'open',
        amount: 17.35,
        date: '2017-04-07',
        currency: 'CHF'
    }
];
