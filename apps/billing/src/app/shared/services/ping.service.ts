import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { appEnvironmentToken } from '@bx-client/env';
import { interval as observableInterval, Observable, of } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';

import { pingInterval } from '../../../config';

const pingResponse = 'pong';

@Injectable()
export class PingService {
    constructor(private httpClient: HttpClient, @Inject(appEnvironmentToken) private environment: AppEnvironment) {}

    /**
     * Pings server on given interval.
     *
     * @param interval
     * @returns {Observable<any>}
     */
    ping(interval: number = pingInterval): Observable<any> {
        return observableInterval(interval).pipe(mergeMap(() => this.pingOnce()));
    }

    /**
     * Pings user module.
     *
     * @returns {Observable<any>}
     */
    pingOnce(): Observable<any> {
        return !this.environment.isProduction ? of(pingResponse) : this.httpClient.get<any>(`/user/ping`).pipe(catchError(() => of(null)));
    }
}
