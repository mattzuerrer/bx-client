import { DOCUMENT } from '@angular/common';
import { Inject } from '@angular/core';

const domEvents = {
    init: 'ANGULAR_INIT',
    afterViewInit: 'ANGULAR_AFTER_VIEW_INIT',
    afterViewChecked: 'ANGULAR_AFTER_VIEW_CHECKED',
    loading: 'ANGULAR_LOADING',
    idle: 'ANGULAR_IDLE'
};

export class DomService {
    constructor(@Inject(DOCUMENT) private document: Document) {}

    dispatchInitEvent(): void {
        this.dispatchEvent(domEvents.init);
    }

    dispatchAfterViewInitEvent(): void {
        this.dispatchEvent(domEvents.afterViewInit);
    }

    dispatchAfterViewCheckedEvent(): void {
        this.dispatchEvent(domEvents.afterViewChecked);
    }

    dispatchLoadingEvent(): void {
        this.dispatchEvent(domEvents.loading);
    }

    dispatchIdleEvent(): void {
        this.dispatchEvent(domEvents.idle);
    }

    /**
     * Dispatch global event outside angular app.
     *
     * @param name
     */
    private dispatchEvent(name: string): void {
        const event = this.document.createEvent('Event');
        event.initEvent(name, true, true);
        this.document.dispatchEvent(event);
    }
}
