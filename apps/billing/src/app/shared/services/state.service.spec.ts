import { inject, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';

import * as fixtures from '../fixtures';
import { StoreMockProvider } from '../mocks';
import { Addon } from '../models';

import { StateService } from './state.service';

describe('State service', () => {
    const state = fixtures.state;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [StateService, StoreMockProvider]
        });
    });

    beforeEach(
        inject([Store], store => {
            store.setState(state);
        })
    );

    it(
        'should pass actions to store dispatch',
        inject([StateService, Store], (stateService, store) => {
            spyOn(store, 'dispatch');
            const action = {};
            stateService.dispatch(action);
            expect(store.dispatch).toHaveBeenCalledTimes(1);
            expect(store.dispatch).toHaveBeenCalledWith(action);
        })
    );

    /**
     * Overview
     */
    it(
        'should have mapping for overview state',
        inject([StateService], stateService => {
            stateService.overviewPreview$.subscribe(result => {
                expect(result).toBe(state.billing.overview.preview);
            });

            stateService.overviewPreviewLoaded$.subscribe(result => {
                expect(result).toBe(state.billing.overview.previewLoaded);
            });

            stateService.overviewPaymentMethod$.subscribe(result => {
                expect(result).toBe(state.billing.overview.paymentMethod);
            });

            stateService.overviewPlan$.subscribe(result => {
                expect(result).toBe(state.billing.overview.plan);
            });

            stateService.overviewProductOptions$.subscribe(result => {
                expect(result).toBe(state.billing.overview.productOptions);
            });

            stateService.isOverviewProductOptionsSelectCountValid$.subscribe(result => {
                expect(result).toBe(state.billing.overview.productOptions.length > 0);
            });

            stateService.overviewProductOptionsAttributes$.subscribe(result => {
                expect(result).toBe(state.billing.overview.productOptionsAttributes);
            });

            stateService.overviewSubscriptionChanged$.subscribe(result => {
                expect(result).toBe(state.billing.overview.originalState);
            });

            stateService.overviewChangePlanLoading$.subscribe(result => {
                expect(result).toBe(state.billing.overview.planChangeLoading);
            });
        })
    );

    /**
     * Routing
     */
    it(
        'should have mapping for page state',
        inject([StateService], stateService => {
            stateService.pageLoaded$.subscribe(result => {
                expect(result).toBe(state.billing.page.loaded);
            });

            stateService.pageBreadcrumbs$.subscribe(result => {
                expect(result).toBe(state.billing.page.breadcrumbs);
            });
        })
    );

    /**
     * Users
     */
    it(
        'should have mapping for users state',
        inject([StateService], stateService => {
            stateService.usersCollection$.subscribe(result => {
                expect(result).toBe(state.billing.users.collection);
            });

            stateService.usersLoaded$.subscribe(result => {
                expect(result).toBe(state.billing.users.loaded);
            });

            stateService.usersColumnConfig$.subscribe(result => {
                expect(result).toBe(state.billing.users.columnConfig);
            });

            stateService.usersInvitingUser$.subscribe(result => {
                expect(result).toBe(state.billing.users.invitingUser);
            });

            stateService.usersRemovingUser$.subscribe(result => {
                expect(result).toBe(state.billing.users.removingUser);
            });
        })
    );

    /**
     * Current user
     */
    it(
        'should have mapping for current user state',
        inject([StateService], stateService => {
            stateService.currentUserInstance$.subscribe(result => {
                expect(result).toBe(state.billing.currentUser.instance);
            });

            stateService.currentUserLoaded$.subscribe(result => {
                expect(result).toBe(state.billing.currentUser.loaded);
            });
        })
    );

    /**
     * Catalog
     */
    it(
        'should have mapping for catalog state',
        inject([StateService], stateService => {
            stateService.catalogInstance$.subscribe(result => {
                expect(result).toBe(state.billing.catalog.instance);
            });

            stateService.catalogPlans$.subscribe(result => {
                expect(result).toBe(state.billing.catalog.instance.plans);
            });

            stateService.catalogAddons$.subscribe(result => {
                expect(result).toBe(state.billing.catalog.instance.addons);
            });

            stateService.catalogLoaded$.subscribe(result => {
                expect(result).toBe(state.billing.catalog.loaded);
            });

            stateService.catalogPaidAddons$.subscribe(result => {
                expect(result).toEqual(state.billing.catalog.instance.addons.filter(addon => addon.price > 0));
            });
        })
    );

    /**
     * Company
     */
    it(
        'should have mapping for company state',
        inject([StateService], stateService => {
            stateService.companyProfile$.subscribe(result => {
                expect(result).toBe(state.billing.company.profile);
            });

            stateService.companyLoaded$.subscribe(result => {
                expect(result).toBe(state.billing.company.loaded);
            });
        })
    );

    /**
     * Countries
     */
    it(
        'should have mapping for countries state',
        inject([StateService], stateService => {
            stateService.countriesCollection$.subscribe(result => {
                expect(result).toBe(state.billing.countries.collection);
            });

            stateService.countriesLoaded$.subscribe(result => {
                expect(result).toBe(state.billing.countries.loaded);
            });
        })
    );

    /**
     * Checkout
     */
    it(
        'should have mapping for checkout state',
        inject([StateService], stateService => {
            stateService.checkoutInstance$.subscribe(result => {
                expect(result).toBe(state.billing.checkout.instance);
            });

            stateService.checkoutInstanceBillingInfo$.subscribe(result => {
                expect(result).toBe(state.billing.checkout.instance.billingInfo);
            });

            stateService.checkoutInstancePaymentMethod$.subscribe(result => {
                expect(result).toBe(state.billing.checkout.instance.paymentMethod);
            });

            stateService.checkoutInstancePlan$.subscribe(result => {
                expect(result).toBe(state.billing.checkout.instance.plan);
            });

            stateService.checkoutInstanceVoucher$.subscribe(result => {
                expect(result).toBe(state.billing.checkout.instance.voucher);
            });

            stateService.checkoutInstanceProductOptions$.subscribe(result => {
                expect(result).toBe(state.billing.checkout.instance.productOptions);
            });

            stateService.isOverviewProductOptionsSelectCountValid$.subscribe(result => {
                expect(result).toBe(state.billing.checkout.instance.productOptions.length > 0);
            });

            stateService.checkoutPreview$.subscribe(result => {
                expect(result).toBe(state.billing.checkout.preview);
            });

            stateService.checkoutPreviewLoaded$.subscribe(result => {
                expect(result).toBe(state.billing.checkout.previewLoaded);
            });

            stateService.checkoutRequestProcessing$.subscribe(result => {
                expect(result).toBe(state.billing.checkout.requestProcessing);
            });

            stateService.checkoutInstanceProductOptionsAtributes$.subscribe(result => {
                expect(result).toBe(state.billing.checkout.instance.productOptionsAttributes);
            });
        })
    );

    /**
     * RSA signature
     */
    it(
        'should have mapping for rsa signature state',
        inject([StateService], stateService => {
            stateService.rsaSignatureInstance$.subscribe(result => {
                expect(result).toBe(state.billing.rsaSignature.instance);
            });

            stateService.rsaSignatureLoaded$.subscribe(result => {
                expect(result).toBe(state.billing.rsaSignature.loaded);
            });
        })
    );

    /**
     * SubscriptionInfo
     */
    it(
        'should have mapping for subscription info state',
        inject([StateService], stateService => {
            stateService.subscriptionInfoInstance$.subscribe(result => {
                expect(result).toBe(state.billing.subscriptionInfo.instance);
            });

            stateService.subscriptionInfoInstanceIsTrial$.subscribe(result => {
                expect(result).toBe(state.billing.subscriptionInfo.instance.isTrial);
            });

            stateService.subscriptionInfoInstancePlan$.subscribe(result => {
                expect(result).toBe(state.billing.subscriptionInfo.instance.plan);
            });

            stateService.subscriptionInfoInstanceAddons$.subscribe(result => {
                expect(result).toEqual(<Addon[]>state.billing.subscriptionInfo.instance.addons);
            });

            stateService.subscriptionInfoInstancePaidAddons$.subscribe(result => {
                expect(result).toEqual(<Addon[]>state.billing.subscriptionInfo.instance.addons.filter(addon => addon.price > 0));
            });

            stateService.subscriptionInfoInstanceBillingInfo$.subscribe(result => {
                expect(result).toBe(state.billing.subscriptionInfo.instance.billingInfo);
            });

            stateService.subscriptionInfoInstancePaymentMethod$.subscribe(result => {
                expect(result).toBe(state.billing.subscriptionInfo.instance.paymentMethod);
            });

            stateService.subscriptionInfoInstancePaymentMethodCreditCard$.subscribe(result => {
                expect(result).toBe(state.billing.subscriptionInfo.instance.paymentMethod.creditCard);
            });

            stateService.subscriptionInfoLoaded$.subscribe(result => {
                expect(result).toBe(state.billing.subscriptionInfo.loaded);
            });

            stateService.subscriptionInfoInstancePlanUser$.subscribe(result => {
                expect(result).toBe(state.billing.subscriptionInfo.instance.plan.user);
            });
        })
    );

    /**
     * Payment history
     */
    it(
        'should have mapping for payment history state',
        inject([StateService], stateService => {
            stateService.paymentHistoryCollection$.subscribe(result => {
                expect(result).toBe(state.billing.paymentHistory.collection);
            });

            stateService.paymentHistoryLoaded$.subscribe(result => {
                expect(result).toBe(state.billing.paymentHistory.loaded);
            });
        })
    );
});
