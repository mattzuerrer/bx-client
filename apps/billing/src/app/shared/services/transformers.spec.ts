import * as fixtures from '../fixtures';

import {
    mapProductOptionsAttributes,
    transformBillingInfoRequest,
    transformBillingInfoResponse,
    transformChangePlanRequest,
    transformCheckoutPreview,
    transformCheckoutRequest,
    transformCountriesResponse,
    transformCreditCardResponse,
    transformProduct,
    transformProductCatalog,
    transformSubscriptionInfoResponse,
    transformVoucher
} from './transformers';

const paymentInformationResponse = fixtures.subscriptionInfoResponse.payment_information;

const billingInfoTransformed = fixtures.subscriptionInfoTransformedResponse.billing_info;

describe('Transformers', () => {
    it('should transform billing info response from payment information', () => {
        expect(transformBillingInfoResponse(paymentInformationResponse)).toEqual(billingInfoTransformed);
    });

    it('should transform subscription info response', () => {
        expect(transformSubscriptionInfoResponse(fixtures.subscriptionInfoResponse)).toEqual(fixtures.subscriptionInfoTransformedResponse);
    });

    it('should transform checkout request', () => {
        expect(transformCheckoutRequest(fixtures.checkoutRequest, 'DE')).toEqual(fixtures.checkoutRequestTransformed);
    });

    it('should transform checkout preview', () => {
        expect(transformCheckoutPreview(fixtures.checkoutRequest)).toEqual(fixtures.checkoutPreviewTransformed);
    });

    it('should transform payment information request from billing info', () => {
        const billingInfo = fixtures.subscriptionInfo.billingInfo;
        const paymentInformationRequestTransformed = Object.assign({}, paymentInformationResponse);
        delete paymentInformationRequestTransformed.payment_type;

        expect(transformBillingInfoRequest(billingInfo)).toEqual(paymentInformationRequestTransformed);
    });

    it('should transform countries', () => {
        expect(transformCountriesResponse(fixtures.countries)).toEqual(fixtures.countriesTransformed);
    });

    it('should transform voucher', () => {
        expect(transformVoucher(fixtures.voucherRequest)).toEqual(fixtures.voucherTransformedRequest);
    });

    it('should transform plan', () => {
        expect(transformProduct(fixtures.planRequest)).toEqual(fixtures.planTransformedRequest);
    });

    it('should transform credit card', () => {
        expect(transformCreditCardResponse(fixtures.creditCardResponse)).toEqual(fixtures.creditCardTransformedResponse);
    });

    it('should transform product catalog', () => {
        expect(transformProductCatalog(fixtures.productCatalogResponse)).toEqual(fixtures.productCatalogResponseTransformed);
    });

    it('should transform change plan request', () => {
        expect(transformChangePlanRequest(fixtures.checkoutRequestChangePlan)).toEqual(fixtures.checkoutRequestChangePlanTransformed);
    });

    it('should transform product options attributes', () => {
        expect(mapProductOptionsAttributes({ endingUnit: 99 })).toEqual({
            custom_attributes: { ending_unit: 99 }
        });
        expect(mapProductOptionsAttributes(null)).toBeNull();
    });
});
