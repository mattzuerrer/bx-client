import { HttpClient } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { appEnvironmentToken } from '@bx-client/env';
import { HttpClientMockProvider } from '@bx-client/http/src/mocks';
import { of, throwError } from 'rxjs';

import { PingService } from './ping.service';

const appEnvironmentProviderDevelopment = {
    provide: appEnvironmentToken,
    useValue: { isProduction: false }
};

const appEnvironmentProviderProduction = {
    provide: appEnvironmentToken,
    useValue: { isProduction: true }
};

describe('Dom service', () => {
    beforeEach(() => {
        jasmine.clock().uninstall();
        jasmine.clock().install();
        TestBed.configureTestingModule({
            providers: [PingService, HttpClientMockProvider, appEnvironmentProviderProduction]
        });
    });

    it('should not send ping request for development env', () => {
        TestBed.configureTestingModule({ providers: [appEnvironmentProviderDevelopment] });
        inject([PingService, HttpClient], (pingService, httpService) => {
            spyOn(httpService, 'get').and.returnValue(of('pong'));
            let pingResult = '';
            pingService.pingOnce().subscribe(result => (pingResult = result));
            expect(httpService.get).toHaveBeenCalledTimes(0);
            expect(pingResult).toBe('pong');
        })();
    });

    it(
        'should send ping request once',
        inject([PingService, HttpClient], (pingService, httpService) => {
            spyOn(httpService, 'get').and.returnValue(of('pong'));
            let pingResult = '';
            pingService.pingOnce().subscribe(result => (pingResult = result));
            expect(httpService.get).toHaveBeenCalledWith('/user/ping');
            expect(pingResult).toBe('pong');
        })
    );

    it(
        'should send ping request on interval duration',
        inject([PingService, HttpClient], (pingService, httpService) => {
            spyOn(httpService, 'get').and.returnValue(of('pong'));
            let pingResult = '';
            pingService.ping(1000).subscribe(result => (pingResult = result));
            jasmine.clock().tick(5000);
            expect(httpService.get).toHaveBeenCalledWith('/user/ping');
            expect(httpService.get).toHaveBeenCalledTimes(5);
            expect(pingResult).toBe('pong');
        })
    );

    it(
        'should ignore errors in ping request',
        inject([PingService, HttpClient], (pingService, httpService) => {
            spyOn(httpService, 'get').and.returnValue(throwError('some error'));
            let pingResult = '';
            pingService.pingOnce().subscribe(result => (pingResult = result));
            expect(httpService.get).toHaveBeenCalledWith('/user/ping');
            expect(pingResult).toBe(null);
        })
    );
});
