import { AddonAttributeCollection, PaymentMethod } from '../models';

export const transformBillingInfoResponse = paymentInformation => ({
    name: paymentInformation.company_name,
    firstname: paymentInformation.firstname,
    lastname: paymentInformation.lastname,
    address: paymentInformation.address,
    mail: paymentInformation.email,
    country_code: paymentInformation.country,
    phone_mobile: paymentInformation.phone_number,
    postcode: paymentInformation.zip,
    city: paymentInformation.city,
    accountant_company_name: paymentInformation.accountant_company_name
});

export const transformPaymentMethodResponse = paymentInformation => {
    let type = PaymentMethod.invoice;
    if (paymentInformation.payment_type === 'credit_card') {
        type = PaymentMethod.card;
    }
    return { type };
};

export const transformSubscriptionInfoResponse = subscriptionInfo => ({
    is_trial: subscriptionInfo.is_trial,
    plan: Object.assign({}, subscriptionInfo.plan, {
        max_number_of_payroll_employees: subscriptionInfo.user_information.payroll_employee_max,
        max_number_of_users: subscriptionInfo.user_information.user_max
    }),
    addons: subscriptionInfo.addons,
    features: subscriptionInfo.features,
    billing_info: transformBillingInfoResponse(subscriptionInfo.payment_information || {}),
    payment_method: transformPaymentMethodResponse(subscriptionInfo.payment_information || {}),
    payroll_info: { number_of_users: subscriptionInfo.user_information.payroll_employee_max }
});

export const transformPaymentMethodRequest = (paymentMethod, language) => ({
    payment_type: paymentMethod.isCard ? 'credit_card' : 'invoice',
    payment_id: paymentMethod.isCard ? paymentMethod.creditCard.refId : '',
    language
});

export const transformCreditCardResponse = creditCard => ({
    expiration_month: creditCard.expiration_month,
    expiration_year: creditCard.expiration_year,
    name: creditCard.name,
    number: creditCard.number,
    type: creditCard.type,
    ref_id: creditCard.payment_id
});

export const transformBillingInfoRequest = billingInfo => ({
    company_name: billingInfo.name,
    email: billingInfo.mail,
    firstname: billingInfo.firstname,
    lastname: billingInfo.lastname,
    address: billingInfo.address,
    zip: billingInfo.postcode,
    city: billingInfo.city,
    phone_number: billingInfo.phoneMobile,
    country: billingInfo.countryCode,
    accountant_company_name: billingInfo.accountantCompanyName
});

export const transformProduct = product => ({ key: product.key, billing_period: product.billingPeriod });

export const mapProductOptionsAttributes = attributes => {
    return attributes ? { custom_attributes: { ending_unit: attributes.endingUnit } } : null;
};

export const filterIncludedProductOptions = (productOptions, planAddons, productOptionsAttributes = new AddonAttributeCollection()) =>
    productOptions.filter(key => planAddons.indexOf(key) === -1).map(key => {
        return Object.assign({ key }, mapProductOptionsAttributes(productOptionsAttributes.get(key)));
    });

export const transformChangePlanRequest = checkoutRequest =>
    Object.assign({}, transformProduct(checkoutRequest.plan), {
        addons: filterIncludedProductOptions(
            checkoutRequest.productOptions,
            checkoutRequest.plan.includedProductOptions,
            checkoutRequest.productOptionsAttributes
        )
    });

export const transformVoucher = voucher => ({ code: voucher.code });

export const transformCheckoutRequest = (checkoutRequest, language) => ({
    plan: transformProduct(checkoutRequest.plan),
    payment_method: transformPaymentMethodRequest(checkoutRequest.paymentMethod, language),
    billing_info: transformBillingInfoRequest(checkoutRequest.billingInfo),
    voucher: transformVoucher(checkoutRequest.voucher),
    addons: filterIncludedProductOptions(
        checkoutRequest.productOptions,
        checkoutRequest.plan.includedProductOptions,
        checkoutRequest.productOptionsAttributes
    )
});

export const transformCheckoutPreview = checkoutRequest => ({
    plan: transformProduct(checkoutRequest.plan),
    voucher: transformVoucher(checkoutRequest.voucher),
    addons: filterIncludedProductOptions(
        checkoutRequest.productOptions,
        checkoutRequest.plan.includedProductOptions,
        checkoutRequest.productOptionsAttributes
    )
});

export const transformCountriesResponse = countries => countries.map(country => ({ id: country.iso_3166_alpha2, name: country.name }));

export const transformProductCatalog = productCatalog => ({
    plans: productCatalog.plans.map(plan => ({
        key: plan.key,
        product_version: plan.product_version,
        billing_period: plan.billing_period,
        currency: plan.currency,
        price: plan.price,
        max_number_of_users: plan.user_max,
        max_number_of_payroll_employees: plan.payroll_employee_max,
        user: plan.user,
        total_price: plan.total_price
    })),
    addons: productCatalog.addons
});
