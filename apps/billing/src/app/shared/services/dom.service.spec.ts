import { DOCUMENT } from '@angular/common';
import { inject, TestBed } from '@angular/core/testing';
import { noop } from 'rxjs';

import { DomService } from './dom.service';

const event = {
    initEvent: () => noop()
};

const documentStub = {
    createEvent: () => event,
    dispatchEvent: () => noop()
};

const documentProvider = {
    provide: DOCUMENT,
    useValue: documentStub
};

describe('Dom service', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({ providers: [DomService, documentProvider] });
    });

    it(
        'should dispatch ANGULAR_INIT event on dispatchInitEvent',
        inject([DomService, DOCUMENT], (domService, document) => {
            spyOn(document, 'createEvent').and.callThrough();
            spyOn(document, 'dispatchEvent').and.callThrough();
            spyOn(event, 'initEvent').and.callThrough();
            domService.dispatchInitEvent();
            expect(document.createEvent).toHaveBeenCalledWith('Event');
            expect(event.initEvent).toHaveBeenCalledWith('ANGULAR_INIT', true, true);
            expect(document.dispatchEvent).toHaveBeenCalledWith(event);
        })
    );

    it(
        'should dispatch ANGULAR_AFTER_VIEW_INIT event on dispatchAfterViewInitEvent',
        inject([DomService, DOCUMENT], (domService, document) => {
            spyOn(document, 'createEvent').and.callThrough();
            spyOn(document, 'dispatchEvent').and.callThrough();
            spyOn(event, 'initEvent').and.callThrough();
            domService.dispatchAfterViewInitEvent();
            expect(document.createEvent).toHaveBeenCalledWith('Event');
            expect(event.initEvent).toHaveBeenCalledWith('ANGULAR_AFTER_VIEW_INIT', true, true);
            expect(document.dispatchEvent).toHaveBeenCalledWith(event);
        })
    );

    it(
        'should dispatch ANGULAR_AFTER_VIEW_CHECKED event on dispatchAfterViewCheckedEvent',
        inject([DomService, DOCUMENT], (domService, document) => {
            spyOn(document, 'createEvent').and.callThrough();
            spyOn(document, 'dispatchEvent').and.callThrough();
            spyOn(event, 'initEvent').and.callThrough();
            domService.dispatchAfterViewCheckedEvent();
            expect(document.createEvent).toHaveBeenCalledWith('Event');
            expect(event.initEvent).toHaveBeenCalledWith('ANGULAR_AFTER_VIEW_CHECKED', true, true);
            expect(document.dispatchEvent).toHaveBeenCalledWith(event);
        })
    );

    it(
        'should dispatch ANGULAR_LOADING event on dispatchLoadingEvent',
        inject([DomService, DOCUMENT], (domService, document) => {
            spyOn(document, 'createEvent').and.callThrough();
            spyOn(document, 'dispatchEvent').and.callThrough();
            spyOn(event, 'initEvent').and.callThrough();
            domService.dispatchLoadingEvent();
            expect(document.createEvent).toHaveBeenCalledWith('Event');
            expect(event.initEvent).toHaveBeenCalledWith('ANGULAR_LOADING', true, true);
            expect(document.dispatchEvent).toHaveBeenCalledWith(event);
        })
    );

    it(
        'should dispatch ANGULAR_IDLE event on dispatchIdleEvent',
        inject([DomService, DOCUMENT], (domService, document) => {
            spyOn(document, 'createEvent').and.callThrough();
            spyOn(document, 'dispatchEvent').and.callThrough();
            spyOn(event, 'initEvent').and.callThrough();
            domService.dispatchIdleEvent();
            expect(document.createEvent).toHaveBeenCalledWith('Event');
            expect(event.initEvent).toHaveBeenCalledWith('ANGULAR_IDLE', true, true);
            expect(document.dispatchEvent).toHaveBeenCalledWith(event);
        })
    );
});
