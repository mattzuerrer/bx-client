import { HttpResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { UtilitiesService } from '@bx-client/common';
import { HttpService } from '@bx-client/http';
import { classToPlain } from 'class-transformer';
import { Observable } from 'rxjs';

import { languageToken } from '@bx-client/env';

import {
    Addon,
    CheckoutRequest,
    CheckoutSummary,
    CompanyProfile,
    Country,
    CreditCard,
    Invoice,
    PaymentMethod,
    ProductCatalog,
    RsaSignature,
    SubscriptionInfo,
    User
} from '../models';

import {
    transformBillingInfoRequest,
    transformBillingInfoResponse,
    transformChangePlanRequest,
    transformCheckoutPreview,
    transformCheckoutRequest,
    transformCountriesResponse,
    transformCreditCardResponse,
    transformPaymentMethodRequest,
    transformPaymentMethodResponse,
    transformProduct,
    transformProductCatalog,
    transformSubscriptionInfoResponse
} from './transformers';

@Injectable()
export class ApiService {
    constructor(
        private httpService: HttpService,
        private utilitiesService: UtilitiesService,
        @Inject(languageToken) private language: string
    ) {}

    /**
     * Get users.
     *
     * @param orderBy
     * @returns {Observable<User[]>}
     */
    getUsers(orderBy: string): Observable<User[]> {
        const searchParams = [{ field: 'is_accountant', value: false }];
        const data = JSON.stringify(searchParams);
        return this.httpService.post(User, `users/search?order_by=${orderBy}`, data);
    }

    /**
     * Get user.
     *
     * @param id
     * @returns {Observable<User>}
     */
    getUser(id: number | string): Observable<User> {
        return this.httpService.get(User, `users/${id}`);
    }

    /**
     * Get current user.
     *
     * @returns {Observable<User>}
     */
    getCurrentUser(): Observable<User> {
        return this.getUser('me');
    }

    /**
     * Get company profile.
     *
     * @returns {Observable<CompanyProfile>}
     */
    getCompanyProfile(): Observable<CompanyProfile> {
        return this.httpService.get(CompanyProfile, 'company_profile', {}, companyProfiles => companyProfiles[0]);
    }

    /**
     *
     * @param newCompanyProfile
     * @returns {Observable<CompanyProfile>}
     */
    saveCompanyProfile(newCompanyProfile: CompanyProfile): Observable<CompanyProfile> {
        const data = JSON.stringify(this.utilitiesService.camelToSnake(classToPlain(newCompanyProfile)));
        const id = newCompanyProfile.id;
        return this.httpService.patch(CompanyProfile, `company_profile/${id}`, data, {}, transformProductCatalog);
    }

    /**
     * Invite user.
     *
     * @param email
     * @returns {Observable<User>}
     */
    inviteUser(email: string): Observable<User> {
        const data = JSON.stringify({ email });
        return this.httpService.post(User, 'users', data);
    }

    /**
     * Remove user.
     *
     * @param id
     * @returns {Observable<boolean>}
     */
    removeUser(id: number): Observable<boolean> {
        return this.httpService.remove(null, `users/${id}`, {}, response => response.success);
    }

    /**
     * Get zuora rsa signature.
     *
     * @return {Observable<RsaSignature>}
     */
    getRsaSignature(): Observable<RsaSignature> {
        return this.httpService.post(RsaSignature, 'billing/payment/credit_card_signature', '');
    }

    /**
     * Get product catalog.
     *
     * @return {Observable<ProductCatalog>}
     */
    getProductCatalog(): Observable<ProductCatalog> {
        return this.httpService.get(ProductCatalog, 'products', {}, transformProductCatalog);
    }

    /**
     * Get countries.
     *
     * @returns {Observable<Country[]>}
     */
    getCountries(): Observable<Country[]> {
        return this.httpService.get(Country, 'country', {}, transformCountriesResponse);
    }

    /**
     * Get checkout preview.
     *
     * @returns {Observable<CheckoutSummary>}
     */
    getCheckoutPreview(checkoutRequest: CheckoutRequest): Observable<CheckoutSummary> {
        const data = JSON.stringify(transformCheckoutPreview(checkoutRequest));
        return this.httpService.post(CheckoutSummary, 'billing/subscription/preview', data);
    }

    /**
     * Complete checkout.
     *
     * @param checkoutRequest
     * @returns {Observable<SubscriptionInfo>}
     */
    sendCompletedCheckout(checkoutRequest: CheckoutRequest): Observable<SubscriptionInfo> {
        const data = JSON.stringify(transformCheckoutRequest(checkoutRequest, this.language));

        return this.httpService.post(SubscriptionInfo, 'billing/subscription', data, {}, transformSubscriptionInfoResponse);
    }

    /**
     * Get subscriptionInfo
     *
     * @returns {Observable<SubscriptionInfo>}
     */
    getSubscriptionInfo(): Observable<SubscriptionInfo> {
        return this.httpService.get(SubscriptionInfo, 'billing/subscription', {}, transformSubscriptionInfoResponse);
    }

    /**
     * Change plan
     *
     * @param checkoutRequest
     * @returns {Observable<SubscriptionInfo>}
     */
    changeSubscriptionPlan(checkoutRequest: CheckoutRequest): Observable<SubscriptionInfo> {
        const data = JSON.stringify(this.utilitiesService.camelToSnake(transformChangePlanRequest(checkoutRequest)));
        return this.httpService.patch(SubscriptionInfo, 'billing/subscription/plan', data, {}, transformSubscriptionInfoResponse);
    }

    /**
     * Change billing address
     *
     * @param billingAddress
     * @returns {observable<CompanyProfile>}
     */
    changeBillingAddress(billingAddress: CompanyProfile): Observable<CompanyProfile> {
        const data = JSON.stringify(transformBillingInfoRequest(billingAddress));
        return this.httpService.patch(CompanyProfile, 'billing/payment/information', data, {}, transformBillingInfoResponse);
    }

    /**
     * Change payment method
     *
     * @param newPaymentMethod
     * @returns {observable<PaymentMethod>}
     */
    changePaymentMethod(newPaymentMethod: PaymentMethod): Observable<PaymentMethod> {
        const data = JSON.stringify(transformPaymentMethodRequest(newPaymentMethod, this.language));
        return this.httpService.patch(PaymentMethod, 'billing/payment/method', data, {}, transformPaymentMethodResponse);
    }

    /**
     * Get payment history.
     *
     * @returns {Observable<Invoice[]>}
     */
    getPaymentHistory(): Observable<Invoice[]> {
        return this.httpService.get(Invoice, 'billing/invoices');
    }

    /**
     *
     * @param id
     */
    getPaymentInvoice(id: number | string): Observable<HttpResponse<Blob>> {
        return this.httpService.get(null, `billing/invoices/${id}/pdf`, { responseType: 'blob', observe: 'response' });
    }

    /**
     * Activate addon.
     *
     * @param addon
     * @returns {Observable<SubscriptionInfo>}
     */
    activateAddon(addon: Addon): Observable<SubscriptionInfo> {
        const data = JSON.stringify(this.utilitiesService.camelToSnake(transformProduct(addon)));
        return this.httpService.post(SubscriptionInfo, 'billing/subscription/addons', data, {}, transformSubscriptionInfoResponse);
    }

    /**
     * Deactivate addon.
     *
     * @param addon
     * @returns {Observable<SubscriptionInfo>}
     */
    deactivateAddon(addon: Addon): Observable<SubscriptionInfo> {
        return this.httpService.remove(
            SubscriptionInfo,
            `billing/subscription/addons/${addon.key}`,
            {},
            transformSubscriptionInfoResponse
        );
    }

    /**
     * Get Credit card info
     *
     * @returns {Observable<CreditCard>}
     */
    getCreditCardInfo(): Observable<CreditCard> {
        return this.httpService.get(CreditCard, 'billing/payment/credit_card', {}, transformCreditCardResponse);
    }
}
