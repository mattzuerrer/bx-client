import { ApiService } from './api.service';
import { DomService } from './dom.service';
import { PingService } from './ping.service';
import { StateService } from './state.service';

export { ApiService } from './api.service';
export { DomService } from './dom.service';
export { PingService } from './ping.service';
export { StateService } from './state.service';

export const sharedServices = [ApiService, DomService, PingService, StateService];
