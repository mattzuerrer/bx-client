import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import {
    Addon,
    AddonAttributeCollection,
    Breadcrumb,
    CheckoutRequest,
    CheckoutSummary,
    ColumnStatus,
    CompanyProfile,
    Country,
    CreditCard,
    Invoice,
    PaymentMethod,
    PayrollInfo,
    Plan,
    Product,
    ProductCatalog,
    RsaSignature,
    SubscriptionInfo,
    User,
    Voucher
} from '../models';
import * as fromRoot from '../reducers';

@Injectable()
export class StateService {
    /**
     * Overview
     */

    overviewPreview$: Observable<CheckoutSummary> = this.store.select(fromRoot.getOverviewPreview);

    overviewPreviewLoaded$: Observable<boolean> = this.store.select(fromRoot.getOverviewPreviewLoaded);

    overviewPaymentMethod$: Observable<PaymentMethod> = this.store.select(fromRoot.getOverviewPaymentMethod);

    overviewPlan$: Observable<Plan> = this.store.select(fromRoot.getOverviewPlan);

    overviewProductOptions$: Observable<string[]> = this.store.select(fromRoot.getOverviewProductOptions);

    isOverviewProductOptionsSelectCountValid$: Observable<boolean> = this.store.select(
        fromRoot.getIsOverviewProductOptionsSelectCountValid
    );

    overviewProductOptionsAttributes$: Observable<any> = this.store.select(fromRoot.getOverviewProductOptionsAttributes);

    overviewSubscriptionChanged$: Observable<boolean> = this.store.select(fromRoot.getOverviewSubscriptionChanged);

    overviewChangePlanLoading$: Observable<boolean> = this.store.select(fromRoot.getOverviewChangePlanLoading);

    /**
     * Page
     */

    pageLoaded$: Observable<boolean> = this.store.select(fromRoot.getPageLoaded);

    pageBreadcrumbs$: Observable<Breadcrumb[]> = this.store.select(fromRoot.getPageBreadcrumbs);

    /**
     * Users
     */

    usersCollection$: Observable<User[]> = this.store.select(fromRoot.getUsersCollection);

    usersLoaded$: Observable<boolean> = this.store.select(fromRoot.getUsersLoaded);

    usersColumnConfig$: Observable<ColumnStatus[]> = this.store.select(fromRoot.getUsersColumnConfig);

    usersInvitingUser$: Observable<boolean> = this.store.select(fromRoot.getUsersInvitingUser);

    usersRemovingUser$: Observable<boolean> = this.store.select(fromRoot.getUsersRemovingUser);

    /**
     * Current user
     */

    currentUserInstance$: Observable<User> = this.store.select(fromRoot.getCurrentUserInstance);

    currentUserLoaded$: Observable<boolean> = this.store.select(fromRoot.getCurrentUserLoaded);

    /**
     * Catalog
     */

    catalogInstance$: Observable<ProductCatalog> = this.store.select(fromRoot.getCatalogInstance);

    catalogPlans$: Observable<Plan[]> = this.store.select(fromRoot.getCatalogPlans);

    catalogAddons$: Observable<Addon[]> = this.store.select(fromRoot.getCatalogAddons);

    catalogPaidAddons$: Observable<Addon[]> = this.store.select(fromRoot.getCatalogPaidAddons);

    catalogLoaded$: Observable<boolean> = this.store.select(fromRoot.getCatalogLoaded);

    /**
     * Company
     */

    companyProfile$: Observable<CompanyProfile> = this.store.select(fromRoot.getCompanyProfile);

    companyLoaded$: Observable<boolean> = this.store.select(fromRoot.getCompanyLoaded);

    /**
     * Countries
     */

    countriesCollection$: Observable<Country[]> = this.store.select(fromRoot.getCountriesCollection);

    countriesLoaded$: Observable<boolean> = this.store.select(fromRoot.getCountriesLoaded);

    /**
     * Checkout
     */

    checkoutInstance$: Observable<CheckoutRequest> = this.store.select(fromRoot.getCheckoutInstance);

    checkoutInstanceBillingInfo$: Observable<CompanyProfile> = this.store.select(fromRoot.getCheckoutInstanceBillingInfo);

    checkoutInstancePaymentMethod$: Observable<PaymentMethod> = this.store.select(fromRoot.getCheckoutInstancePaymentMethod);

    checkoutInstancePlan$: Observable<Plan> = this.store.select(fromRoot.getCheckoutInstancePlan);

    checkoutInstanceVoucher$: Observable<Voucher> = this.store.select(fromRoot.getCheckoutInstanceVoucher);

    checkoutInstanceProductOptions$: Observable<string[]> = this.store.select(fromRoot.getCheckoutInstanceProductOptions);

    checkoutInstanceProductOptionsAtributes$: Observable<AddonAttributeCollection> = this.store.select(
        fromRoot.getCheckoutInstanceProductOptionsAtributes
    );

    isCheckoutProductOptionsSelectCountValid$: Observable<boolean> = this.store.select(
        fromRoot.getIsCheckoutProductOptionsSelectCountValid
    );

    checkoutPreview$: Observable<CheckoutSummary> = this.store.select(fromRoot.getCheckoutPreview);

    checkoutPreviewLoaded$: Observable<boolean> = this.store.select(fromRoot.getCheckoutPreviewLoaded);

    checkoutRequestProcessing$: Observable<boolean> = this.store.select(fromRoot.getCheckoutRequestProcessing);

    /**
     * RSA signature
     */

    rsaSignatureInstance$: Observable<RsaSignature> = this.store.select(fromRoot.getRsaSignatureInstance);

    rsaSignatureLoaded$: Observable<boolean> = this.store.select(fromRoot.getRsaSignatureLoaded);

    /**
     * SubscriptionInfo
     */

    subscriptionInfoInstance$: Observable<SubscriptionInfo> = this.store.select(fromRoot.getSubscriptionInfoInstance);

    subscriptionInfoInstanceIsTrial$: Observable<boolean> = this.store.select(fromRoot.getSubscriptionInfoInstanceIsTrial);

    subscriptionInfoInstancePlan$: Observable<Plan> = this.store.select(fromRoot.getSubscriptionInfoInstancePlan);

    subscriptionInfoInstanceAddons$: Observable<Addon[]> = this.store.select(fromRoot.getSubscriptionInfoInstanceAddons);

    subscriptionInfoInstancePaidAddons$: Observable<Addon[]> = this.store.select(fromRoot.getSubscriptionInfoInstancePaidAddons);

    subscriptionInfoInstanceBillingInfo$: Observable<CompanyProfile> = this.store.select(fromRoot.getSubscriptionInfoInstanceBillingInfo);

    subscriptionInfoInstancePaymentMethod$: Observable<PaymentMethod> = this.store.select(
        fromRoot.getSubscriptionInfoInstancePaymentMethod
    );

    subscriptionInfoInstancePaymentMethodCreditCard$: Observable<CreditCard> = this.store.select(
        fromRoot.getSubscriptionInfoInstancePaymentMethodCreditCard
    );

    subscriptionInfoInstancePayrollInfo$: Observable<PayrollInfo> = this.store.select(fromRoot.getSubscriptionInfoInstancePayrollInfo);

    subscriptionInfoLoaded$: Observable<boolean> = this.store.select(fromRoot.getSubscriptionInfoLoaded);

    subscriptionInfoInstancePlanUser$: Observable<Product> = this.store.select(fromRoot.getSubscriptionInfoInstancePlanUser);

    /**
     * Addons
     */
    addonsLoadingList$: Observable<string[]> = this.store.select(fromRoot.getAddonsLoadingList);

    /**
     * Payment history
     */

    paymentHistoryCollection$: Observable<Invoice[]> = this.store.select(fromRoot.getPaymentHistoryCollection);

    paymentHistoryLoaded$: Observable<boolean> = this.store.select(fromRoot.getPaymentHistoryLoaded);

    constructor(private store: Store<fromRoot.State>) {}

    dispatch(action: Action): void {
        this.store.dispatch(action);
    }
}
