import { inject, TestBed } from '@angular/core/testing';
import { UtilitiesService } from '@bx-client/common';
import { apiToken, appEnvironmentToken, languageToken } from '@bx-client/env';
import { HttpService } from '@bx-client/http';
import { HttpServiceMockProvider } from '@bx-client/http/src/mocks';
import { classToPlain, plainToClass } from 'class-transformer';
import { of } from 'rxjs';

import { addonPayrollKey } from '../../../config/addonsMapping';
import * as fixtures from '../fixtures';
import { productCatalogResponseTransformed } from '../fixtures/productCatalog';
import { UtilitiesServiceMockProvider } from '../mocks';
import {
    Addon,
    AddonAttribute,
    AddonAttributeCollection,
    CheckoutRequest,
    CheckoutSummary,
    CompanyProfile,
    Country,
    CreditCard,
    Invoice,
    PaymentMethod,
    Plan,
    ProductCatalog,
    RsaSignature,
    SubscriptionInfo,
    User
} from '../models';
import { ApiService } from '../services';

import {
    transformBillingInfoRequest,
    transformBillingInfoResponse,
    transformCheckoutRequest,
    transformCountriesResponse,
    transformCreditCardResponse,
    transformPaymentMethodResponse,
    transformProduct,
    transformProductCatalog,
    transformSubscriptionInfoResponse
} from './transformers';

const apiProvider = {
    provide: apiToken,
    useValue: 'API'
};

const appEnvironmentProviderProduction = {
    provide: appEnvironmentToken,
    useValue: { isProduction: true }
};

const languageProvider = {
    provide: languageToken,
    useValue: 'de'
};

describe('Api service', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                ApiService,
                HttpServiceMockProvider,
                UtilitiesServiceMockProvider,
                apiProvider,
                appEnvironmentProviderProduction,
                languageProvider
            ]
        });
    });

    it(
        'should fetch users',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const users = fixtures.users;
            const data = '[{"field":"is_accountant","value":false}]';
            spyOn(httpService, 'post').and.returnValue(of(plainToClass(User, users)));
            apiService.getUsers('id_asc,email_desc').subscribe(result => {
                expect(httpService.post).toHaveBeenCalledTimes(1);
                expect(httpService.post).toHaveBeenCalledWith(User, 'users/search?order_by=id_asc,email_desc', data);
                expect(result).toEqual(plainToClass(User, users));
            });
        })
    );

    it(
        'should fetch single user',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const user = fixtures.user;
            spyOn(httpService, 'get').and.returnValue(of(plainToClass(User, user)));
            apiService.getUser(123).subscribe(result => {
                expect(httpService.get).toHaveBeenCalledTimes(1);
                expect(httpService.get).toHaveBeenCalledWith(User, 'users/123');
                expect(result).toEqual(plainToClass(User, user));
            });
        })
    );

    it(
        'should fetch current user',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const user = fixtures.user;
            spyOn(httpService, 'get').and.returnValue(of(plainToClass(User, user)));
            apiService.getCurrentUser().subscribe(result => {
                expect(httpService.get).toHaveBeenCalledTimes(1);
                expect(httpService.get).toHaveBeenCalledWith(User, 'users/me');
                expect(result).toEqual(plainToClass(User, user));
            });
        })
    );

    it(
        'should fetch company profile',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const company = fixtures.companyProfile;
            spyOn(httpService, 'get').and.returnValue(of(plainToClass(CompanyProfile, company)));
            apiService.getCompanyProfile().subscribe(result => {
                expect(httpService.get).toHaveBeenCalledTimes(1);
                expect(httpService.get).toHaveBeenCalledWith(CompanyProfile, 'company_profile', {}, jasmine.any(Function));
                expect(result).toEqual(plainToClass(CompanyProfile, company));
            });
        })
    );

    it(
        'should save company profile',
        inject([ApiService, HttpService, UtilitiesService], (apiService, httpService, utilitiesService) => {
            const company = fixtures.companyProfile;
            company.id = 123;
            const data = JSON.stringify(classToPlain(company));
            spyOn(utilitiesService, 'camelToSnake').and.callThrough();
            spyOn(httpService, 'patch').and.returnValue(of(plainToClass(CompanyProfile, company)));
            apiService.saveCompanyProfile(company).subscribe(result => {
                expect(httpService.patch).toHaveBeenCalledTimes(1);
                expect(httpService.patch).toHaveBeenCalledWith(CompanyProfile, 'company_profile/123', data, {}, transformProductCatalog);
                expect(utilitiesService.camelToSnake).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(CompanyProfile, company));
            });
        })
    );

    it(
        'should invite user',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const user = fixtures.user;
            const email = 'test@joes.test.com';
            const data = JSON.stringify({ email });
            spyOn(httpService, 'post').and.returnValue(of(plainToClass(User, user)));
            apiService.inviteUser(email).subscribe(result => {
                expect(httpService.post).toHaveBeenCalledTimes(1);
                expect(httpService.post).toHaveBeenCalledWith(User, 'users', data);
                expect(result).toEqual(plainToClass(User, user));
            });
        })
    );

    it(
        'should remove user',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const id = 123;
            const response = {};
            spyOn(httpService, 'remove').and.returnValue(of(response));
            apiService.removeUser(id).subscribe(result => {
                expect(httpService.remove).toHaveBeenCalledTimes(1);
                expect(httpService.remove).toHaveBeenCalledWith(null, 'users/123', {}, jasmine.any(Function));
                expect(result).toBe(response);
            });
        })
    );

    it(
        'should fetch rsa signature',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const rsaSignature = fixtures.rsaSignature;
            spyOn(httpService, 'post').and.returnValue(of(plainToClass(RsaSignature, rsaSignature)));
            apiService.getRsaSignature().subscribe(result => {
                expect(httpService.post).toHaveBeenCalledTimes(1);
                expect(httpService.post).toHaveBeenCalledWith(RsaSignature, 'billing/payment/credit_card_signature', '');
                expect(result).toEqual(plainToClass(RsaSignature, rsaSignature));
            });
        })
    );

    it(
        'should fetch product catalog',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const productCatalogObj = plainToClass(ProductCatalog, transformProductCatalog(fixtures.productCatalogResponse));
            spyOn(httpService, 'get').and.returnValue(of(productCatalogObj));
            apiService.getProductCatalog().subscribe(result => {
                expect(httpService.get).toHaveBeenCalledTimes(1);
                expect(httpService.get).toHaveBeenCalledWith(ProductCatalog, 'products', {}, transformProductCatalog);
                expect(result).toEqual(plainToClass(ProductCatalog, productCatalogResponseTransformed));
            });
        })
    );

    it(
        'should fetch countries',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const countries = fixtures.countries;
            const countriesTransformed = transformCountriesResponse(countries);
            spyOn(httpService, 'get').and.returnValue(of(plainToClass(Country, countriesTransformed)));
            apiService.getCountries().subscribe(result => {
                expect(httpService.get).toHaveBeenCalledTimes(1);
                expect(httpService.get).toHaveBeenCalledWith(Country, 'country', {}, transformCountriesResponse);
                expect(result).toEqual(plainToClass(Country, countriesTransformed));
            });
        })
    );

    it(
        'should fetch checkout preview',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const checkoutPreview = fixtures.checkoutPreviewMonth;
            const checkoutRequest = Object.assign(new CheckoutRequest(), fixtures.checkoutRequest);
            const data = JSON.stringify(fixtures.checkoutRequestPreview);
            spyOn(httpService, 'post').and.returnValue(of(plainToClass(CheckoutSummary, checkoutPreview)));
            apiService.getCheckoutPreview(checkoutRequest).subscribe(result => {
                expect(httpService.post).toHaveBeenCalledTimes(1);
                expect(httpService.post).toHaveBeenCalledWith(CheckoutSummary, 'billing/subscription/preview', data);
                expect(result).toEqual(plainToClass(CheckoutSummary, checkoutPreview));
            });
        })
    );

    it(
        'should send completed checkout',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const subscriptionInfoTransformed = transformSubscriptionInfoResponse(fixtures.subscriptionInfoResponse);
            spyOn(httpService, 'post').and.returnValue(of(plainToClass(SubscriptionInfo, subscriptionInfoTransformed)));
            const data = JSON.stringify(transformCheckoutRequest(fixtures.checkoutRequest, 'de'));
            apiService.sendCompletedCheckout(fixtures.checkoutRequest).subscribe(result => {
                expect(httpService.post).toHaveBeenCalledTimes(1);
                expect(httpService.post).toHaveBeenCalledWith(
                    SubscriptionInfo,
                    'billing/subscription',
                    data,
                    {},
                    transformSubscriptionInfoResponse
                );
                expect(result).toEqual(plainToClass(SubscriptionInfo, subscriptionInfoTransformed));
            });
        })
    );

    it(
        'should fetch subscription info',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const subscriptionInfoTransformed = transformSubscriptionInfoResponse(fixtures.subscriptionInfoResponse);
            spyOn(httpService, 'get').and.returnValue(of(plainToClass(SubscriptionInfo, subscriptionInfoTransformed)));
            apiService.getSubscriptionInfo().subscribe(result => {
                expect(httpService.get).toHaveBeenCalledTimes(1);
                expect(httpService.get).toHaveBeenCalledWith(
                    SubscriptionInfo,
                    'billing/subscription',
                    {},
                    transformSubscriptionInfoResponse
                );
                expect(result).toEqual(
                    plainToClass(SubscriptionInfo, Object.assign(new SubscriptionInfo(), fixtures.subscriptionInfoTransformedResponse))
                );
            });
        })
    );

    it(
        'should change subscription plan',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const subscriptionInfoTransformed = transformSubscriptionInfoResponse(fixtures.subscriptionInfoResponse);
            const plan = Object.assign(new Plan(), {
                key: 'plan_standard_test',
                billingPeriod: 'month',
                includedProductOptions: ['addon_banking']
            });
            const productOptions = ['addon_payroll', 'addon_banking'];
            const addonAttributes = [];
            addonAttributes[addonPayrollKey] = new AddonAttribute(99);
            const productOptionsAttributes = new AddonAttributeCollection(addonAttributes);
            const checkoutRequest = Object.assign(new CheckoutRequest(), {
                plan,
                productOptions,
                productOptionsAttributes
            });
            const data = JSON.stringify({
                key: 'plan_standard_test',
                billing_period: 'month',
                addons: [{ key: 'addon_payroll', custom_attributes: { ending_unit: 99 } }]
            });
            spyOn(httpService, 'patch').and.returnValue(of(plainToClass(SubscriptionInfo, subscriptionInfoTransformed)));
            let result = null;
            apiService.changeSubscriptionPlan(checkoutRequest).subscribe(value => (result = value));
            expect(httpService.patch).toHaveBeenCalledTimes(1);
            expect(httpService.patch).toHaveBeenCalledWith(
                SubscriptionInfo,
                'billing/subscription/plan',
                data,
                {},
                transformSubscriptionInfoResponse
            );
            expect(result).toEqual(plainToClass(SubscriptionInfo, fixtures.subscriptionInfoTransformedResponse));
        })
    );

    it(
        'should change subscription billing address',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const billingAddress = Object.assign(new CompanyProfile(), fixtures.companyProfile);
            const data = JSON.stringify(transformBillingInfoRequest(billingAddress));
            const paymentInformation = fixtures.subscriptionInfoResponse.payment_information;
            const paymentInformationTransformed = transformBillingInfoResponse(paymentInformation);
            spyOn(httpService, 'patch').and.returnValue(of(plainToClass(CompanyProfile, paymentInformationTransformed)));
            apiService.changeBillingAddress(billingAddress).subscribe(result => {
                expect(httpService.patch).toHaveBeenCalledTimes(1);
                expect(httpService.patch).toHaveBeenCalledWith(
                    CompanyProfile,
                    'billing/payment/information',
                    data,
                    {},
                    transformBillingInfoResponse
                );
                expect(result).toEqual(plainToClass(CompanyProfile, paymentInformationTransformed));
            });
        })
    );

    it(
        'should change subscription payment method',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const paymentMethod = Object.assign(new PaymentMethod(), { type: 'card' });
            const paymentInformationTransformed = transformPaymentMethodResponse(fixtures.subscriptionInfoResponse.payment_information);
            spyOn(httpService, 'patch').and.returnValue(of(plainToClass(PaymentMethod, paymentInformationTransformed)));
            const data = JSON.stringify({ payment_type: 'credit_card', payment_id: '', language: 'de' });
            apiService.changePaymentMethod(paymentMethod).subscribe(result => {
                expect(httpService.patch).toHaveBeenCalledTimes(1);
                expect(httpService.patch).toHaveBeenCalledWith(
                    PaymentMethod,
                    'billing/payment/method',
                    data,
                    {},
                    transformPaymentMethodResponse
                );
                expect(result).toEqual(Object.assign(new PaymentMethod(), { type: 'card' }));
            });
        })
    );

    it(
        'should fetch payment history',
        inject([ApiService, HttpService], (apiService, httpService) => {
            spyOn(httpService, 'get').and.returnValue(of(plainToClass(Invoice, fixtures.invoices)));
            apiService.getPaymentHistory().subscribe(result => {
                expect(httpService.get).toHaveBeenCalledTimes(1);
                expect(httpService.get).toHaveBeenCalledWith(Invoice, 'billing/invoices');
                expect(result).toEqual(plainToClass(Invoice, fixtures.invoices));
            });
        })
    );

    it(
        'should post activate addon',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const addon = Object.assign(new Addon(), fixtures.addon);
            const data = JSON.stringify(transformProduct(addon));
            spyOn(httpService, 'post').and.returnValue(of(plainToClass(SubscriptionInfo, fixtures.subscriptionInfoTransformedResponse)));
            apiService.activateAddon(addon).subscribe(result => {
                expect(httpService.post).toHaveBeenCalledTimes(1);
                expect(httpService.post).toHaveBeenCalledWith(
                    SubscriptionInfo,
                    'billing/subscription/addons',
                    data,
                    {},
                    transformSubscriptionInfoResponse
                );
                expect(result).toEqual(plainToClass(SubscriptionInfo, fixtures.subscriptionInfoTransformedResponse));
            });
        })
    );

    it(
        'should post deactivate addon',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const addon = Object.assign(new Addon(), fixtures.addon, { addon_key: fixtures.addon.key });
            spyOn(httpService, 'remove').and.returnValue(of(plainToClass(SubscriptionInfo, fixtures.subscriptionInfoTransformedResponse)));
            apiService.deactivateAddon(addon).subscribe(result => {
                expect(httpService.remove).toHaveBeenCalledTimes(1);
                expect(httpService.remove).toHaveBeenCalledWith(
                    SubscriptionInfo,
                    'billing/subscription/addons/' + addon.addon_key,
                    {},
                    transformSubscriptionInfoResponse
                );
                expect(result).toEqual(plainToClass(SubscriptionInfo, fixtures.subscriptionInfoTransformedResponse));
            });
        })
    );

    it(
        'should fetch credit card info',
        inject([ApiService, HttpService], (apiService, httpService) => {
            spyOn(httpService, 'get').and.returnValue(of(plainToClass(CreditCard, fixtures.creditCardTransformedResponse)));
            apiService.getCreditCardInfo().subscribe(result => {
                expect(httpService.get).toHaveBeenCalledTimes(1);
                expect(httpService.get).toHaveBeenCalledWith(CreditCard, 'billing/payment/credit_card', {}, transformCreditCardResponse);
                expect(result).toEqual(plainToClass(CreditCard, fixtures.creditCardTransformedResponse));
            });
        })
    );

    it(
        'should fetch a invoice and return a blob',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const invoiceId = '2394568';
            const responseBlob = new Blob();

            spyOn(httpService, 'get').and.returnValue(of(Blob, responseBlob));
            apiService.getPaymentInvoice(invoiceId).subscribe(result => {
                expect(httpService.get).toHaveBeenCalledTimes(1);
                expect(httpService.get).toHaveBeenCalledWith(
                    null,
                    `billing/invoices/${invoiceId}/pdf`, {
                        responseType: 'blob',
                        observe: 'response'
                    });
            });
        })
    );
});
