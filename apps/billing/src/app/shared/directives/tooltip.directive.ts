import { ConnectedPosition, Overlay, OverlayPositionBuilder, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { ComponentRef, Directive, ElementRef, HostListener, Input, OnInit, TemplateRef } from '@angular/core';

import { bottomPosition, leftPosition, rightPosition, TooltipPlacement, topPosition } from '../models/Tooltip';

import { TooltipComponent } from './tooltip.component';

@Directive({ selector: '[appTooltip]' })
export class TooltipDirective implements OnInit {
    @Input('content') content: TemplateRef<any>;
    @Input('placement') placement: TooltipPlacement;
    tooltipClassName: string;

    private overlayRef: OverlayRef;
    private visible = false;
    private position: ConnectedPosition[];

    constructor(private overlay: Overlay, private overlayPositionBuilder: OverlayPositionBuilder, private elementRef: ElementRef) {}

    ngOnInit(): void {
        switch (this.placement) {
            case TooltipPlacement.BOTTOM:
                this.position = bottomPosition;
                this.tooltipClassName = `Tooltip-content--${TooltipPlacement.BOTTOM}`;
                break;
            case TooltipPlacement.TOP:
                this.position = topPosition;
                this.tooltipClassName = `Tooltip-content--${TooltipPlacement.TOP}`;
                break;
            case TooltipPlacement.LEFT:
                this.position = leftPosition;
                this.tooltipClassName = `Tooltip-content--${TooltipPlacement.LEFT}`;
                break;
            case TooltipPlacement.RIGHT:
                this.position = rightPosition;
                this.tooltipClassName = `Tooltip-content--${TooltipPlacement.RIGHT}`;
                break;
            default:
                this.position = bottomPosition;
                this.tooltipClassName = `Tooltip-content--${TooltipPlacement.BOTTOM}`;
                break;
        }
        const positionStrategy = this.overlayPositionBuilder.flexibleConnectedTo(this.elementRef).withPositions(this.position);

        this.overlayRef = this.overlay.create({ positionStrategy });
    }

    @HostListener('click')
    onShow(): void {
        if (!this.visible) {
            setTimeout(() => (this.visible = true), 0);
            const tooltipRef: ComponentRef<TooltipComponent> = this.overlayRef.attach(new ComponentPortal(TooltipComponent));
            tooltipRef.instance.content = this.content;
            tooltipRef.instance.tooltipClassName = this.tooltipClassName;
        }

    }

    @HostListener('window:click')
    @HostListener('window:keydown.esc')
    onHide(): void {
        if (this.visible) {
            this.overlayRef.detach();
            this.visible = false;
        }

    }
}
