import { Component, Input, OnInit, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-tooltip',
  styleUrls: ['./tooltip.component.scss'],
  templateUrl: './tooltip.component.html'
})
export class TooltipComponent implements OnInit {

  @Input('content') content: TemplateRef<any>;
  @Input('tooltipClassName') tooltipClassName: string;

  currentTemplate: TemplateRef<any>;

  ngOnInit(): void {
    this.currentTemplate = this.content;
  }
}
