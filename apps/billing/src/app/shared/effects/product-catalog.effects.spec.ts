import { inject, TestBed } from '@angular/core/testing';
import { MessageActions } from '@bx-client/message';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { Observable, of, throwError } from 'rxjs';

import * as productCatalogActions from '../actions/product-catalog.action';
import * as fixtures from '../fixtures';
import { ApiServiceMockProvider } from '../mocks';
import { ProductCatalog } from '../models';
import { ApiService } from '../services';

import { ProductCatalogEffects } from './product-catalog.effects';

describe('productCatalogEffects Catalog Effect', () => {
    let productCatalogEffects: ProductCatalogEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<ProductCatalogEffects>;

    const productCatalog = Object.assign(new ProductCatalog(), fixtures.productCatalog);

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [ProductCatalogEffects, ApiServiceMockProvider, provideMockActions(() => actions)]
        });

        productCatalogEffects = TestBed.get(ProductCatalogEffects);
        metadata = getEffectsMetadata(productCatalogEffects);
    });

    it(
        'should return LOAD_SUCCESS on LOAD if getProductCatalog is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new productCatalogActions.LoadAction() });
            const expected = cold('b', { b: new productCatalogActions.LoadSuccessAction(productCatalog) });
            spyOn(apiService, 'getProductCatalog').and.returnValue(of(productCatalog));
            expect(productCatalogEffects.fetchCatalog$).toBeObservable(expected);
            expect(apiService.getProductCatalog).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_FAIL on LOAD if getProductCatalog fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new productCatalogActions.LoadAction() });
            const expected = cold('b', { b: new productCatalogActions.LoadFailAction() });
            spyOn(apiService, 'getProductCatalog').and.returnValue(throwError(null));
            expect(productCatalogEffects.fetchCatalog$).toBeObservable(expected);
            expect(apiService.getProductCatalog).toHaveBeenCalledTimes(1);
        })
    );

    it('should return ERROR_MSG on productCatalog.LOAD_FAIL action', () => {
        actions = cold('a', { a: new productCatalogActions.LoadFailAction() });
        const payload = {
            messageKey: 'billing.load_subscription_info_no_permission'
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(productCatalogEffects.productCatalogLoadFail$).toBeObservable(expected);
    });

    it('should test metadata', () => {
        expect(metadata.fetchCatalog$).toEqual({ dispatch: true });
        expect(metadata.productCatalogLoadFail$).toEqual({ dispatch: true });
    });
});
