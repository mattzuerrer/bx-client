import { inject, TestBed } from '@angular/core/testing';
import { MessageActions } from '@bx-client/message';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import * as HTTPStatus from 'http-status';
import { cold } from 'jasmine-marbles';
import { Observable, of, throwError } from 'rxjs';

import * as userActions from '../actions/user.action';
import * as fixtures from '../fixtures';
import { ApiServiceMockProvider, StateServiceMockProvider } from '../mocks';
import { User } from '../models';
import { ApiService } from '../services';

import { UserEffects } from './user.effects';

describe('User Effects', () => {
    let userEffects: UserEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<UserEffects>;

    const currentUser = Object.assign(new User(), fixtures.user);
    const users = [Object.assign(new User(), ...fixtures.users)];
    const id = 123;
    const email = 'test@mail.com';

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [UserEffects, ApiServiceMockProvider, StateServiceMockProvider, provideMockActions(() => actions)]
        });

        userEffects = TestBed.get(UserEffects);
        metadata = getEffectsMetadata(userEffects);
    });

    it(
        'should return FETCH_CURRENT_SUCCESS on FETCH_CURRENT if getCurrentUser is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new userActions.FetchCurrentAction() });
            const expected = cold('b', { b: new userActions.FetchCurrentSuccessAction(currentUser) });
            spyOn(apiService, 'getCurrentUser').and.returnValue(of(currentUser));
            expect(userEffects.fetchCurrentUser$).toBeObservable(expected);
            expect(apiService.getCurrentUser).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return FETCH_CURRENT_FAIL on FETCH_CURRENT if getCurrentUser fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new userActions.FetchCurrentAction() });
            const expected = cold('b', { b: new userActions.FetchCurrentFailAction() });
            spyOn(apiService, 'getCurrentUser').and.returnValue(throwError(null));
            expect(userEffects.fetchCurrentUser$).toBeObservable(expected);
            expect(apiService.getCurrentUser).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return REMOVE_SUCCESS on REMOVE if removeUser is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new userActions.RemoveAction(id) });
            const expected = cold('b', { b: new userActions.RemoveSuccessAction(id) });
            spyOn(apiService, 'removeUser').and.returnValue(of(id));
            expect(userEffects.removeUser$).toBeObservable(expected);
            expect(apiService.removeUser).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return REMOVE_FAIL on REMOVE if removeUser fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new userActions.RemoveAction(id) });
            const expected = cold('b', { b: new userActions.RemoveFailAction(id) });
            spyOn(apiService, 'removeUser').and.returnValue(throwError(null));
            expect(userEffects.removeUser$).toBeObservable(expected);
            expect(apiService.removeUser).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return INVITE_SUCCESS on INVITE if inviteUser is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new userActions.InviteAction(email) });
            const expected = cold('b', { b: new userActions.InviteSuccessAction(currentUser) });
            spyOn(apiService, 'inviteUser').and.returnValue(of(currentUser));
            expect(userEffects.inviteUser$).toBeObservable(expected);
            expect(apiService.inviteUser).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_FAIL with reason: billing.invite_user.email_exists on ' + 'INVITE if inviteUser fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new userActions.InviteAction(email) });
            const expected = cold('b', {
                b: new userActions.InviteFailAction({ reason: 'billing.invite_user.email_exists', retryAction: null })
            });
            spyOn(apiService, 'inviteUser').and.returnValue(
                throwError({
                    status: HTTPStatus.BAD_REQUEST
                })
            );
            expect(userEffects.inviteUser$).toBeObservable(expected);
            expect(apiService.inviteUser).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_FAIL with reason: billing.invite_user.invalid_email on ' + 'INVITE if inviteUser fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new userActions.InviteAction(email) });
            const expected = cold('b', {
                b: new userActions.InviteFailAction({ reason: 'billing.invite_user.invalid_email', retryAction: null })
            });
            spyOn(apiService, 'inviteUser').and.returnValue(
                throwError({
                    status: HTTPStatus.UNPROCESSABLE_ENTITY
                })
            );
            expect(userEffects.inviteUser$).toBeObservable(expected);
            expect(apiService.inviteUser).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_FAIL with reason: billing.invite_user.general_fail on INVITE if' + ' inviteUser fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new userActions.InviteAction(email) });
            const expected = cold('b', {
                b: new userActions.InviteFailAction({
                    reason: 'billing.invite_user.general_fail',
                    retryAction: new userActions.InviteAction(email)
                })
            });
            spyOn(apiService, 'inviteUser').and.returnValue(throwError({ status: 'test' }));
            expect(userEffects.inviteUser$).toBeObservable(expected);
            expect(apiService.inviteUser).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_SUCCESS on LOAD if getUsers is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new userActions.LoadAction() });
            const expected = cold('b', { b: new userActions.LoadSuccessAction(users) });
            spyOn(apiService, 'getUsers').and.returnValue(of(users));
            expect(userEffects.fetchUsers$).toBeObservable(expected);
            expect(apiService.getUsers).toHaveBeenCalledTimes(1);
            expect(apiService.getUsers).toHaveBeenCalledWith('test_1_desc,test_2_desc');
        })
    );

    it(
        'should return LOAD_SUCCESS on SORT if getUsers is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new userActions.SortAction([]) });
            const expected = cold('b', { b: new userActions.LoadSuccessAction(users) });
            spyOn(apiService, 'getUsers').and.returnValue(of(users));
            expect(userEffects.fetchUsers$).toBeObservable(expected);
            expect(apiService.getUsers).toHaveBeenCalledTimes(1);
            expect(apiService.getUsers).toHaveBeenCalledWith('test_1_desc,test_2_desc');
        })
    );

    it(
        'should return LOAD_FAIL on SORT if getUsers fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new userActions.SortAction([]) });
            const expected = cold('b', { b: new userActions.LoadFailAction() });
            spyOn(apiService, 'getUsers').and.returnValue(throwError(null));
            expect(userEffects.fetchUsers$).toBeObservable(expected);
            expect(apiService.getUsers).toHaveBeenCalledTimes(1);
            expect(apiService.getUsers).toHaveBeenCalledWith('test_1_desc,test_2_desc');
        })
    );

    it(
        'should return LOAD_FAIL on LOAD if getUsers fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new userActions.LoadAction() });
            const expected = cold('b', { b: new userActions.LoadFailAction() });
            spyOn(apiService, 'getUsers').and.returnValue(throwError(null));
            expect(userEffects.fetchUsers$).toBeObservable(expected);
            expect(apiService.getUsers).toHaveBeenCalledTimes(1);
            expect(apiService.getUsers).toHaveBeenCalledWith('test_1_desc,test_2_desc');
        })
    );

    it('should return ERROR_MSG on LOAD_FAIL action', () => {
        actions = cold('a', { a: new userActions.LoadFailAction() });
        const payload = {
            messageKey: 'error.server.fetch_users',
            titleKey: 'globals.try_again',
            options: { data: { action: new userActions.LoadAction() } }
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(userEffects.usersLoadFail$).toBeObservable(expected);
    });

    it('should return ERROR_MSG on LOAD_FAIL action', () => {
        actions = cold('a', { a: new userActions.LoadFailAction() });
        const payload = {
            messageKey: 'error.server.fetch_users',
            titleKey: 'globals.try_again',
            options: { data: { action: new userActions.LoadAction() } }
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(userEffects.usersLoadFail$).toBeObservable(expected);
    });

    it('should return ERROR_MSG on INVITE_FAIL action', () => {
        const invitePayload = { reason: 'error message', retryAction: new userActions.InviteAction('user') };
        actions = cold('a', { a: new userActions.InviteFailAction(invitePayload) });
        const payload = {
            messageKey: invitePayload.reason,
            titleKey: 'globals.try_again',
            options: { data: { action: invitePayload.retryAction } }
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(userEffects.usersInviteFail$).toBeObservable(expected);
    });

    it('should return SUCCESS_MSG on INVITE_SUCCESS action', () => {
        const user = users[0];
        actions = cold('a', { a: new userActions.InviteSuccessAction(user) });
        const payload = {
            messageKey: 'billing.invite_user_success',
            titleKey: '',
            options: { data: { translateParams: { email: user.email } } }
        };
        const expected = cold('b', { b: new MessageActions.SuccessMessage(payload) });
        expect(userEffects.usersInviteSuccess$).toBeObservable(expected);
    });

    it('should return ERROR_MSG on REMOVE_FAIL action', () => {
        actions = cold('a', { a: new userActions.RemoveFailAction(22) });
        const payload = {
            messageKey: 'billing.remove_user_fail',
            titleKey: 'globals.try_again',
            options: { data: { action: new userActions.RemoveAction(22) } }
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(userEffects.usersRemoveFail$).toBeObservable(expected);
    });

    it('should return SUCCESS_MSG on REMOVE_SUCCESS action', () => {
        actions = cold('a', { a: new userActions.RemoveSuccessAction(22) });
        const payload = {
            messageKey: 'billing.remove_user_success'
        };
        const expected = cold('b', { b: new MessageActions.SuccessMessage(payload) });
        expect(userEffects.usersRemoveSuccess$).toBeObservable(expected);
    });

    it('should test metadata', () => {
        expect(metadata.fetchUsers$).toEqual({ dispatch: true });
        expect(metadata.fetchCurrentUser$).toEqual({ dispatch: true });
        expect(metadata.inviteUser$).toEqual({ dispatch: true });
        expect(metadata.removeUser$).toEqual({ dispatch: true });
        expect(metadata.usersLoadFail$).toEqual({ dispatch: true });
        expect(metadata.usersInviteFail$).toEqual({ dispatch: true });
        expect(metadata.usersInviteSuccess$).toEqual({ dispatch: true });
        expect(metadata.usersRemoveFail$).toEqual({ dispatch: true });
        expect(metadata.usersRemoveSuccess$).toEqual({ dispatch: true });
    });
});
