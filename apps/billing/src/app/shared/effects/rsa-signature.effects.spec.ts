import { inject, TestBed } from '@angular/core/testing';
import { languageToken } from '@bx-client/env';
import { MessageActions } from '@bx-client/message';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { Observable, of, throwError } from 'rxjs';

import * as rsaSignatureActions from '../actions/rsa-signature.action';
import * as fixtures from '../fixtures';
import { ApiServiceMockProvider } from '../mocks';
import { RsaSignature } from '../models';
import { ApiService } from '../services';

import { RsaSignatureEffects } from './rsa-signature.effects';

const languageProvider = {
    provide: languageToken,
    useValue: 'de'
};

describe('Rsa Signature Effect', () => {
    let rsaSignatureEffects: RsaSignatureEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<RsaSignatureEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [RsaSignatureEffects, ApiServiceMockProvider, languageProvider, provideMockActions(() => actions)]
        });

        rsaSignatureEffects = TestBed.get(RsaSignatureEffects);
        metadata = getEffectsMetadata(rsaSignatureEffects);
    });

    it(
        'should return LOAD_SUCCESS on LOAD if getRsaSignature is successful',
        inject([ApiService], apiService => {
            const signature = Object.assign(new RsaSignature(), fixtures.rsaSignature, { locale: 'de' });
            actions = cold('a', { a: new rsaSignatureActions.LoadAction() });
            const expected = cold('b', { b: new rsaSignatureActions.LoadSuccessAction(signature) });
            spyOn(apiService, 'getRsaSignature').and.returnValue(of(signature));
            expect(rsaSignatureEffects.getRsaSignature$).toBeObservable(expected);
            expect(apiService.getRsaSignature).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_FAIL on LOAD if getRsaSignature fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new rsaSignatureActions.LoadAction() });
            const expected = cold('b', { b: new rsaSignatureActions.LoadFailAction() });
            spyOn(apiService, 'getRsaSignature').and.returnValue(throwError(null));
            expect(rsaSignatureEffects.getRsaSignature$).toBeObservable(expected);
            expect(apiService.getRsaSignature).toHaveBeenCalledTimes(1);
        })
    );

    it('should return ERROR_MSG on LOAD_FAIL action', () => {
        actions = cold('a', { a: new rsaSignatureActions.LoadFailAction() });
        const payload = {
            messageKey: 'billing.load_rsa_signature_fail',
            titleKey: 'globals.try_again',
            options: { data: { action: new rsaSignatureActions.LoadAction() } }
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(rsaSignatureEffects.rsaSignatureLoadFail$).toBeObservable(expected);
    });

    it('should test metadata', () => {
        expect(metadata.getRsaSignature$).toEqual({ dispatch: true });
        expect(metadata.rsaSignatureLoadFail$).toEqual({ dispatch: true });
    });
});
