import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessageActions } from '@bx-client/message';
import { RouterActions } from '@bx-client/router';
import { Actions, Effect } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import * as HTTPStatus from 'http-status';
import { Observable, of } from 'rxjs';
import { catchError, delay, filter, map, mergeMap, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';

import * as addonActions from '../actions/addon.action';
import * as pageActions from '../actions/page.action';
import * as subscriptionInfoActions from '../actions/subscription-info.action';
import { CheckoutRequest } from '../models';
import { ApiService, StateService } from '../services';

@Injectable()
export class SubscriptionInfoEffects {
    @Effect()
    getSubscriptionInfo$: Observable<any> = this.actions$.ofType(subscriptionInfoActions.LOAD).pipe(
        switchMap(() =>
            this.apiService.getSubscriptionInfo().pipe(
                map(subscriptionInfo => new subscriptionInfoActions.LoadSuccessAction(subscriptionInfo)),
                catchError(errorResponse => {
                    const error = errorResponse || { status: 0 };
                    let errorMsg = 'billing.load_subscription_info_fail';
                    switch (error.status) {
                        case HTTPStatus.FORBIDDEN:
                            errorMsg = 'billing.load_subscription_info_no_permission';
                            break;
                    }

                    return of(new subscriptionInfoActions.LoadFailAction(errorMsg));
                })
            )
        )
    );

    @Effect()
    initCreditCard$: Observable<any> = this.actions$
        .ofType(subscriptionInfoActions.LOAD_SUCCESS)
        .pipe(
            map((action: subscriptionInfoActions.LoadSuccessAction) => action.payload),
            filter(subscription => subscription.paymentMethod.isCard),
            map(() => new subscriptionInfoActions.LoadCreditCardAction())
        );

    @Effect()
    updateCreditCard$: Observable<any> = this.actions$
        .ofType(subscriptionInfoActions.UPDATE_PAYMENT_METHOD_SUCCESS)
        .pipe(
            map((action: subscriptionInfoActions.UpdatePaymentMethodSuccessAction) => action.payload),
            filter(paymentMethod => paymentMethod.isCard),
            map(() => new subscriptionInfoActions.LoadCreditCardAction())
        );

    @Effect()
    loadCreditCard$: Observable<any> = this.actions$
        .ofType(subscriptionInfoActions.LOAD_CREDIT_CARD)
        .pipe(
            switchMap(() =>
                this.apiService
                    .getCreditCardInfo()
                    .pipe(
                        map(creditCardInfo => new subscriptionInfoActions.LoadCreditCardSuccessAction(creditCardInfo)),
                        catchError(() => of(new subscriptionInfoActions.LoadCreditCardFailAction()))
                    )
            )
        );

    @Effect()
    updatePlan$: Observable<any> = this.actions$.ofType(subscriptionInfoActions.UPDATE_PLAN).pipe(
        withLatestFrom(
            this.stateService.overviewPlan$,
            this.stateService.overviewProductOptions$,
            this.stateService.overviewProductOptionsAttributes$,
            (_, plan, productOptions, productOptionsAttributes) => ({ plan, productOptions, productOptionsAttributes })
        ),
        map(({ plan, productOptions, productOptionsAttributes }) =>
            Object.assign(new CheckoutRequest(), { plan, productOptions, productOptionsAttributes })
        ),
        tap(() => this.stateService.dispatch(new pageActions.LoadingAction())),
        switchMap(checkoutRequest =>
            this.apiService.changeSubscriptionPlan(checkoutRequest).pipe(
                map(subscriptionInfo => new subscriptionInfoActions.UpdatePlanSuccessAction(subscriptionInfo)),
                catchError(() => {
                    return of(new subscriptionInfoActions.UpdatePlanFailAction());
                })
            )
        )
    );

    @Effect()
    updateBillingAddress$: Observable<any> = this.actions$.ofType(subscriptionInfoActions.UPDATE_BILLING_ADDRESS).pipe(
        map((action: subscriptionInfoActions.UpdateBillingAddressAction) => action.payload),
        switchMap(billingInfo =>
            this.apiService.changeBillingAddress(billingInfo).pipe(
                map(billingInfoResponse => new subscriptionInfoActions.UpdateBillingAddressSuccessAction(billingInfoResponse)),
                catchError(() => {
                    return of(new subscriptionInfoActions.UpdateBillingAddressFailAction(billingInfo));
                })
            )
        )
    );

    @Effect()
    updatePaymentMethod$: Observable<any> = this.actions$.ofType(subscriptionInfoActions.UPDATE_PAYMENT_METHOD).pipe(
        map((action: subscriptionInfoActions.UpdatePaymentMethodAction) => action.payload),
        filter(paymentMethod => !paymentMethod.isCard || paymentMethod.creditCard.hasRefId),
        switchMap(paymentMethod =>
            this.apiService.changePaymentMethod(paymentMethod).pipe(
                map(response => new subscriptionInfoActions.UpdatePaymentMethodSuccessAction(response)),
                catchError(() => {
                    return of(new subscriptionInfoActions.UpdatePaymentMethodFailAction(paymentMethod));
                })
            )
        )
    );

    @Effect()
    activateAddon$: Observable<any> = this.actions$
        .ofType(addonActions.ACTIVATE)
        .pipe(
            map((action: addonActions.ActivateAction) => action.payload),
            mergeMap(addon =>
                this.apiService
                    .activateAddon(addon)
                    .pipe(
                        map(() => new addonActions.ActivateSuccessAction(addon)),
                        catchError(() => of(new addonActions.ActivateFailAction(addon)))
                    )
            )
        );

    @Effect()
    deactivateAddon$: Observable<any> = this.actions$
        .ofType(addonActions.DEACTIVATE)
        .pipe(
            map((action: addonActions.DeactivateAction) => action.payload),
            mergeMap(addon =>
                this.apiService
                    .deactivateAddon(addon)
                    .pipe(
                        map(() => new addonActions.DeactivateSuccessAction(addon)),
                        catchError(() => of(new addonActions.DeactivateFailAction(addon)))
                    )
            )
        );

    @Effect({ dispatch: true })
    addonActivateFail$: Observable<any> = this.actions$.ofType(addonActions.ACTIVATE_FAIL).pipe(
        map((action: addonActions.ActivateFailAction) => action.payload),
        switchMap(addon =>
            this.translateService.get(addon.name).pipe(
                map(
                    name =>
                        new MessageActions.ErrorMessage({
                            messageKey: 'billing.activate_addon_fail',
                            titleKey: 'globals.try_again',
                            options: {
                                data: { action: new addonActions.ActivateAction(addon), translateParams: { name } }
                            }
                        })
                )
            )
        )
    );

    @Effect({ dispatch: true })
    addonDeactivateFail$: Observable<any> = this.actions$.ofType(addonActions.DEACTIVATE_FAIL).pipe(
        map((action: addonActions.DeactivateFailAction) => action.payload),
        switchMap(addon =>
            this.translateService.get(addon.name).pipe(
                map(
                    name =>
                        new MessageActions.ErrorMessage({
                            messageKey: 'billing.deactivate_addon_fail',
                            titleKey: 'globals.try_again',
                            options: {
                                data: { action: new addonActions.DeactivateAction(addon), translateParams: { name } }
                            }
                        })
                )
            )
        )
    );

    @Effect({ dispatch: false })
    updatePlanSuccess$: Observable<any> = this.actions$.ofType(subscriptionInfoActions.UPDATE_PLAN_SUCCESS).pipe(
        switchMap(() => this.activatedRoute.queryParams.pipe(take(1))),
        map(params => params.redirect_to),
        tap(path => {
            if (path) {
                this.stateService.dispatch(new RouterActions.Go({ path: [path] }));
            }
        }),
        tap(() => this.stateService.dispatch(new pageActions.ForceLoadingAction())),
        delay(1),
        tap(() => this.stateService.dispatch(new pageActions.ReloadAction()))
    );

    @Effect({ dispatch: true })
    updatePlanFail$: Observable<any> = this.actions$.ofType(subscriptionInfoActions.UPDATE_PLAN_FAIL).pipe(
        switchMap((action: subscriptionInfoActions.UpdatePlanFailAction) => [
            new MessageActions.ErrorMessage({
                messageKey: 'billing.update_plan_fail',
                titleKey: 'globals.try_again',
                options: { data: { action: new subscriptionInfoActions.UpdatePlanAction() } }
            }),
            new pageActions.IdleAction()
        ])
    );

    @Effect({ dispatch: true })
    subscriptionInfoFail$: Observable<any> = this.actions$.ofType(subscriptionInfoActions.LOAD_FAIL).pipe(
        map(
            (action: subscriptionInfoActions.LoadFailAction) =>
                new MessageActions.ErrorMessage({
                    messageKey: action.payload,
                    titleKey: 'globals.try_again',
                    options: { data: { action: new subscriptionInfoActions.LoadAction() } }
                })
        )
    );

    constructor(
        private actions$: Actions,
        private apiService: ApiService,
        private stateService: StateService,
        private activatedRoute: ActivatedRoute,
        private translateService: TranslateService
    ) {}
}
