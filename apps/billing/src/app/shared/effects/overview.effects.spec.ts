import { inject, TestBed } from '@angular/core/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { Observable, of, throwError } from 'rxjs';

import { addonPayrollKey } from '../../../config/addonsMapping';
import * as overviewActions from '../actions/overview.actions';
import * as fixtures from '../fixtures';
import { ApiServiceMockProvider, StateServiceMockProvider } from '../mocks';
import { Addon, AddonAttribute, AddonAttributeCollection, CheckoutRequest, CheckoutSummary, Plan, ProductVersion } from '../models';
import { ApiService, StateService } from '../services';

import { OverviewEffects } from './overview.effects';

describe('Overview Effects', () => {
    let overviewEffects: OverviewEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<OverviewEffects>;

    const plan = Object.assign(new Plan(), fixtures.checkoutPreviewMonth.plan);
    const productOptions = ['addon_payroll'];
    const addonAttributes = [];
    addonAttributes[addonPayrollKey] = new AddonAttribute(99);
    const productOptionsAttributes = new AddonAttributeCollection(addonAttributes);
    const payrollAddon = { key: 'addon_payroll', customAttributes: new AddonAttribute(99) };
    const checkoutRequest = Object.assign(new CheckoutRequest(), { plan, productOptions, productOptionsAttributes });
    const checkoutPreview = Object.assign(new CheckoutSummary(), fixtures.checkoutPreviewMonth);

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [StateServiceMockProvider, OverviewEffects, ApiServiceMockProvider, provideMockActions(() => actions)]
        });

        const stateService = TestBed.get(StateService);

        stateService.overviewPlan$ = of(plan);
        stateService.overviewProductOptions$ = of(productOptions);
        stateService.overviewProductOptionsAttributes$ = of(productOptionsAttributes);
        stateService.subscriptionInfoInstancePlan$ = of(plan);
        stateService.subscriptionInfoInstanceAddons$ = of([payrollAddon]);

        overviewEffects = TestBed.get(OverviewEffects);
        metadata = getEffectsMetadata(overviewEffects);
    });

    it(
        'should return PREVIEW_SUCCESS on PREVIEW if getCheckoutPreview is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new overviewActions.PreviewAction() });
            spyOn(apiService, 'getCheckoutPreview').and.returnValue(of(checkoutPreview));
            const expected = cold('b', { b: new overviewActions.PreviewSuccessAction(checkoutPreview) });
            expect(overviewEffects.getOverviewPreview$).toBeObservable(expected);
            expect(apiService.getCheckoutPreview).toHaveBeenCalledTimes(1);
            expect(apiService.getCheckoutPreview).toHaveBeenCalledWith(checkoutRequest);
        })
    );

    it(
        'should return PREVIEW_FAIL on PREVIEW if getCheckoutPreview fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new overviewActions.PreviewAction() });
            spyOn(apiService, 'getCheckoutPreview').and.returnValue(throwError(null));
            const expected = cold('b', { b: new overviewActions.PreviewFailAction() });
            expect(overviewEffects.getOverviewPreview$).toBeObservable(expected);
            expect(apiService.getCheckoutPreview).toHaveBeenCalledTimes(1);
            expect(apiService.getCheckoutPreview).toHaveBeenCalledWith(checkoutRequest);
        })
    );

    it('should return PREVIEW on CHECK_PRODUCT_OPTION', () => {
        actions = cold('a', { a: new overviewActions.CheckProductOptionAction('addon_payroll') });
        const expected = cold('b', { b: new overviewActions.PreviewAction() });
        expect(overviewEffects.triggerOverviewPreview$).toBeObservable(expected);
    });

    it('should return PREVIEW on UNCHECK_PRODUCT_OPTION', () => {
        actions = cold('a', { a: new overviewActions.UncheckProductOptionAction('addon_payroll') });
        const expected = cold('b', { b: new overviewActions.PreviewAction() });
        expect(overviewEffects.triggerOverviewPreview$).toBeObservable(expected);
    });

    it('should return PREVIEW on CHANGE_PLAN', () => {
        actions = cold('a', { a: new overviewActions.ChangePlanAction(plan) });
        const expected = cold('b', { b: new overviewActions.PreviewAction() });
        expect(overviewEffects.triggerOverviewPreview$).toBeObservable(expected);
    });

    it('should return PREVIEW on CHANGE_PAYROLL_ENDING_UNIT_ATTRIBUTE', () => {
        actions = cold('a', { a: new overviewActions.ChangePayrollEndingUnitAttribute(99) });
        const expected = cold('b', { b: new overviewActions.PreviewAction() });
        expect(overviewEffects.triggerOverviewPreview$).toBeObservable(expected);
    });

    it('should return INIT_PLAN on INIT_OPTIONS', () => {
        actions = cold('a', { a: new overviewActions.InitOptionsAction() });
        const expected = cold('b', { b: new overviewActions.InitPlanAction(plan) });
        expect(overviewEffects.initActivePlan$).toBeObservable(expected);
    });

    it(
        'should dispatch INIT_PRODUCT_OPTIONS and INIT_PRODUCT_OPTIONS_ATTRIBUTES on INIT_OPTIONS',
        inject([StateService], stateService => {
            actions = cold('a', { a: new overviewActions.InitOptionsAction() });
            spyOn(stateService, 'dispatch').and.callThrough();
            const expected = cold('b', { b: [payrollAddon] });
            expect(overviewEffects.initProductOptions$).toBeObservable(expected);
            expect(stateService.dispatch).toHaveBeenCalledTimes(2);
            expect(stateService.dispatch.calls.allArgs()).toEqual([
                [new overviewActions.InitProductOptionsAction(productOptions)],
                [new overviewActions.InitProductOptionsAttributesAction(productOptionsAttributes)]
            ]);
        })
    );

    it('should test metadata', () => {
        expect(metadata.initActivePlan$).toEqual({ dispatch: true });
        expect(metadata.initProductOptions$).toEqual({ dispatch: false });
        expect(metadata.getOverviewPreview$).toEqual({ dispatch: true });
        expect(metadata.triggerOverviewPreview$).toEqual({ dispatch: true });
    });
});

describe('Overview Effects (custom for productVersionV2)', () => {
    let overviewEffects: OverviewEffects;
    let actions: Observable<any>;

    const plan = Object.assign(new Plan(), fixtures.checkoutPreviewMonth.plan);
    const productOptions = ['addon_payroll'];
    const addonAttributes = [];
    addonAttributes[addonPayrollKey] = new AddonAttribute(99);
    const productOptionsAttributes = new AddonAttributeCollection(addonAttributes);
    const payrollAddon = Object.assign(new Addon(), {
        key: 'addon_payroll',
        customAttributes: new AddonAttribute(99),
        productVersion: ProductVersion.V2
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [StateServiceMockProvider, OverviewEffects, ApiServiceMockProvider, provideMockActions(() => actions)]
        });

        const planV2 = Object.assign(new Plan(), fixtures.subscriptionInfo.plan);
        const stateService = TestBed.get(StateService);

        stateService.overviewPlan$ = of(planV2);
        stateService.overviewProductOptions$ = of(productOptions);
        stateService.overviewProductOptionsAttributes$ = of(productOptionsAttributes);
        stateService.subscriptionInfoInstancePlan$ = of(plan);
        stateService.subscriptionInfoInstanceAddons$ = of([payrollAddon]);

        overviewEffects = TestBed.get(OverviewEffects);
    });
    it(
        'should dispatch INIT_PRODUCT_OPTIONS and INIT_PRODUCT_OPTIONS_ATTRIBUTES on INIT_OPTIONS',
        inject([StateService], stateService => {
            actions = cold('a', { a: new overviewActions.InitOptionsAction() });
            spyOn(stateService, 'dispatch').and.callThrough();
            const expected = cold('b', { b: [payrollAddon] });
            expect(overviewEffects.initProductOptions$).toBeObservable(expected);
            expect(stateService.dispatch).toHaveBeenCalledTimes(2);
            expect(stateService.dispatch.calls.allArgs()).toEqual([
                [new overviewActions.InitProductOptionsAction(['addon_payroll'])],
                [new overviewActions.InitProductOptionsAttributesAction(productOptionsAttributes)]
            ]);
        })
    );
});
