import { inject, TestBed } from '@angular/core/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { Observable, of, throwError } from 'rxjs';

import * as paymentHistoryActions from '../actions/payment-history.action';
import * as fixtures from '../fixtures';
import { ApiServiceMockProvider } from '../mocks';
import { Invoice } from '../models';
import { ApiService } from '../services';

import { PaymentHistoryEffects } from './payment-history.effects';

describe('Payment History Effects', () => {
    let paymentHistoryEffects: PaymentHistoryEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<PaymentHistoryEffects>;

    const invoices = fixtures.invoices.map(invoice => Object.assign(new Invoice(), invoice));

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [PaymentHistoryEffects, ApiServiceMockProvider, provideMockActions(() => actions)]
        });

        paymentHistoryEffects = TestBed.get(PaymentHistoryEffects);
        metadata = getEffectsMetadata(paymentHistoryEffects);
    });

    it(
        'should return LOAD_SUCCESS on LOAD if getPaymentHistory is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new paymentHistoryActions.LoadAction() });
            const expected = cold('b', { b: new paymentHistoryActions.LoadSuccessAction(invoices) });
            spyOn(apiService, 'getPaymentHistory').and.returnValue(of(invoices));
            expect(paymentHistoryEffects.fetchPaymentHistory$).toBeObservable(expected);
            expect(apiService.getPaymentHistory).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_FAIL on LOAD if getPaymentHistory fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new paymentHistoryActions.LoadAction() });
            const expected = cold('b', { b: new paymentHistoryActions.LoadFailAction() });
            spyOn(apiService, 'getPaymentHistory').and.returnValue(throwError(null));
            expect(paymentHistoryEffects.fetchPaymentHistory$).toBeObservable(expected);
            expect(apiService.getPaymentHistory).toHaveBeenCalledTimes(1);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetchPaymentHistory$).toEqual({ dispatch: true });
    });
});
