import { inject, TestBed } from '@angular/core/testing';
import { MessageActions } from '@bx-client/message';
import { RouterActions } from '@bx-client/router';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold, getTestScheduler } from 'jasmine-marbles';
import { Observable, of, throwError } from 'rxjs';
import { map } from 'rxjs/operators';

import * as checkoutActions from '../actions/checkout.action';
import * as creditCardActions from '../actions/credit-card.action';
import * as pageActions from '../actions/page.action';
import * as subscriptionInfoActions from '../actions/subscription-info.action';
import * as fixtures from '../fixtures';
import { ApiServiceMockProvider, StateServiceMockProvider } from '../mocks';
import { CheckoutRequest, CheckoutSummary, PaymentMethod, Plan, SubscriptionInfo, Voucher } from '../models';
import { ApiService, StateService } from '../services';

import { CheckoutEffects } from './checkout.effects';

describe('Checkout Effect', () => {
    let checkoutEffects: CheckoutEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<CheckoutEffects>;

    const checkoutSummary = Object.assign(new CheckoutSummary(), fixtures.checkoutPreviewMonth);
    const checkoutRequest = Object.assign(new CheckoutRequest(), fixtures.checkoutRequest);

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [CheckoutEffects, ApiServiceMockProvider, StateServiceMockProvider, provideMockActions(() => actions)]
        });

        const stateService = TestBed.get(StateService);

        stateService.catalogPlans$ = of(fixtures.productCatalog.plans).pipe(
            map(plans => plans.map(plan => Object.assign(new Plan(), plan)))
        );

        checkoutEffects = TestBed.get(CheckoutEffects);
        metadata = getEffectsMetadata(checkoutEffects);
    });

    it(
        'should return PREVIEW_SUCCESS on PREVIEW if getCheckoutPreview is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new checkoutActions.PreviewAction() });
            spyOn(apiService, 'getCheckoutPreview').and.returnValue(of(checkoutSummary));
            const expected = cold('b', { b: new checkoutActions.PreviewSuccessAction(checkoutSummary) });
            expect(checkoutEffects.getCheckoutPreview$).toBeObservable(expected);
            expect(apiService.getCheckoutPreview).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return INIT on subscription info LOAD_SUCCESS',
        inject([ApiService], apiService => {
            const monthlyPlan = Object.assign(new Plan(), fixtures.productCatalog.plans[1]);
            const paymentMethodCard = Object.assign(new PaymentMethod(), { type: 'card' });
            const subscriptionInfo = Object.assign(new SubscriptionInfo(), { plan: monthlyPlan });
            const checkoutRequestPredefined = Object.assign(new CheckoutRequest(), {
                plan: monthlyPlan,
                paymentMethod: paymentMethodCard,
                productOptions: []
            });

            actions = cold('a', { a: new subscriptionInfoActions.LoadSuccessAction(subscriptionInfo) });
            spyOn(apiService, 'sendCompletedCheckout').and.returnValue(throwError(null));
            const expected = cold('b', { b: new checkoutActions.InitAction(checkoutRequestPredefined) });
            expect(checkoutEffects.checkoutInit$).toBeObservable(expected);
        })
    );

    it(
        'should return PREVIEW_FAIL on PREVIEW if getCheckoutPreview fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new checkoutActions.PreviewAction() });
            spyOn(apiService, 'getCheckoutPreview').and.returnValue(throwError(of(null)));
            const expected = cold('b', { b: new checkoutActions.PreviewFailAction() });
            expect(checkoutEffects.getCheckoutPreview$).toBeObservable(expected);
            expect(apiService.getCheckoutPreview).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return CHECKOUT_SUCCESS on CHECKOUT if sendCompletedCheckout is successful',
        inject([ApiService], apiService => {
            const subscriptionInfo = Object.assign(new SubscriptionInfo(), fixtures.subscriptionInfo);

            actions = cold('a', { a: new checkoutActions.CheckoutAction() });
            spyOn(apiService, 'sendCompletedCheckout').and.returnValue(of(subscriptionInfo));
            const expected = cold('b', { b: new checkoutActions.CheckoutSuccessAction(subscriptionInfo) });
            expect(checkoutEffects.sendCompletedCheckout$).toBeObservable(expected);
            expect(apiService.sendCompletedCheckout).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return CHECKOUT_FAIL on CHECKOUT if sendCompletedCheckout fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new checkoutActions.CheckoutAction() });
            spyOn(apiService, 'sendCompletedCheckout').and.returnValue(throwError(null));
            const expected = cold('b', { b: new checkoutActions.CheckoutFailAction() });
            expect(checkoutEffects.sendCompletedCheckout$).toBeObservable(expected);
            expect(apiService.sendCompletedCheckout).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return GO to thank you page on CHECKOUT_SUCCESS',
        inject([StateService], stateService => {
            const subscriptionInfo = Object.assign(new SubscriptionInfo(), fixtures.subscriptionInfo);
            spyOn(stateService, 'dispatch');
            actions = cold('a', { a: new checkoutActions.CheckoutSuccessAction(subscriptionInfo) });
            const expected = cold('b', { b: new RouterActions.Go({ path: ['checkout/thankyou'] }) });
            expect(checkoutEffects.checkoutCompleteRedirect$).toBeObservable(expected);
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.LoadingAction());
        })
    );

    it('should reload page on GO to thank you page', () => {
        const testScheduler = getTestScheduler();
        testScheduler.run(() => {
            actions = cold('a', { a: new RouterActions.Go({ path: ['checkout/thankyou'] }) });
            const expected = cold('-b', { b: new pageActions.ReloadAction() });
            expect(checkoutEffects.checkoutCompleteReload$).toBeObservable(expected);
        });
    });

    it('should return CHECKOUT on CHANGE_CREDIT_CARD', () => {
        actions = cold('a', { a: new checkoutActions.CheckoutChangeCreditCardAction('reference') });
        const expected = cold('b', { b: new checkoutActions.CheckoutAction() });
        expect(checkoutEffects.creditCardDetails$).toBeObservable(expected);
    });

    it('should return PREVIEW on CHANGE_VOUCHER_CODE', () => {
        const voucher = new Voucher();
        actions = cold('a', { a: new checkoutActions.CheckoutChangeVoucherAction(voucher) });
        const expected = cold('b', { b: new checkoutActions.PreviewAction() });
        expect(checkoutEffects.triggerPreview$).toBeObservable(expected);
    });

    it('should return PREVIEW on CHECK_PRODUCT_OPTION', () => {
        const addonKey = 'addon_payroll';
        actions = cold('a', { a: new checkoutActions.CheckoutCheckProductOptionAction(addonKey) });
        const expected = cold('b', { b: new checkoutActions.PreviewAction() });
        expect(checkoutEffects.triggerPreview$).toBeObservable(expected);
    });

    it('should return PREVIEW on UNCHECK_PRODUCT_OPTION', () => {
        const addonKey = 'addon_payroll';
        actions = cold('a', { a: new checkoutActions.CheckoutUncheckProductOptionAction(addonKey) });
        const expected = cold('b', { b: new checkoutActions.PreviewAction() });
        expect(checkoutEffects.triggerPreview$).toBeObservable(expected);
    });

    it('should return PREVIEW on UPDATE_PLAN', () => {
        actions = cold('a', { a: new checkoutActions.CheckoutChangePlanAction(checkoutRequest.plan) });
        const expected = cold('b', { b: new checkoutActions.PreviewAction() });
        expect(checkoutEffects.triggerPreview$).toBeObservable(expected);
    });

    it('should return ERROR_MSG on CHECKOUT_FAIL action', () => {
        actions = cold('a', { a: new checkoutActions.CheckoutFailAction() });
        const payload = {
            messageKey: 'billing.checkout_fail',
            titleKey: 'globals.try_again',
            options: { data: { action: new checkoutActions.CheckoutAction() } }
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(checkoutEffects.checkoutCompleteFail$).toBeObservable(expected);
    });

    it('should return ERROR_MSG on CREDIT_CARD_ERROR action', () => {
        const errorMessage = 'error_message';
        actions = cold('a', { a: new creditCardActions.CreditCardError(errorMessage) });
        const payload = {
            messageKey: errorMessage
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(checkoutEffects.creditCardFail$).toBeObservable(expected);
    });

    it('should test metadata', () => {
        expect(metadata.getCheckoutPreview$).toEqual({ dispatch: true });
        expect(metadata.sendCompletedCheckout$).toEqual({ dispatch: true });
        expect(metadata.checkoutCompleteRedirect$).toEqual({ dispatch: true });
        expect(metadata.checkoutInit$).toEqual({ dispatch: true });
        expect(metadata.checkoutCompleteReload$).toEqual({ dispatch: true });
        expect(metadata.creditCardDetails$).toEqual({ dispatch: true });
        expect(metadata.triggerPreview$).toEqual({ dispatch: true });
        expect(metadata.checkoutCompleteFail$).toEqual({ dispatch: true });
        expect(metadata.creditCardFail$).toEqual({ dispatch: true });
    });
});
