import { Inject, Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { windowLocationToken } from '@bx-client/env';
import { Actions, Effect } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

import * as pageActions from '../actions/page.action';
import { DomService } from '../services/dom.service';

@Injectable()
export class PageEffects {
    @Effect({ dispatch: false })
    changeTitle$: Observable<string> = this.actions$
        .ofType(pageActions.SET_PAGE_TITLE)
        .pipe(
            map((action: pageActions.SetPageTitleAction) => action.payload),
            switchMap(titleTranslationKey =>
                this.translateService
                    .get(titleTranslationKey)
                    .pipe(map(title => `${title} | bexio`), tap(title => this.titleService.setTitle(title)))
            )
        );

    @Effect({ dispatch: false })
    reload$: Observable<any> = this.actions$.ofType(pageActions.RELOAD).pipe(tap(() => this.location.reload()));

    @Effect({ dispatch: false })
    loading$: Observable<any> = this.actions$
        .ofType(pageActions.LOADING, pageActions.FORCE_LOADING)
        .pipe(tap(() => this.domService.dispatchLoadingEvent()));

    @Effect({ dispatch: false })
    idle$: Observable<any> = this.actions$.ofType(pageActions.IDLE).pipe(tap(() => this.domService.dispatchIdleEvent()));

    constructor(
        private actions$: Actions,
        private titleService: Title,
        private translateService: TranslateService,
        private domService: DomService,
        @Inject(windowLocationToken) private location: any
    ) {}
}
