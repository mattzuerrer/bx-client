import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import * as paymentHistoryActions from '../actions/payment-history.action';
import { ApiService } from '../services';

@Injectable()
export class PaymentHistoryEffects {
    @Effect()
    fetchPaymentHistory$: Observable<any> = this.actions$
        .ofType(paymentHistoryActions.LOAD)
        .pipe(
            switchMap(() =>
                this.apiService
                    .getPaymentHistory()
                    .pipe(
                        map(payments => new paymentHistoryActions.LoadSuccessAction(payments)),
                        catchError(() => of(new paymentHistoryActions.LoadFailAction()))
                    )
            )
        );

    constructor(private actions$: Actions, private apiService: ApiService) {}
}
