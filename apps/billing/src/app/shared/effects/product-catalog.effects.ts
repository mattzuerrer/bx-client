import { Injectable } from '@angular/core';
import { MessageActions } from '@bx-client/message';
import { Actions, Effect } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, mapTo, switchMap } from 'rxjs/operators';

import * as productCatalogActions from '../actions/product-catalog.action';
import { ApiService } from '../services';

@Injectable()
export class ProductCatalogEffects {
    @Effect()
    fetchCatalog$: Observable<any> = this.actions$
        .ofType(productCatalogActions.LOAD)
        .pipe(
            switchMap(() =>
                this.apiService
                    .getProductCatalog()
                    .pipe(
                        map(catalog => new productCatalogActions.LoadSuccessAction(catalog)),
                        catchError(() => of(new productCatalogActions.LoadFailAction()))
                    )
            )
        );

    @Effect({ dispatch: true })
    productCatalogLoadFail$: Observable<any> = this.actions$.ofType(productCatalogActions.LOAD_FAIL).pipe(
        mapTo(
            new MessageActions.ErrorMessage({
                messageKey: 'billing.load_subscription_info_no_permission'
            })
        )
    );

    constructor(private actions$: Actions, private apiService: ApiService) {}
}
