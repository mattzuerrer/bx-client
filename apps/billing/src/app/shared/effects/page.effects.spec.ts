import { inject, TestBed } from '@angular/core/testing';
import { Title } from '@angular/platform-browser';
import { windowLocationToken } from '@bx-client/env';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { TranslateService } from '@ngx-translate/core';
import { cold } from 'jasmine-marbles';
import { noop, Observable, of } from 'rxjs';

import * as pageActions from '../actions/page.action';
import { DomServiceMockProvider, TitleServiceMockProvider, TranslateServiceMockProvider } from '../mocks';
import { DomService } from '../services';

import { PageEffects } from './page.effects';

const locationStub = {
    reload: () => noop()
};

const locationProvider = {
    provide: windowLocationToken,
    useValue: locationStub
};

describe('Page Effects', () => {
    let pageEffects: PageEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<PageEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                PageEffects,
                TitleServiceMockProvider,
                TranslateServiceMockProvider,
                DomServiceMockProvider,
                locationProvider,
                provideMockActions(() => actions)
            ]
        });

        pageEffects = TestBed.get(PageEffects);
        metadata = getEffectsMetadata(pageEffects);
    });

    it(
        'should set page title on SET_PAGE_TITLE',
        inject([Title, TranslateService], (titleService, translateService) => {
            actions = cold('a', { a: new pageActions.SetPageTitleAction('page_title') });
            spyOn(translateService, 'get').and.returnValue(of('page_title_translation'));
            spyOn(titleService, 'setTitle').and.callThrough();
            const expected = cold('b', { b: 'page_title_translation | bexio' });
            expect(pageEffects.changeTitle$).toBeObservable(expected);
            expect(translateService.get).toHaveBeenCalledTimes(1);
            expect(translateService.get).toHaveBeenCalledWith('page_title');
            expect(titleService.setTitle).toHaveBeenCalledTimes(1);
            expect(titleService.setTitle).toHaveBeenCalledWith('page_title_translation | bexio');
        })
    );

    it(
        'should reload page on RELOAD',
        inject([windowLocationToken], location => {
            actions = cold('a', { a: new pageActions.ReloadAction() });
            spyOn(location, 'reload').and.callThrough();
            const expected = cold('b', { b: new pageActions.ReloadAction() });
            expect(pageEffects.reload$).toBeObservable(expected);
            expect(location.reload).toHaveBeenCalled();
        })
    );

    it(
        'should dispatch loading event on LOADING',
        inject([DomService], domService => {
            actions = cold('a', { a: new pageActions.LoadingAction() });
            spyOn(domService, 'dispatchLoadingEvent').and.callThrough();
            const expected = cold('b', { b: new pageActions.LoadingAction() });
            expect(pageEffects.loading$).toBeObservable(expected);
            expect(domService.dispatchLoadingEvent).toHaveBeenCalled();
        })
    );

    it(
        'should dispatch loading event on FORCE_LOADING',
        inject([DomService], domService => {
            actions = cold('a', { a: new pageActions.ForceLoadingAction() });
            spyOn(domService, 'dispatchLoadingEvent').and.callThrough();
            const expected = cold('b', { b: new pageActions.ForceLoadingAction() });
            expect(pageEffects.loading$).toBeObservable(expected);
            expect(domService.dispatchLoadingEvent).toHaveBeenCalled();
        })
    );

    it(
        'should dispatch idle event on IDLE',
        inject([DomService], domService => {
            actions = cold('a', { a: new pageActions.IdleAction() });
            spyOn(domService, 'dispatchIdleEvent').and.callThrough();
            const expected = cold('b', { b: new pageActions.IdleAction() });
            expect(pageEffects.idle$).toBeObservable(expected);
            expect(domService.dispatchIdleEvent).toHaveBeenCalled();
        })
    );

    it('should test metadata', () => {
        expect(metadata.changeTitle$).toEqual({ dispatch: false });
        expect(metadata.reload$).toEqual({ dispatch: false });
        expect(metadata.loading$).toEqual({ dispatch: false });
        expect(metadata.idle$).toEqual({ dispatch: false });
    });
});
