import { inject, TestBed } from '@angular/core/testing';
import { BxLocalStorageService, localStorageConfigToken } from '@bx-client/local-storage';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { Observable, of } from 'rxjs';

import { LocalStorageConfig } from '../../../config';
import * as checkoutActions from '../actions/checkout.action';
import * as fixtures from '../fixtures';
import { LocalStorageServiceMockProvider, StateServiceMockProvider } from '../mocks';
import { Addon, CheckoutSummary, Plan, SubscriptionInfo, User, Voucher } from '../models';
import { StateService } from '../services';

import { GoogleEffects, googleTagManagerDataLayer } from './google.effects';

const addons = [new Addon(), new Addon(), new Addon()];
const users = [new User(), new User()];
const plan = new Plan();
const voucher = new Voucher();
const checkoutPreview = new CheckoutSummary();

describe('Google Effects', () => {
    let googleEffects: GoogleEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<GoogleEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                GoogleEffects,
                StateServiceMockProvider,
                LocalStorageServiceMockProvider,
                {
                    provide: localStorageConfigToken,
                    useClass: LocalStorageConfig
                },
                provideMockActions(() => actions)
            ]
        });

        const stateService = TestBed.get(StateService);

        stateService.subscriptionInfoInstanceAddons$ = of(addons);
        stateService.usersCollection$ = of(users);
        stateService.checkoutInstancePlan$ = of(plan);
        stateService.checkoutInstanceVoucher$ = of(voucher);
        stateService.checkoutPreview$ = of(checkoutPreview);

        googleEffects = TestBed.get(GoogleEffects);
        metadata = getEffectsMetadata(googleEffects);
    });

    it('should push to dataLayer on STEP_1_LOADED', () => {
        actions = cold('a', { a: new checkoutActions.Step1LoadedAction() });
        spyOn(googleTagManagerDataLayer, 'push').and.callThrough();
        const expected = cold('(b|)', { b: { plan, addons, users } });
        expect(googleEffects.checkoutStep1$).toBeObservable(expected);
        expect(googleTagManagerDataLayer.push).toHaveBeenCalledTimes(1);
    });

    it('should push to dataLayer on STEP_2_LOADED', () => {
        actions = cold('a', { a: new checkoutActions.Step2LoadedAction() });
        spyOn(googleTagManagerDataLayer, 'push').and.callThrough();
        const expected = cold('(b|)', { b: { plan, addons, users } });
        expect(googleEffects.checkoutStep2$).toBeObservable(expected);
        expect(googleTagManagerDataLayer.push).toHaveBeenCalledTimes(1);
    });

    it('should push to dataLayer on STEP_3_LOADED', () => {
        actions = cold('a', { a: new checkoutActions.Step3LoadedAction() });
        const expected = cold('(b|)', { b: new checkoutActions.Step3LoadedAction() });
        spyOn(googleTagManagerDataLayer, 'push').and.callThrough();
        expect(googleEffects.checkoutStep3$).toBeObservable(expected);
        expect(googleTagManagerDataLayer.push).toHaveBeenCalledTimes(1);
    });

    it(
        'should save to locale storage on CHECKOUT_SUCCESS',
        inject([BxLocalStorageService], localStorageService => {
            const subscriptionInfo = Object.assign(new SubscriptionInfo(), fixtures.subscriptionInfo);
            actions = cold('a', { a: new checkoutActions.CheckoutSuccessAction(subscriptionInfo) });
            spyOn(localStorageService, 'set').and.callThrough();
            const expected = cold('(b|)', { b: { plan, voucher, addons, users, checkoutPreview } });
            expect(googleEffects.checkoutSuccess$).toBeObservable(expected);
            expect(localStorageService.set).toHaveBeenCalledTimes(2);
        })
    );

    it(
        'should push to dataLayer on THANK_YOU_PAGE_LOADED if key exists',
        inject([BxLocalStorageService, localStorageConfigToken], (localStorageService, localStorageConfig) => {
            const savedData = {};
            actions = cold('a', { a: new checkoutActions.ThankYouPageLoadedAction() });
            spyOn(localStorageService, 'get').and.returnValues(null, savedData);
            spyOn(googleTagManagerDataLayer, 'push').and.callThrough();
            spyOn(localStorageService, 'remove').and.callThrough();
            const expected = cold('(b|)', { b: {} });
            expect(googleEffects.thankYouPageLoaded$).toBeObservable(expected);
            expect(localStorageService.get.calls.allArgs()).toEqual([
                [localStorageConfig.keys.checkoutAnalyticsData],
                [localStorageConfig.keys.checkoutSuccess]
            ]);
            expect(googleTagManagerDataLayer.push).toHaveBeenCalledTimes(1);
            expect(localStorageService.remove).toHaveBeenCalledWith(localStorageConfig.keys.checkoutSuccess);
        })
    );

    it(
        'should not push to dataLayer on THANK_YOU_PAGE_LOADED if refresh flag is on',
        inject([BxLocalStorageService, localStorageConfigToken], (localStorageService, localStorageConfig) => {
            actions = cold('a', { a: new checkoutActions.ThankYouPageLoadedAction() });
            spyOn(localStorageService, 'get').and.returnValues(true, null);
            spyOn(googleTagManagerDataLayer, 'push').and.callThrough();
            spyOn(localStorageService, 'remove').and.callThrough();
            const expected = cold('-');
            expect(googleEffects.thankYouPageLoaded$).toBeObservable(expected);
            expect(localStorageService.get).toHaveBeenCalledWith(localStorageConfig.keys.checkoutAnalyticsData);
            expect(localStorageService.remove).toHaveBeenCalledWith(localStorageConfig.keys.checkoutAnalyticsData);
            expect(googleTagManagerDataLayer.push).toHaveBeenCalledTimes(0);
        })
    );

    it(
        'should not push to dataLayer on THANK_YOU_PAGE_LOADED if key does not exists',
        inject([BxLocalStorageService, localStorageConfigToken], (localStorageService, localStorageConfig) => {
            actions = cold('a', { a: new checkoutActions.ThankYouPageLoadedAction() });
            spyOn(localStorageService, 'get').and.returnValues(null, null);
            spyOn(localStorageService, 'remove').and.callThrough();
            spyOn(googleTagManagerDataLayer, 'push').and.callThrough();
            const expected = cold('-');
            expect(googleEffects.thankYouPageLoaded$).toBeObservable(expected);
            expect(localStorageService.get.calls.allArgs()).toEqual([
                [localStorageConfig.keys.checkoutAnalyticsData],
                [localStorageConfig.keys.checkoutSuccess]
            ]);
            expect(googleTagManagerDataLayer.push).toHaveBeenCalledTimes(0);
            expect(localStorageService.remove).toHaveBeenCalledTimes(0);
        })
    );

    it('should test metadata', () => {
        expect(metadata.checkoutStep1$).toEqual({ dispatch: false });
        expect(metadata.checkoutStep2$).toEqual({ dispatch: false });
        expect(metadata.checkoutStep3$).toEqual({ dispatch: false });
        expect(metadata.checkoutSuccess$).toEqual({ dispatch: false });
        expect(metadata.thankYouPageLoaded$).toEqual({ dispatch: false });
    });
});
