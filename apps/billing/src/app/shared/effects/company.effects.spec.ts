import { inject, TestBed } from '@angular/core/testing';
import { MessageActions } from '@bx-client/message';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { Observable, of, throwError } from 'rxjs';

import * as companyActions from '../actions/company.action';
import * as fixtures from '../fixtures';
import { ApiServiceMockProvider } from '../mocks';
import { CompanyProfile } from '../models';
import { ApiService } from '../services';

import { CompanyEffects } from './company.effects';

describe('Company Effect', () => {
    let companyEffects: CompanyEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<CompanyEffects>;

    const companyProfile = Object.assign(new CompanyProfile(), fixtures.companyProfile);
    const savedCompanyProfile = Object.assign(new CompanyProfile(), companyProfile, {
        id: 12345,
        name: 'company_name'
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [CompanyEffects, ApiServiceMockProvider, provideMockActions(() => actions)]
        });

        companyEffects = TestBed.get(CompanyEffects);
        metadata = getEffectsMetadata(companyEffects);
    });

    it(
        'should return LOAD_SUCCESS on LOAD if fetchCompanyProfile is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new companyActions.LoadAction() });
            const expected = cold('b', { b: new companyActions.LoadSuccessAction(companyProfile) });
            spyOn(apiService, 'getCompanyProfile').and.returnValue(of(companyProfile));
            expect(companyEffects.fetchCompanyProfile$).toBeObservable(expected);
            expect(apiService.getCompanyProfile).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_FAIL on LOAD if fetchCompanyProfile fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new companyActions.LoadAction() });
            const expected = cold('b', { b: new companyActions.LoadFailAction() });
            spyOn(apiService, 'getCompanyProfile').and.returnValue(throwError(null));
            expect(companyEffects.fetchCompanyProfile$).toBeObservable(expected);
            expect(apiService.getCompanyProfile).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return SAVE_SUCCESS on SAVE if saveCompanyProfile is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new companyActions.SaveAction(companyProfile) });
            const expected = cold('b', { b: new companyActions.SaveSuccessAction(savedCompanyProfile) });
            spyOn(apiService, 'saveCompanyProfile').and.returnValue(of(savedCompanyProfile));
            expect(companyEffects.saveCompanyProfile).toBeObservable(expected);
            expect(apiService.saveCompanyProfile).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return SAVE_FAIL on SAVE if saveCompanyProfile fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new companyActions.SaveAction(companyProfile) });
            const expected = cold('b', { b: new companyActions.SaveFailAction(companyProfile) });
            spyOn(apiService, 'saveCompanyProfile').and.returnValue(throwError(null));
            expect(companyEffects.saveCompanyProfile).toBeObservable(expected);
            expect(apiService.saveCompanyProfile).toHaveBeenCalledTimes(1);
        })
    );

    it('should return ERROR_MSG on SAVE_FAIL action', () => {
        actions = cold('a', { a: new companyActions.SaveFailAction(companyProfile) });
        const payload = {
            messageKey: 'billing.save_company_profile_fail',
            titleKey: 'globals.try_again',
            options: { data: { action: new companyActions.SaveAction(companyProfile) } }
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(companyEffects.companyProfileSaveFail$).toBeObservable(expected);
    });

    it('should test metadata', () => {
        expect(metadata.fetchCompanyProfile$).toEqual({ dispatch: true });
        expect(metadata.saveCompanyProfile).toEqual({ dispatch: true });
        expect(metadata.companyProfileSaveFail$).toEqual({ dispatch: true });
    });
});
