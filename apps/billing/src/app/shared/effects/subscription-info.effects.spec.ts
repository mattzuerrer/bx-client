import { inject, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { TranslateServiceMockProvider } from '@bx-client/i18n';
import { MessageActions } from '@bx-client/message';
import { RouterActions } from '@bx-client/router';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { TranslateService } from '@ngx-translate/core';
import { cold } from 'jasmine-marbles';
import { Observable, of, throwError } from 'rxjs';

import * as addonActions from '../actions/addon.action';
import * as pageActions from '../actions/page.action';
import * as subscriptionInfoActions from '../actions/subscription-info.action';
import * as fixtures from '../fixtures';
import { ApiServiceMockProvider, StateServiceMockProvider } from '../mocks';
import { Addon, CheckoutRequest, CompanyProfile, CreditCard, PaymentMethod, SubscriptionInfo } from '../models';
import { ApiService, StateService } from '../services';

import { SubscriptionInfoEffects } from './subscription-info.effects';

const queryParams = {
    redirect_to: 'user_manager',
    upgrade_plan: true
};

const activatedRouteProvider = {
    provide: ActivatedRoute,
    useValue: { queryParams: of(queryParams) }
};

describe('SubscriptionInfo Effects', () => {
    let subscriptionInfoEffects: SubscriptionInfoEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<SubscriptionInfoEffects>;

    const addon = Object.assign(new Addon(), fixtures.addon);
    const subscriptionInfo = Object.assign(new SubscriptionInfo(), fixtures.subscriptionInfo);
    const plan = subscriptionInfo.plan;
    const productOptions = ['addon_payroll'];
    const productOptionsAttributes = { addon_payroll: { endingUnit: 99 } };
    const checkoutRequest = Object.assign(new CheckoutRequest(), { plan, productOptions, productOptionsAttributes });

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                SubscriptionInfoEffects,
                ApiServiceMockProvider,
                StateServiceMockProvider,
                activatedRouteProvider,
                TranslateServiceMockProvider,
                provideMockActions(() => actions)
            ]
        });

        const stateService = TestBed.get(StateService);

        stateService.overviewPlan$ = of(plan);
        stateService.overviewProductOptions$ = of(productOptions);
        stateService.overviewProductOptionsAttributes$ = of(productOptionsAttributes);

        subscriptionInfoEffects = TestBed.get(SubscriptionInfoEffects);
        metadata = getEffectsMetadata(subscriptionInfoEffects);
    });

    it(
        'should return LOAD_SUCCESS on LOAD if getSubscriptionInfo is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new subscriptionInfoActions.LoadAction() });
            const expected = cold('b', { b: new subscriptionInfoActions.LoadSuccessAction(subscriptionInfo) });
            spyOn(apiService, 'getSubscriptionInfo').and.returnValue(of(subscriptionInfo));
            expect(subscriptionInfoEffects.getSubscriptionInfo$).toBeObservable(expected);
            expect(apiService.getSubscriptionInfo).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_FAIL on LOAD if getSubscriptionInfo fails',
        inject([ApiService], apiService => {
            const errorMessage = 'billing.load_subscription_info_fail';
            actions = cold('a', { a: new subscriptionInfoActions.LoadAction() });
            const expected = cold('b', { b: new subscriptionInfoActions.LoadFailAction(errorMessage) });
            spyOn(apiService, 'getSubscriptionInfo').and.returnValue(throwError(null));
            expect(subscriptionInfoEffects.getSubscriptionInfo$).toBeObservable(expected);
            expect(apiService.getSubscriptionInfo).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_FAIL with no permission message on LOAD if getSubscriptionInfo fails with http forbidden',
        inject([ApiService], apiService => {
            const errorMessage = 'billing.load_subscription_info_no_permission';
            actions = cold('a', { a: new subscriptionInfoActions.LoadAction() });
            const expected = cold('b', { b: new subscriptionInfoActions.LoadFailAction(errorMessage) });
            spyOn(apiService, 'getSubscriptionInfo').and.returnValue(
                throwError({
                    status: 403
                })
            );
            expect(subscriptionInfoEffects.getSubscriptionInfo$).toBeObservable(expected);
            expect(apiService.getSubscriptionInfo).toHaveBeenCalledTimes(1);
        })
    );

    it('should return LOAD_CREDIT_CARD on LOAD_SUCCESS if payment method is credit card', () => {
        const subscriptionInfoCard = Object.assign(new SubscriptionInfo(), {
            paymentMethod: Object.assign(new PaymentMethod(), { type: PaymentMethod.card })
        });
        actions = cold('a', { a: new subscriptionInfoActions.LoadSuccessAction(subscriptionInfoCard) });
        const expected = cold('b', { b: new subscriptionInfoActions.LoadCreditCardAction() });
        expect(subscriptionInfoEffects.initCreditCard$).toBeObservable(expected);
    });

    it('should not return LOAD_CREDIT_CARD on LOAD_SUCCESS if payment method is not credit card', () => {
        const subscriptionInfoInvoice = Object.assign(new SubscriptionInfo(), {
            paymentMethod: Object.assign(new PaymentMethod(), { type: PaymentMethod.invoice })
        });
        actions = cold('a', { a: new subscriptionInfoActions.LoadSuccessAction(subscriptionInfoInvoice) });
        const expected = cold('-');
        expect(subscriptionInfoEffects.initCreditCard$).toBeObservable(expected);
    });

    it('should return LOAD_CREDIT_CARD on UPDATE_PAYMENT_METHOD_SUCCESS if payment method is credit card', () => {
        const paymentMethod = Object.assign(new PaymentMethod(), { type: PaymentMethod.card });
        actions = cold('a', { a: new subscriptionInfoActions.UpdatePaymentMethodSuccessAction(paymentMethod) });
        const expected = cold('b', { b: new subscriptionInfoActions.LoadCreditCardAction() });
        expect(subscriptionInfoEffects.updateCreditCard$).toBeObservable(expected);
    });

    it('should not return LOAD_CREDIT_CARD on UPDATE_PAYMENT_METHOD_SUCCESS if payment method is not credit card', () => {
        const paymentMethodInvoice = Object.assign(new PaymentMethod(), { type: PaymentMethod.invoice });
        const stub = jasmine.createSpyObj('stub', ['init']);
        actions = cold('a', {
            a: new subscriptionInfoActions.UpdatePaymentMethodSuccessAction(paymentMethodInvoice)
        });
        subscriptionInfoEffects.updateCreditCard$.subscribe(result => stub.init());
        expect(stub.init).toHaveBeenCalledTimes(0);
    });

    it(
        'should return LOAD_CREDIT_CARD_SUCCESS on LOAD_CREDIT_CARD if LoadCreditCard is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new subscriptionInfoActions.LoadCreditCardAction() });
            const expected = cold('b', {
                b: new subscriptionInfoActions.LoadCreditCardSuccessAction(new CreditCard())
            });
            spyOn(apiService, 'getCreditCardInfo').and.returnValue(of(new CreditCard()));
            expect(subscriptionInfoEffects.loadCreditCard$).toBeObservable(expected);
            expect(apiService.getCreditCardInfo).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_CREDIT_CARD_FAIL on LOAD_CREDIT_CARD if LoadCreditCard fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new subscriptionInfoActions.LoadCreditCardAction() });
            const expected = cold('b', { b: new subscriptionInfoActions.LoadCreditCardFailAction() });
            spyOn(apiService, 'getCreditCardInfo').and.returnValue(throwError(null));
            expect(subscriptionInfoEffects.loadCreditCard$).toBeObservable(expected);
            expect(apiService.getCreditCardInfo).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return UPDATE_PLAN_SUCCESS on UPDATE_PLAN if changeSubscriptionPlan is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new subscriptionInfoActions.UpdatePlanAction() });
            const expected = cold('b', { b: new subscriptionInfoActions.UpdatePlanSuccessAction(subscriptionInfo) });
            spyOn(apiService, 'changeSubscriptionPlan').and.returnValue(of(subscriptionInfo));
            expect(subscriptionInfoEffects.updatePlan$).toBeObservable(expected);
            expect(apiService.changeSubscriptionPlan).toHaveBeenCalledTimes(1);
            expect(apiService.changeSubscriptionPlan).toHaveBeenCalledWith(checkoutRequest);
        })
    );

    it(
        'should return UPDATE_PLAN_FAIL on UPDATE_PLAN if changeSubscriptionPlan fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new subscriptionInfoActions.UpdatePlanAction() });
            const expected = cold('b', { b: new subscriptionInfoActions.UpdatePlanFailAction() });
            spyOn(apiService, 'changeSubscriptionPlan').and.returnValue(throwError(null));
            expect(subscriptionInfoEffects.updatePlan$).toBeObservable(expected);
            expect(apiService.changeSubscriptionPlan).toHaveBeenCalledTimes(1);
            expect(apiService.changeSubscriptionPlan).toHaveBeenCalledWith(checkoutRequest);
        })
    );

    it(
        'should return UPDATE_BILLING_ADDRESS_SUCCESS on UPDATE_BILLING_ADDRESS if changeBillingAddress is successful',
        inject([ApiService], apiService => {
            const billingInfo = Object.assign(new CompanyProfile(), fixtures.companyProfile);
            actions = cold('a', { a: new subscriptionInfoActions.UpdateBillingAddressAction(billingInfo) });
            const expected = cold('b', {
                b: new subscriptionInfoActions.UpdateBillingAddressSuccessAction(billingInfo)
            });
            spyOn(apiService, 'changeBillingAddress').and.returnValue(of(billingInfo));
            expect(subscriptionInfoEffects.updateBillingAddress$).toBeObservable(expected);
            expect(apiService.changeBillingAddress).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return UPDATE_BILLING_ADDRESS_FAIL on UPDATE_BILLING_ADDRESS if changeBillingAddress fails',
        inject([ApiService], apiService => {
            const billingInfo = Object.assign(new CompanyProfile(), fixtures.companyProfile);
            actions = cold('a', { a: new subscriptionInfoActions.UpdateBillingAddressAction(billingInfo) });
            const expected = cold('b', { b: new subscriptionInfoActions.UpdateBillingAddressFailAction(billingInfo) });
            spyOn(apiService, 'changeBillingAddress').and.returnValue(throwError(null));
            expect(subscriptionInfoEffects.updateBillingAddress$).toBeObservable(expected);
            expect(apiService.changeBillingAddress).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should filter out UPDATE_PAYMENT_METHOD action if payment method type is card and reference id is not set',
        inject([ApiService], apiService => {
            const paymentMethod = Object.assign(new PaymentMethod(), { type: 'card' });
            actions = cold('a', { a: new subscriptionInfoActions.UpdatePaymentMethodAction(paymentMethod) });
            const expected = cold('-');
            expect(subscriptionInfoEffects.updatePaymentMethod$).toBeObservable(expected);
        })
    );

    it(
        'should return UPDATE_PAYMENT_METHOD_SUCCESS on UPDATE_PAYMENT_METHOD if changePaymentMethod is successful',
        inject([ApiService], apiService => {
            const paymentMethod = new PaymentMethod();
            actions = cold('a', { a: new subscriptionInfoActions.UpdatePaymentMethodAction(paymentMethod) });
            const expected = cold('b', {
                b: new subscriptionInfoActions.UpdatePaymentMethodSuccessAction(paymentMethod)
            });
            spyOn(apiService, 'changePaymentMethod').and.returnValue(of(paymentMethod));
            expect(subscriptionInfoEffects.updatePaymentMethod$).toBeObservable(expected);
            expect(apiService.changePaymentMethod).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return UPDATE_PAYMENT_METHOD_FAIL on UPDATE_PAYMENT_METHOD if changePaymentMethod fails',
        inject([ApiService], apiService => {
            const paymentMethod = new PaymentMethod();
            actions = cold('a', { a: new subscriptionInfoActions.UpdatePaymentMethodAction(paymentMethod) });
            const expected = cold('b', { b: new subscriptionInfoActions.UpdatePaymentMethodFailAction(paymentMethod) });
            spyOn(apiService, 'changePaymentMethod').and.returnValue(throwError(null));
            expect(subscriptionInfoEffects.updatePaymentMethod$).toBeObservable(expected);
            expect(apiService.changePaymentMethod).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return ACTIVATE_SUCCESS on ACTIVATE if activateAddon is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new addonActions.ActivateAction(addon) });
            const expected = cold('b', { b: new addonActions.ActivateSuccessAction(addon) });
            spyOn(apiService, 'activateAddon').and.returnValue(of(addon));
            expect(subscriptionInfoEffects.activateAddon$).toBeObservable(expected);
            expect(apiService.activateAddon).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return ACTIVATE_FAIL on ACTIVATE if activateAddon fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new addonActions.ActivateAction(addon) });
            const expected = cold('b', { b: new addonActions.ActivateFailAction(addon) });
            spyOn(apiService, 'activateAddon').and.returnValue(throwError(null));
            expect(subscriptionInfoEffects.activateAddon$).toBeObservable(expected);
            expect(apiService.activateAddon).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return DEACTIVATE_SUCCESS on DEACTIVATE if deactivateAddon is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new addonActions.DeactivateAction(addon) });
            const expected = cold('b', { b: new addonActions.DeactivateSuccessAction(addon) });
            spyOn(apiService, 'deactivateAddon').and.returnValue(of(addon));
            expect(subscriptionInfoEffects.deactivateAddon$).toBeObservable(expected);
            expect(apiService.deactivateAddon).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return DEACTIVATE_FAIL on DEACTIVATE if deactivateAddon fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new addonActions.DeactivateAction(addon) });
            const expected = cold('b', { b: new addonActions.DeactivateFailAction(addon) });
            spyOn(apiService, 'deactivateAddon').and.returnValue(throwError(null));
            expect(subscriptionInfoEffects.deactivateAddon$).toBeObservable(expected);
            expect(apiService.deactivateAddon).toHaveBeenCalledTimes(1);
        })
    );

    it('should FORCE LOADING and RELOAD on UPDATE_PLAN_SUCCESS', () => {
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();
            actions = cold('a', { a: new subscriptionInfoActions.UpdatePlanSuccessAction(subscriptionInfo) });
            const expected = cold('b', { b: 'user_manager' });
            expect(subscriptionInfoEffects.updatePlanSuccess$).toBeObservable(expected);
            expect(stateService.dispatch.calls.allArgs()).toEqual([
                [new RouterActions.Go({ path: ['user_manager'] })],
                [new pageActions.ForceLoadingAction()],
                [new pageActions.ReloadAction()]
            ]);
        });
    });

    it('should return both ERROR_MSG and IDLE on UPDATE_PLAN_FAIL', () => {
        actions = cold('a', { a: new subscriptionInfoActions.UpdatePlanFailAction() });
        const payload = {
            messageKey: 'billing.update_plan_fail',
            titleKey: 'globals.try_again',
            options: { data: { action: new subscriptionInfoActions.UpdatePlanAction() } }
        };
        const expected = cold('(bc)', { b: new MessageActions.ErrorMessage(payload), c: new pageActions.IdleAction() });
        expect(subscriptionInfoEffects.updatePlanFail$).toBeObservable(expected);
    });

    it(
        'should return ERROR_MSG on ACTIVATE_FAIL action',
        inject([TranslateService], translateService => {
            actions = cold('a', { a: new addonActions.ActivateFailAction(addon) });
            spyOn(translateService, 'get').and.returnValue(of('addonName'));
            const payload = {
                messageKey: 'billing.activate_addon_fail',
                titleKey: 'globals.try_again',
                options: {
                    data: { action: new addonActions.ActivateAction(addon), translateParams: { name: 'addonName' } }
                }
            };
            const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
            expect(subscriptionInfoEffects.addonActivateFail$).toBeObservable(expected);
        })
    );

    it(
        'should return ERROR_MSG on DEACTIVATE_FAIL action',
        inject([TranslateService], translateService => {
            actions = cold('a', { a: new addonActions.DeactivateFailAction(addon) });
            spyOn(translateService, 'get').and.returnValue(of('addonName'));
            const payload = {
                messageKey: 'billing.deactivate_addon_fail',
                titleKey: 'globals.try_again',
                options: {
                    data: { action: new addonActions.DeactivateAction(addon), translateParams: { name: 'addonName' } }
                }
            };
            const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
            expect(subscriptionInfoEffects.addonDeactivateFail$).toBeObservable(expected);
        })
    );

    it('should return ERROR_MSG on LOAD_FAIL action', () => {
        const errorMessage = 'error message';
        actions = cold('a', { a: new subscriptionInfoActions.LoadFailAction(errorMessage) });
        const payload = {
            messageKey: errorMessage,
            titleKey: 'globals.try_again',
            options: { data: { action: new subscriptionInfoActions.LoadAction() } }
        };
        const expected = cold('b', { b: new MessageActions.ErrorMessage(payload) });
        expect(subscriptionInfoEffects.subscriptionInfoFail$).toBeObservable(expected);
    });

    it('should test metadata', () => {
        expect(metadata.getSubscriptionInfo$).toEqual({ dispatch: true });
        expect(metadata.initCreditCard$).toEqual({ dispatch: true });
        expect(metadata.updateCreditCard$).toEqual({ dispatch: true });
        expect(metadata.loadCreditCard$).toEqual({ dispatch: true });
        expect(metadata.updatePlan$).toEqual({ dispatch: true });
        expect(metadata.updateBillingAddress$).toEqual({ dispatch: true });
        expect(metadata.updatePaymentMethod$).toEqual({ dispatch: true });
        expect(metadata.activateAddon$).toEqual({ dispatch: true });
        expect(metadata.deactivateAddon$).toEqual({ dispatch: true });
        expect(metadata.updatePlanSuccess$).toEqual({ dispatch: false });
        expect(metadata.updatePlanFail$).toEqual({ dispatch: true });
        expect(metadata.addonActivateFail$).toEqual({ dispatch: true });
        expect(metadata.addonDeactivateFail$).toEqual({ dispatch: true });
        expect(metadata.subscriptionInfoFail$).toEqual({ dispatch: true });
    });
});
