import { inject, TestBed } from '@angular/core/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { Observable, of, throwError } from 'rxjs';

import * as countryActions from '../actions/countries.action';
import * as fixtures from '../fixtures';
import { ApiServiceMockProvider } from '../mocks';
import { Country } from '../models';
import { ApiService } from '../services';

import { CountiresEffects } from './countries.effects';

describe('Countries Effects', () => {
    let countriesEffects: CountiresEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<CountiresEffects>;

    const countries = [Object.assign(new Country(), fixtures.country)];

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [CountiresEffects, ApiServiceMockProvider, provideMockActions(() => actions)]
        });

        countriesEffects = TestBed.get(CountiresEffects);
        metadata = getEffectsMetadata(countriesEffects);
    });

    it(
        'should return LOAD_SUCCESS on LOAD if getCountries is successful',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new countryActions.LoadAction() });
            const expected = cold('b', { b: new countryActions.LoadSuccessAction(countries) });
            spyOn(apiService, 'getCountries').and.returnValue(of(countries));
            expect(countriesEffects.fetchCountries$).toBeObservable(expected);
            expect(apiService.getCountries).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_FAIL on LOAD if getCountries fails',
        inject([ApiService], apiService => {
            actions = cold('a', { a: new countryActions.LoadAction() });
            const expected = cold('b', { b: new countryActions.LoadFailAction() });
            spyOn(apiService, 'getCountries').and.returnValue(throwError(null));
            expect(countriesEffects.fetchCountries$).toBeObservable(expected);
            expect(apiService.getCountries).toHaveBeenCalledTimes(1);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetchCountries$).toEqual({ dispatch: true });
    });
});
