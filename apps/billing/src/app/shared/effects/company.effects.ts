import { Injectable } from '@angular/core';
import { MessageActions } from '@bx-client/message';
import { Actions, Effect } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import * as companyActions from '../actions/company.action';
import { ApiService } from '../services';

@Injectable()
export class CompanyEffects {
    @Effect()
    fetchCompanyProfile$: Observable<any> = this.actions$
        .ofType(companyActions.LOAD)
        .pipe(
            switchMap(() =>
                this.apiService
                    .getCompanyProfile()
                    .pipe(
                        map(companyProfile => new companyActions.LoadSuccessAction(companyProfile)),
                        catchError(() => of(new companyActions.LoadFailAction()))
                    )
            )
        );

    @Effect()
    saveCompanyProfile: Observable<any> = this.actions$
        .ofType(companyActions.SAVE)
        .pipe(
            map((action: companyActions.SaveAction) => action.payload),
            switchMap(newCompanyProfile =>
                this.apiService
                    .saveCompanyProfile(newCompanyProfile)
                    .pipe(
                        map(companyProfile => new companyActions.SaveSuccessAction(companyProfile)),
                        catchError(() => of(new companyActions.SaveFailAction(newCompanyProfile)))
                    )
            )
        );

    @Effect({ dispatch: true })
    companyProfileSaveFail$: Observable<any> = this.actions$.ofType(companyActions.SAVE_FAIL).pipe(
        map(
            (action: companyActions.SaveFailAction) =>
                new MessageActions.ErrorMessage({
                    messageKey: 'billing.save_company_profile_fail',
                    titleKey: 'globals.try_again',
                    options: { data: { action: new companyActions.SaveAction(action.payload) } }
                })
        )
    );

    constructor(private actions$: Actions, private apiService: ApiService) {}
}
