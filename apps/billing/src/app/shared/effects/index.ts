import { CheckoutEffects } from './checkout.effects';
import { CompanyEffects } from './company.effects';
import { CountiresEffects } from './countries.effects';
import { GoogleEffects } from './google.effects';
import { OverviewEffects } from './overview.effects';
import { PageEffects } from './page.effects';
import { PaymentHistoryEffects } from './payment-history.effects';
import { ProductCatalogEffects } from './product-catalog.effects';
import { RouterEffects } from './router.effects';
import { RsaSignatureEffects } from './rsa-signature.effects';
import { SubscriptionInfoEffects } from './subscription-info.effects';
import { UserEffects } from './user.effects';

export const sharedEffects = [
    UserEffects,
    ProductCatalogEffects,
    CompanyEffects,
    CountiresEffects,
    CheckoutEffects,
    RouterEffects,
    RsaSignatureEffects,
    SubscriptionInfoEffects,
    PaymentHistoryEffects,
    PageEffects,
    OverviewEffects,
    GoogleEffects
];
