import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import * as countriesActions from '../actions/countries.action';
import { ApiService } from '../services';

@Injectable()
export class CountiresEffects {
    @Effect()
    fetchCountries$: Observable<any> = this.actions$
        .ofType(countriesActions.LOAD)
        .pipe(
            switchMap(() =>
                this.apiService
                    .getCountries()
                    .pipe(
                        map(countries => new countriesActions.LoadSuccessAction(countries)),
                        catchError(() => of(new countriesActions.LoadFailAction()))
                    )
            )
        );

    constructor(private actions$: Actions, private apiService: ApiService) {}
}
