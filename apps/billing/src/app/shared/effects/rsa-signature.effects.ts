import { Inject, Injectable } from '@angular/core';
import { languageToken } from '@bx-client/env';
import { MessageActions } from '@bx-client/message';
import { Actions, Effect } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import * as rsaSignatureActions from '../actions/rsa-signature.action';
import { RsaSignature } from '../models';
import { ApiService } from '../services';

@Injectable()
export class RsaSignatureEffects {
    @Effect()
    getRsaSignature$: Observable<any> = this.actions$
        .ofType(rsaSignatureActions.LOAD)
        .pipe(
            switchMap(() =>
                this.apiService
                    .getRsaSignature()
                    .pipe(
                        map(rsaSignature => Object.assign(new RsaSignature(), rsaSignature, { locale: this.language })),
                        map(rsaSignature => new rsaSignatureActions.LoadSuccessAction(rsaSignature)),
                        catchError(() => of(new rsaSignatureActions.LoadFailAction()))
                    )
            )
        );

    @Effect({ dispatch: true })
    rsaSignatureLoadFail$: Observable<any> = this.actions$.ofType(rsaSignatureActions.LOAD_FAIL).pipe(
        map(
            (action: rsaSignatureActions.LoadFailAction) =>
                new MessageActions.ErrorMessage({
                    messageKey: 'billing.load_rsa_signature_fail',
                    titleKey: 'globals.try_again',
                    options: { data: { action: new rsaSignatureActions.LoadAction() } }
                })
        )
    );

    constructor(private actions$: Actions, private apiService: ApiService, @Inject(languageToken) private language: string) {}
}
