import { Injectable } from '@angular/core';
import { MessageActions } from '@bx-client/message';
import { Actions, Effect } from '@ngrx/effects';
import * as HTTPStatus from 'http-status';
import { Observable, of } from 'rxjs';
import { catchError, map, mapTo, switchMap, withLatestFrom } from 'rxjs/operators';

import * as userActions from '../actions/user.action';
import { ApiService, StateService } from '../services';

@Injectable()
export class UserEffects {
    @Effect()
    fetchUsers$: Observable<any> = this.actions$
        .ofType(userActions.LOAD, userActions.SORT)
        .pipe(
            withLatestFrom(this.stateService.usersColumnConfig$, (_, columnConfig) => columnConfig),
            map(columnConfig => columnConfig.join(',')),
            switchMap(orderBy =>
                this.apiService
                    .getUsers(orderBy)
                    .pipe(map(users => new userActions.LoadSuccessAction(users)), catchError(() => of(new userActions.LoadFailAction())))
            )
        );

    @Effect()
    fetchCurrentUser$: Observable<any> = this.actions$
        .ofType(userActions.FETCH_CURRENT)
        .pipe(
            switchMap(() =>
                this.apiService
                    .getCurrentUser()
                    .pipe(
                        map(user => new userActions.FetchCurrentSuccessAction(user)),
                        catchError(() => of(new userActions.FetchCurrentFailAction()))
                    )
            )
        );

    @Effect()
    inviteUser$: Observable<any> = this.actions$.ofType(userActions.INVITE).pipe(
        map((action: userActions.InviteAction) => action.payload),
        switchMap(email =>
            this.apiService.inviteUser(email).pipe(
                map(user => new userActions.InviteSuccessAction(user)),
                catchError(errorResponse => {
                    const error = errorResponse || {};
                    let reason = 'billing.invite_user.general_fail';
                    let retryAction = new userActions.InviteAction(email);
                    switch (error.status) {
                        case HTTPStatus.BAD_REQUEST:
                            reason = 'billing.invite_user.email_exists';
                            retryAction = null;
                            break;
                        case HTTPStatus.UNPROCESSABLE_ENTITY:
                            reason = 'billing.invite_user.invalid_email';
                            retryAction = null;
                            break;
                    }
                    return of(new userActions.InviteFailAction({ reason, retryAction }));
                })
            )
        )
    );

    @Effect()
    removeUser$: Observable<any> = this.actions$
        .ofType(userActions.REMOVE)
        .pipe(
            map((action: userActions.RemoveAction) => action.payload),
            switchMap(id =>
                this.apiService
                    .removeUser(id)
                    .pipe(map(() => new userActions.RemoveSuccessAction(id)), catchError(() => of(new userActions.RemoveFailAction(id))))
            )
        );

    @Effect({ dispatch: true })
    usersLoadFail$: Observable<any> = this.actions$.ofType(userActions.LOAD_FAIL).pipe(
        map(
            (action: userActions.LoadFailAction) =>
                new MessageActions.ErrorMessage({
                    messageKey: 'error.server.fetch_users',
                    titleKey: 'globals.try_again',
                    options: { data: { action: new userActions.LoadAction() } }
                })
        )
    );

    @Effect({ dispatch: true })
    usersInviteFail$: Observable<any> = this.actions$.ofType(userActions.INVITE_FAIL).pipe(
        map(
            (action: userActions.InviteFailAction) =>
                new MessageActions.ErrorMessage({
                    messageKey: action.payload.reason,
                    titleKey: 'globals.try_again',
                    options: { data: { action: action.payload.retryAction } }
                })
        )
    );

    @Effect({ dispatch: true })
    usersInviteSuccess$: Observable<any> = this.actions$.ofType(userActions.INVITE_SUCCESS).pipe(
        map(
            (action: userActions.InviteSuccessAction) =>
                new MessageActions.SuccessMessage({
                    messageKey: 'billing.invite_user_success',
                    titleKey: '',
                    options: { data: { translateParams: { email: action.payload.email } } }
                })
        )
    );

    @Effect({ dispatch: true })
    usersRemoveFail$: Observable<any> = this.actions$.ofType(userActions.REMOVE_FAIL).pipe(
        map(
            (action: userActions.RemoveFailAction) =>
                new MessageActions.ErrorMessage({
                    messageKey: 'billing.remove_user_fail',
                    titleKey: 'globals.try_again',
                    options: { data: { action: new userActions.RemoveAction(action.payload) } }
                })
        )
    );

    @Effect({ dispatch: true })
    usersRemoveSuccess$: Observable<any> = this.actions$.ofType(userActions.REMOVE_SUCCESS).pipe(
        mapTo(
            new MessageActions.SuccessMessage({
                messageKey: 'billing.remove_user_success'
            })
        )
    );

    constructor(private actions$: Actions, private apiService: ApiService, private stateService: StateService) {}
}
