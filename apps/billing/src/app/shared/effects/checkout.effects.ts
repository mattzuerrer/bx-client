import { Injectable } from '@angular/core';
import { MessageActions } from '@bx-client/message';
import { RouterActions } from '@bx-client/router';
import { by } from '@bx-client/utils';
import { Actions, Effect } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, combineLatest, delay, filter, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import * as checkoutActions from '../actions/checkout.action';
import * as creditCardActions from '../actions/credit-card.action';
import * as pageActions from '../actions/page.action';
import * as subscriptionInfoActions from '../actions/subscription-info.action';
import { CheckoutRequest, PaymentMethod, Plan } from '../models';
import { ApiService, StateService } from '../services';

const byIdentifier = by('identifier');

@Injectable()
export class CheckoutEffects {
    @Effect()
    getCheckoutPreview$: Observable<any> = this.actions$
        .ofType(checkoutActions.PREVIEW)
        .pipe(
            withLatestFrom(this.stateService.checkoutInstance$, (_, checkoutRequest) => checkoutRequest),
            switchMap(checkoutRequest =>
                this.apiService
                    .getCheckoutPreview(checkoutRequest)
                    .pipe(
                        map(checkoutSummary => new checkoutActions.PreviewSuccessAction(checkoutSummary)),
                        catchError(() => of(new checkoutActions.PreviewFailAction()))
                    )
            )
        );

    @Effect()
    sendCompletedCheckout$: Observable<any> = this.actions$
        .ofType(checkoutActions.CHECKOUT)
        .pipe(
            withLatestFrom(this.stateService.checkoutInstance$, (_, checkoutRequest) => checkoutRequest),
            switchMap(checkoutRequest =>
                this.apiService
                    .sendCompletedCheckout(checkoutRequest)
                    .pipe(
                        map(subscriptionInfo => new checkoutActions.CheckoutSuccessAction(subscriptionInfo)),
                        catchError(() => of(new checkoutActions.CheckoutFailAction()))
                    )
            )
        );

    @Effect()
    checkoutCompleteRedirect$: Observable<any> = this.actions$
        .ofType(checkoutActions.CHECKOUT_SUCCESS)
        .pipe(
            tap(() => this.stateService.dispatch(new pageActions.LoadingAction())),
            map(() => new RouterActions.Go({ path: ['checkout/thankyou'] }))
        );

    @Effect()
    checkoutInit$: Observable<any> = this.actions$.ofType(subscriptionInfoActions.LOAD_SUCCESS).pipe(
        map((action: subscriptionInfoActions.LoadSuccessAction) => action.payload),
        combineLatest(this.stateService.catalogPlans$, (subscriptionInfo, plans) => ({ subscriptionInfo, plans })),
        filter(({ plans }) => plans.length > 0),
        map(({ subscriptionInfo, plans }) => {
            const productOptions = [];
            subscriptionInfo.productOptions.forEach(option => {
                productOptions.push(option.key);
            });
            const billingPeriod = subscriptionInfo.plan.billingPeriod;
            const relatedPlan = Object.assign(new Plan(), subscriptionInfo.plan, { billingPeriod });
            const plan = plans.find(byIdentifier(relatedPlan.identifier));
            const billingInfo = subscriptionInfo.billingInfo;
            const paymentMethod = Object.assign(new PaymentMethod(), { type: PaymentMethod.card });
            return Object.assign(new CheckoutRequest(), { plan, billingInfo, paymentMethod, productOptions });
        }),
        map(checkoutRequest => new checkoutActions.InitAction(checkoutRequest))
    );

    @Effect()
    checkoutCompleteReload$: Observable<any> = this.actions$
        .ofType(RouterActions.GO)
        .pipe(
            map((action: RouterActions.Go) => action.payload),
            map(payload => payload.path),
            filter(path => path[0] === 'checkout/thankyou'),
            map(() => new pageActions.ReloadAction()),
            delay(1)
        );

    @Effect()
    creditCardDetails$: Observable<any> = this.actions$
        .ofType(checkoutActions.CHANGE_CREDIT_CARD)
        .pipe(map(() => new checkoutActions.CheckoutAction()));

    @Effect()
    triggerPreview$: Observable<any> = this.actions$
        .ofType(
            checkoutActions.CHANGE_VOUCHER_CODE,
            checkoutActions.CHANGE_PLAN,
            checkoutActions.CHECK_PRODUCT_OPTION,
            checkoutActions.UNCHECK_PRODUCT_OPTION,
            checkoutActions.CHANGE_PAYROLL_ENDING_UNIT_ATTRIBUTE
        )
        .pipe(map(() => new checkoutActions.PreviewAction()));

    @Effect({ dispatch: true })
    checkoutCompleteFail$: Observable<any> = this.actions$.ofType(checkoutActions.CHECKOUT_FAIL).pipe(
        map(
            (action: checkoutActions.CheckoutFailAction) =>
                new MessageActions.ErrorMessage({
                    messageKey: 'billing.checkout_fail',
                    titleKey: 'globals.try_again',
                    options: { data: { action: new checkoutActions.CheckoutAction() } }
                })
        )
    );

    @Effect({ dispatch: true })
    creditCardFail$: Observable<any> = this.actions$
        .ofType(creditCardActions.CREDIT_CARD_ERROR)
        .pipe(map((action: creditCardActions.CreditCardError) => new MessageActions.ErrorMessage({ messageKey: action.payload })));

    constructor(private actions$: Actions, private apiService: ApiService, private stateService: StateService) {}
}
