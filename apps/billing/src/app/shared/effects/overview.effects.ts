import { Observable, of } from 'rxjs';

import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { by } from '@bx-client/common';
import { Actions, Effect } from '@ngrx/effects';

import { addonPayrollKey, productOptionsKeys } from '../../../config';
import * as overviewActions from '../actions/overview.actions';
import { AddonAttributeCollection, CheckoutRequest } from '../models';
import { ApiService, StateService } from '../services';

const byKey = by('key');

@Injectable()
export class OverviewEffects {
    @Effect()
    initActivePlan$: Observable<any> = this.actions$
        .ofType(overviewActions.INIT_OPTIONS)
        .pipe(
            withLatestFrom(this.stateService.subscriptionInfoInstancePlan$, (_, plan) => plan),
            map(plan => new overviewActions.InitPlanAction(plan))
        );

    @Effect({ dispatch: false })
    initProductOptions$: Observable<any> = this.actions$.ofType(overviewActions.INIT_OPTIONS).pipe(
        withLatestFrom(this.stateService.subscriptionInfoInstanceAddons$, (_, addons) => addons),
        tap(addons => {
            const productOptions = addons.filter(addon => productOptionsKeys.indexOf(addon.key) > -1).map(addon => addon.key);
            this.stateService.dispatch(new overviewActions.InitProductOptionsAction(productOptions));
        }),
        tap(addons => {
            const payrollAddon = addons.find(byKey(addonPayrollKey));
            if (payrollAddon) {
                const attributes = [];
                attributes[addonPayrollKey] = payrollAddon.customAttributes;
                this.stateService.dispatch(
                    new overviewActions.InitProductOptionsAttributesAction(new AddonAttributeCollection(attributes))
                );
            }
        })
    );

    @Effect()
    getOverviewPreview$: Observable<any> = this.actions$.ofType(overviewActions.PREVIEW).pipe(
        withLatestFrom(
            this.stateService.overviewPlan$,
            this.stateService.overviewProductOptions$,
            this.stateService.overviewProductOptionsAttributes$,
            (_, plan, productOptions, productOptionsAttributes) => ({
                plan,
                productOptions,
                productOptionsAttributes
            })
        ),
        map(({ plan, productOptions, productOptionsAttributes }) =>
            Object.assign(new CheckoutRequest(), { plan, productOptions, productOptionsAttributes })
        ),
        switchMap(checkoutRequest =>
            this.apiService
                .getCheckoutPreview(checkoutRequest)
                .pipe(
                    map(checkoutSummary => new overviewActions.PreviewSuccessAction(checkoutSummary)),
                    catchError(() => of(new overviewActions.PreviewFailAction()))
                )
        )
    );

    @Effect()
    triggerOverviewPreview$: Observable<any> = this.actions$
        .ofType(
            overviewActions.CHECK_PRODUCT_OPTION,
            overviewActions.UNCHECK_PRODUCT_OPTION,
            overviewActions.CHANGE_PLAN,
            overviewActions.CHANGE_PAYROLL_ENDING_UNIT_ATTRIBUTE
        )
        .pipe(map(() => new overviewActions.PreviewAction()));

    constructor(private actions$: Actions, private stateService: StateService, private apiService: ApiService) {}
}
