import { combineLatest, filter, map, take, tap } from 'rxjs/operators';

import { Inject, Injectable } from '@angular/core';
import { identity } from '@bx-client/common';
import { BxLocalStorageService, localStorageConfigToken } from '@bx-client/local-storage';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';

import * as checkoutActions from '../actions/checkout.action';
import { Product } from '../models';
import { StateService } from '../services';

export const googleTagManagerDataLayer = typeof dataLayer === 'object' ? dataLayer : [];

const transformProduct = (product: Product) => ({
    name: product.key,
    id: product.key,
    price: product.totalPrice.toString(),
    brand: '',
    category: product.category,
    variant: product.billingPeriod,
    quantity: product.quantity.toString()
});

const addUserProduct = (plan, users, products) => {
    if (users.length > 1 && plan.user) {
        const usersCount = users.length - 1;
        products.push(Object.assign(transformProduct(plan.user), { quantity: usersCount.toString() }));
    }
};

@Injectable()
export class GoogleEffects {
    @Effect({ dispatch: false })
    checkoutStep1$: Observable<any> = this.actions$.ofType(checkoutActions.STEP_1_LOADED).pipe(
        combineLatest(
            this.stateService.checkoutInstancePlan$,
            this.stateService.subscriptionInfoInstanceAddons$,
            this.stateService.usersCollection$,
            (_, plan, addons, users) => ({ plan, addons, users })
        ),
        take(1),
        tap(({ plan, addons, users }) => {
            const products = [transformProduct(plan), ...addons.map(addon => transformProduct(addon))];
            addUserProduct(plan, users, products);
            googleTagManagerDataLayer.push({
                event: 'checkout',
                ecommerce: { checkout: { actionField: { step: 1, option: '' }, products } }
            });
        })
    );

    @Effect({ dispatch: false })
    checkoutStep2$: Observable<any> = this.actions$.ofType(checkoutActions.STEP_2_LOADED).pipe(
        combineLatest(
            this.stateService.checkoutInstancePlan$,
            this.stateService.subscriptionInfoInstanceAddons$,
            this.stateService.usersCollection$,
            (_, plan, addons, users) => ({ plan, addons, users })
        ),
        take(1),
        tap(({ plan, addons, users }) => {
            const products = [transformProduct(plan), ...addons.map(addon => transformProduct(addon))];
            addUserProduct(plan, users, products);
            googleTagManagerDataLayer.push({
                event: 'checkout',
                ecommerce: { checkout: { actionField: { step: 2, option: '' }, products } }
            });
        })
    );

    @Effect({ dispatch: false })
    checkoutStep3$: Observable<any> = this.actions$.ofType(checkoutActions.STEP_3_LOADED).pipe(
        take(1),
        tap(() =>
            googleTagManagerDataLayer.push({
                event: 'checkoutOption',
                ecommerce: { checkout_option: { actionField: { step: 'creditcard', option: '' } } }
            })
        )
    );

    @Effect({ dispatch: false })
    checkoutSuccess$: Observable<any> = this.actions$.ofType(checkoutActions.CHECKOUT_SUCCESS).pipe(
        map((action: checkoutActions.CheckoutSuccessAction) => action.payload),
        combineLatest(
            this.stateService.checkoutInstancePlan$,
            this.stateService.checkoutInstanceVoucher$,
            this.stateService.subscriptionInfoInstanceAddons$,
            this.stateService.usersCollection$,
            this.stateService.checkoutPreview$,
            (_, plan, voucher, addons, users, checkoutPreview) => ({ plan, voucher, addons, users, checkoutPreview })
        ),
        take(1),
        tap(({ plan, voucher, addons, users, checkoutPreview }) => {
            const products = [transformProduct(plan), ...addons.map(addon => transformProduct(addon))];
            const uniqueId = +new Date();
            const voucherCode = voucher.code || '';
            const coupon = checkoutPreview.isVoucherValid ? voucherCode : '';
            addUserProduct(plan, users, products);
            this.localStorageService.set(this.localStorageConfig.keys.checkoutSuccess, {
                event: 'transaction',
                ecommerce: {
                    purchase: {
                        actionField: {
                            affiliation: 'bexio-office',
                            coupon,
                            id: uniqueId,
                            revenue: checkoutPreview.total.totalPrice.toString(),
                            shipping: '0',
                            tax: '0'
                        },
                        products
                    }
                }
            });
            this.localStorageService.set(this.localStorageConfig.keys.checkoutAnalyticsData, true);
        })
    );

    @Effect({ dispatch: false })
    thankYouPageLoaded$: Observable<any> = this.actions$.ofType(checkoutActions.THANK_YOU_PAGE_LOADED).pipe(
        map(() => this.localStorageService.get(this.localStorageConfig.keys.checkoutAnalyticsData)),
        filter(refreshFlag => {
            if (refreshFlag) {
                this.localStorageService.remove(this.localStorageConfig.keys.checkoutAnalyticsData);
            }
            return !refreshFlag;
        }),
        map(() => this.localStorageService.get(this.localStorageConfig.keys.checkoutSuccess)),
        filter(identity),
        take(1),
        tap(payload => googleTagManagerDataLayer.push(payload)),
        tap(() => this.localStorageService.remove(this.localStorageConfig.keys.checkoutSuccess))
    );

    constructor(
        private actions$: Actions,
        private stateService: StateService,
        private localStorageService: BxLocalStorageService,
        @Inject(localStorageConfigToken) private localStorageConfig: any
    ) {}
}
