import { map, tap } from 'rxjs/operators';

import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';

import * as RouterActions from '../actions/router.action';

@Injectable()
export class RouterEffects {
    @Effect({ dispatch: false })
    navigate$: Observable<any> = this.actions$
        .ofType(RouterActions.GO)
        .pipe(
            map((action: RouterActions.Go) => action.payload),
            tap(({ path, query: queryParams, extras }) => this.router.navigate(path, { queryParams, ...extras }))
        );

    @Effect({ dispatch: false })
    navigateBack$: Observable<any> = this.actions$.ofType(RouterActions.BACK).pipe(tap(() => this.location.back()));

    @Effect({ dispatch: false })
    navigateForward$: Observable<any> = this.actions$.ofType(RouterActions.FORWARD).pipe(tap(() => this.location.forward()));

    constructor(private actions$: Actions, private router: Router, private location: Location) {}
}
