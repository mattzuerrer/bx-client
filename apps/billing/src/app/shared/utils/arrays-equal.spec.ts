import { arraysEqual } from './arrays-equal';

describe('The arraysEqual method', () => {
    it('should be true when arrays are equal in item count', () => {
        const ary1: string[] = ['item1', 'item2'];
        const ary2: string[] = ['item2', 'item1'];
        expect(arraysEqual(ary1, ary2)).toBe(true);
    });

    it('should be false when arrays differ in item count', () => {
        const ary1: string[] = ['item1'];
        const ary2: string[] = ['item2', 'item1'];
        expect(arraysEqual(ary1, ary2)).toBe(false);
    });

    it('should be true when array items differ in order', () => {
        const ary1: string[] = ['item1', 'item2'];
        const ary2: string[] = ['item2', 'item1'];
        expect(arraysEqual(ary1, ary2)).toBe(true);
    });

    it('should be false when array items differ in naming', () => {
        const ary1: string[] = ['item1', 'item2'];
        const ary2: string[] = ['item1', 'item3'];
        expect(arraysEqual(ary1, ary2)).toBe(false);
    });
});
