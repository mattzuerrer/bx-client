export function arraysEqual(ary1: string[], ary2: string[]): boolean {
    return ary1.length === ary2.length && ary1.every(item => ary2.indexOf(item) > -1);
}
