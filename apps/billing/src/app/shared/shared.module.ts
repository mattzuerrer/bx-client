import { CommonModule, I18nPluralPipe } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule as BxCommonModule } from '@bx-client/common';
import { CoreModule } from '@bx-client/core';
import { PipesModule } from '@bx-client/pipes';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { sharedComponents } from './components';
import { featureName } from './config';
import { sharedContainers } from './containers';
import { CustomMaterialModule } from './custom-material.module';
import { TooltipModule } from './directives/tooltip.module';
import { sharedEffects } from './effects';
import { sharedEntryComponents } from './entry-components';
import { sharedGuards } from './guards';
import { sharedPipes } from './pipes';
import { reducers } from './reducers';
import { sharedServices } from './services';

@NgModule({
    imports: [
        CoreModule,
        CommonModule,
        BxCommonModule,
        CustomMaterialModule,
        StoreModule.forFeature(featureName, reducers),
        EffectsModule.forFeature(sharedEffects),
        PipesModule,
        TooltipModule

    ],
    exports: [
        CoreModule,
        CommonModule,
        BxCommonModule,
        ...sharedPipes,
        ...sharedComponents,
        ...sharedContainers,
        CustomMaterialModule,
        TooltipModule,
        PipesModule
    ],
    providers: [I18nPluralPipe],
    declarations: [...sharedPipes, ...sharedComponents,  ...sharedEntryComponents, ...sharedContainers],
    entryComponents: [...sharedEntryComponents]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [...sharedServices, ...sharedGuards]
        };
    }
}
