import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule, MatIconModule, MatRadioButton, MatRadioModule } from '@angular/material';
import { By } from '@angular/platform-browser';

import * as fixtures from '../../fixtures';
import { PerBillingPeriodDescriptiveMockPipe, PluralizeMockPipe, SafeHtmlMockPipe, TranslateMockPipe } from '../../mocks';
import { LearnMoreMockComponent } from '../../mocks/components';
import { Plan } from '../../models';

import { PlanCardComponent } from './plan-card.component';

describe('Plan card component', () => {
    let component: PlanCardComponent;
    let fixture: ComponentFixture<PlanCardComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                PlanCardComponent,
                LearnMoreMockComponent,
                PerBillingPeriodDescriptiveMockPipe,
                SafeHtmlMockPipe,
                TranslateMockPipe,
                PluralizeMockPipe
            ],
            imports: [MatIconModule, MatCardModule, MatRadioModule]
        });
    });

    it('should create the component', () => {
        fixture = TestBed.createComponent(PlanCardComponent);
        component = fixture.componentInstance;
        expect(component).toBeTruthy();
    });

    it('should show name', () => {
        const plan = Object.assign(new Plan(), fixtures.productCatalog.plans[0]);
        fixture = TestBed.createComponent(PlanCardComponent);
        fixture.componentInstance.plan = plan;
        fixture.detectChanges();
        const name = fixture.debugElement.query(By.css('.name'));
        expect(name.nativeElement.textContent).toBe(plan.name);
    });

    it('should show price and currency', () => {
        const plan = Object.assign(new Plan(), fixtures.productCatalog.plans[0]);
        fixture = TestBed.createComponent(PlanCardComponent);
        fixture.componentInstance.plan = plan;
        fixture.detectChanges();
        const radioButton = fixture.debugElement.query(By.css('.radio-button'));
        expect(radioButton.nativeElement.textContent).toContain(plan.currency);
        expect(radioButton.nativeElement.textContent).toContain(plan.price);
    });

    it('should have right value and be checked if needed', () => {
        const plan = Object.assign(new Plan(), fixtures.productCatalog.plans[0]);
        fixture = TestBed.createComponent(PlanCardComponent);
        fixture.componentInstance.plan = plan;
        fixture.componentInstance.checked = true;
        fixture.detectChanges();
        const radioButton = fixture.debugElement.query(By.directive(MatRadioButton));
        expect(radioButton.componentInstance.checked).toBeTruthy();
        expect(radioButton.componentInstance.value).toBe(plan.identifier);
    });
});
