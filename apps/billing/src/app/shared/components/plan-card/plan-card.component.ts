import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Plan, UserProduct } from '../../models';

@Component({
    selector: 'app-plan-card',
    templateUrl: 'plan-card.component.html',
    styleUrls: ['plan-card.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlanCardComponent {
    @Input() active = false;

    @Input() checked = false;

    @Input() plan: Plan = new Plan();

    @Input() disabled = false;

    get user(): UserProduct {
        return this.plan.user;
    }
}
