import { combineLatest as observableCombineLatest, Observable, Observer, Subject, Subscription } from 'rxjs';

import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, NgZone, OnDestroy, OnInit, Output } from '@angular/core';
import { identity } from '@bx-client/common';
import { TranslateService } from '@ngx-translate/core';
import { filter, map, take } from 'rxjs/operators';

import { getCreditCardErrorMessage } from '../../../../config/creditCardMapping';
import * as zuoraConfig from '../../../../config/zuora';
import { RsaSignature } from '../../../shared/models';

// tslint:disable-next-line:no-var-requires load-script is a CommonJS module and cannot be imported using ES6 syntax
const load = require('load-script');

/**
 * Zuora library global variable.
 */
declare const Z: any;

const cardContainerWidth = 696;

@Component({
    selector: 'app-credit-card',
    templateUrl: './credit-card.component.html',
    styleUrls: ['./credit-card.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreditCardComponent implements OnInit, OnDestroy, AfterViewInit {
    @Output() fetchSignature: EventEmitter<void> = new EventEmitter<void>();

    @Output() creditCardClear: EventEmitter<void> = new EventEmitter<void>();

    @Output() creditCardLoadError: EventEmitter<string> = new EventEmitter<string>();

    @Output() creditCardError: EventEmitter<any> = new EventEmitter<any>();

    @Output() creditCardChange: EventEmitter<string> = new EventEmitter<string>();

    @Output() creditCardFormLoaded: EventEmitter<void> = new EventEmitter<void>();

    @Output() componentLoaded: EventEmitter<void> = new EventEmitter<void>();

    @Input() rsaSignature$: Observable<RsaSignature>;

    @Input() width: number = cardContainerWidth;

    @Input() creditCardFormSubmit$: Subject<void> = new Subject<void>();

    isZuoraIframeLoaded = false;

    submitSubscription: Subscription;

    constructor(private zone: NgZone, private translateService: TranslateService) {}

    ngOnInit(): void {
        this.fetchSignature.emit();
        observableCombineLatest(this.loadZuoraLibrary(), this.rsaSignature$.pipe(filter(identity)), (_, rsaSignature) => rsaSignature)
            .pipe(take(1), map(rsaSignature => Object.assign(new RsaSignature(), rsaSignature)))
            .subscribe(rsaSignature => this.renderCreditCardFrame(rsaSignature, {}));
        this.submitSubscription = this.creditCardFormSubmit$.subscribe(() => Z.submit());
    }

    ngAfterViewInit(): void {
        setTimeout(() => this.componentLoaded.emit(), 0);
    }

    ngOnDestroy(): void {
        this.submitSubscription.unsubscribe();
        this.creditCardClear.emit();
    }

    private loadZuoraLibrary(): Observable<void> {
        return Observable.create((observer: Observer<void>) => {
            const loadComplete = () => {
                observer.next(null);
                observer.complete();
            };
            typeof Z === 'undefined' ? load(zuoraConfig.libraryUrl, loadComplete) : loadComplete();
        });
    }

    private renderCreditCardFrame(params: any, populateFields: any): void {
        Z.renderWithErrorHandler(
            params,
            populateFields,
            response => this.onCreditCardSuccess(response),
            (key, code, message) => this.onCreditCardError(key, code, message)
        );
        Z.runAfterRender(() => {
            this.isZuoraIframeLoaded = true;
            this.creditCardFormLoaded.emit();
        });
    }

    private onCreditCardSuccess(response: any): void {
        this.zone.run(() => {
            if (response.success) {
                const refId = response.refId;
                this.creditCardChange.emit(refId);
            } else {
                this.creditCardLoadError.emit(response.errorMessage);
            }
        });
    }

    private onCreditCardError(key: string, code: string, errorMessage: string): void {
        this.zone.run(() => {
            const translationKey = getCreditCardErrorMessage(errorMessage);
            this.translateService.get(translationKey).subscribe(message => {
                this.creditCardError.emit(message);
                Z.sendErrorMessageToHpm(key, message);
            });
        });
    }
}
