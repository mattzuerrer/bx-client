import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

import { ColumnStatus } from '../../models';

@Component({
    selector: 'app-list-sort',
    templateUrl: 'list-sort.component.html',
    styleUrls: ['list-sort.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListSortComponent {
    columnStatus: ColumnStatus = new ColumnStatus();

    @Input() column = '';

    @Input() title = '';

    @Input()
    set columnConfig(columnConfig: ColumnStatus[]) {
        this.columnStatus = columnConfig.find(item => item.column === this.column) || new ColumnStatus(this.column);
    }

    @Output() sort: EventEmitter<ColumnStatus> = new EventEmitter<ColumnStatus>();

    onSort(): void {
        this.sort.emit(this.columnStatus);
    }
}
