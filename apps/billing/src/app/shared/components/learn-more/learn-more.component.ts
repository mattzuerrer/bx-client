import { ChangeDetectionStrategy, Component, Inject, LOCALE_ID } from '@angular/core';

@Component({
    selector: 'app-learn-more',
    templateUrl: './learn-more.component.html',
    styleUrls: ['./learn-more.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LearnMoreComponent {
    constructor(@Inject(LOCALE_ID) private locale: string) {}

    /**
     * Concat correct url for 'Learn more' link.
     *
     * @returns {string}
     */
    get learnMoreUrl(): string {
        const germanDetailsLink = 'pakete-preise/paket-details.html';
        const frenchDetailsLink = 'packages-et-prix/package-details.html';
        const englishDetailsLink = 'packages-and-prices/package-details.html';
        const italianDetailsLink = 'prezzi-pacchetti/dettagli.html';
        let detaislLink = '';
        let locale = this.locale;

        switch (this.locale) {
            case 'de-AT':
            case 'de-DE':
            case 'de-CH':
                locale = 'de-CH';
                detaislLink = germanDetailsLink;
                break;
            case 'fr-CH':
                detaislLink = frenchDetailsLink;
                break;
            case 'en-GB':
                locale = 'en-CH';
                detaislLink = englishDetailsLink;
                break;
            case 'it-CH':
                detaislLink = italianDetailsLink;
                break;
            default:
                locale = 'de-CH';
                detaislLink = germanDetailsLink;
        }

        return `https://www.bexio.com/${locale}/${detaislLink}`;
    }
}
