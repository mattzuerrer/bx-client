import { DebugElement, LOCALE_ID } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { TranslateMockPipe } from '../../mocks';

import { LearnMoreComponent } from './learn-more.component';

describe('LearnMoreComponent', () => {
    let component: LearnMoreComponent;
    let fixture: ComponentFixture<LearnMoreComponent>;
    let debugElement: DebugElement;
    let element: HTMLElement;

    beforeEach(
        async(() => {
            TestBed.configureTestingModule({ declarations: [LearnMoreComponent, TranslateMockPipe] });
        })
    );

    const provideLocale = (locale = 'de-CH') => {
        fixture = TestBed.overrideComponent(LearnMoreComponent, {
            set: { providers: [{ provide: LOCALE_ID, useValue: locale }] }
        }).createComponent(LearnMoreComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement.query(By.css('a'));
        element = debugElement.nativeElement;
    };

    it('should have learn more translation key', () => {
        provideLocale();
        const translationKey = 'billing.learn_more';
        fixture.detectChanges();
        expect(element.textContent).toContain(translationKey);
    });

    it('should return de-CH url for unknown locale', () => {
        provideLocale('xx-XX');
        fixture.detectChanges();
        expect(component.learnMoreUrl).toContain('https://www.bexio.com/de-CH/pakete-preise/paket-details.html');
    });

    it('should return correct url for de-CH locale', () => {
        provideLocale('de-CH');
        fixture.detectChanges();
        expect(component.learnMoreUrl).toContain('https://www.bexio.com/de-CH/pakete-preise/paket-details.html');
    });

    it('should return correct url for de-AT locale', () => {
        provideLocale('de-AT');
        fixture.detectChanges();
        expect(component.learnMoreUrl).toContain('https://www.bexio.com/de-CH/pakete-preise/paket-details.html');
    });

    it('should return correct url for de-DE locale', () => {
        provideLocale('de-DE');
        fixture.detectChanges();
        expect(component.learnMoreUrl).toContain('https://www.bexio.com/de-CH/pakete-preise/paket-details.html');
    });

    it('should return correct url for en-GB locale', () => {
        provideLocale('en-GB');
        fixture.detectChanges();
        expect(component.learnMoreUrl).toContain('https://www.bexio.com/en-CH/packages-and-prices/package-details.html');
    });

    it('should return correct url for fr-CH locale', () => {
        provideLocale('fr-CH');
        fixture.detectChanges();
        expect(component.learnMoreUrl).toContain('https://www.bexio.com/fr-CH/packages-et-prix/package-details.html');
    });
});
