import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { by } from '@bx-client/common';

import {
    addonAccountantAccountingKey,
    addonAccountantPayrollKey,
    addonPayrollKey,
    planProPlusKey,
    planStarterKey
} from '../../../../config';
import { Addon, AddonAttributeCollection, CompanyProfile, Plan, ScalePrice } from '../../models';

const byIdentifier = by('identifier');

const byKey = by('key');

const checkoutScope = 'checkout';
const overviewScope = 'overview';

@Component({
    selector: 'app-plan-options',
    templateUrl: 'plan-options.component.html',
    styleUrls: ['plan-options.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlanOptionsComponent {
    @Input() activePlan: Plan = new Plan();

    @Input() addons: Addon[] = [];

    @Input() activeAddons: Addon[] = [];

    @Input() activatedProductOptions: string[] = [];

    @Input() companyProfile: CompanyProfile;

    @Input() activatedProductOptionsAttributes: AddonAttributeCollection = new AddonAttributeCollection();

    @Input() scope: string = checkoutScope;

    @Output() checkProductOption: EventEmitter<string> = new EventEmitter<string>();

    @Output() uncheckProductOption: EventEmitter<string> = new EventEmitter<string>();

    @Output() payrollAttributesEndingUnitChange: EventEmitter<number> = new EventEmitter<number>();

    selectedNumberOfPayrollEmployees = 1;

    selectedNumberOfPayrollEmployeesStarterPlan = 5;

    get isCheckoutScope(): boolean {
        return this.scope === checkoutScope;
    }

    get isOverviewScope(): boolean {
        return this.scope === overviewScope;
    }

    get isPlanProPlus(): boolean {
        return this.activePlan.key === planProPlusKey;
    }

    onPayrollChange(checked: boolean): void {
        if (checked) {
            this.checkProductOption.emit(addonPayrollKey);
            // We need to handle endingUnit separately as long we have to manage old plans
            if (this.activePlan.isProductVersionV1 && this.activatedProductOptionsAttributes.get(addonPayrollKey).endingUnit === 0) {
                this.payrollAttributesEndingUnitChange.emit(this.selectedNumberOfPayrollEmployees);
            } else {
                if (this.isStarterPlan) {
                    this.payrollAttributesEndingUnitChange.emit(this.selectedNumberOfPayrollEmployeesStarterPlan);
                }
            }
        } else {
            this.uncheckProductOption.emit(addonPayrollKey);
        }
    }

    onAccountantPayrollChange(checked: boolean): void {
        checked ? this.checkProductOption.emit(addonAccountantPayrollKey) : this.uncheckProductOption.emit(addonAccountantPayrollKey);
    }

    onAccountantAccountingChange(checked: boolean): void {
        checked ? this.checkProductOption.emit(addonAccountantAccountingKey) : this.uncheckProductOption.emit(addonAccountantAccountingKey);
    }

    findAddon(key: string): Addon {
        const addon = Object.assign(new Addon(), { key, billingPeriod: this.billingPeriod });
        return this.addons.find(byIdentifier(addon.identifier));
    }

    get payrollChecked(): boolean {
        return this.planHasPayroll || this.activatedProductOptions.indexOf(addonPayrollKey) > -1;
    }

    get accountantPayrollChecked(): boolean {
        return this.activatedProductOptions.indexOf(addonAccountantPayrollKey) > -1;
    }

    get accountantAccountingChecked(): boolean {
        return this.activatedProductOptions.indexOf(addonAccountantAccountingKey) > -1;
    }

    get planHasPayroll(): boolean {
        return this.activePlan.hasPayroll;
    }

    get payrollAddon(): Addon {
        return this.findAddon(addonPayrollKey);
    }

    get accountantPayrollAddon(): Addon {
        return this.findAddon(addonAccountantPayrollKey);
    }

    get accountantAccountingAddon(): Addon {
        return this.findAddon(addonAccountantAccountingKey);
    }

    get payrollAddonScalePrice(): number {
        if (this.isStarterPlan) {
            return this.payrollAddon.getScalePrice(this.numberOfPayrollEmployees).price;
        }
        return this.payrollAddon.getScalePriceByPlan(this.activePlan).price;
    }

    get activePayrollAddon(): Addon {
        return this.activeAddons.find(byKey(addonPayrollKey));
    }

    get billingPeriod(): string {
        return this.activePlan.billingPeriod;
    }

    get maxNumberOfPayrollEmployees(): number {
        return this.activePlan.maxNumberOfPayrollEmployees;
    }

    get isProductVersionV2(): boolean {
        return this.activePlan.isProductVersionV2;
    }

    get numberOfPayrollEmployees(): number {
        const attributes = this.activatedProductOptionsAttributes.get(addonPayrollKey);
        return attributes && attributes.endingUnit
            ? attributes.endingUnit
            : this.isStarterPlan ? this.selectedNumberOfPayrollEmployeesStarterPlan : ScalePrice.DEFAULT_NB_PAYROLL_USERS;
    }

    get isStarterPlan(): boolean {
        return this.activePlan.key === planStarterKey;
    }

    onPayrollAttributesChange(payrollEmployees: number): void {
        this.isStarterPlan
            ? (this.selectedNumberOfPayrollEmployeesStarterPlan = payrollEmployees)
            : (this.selectedNumberOfPayrollEmployees = payrollEmployees);
        this.payrollAttributesEndingUnitChange.emit(payrollEmployees);
    }

    showTrialDaysLeft(addon: Addon): boolean {
        return addon && addon.isTrial && addon.trialDaysLeft > 0;
    }
}
