import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatCheckboxModule, MatRadioModule } from '@angular/material';

import { addonPayrollKey } from '../../../../config';
import { TranslateMockPipe } from '../../mocks';
import { LearnMoreMockComponent } from '../../mocks/components';
import { PluralizeMockPipe, SafeHtmlMockPipe } from '../../mocks/pipes';
import { AddonAttribute, AddonAttributeCollection, Plan } from '../../models';
import { ProductVersion } from '../../models/ProductVersion';

import { PlanOptionsComponent } from './plan-options.component';

describe('Plan options component', () => {
    let component: PlanOptionsComponent;
    let fixture: ComponentFixture<PlanOptionsComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [PlanOptionsComponent, TranslateMockPipe, LearnMoreMockComponent, PluralizeMockPipe, SafeHtmlMockPipe],
            imports: [MatCheckboxModule, MatRadioModule, FormsModule]
        });
        fixture = TestBed.createComponent(PlanOptionsComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it(
        'should emit checkProductOption and payrollAttributesEndingUnitChange on payroll checkbox change ' +
            'if product version is V1 and endingUnit is 0',
        () => {
            component.activePlan = Object.assign(new Plan(), { productVersion: ProductVersion.V1 });
            const addonAttributes = [];
            addonAttributes[addonPayrollKey] = new AddonAttribute(0);
            component.activatedProductOptionsAttributes = new AddonAttributeCollection(addonAttributes);

            spyOn(component.checkProductOption, 'emit');
            spyOn(component.payrollAttributesEndingUnitChange, 'emit');

            component.onPayrollChange(true);

            expect(component.checkProductOption.emit).toHaveBeenCalled();
            expect(component.checkProductOption.emit).toHaveBeenCalledWith(addonPayrollKey);

            expect(component.payrollAttributesEndingUnitChange.emit).toHaveBeenCalled();
            expect(component.payrollAttributesEndingUnitChange.emit).toHaveBeenCalledWith(1);
        }
    );

    it('should NOT emit payrollAttributesEndingUnitChange on payroll checkbox change if product version is not V1', () => {
        component.activePlan = Object.assign(new Plan(), { productVersion: ProductVersion.V2 });
        const addonAttributes = [];
        addonAttributes[addonPayrollKey] = new AddonAttribute(0);
        component.activatedProductOptionsAttributes = new AddonAttributeCollection(addonAttributes);

        spyOn(component.checkProductOption, 'emit');
        spyOn(component.payrollAttributesEndingUnitChange, 'emit');

        component.onPayrollChange(true);

        expect(component.checkProductOption.emit).toHaveBeenCalled();
        expect(component.checkProductOption.emit).toHaveBeenCalledWith(addonPayrollKey);

        expect(component.payrollAttributesEndingUnitChange.emit).not.toHaveBeenCalled();
    });

    it('should NOT emit payrollAttributesEndingUnitChange on payroll checkbox change if endingUnit is not 0', () => {
        component.activePlan = Object.assign(new Plan(), { productVersion: ProductVersion.V1 });
        const addonAttributes = [];
        addonAttributes[addonPayrollKey] = new AddonAttribute(99);
        component.activatedProductOptionsAttributes = new AddonAttributeCollection(addonAttributes);

        spyOn(component.checkProductOption, 'emit');
        spyOn(component.payrollAttributesEndingUnitChange, 'emit');

        component.onPayrollChange(true);

        expect(component.checkProductOption.emit).toHaveBeenCalled();
        expect(component.checkProductOption.emit).toHaveBeenCalledWith(addonPayrollKey);

        expect(component.payrollAttributesEndingUnitChange.emit).not.toHaveBeenCalled();
    });
});
