import { BillingAddressFormComponent } from './billing-address-form/billing-address-form.component';
import { CreditCardComponent } from './credit-card/credit-card.component';
import { LearnMoreComponent } from './learn-more/learn-more.component';
import { ListSortComponent } from './list-sort/list-sort.component';
import { PlanCardComponent } from './plan-card/plan-card.component';
import { PlanOptionsComponent } from './plan-options/plan-options.component';

export { BillingAddressFormComponent } from './billing-address-form/billing-address-form.component';

export const sharedComponents = [
    CreditCardComponent,
    LearnMoreComponent,
    ListSortComponent,
    BillingAddressFormComponent,
    PlanCardComponent,
    PlanOptionsComponent
];
