import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ariaInvalidFn, ariaLiveFn, byId, makeAriaInvalidFn, makeAriaLiveFn, Validators as CustomValidators } from '@bx-client/common';
import { Subject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { CompanyProfile, Country } from '../../models';

@Component({
    selector: 'app-billing-address-form',
    templateUrl: 'billing-address-form.component.html',
    styleUrls: ['billing-address-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BillingAddressFormComponent implements OnInit, OnDestroy, AfterViewInit {
    form: FormGroup;

    @Input() countries: Country[] = [];

    @Input()
    set companyProfile(companyProfile: CompanyProfile) {
        if (!this.isInitialized) {
            this.isInitialized = true;
            this.form.reset();
            this.form.patchValue(companyProfile);
        }
    }

    @Input() touchedFormValidation = true;

    @Output() changeForm: EventEmitter<any> = new EventEmitter<any>();

    @Output() changeFormValidity: EventEmitter<boolean> = new EventEmitter<boolean>();

    filteredCountries$: Subject<Country[]>;

    isAriaInvalid: ariaInvalidFn;

    ariaLive: ariaLiveFn;

    private valueChangesSubscription: Subscription;

    private isInitialized = false;

    constructor(private formBuilder: FormBuilder) {
        this.initForm();
    }

    ngOnInit(): void {
        this.valueChangesSubscription = this.form.valueChanges
            .pipe(map(changeForm => Object.assign(new CompanyProfile(), changeForm)))
            .subscribe(billingInfo => this.changeForm.emit(billingInfo));

        this.form.statusChanges.subscribe(() => this.changeFormValidity.emit(this.form.valid));

        this.form.get('countryCode').setValidators([Validators.required, CustomValidators.isInArray(this.countries)]);

        this.filteredCountries$ = new Subject<Country[]>();
        this.isAriaInvalid = makeAriaInvalidFn(this.form, { touchedCondition: this.touchedFormValidation });
        this.ariaLive = makeAriaLiveFn(this.form, { touchedCondition: this.touchedFormValidation });
    }

    ngOnDestroy(): void {
        this.valueChangesSubscription.unsubscribe();
    }

    ngAfterViewInit(): void {
        setTimeout(() => this.changeFormValidity.emit(this.form.valid), 0);
    }

    checkCountry(countryName: string): void {
        const matchedCountries = this.filterCountries(countryName);
        this.filteredCountries$.next(matchedCountries);

        const matchedCountry = this.findCountry(countryName);
        if (matchedCountry) {
            this.form.get('countryCode').setValue(matchedCountry.id);
        }
    }

    private initForm(): void {
        this.form = this.formBuilder.group({
            name: ['', Validators.required],
            firstname: ['', Validators.required],
            lastname: ['', Validators.required],
            address: ['', Validators.required],
            mail: ['', [Validators.required, CustomValidators.email]],
            countryCode: [null, Validators.required],
            phoneMobile: [''],
            postcode: ['', Validators.required],
            city: ['', Validators.required],
            status: [''],
            accountantCompanyName: ['']
        });
    }

    private filterCountries(countryName: string): Country[] {
        return this.countries.filter(country => new RegExp(`^${countryName}`, 'gi').test(country.name));
    }

    private findCountry(countryName: string): Country {
        return this.countries.find(country => new RegExp(`^${countryName}$`, 'gi').test(country.name));
    }

    get countryDisplay(): any {
        return (countryCode: string): string => {
            const country = this.countries.find(byId(countryCode));
            return country ? country.name : '';
        };
    }
}
