import { NgModule } from '@angular/core';
import { CoreModule } from '@bx-client/core';
import { apiHttpInterceptorProvider, ApiJwtInterceptorProvider, httpApiProviderToken } from '@bx-client/http';
import { BxLocalStorageModule, localStorageConfigToken } from '@bx-client/local-storage';
import { MessageModule } from '@bx-client/message';

import { LocalStorageConfig, version } from '../config';

import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { SharedModule } from './shared';

@NgModule({
    declarations: [AppComponent],
    imports: [
        routing,
        CoreModule.forRoot(version),
        MessageModule.forRoot(),
        SharedModule.forRoot(),
        BxLocalStorageModule.forRoot()
    ],
    providers: [
        apiHttpInterceptorProvider,
        { provide: httpApiProviderToken, useClass: ApiJwtInterceptorProvider },
        { provide: localStorageConfigToken, useClass: LocalStorageConfig }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
