import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageGuard } from '../shared';

import { breadcrumbs, title } from './config';
import { AddonManagerComponent } from './containers';
import { AddonManagerLoadedGuard } from './guards';

const routes: Routes = [
    {
        path: '',
        component: AddonManagerComponent,
        data: { breadcrumbs, title },
        canActivate: [PageGuard, AddonManagerLoadedGuard]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
