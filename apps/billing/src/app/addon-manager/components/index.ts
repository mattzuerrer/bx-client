import { AddonsListItemComponent } from './addon-list-item/addons-list-item.component';
import { AddonsListComponent } from './addons-list/addons-list.component';

export const components = [AddonsListComponent, AddonsListItemComponent];
