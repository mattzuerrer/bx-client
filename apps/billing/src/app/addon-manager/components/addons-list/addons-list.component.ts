import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { by, byId } from '@bx-client/common';
import { TranslateService } from '@ngx-translate/core';

import { productOptionsKeys } from '../../../../config/';
import { ConfirmDialogComponent } from '../../../shared/entry-components';
import { Addon, Plan } from '../../../shared/models';
import { AddonDialogComponent } from '../../entry-components';

const byBillingPeriod = by('billingPeriod');

@Component({
    selector: 'app-addons-list',
    templateUrl: 'addons-list.component.html',
    styleUrls: ['addons-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddonsListComponent {
    @Input() isTrial = false;

    @Input() addons: Addon[] = [];

    @Input() activePlan: Plan = new Plan();

    @Input() activeAddons: Addon[] = [];

    @Input() addonsLoadingList: string[] = [];

    @Output() addonActivate: EventEmitter<Addon> = new EventEmitter<Addon>();

    @Output() addonDeactivate: EventEmitter<Addon> = new EventEmitter<Addon>();

    private activeAddonState: Addon = null;

    private inactiveAddonState: Addon = null;

    constructor(private matDialog: MatDialog, private translateService: TranslateService, private changeDetectorRef: ChangeDetectorRef) {}

    isActive(addon: Addon): boolean {
        return this.inactiveAddonState !== addon && (this.activeAddons.some(byId(addon.id)) || this.activeAddonState === addon);
    }

    onAddonActivate(addon: Addon): void {
        if (addon.isPaid && !this.isTrial) {
            this.activeAddonState = addon;
            const dialogRef = this.matDialog.open(AddonDialogComponent);
            dialogRef.componentInstance.addon = addon;
            dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    this.addonActivate.emit(addon);
                }
                this.activeAddonState = null;
                this.changeDetectorRef.detectChanges();
            });
        } else {
            this.addonActivate.emit(addon);
        }
    }

    onAddonDeactivate(addon: Addon): void {
        if (addon.isDeprecated) {
            const translationKey = addon.name;
            this.translateService.get(translationKey).subscribe(name => {
                this.inactiveAddonState = addon;
                const dialogRef = this.matDialog.open(ConfirmDialogComponent);
                dialogRef.componentInstance.content = 'billing.addon_manager.confirm_addon_removal';
                dialogRef.componentInstance.data = { name };
                dialogRef.afterClosed().subscribe(result => {
                    if (result) {
                        this.addonDeactivate.emit(addon);
                    }
                    this.inactiveAddonState = null;
                });
            });
        } else {
            this.addonDeactivate.emit(addon);
        }
    }

    get filteredAddons(): Addon[] {
        return this.addons
            .filter(byBillingPeriod(this.activePlan.billingPeriod))
            .filter(
                addon => productOptionsKeys.indexOf(addon.key) === -1 && (!addon.isDeprecated || this.activeAddons.some(byId(addon.id)))
            );
    }

    isLoading(addonKey: string): boolean {
        return this.addonsLoadingList.indexOf(addonKey) > -1;
    }
}
