import { ChangeDetectionStrategy, Component, EventEmitter, Inject, Input, Output } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material';
import { languageToken } from '@bx-client/env';

import { Addon } from '../../../shared/models';

@Component({
    selector: 'app-addons-list-item',
    templateUrl: 'addons-list-item.component.html',
    styleUrls: ['addons-list-item.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddonsListItemComponent {
    @Input() addon: Addon = new Addon();

    @Input() isActive = false;

    @Input() isDisabled = false;

    @Output() addonActivate: EventEmitter<Addon> = new EventEmitter<Addon>();

    @Output() addonDeactivate: EventEmitter<Addon> = new EventEmitter<Addon>();

    constructor(@Inject(languageToken) private language: string) {}

    onChange(addon: Addon, e: MatSlideToggleChange): void {
        e.checked ? this.addonActivate.emit(addon) : this.addonDeactivate.emit(addon);
    }

    get instructionsUrl(): string {
        const url = this.addon.instructionsUrl[this.language];
        return url || '';
    }
}
