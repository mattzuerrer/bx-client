import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { RouterActions } from '@bx-client/router';
import { Observable, of } from 'rxjs';
import { switchMap, take, tap } from 'rxjs/operators';

import { StateService, StatesLoadedGuard } from '../../shared';
import * as productCatalogActions from '../../shared/actions/product-catalog.action';
import * as subscriptionInfoActions from '../../shared/actions/subscription-info.action';
import { GuardConfig } from '../../shared/models';

@Injectable()
export class AddonManagerLoadedGuard extends StatesLoadedGuard implements CanActivate {
    protected config: GuardConfig[] = [
        {
            stream: this.stateService.subscriptionInfoLoaded$,
            required: true,
            action: new subscriptionInfoActions.LoadAction()
        },
        {
            stream: this.stateService.catalogLoaded$,
            required: true,
            action: new productCatalogActions.LoadAction()
        }
    ];

    constructor(protected stateService: StateService) {
        super(stateService);
    }

    canActivate(): Observable<boolean> {
        return super.canActivate().pipe(switchMap(() => this.checkAccountantPlan()));
    }

    private checkAccountantPlan(): Observable<boolean> {
        return this.stateService.subscriptionInfoInstancePlan$.pipe(
            take(1),
            switchMap(
                plan =>
                    plan.isAccountantPlan
                        ? of(true).pipe(tap(() => this.stateService.dispatch(new RouterActions.Go({ path: ['overview'] }))))
                        : of(true)
            )
        );
    }
}
