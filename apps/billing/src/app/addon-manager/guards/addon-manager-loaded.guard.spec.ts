import { inject, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { StateService } from '../../shared';
import * as pageActions from '../../shared/actions/page.action';
import * as productCatalogActions from '../../shared/actions/product-catalog.action';
import * as subscriptionInfoActions from '../../shared/actions/subscription-info.action';
import { StateServiceMockProvider } from '../../shared/mocks';

import { AddonManagerLoadedGuard } from './addon-manager-loaded.guard';

describe('Addon Manager Loaded Guard', () => {
    let guard: AddonManagerLoadedGuard;

    beforeEach(() => TestBed.configureTestingModule({ providers: [StateServiceMockProvider, AddonManagerLoadedGuard] }));

    it(
        'should dispatch actions to load states if states are not loaded',
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();

            stateService.subscriptionInfoLoaded$ = of(false);
            stateService.catalogLoaded$ = of(false);

            guard = new AddonManagerLoadedGuard(stateService);

            guard.canActivate();

            expect(stateService.dispatch).toHaveBeenCalledTimes(3);
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.LoadingAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new subscriptionInfoActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new productCatalogActions.LoadAction());
        })
    );

    it(
        'should not dispatch actions to load states if states are loaded',
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();

            stateService.subscriptionInfoLoaded$ = of(true);
            stateService.catalogLoaded$ = of(true);

            guard = new AddonManagerLoadedGuard(stateService);

            guard.canActivate();

            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.LoadingAction());
        })
    );

    it(
        'should wait for required states to load',
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();

            let canActivate = null;

            stateService.subscriptionInfoLoaded$ = of(true);
            stateService.catalogLoaded$ = of(true);

            guard = new AddonManagerLoadedGuard(stateService);

            guard.canActivate().subscribe(result => (canActivate = result));

            expect(stateService.dispatch).toHaveBeenCalledTimes(2);
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.LoadingAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.IdleAction());
            expect(canActivate).toBeTruthy();
        })
    );
});
