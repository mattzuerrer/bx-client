import { AddonManagerLoadedGuard } from './addon-manager-loaded.guard';

export { AddonManagerLoadedGuard } from './addon-manager-loaded.guard';

export const guards = [AddonManagerLoadedGuard];
