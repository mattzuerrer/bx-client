import { AddonManagerComponent } from './addon-manager/addon-manager.component';

export { AddonManagerComponent } from './addon-manager/addon-manager.component';

export const containers = [AddonManagerComponent];
