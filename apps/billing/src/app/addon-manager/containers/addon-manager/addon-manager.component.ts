import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { sortBy } from '@bx-client/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { StateService } from '../../../shared';
import * as addonActions from '../../../shared/actions/addon.action';
import { Addon, Plan } from '../../../shared/models';

const sortByName = sortBy('name');

@Component({
    selector: 'app-addon-manager',
    styleUrls: ['addon-manager.component.scss'],
    templateUrl: 'addon-manager.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddonManagerComponent implements OnInit {
    activePlan$: Observable<Plan>;

    addons$: Observable<Addon[]>;

    activeAddons$: Observable<Addon[]>;

    isTrial$: Observable<boolean>;

    addonsLoadingList$: Observable<string[]>;

    constructor(private stateService: StateService) {}

    ngOnInit(): void {
        this.activePlan$ = this.stateService.subscriptionInfoInstancePlan$;

        this.addons$ = this.stateService.catalogAddons$.pipe(map(addons => [...addons]), map(sortByName));

        this.activeAddons$ = this.stateService.subscriptionInfoInstanceAddons$;

        this.isTrial$ = this.stateService.subscriptionInfoInstanceIsTrial$;

        this.addonsLoadingList$ = this.stateService.addonsLoadingList$;
    }

    onActivateAddon(addon: Addon): void {
        this.stateService.dispatch(new addonActions.ActivateAction(addon));
    }

    onDeactivateAddon(addon: Addon): void {
        this.stateService.dispatch(new addonActions.DeactivateAction(addon));
    }
}
