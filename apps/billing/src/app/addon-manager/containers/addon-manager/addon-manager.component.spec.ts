import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material';
import { getChildDebugElement, getChildInstance, sortBy } from '@bx-client/common';
import { of } from 'rxjs';

import { StateService } from '../../../shared';
import * as addonActions from '../../../shared/actions/addon.action';
import * as fixtures from '../../../shared/fixtures';
import { StateServiceMockProvider, TranslateMockPipe } from '../../../shared/mocks';
import { AppAddonsListMockComponent, AppBreadcrumbsMockComponent } from '../../../shared/mocks/components';
import { Addon } from '../../../shared/models';

import { AddonManagerComponent } from './addon-manager.component';

describe('Addon Manager Container', () => {
    let component: AddonManagerComponent;
    let fixture: ComponentFixture<AddonManagerComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [AddonManagerComponent, AppBreadcrumbsMockComponent, AppAddonsListMockComponent, TranslateMockPipe],
            imports: [MatCardModule],
            providers: [StateServiceMockProvider]
        });
    });

    it('should create the component', () => {
        fixture = TestBed.createComponent(AddonManagerComponent);
        component = fixture.componentInstance;
        expect(component).toBeTruthy();
    });

    it(
        'should pass resolved states to addon list',
        inject([StateService], stateService => {
            const productCatalogAddons = fixtures.productCatalog.addons.map(addon => Object.assign(new Addon(), addon));
            stateService.subscriptionInfoInstancePlan$ = of(fixtures.subscriptionInfo);
            stateService.catalogAddons$ = of(productCatalogAddons);
            stateService.subscriptionInfoInstanceAddons$ = of(fixtures.subscriptionInfo.addons);
            stateService.subscriptionInfoInstanceIsTrial$ = of(fixtures.subscriptionInfo.isTrial);
            fixture = TestBed.createComponent(AddonManagerComponent);
            fixture.detectChanges();
            const addonList = getChildInstance(fixture, AppAddonsListMockComponent);
            expect(addonList.activePlan).toBe(fixtures.subscriptionInfo);
            expect(addonList.addons).toEqual(sortBy('name')(productCatalogAddons));
            expect(addonList.activeAddons).toBe(fixtures.subscriptionInfo.addons);
            expect(addonList.isTrial).toBe(fixtures.subscriptionInfo.isTrial);
        })
    );

    it(
        'should dispatch addon activate action',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(AddonManagerComponent);
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            const addon = new Addon();
            getChildDebugElement(fixture, AppAddonsListMockComponent).triggerEventHandler('addonActivate', addon);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new addonActions.ActivateAction(addon));
        })
    );

    it(
        'should dispatch addon deactivate action',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(AddonManagerComponent);
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            const addon = new Addon();
            getChildDebugElement(fixture, AppAddonsListMockComponent).triggerEventHandler('addonDeactivate', addon);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new addonActions.DeactivateAction(addon));
        })
    );
});
