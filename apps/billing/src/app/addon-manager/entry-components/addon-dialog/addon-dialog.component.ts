import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import { Addon } from '../../../shared/models';

@Component({
    selector: 'app-addon-dialog',
    templateUrl: 'addon-dialog.component.html',
    styleUrls: ['addon-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddonDialogComponent {
    @Input() addon: Addon = new Addon();

    constructor(private dialogRef: MatDialogRef<AddonDialogComponent>) {}

    closeModal(): void {
        this.dialogRef.close(true);
    }
}
