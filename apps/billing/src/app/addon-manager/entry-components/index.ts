import { AddonDialogComponent } from './addon-dialog/addon-dialog.component';

export { AddonDialogComponent } from './addon-dialog/addon-dialog.component';

export const entryComponents = [AddonDialogComponent];
