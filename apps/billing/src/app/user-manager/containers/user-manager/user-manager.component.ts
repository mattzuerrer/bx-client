import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, OnInit, Renderer2 } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs';

import { StateService } from '../../../shared';
import * as userActions from '../../../shared/actions/user.action';
import { ColumnStatus, CompanyProfile, Plan, User } from '../../../shared/models';
import { AddUserFormComponent } from '../../entry-components';

@Component({
    selector: 'app-user-manager',
    styleUrls: ['user-manager.component.scss'],
    templateUrl: 'user-manager.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserManagerComponent implements OnInit, AfterViewInit {
    users$: Observable<User[]>;

    currentUser$: Observable<User>;

    columnConfig$: Observable<ColumnStatus[]>;

    companyProfile$: Observable<CompanyProfile>;

    plan$: Observable<Plan>;

    constructor(
        private stateService: StateService,
        private matDialog: MatDialog,
        private renderer: Renderer2,
        private elementRef: ElementRef
    ) {}

    ngOnInit(): void {
        this.users$ = this.stateService.usersCollection$;
        this.currentUser$ = this.stateService.currentUserInstance$;
        this.columnConfig$ = this.stateService.usersColumnConfig$;
        this.companyProfile$ = this.stateService.companyProfile$;
        this.plan$ = this.stateService.subscriptionInfoInstancePlan$;
    }

    ngAfterViewInit(): void {
        // FIX: EDGE renderer not showing user manager page
        setTimeout(() => this.renderer.setStyle(this.elementRef.nativeElement, 'display', 'block'), 0);
    }

    onSort(columnStatus: ColumnStatus): void {
        this.stateService.dispatch(new userActions.SortAction([columnStatus]));
    }

    onAddUser(): void {
        this.matDialog.open(AddUserFormComponent, { width: '360px' });
    }

    onRemove(user: User): void {
        this.stateService.dispatch(new userActions.RemoveAction(user.id));
    }
}
