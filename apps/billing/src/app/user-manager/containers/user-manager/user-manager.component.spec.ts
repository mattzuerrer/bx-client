import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { MatCardModule, MatDialog, MatDialogModule } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { getChildDebugElement, getChildInstance } from '@bx-client/common';
import { of } from 'rxjs';

import { StateService } from '../../../shared';
import * as userActions from '../../../shared/actions/user.action';
import * as fixtures from '../../../shared/fixtures';
import { DialogReference, StateServiceMockProvider, TranslateMockPipe } from '../../../shared/mocks';
import { AppBreadcrumbsMockComponent, AppUserControlsMockComponent, AppUserListMockComponent } from '../../../shared/mocks/components';
import { ColumnStatus, User } from '../../../shared/models';

import { UserManagerComponent } from './user-manager.component';

describe('User Manager Container', () => {
    let component: UserManagerComponent;
    let fixture: ComponentFixture<UserManagerComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                UserManagerComponent,
                AppBreadcrumbsMockComponent,
                AppUserListMockComponent,
                AppUserControlsMockComponent,
                TranslateMockPipe
            ],
            imports: [NoopAnimationsModule, MatCardModule, MatDialogModule],
            providers: [StateServiceMockProvider]
        });
    });

    it('should create the component', () => {
        fixture = TestBed.createComponent(UserManagerComponent);
        component = fixture.componentInstance;
        expect(component).toBeTruthy();
    });

    it(
        'should pass resolved states to user list',
        inject([StateService], stateService => {
            stateService.usersCollection$ = of(fixtures.users);
            stateService.currentUserInstance$ = of(fixtures.user);
            stateService.usersColumnConfig$ = of(fixtures.columnConfig);
            stateService.companyProfile$ = of(fixtures.companyProfile);

            fixture = TestBed.createComponent(UserManagerComponent);
            fixture.detectChanges();
            const userList = getChildInstance(fixture, AppUserListMockComponent);
            expect(userList.users).toBe(fixtures.users);
            expect(userList.currentUser).toBe(fixtures.user);
            expect(userList.columnConfig).toBe(fixtures.columnConfig);
            expect(userList.companyProfile).toBe(fixtures.companyProfile);
        })
    );

    it(
        'should pass resolved states to user controls',
        inject([StateService], stateService => {
            stateService.usersCollection$ = of(fixtures.users);
            stateService.subscriptionInfoInstancePlan$ = of(fixtures.subscriptionInfo.plan);

            fixture = TestBed.createComponent(UserManagerComponent);
            fixture.detectChanges();
            const userList = getChildInstance(fixture, AppUserControlsMockComponent);
            expect(userList.users).toBe(fixtures.users);
            expect(userList.plan).toBe(fixtures.subscriptionInfo.plan);
        })
    );

    it(
        'should dispatch sort action',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(UserManagerComponent);
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            const columnStatus = new ColumnStatus('column');
            getChildDebugElement(fixture, AppUserListMockComponent).triggerEventHandler('sort', columnStatus);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new userActions.SortAction([columnStatus]));
        })
    );

    it(
        'should dispatch remove action',
        inject([StateService], stateService => {
            fixture = TestBed.createComponent(UserManagerComponent);
            fixture.detectChanges();
            spyOn(stateService, 'dispatch');
            const user = Object.assign(new User(), { id: 1 });
            getChildDebugElement(fixture, AppUserListMockComponent).triggerEventHandler('remove', user);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new userActions.RemoveAction(1));
        })
    );

    it(
        'should open add user dialog on add user',
        inject([MatDialog], mdDialog => {
            fixture = TestBed.createComponent(UserManagerComponent);
            fixture.detectChanges();
            const dialogRef = new DialogReference();
            spyOn(mdDialog, 'open').and.returnValue(dialogRef);
            getChildDebugElement(fixture, AppUserControlsMockComponent).triggerEventHandler('addUser', null);
            expect(mdDialog.open).toHaveBeenCalled();
        })
    );
});
