import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { Validators as CustomValidators } from '@bx-client/common';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import * as userActions from '../../../shared/actions/user.action';
import { Product } from '../../../shared/models';
import { StateService } from '../../../shared/services';

@Component({
    selector: 'app-add-user-form',
    templateUrl: 'add-user-form.component.html',
    styleUrls: ['add-user-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddUserFormComponent implements OnInit, OnDestroy {
    form: FormGroup;

    userProduct: Product = new Product();

    isTrial = true;

    invitingUser = false;

    private subscriptions: Subscription[] = [];

    constructor(
        private dialogRef: MatDialogRef<AddUserFormComponent>,
        private formBuilder: FormBuilder,
        private stateService: StateService
    ) {}

    ngOnInit(): void {
        this.form = this.formBuilder.group({ email: ['', [Validators.required, CustomValidators.email]] });
        this.subscriptions.push(
            this.stateService.subscriptionInfoInstancePlanUser$.subscribe(userProduct => (this.userProduct = userProduct))
        );
        this.subscriptions.push(this.stateService.subscriptionInfoInstanceIsTrial$.subscribe(isTrial => (this.isTrial = isTrial)));
        this.subscriptions.push(
            this.stateService.usersInvitingUser$
                .pipe(filter(invitingUser => this.invitingUser !== invitingUser))
                .subscribe(() => this.dialogRef.close())
        );
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    inviteUser(): void {
        this.invitingUser = true;
        const email = this.form.get('email').value;
        this.stateService.dispatch(new userActions.InviteAction(email));
    }

    get invitingUserStatus(): string {
        return this.invitingUser ? 'progress' : '';
    }
}
