import { AddUserFormComponent } from './add-user-form/add-user-form.component';

export { AddUserFormComponent } from './add-user-form/add-user-form.component';

export const entryComponents = [AddUserFormComponent];
