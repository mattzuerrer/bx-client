import { inject, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { StateService } from '../../shared';
import * as companyActions from '../../shared/actions/company.action';
import * as pageActions from '../../shared/actions/page.action';
import * as subscriptionInfoActions from '../../shared/actions/subscription-info.action';
import * as userActions from '../../shared/actions/user.action';
import { StateServiceMockProvider } from '../../shared/mocks';

import { UserManagerLoadedGuard } from './user-manager-loaded.guard';

describe('User Manager Loaded Guard', () => {
    let guard: UserManagerLoadedGuard;

    beforeEach(() => TestBed.configureTestingModule({ providers: [StateServiceMockProvider, UserManagerLoadedGuard] }));

    it(
        'should dispatch actions to load states if states are not loaded',
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();

            stateService.usersLoaded$ = of(false);
            stateService.currentUserLoaded$ = of(false);
            stateService.companyLoaded$ = of(false);
            stateService.subscriptionInfoLoaded$ = of(false);

            guard = new UserManagerLoadedGuard(stateService);

            guard.canActivate();

            expect(stateService.dispatch).toHaveBeenCalledTimes(5);
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.LoadingAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new userActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new userActions.FetchCurrentAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new companyActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new subscriptionInfoActions.LoadAction());
        })
    );

    it(
        'should not dispatch actions to load states if states are loaded',
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();

            stateService.usersLoaded$ = of(true);
            stateService.currentUserLoaded$ = of(true);
            stateService.companyLoaded$ = of(true);
            stateService.subscriptionInfoLoaded$ = of(true);

            guard = new UserManagerLoadedGuard(stateService);

            guard.canActivate();

            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.LoadingAction());
        })
    );

    it(
        'should wait for required states to load',
        inject([StateService], stateService => {
            spyOn(stateService, 'dispatch').and.callThrough();

            let canActivate = null;

            stateService.usersLoaded$ = of(true);
            stateService.currentUserLoaded$ = of(true);
            stateService.subscriptionInfoLoaded$ = of(true);
            stateService.companyLoaded$ = of(false);

            guard = new UserManagerLoadedGuard(stateService);

            guard.canActivate().subscribe(result => (canActivate = result));

            expect(stateService.dispatch).toHaveBeenCalledTimes(3);
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.LoadingAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new companyActions.LoadAction());
            expect(stateService.dispatch).toHaveBeenCalledWith(new pageActions.IdleAction());
            expect(canActivate).toBeTruthy();
        })
    );
});
