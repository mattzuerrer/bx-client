import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { StateService, StatesLoadedGuard } from '../../shared';
import * as companyActions from '../../shared/actions/company.action';
import * as subscriptionInfoActions from '../../shared/actions/subscription-info.action';
import * as userActions from '../../shared/actions/user.action';
import { GuardConfig } from '../../shared/models';

@Injectable()
export class UserManagerLoadedGuard extends StatesLoadedGuard implements CanActivate {
    protected config: GuardConfig[] = [
        {
            stream: this.stateService.usersLoaded$,
            required: true,
            action: new userActions.LoadAction()
        },
        {
            stream: this.stateService.currentUserLoaded$,
            required: true,
            action: new userActions.FetchCurrentAction()
        },
        {
            stream: this.stateService.subscriptionInfoLoaded$,
            required: true,
            action: new subscriptionInfoActions.LoadAction()
        },
        {
            stream: this.stateService.companyLoaded$,
            required: false,
            action: new companyActions.LoadAction()
        }
    ];

    constructor(protected stateService: StateService) {
        super(stateService);
    }
}
