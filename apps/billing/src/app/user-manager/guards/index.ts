import { UserManagerLoadedGuard } from './user-manager-loaded.guard';

export { UserManagerLoadedGuard } from './user-manager-loaded.guard';

export const guards = [UserManagerLoadedGuard];
