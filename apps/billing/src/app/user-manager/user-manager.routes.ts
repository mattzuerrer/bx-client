import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageGuard } from '../shared';

import { breadcrumbs, title } from './config';
import { UserManagerComponent } from './containers';
import { UserManagerLoadedGuard } from './guards';

const routes: Routes = [
    {
        path: '',
        component: UserManagerComponent,
        data: { breadcrumbs, title },
        canActivate: [PageGuard, UserManagerLoadedGuard]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
