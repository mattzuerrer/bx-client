import { NgModule } from '@angular/core';

import { SharedModule } from '../shared';

import { components } from './components';
import { containers } from './containers';
import { entryComponents } from './entry-components';
import { guards } from './guards';
import { routing } from './user-manager.routes';

@NgModule({
    imports: [routing, SharedModule],
    declarations: [...components, ...entryComponents, ...containers],
    providers: [...guards],
    entryComponents: [...entryComponents]
})
export class UserManagerModule {}
