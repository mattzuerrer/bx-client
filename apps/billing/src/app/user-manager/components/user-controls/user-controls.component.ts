import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { CompanyProfile, Plan, User } from '../../../shared/models';
import { TooltipPlacement } from '../../../shared/models/Tooltip';

@Component({
    selector: 'app-user-controls',
    templateUrl: 'user-controls.component.html',
    styleUrls: ['user-controls.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserControlsComponent {
    @Input() users: User[] = [];

    @Input() plan: Plan = new Plan();

    @Input() companyProfile: CompanyProfile = new CompanyProfile();

    @Output() addUser: EventEmitter<void> = new EventEmitter<void>();
    tooltipPlacement = TooltipPlacement;

    constructor(private translateService: TranslateService) {}

    onClick(): void {
        this.addUser.emit();
    }

    get upgradeUserMailToLink(): string {
        const emailAddress = this.translateService.instant('billing.user_manager.pro_plus.upgrade_email_address');
        const emailSubject = encodeURIComponent(
            this.translateService.instant('billing.user_manager.pro_plus.upgrade_max_user_mail_subject', {
                company: this.companyProfile.name
            })
        );

        return 'mailto:' + emailAddress + '?subject=' + emailSubject;
    }

    get maxNumberOfUsersReached(): boolean {
        return this.plan.isProductVersionV2 && this.users.length >= this.maxNumberOfUsers;
    }

    get maxNumberOfUsers(): number {
        return this.plan.maxNumberOfUsers;
    }
}
