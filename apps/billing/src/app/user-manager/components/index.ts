import { UserControlsComponent } from './user-controls/user-controls.component';
import { UserListComponent } from './user-list/user-list.component';

export const components = [UserControlsComponent, UserListComponent];
