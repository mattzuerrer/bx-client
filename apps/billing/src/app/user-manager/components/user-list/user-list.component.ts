import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material';

import { ConfirmDialogComponent } from '../../../shared/entry-components';
import { ColumnStatus, CompanyProfile, User } from '../../../shared/models';

@Component({
    selector: 'app-user-list',
    templateUrl: 'user-list.component.html',
    styleUrls: ['user-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserListComponent {
    @Input() users: User[] = [];

    @Input() currentUser: User = new User();

    @Input() columnConfig: ColumnStatus[] = [];

    @Input() companyProfile: CompanyProfile = new CompanyProfile();

    @Output() sort: EventEmitter<ColumnStatus> = new EventEmitter<ColumnStatus>();

    @Output() remove: EventEmitter<User> = new EventEmitter<User>();

    constructor(private matDialog: MatDialog) {}

    openRemoveUserConfirmationModal(user: User): void {
        const companyName = this.companyProfile.name;
        const dialogRef = this.matDialog.open(ConfirmDialogComponent);
        dialogRef.componentInstance.content = 'billing.user_management.remove_user_confirmation';
        dialogRef.componentInstance.data = Object.assign(new User(), user, { companyName });
        dialogRef.componentInstance.loadingStreamName = 'usersRemovingUser$';
        dialogRef.componentInstance.result.subscribe(() => this.remove.emit(user));
        dialogRef.componentInstance.confirmButtonText = 'billing.user_manager.disconnect';
    }
}
