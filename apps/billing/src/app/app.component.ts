import { AfterViewChecked, AfterViewInit, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { negation } from '@bx-client/common';
import { MessageService } from '@bx-client/message';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { DomService, PingService, StateService } from './shared';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit, AfterViewInit, AfterViewChecked {
    loading$: Observable<boolean>;

    private isViewRendered = false;

    constructor(
        private messageService: MessageService,
        private stateService: StateService,
        private domService: DomService,
        private pingService: PingService
    ) {}

    ngOnInit(): void {
        this.messageService.action$.subscribe(action => this.stateService.dispatch(action));
        this.loading$ = this.stateService.pageLoaded$.pipe(map(negation));
        this.domService.dispatchInitEvent();
        this.pingService.ping().subscribe();
    }

    ngAfterViewInit(): void {
        this.domService.dispatchAfterViewInitEvent();
    }

    ngAfterViewChecked(): void {
        if (!this.isViewRendered) {
            this.isViewRendered = true;
            setTimeout(() => this.domService.dispatchAfterViewCheckedEvent(), 0);
        }
    }
}
