export const version = '_BEXIO_FE_DEPLOYMENT_REPLACED_VERSION_';

export * from './addonsMapping';
export * from './plansMapping';
export * from './translate';
export * from './zuora';
export * from './ping';
export * from './local-storage-config';
export * from './countries';
export * from './translate.initializer';
