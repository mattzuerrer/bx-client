// tslint:disable:max-line-length

export const addonsMapping = [
    {
        key: 'addon_box_net',
        logoUrl: 'assets/images/addons/box.png',
        instructionsUrl: {
            de: 'https://support.bexio.com/hc/de/articles/206488628-Wie-binde-ich-Google-Drive-Dorpbox-und-Box-ans-easySYS-an-'
        }
    },
    {
        key: 'addon_dropbox',
        logoUrl: 'assets/images/addons/dropbox.png',
        instructionsUrl: {
            de: 'https://support.bexio.com/hc/de/articles/206488628-Wie-binde-ich-Google-Drive-Dorpbox-und-Box-ans-easySYS-an-'
        }
    },
    {
        key: 'addon_file_sharer',
        logoUrl: 'assets/images/addons/file_sharer.png',
        instructionsUrl: {}
    },
    {
        key: 'addon_google_authenticator',
        logoUrl: 'assets/images/addons/google_authenticator.png',
        instructionsUrl: {}
    },
    {
        key: 'addon_google_drive',
        logoUrl: 'assets/images/addons/google_drive.png',
        instructionsUrl: {
            de: 'https://support.bexio.com/hc/de/articles/206488628-Wie-binde-ich-Google-Drive-Dorpbox-und-Box-ans-easySYS-an-'
        }
    },
    {
        key: 'addon_kickshops',
        logoUrl: 'assets/images/addons/kickshops.png',
        instructionsUrl: {
            de: 'https://support.bexio.com/hc/de/articles/206488678-Was-bietet-mir-das-Add-on-Kickshops-',
            en: 'https://support.bexio.com/hc/en-us/articles/206488678-What-does-the-Kickshops-add-on-offer-me-',
            fr: 'https://support.bexio.com/hc/fr/articles/206488678-Que-propose-l-Add-on-Kickshops-'
        }
    },
    {
        key: 'addon_lead_management',
        logoUrl: 'assets/images/addons/lead_marketing.png',
        instructionsUrl: {}
    },
    {
        key: 'addon_mailchimp',
        logoUrl: 'assets/images/addons/mailchimp.png',
        instructionsUrl: {
            de: 'https://support.bexio.com/hc/de/articles/206488658-Wir-richte-ich-das-MailChimp-Add-on-ein-',
            en: 'https://support.bexio.com/hc/en-us/articles/206488658-How-do-I-set-up-the-MailChimp-add-on-',
            fr: 'https://support.bexio.com/hc/fr/articles/206488658-Comment-puis-je-installer-l-add-on-MailChimp-'
        }
    },
    {
        key: 'addon_mailxpert',
        logoUrl: 'assets/images/addons/mailxpert.png',
        instructionsUrl: {
            de: 'https://support.bexio.com/hc/de/articles/206488638-Was-muss-ich-%C3%BCber-mailXpert-wissen-',
            en: 'https://support.bexio.com/hc/en-us/articles/206488638-What-do-I-need-to-know-about-mailXpert-',
            fr: 'https://support.bexio.com/hc/fr/articles/206488638-Que-dois-je-savoir-sur-mailXpert-'
        }
    },
    {
        key: 'addon_pingen',
        logoUrl: 'assets/images/addons/pingen.png',
        instructionsUrl: {
            de: 'https://support.bexio.com/hc/de/articles/206488738-Wie-richte-ich-Pingen-Add-on-ein-',
            en: 'https://support.bexio.com/hc/en-us/articles/206488738-How-do-I-install-the-add-on-Pingen-',
            fr: 'https://support.bexio.com/hc/fr/articles/206488738-Comment-installer-l-Add-on-Pingen-'
        }
    },
    {
        key: 'addon_phpeppershop_pos',
        logoUrl: 'assets/images/addons/phpeppershop_pos.png',
        instructionsUrl: {
            de: 'https://support.bexio.com/hc/de/articles/206488778-Was-sind-das-f%C3%BCr-Webshop-Kassensystem-Add-ons-BETA-',
            en: 'https://support.bexio.com/hc/en-us/articles/206488778-What-are-the-PepperShop-Webshop-POS-System-BETA-add-ons-',
            fr:
                'https://support.bexio.com/hc/fr/articles/206488778-Quels-add-on-utiliser-pour-PepperShop-Webshop-et-syst%C3%A8me-de-caisse-BETA-'
        }
    },
    {
        key: 'addon_phpeppershop_webshop',
        logoUrl: 'assets/images/addons/phpeppershop_webshop.png',
        instructionsUrl: {
            de: 'https://support.bexio.com/hc/de/articles/206488778-Was-sind-das-f%C3%BCr-Webshop-Kassensystem-Add-ons-BETA-',
            en: 'https://support.bexio.com/hc/en-us/articles/206488778-What-are-the-PepperShop-Webshop-POS-System-BETA-add-ons-',
            fr:
                'https://support.bexio.com/hc/fr/articles/206488778-Quels-add-on-utiliser-pour-PepperShop-Webshop-et-syst%C3%A8me-de-caisse-BETA-'
        }
    },
    {
        key: 'addon_text_editor',
        logoUrl: 'assets/images/addons/text_editor.png',
        instructionsUrl: {}
    },
    {
        key: 'addon_file_manager',
        logoUrl: 'assets/images/addons/file_manager.png',
        instructionsUrl: {}
    },
    {
        key: 'addon_advanon',
        logoUrl: 'assets/images/addons/advanon.png',
        instructionsUrl: {
            de: 'https://support.bexio.com/hc/de/articles/115003613808-Was-ist-Advanon-und-wie-kann-ich-Advanon-nutzen-'
        }
    },
    {
        key: 'addon_synceria',
        logoUrl: 'assets/images/addons/synceria.png',
        instructionsUrl: { de: 'https://www.synceria.com' }
    },
    { key: 'addon_banking', instructionsUrl: {}, isProductOption: true },
    { key: 'addon_payroll', instructionsUrl: {}, isProductOption: true },
    {
        key: 'addon_lean_sync',
        logoUrl: 'assets/images/addons/swisscom.png',
        instructionsUrl: { de: 'https://support.bexio.com/hc/de/articles/360000972752' }
    }
];

export const addonBankingKey = 'addon_banking';

export const addonPayrollKey = 'addon_payroll';

export const addonAccountantPayrollKey = 'addon_accountant_payroll';

export const addonAccountantAccountingKey = 'addon_accountant_accounting';

export const productOptionsKeys = [addonBankingKey, addonPayrollKey, addonAccountantPayrollKey, addonAccountantAccountingKey];
