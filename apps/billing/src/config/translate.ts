import { HttpClient } from '@angular/common/http';
import { TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function translateLoaderFactory(http: HttpClient): TranslateHttpLoader {
    const date = +new Date();
    return new TranslateHttpLoader(http, 'assets/i18n/', `.json?${date}`);
}

export const translateConfig = {
    loader: { provide: TranslateLoader, useFactory: translateLoaderFactory, deps: [HttpClient] }
};
