import { Injectable } from '@angular/core';
import { LocalStorageConfigInterface } from 'libs/local-storage/index';

@Injectable()
export class LocalStorageConfig implements LocalStorageConfigInterface {
    appPrefix = 'billing';
    keys = {
        checkoutSuccess: 'checkout-success',
        checkoutAnalyticsData: 'checkout-analytics-data'
    };
}
