import { APP_INITIALIZER } from '@angular/core';
import { languageToken } from '@bx-client/env';
import { TranslateService } from '@ngx-translate/core';

export function initTranslateService(translateService: TranslateService, language: string): () => void {
    const defaultLocale = 'de';

    return () => {
        translateService.setDefaultLang(defaultLocale);
        translateService.use(language);
    };
}

export const translateInitializerProvider = {
    provide: APP_INITIALIZER,
    useFactory: initTranslateService,
    deps: [TranslateService, languageToken],
    multi: true
};
