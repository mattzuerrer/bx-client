import { addonBankingKey, addonPayrollKey } from './addonsMapping';

export const planStandardKey = 'plan_standard';
export const planServiceKey = 'plan_service';
export const planTradeKey = 'plan_trade';
export const planAllInOneKey = 'plan_all_in_one';
export const planStarterKey = 'plan_starter';
export const planProKey = 'plan_pro';
export const planProPlusKey = 'plan_pro_plus';
export const planCustomKey = 'plan_custom';
export const planAccountantKey = 'plan_accountant';
export const planMiniKey = 'plan_mini';

export const plansOrder = [
    planStandardKey,
    planServiceKey,
    planTradeKey,
    planAllInOneKey,
    planStarterKey,
    planProKey,
    planProPlusKey,
    planAccountantKey,
    planMiniKey
];

export const plansMapping = {
    [planProKey]: { includedProductOptions: [addonBankingKey] },
    [planProPlusKey]: { includedProductOptions: [addonBankingKey, addonPayrollKey] },
    [planCustomKey]: { includedProductOptions: [addonBankingKey, addonPayrollKey] }
};
