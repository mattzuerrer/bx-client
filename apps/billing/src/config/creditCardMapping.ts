const errors = {
    generic_decline: 'billing.error.credit_card.generic_decline',
    card_declined: 'billing.error.credit_card.card_declined',
    fraudulent: 'billing.error.credit_card.fraudulent',
    incorrect_cvc: 'billing.error.credit_card.incorrect_cvc',
    expired_card: 'billing.error.credit_card.expired_card',
    processing_error: 'billing.error.credit_card.processing_error',
    'future date': 'billing.error.credit_card.expiration_date_passed'
};

export function getCreditCardErrorMessage(message: string): string {
    for (const key in errors) {
        if (errors.hasOwnProperty(key) && new RegExp(key).test(message)) {
            return errors[key];
        }
    }
    return message.split(' - ')[1] || message;
}
