import { createSelector } from '@ngrx/store';

import { AccountantPortalSelectors } from './accountant-portal.selectors';

export namespace AnalyticsSelectors {
    export const getGoogleAnalyticsLoaded = createSelector(AccountantPortalSelectors.getAnalytics, state => state.googleAnalyticsLoaded);

    export const getGoogleAnalyticsConfiguration = createSelector(AccountantPortalSelectors.getAnalytics, state => state.googleAnalytics);
}
