import { createSelector } from '@ngrx/store';

import { AccountantPortalSelectors } from './accountant-portal.selectors';

export namespace CompanySelectors {
    export const getSettings = createSelector(AccountantPortalSelectors.getCompanyState, state => state.settings);

    export const getLoaded = createSelector(AccountantPortalSelectors.getCompanyState, state => state.loaded);

    export const getLogo = createSelector(AccountantPortalSelectors.getCompanyState, state => state.logo);

    export const getLogoLoaded = createSelector(AccountantPortalSelectors.getCompanyState, state => state.loadedLogo);

    export const getLogoUploaded = createSelector(AccountantPortalSelectors.getCompanyState, state => state.logoUploaded);

    export const getLogoChanged = createSelector(AccountantPortalSelectors.getCompanyState, state => state.logoChanged);

    export const getPdf = createSelector(AccountantPortalSelectors.getCompanyState, state => state.pdf);

    export const getPdfLoaded = createSelector(AccountantPortalSelectors.getCompanyState, state => state.loadedPdf);

    export const getPdfUploaded = createSelector(AccountantPortalSelectors.getCompanyState, state => state.pdfUploaded);

    export const getPdfChanged = createSelector(AccountantPortalSelectors.getCompanyState, state => state.pdfChanged);

    export const getNewCompanyId = createSelector(AccountantPortalSelectors.getCompanyState, state => state.newCompanyId);

    export const getUploadLogoAndPdf = createSelector(AccountantPortalSelectors.getCompanyState, state => state.uploadLogoAndPdf);

    export const getCollection = createSelector(AccountantPortalSelectors.getCompanyState, state => state.collection);

    export const getCollectionLoaded = createSelector(AccountantPortalSelectors.getCompanyState, state => state.collectionLoaded);
}
