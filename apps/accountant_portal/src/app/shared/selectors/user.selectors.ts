import { createSelector } from '@ngrx/store';

import { ACCOUNTANT_ROLE, OWNER_ROLE } from '../models';

import { AccountantPortalSelectors } from './accountant-portal.selectors';

export namespace UserSelectors {
    export const getLoginUser = createSelector(AccountantPortalSelectors.getUserState, state => state.loginUser);

    export const getAccountantUser = createSelector(AccountantPortalSelectors.getUserState, state => state.accountantUser);

    export const getLoginUserloaded = createSelector(AccountantPortalSelectors.getUserState, state => state.loginUserloaded);

    export const getAccountantUserLoaded = createSelector(AccountantPortalSelectors.getUserState, state => state.accountantUserLoaded);

    export const getRestApiError = createSelector(AccountantPortalSelectors.getUserState, state => state.restApiError);

    export const getAccessPermissions = createSelector(AccountantPortalSelectors.getUserState, state => {
        return {
            isPendingAccountant: state.accountantUser.inviteStatus === 'pending',
            isRejected: state.accountantUser.inviteStatus === 'rejected',
            isUnconnectedAccountant: state.accountantUser.companyId === null,
            isOwner: state.accountantUser.role === OWNER_ROLE,
            isAccountant: state.accountantUser.role === ACCOUNTANT_ROLE,
            isAdmin: state.accountantUser.role === 'accountant' && state.loginUser.hasOwnProperty('isAccountantAdmin')
        };
    });
}
