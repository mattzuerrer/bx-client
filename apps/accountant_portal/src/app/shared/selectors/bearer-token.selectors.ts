import { createSelector } from '@ngrx/store';

import { AccountantPortalSelectors } from './accountant-portal.selectors';

export namespace BearerTokenSelectors {
    export const getUser = createSelector(AccountantPortalSelectors.getBearerToken, state => state.bearerTokenUser);

    export const getLoaded = createSelector(AccountantPortalSelectors.getBearerToken, state => state.loaded);

    export const getAccessPermissions = createSelector(AccountantPortalSelectors.getBearerToken, state => {
        return {
            isBearerTokenUser: state.loaded
        };
    });
}
