import { createSelector } from '@ngrx/store';

import { Accountant } from '../models';

import { AccountantPortalSelectors } from './accountant-portal.selectors';

const getWithOnboardingFinished = (accountants: Accountant[]) => {
    return accountants.filter(accountant => accountant.onboardingStatus === 'finished');
};

export namespace AccountantsSelectors {
    export const getAccountants = createSelector(AccountantPortalSelectors.getAccountantsState, state => state.collection);

    export const getLoaded = createSelector(AccountantPortalSelectors.getAccountantsState, state => state.loaded);

    export const getAccountantsWithFinishedOnboarding = createSelector(getAccountants, getWithOnboardingFinished);
}
