import { createFeatureSelector, createSelector } from '@ngrx/store';

import { featureName } from '../../../config';
import { State } from '../reducers';

export namespace AccountantPortalSelectors {
    export const getState = createFeatureSelector<State>(featureName);

    export const getAccountantsState = createSelector(getState, state => {
        return state.accountants;
    });

    export const getUserState = createSelector(getState, state => {
        return state.user;
    });

    export const getClientsState = createSelector(getState, state => {
        return state.clients;
    });

    export const getCompanyState = createSelector(getState, state => {
        return state.company;
    });

    export const getAnalytics = createSelector(getState, state => {
        return state.analytics;
    });

    export const getBearerToken = createSelector(getState, state => {
        return state.bearerToken;
    });
}
