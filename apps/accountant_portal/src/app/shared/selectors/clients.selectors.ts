import { createSelector } from '@ngrx/store';

import { Client } from '../models';

import { AccountantPortalSelectors } from './accountant-portal.selectors';

const getIdsOfAccountantsWithConnectedClients = (clients: Client[]) => {
    const ids: number[] = [];
    clients.forEach(client =>
        client.clientAccesses.forEach(clientAccess => {
            if (!ids.includes(clientAccess.accountantId)) {
                ids.push(clientAccess.accountantId);
            }
        })
    );
    return ids;
};

export namespace ClientsSelectors {
    export const getClients = createSelector(AccountantPortalSelectors.getClientsState, state => state.clientsCollection);

    export const getClientsLoaded = createSelector(AccountantPortalSelectors.getClientsState, state => state.clientsLoaded);

    export const getPendingClientInvites = createSelector(
        AccountantPortalSelectors.getClientsState,
        state => state.pendingClientInvitesCollection
    );

    export const getPendingClientInvitesLoaded = createSelector(
        AccountantPortalSelectors.getClientsState,
        state => state.pendingClientInvitesLoaded
    );

    export const getAccountantsWithConnectedClients = createSelector(getClients, getIdsOfAccountantsWithConnectedClients);
}
