import { MockComponent } from 'ng2-mock-component';

export const LoaderOverlayMockComponent = MockComponent({
    selector: 'app-loader-overlay',
    inputs: ['disableLoader']
});

export const HeaderMockComponent = MockComponent({
    selector: 'app-header',
    inputs: ['loginUser']
});

export const BxSpinnerMockComponent = MockComponent({
    selector: 'bx-spinner'
});
