import { Component, Input } from '@angular/core';

import { Accountant, Permissions } from '../../models';

@Component({
    selector: 'app-navbar',
    templateUrl: 'navbar.component.html',
    styleUrls: ['navbar.component.scss']
})
export class NavbarComponent {
    @Input() accessPermissions: Permissions;
    @Input() accountantUser: Accountant;
}
