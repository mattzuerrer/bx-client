import { CompanyFormComponent } from './company-form/company-form.component';
import { HeaderComponent } from './header/header.component';
import { NavbarComponent } from './navbar/navbar.component';

export const sharedComponents = [CompanyFormComponent, HeaderComponent, NavbarComponent];
