import { Component } from '@angular/core';
import { CdnService } from '@bx-client/core/src/environments/service/cdn.service';

import { version } from '../../../../../config';

@Component({
    selector: 'app-status-403',
    templateUrl: 'status-403.component.html',
    styleUrls: ['status-403.component.scss']
})
export class Status403Component {
    assetUrl: string = this.cdn.getAssetUrl(version);

    constructor(private cdn: CdnService) {}
}
