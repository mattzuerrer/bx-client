import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoggedInGuard } from '../../../guards';

import { Status403Component } from './status-403.component';

const routes: Routes = [
    {
        path: '',
        component: Status403Component,
        canActivate: [LoggedInGuard]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
