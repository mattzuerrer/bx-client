import { NgModule } from '@angular/core';

import { SharedModule } from '../../../shared.module';

import { containers } from './index';
import { routing } from './status-403.routes';

@NgModule({
    imports: [routing, SharedModule],
    exports: [containers],
    declarations: [...containers]
})
export class Status403Module {}
