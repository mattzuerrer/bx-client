import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ariaInvalidFn, ariaLiveFn, makeAriaInvalidFn, makeAriaLiveFn, Validators as CustomValidators } from '@bx-client/common';
import { TranslateService } from '@ngx-translate/core';
import { FileLikeObject, FileUploader, FileUploaderOptions } from 'ng2-file-upload';

import {
    COUNTRIES,
    LEGAL_ENTITIES,
    UPLOAD_LOGO_MAX_SIZE_IN_MB,
    UPLOAD_LOGO_MIME_TYPES,
    UPLOAD_PDF_MAX_SIZE_IN_MB, UPLOAD_PDF_MIME_TYPES
} from '../../../../config';
import { PortalCompanyFile } from '../../models';
import { PortalMessageService } from '../../services';
import { binaryToBase64, createLogoFromBinaryData } from '../../utils/binaries';

@Component({
    selector: 'app-company-form',
    templateUrl: 'company-form.component.html',
    styleUrls: ['company-form.component.scss']
})
export class CompanyFormComponent implements OnInit {
    companyForm: FormGroup;

    isAriaInvalid: ariaInvalidFn;
    ariaLive: ariaLiveFn;

    legalEntities = LEGAL_ENTITIES;
    countries = COUNTRIES;

    isPublic = false;
    hasAssociation = false;

    logoUploader: FileUploader;
    logo: PortalCompanyFile = null;
    originalLogo: PortalCompanyFile = null;
    isLogoFileOver = false;
    maxLogoFileSize = UPLOAD_LOGO_MAX_SIZE_IN_MB;
    validLogoMimeTypes = UPLOAD_LOGO_MIME_TYPES;

    pdfUploader: FileUploader;
    pdf: PortalCompanyFile = null;
    originalPdf: PortalCompanyFile = null;
    isPdfFileOver = false;
    maxPdfFileSize = UPLOAD_PDF_MAX_SIZE_IN_MB;
    validPdfMimeTypes = UPLOAD_PDF_MIME_TYPES;

    @Input() companyLogo: ArrayBuffer;
    @Input() companyPdf: ArrayBuffer;
    @Input() companyLogoChanged: boolean;
    @Input() companyPdfChanged: boolean;
    @Input() moreInfo = false;
    @Input() isSetupForm = false;

    @Output() formReady = new EventEmitter<FormGroup>();
    @Output() logoChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() pdfChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor(
        private formBuilder: FormBuilder,
        private portalMessageService: PortalMessageService,
        private translateService: TranslateService,
        private route: ActivatedRoute
    ) {}

    ngOnInit(): void {
        this.companyForm = this.formBuilder.group({
            name: ['', Validators.required],
            legal: [this.legalEntities[0]],
            address: [''],
            postcode: [''],
            city: [''],
            country: [this.countries[0]],
            email: ['', [Validators.required, CustomValidators.email]],
            phone: ['', [Validators.required, CustomValidators.phone]],
            website: ['', CustomValidators.url],
            isPublic: [false],
            description: [''],
            quote: [''],
            association: [false],
            logo: [null],
            pdf: [null]
        });

        this.isAriaInvalid = makeAriaInvalidFn(this.companyForm);
        this.ariaLive = makeAriaLiveFn(this.companyForm);

        this.formReady.emit(this.companyForm);
        this.initLogoFile();
        this.initPdfFile();
        this.initFileUploader();
    }

    onLogoFileOver(isHovering: boolean): void {
        this.isLogoFileOver = isHovering;
    }

    resetLogoFile(): void {
        this.logoUploader.clearQueue();
        this.logo = this.originalLogo;

        this.companyForm.get('logo').setValue(this.logo);
        this.companyForm.markAsDirty();

        this.logoChanged.emit(false);
    }

    onPdfFileOver(isHovering: boolean): void {
        this.isPdfFileOver = isHovering;
    }

    resetPdfFile(): void {
        this.pdfUploader.clearQueue();
        this.pdf = this.originalPdf;

        this.companyForm.get('pdf').setValue(this.pdf);
        this.companyForm.markAsDirty();

        this.pdfChanged.emit(false);
    }

    private initFileUploader(): void {
        this.initLogoUpload();
        this.initPdfUpload();
    }

    private initLogoFile(): void {
        this.logo = this.companyLogo
            ? createLogoFromBinaryData(this.companyLogo, this.route.snapshot.params.accountantUserId)
            : new PortalCompanyFile(this.route.snapshot.params.accountantUserId);

        this.originalLogo = this.logo;
    }

    private initLogoUpload(): void {
        const logoOptions: FileUploaderOptions = {
            isHTML5: true,
            maxFileSize: this.maxLogoFileSize * 1024 * 1024, // MB -> Bytes
            allowedMimeType: this.validLogoMimeTypes
        };

        this.logoUploader = new FileUploader(logoOptions);
        this.logoUploader.onWhenAddingFileFailed = (item, filter) => {
            this.showError(item, filter, this.maxLogoFileSize);
        };

        this.logoUploader.onAfterAddingFile = fileItem => {
            this.logo = new PortalCompanyFile(this.route.snapshot.params.accountantUserId);
            this.logo.file = fileItem._file;
            this.logo.name = fileItem.file.name;

            this.logoChanged.emit(true);

            // base64
            const reader = new FileReader();
            reader.readAsDataURL(fileItem._file);
            reader.onloadend = () => {
                this.logo.base64 = reader.result;
            };

            this.companyForm.get('logo').setValue(this.logo);
            this.companyForm.markAsDirty();
        };
    }

    private initPdfFile(): void {
        this.pdf = new PortalCompanyFile(this.route.snapshot.params.accountantUserId);
        this.pdf.file = null;

        if (this.companyPdf) {
            this.pdf.base64 = 'data:application/pdf;base64,' + binaryToBase64(this.companyPdf);
        }
        this.originalPdf = this.pdf;
    }

    private initPdfUpload(): void {
        const pdfOptions: FileUploaderOptions = {
            isHTML5: true,
            maxFileSize: this.maxPdfFileSize * 1024 * 1024, // MB -> Bytes
            allowedMimeType: this.validPdfMimeTypes
        };

        this.pdfUploader = new FileUploader(pdfOptions);
        this.pdfUploader.onWhenAddingFileFailed = (item, filter) => {
            this.showError(item, filter, this.maxPdfFileSize);
        };

        this.pdfUploader.onAfterAddingFile = fileItem => {
            this.pdf = new PortalCompanyFile(this.route.snapshot.params.accountantUserId);
            this.pdf.file = fileItem._file;
            this.pdf.name = fileItem.file.name;

            this.pdfChanged.emit(true);

            // base64
            const reader = new FileReader();
            reader.readAsDataURL(fileItem._file);
            reader.onloadend = () => {
                this.pdf.base64 = reader.result;
            };

            this.companyForm.get('pdf').setValue(this.pdf);
            this.companyForm.markAsDirty();
        };
    }

    private showError(item: FileLikeObject, filter: any, maxUploadSize: number): void {
        let error = '';

        switch (filter.name) {
            case 'fileSize':
                error = this.translateService.instant('accountant_portal.company_settings.file_upload.error.max_size', {
                    fileName: item.name,
                    maxSize: maxUploadSize
                });
                break;
            case 'mimeType':
                error = this.translateService.instant('accountant_portal.company_settings.file_upload.error.type');
                break;
        }

        if (error.length > 0) {
            this.portalMessageService.showErrorMessageFromPlainString(error);
        }
    }
}
