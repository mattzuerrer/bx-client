import { Component, Input, OnInit } from '@angular/core';
import { CdnService } from '@bx-client/core/src/environments/service/cdn.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { version } from '../../../../config';
import { Accountant } from '../../models';
import { AccountantPortalStateService } from '../../services';

@Component({
    selector: 'app-header',
    templateUrl: 'header.component.html',
    styleUrls: ['header.component.scss']
})
export class HeaderComponent implements OnInit {
    @Input() loginUser: Accountant;
    isValidSession$: Observable<boolean>;
    isNotTokenUser$: Observable<boolean>;
    assetUrl: string = this.cdn.getAssetUrl(version);

    constructor(public accountantPortalStateService: AccountantPortalStateService, private cdn: CdnService) {}

    goToMyProfile(): void {
        window.location.replace(this.loginUser.profileEditUrl);
    }

    gotToLogout(): void {
        window.location.replace('/logout');
    }

    ngOnInit(): void {
        this.isValidSession$ = this.accountantPortalStateService.restApiError$.pipe(map(apiError => !apiError));
        this.isNotTokenUser$ = this.accountantPortalStateService.tokenUserLoaded$.pipe(map(loaded => !loaded));
    }
}
