import { HttpErrorResponse } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { FormGroup } from '@angular/forms';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { noop, Observable, of, throwError } from 'rxjs';

import { CompanyActions } from '../actions';
import { accountants, company } from '../fixtures';
import { PortalCompanyFile } from '../models';
import { AccountantPortalStateService, ApiService, PortalMessageService } from '../services';

import { CompanyEffects } from './company.effects';

const apiServiceStub = {
    getCompanySettings: () => noop(),
    saveCompanySettings: () => noop(),
    getCompanyLogo: () => noop(),
    saveCompanyLogo: () => noop(),
    getCompanyPdf: () => noop(),
    saveCompanyPdf: () => noop(),
    createOrUpdateNewCompany: () => noop(),
    createOrUpdateNewCompanyLogo: () => noop,
    createOrUpdateNewCompanyPdf: () => noop
};

const portalMessageServiceStub = {
    showSuccessMessage: () => noop(),
    showErrorMessage: () => noop()
};

const stateServiceStub = {
    accountantUser$: of(accountants[0]),
    newCompanyId$: of(company.id),
    dispatch: () => noop()
};

const formGroup = new FormGroup({});

const httpErrorPayload = new HttpErrorResponse({
    error: 'Unprocessable Entity',
    status: 422
});

const response = { id: 1 };

describe('Company effects', () => {
    let companyEffects: CompanyEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<CompanyEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                CompanyEffects,
                {
                    provide: ApiService,
                    useValue: apiServiceStub
                },
                {
                    provide: AccountantPortalStateService,
                    useValue: stateServiceStub
                },
                {
                    provide: PortalMessageService,
                    useValue: portalMessageServiceStub
                },
                provideMockActions(() => actions)
            ]
        });

        companyEffects = TestBed.get(CompanyEffects);
        metadata = getEffectsMetadata(companyEffects);
    });

    it(
        'should return LOAD_SETTINGS_SUCCESS on LOAD_SETTINGS if getCompanySettings is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getCompanySettings').and.returnValue(of(company));

            actions = cold('a', { a: new CompanyActions.LoadSettingsAction(1) });
            const expected = cold('b', { b: new CompanyActions.LoadSettingsSuccessAction(company) });

            expect(companyEffects.fetch$).toBeObservable(expected);
            expect(apiService.getCompanySettings).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_SETTINGS_FAIL on LOAD_SETTINGS if getCompanySettings fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getCompanySettings').and.returnValue(throwError(null));

            actions = cold('a', { a: new CompanyActions.LoadSettingsAction(1) });
            const expected = cold('b', { b: new CompanyActions.LoadSettingsFailAction() });

            expect(companyEffects.fetch$).toBeObservable(expected);
            expect(apiService.getCompanySettings).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return SAVE_SETTINGS_SUCCESS if saveCompanySettings is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'saveCompanySettings').and.returnValue(of(company));

            actions = cold('a', { a: new CompanyActions.SaveSettingsAction(company, formGroup) });
            const expected = cold('b', { b: new CompanyActions.SaveSettingsSuccessAction(company, formGroup) });

            expect(companyEffects.save$).toBeObservable(expected);
            expect(apiService.saveCompanySettings).toHaveBeenCalledTimes(1);
            expect(apiService.saveCompanySettings).toHaveBeenCalledWith(company, accountants[0].id);
        })
    );

    it(
        'should call showSuccessMessage on SAVE_SETTINGS_SUCCESS action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showSuccessMessage').and.callThrough();

            actions = cold('a', { a: new CompanyActions.SaveSettingsSuccessAction(company, formGroup) });
            const expected = cold('b', { b: new CompanyActions.SaveSettingsSuccessAction(company, formGroup) });

            expect(companyEffects.saveSuccess$).toBeObservable(expected);
            expect(portalMessageService.showSuccessMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return SAVE_SETTINGS_FAIL if saveCompanySettings fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'saveCompanySettings').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new CompanyActions.SaveSettingsAction(company, formGroup) });
            const expected = cold('b', { b: new CompanyActions.SaveSettingsFailAction(httpErrorPayload) });

            expect(companyEffects.save$).toBeObservable(expected);
            expect(apiService.saveCompanySettings).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showErrorMessage on SAVE_SETTINGS_FAIL action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showErrorMessage').and.callThrough();

            actions = cold('a', { a: new CompanyActions.SaveSettingsFailAction(httpErrorPayload) });
            const expected = cold('b', { b: new CompanyActions.SaveSettingsFailAction(httpErrorPayload) });

            expect(companyEffects.saveFail$).toBeObservable(expected);
            expect(portalMessageService.showErrorMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_LOGO_SUCCESS if getCompanyLogo is successful',
        inject([ApiService], apiService => {
            const logoReturn = new ArrayBuffer(1);
            spyOn(apiService, 'getCompanyLogo').and.returnValue(of(logoReturn));

            actions = cold('a', { a: new CompanyActions.LoadLogoAction(1) });
            const expected = cold('b', { b: new CompanyActions.LoadLogoSuccessAction(logoReturn) });

            expect(companyEffects.fetchLogo$).toBeObservable(expected);
            expect(apiService.getCompanyLogo).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_LOGO_FAIL if getCompanyLogo is fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getCompanyLogo').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new CompanyActions.LoadLogoAction(1) });
            const expected = cold('b', { b: new CompanyActions.LoadLogoFailAction() });

            expect(companyEffects.fetchLogo$).toBeObservable(expected);
            expect(apiService.getCompanyLogo).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOGO_UPLOAD_SUCCESS if saveCompanyLogo is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'saveCompanyLogo').and.returnValue(of(company));

            actions = cold('a', { a: new CompanyActions.UploadLogoAction(new PortalCompanyFile(1)) });
            const expected = cold('b', { b: new CompanyActions.UploadLogoSuccessAction() });

            expect(companyEffects.uploadLogo$).toBeObservable(expected);
            expect(apiService.saveCompanyLogo).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOGO_UPLOAD_FAIL if saveCompanyLogo is fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'saveCompanyLogo').and.returnValue(throwError('apiError'));

            actions = cold('a', { a: new CompanyActions.UploadLogoAction(new PortalCompanyFile(1)) });
            const expected = cold('b', { b: new CompanyActions.UploadLogoFailAction('apiError') });

            expect(companyEffects.uploadLogo$).toBeObservable(expected);
            expect(apiService.saveCompanyLogo).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOGO_UPLOAD_SUCCESS if createOrUpdateNewCompanyLogo is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'createOrUpdateNewCompanyLogo').and.returnValue(of(company));

            actions = cold('a', { a: new CompanyActions.CreateNewCompanySuccessAction(company, response) });
            const expected = cold('b', { b: new CompanyActions.UploadLogoSuccessAction() });

            expect(companyEffects.createOrUpdateNewCompanyLogo$).toBeObservable(expected);
            expect(apiService.createOrUpdateNewCompanyLogo).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOGO_UPLOAD_FAIL if createOrUpdateNewCompanyLogo is fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'createOrUpdateNewCompanyLogo').and.returnValue(throwError('apiError'));

            actions = cold('a', { a: new CompanyActions.CreateNewCompanySuccessAction(company, response) });
            const expected = cold('b', { b: new CompanyActions.UploadLogoFailAction('apiError') });

            expect(companyEffects.createOrUpdateNewCompanyLogo$).toBeObservable(expected);
            expect(apiService.createOrUpdateNewCompanyLogo).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_PDF_SUCCESS if getCompanyPdf is successful',
        inject([ApiService], apiService => {
            const pdfReturn = new ArrayBuffer(1);
            spyOn(apiService, 'getCompanyPdf').and.returnValue(of(pdfReturn));

            actions = cold('a', { a: new CompanyActions.LoadPdfAction(1) });
            const expected = cold('b', { b: new CompanyActions.LoadPdfSuccessAction(pdfReturn) });

            expect(companyEffects.fetchPdf$).toBeObservable(expected);
            expect(apiService.getCompanyPdf).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_PDF_FAIL if getCompanyPdf is fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getCompanyPdf').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new CompanyActions.LoadPdfAction(1) });
            const expected = cold('b', { b: new CompanyActions.LoadPdfFailAction() });

            expect(companyEffects.fetchPdf$).toBeObservable(expected);
            expect(apiService.getCompanyPdf).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return PDF_UPLOAD_SUCCESS if createOrUpdateNewCompanyPdf is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'createOrUpdateNewCompanyPdf').and.returnValue(of(company));

            actions = cold('a', { a: new CompanyActions.CreateNewCompanySuccessAction(company, response) });
            const expected = cold('b', { b: new CompanyActions.UploadPdfSuccessAction() });

            expect(companyEffects.createOrUpdateNewCompanyPdf$).toBeObservable(expected);
            expect(apiService.createOrUpdateNewCompanyPdf).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return PDF_UPLOAD_FAIL if createOrUpdateNewCompanyPdf is fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'createOrUpdateNewCompanyPdf').and.returnValue(throwError('apiError'));

            actions = cold('a', { a: new CompanyActions.CreateNewCompanySuccessAction(company, response) });
            const expected = cold('b', { b: new CompanyActions.UploadPdfFailAction('apiError') });

            expect(companyEffects.createOrUpdateNewCompanyPdf$).toBeObservable(expected);
            expect(apiService.createOrUpdateNewCompanyPdf).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return PDF_UPLOAD_SUCCESS if saveCompanyPdf is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'saveCompanyPdf').and.returnValue(of(company));

            actions = cold('a', { a: new CompanyActions.UploadPdfAction(new PortalCompanyFile(1)) });
            const expected = cold('b', { b: new CompanyActions.UploadPdfSuccessAction() });

            expect(companyEffects.uploadPdf$).toBeObservable(expected);
            expect(apiService.saveCompanyPdf).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return PDF_UPLOAD_FAIL if saveCompanyPdf is fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'saveCompanyPdf').and.returnValue(throwError('apiError'));

            actions = cold('a', { a: new CompanyActions.UploadPdfAction(new PortalCompanyFile(1)) });
            const expected = cold('b', { b: new CompanyActions.UploadPdfFailAction('apiError') });

            expect(companyEffects.uploadPdf$).toBeObservable(expected);
            expect(apiService.saveCompanyPdf).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return CREATE_NEW_COMPANY_SUCCESS if createOrUpdateNewCompany is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'createOrUpdateNewCompany').and.returnValue(of(response));

            actions = cold('a', { a: new CompanyActions.CreateNewCompanyAction(company) });
            const expected = cold('b', { b: new CompanyActions.CreateNewCompanySuccessAction(company, response) });

            expect(companyEffects.createOrUpdateNewCompany$).toBeObservable(expected);
            expect(apiService.createOrUpdateNewCompany).toHaveBeenCalledTimes(1);
            expect(apiService.createOrUpdateNewCompany).toHaveBeenCalledWith(company, response.id);
        })
    );

    it(
        'should return CREATE_NEW_COMPANY_SUCCESS_FAIL if createOrUpdateNewCompany fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'createOrUpdateNewCompany').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new CompanyActions.CreateNewCompanyAction(company) });
            const expected = cold('b', { b: new CompanyActions.CreateNewCompanyFailAction(httpErrorPayload) });

            expect(companyEffects.createOrUpdateNewCompany$).toBeObservable(expected);
            expect(apiService.createOrUpdateNewCompany).toHaveBeenCalledTimes(1);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetch$).toEqual({ dispatch: true });
        expect(metadata.save$).toEqual({ dispatch: true });
        expect(metadata.saveSuccess$).toEqual({ dispatch: false });
        expect(metadata.saveFail$).toEqual({ dispatch: false });
        expect(metadata.createOrUpdateNewCompany$).toEqual({ dispatch: true });
        expect(metadata.createOrUpdateNewCompanyLogo$).toEqual({ dispatch: true });
        expect(metadata.createOrUpdateNewCompanyPdf$).toEqual({ dispatch: true });
        expect(metadata.createNewCompanyLogoAndPdfRedirect$).toEqual({ dispatch: false });
    });
});
