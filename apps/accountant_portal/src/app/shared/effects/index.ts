import { AccountantsEffects } from './accountants.effects';
import { AnalyticsEffects } from './analytics.effects';
import { BearerTokenEffects } from './bearer-token.effects';
import { ClientsEffects } from './clients.effects';
import { CompanyEffects } from './company.effects';
import { ServicesEffects } from './services.effects';
import { UserEffects } from './user.effects';

export const effects = [
    AccountantsEffects,
    AnalyticsEffects,
    BearerTokenEffects,
    ClientsEffects,
    CompanyEffects,
    ServicesEffects,
    UserEffects
];
