import { HttpErrorResponse } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { noop, Observable, of, throwError } from 'rxjs';

import { AccountantActions, ClientsActions } from '../actions';
import { accountants, clientAccess, clients, keyPerformanceIndicators, pendingClientInvites } from '../fixtures';
import { AccountantPortalStateService, ApiService, PortalMessageService } from '../services';

import { ClientsEffects } from './clients.effects';

const apiServiceStub = {
    getClients: () => noop(),
    reinviteClientToAccountant: () => noop(),
    connectClientAccountant: () => noop(),
    disconnectClientAccountant: () => noop(),
    rejectClientAccountantInvitation: () => noop(),
    getClientAccess: () => noop(),
    createClient: () => noop(),
    getPendingClientInvites: () => noop(),
    loadKeyPerformanceIndicators: () => noop()
};

const stateServiceStub = {
    dispatch: () => noop(),
    accessPermissions$: of({ isOwner: false })
};

const portalMessageServiceStub = {
    showSuccessMessage: () => noop(),
    showErrorMessage: () => noop()
};

const httpErrorPayload = new HttpErrorResponse({
    error: 'Unprocessable Entity',
    status: 422
});

const clientAccountantId = { clientId: 1, id: 2 };
const clientAccountantCollection = { clientId: 1, ids: [accountants[0]] };

describe('Clients effects', () => {
    let clientsEffects: ClientsEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<ClientsEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                ClientsEffects,
                {
                    provide: ApiService,
                    useValue: apiServiceStub
                },
                {
                    provide: AccountantPortalStateService,
                    useValue: stateServiceStub
                },
                {
                    provide: PortalMessageService,
                    useValue: portalMessageServiceStub
                },
                provideMockActions(() => actions)
            ]
        });

        clientsEffects = TestBed.get(ClientsEffects);
        metadata = getEffectsMetadata(clientsEffects);
    });

    it(
        'should return LOAD_SUCCESS on LOAD if getClients is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getClients').and.returnValue(of(clients));

            actions = cold('a', { a: new ClientsActions.LoadAction() });
            const expected = cold('b', { b: new ClientsActions.LoadSuccessAction(clients) });

            expect(clientsEffects.fetch$).toBeObservable(expected);
            expect(apiService.getClients).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_FAIL on LOAD if getClients fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getClients').and.returnValue(throwError(null));

            actions = cold('a', { a: new ClientsActions.LoadAction() });
            const expected = cold('b', { b: new ClientsActions.LoadFailAction() });

            expect(clientsEffects.fetch$).toBeObservable(expected);
            expect(apiService.getClients).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return REINVITE_SUCCESS if reinviteClientToAccountant is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'reinviteClientToAccountant').and.returnValue(of(clientAccess));

            actions = cold('a', { a: new ClientsActions.ReinviteAction(clientAccountantId) });
            const expected = cold('b', { b: new ClientsActions.ReinviteSuccessAction() });

            expect(clientsEffects.reinvite$).toBeObservable(expected);
            expect(apiService.reinviteClientToAccountant).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return REINVITE_FAIL if reinviteClientToAccountant fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'reinviteClientToAccountant').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new ClientsActions.ReinviteAction(clientAccountantId) });
            const expected = cold('b', { b: new ClientsActions.ReinviteFailAction(httpErrorPayload) });

            expect(clientsEffects.reinvite$).toBeObservable(expected);
            expect(apiService.reinviteClientToAccountant).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return CONNECT_SUCCESS if connectClientAccountant is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'connectClientAccountant').and.returnValue(of(clientAccess));

            actions = cold('a', { a: new ClientsActions.ConnectAction(clientAccountantCollection) });
            const expected = cold('b', { b: new ClientsActions.ConnectSuccessAction() });

            expect(clientsEffects.connect$).toBeObservable(expected);
            expect(apiService.connectClientAccountant).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return CONNECT_FAIL if connectClientAccountant fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'connectClientAccountant').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new ClientsActions.ConnectAction(clientAccountantCollection) });
            const expected = cold('b', { b: new ClientsActions.ConnectFailAction(httpErrorPayload) });

            expect(clientsEffects.connect$).toBeObservable(expected);
            expect(apiService.connectClientAccountant).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return DISCONNECT_SUCCESS if disconnectClientAccountant is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'disconnectClientAccountant').and.returnValue(of(clientAccess));

            actions = cold('a', { a: new ClientsActions.DisconnectAction(clientAccountantId) });
            const expected = cold('b', { b: new ClientsActions.DisconnectSuccessAction(clientAccess) });

            expect(clientsEffects.disconnect$).toBeObservable(expected);
            expect(apiService.disconnectClientAccountant).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return DISCONNECT_FAIL if disconnectClientAccountant fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'disconnectClientAccountant').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new ClientsActions.DisconnectAction(clientAccountantId) });
            const expected = cold('b', { b: new ClientsActions.DisconnectFailAction(httpErrorPayload) });

            expect(clientsEffects.disconnect$).toBeObservable(expected);
            expect(apiService.disconnectClientAccountant).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return REJECT_SUCCESS if rejectClientAccountantInvitation is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'rejectClientAccountantInvitation').and.returnValue(of(clientAccess));

            actions = cold('a', { a: new ClientsActions.RejectAction(clientAccountantId) });
            const expected = cold('b', { b: new ClientsActions.RejectSuccessAction(clientAccess) });

            expect(clientsEffects.reject$).toBeObservable(expected);
            expect(apiService.rejectClientAccountantInvitation).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return REJECT_FAIL if rejectClientAccountantInvitation fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'rejectClientAccountantInvitation').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new ClientsActions.RejectAction(clientAccountantId) });
            const expected = cold('b', { b: new ClientsActions.RejectFailAction(httpErrorPayload) });

            expect(clientsEffects.reject$).toBeObservable(expected);
            expect(apiService.rejectClientAccountantInvitation).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_ACCESS_SUCCESS if getClientAccess is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getClientAccess').and.returnValue(of([clientAccess]));

            actions = cold('a', { a: new ClientsActions.LoadAccessAction(1) });
            const expected = cold('b', { b: new ClientsActions.LoadAccessSuccessAction([clientAccess]) });

            expect(clientsEffects.loadAccess$).toBeObservable(expected);
            expect(apiService.getClientAccess).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_ACCESS_FAIL if getClientAccess fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getClientAccess').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new ClientsActions.LoadAccessAction(1) });
            const expected = cold('b', { b: new ClientsActions.LoadAccessFailAction(httpErrorPayload) });

            expect(clientsEffects.loadAccess$).toBeObservable(expected);
            expect(apiService.getClientAccess).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_KEY_PERFORMANCE_INDICATORS_SUCCESS if loadKeyPerformanceIndicators is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'loadKeyPerformanceIndicators').and.returnValue(of(keyPerformanceIndicators));

            actions = cold('a', { a: new ClientsActions.LoadKeyPerformanceIndicatorsAction(1) });
            const expected = cold('b', {
                b: new ClientsActions.LoadKeyPerformanceIndicatorsSuccessAction({ id: 1, keyPerformanceIndicators })
            });

            expect(clientsEffects.loadKeyPerformanceIndicators$).toBeObservable(expected);
            expect(apiService.loadKeyPerformanceIndicators).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_KEY_PERFORMANCE_INDICATORS_FAIL if loadKeyPerformanceIndicators fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'loadKeyPerformanceIndicators').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new ClientsActions.LoadKeyPerformanceIndicatorsAction(1) });
            const expected = cold('b', { b: new ClientsActions.LoadKeyPerformanceIndicatorsFailAction(httpErrorPayload) });

            expect(clientsEffects.loadKeyPerformanceIndicators$).toBeObservable(expected);
            expect(apiService.loadKeyPerformanceIndicators).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showSuccessMessage on REINVITE_SUCCESS action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showSuccessMessage').and.callThrough();

            actions = cold('a', { a: new ClientsActions.ReinviteSuccessAction() });
            const expected = cold('b', { b: new ClientsActions.ReinviteSuccessAction() });

            expect(clientsEffects.reinviteSuccess$).toBeObservable(expected);
            expect(portalMessageService.showSuccessMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showSuccessMessage on DISCONNECT_SUCCESS action',
        inject([PortalMessageService, AccountantPortalStateService], (portalMessageService, stateService) => {
            spyOn(portalMessageService, 'showSuccessMessage').and.callThrough();
            spyOn(stateService, 'dispatch').and.callThrough();

            actions = cold('a', { a: new ClientsActions.DisconnectSuccessAction(clientAccess) });
            const expected = cold('b', { b: new ClientsActions.LoadAccessAction(clientAccess.clientId) });

            expect(clientsEffects.disconnectSuccess$).toBeObservable(expected);
            expect(portalMessageService.showSuccessMessage).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new AccountantActions.LoadOneAction(clientAccess.accountantId));
        })
    );

    it(
        'should call showSuccessMessage on ACCEPT_SUCCESS action',
        inject([PortalMessageService], portalMessageService => {
            const clientNamePayload = Object.assign({}, clientAccess, { client: clients[0] });
            spyOn(portalMessageService, 'showSuccessMessage').and.callThrough();

            actions = cold('a', { a: new ClientsActions.AcceptSuccessAction(clientNamePayload) });
            const expected = cold('b', { b: new ClientsActions.AcceptSuccessAction(clientNamePayload) });

            expect(clientsEffects.acceptSuccess$).toBeObservable(expected);
            expect(portalMessageService.showSuccessMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showSuccessMessage without client name on REJECT_SUCCESS action',
        inject([PortalMessageService, AccountantPortalStateService], (portalMessageService, stateService) => {
            spyOn(portalMessageService, 'showSuccessMessage').and.callThrough();
            spyOn(stateService, 'dispatch').and.callThrough();

            actions = cold('a', { a: new ClientsActions.RejectSuccessAction(clientAccess) });
            const expected = cold('b', { b: new ClientsActions.LoadAccessAction(clientAccess.clientId) });

            expect(clientsEffects.rejectSuccess$).toBeObservable(expected);
            expect(portalMessageService.showSuccessMessage).toHaveBeenCalledTimes(1);
            expect(portalMessageService.showSuccessMessage).toHaveBeenCalledWith('accountant_portal.messages.success.clients.reject');
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new AccountantActions.LoadOneAction(clientAccess.accountantId));
        })
    );

    it(
        'should call showSuccessMessage with client name on REJECT_SUCCESS action',
        inject([PortalMessageService, AccountantPortalStateService], (portalMessageService, stateService) => {
            const clientNamePayload = Object.assign({}, clientAccess, { client: clients[0] });
            spyOn(portalMessageService, 'showSuccessMessage').and.callThrough();
            spyOn(stateService, 'dispatch').and.callThrough();

            actions = cold('a', { a: new ClientsActions.RejectSuccessAction(clientNamePayload) });
            const expected = cold('b', { b: new ClientsActions.LoadAccessAction(clientAccess.clientId) });

            expect(clientsEffects.rejectSuccess$).toBeObservable(expected);
            expect(portalMessageService.showSuccessMessage).toHaveBeenCalledTimes(1);
            expect(portalMessageService.showSuccessMessage).toHaveBeenCalledWith(
                'accountant_portal.messages.success.clients.reject_pending_client_invite',
                { clientName: 'Edge Garden Services' }
            );
            expect(stateService.dispatch).toHaveBeenCalledTimes(1);
            expect(stateService.dispatch).toHaveBeenCalledWith(new AccountantActions.LoadOneAction(clientAccess.accountantId));
        })
    );

    it(
        'should call showErrorMessage on REINVITE_FAIL action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showErrorMessage').and.callThrough();

            actions = cold('a', { a: new ClientsActions.ReinviteFailAction(httpErrorPayload) });
            const expected = cold('b', { b: new ClientsActions.ReinviteFailAction(httpErrorPayload) });

            expect(clientsEffects.reinviteFail$).toBeObservable(expected);
            expect(portalMessageService.showErrorMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showErrorMessage on CONNECT_FAIL action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showErrorMessage').and.callThrough();

            actions = cold('a', { a: new ClientsActions.ConnectFailAction(httpErrorPayload) });
            const expected = cold('b', { b: new ClientsActions.ConnectFailAction(httpErrorPayload) });

            expect(clientsEffects.connectFail$).toBeObservable(expected);
            expect(portalMessageService.showErrorMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showErrorMessage on DISCONNECT_FAIL action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showErrorMessage').and.callThrough();

            actions = cold('a', { a: new ClientsActions.DisconnectFailAction(httpErrorPayload) });
            const expected = cold('b', { b: new ClientsActions.DisconnectFailAction(httpErrorPayload) });

            expect(clientsEffects.disconnectFail$).toBeObservable(expected);
            expect(portalMessageService.showErrorMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showErrorMessage on REJECT_FAIL action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showErrorMessage').and.callThrough();

            actions = cold('a', { a: new ClientsActions.RejectFailAction(httpErrorPayload) });
            const expected = cold('b', { b: new ClientsActions.RejectFailAction(httpErrorPayload) });

            expect(clientsEffects.rejectFail$).toBeObservable(expected);
            expect(portalMessageService.showErrorMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showErrorMessage on LOAD_ACCESS_FAIL action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showErrorMessage').and.callThrough();

            actions = cold('a', { a: new ClientsActions.LoadAccessFailAction(httpErrorPayload) });
            const expected = cold('b', { b: new ClientsActions.LoadAccessFailAction(httpErrorPayload) });

            expect(clientsEffects.loadAccessFail$).toBeObservable(expected);
            expect(portalMessageService.showErrorMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return GET_LOGIN_LINK if createClient is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'createClient').and.returnValue(of(clients[0]));

            actions = cold('a', { a: new ClientsActions.CreateNewAction() });
            const expected = cold('b', { b: new ClientsActions.GetLoginLinkAction(clients[0].id) });

            expect(clientsEffects.create$).toBeObservable(expected);
            expect(apiService.createClient).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return CREATE_NEW_FAIL if createClient fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'createClient').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new ClientsActions.CreateNewAction() });
            const expected = cold('b', { b: new ClientsActions.CreateNewFailActon(httpErrorPayload) });

            expect(clientsEffects.create$).toBeObservable(expected);
            expect(apiService.createClient).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showErrorMessage on REINVITE_FAIL action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showErrorMessage').and.callThrough();

            actions = cold('a', { a: new ClientsActions.CreateNewFailActon(httpErrorPayload) });
            const expected = cold('b', { b: new ClientsActions.CreateNewFailActon(httpErrorPayload) });

            expect(clientsEffects.createFail$).toBeObservable(expected);
            expect(portalMessageService.showErrorMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_PENDING_INVITES_SUCCESS on LOAD_PENDING_INVITES if getPendingClientInvites is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getPendingClientInvites').and.returnValue(of(pendingClientInvites));

            actions = cold('a', { a: new ClientsActions.LoadPendingInvitesAction() });
            const expected = cold('b', { b: new ClientsActions.LoadPendingInvitesSuccessAction(pendingClientInvites) });

            expect(clientsEffects.fetchInvites$).toBeObservable(expected);
            expect(apiService.getPendingClientInvites).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_PENDING_INVITES_FAIL on LOAD_PENDING_INVITES if getClients fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getPendingClientInvites').and.returnValue(throwError(null));

            actions = cold('a', { a: new ClientsActions.LoadPendingInvitesAction() });
            const expected = cold('b', { b: new ClientsActions.LoadPendingInvitesFailAction() });

            expect(clientsEffects.fetchInvites$).toBeObservable(expected);
            expect(apiService.getPendingClientInvites).toHaveBeenCalledTimes(1);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetch$).toEqual({ dispatch: true });
        expect(metadata.getLoginLink$).toEqual({ dispatch: false });
        expect(metadata.reinvite$).toEqual({ dispatch: true });
        expect(metadata.connect$).toEqual({ dispatch: true });
        expect(metadata.disconnect$).toEqual({ dispatch: true });
        expect(metadata.reject$).toEqual({ dispatch: true });
        expect(metadata.loadAccess$).toEqual({ dispatch: true });
        expect(metadata.reinviteSuccess$).toEqual({ dispatch: false });
        expect(metadata.connectSuccess$).toEqual({ dispatch: false });
        expect(metadata.disconnectSuccess$).toEqual({ dispatch: true });
        expect(metadata.rejectSuccess$).toEqual({ dispatch: true });
        expect(metadata.acceptSuccess$).toEqual({ dispatch: false });
        expect(metadata.reinviteFail$).toEqual({ dispatch: false });
        expect(metadata.connectFail$).toEqual({ dispatch: false });
        expect(metadata.disconnectFail$).toEqual({ dispatch: false });
        expect(metadata.rejectFail$).toEqual({ dispatch: false });
        expect(metadata.rejectFail$).toEqual({ dispatch: false });
        expect(metadata.loadAccessFail$).toEqual({ dispatch: false });
        expect(metadata.create$).toEqual({ dispatch: true });
        expect(metadata.createFail$).toEqual({ dispatch: true });
        expect(metadata.fetchInvites$).toEqual({ dispatch: true });
        expect(metadata.loadKeyPerformanceIndicators$).toEqual({ dispatch: true });
    });
});
