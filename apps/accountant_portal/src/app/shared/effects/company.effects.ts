import { Injectable } from '@angular/core';
import { EffectsService } from '@bx-client/ngrx';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { EMPTY, Observable, of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { CompanyActions } from '../actions';
import { AccountantPortalStateService, ApiService, PortalMessageService } from '../services';

@Injectable()
export class CompanyEffects extends EffectsService {
    @Effect()
    readonly fetch$: Observable<any> = this.actions$.pipe(
        ofType<CompanyActions.LoadSettingsAction>(CompanyActions.LOAD_SETTINGS),
        switchMap(action =>
            this.apiService
                .getCompanySettings(action.payload)
                .pipe(
                    map(companySettings => new CompanyActions.LoadSettingsSuccessAction(companySettings)),
                    catchError(() => of(new CompanyActions.LoadSettingsFailAction()))
                )
        )
    );

    @Effect()
    readonly fetchLogo$: Observable<any> = this.actions$.pipe(
        ofType<CompanyActions.LoadLogoAction>(CompanyActions.LOAD_LOGO),
        switchMap(action =>
            this.apiService
                .getCompanyLogo(action.payload)
                .pipe(
                    map(companyLogo => new CompanyActions.LoadLogoSuccessAction(companyLogo)),
                    catchError(() => of(new CompanyActions.LoadLogoFailAction()))
                )
        )
    );

    @Effect()
    readonly fetchPdf$: Observable<any> = this.actions$.pipe(
        ofType<CompanyActions.LoadPdfAction>(CompanyActions.LOAD_PDF),
        switchMap(action =>
            this.apiService
                .getCompanyPdf(action.payload)
                .pipe(
                    map(companyPdf => new CompanyActions.LoadPdfSuccessAction(companyPdf)),
                    catchError(() => of(new CompanyActions.LoadPdfFailAction()))
                )
        )
    );

    @Effect()
    readonly uploadLogo$: Observable<any> = this.actions$.pipe(
        ofType<CompanyActions.UploadLogoAction>(CompanyActions.LOGO_UPLOAD),
        switchMap(action =>
            this.apiService
                .saveCompanyLogo(action.payload)
                .pipe(
                    tap(response => this.stateService.dispatch(new CompanyActions.LoadLogoAction(response.id))),
                    map(() => new CompanyActions.UploadLogoSuccessAction()),
                    catchError(errorResponse => of(new CompanyActions.UploadLogoFailAction(errorResponse)))
                )
        )
    );

    @Effect({ dispatch: false })
    readonly uploadLogoFail$: Observable<any> = this.actions$.pipe(
        ofType<CompanyActions.UploadLogoFailAction>(CompanyActions.LOGO_UPLOAD_FAIL),
        tap(errorResponse => {
            if (errorResponse.payload.error.errors) {
                const apiError = errorResponse.payload.error.errors.logo_file.message;
                this.portalMessageService.showErrorMessageFromPlainString(apiError);
            } else {
                this.portalMessageService.showErrorMessage(503);
            }
        })
    );

    @Effect()
    readonly uploadPdf$: Observable<any> = this.actions$.pipe(
        ofType<CompanyActions.UploadPdfAction>(CompanyActions.PDF_UPLOAD),
        switchMap(action =>
            this.apiService
                .saveCompanyPdf(action.payload)
                .pipe(
                    map(() => new CompanyActions.UploadPdfSuccessAction()),
                    catchError(errorResponse => of(new CompanyActions.UploadPdfFailAction(errorResponse)))
                )
        )
    );

    @Effect({ dispatch: false })
    readonly uploadPdfFail$: Observable<any> = this.actions$.pipe(
        ofType<CompanyActions.UploadPdfFailAction>(CompanyActions.PDF_UPLOAD_FAIL),
        tap(errorResponse => {
            if (errorResponse.payload.error.errors) {
                const apiError = errorResponse.payload.error.errors.pdf_file.message;
                this.portalMessageService.showErrorMessageFromPlainString(apiError);
            } else {
                this.portalMessageService.showErrorMessage(503);
            }
        })
    );

    @Effect()
    readonly save$: Observable<any> = this.actions$.pipe(
        ofType<CompanyActions.SaveSettingsAction>(CompanyActions.SAVE_SETTINGS),
        withLatestFrom(this.accountantPortalStateService.accountantUser$),
        switchMap(([action, accountantUser]) =>
            this.apiService
                .saveCompanySettings(action.payload, accountantUser.companyId)
                .pipe(
                    map(() => new CompanyActions.SaveSettingsSuccessAction(action.payload, action.form)),
                    catchError(errorResponse => of(new CompanyActions.SaveSettingsFailAction(errorResponse)))
                )
        )
    );

    @Effect({ dispatch: false })
    readonly saveSuccess$: Observable<any> = this.actions$.pipe(
        ofType<CompanyActions.SaveSettingsSuccessAction>(CompanyActions.SAVE_SETTINGS_SUCCESS),
        tap(() => this.portalMessageService.showSuccessMessage('accountant_portal.messages.success.company.save_company_settings')),
        tap(action => action.form.markAsPristine())
    );

    @Effect({ dispatch: false })
    readonly saveFail$: Observable<any> = this.actions$.pipe(
        ofType<CompanyActions.SaveSettingsFailAction>(CompanyActions.SAVE_SETTINGS_FAIL),
        tap(errorResponse => this.portalMessageService.showErrorMessage(errorResponse.payload.status))
    );

    @Effect()
    readonly createOrUpdateNewCompany$: Observable<any> = this.actions$.pipe(
        ofType<CompanyActions.CreateNewCompanyAction>(CompanyActions.CREATE_NEW_COMPANY),
        withLatestFrom(this.accountantPortalStateService.accountantUser$, this.accountantPortalStateService.newCompanyId$),
        switchMap(([action, accountantUser, newCompanyId]) =>
            this.apiService
                .createOrUpdateNewCompany(action.payload, accountantUser.companyId ? accountantUser.companyId : newCompanyId)
                .pipe(
                    tap(() => {
                        if (!action.payload.logo && !action.payload.pdf) {
                            window.location.replace('/clients');
                        }
                    }),
                    map(response => new CompanyActions.CreateNewCompanySuccessAction(action.payload, response)),
                    catchError(errorResponse => of(new CompanyActions.CreateNewCompanyFailAction(errorResponse)))
                )
        )
    );

    @Effect()
    readonly createOrUpdateNewCompanyLogo$: Observable<any> = this.actions$.pipe(
        ofType<CompanyActions.CreateNewCompanySuccessAction>(CompanyActions.CREATE_NEW_COMPANY_SUCCESS),
        switchMap(action => {
            if (action.payload.logo) {
                return this.apiService
                    .createOrUpdateNewCompanyLogo(action.payload.logo, action.response.id)
                    .pipe(
                        map(() => new CompanyActions.UploadLogoSuccessAction()),
                        catchError(errorResponse => of(new CompanyActions.UploadLogoFailAction(errorResponse)))
                    );
            } else {
                return EMPTY;
            }
        })
    );

    @Effect()
    readonly createOrUpdateNewCompanyPdf$: Observable<any> = this.actions$.pipe(
        ofType<CompanyActions.CreateNewCompanySuccessAction>(CompanyActions.CREATE_NEW_COMPANY_SUCCESS),
        switchMap(action => {
            if (action.payload.pdf) {
                return this.apiService
                    .createOrUpdateNewCompanyPdf(action.payload.pdf, action.response.id)
                    .pipe(
                        map(() => new CompanyActions.UploadPdfSuccessAction()),
                        catchError(errorResponse => of(new CompanyActions.UploadPdfFailAction(errorResponse)))
                    );
            } else {
                return EMPTY;
            }
        })
    );

    @Effect({ dispatch: false })
    readonly createNewCompanyLogoAndPdfRedirect$: Observable<any> = this.actions$.pipe(
        ofType<CompanyActions.UploadPdfSuccessAction | CompanyActions.UploadLogoSuccessAction>(
            CompanyActions.PDF_UPLOAD_SUCCESS,
            CompanyActions.LOGO_UPLOAD_SUCCESS
        ),
        withLatestFrom(
            this.accountantPortalStateService.uploadLogoAndPdf$,
            this.accountantPortalStateService.companyPdfUploaded$,
            this.accountantPortalStateService.companyLogoUploaded$,
            this.accountantPortalStateService.accountantUser$
        ),
        tap(([_, uploadLogoAndPdf, companyPdfUploaded, companyLogoUploaded, accountantUser]) => {
            if (
                this.hasLogoAndPdfUploaded(uploadLogoAndPdf, companyPdfUploaded, companyLogoUploaded) &&
                accountantUser.isUnconnectedAccountantUser
            ) {
                window.location.replace('/clients');
            }
        })
    );

    @Effect({ dispatch: false })
    readonly createNewCompanyFail$: Observable<any> = this.actions$.pipe(
        ofType<CompanyActions.CreateNewCompanyFailAction>(CompanyActions.CREATE_NEW_COMPANY_FAIL),
        tap(errorResponse => this.portalMessageService.showErrorMessage(errorResponse.payload.status))
    );

    @Effect()
    readonly fetchCompanies$: Observable<any> = this.actions$.pipe(
        ofType<CompanyActions.LoadCollectionAction>(CompanyActions.LOAD_COLLECTION),
        switchMap(() =>
            this.apiService
                .getCompanies()
                .pipe(
                    map(companies => new CompanyActions.LoadCollectionSuccessAction(companies)),
                    catchError(errorResponse => of(new CompanyActions.LoadCollectionFailAction(errorResponse)))
                )
        )
    );

    constructor(
        private actions$: Actions,
        private apiService: ApiService,
        private accountantPortalStateService: AccountantPortalStateService,
        private portalMessageService: PortalMessageService
    ) {
        super(accountantPortalStateService);
    }

    private hasLogoAndPdfUploaded(uploadLogoAndPdf: boolean, companyPdfUploaded: boolean, companyLogoUploaded: boolean): boolean {
        return (
            (uploadLogoAndPdf && companyPdfUploaded && companyLogoUploaded) ||
            (!uploadLogoAndPdf && companyPdfUploaded) ||
            (!uploadLogoAndPdf && companyLogoUploaded)
        );
    }
}
