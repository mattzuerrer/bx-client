import { Injectable } from '@angular/core';
import { EffectsService } from '@bx-client/ngrx';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { ServicesActions } from '../actions';
import { AccountantPortalStateService } from '../services';

@Injectable()
export class ServicesEffects extends EffectsService {
    @Effect({ dispatch: false })
    readonly redirectToPartnerService$: Observable<any> = this.actions$.pipe(
        ofType<ServicesActions.RedirectToPartnerServiceAction>(ServicesActions.REDIRECT_TO_PARTNER_SERVICE),
        tap(action => window.open(`api/partners/open-${action.payload}`))
    );

    constructor(private actions$: Actions, accountantPortalStateService: AccountantPortalStateService) {
        super(accountantPortalStateService);
    }
}
