import { HttpErrorResponse } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { plainToClass } from 'class-transformer';
import { cold } from 'jasmine-marbles';
import { noop, Observable, of, throwError } from 'rxjs';

import { AccountantActions, UserActions } from '../actions';
import { accountants, company } from '../fixtures';
import { Accountant } from '../models';
import { AccountantPortalStateService, ApiService, PortalMessageService } from '../services';

import { AccountantsEffects } from './accountants.effects';

const apiServiceStub = {
    getAccountants: () => noop(),
    changeRole: () => noop(),
    getAccountantUser: () => noop(),
    inviteAccountant: () => noop(),
    removeAccountant: () => noop(),
    getAccountant: () => noop(),
    newAccountantFromInvitation: () => noop(),
    acceptAccountantInvitation: () => noop(),
    rejectAccountantInvitation: () => noop(),
    revokeAccountantInvitation: () => noop(),
    reinviteAccountant: () => noop(),
    joinExistingCompany: () => noop()
};

const stateServiceStub = {
    dispatch: () => noop(),
    tokenUser$: of({ token: 'token' }),
    accountantUser$: of(accountants[0])
};

const portalMessageServiceStub = {
    showSuccessMessage: () => noop(),
    showErrorMessage: () => noop()
};

const httpErrorPayload = new HttpErrorResponse({
    error: 'Access Forbidden',
    status: 500
});

const accountantToInvite = {
    firstName: 'John',
    lastName: 'Doe',
    email: 'johndoe@email.com',
    role: 'accountant',
    language: 'en_GB'
};

const invitedAccountant = plainToClass(Accountant, {
    id: 1,
    firstName: 'John',
    lastName: 'Doe',
    email: 'johndoe@email.com',
    role: 'accountant',
    salutationType: 'male',
    language: 'en_GB',
    loginUserId: 12,
    onboardingStatus: 'finished',
    companyId: 1,
    invitedById: null,
    inviteStatus: 'accepted',
    createdAt: new Date(),
    updatedAt: new Date(),
    profileImageUrl: 'http://my.bexio.bx/img/profile_picture/CDQRyt_Sr7qaDIZWlShoxg.png',
    roleChangeInProgress: false
});

describe('Accountants effects', () => {
    let accountantsEffects: AccountantsEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<AccountantsEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                AccountantsEffects,
                {
                    provide: ApiService,
                    useValue: apiServiceStub
                },
                {
                    provide: AccountantPortalStateService,
                    useValue: stateServiceStub
                },
                {
                    provide: PortalMessageService,
                    useValue: portalMessageServiceStub
                },
                provideMockActions(() => actions)
            ]
        });

        accountantsEffects = TestBed.get(AccountantsEffects);
        metadata = getEffectsMetadata(accountantsEffects);
    });

    it(
        'should return LOAD_SUCCESS on LOAD if getAccountants is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getAccountants').and.returnValue(of(accountants));

            actions = cold('a', { a: new AccountantActions.LoadAction() });
            const expected = cold('b', { b: new AccountantActions.LoadSuccessAction(accountants) });

            expect(accountantsEffects.fetch$).toBeObservable(expected);
            expect(apiService.getAccountants).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_FAIL on LOAD if getClientInfo fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getAccountants').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new AccountantActions.LoadAction() });
            const expected = cold('b', { b: new AccountantActions.LoadFailAction() });

            expect(accountantsEffects.fetch$).toBeObservable(expected);
            expect(apiService.getAccountants).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return CHANGE_ROLE_SUCCESS on CHANGE_ROLE if changeRole is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'changeRole').and.returnValue(of(accountants[0]));

            actions = cold('a', { a: new AccountantActions.ChangeRoleAction({ id: 1, role: 'owner' }) });
            const expected = cold('b', { b: new AccountantActions.ChangeRoleSuccessAction(accountants[0]) });

            expect(accountantsEffects.changeRole$).toBeObservable(expected);
            expect(apiService.changeRole).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showSuccessMessage on CHANGE_ROLE_SUCCESS action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showSuccessMessage').and.callThrough();

            actions = cold('a', { a: new AccountantActions.ChangeRoleSuccessAction(accountants[0]) });
            const expected = cold('b', { b: new AccountantActions.ChangeRoleSuccessAction(accountants[0]) });

            expect(accountantsEffects.changeRoleSuccess$).toBeObservable(expected);
            expect(portalMessageService.showSuccessMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return CHANGE_ROLE_FAIL on CHANGE_ROLE if changeRole fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'changeRole').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new AccountantActions.ChangeRoleAction({ id: 1, role: 'owner' }) });
            const expected = cold('b', { b: new AccountantActions.ChangeRoleFailAction({ id: 1, error: httpErrorPayload }) });

            expect(accountantsEffects.changeRole$).toBeObservable(expected);
            expect(apiService.changeRole).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showErrorMessage on CHANGE_ROLE_FAIL action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showErrorMessage').and.callThrough();

            actions = cold('a', { a: new AccountantActions.ChangeRoleFailAction({ id: 1, error: httpErrorPayload }) });
            const expected = cold('b', { b: new AccountantActions.ChangeRoleFailAction({ id: 1, error: httpErrorPayload }) });

            expect(accountantsEffects.changeRoleFail$).toBeObservable(expected);
            expect(portalMessageService.showErrorMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_ACCOUNTANT_USER_SUCCESS on LOAD_ACCOUNTANT_USER if getAccountantUser is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getAccountantUser').and.returnValue(of(accountants[0]));

            actions = cold('a', { a: new UserActions.LoadAccountantUserAction() });
            const expected = cold('b', { b: new UserActions.LoadAccountantUserSuccessAction(accountants[0]) });

            expect(accountantsEffects.fetchAccountantUser$).toBeObservable(expected);
            expect(apiService.getAccountantUser).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_ACCOUNTANT_USER_FAIL on LOAD_ACCOUNTANT_USER if getAccountantUser fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getAccountantUser').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new UserActions.LoadAccountantUserAction() });
            const expected = cold('b', { b: new UserActions.LoadAccountantUserFailAction() });

            expect(accountantsEffects.fetchAccountantUser$).toBeObservable(expected);
            expect(apiService.getAccountantUser).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return ADD_NEW_ACCOUNTANT_SUCCESS on ADD_NEW_ACCOUNTANT if inviteAccountant is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'inviteAccountant').and.returnValue(of(invitedAccountant));

            actions = cold('a', { a: new AccountantActions.AddNewAccountantAction(accountantToInvite) });
            const expected = cold('b', { b: new AccountantActions.AddNewAccountantSuccessAction(invitedAccountant) });

            expect(accountantsEffects.addNewAccountant$).toBeObservable(expected);
            expect(apiService.inviteAccountant).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showSuccessMessage on ADD_NEW_ACCOUNTANT_SUCCESS action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showSuccessMessage').and.callThrough();

            actions = cold('a', { a: new AccountantActions.AddNewAccountantSuccessAction(accountantToInvite) });
            const expected = cold('b', { b: new AccountantActions.AddNewAccountantSuccessAction(accountantToInvite) });

            expect(accountantsEffects.addNewAccountantSuccess$).toBeObservable(expected);
            expect(portalMessageService.showSuccessMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return ADD_NEW_ACCOUNTANT_FAIL on ADD_NEW_ACCOUNTANT if inviteAccountant fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'inviteAccountant').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new AccountantActions.AddNewAccountantAction(accountantToInvite) });
            const expected = cold('b', { b: new AccountantActions.AddNewAccountantFailAction(httpErrorPayload) });

            expect(accountantsEffects.addNewAccountant$).toBeObservable(expected);
            expect(apiService.inviteAccountant).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showErrorMessage on ADD_NEW_ACCOUNTANT_FAIL action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showErrorMessage').and.callThrough();

            actions = cold('a', { a: new AccountantActions.AddNewAccountantFailAction(httpErrorPayload) });
            const expected = cold('b', { b: new AccountantActions.AddNewAccountantFailAction(httpErrorPayload) });

            expect(accountantsEffects.addNewAccountantFail$).toBeObservable(expected);
            expect(portalMessageService.showErrorMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return REMOVE_SUCCESS on REMOVE if removeAccountant is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'removeAccountant').and.returnValue(of(null));

            actions = cold('a', { a: new AccountantActions.RemoveAction(accountants[0]) });
            const expected = cold('b', { b: new AccountantActions.RemoveSuccessAction(accountants[0]) });

            expect(accountantsEffects.removeAccountant$).toBeObservable(expected);
            expect(apiService.removeAccountant).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showSuccessMessage on REMOVE_SUCCESS action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showSuccessMessage').and.callThrough();

            actions = cold('a', { a: new AccountantActions.RemoveSuccessAction(accountants[0]) });
            const expected = cold('b', { b: new AccountantActions.RemoveSuccessAction(accountants[0]) });

            expect(accountantsEffects.removeAccountantSuccess$).toBeObservable(expected);
            expect(portalMessageService.showSuccessMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return REMOVE_FAIL on REMOVE if removeAccountant fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'removeAccountant').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new AccountantActions.RemoveAction(new Accountant()) });
            const expected = cold('b', { b: new AccountantActions.RemoveFailAction(httpErrorPayload) });

            expect(accountantsEffects.removeAccountant$).toBeObservable(expected);
            expect(apiService.removeAccountant).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showErrorMessage on REMOVE_FAIL action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showErrorMessage').and.callThrough();

            actions = cold('a', { a: new AccountantActions.RemoveFailAction(httpErrorPayload) });
            const expected = cold('b', { b: new AccountantActions.RemoveFailAction(httpErrorPayload) });

            expect(accountantsEffects.removeAccountantFail$).toBeObservable(expected);
            expect(portalMessageService.showErrorMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_ONE_SUCCESS on LOAD_ONE if getAccountant is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getAccountant').and.returnValue(of(accountants[0]));

            actions = cold('a', { a: new AccountantActions.LoadOneAction(accountants[0].id) });
            const expected = cold('b', { b: new AccountantActions.LoadOneSuccessAction(accountants[0]) });

            expect(accountantsEffects.fetchOne$).toBeObservable(expected);
            expect(apiService.getAccountant).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_ONE_FAIL on LOAD_ONE if getAccountant fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getAccountant').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new AccountantActions.LoadOneAction(1) });
            const expected = cold('b', { b: new AccountantActions.LoadOneFailAction(httpErrorPayload) });

            expect(accountantsEffects.fetchOne$).toBeObservable(expected);
            expect(apiService.getAccountant).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return JOIN_EXISTING_COMPANY_SUCCESS if joinExistingCompany is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'joinExistingCompany').and.returnValue(of(company.id));

            actions = cold('a', { a: new AccountantActions.JoinExistingCompanyAction(company.id) });
            const expected = cold('b', { b: new AccountantActions.JoinExistingCompanySuccessAction() });

            expect(accountantsEffects.joinExistingCompany$).toBeObservable(expected);
            expect(apiService.joinExistingCompany).toHaveBeenCalledTimes(1);
            expect(apiService.joinExistingCompany).toHaveBeenCalledWith(company.id, accountants[0].id);
        })
    );

    it(
        'should return JOIN_EXISTING_COMPANY_FAIL if joinExistingCompany fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'joinExistingCompany').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new AccountantActions.JoinExistingCompanyAction(company.id) });
            const expected = cold('b', { b: new AccountantActions.JoinExistingCompanyFailAction(httpErrorPayload) });

            expect(accountantsEffects.joinExistingCompany$).toBeObservable(expected);
            expect(apiService.joinExistingCompany).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showErrorMessage on LOAD_ONE_FAIL action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showErrorMessage').and.callThrough();

            actions = cold('a', { a: new AccountantActions.LoadOneFailAction(httpErrorPayload) });
            const expected = cold('b', { b: new AccountantActions.LoadOneFailAction(httpErrorPayload) });

            expect(accountantsEffects.fetchOneFail$).toBeObservable(expected);
            expect(portalMessageService.showErrorMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return NEW_ACCOUNTANT_FROM_INVITATION_FAIL if newAccountantFromInvitation fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'newAccountantFromInvitation').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new AccountantActions.NewAccountantFromInvitationAction({ firstName: 'John', lastName: 'Doe' }) });
            const expected = cold('b', { b: new AccountantActions.NewAccountantFromInvitationFailAction(httpErrorPayload) });

            expect(accountantsEffects.newAccountantFromInvitation$).toBeObservable(expected);
            expect(apiService.newAccountantFromInvitation).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showErrorMessage on NEW_ACCOUNTANT_FROM_INVITATION_FAIL action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showErrorMessage').and.callThrough();

            actions = cold('a', { a: new AccountantActions.NewAccountantFromInvitationFailAction(httpErrorPayload) });
            const expected = cold('b', { b: new AccountantActions.NewAccountantFromInvitationFailAction(httpErrorPayload) });

            expect(accountantsEffects.newAccountantFromInvitationFail$).toBeObservable(expected);
            expect(portalMessageService.showErrorMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return ACCEPT_ACCOUNTANT_INVITATION_SUCCESS on ACCEPT_ACCOUNTANT_INVITATION if acceptAccountantInvitation is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'acceptAccountantInvitation').and.returnValue(of(accountants[0]));

            actions = cold('a', { a: new AccountantActions.AcceptAccountantInvitationAction(accountants[0].id) });
            const expected = cold('b', { b: new AccountantActions.AcceptAccountantInvitationSuccessAction(accountants[0]) });

            expect(accountantsEffects.acceptAccountantInvitation$).toBeObservable(expected);
            expect(apiService.acceptAccountantInvitation).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return ACCEPT_ACCOUNTANT_INVITATION_FAIL on ACCEPT_ACCOUNTANT_INVITATION if acceptAccountantInvitation fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'acceptAccountantInvitation').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new AccountantActions.AcceptAccountantInvitationAction(accountants[0].id) });
            const expected = cold('b', { b: new AccountantActions.AcceptAccountantInvitationFailAction(httpErrorPayload) });

            expect(accountantsEffects.acceptAccountantInvitation$).toBeObservable(expected);
            expect(apiService.acceptAccountantInvitation).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return REJECT_ACCOUNTANT_INVITATION_SUCCESS on REJECT_ACCOUNTANT_INVITATION if rejectAccountantInvitation is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'rejectAccountantInvitation').and.returnValue(of(accountants[0]));

            actions = cold('a', { a: new AccountantActions.RejectAccountantInvitationAction(accountants[0].id) });
            const expected = cold('b', { b: new AccountantActions.RejectAccountantInvitationSuccessAction(accountants[0]) });

            expect(accountantsEffects.rejectAccountantInvitation$).toBeObservable(expected);
            expect(apiService.rejectAccountantInvitation).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return REJECT_ACCOUNTANT_INVITATION_FAIL on REJECT_ACCOUNTANT_INVITATION if rejectAccountantInvitation fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'rejectAccountantInvitation').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new AccountantActions.RejectAccountantInvitationAction(accountants[0].id) });
            const expected = cold('b', { b: new AccountantActions.RejectAccountantInvitationFailAction(httpErrorPayload) });

            expect(accountantsEffects.rejectAccountantInvitation$).toBeObservable(expected);
            expect(apiService.rejectAccountantInvitation).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showSuccessMessage on ACCEPT_ACCOUNTANT_INVITATION_SUCCESS action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showSuccessMessage').and.callThrough();

            actions = cold('a', { a: new AccountantActions.AcceptAccountantInvitationSuccessAction(accountants[0]) });
            const expected = cold('b', { b: new AccountantActions.AcceptAccountantInvitationSuccessAction(accountants[0]) });

            expect(accountantsEffects.acceptAccountantInvitationSuccess$).toBeObservable(expected);
            expect(portalMessageService.showSuccessMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showErrorMessage on ACCEPT_ACCOUNTANT_INVITATION_FAIL action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showErrorMessage').and.callThrough();

            actions = cold('a', { a: new AccountantActions.AcceptAccountantInvitationFailAction(httpErrorPayload) });
            const expected = cold('b', { b: new AccountantActions.AcceptAccountantInvitationFailAction(httpErrorPayload) });

            expect(accountantsEffects.acceptAccountantInvitationFail$).toBeObservable(expected);
            expect(portalMessageService.showErrorMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showSuccessMessage on REJECT_ACCOUNTANT_INVITATION_SUCCESS action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showSuccessMessage').and.callThrough();

            actions = cold('a', { a: new AccountantActions.RejectAccountantInvitationSuccessAction(accountants[0]) });
            const expected = cold('b', { b: new AccountantActions.RejectAccountantInvitationSuccessAction(accountants[0]) });

            expect(accountantsEffects.rejectAccountantInvitationSuccess$).toBeObservable(expected);
            expect(portalMessageService.showSuccessMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showErrorMessage on REJECT_ACCOUNTANT_INVITATION_FAIL action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showErrorMessage').and.callThrough();

            actions = cold('a', { a: new AccountantActions.RejectAccountantInvitationFailAction(httpErrorPayload) });
            const expected = cold('b', { b: new AccountantActions.RejectAccountantInvitationFailAction(httpErrorPayload) });

            expect(accountantsEffects.rejectAccountantInvitationFail$).toBeObservable(expected);
            expect(portalMessageService.showErrorMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return REVOKE_ACCOUNTANT_INVITATION_SUCCESS on REVOKE_ACCOUNTANT_INVITATION if reinviteAccountant is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'revokeAccountantInvitation').and.returnValue(of(accountants[0]));

            actions = cold('a', { a: new AccountantActions.RevokeAccountantInvitationAction(accountants[0]) });
            const expected = cold('b', { b: new AccountantActions.RevokeAccountantInvitationSuccessAction(accountants[0]) });

            expect(accountantsEffects.revokeAccountantInvitation$).toBeObservable(expected);
            expect(apiService.revokeAccountantInvitation).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return REVOKE_ACCOUNTANT_INVITATION_FAIL on REVOKE_ACCOUNTANT_INVITATION if rejectAccountantInvitation fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'revokeAccountantInvitation').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new AccountantActions.RevokeAccountantInvitationAction(accountants[0]) });
            const expected = cold('b', { b: new AccountantActions.RevokeAccountantInvitationFailAction(httpErrorPayload) });

            expect(accountantsEffects.revokeAccountantInvitation$).toBeObservable(expected);
            expect(apiService.revokeAccountantInvitation).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showSuccessMessage on REVOKE_ACCOUNTANT_INVITATION_SUCCESS action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showSuccessMessage').and.callThrough();

            actions = cold('a', { a: new AccountantActions.RevokeAccountantInvitationSuccessAction(accountants[0]) });
            const expected = cold('b', { b: new AccountantActions.RevokeAccountantInvitationSuccessAction(accountants[0]) });

            expect(accountantsEffects.revokeAccountantInvitationSuccess$).toBeObservable(expected);
            expect(portalMessageService.showSuccessMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showErrorMessage on REVOKE_ACCOUNTANT_INVITATION_FAIL action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showErrorMessage').and.callThrough();

            actions = cold('a', { a: new AccountantActions.RevokeAccountantInvitationFailAction(httpErrorPayload) });
            const expected = cold('b', { b: new AccountantActions.RevokeAccountantInvitationFailAction(httpErrorPayload) });

            expect(accountantsEffects.revokeAccountantInvitationFail$).toBeObservable(expected);
            expect(portalMessageService.showErrorMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return RESEND_ACCOUNTANT_INVITATION_SUCCESS on RESEND_ACCOUNTANT_INVITATION if reinviteAccountant is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'reinviteAccountant').and.returnValue(of(accountants[0]));

            actions = cold('a', { a: new AccountantActions.ResendAccountantInvitationAction(accountants[0].id) });
            const expected = cold('b', { b: new AccountantActions.ResendAccountantInvitationSuccessAction(accountants[0]) });

            expect(accountantsEffects.resendAccountantInvitation$).toBeObservable(expected);
            expect(apiService.reinviteAccountant).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return RESEND_ACCOUNTANT_INVITATION_FAIL on RESEND_ACCOUNTANT_INVITATION if rejectAccountantInvitation fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'reinviteAccountant').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new AccountantActions.ResendAccountantInvitationAction(accountants[0].id) });
            const expected = cold('b', { b: new AccountantActions.ResendAccountantInvitationFailAction(httpErrorPayload) });

            expect(accountantsEffects.resendAccountantInvitation$).toBeObservable(expected);
            expect(apiService.reinviteAccountant).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showSuccessMessage on RESEND_ACCOUNTANT_INVITATION_SUCCESS action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showSuccessMessage').and.callThrough();

            actions = cold('a', { a: new AccountantActions.ResendAccountantInvitationSuccessAction(accountants[0]) });
            const expected = cold('b', { b: new AccountantActions.ResendAccountantInvitationSuccessAction(accountants[0]) });

            expect(accountantsEffects.resendAccountantInvitationSuccess$).toBeObservable(expected);
            expect(portalMessageService.showSuccessMessage).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should call showErrorMessage on RESEND_ACCOUNTANT_INVITATION_FAIL action',
        inject([PortalMessageService], portalMessageService => {
            spyOn(portalMessageService, 'showErrorMessage').and.callThrough();

            actions = cold('a', { a: new AccountantActions.ResendAccountantInvitationFailAction(httpErrorPayload) });
            const expected = cold('b', { b: new AccountantActions.ResendAccountantInvitationFailAction(httpErrorPayload) });

            expect(accountantsEffects.resendAccountantInvitationFail$).toBeObservable(expected);
            expect(portalMessageService.showErrorMessage).toHaveBeenCalledTimes(1);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetch$).toEqual({ dispatch: true });
        expect(metadata.changeRole$).toEqual({ dispatch: true });
        expect(metadata.fetchAccountantUser$).toEqual({ dispatch: true });
        expect(metadata.addNewAccountant$).toEqual({ dispatch: true });
        expect(metadata.removeAccountant$).toEqual({ dispatch: true });
        expect(metadata.changeRoleSuccess$).toEqual({ dispatch: false });
        expect(metadata.changeRoleFail$).toEqual({ dispatch: false });
        expect(metadata.addNewAccountantSuccess$).toEqual({ dispatch: false });
        expect(metadata.addNewAccountantFail$).toEqual({ dispatch: false });
        expect(metadata.removeAccountantSuccess$).toEqual({ dispatch: false });
        expect(metadata.removeAccountantFail$).toEqual({ dispatch: false });
        expect(metadata.fetchOne$).toEqual({ dispatch: true });
        expect(metadata.fetchOneFail$).toEqual({ dispatch: false });
        expect(metadata.newAccountantFromInvitation$).toEqual({ dispatch: true });
        expect(metadata.newAccountantFromInvitationFail$).toEqual({ dispatch: false });
        expect(metadata.acceptAccountantInvitation$).toEqual({ dispatch: true });
        expect(metadata.rejectAccountantInvitation$).toEqual({ dispatch: true });
        expect(metadata.acceptAccountantInvitationSuccess$).toEqual({ dispatch: false });
        expect(metadata.acceptAccountantInvitationFail$).toEqual({ dispatch: false });
        expect(metadata.rejectAccountantInvitationSuccess$).toEqual({ dispatch: false });
        expect(metadata.rejectAccountantInvitationFail$).toEqual({ dispatch: false });
        expect(metadata.revokeAccountantInvitation$).toEqual({ dispatch: true });
        expect(metadata.resendAccountantInvitation$).toEqual({ dispatch: true });
        expect(metadata.joinExistingCompany$).toEqual({ dispatch: true });
    });
});
