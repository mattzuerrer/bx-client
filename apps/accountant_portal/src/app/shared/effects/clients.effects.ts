import { Injectable } from '@angular/core';
import { EffectsService } from '@bx-client/ngrx';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { forkJoin, Observable, of } from 'rxjs';
import { catchError, exhaustMap, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { AccountantActions, ClientsActions } from '../actions';
import { ClientAccess } from '../models';
import { AccountantPortalStateService, ApiService, PortalMessageService } from '../services';

@Injectable()
export class ClientsEffects extends EffectsService {
    @Effect()
    readonly fetch$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.LoadAction | ClientsActions.AcceptSuccessAction>(ClientsActions.LOAD, ClientsActions.ACCEPT_SUCCESS),
        withLatestFrom(this.accountantPortalStateService.accessPermissions$),
        switchMap(([_, permissions]) =>
            this.apiService
                .getClients(permissions.isOwner)
                .pipe(
                    map(clients => new ClientsActions.LoadSuccessAction(clients)),
                    catchError(() => of(new ClientsActions.LoadFailAction()))
                )
        )
    );

    @Effect({ dispatch: false })
    readonly getLoginLink$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.GetLoginLinkAction>(ClientsActions.GET_LOGIN_LINK),
        switchMap(action =>
            this.apiService.getClientLoginLink(action.payload).pipe(
                tap(link => window.location.replace(link.location)),
                catchError(errorResponse => {
                    this.portalMessageService.showErrorMessage(errorResponse.status);
                    return null;
                })
            )
        )
    );

    @Effect()
    readonly reinvite$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.ReinviteAction>(ClientsActions.REINVITE),
        switchMap(action =>
            this.apiService
                .reinviteClientToAccountant(action.payload)
                .pipe(
                    map(() => new ClientsActions.ReinviteSuccessAction()),
                    catchError(errorResponse => of(new ClientsActions.ReinviteFailAction(errorResponse)))
                )
        )
    );

    @Effect()
    readonly connect$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.ConnectAction>(ClientsActions.CONNECT),
        exhaustMap(action => {
            const result: Array<Observable<ClientAccess>> = new Array<Observable<ClientAccess>>();
            for (const accountant of action.payload.ids) {
                const access = { clientId: action.payload.clientId, id: accountant.id };
                result.push(this.apiService.connectClientAccountant(access));
            }

            return forkJoin(result).pipe(
                map(response => {
                    for (const access of response) {
                        this.stateService.dispatch(new AccountantActions.LoadOneAction(access.accountantId));
                        this.stateService.dispatch(new ClientsActions.LoadAccessAction(access.clientId));
                    }

                    return new ClientsActions.ConnectSuccessAction();
                }),
                catchError(errorResponse => of(new ClientsActions.ConnectFailAction(errorResponse)))
            );
        })
    );

    @Effect()
    readonly disconnect$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.DisconnectAction>(ClientsActions.DISCONNECT),
        switchMap(action =>
            this.apiService
                .disconnectClientAccountant(action.payload)
                .pipe(
                    map(response => new ClientsActions.DisconnectSuccessAction(response)),
                    catchError(errorResponse => of(new ClientsActions.DisconnectFailAction(errorResponse)))
                )
        )
    );

    @Effect()
    readonly reject$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.RejectAction>(ClientsActions.REJECT),
        switchMap(action =>
            this.apiService
                .rejectClientAccountantInvitation(action.payload)
                .pipe(
                    map(response => new ClientsActions.RejectSuccessAction(response)),
                    catchError(errorResponse => of(new ClientsActions.RejectFailAction(errorResponse)))
                )
        )
    );

    @Effect()
    readonly accept$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.AcceptAction>(ClientsActions.ACCEPT),
        switchMap(action =>
            this.apiService
                .acceptClientAccountantInvitation(action.payload)
                .pipe(
                    map(response => new ClientsActions.AcceptSuccessAction(response)),
                    catchError(errorResponse => of(new ClientsActions.AcceptFailAction(errorResponse)))
                )
        )
    );

    @Effect()
    readonly loadAccess$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.LoadAccessAction>(ClientsActions.LOAD_ACCESS),
        switchMap(action =>
            this.apiService
                .getClientAccess(action.payload)
                .pipe(
                    map(response => new ClientsActions.LoadAccessSuccessAction(response)),
                    catchError(errorResponse => of(new ClientsActions.LoadAccessFailAction(errorResponse)))
                )
        )
    );

    @Effect()
    readonly loadKeyPerformanceIndicators$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.LoadKeyPerformanceIndicatorsAction>(ClientsActions.LOAD_KEY_PERFORMANCE_INDICATORS),
        switchMap(action =>
            this.apiService
                .loadKeyPerformanceIndicators(action.payload)
                .pipe(
                    map(
                        keyPerformanceIndicators =>
                            new ClientsActions.LoadKeyPerformanceIndicatorsSuccessAction({ id: action.payload, keyPerformanceIndicators })
                    ),
                    catchError(errorResponse => of(new ClientsActions.LoadKeyPerformanceIndicatorsFailAction(errorResponse)))
                )
        )
    );

    // Success and failure handling
    @Effect({ dispatch: false })
    readonly reinviteSuccess$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.ReinviteSuccessAction>(ClientsActions.REINVITE_SUCCESS),
        tap(() => this.portalMessageService.showSuccessMessage('accountant_portal.messages.success.clients.reinvite'))
    );

    @Effect({ dispatch: false })
    readonly connectSuccess$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.ConnectSuccessAction>(ClientsActions.CONNECT_SUCCESS),
        tap(() => this.portalMessageService.showSuccessMessage('accountant_portal.messages.success.clients.connect'))
    );

    @Effect()
    readonly disconnectSuccess$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.DisconnectSuccessAction>(ClientsActions.DISCONNECT_SUCCESS),
        tap(action => this.stateService.dispatch(new AccountantActions.LoadOneAction(action.payload.accountantId))),
        map(action => new ClientsActions.LoadAccessAction(action.payload.clientId)),
        tap(() => this.portalMessageService.showSuccessMessage('accountant_portal.messages.success.clients.disconnect'))
    );

    @Effect()
    readonly rejectSuccess$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.RejectSuccessAction>(ClientsActions.REJECT_SUCCESS),
        tap(action => {
            if (action.payload.client && action.payload.client.name) {
                this.portalMessageService.showSuccessMessage('accountant_portal.messages.success.clients.reject_pending_client_invite', {
                    clientName: action.payload.client.name
                });
            } else {
                this.portalMessageService.showSuccessMessage('accountant_portal.messages.success.clients.reject');
            }
        }),
        tap(action => this.stateService.dispatch(new AccountantActions.LoadOneAction(action.payload.accountantId))),
        map(action => new ClientsActions.LoadAccessAction(action.payload.clientId))
    );

    @Effect({ dispatch: false })
    readonly acceptSuccess$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.AcceptSuccessAction>(ClientsActions.ACCEPT_SUCCESS),
        tap(action =>
            this.portalMessageService.showSuccessMessage('accountant_portal.messages.success.clients.accept_pending_client_invite', {
                clientName: action.payload.client.name
            })
        )
    );

    @Effect({ dispatch: false })
    readonly reinviteFail$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.ReinviteFailAction>(ClientsActions.REINVITE_FAIL),
        tap(action => this.portalMessageService.showErrorMessage(action.payload.status))
    );

    @Effect({ dispatch: false })
    readonly connectFail$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.ConnectFailAction>(ClientsActions.CONNECT_FAIL),
        tap(action => this.portalMessageService.showErrorMessage(action.payload.status))
    );

    @Effect({ dispatch: false })
    readonly disconnectFail$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.DisconnectFailAction>(ClientsActions.DISCONNECT_FAIL),
        tap(action => this.portalMessageService.showErrorMessage(action.payload.status))
    );

    @Effect({ dispatch: false })
    readonly rejectFail$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.RejectFailAction>(ClientsActions.REJECT_FAIL),
        tap(action => this.portalMessageService.showErrorMessage(action.payload.status))
    );

    @Effect({ dispatch: false })
    readonly loadAccessFail$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.LoadAccessFailAction>(ClientsActions.LOAD_ACCESS_FAIL),
        tap(action => this.portalMessageService.showErrorMessage(action.payload.status))
    );

    @Effect()
    readonly create$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.CreateNewAction>(ClientsActions.CREATE_NEW),
        switchMap(() =>
            this.apiService
                .createClient()
                .pipe(
                    map(client => new ClientsActions.GetLoginLinkAction(client.id)),
                    catchError(errorResponse => of(new ClientsActions.CreateNewFailActon(errorResponse)))
                )
        )
    );

    @Effect()
    readonly createFail$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.CreateNewFailActon>(ClientsActions.CREATE_NEW_FAIL),
        tap(action => this.portalMessageService.showErrorMessage(action.payload.status))
    );

    @Effect()
    readonly fetchInvites$: Observable<any> = this.actions$.pipe(
        ofType<ClientsActions.LoadPendingInvitesAction | ClientsActions.RejectSuccessAction | ClientsActions.AcceptSuccessAction>(
            ClientsActions.LOAD_PENDING_INVITES,
            ClientsActions.REJECT_SUCCESS,
            ClientsActions.ACCEPT_SUCCESS
        ),
        switchMap(() =>
            this.apiService
                .getPendingClientInvites()
                .pipe(
                    map(pendingClientInvites => new ClientsActions.LoadPendingInvitesSuccessAction(pendingClientInvites)),
                    catchError(() => of(new ClientsActions.LoadPendingInvitesFailAction()))
                )
        )
    );

    constructor(
        private actions$: Actions,
        private apiService: ApiService,
        public accountantPortalStateService: AccountantPortalStateService,
        private portalMessageService: PortalMessageService
    ) {
        super(accountantPortalStateService);
    }
}
