import { TestBed } from '@angular/core/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { Observable } from 'rxjs';

import { ServicesActions } from '../actions';
import { AccountantPortalStateService } from '../services';

import { ServicesEffects } from './services.effects';

const stateServiceStub = {};

describe('Services effects', () => {
    let servicesEffects: ServicesEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<ServicesEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                ServicesEffects,
                {
                    provide: AccountantPortalStateService,
                    useValue: stateServiceStub
                },
                provideMockActions(() => actions)
            ]
        });

        servicesEffects = TestBed.get(ServicesEffects);
        metadata = getEffectsMetadata(servicesEffects);
    });

    it('should pass trough without changing', () => {
        actions = cold('a', { a: new ServicesActions.RedirectToPartnerServiceAction('example-service') });
        const expected = cold('b', { b: new ServicesActions.RedirectToPartnerServiceAction('example-service') });

        expect(servicesEffects.redirectToPartnerService$).toBeObservable(expected);
    });

    it('should test metadata', () => {
        expect(metadata.redirectToPartnerService$).toEqual({ dispatch: false });
    });
});
