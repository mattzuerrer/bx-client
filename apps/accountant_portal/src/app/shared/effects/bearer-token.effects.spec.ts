import { HttpErrorResponse } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { noop, Observable, of, throwError } from 'rxjs';

import { BearerTokenActions } from '../actions';
import { bearerTokenUser } from '../fixtures';
import { AccountantPortalStateService, ApiService } from '../services';

import { BearerTokenEffects } from './bearer-token.effects';

const testToken = 'my-test-bearer-token';

const apiServiceStub = {
    getClients: () => noop(),
    validateAccountantToken: () => noop()
};

const stateServiceStub = {
    accessPermissions$: of({ isOwner: false })
};

const httpErrorPayload = new HttpErrorResponse({
    error: 'Unprocessable Entity',
    status: 422
});

describe('Bearer Token effects', () => {
    let bearerTokenEffects: BearerTokenEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<BearerTokenEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                BearerTokenEffects,
                {
                    provide: ApiService,
                    useValue: apiServiceStub
                },
                {
                    provide: AccountantPortalStateService,
                    useValue: stateServiceStub
                },
                provideMockActions(() => actions)
            ]
        });

        bearerTokenEffects = TestBed.get(BearerTokenEffects);
        metadata = getEffectsMetadata(bearerTokenEffects);
    });

    it(
        'should return VERIFY_BEARER_TOKEN_SUCCESS on VERIFY_BEARER_TOKEN if validateAccountantToken is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'validateAccountantToken').and.returnValue(of(bearerTokenUser));

            actions = cold('a', { a: new BearerTokenActions.VerifyBearerTokenAction(testToken) });
            const expected = cold('b', { b: new BearerTokenActions.VerifyBearerTokenSuccessAction(bearerTokenUser) });

            expect(bearerTokenEffects.bearerToken$).toBeObservable(expected);
            expect(apiService.validateAccountantToken).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return VERIFY_BEARER_TOKEN_FAIL on VERIFY_BEARER_TOKEN if validateAccountantToken fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'validateAccountantToken').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new BearerTokenActions.VerifyBearerTokenAction(testToken) });
            const expected = cold('b', { b: new BearerTokenActions.VerifyBearerTokenFailAction() });

            expect(bearerTokenEffects.bearerToken$).toBeObservable(expected);
            expect(apiService.validateAccountantToken).toHaveBeenCalledTimes(1);
        })
    );

    it('should test metadata', () => {
        expect(metadata.bearerToken$).toEqual({ dispatch: true });
        expect(metadata.bearerTokenFail$).toEqual({ dispatch: false });
    });
});
