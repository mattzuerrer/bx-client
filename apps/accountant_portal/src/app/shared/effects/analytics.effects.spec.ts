import { inject, TestBed } from '@angular/core/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { noop, Observable, of, throwError } from 'rxjs';

import { AnalyticsActions } from '../actions';
import { googleAnalytics } from '../fixtures';
import { AccountantPortalStateService, ApiService } from '../services';

import { AnalyticsEffects } from './analytics.effects';

const apiServiceStub = {
    getGoogleAnalyticsConfiguration: () => noop()
};

const stateServiceStub = {};

describe('Analytics effects', () => {
    let analyticsEffects: AnalyticsEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<AnalyticsEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                AnalyticsEffects,
                {
                    provide: ApiService,
                    useValue: apiServiceStub
                },
                {
                    provide: AccountantPortalStateService,
                    useValue: stateServiceStub
                },
                provideMockActions(() => actions)
            ]
        });

        analyticsEffects = TestBed.get(AnalyticsEffects);
        metadata = getEffectsMetadata(analyticsEffects);
    });

    it(
        'should return LOAD_GOOGLE_ANALYTICS_SUCCESS on LOAD_GOOGLE_ANALYTICS if getGoogleAnalyticsConfiguration is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getGoogleAnalyticsConfiguration').and.returnValue(of(googleAnalytics));

            actions = cold('a', { a: new AnalyticsActions.LoadGoogleAnalyticsAction() });
            const expected = cold('b', { b: new AnalyticsActions.LoadGoogleAnalyticsSuccessAction(googleAnalytics) });

            expect(analyticsEffects.fetch$).toBeObservable(expected);
            expect(apiService.getGoogleAnalyticsConfiguration).toHaveBeenCalledTimes(1);
        })
    );

    it(
        'should return LOAD_GOOGLE_ANALYTICS_FAIL on LOAD_GOOGLE_ANALYTICS if getGoogleAnalyticsConfiguration fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getGoogleAnalyticsConfiguration').and.returnValue(throwError(null));

            actions = cold('a', { a: new AnalyticsActions.LoadGoogleAnalyticsAction() });
            const expected = cold('b', { b: new AnalyticsActions.LoadGoogleAnalyticsFailAction() });

            expect(analyticsEffects.fetch$).toBeObservable(expected);
            expect(apiService.getGoogleAnalyticsConfiguration).toHaveBeenCalledTimes(1);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetch$).toEqual({ dispatch: true });
    });
});
