import { Injectable } from '@angular/core';
import { EffectsService } from '@bx-client/ngrx';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { AnalyticsActions } from '../actions';
import { AccountantPortalStateService, ApiService } from '../services';

@Injectable()
export class AnalyticsEffects extends EffectsService {
    @Effect()
    readonly fetch$: Observable<any> = this.actions$.pipe(
        ofType<AnalyticsActions.LoadGoogleAnalyticsAction>(AnalyticsActions.LOAD_GOOGLE_ANALYTICS),
        switchMap(() =>
            this.apiService
                .getGoogleAnalyticsConfiguration()
                .pipe(
                    map(configuration => new AnalyticsActions.LoadGoogleAnalyticsSuccessAction(configuration)),
                    catchError(() => of(new AnalyticsActions.LoadGoogleAnalyticsFailAction()))
                )
        )
    );

    constructor(private actions$: Actions, private apiService: ApiService, accountantPortalStateService: AccountantPortalStateService) {
        super(accountantPortalStateService);
    }
}
