import { HttpErrorResponse } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { LanguageActions } from '@bx-client/i18n';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { noop, Observable, of, throwError } from 'rxjs';

import { UserActions } from '../actions';
import { user } from '../fixtures';
import { AccountantPortalStateService, ApiService } from '../services';

import { UserEffects } from './user.effects';

const apiServiceStub = {
    getLoginUser: () => noop()
};

const httpErrorPayload = new HttpErrorResponse({
    error: 'Access Denied',
    status: 403
});

const stateServiceStub = {};

describe('User effects', () => {
    let userEffects: UserEffects;
    let actions: Observable<any>;
    let metadata: EffectsMetadata<UserEffects>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                UserEffects,
                {
                    provide: ApiService,
                    useValue: apiServiceStub
                },
                {
                    provide: AccountantPortalStateService,
                    useValue: stateServiceStub
                },
                provideMockActions(() => actions)
            ]
        });

        userEffects = TestBed.get(UserEffects);
        metadata = getEffectsMetadata(userEffects);
    });

    it(
        'should return LOAD_LOGIN_USER_SUCCESS on LOAD if getLoginUser is successful',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getLoginUser').and.returnValue(of(user));

            actions = cold('a', { a: new UserActions.LoadLoginUserAction() });
            const expected = cold('b', { b: new UserActions.LoadLoginUserSuccessAction(user) });

            expect(userEffects.fetch$).toBeObservable(expected);
            expect(apiService.getLoginUser).toHaveBeenCalledTimes(1);
        })
    );

    it('should return SET_LANGUAGE on LOAD_LOGIN_USER_SUCCESS action', () => {
        actions = cold('a', { a: new UserActions.LoadLoginUserSuccessAction(user) });
        const expected = cold('b', { b: new LanguageActions.SetLanguageAction('en_GB') });

        expect(userEffects.setLanguage$).toBeObservable(expected);
    });

    it('should return SET_TO_BROWSER_LANGUAGE on LOAD_LOGIN_USER_FAIL action', () => {
        actions = cold('a', { a: new UserActions.LoadLoginUserFailAction(httpErrorPayload) });
        const expected = cold('b', { b: new LanguageActions.SetToBrowserLanguageAction() });

        expect(userEffects.setToBrowserLanguage$).toBeObservable(expected);
    });

    it(
        'should return LOAD_LOGIN_USER_FAIL on LOAD if getLoginUser fails',
        inject([ApiService], apiService => {
            spyOn(apiService, 'getLoginUser').and.returnValue(throwError(httpErrorPayload));

            actions = cold('a', { a: new UserActions.LoadLoginUserAction() });
            const expected = cold('b', { b: new UserActions.LoadLoginUserFailAction(httpErrorPayload) });

            expect(userEffects.fetch$).toBeObservable(expected);
            expect(apiService.getLoginUser).toHaveBeenCalledTimes(1);
        })
    );

    it('should test metadata', () => {
        expect(metadata.fetch$).toEqual({ dispatch: true });
        expect(metadata.setLanguage$).toEqual({ dispatch: true });
        expect(metadata.setToBrowserLanguage$).toEqual({ dispatch: true });
    });
});
