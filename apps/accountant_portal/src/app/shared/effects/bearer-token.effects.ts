import { Injectable } from '@angular/core';
import { EffectsService } from '@bx-client/ngrx';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import { BearerTokenActions } from '../actions';
import { AccountantPortalStateService, ApiService } from '../services';

@Injectable()
export class BearerTokenEffects extends EffectsService {
    @Effect()
    readonly bearerToken$: Observable<any> = this.actions$.pipe(
        ofType<BearerTokenActions.VerifyBearerTokenAction>(BearerTokenActions.VERIFY_BEARER_TOKEN),
        switchMap(action =>
            this.apiService
                .validateAccountantToken(action.payload)
                .pipe(
                    map(token => new BearerTokenActions.VerifyBearerTokenSuccessAction(token)),
                    catchError(() => of(new BearerTokenActions.VerifyBearerTokenFailAction()))
                )
        )
    );

    @Effect({ dispatch: false })
    readonly bearerTokenFail$: Observable<any> = this.actions$.pipe(
        ofType<BearerTokenActions.VerifyBearerTokenFailAction>(BearerTokenActions.VERIFY_BEARER_TOKEN_FAIL),
        tap(() => window.location.replace('/403'))
    );

    constructor(
        private actions$: Actions,
        private apiService: ApiService,
        public accountantPortalStateService: AccountantPortalStateService
    ) {
        super(accountantPortalStateService);
    }
}
