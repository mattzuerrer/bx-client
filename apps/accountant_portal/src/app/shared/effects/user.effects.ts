import { Injectable } from '@angular/core';
import { EffectsService } from '@bx-client/ngrx';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { LanguageActions } from '@bx-client/i18n';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { UserActions } from '../actions';
import { AccountantPortalStateService, ApiService } from '../services';

@Injectable()
export class UserEffects extends EffectsService {
    @Effect()
    readonly fetch$: Observable<any> = this.actions$.pipe(
        ofType<UserActions.LoadLoginUserAction>(UserActions.LOAD_LOGIN_USER),
        switchMap(() =>
            this.apiService.getLoginUser().pipe(
                map(user => new UserActions.LoadLoginUserSuccessAction(user)),
                catchError(errorResponse => {
                    return of(new UserActions.LoadLoginUserFailAction(errorResponse));
                })
            )
        )
    );

    @Effect()
    readonly setLanguage$: Observable<any> = this.actions$.pipe(
        ofType<UserActions.LoadLoginUserSuccessAction>(UserActions.LOAD_LOGIN_USER_SUCCESS),
        map(user => new LanguageActions.SetLanguageAction(user.payload.language))
    );

    @Effect()
    readonly setToBrowserLanguage$: Observable<any> = this.actions$.pipe(
        ofType<UserActions.LoadLoginUserFailAction>(UserActions.LOAD_LOGIN_USER_FAIL),
        map(() => new LanguageActions.SetToBrowserLanguageAction())
    );

    @Effect()
    readonly createUserFromExisting$: Observable<any> = this.actions$.pipe(
        ofType<UserActions.CreateAccountantFromExistingUserAction>(UserActions.CREATE_ACCOUNTANT_FROM_EXISTING_USER),
        switchMap(action =>
            this.apiService.createAccountantFromExistingUser(action.payload).pipe(
                map(user => new UserActions.CreateAccountantFromExistingUserSuccessAction(user)),
                catchError(errorResponse => {
                    return of(new UserActions.CreateAccountantFromExistingUserFailAction(errorResponse));
                })
            )
        )
    );

    constructor(private actions$: Actions, private apiService: ApiService, accountantPortalStateService: AccountantPortalStateService) {
        super(accountantPortalStateService);
    }
}
