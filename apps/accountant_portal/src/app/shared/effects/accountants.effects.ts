import { Injectable } from '@angular/core';
import { EffectsService } from '@bx-client/ngrx';
import { RouterActions } from '@bx-client/router';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { AccountantActions, UserActions } from '../actions';
import { AccountantPortalStateService, ApiService, PortalMessageService } from '../services';

@Injectable()
export class AccountantsEffects extends EffectsService {
    @Effect()
    readonly fetch$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.LoadAction | AccountantActions.AddNewAccountantSuccessAction | AccountantActions.RemoveSuccessAction>(
            AccountantActions.LOAD,
            AccountantActions.ADD_NEW_ACCOUNTANT_SUCCESS,
            AccountantActions.REMOVE_SUCCESS
        ),
        switchMap(() =>
            this.apiService
                .getAccountants()
                .pipe(
                    map(accountants => new AccountantActions.LoadSuccessAction(accountants)),
                    catchError(() => of(new AccountantActions.LoadFailAction()))
                )
        )
    );

    @Effect()
    readonly changeRole$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.ChangeRoleAction>(AccountantActions.CHANGE_ROLE),
        switchMap(action =>
            this.apiService
                .changeRole(action.payload)
                .pipe(
                    map(accountant => new AccountantActions.ChangeRoleSuccessAction(accountant)),
                    catchError(errorResponse =>
                        of(new AccountantActions.ChangeRoleFailAction({ id: action.payload.id, error: errorResponse }))
                    )
                )
        )
    );

    @Effect({ dispatch: false })
    readonly changeRoleSuccess$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.ChangeRoleSuccessAction>(AccountantActions.CHANGE_ROLE_SUCCESS),
        tap(() => this.portalMessageService.showSuccessMessage('accountant_portal.messages.success.accountants.change_role'))
    );

    @Effect({ dispatch: false })
    readonly changeRoleFail$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.ChangeRoleFailAction>(AccountantActions.CHANGE_ROLE_FAIL),
        tap(errorResponse => this.portalMessageService.showErrorMessage(errorResponse.payload.error.status))
    );

    @Effect()
    readonly fetchAccountantUser$: Observable<any> = this.actions$.pipe(
        ofType<UserActions.LoadAccountantUserAction | AccountantActions.JoinExistingCompanySuccessAction>(
            UserActions.LOAD_ACCOUNTANT_USER,
            AccountantActions.JOIN_EXISTING_COMPANY_SUCCESS
        ),
        switchMap(() =>
            this.apiService
                .getAccountantUser()
                .pipe(
                    map(accountant => new UserActions.LoadAccountantUserSuccessAction(accountant)),
                    catchError(() => of(new UserActions.LoadAccountantUserFailAction()))
                )
        )
    );

    @Effect()
    readonly addNewAccountant$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.AddNewAccountantAction>(AccountantActions.ADD_NEW_ACCOUNTANT),
        switchMap(action =>
            this.apiService
                .inviteAccountant(action.payload)
                .pipe(
                    map(accountant => new AccountantActions.AddNewAccountantSuccessAction(accountant)),
                    catchError(errorResponse => of(new AccountantActions.AddNewAccountantFailAction(errorResponse)))
                )
        )
    );

    @Effect({ dispatch: false })
    readonly addNewAccountantSuccess$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.AddNewAccountantSuccessAction>(AccountantActions.ADD_NEW_ACCOUNTANT_SUCCESS),
        tap(action =>
            this.portalMessageService.showSuccessMessage('accountant_portal.messages.success.accountants.add_new_accountant', {
                accountant: action.payload.fullName
            })
        )
    );

    @Effect({ dispatch: false })
    readonly addNewAccountantFail$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.AddNewAccountantFailAction>(AccountantActions.ADD_NEW_ACCOUNTANT_FAIL),
        tap(errorResponse => this.portalMessageService.showErrorMessage(errorResponse.payload.status))
    );

    @Effect()
    readonly removeAccountant$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.RemoveAction>(AccountantActions.REMOVE),
        switchMap(action =>
            this.apiService
                .removeAccountant(action.payload.id)
                .pipe(
                    map(() => new AccountantActions.RemoveSuccessAction(action.payload)),
                    catchError(errorResponse => of(new AccountantActions.RemoveFailAction(errorResponse)))
                )
        )
    );

    @Effect({ dispatch: false })
    readonly removeAccountantSuccess$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.RemoveSuccessAction>(AccountantActions.REMOVE_SUCCESS),
        tap(action =>
            this.portalMessageService.showSuccessMessage('accountant_portal.messages.success.accountants.remove_accountant', {
                accountant: action.payload.fullName
            })
        )
    );

    @Effect({ dispatch: false })
    readonly removeAccountantFail$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.RemoveFailAction>(AccountantActions.REMOVE_FAIL),
        tap(errorResponse => this.portalMessageService.showErrorMessage(errorResponse.payload.status))
    );

    @Effect()
    readonly fetchOne$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.LoadOneAction>(AccountantActions.LOAD_ONE),
        switchMap(action =>
            this.apiService
                .getAccountant(action.payload)
                .pipe(
                    map(response => new AccountantActions.LoadOneSuccessAction(response)),
                    catchError(errorResponse => of(new AccountantActions.LoadOneFailAction(errorResponse)))
                )
        )
    );

    @Effect({ dispatch: false })
    readonly fetchOneFail$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.LoadOneFailAction>(AccountantActions.LOAD_ONE_FAIL),
        tap(errorResponse => this.portalMessageService.showErrorMessage(errorResponse.payload.status))
    );

    @Effect()
    readonly joinExistingCompany$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.JoinExistingCompanyAction>(AccountantActions.JOIN_EXISTING_COMPANY),
        withLatestFrom(this.accountantPortalStateService.accountantUser$),
        switchMap(([action, accountantUser]) =>
            this.apiService
                .joinExistingCompany(action.payload, accountantUser.id)
                .pipe(
                    tap(() => this.accountantPortalStateService.dispatch(new RouterActions.Go({ path: ['clients'] }))),
                    map(() => new AccountantActions.JoinExistingCompanySuccessAction()),
                    catchError(errorResponse => of(new AccountantActions.JoinExistingCompanyFailAction(errorResponse)))
                )
        )
    );

    @Effect()
    readonly savePassword$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.SavePasswordAction>(AccountantActions.SAVE_PASSWORD),
        withLatestFrom(this.accountantPortalStateService.tokenUser$),
        switchMap(([action, tokenUser]) =>
            this.apiService
                .savePassword(action.payload, tokenUser.token)
                .pipe(
                    map(() => new AccountantActions.SavePasswordSuccessAction()),
                    catchError(errorResponse => of(new AccountantActions.SavePasswordFailAction(errorResponse)))
                )
        )
    );

    @Effect({ dispatch: false })
    readonly savePasswordSuccess$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.SavePasswordSuccessAction>(AccountantActions.SAVE_PASSWORD_SUCCESS),
        tap(() => window.location.replace('/logout'))
    );

    @Effect()
    readonly newAccountantFromInvitation$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.NewAccountantFromInvitationAction>(AccountantActions.NEW_ACCOUNTANT_FROM_INVITATION),
        withLatestFrom(this.accountantPortalStateService.tokenUser$),
        switchMap(([action, tokenUser]) =>
            this.apiService
                .newAccountantFromInvitation(action.payload, tokenUser.token)
                .pipe(
                    tap(() => window.location.replace('/logout')),
                    catchError(errorResponse => of(new AccountantActions.NewAccountantFromInvitationFailAction(errorResponse)))
                )
        )
    );

    @Effect({ dispatch: false })
    readonly newAccountantFromInvitationFail$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.NewAccountantFromInvitationFailAction>(AccountantActions.NEW_ACCOUNTANT_FROM_INVITATION_FAIL),
        tap(errorResponse => this.portalMessageService.showErrorMessage(errorResponse.payload.status))
    );

    @Effect()
    readonly acceptAccountantInvitation$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.AcceptAccountantInvitationAction>(AccountantActions.ACCEPT_ACCOUNTANT_INVITATION),
        switchMap(action =>
            this.apiService
                .acceptAccountantInvitation(action.payload)
                .pipe(
                    map(response => new AccountantActions.AcceptAccountantInvitationSuccessAction(response)),
                    catchError(errorResponse => of(new AccountantActions.AcceptAccountantInvitationFailAction(errorResponse)))
                )
        )
    );

    @Effect()
    readonly rejectAccountantInvitation$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.RejectAccountantInvitationAction>(AccountantActions.REJECT_ACCOUNTANT_INVITATION),
        switchMap(action =>
            this.apiService
                .rejectAccountantInvitation(action.payload)
                .pipe(
                    map(response => new AccountantActions.RejectAccountantInvitationSuccessAction(response)),
                    catchError(errorResponse => of(new AccountantActions.RejectAccountantInvitationFailAction(errorResponse)))
                )
        )
    );

    @Effect({ dispatch: false })
    readonly acceptAccountantInvitationSuccess$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.AcceptAccountantInvitationSuccessAction>(AccountantActions.ACCEPT_ACCOUNTANT_INVITATION_SUCCESS),
        tap(action =>
            this.portalMessageService.showSuccessMessage('accountant_portal.messages.success.accountant_invitation.accept', {
                accountant: action.payload.fullName
            })
        )
    );

    @Effect({ dispatch: false })
    readonly acceptAccountantInvitationFail$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.AcceptAccountantInvitationFailAction>(AccountantActions.ACCEPT_ACCOUNTANT_INVITATION_FAIL),
        tap(errorResponse => this.portalMessageService.showErrorMessage(errorResponse.payload.status))
    );

    @Effect({ dispatch: false })
    readonly rejectAccountantInvitationSuccess$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.RejectAccountantInvitationSuccessAction>(AccountantActions.REJECT_ACCOUNTANT_INVITATION_SUCCESS),
        tap(action =>
            this.portalMessageService.showSuccessMessage('accountant_portal.messages.success.accountant_invitation.reject', {
                accountant: action.payload.fullName
            })
        )
    );

    @Effect({ dispatch: false })
    readonly rejectAccountantInvitationFail$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.RejectAccountantInvitationFailAction>(AccountantActions.REJECT_ACCOUNTANT_INVITATION_FAIL),
        tap(errorResponse => this.portalMessageService.showErrorMessage(errorResponse.payload.status))
    );

    @Effect()
    readonly revokeAccountantInvitation$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.RevokeAccountantInvitationAction>(AccountantActions.REVOKE_ACCOUNTANT_INVITATION),
        switchMap(action =>
            this.apiService
                .revokeAccountantInvitation(action.payload.id)
                .pipe(
                    map(() => new AccountantActions.RevokeAccountantInvitationSuccessAction(action.payload)),
                    catchError(errorResponse => of(new AccountantActions.RevokeAccountantInvitationFailAction(errorResponse)))
                )
        )
    );

    @Effect({ dispatch: false })
    readonly revokeAccountantInvitationSuccess$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.RevokeAccountantInvitationSuccessAction>(AccountantActions.REVOKE_ACCOUNTANT_INVITATION_SUCCESS),
        tap(action =>
            this.portalMessageService.showSuccessMessage('accountant_portal.messages.success.accountant_invitation.revoke', {
                accountant: action.payload.fullName
            })
        )
    );

    @Effect({ dispatch: false })
    readonly revokeAccountantInvitationFail$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.RevokeAccountantInvitationFailAction>(AccountantActions.REVOKE_ACCOUNTANT_INVITATION_FAIL),
        tap(errorResponse => this.portalMessageService.showErrorMessage(errorResponse.payload.status))
    );

    @Effect()
    readonly resendAccountantInvitation$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.ResendAccountantInvitationAction>(AccountantActions.RESEND_ACCOUNTANT_INVITATION),
        switchMap(action =>
            this.apiService
                .reinviteAccountant(action.payload)
                .pipe(
                    map(response => new AccountantActions.ResendAccountantInvitationSuccessAction(response)),
                    catchError(errorResponse => of(new AccountantActions.ResendAccountantInvitationFailAction(errorResponse)))
                )
        )
    );

    @Effect({ dispatch: false })
    readonly resendAccountantInvitationSuccess$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.ResendAccountantInvitationSuccessAction>(AccountantActions.RESEND_ACCOUNTANT_INVITATION_SUCCESS),
        tap(action =>
            this.portalMessageService.showSuccessMessage('accountant_portal.messages.success.accountant_invitation.resend', {
                accountant: action.payload.fullName
            })
        )
    );

    @Effect({ dispatch: false })
    readonly resendAccountantInvitationFail$: Observable<any> = this.actions$.pipe(
        ofType<AccountantActions.ResendAccountantInvitationFailAction>(AccountantActions.RESEND_ACCOUNTANT_INVITATION_FAIL),
        tap(errorResponse => this.portalMessageService.showErrorMessage(errorResponse.payload.status))
    );

    constructor(
        private actions$: Actions,
        private portalMessageService: PortalMessageService,
        private apiService: ApiService,
        private accountantPortalStateService: AccountantPortalStateService
    ) {
        super(accountantPortalStateService);
    }
}
