import { ActionReducerMap } from '@ngrx/store';

import * as fromAccountants from './accountants.reducer';
import * as fromAnalytics from './analytics.reducer';
import * as fromBearerToken from './bearer-token.reducer';
import * as fromClients from './clients.reducer';
import * as fromCompany from './company.reducer';
import * as fromUser from './user.reducer';

export interface State {
    accountants: fromAccountants.State;
    analytics: fromAnalytics.State;
    bearerToken: fromBearerToken.State;
    clients: fromClients.State;
    company: fromCompany.State;
    user: fromUser.State;
}

export const reducers: ActionReducerMap<State> = {
    accountants: fromAccountants.reducer,
    analytics: fromAnalytics.reducer,
    bearerToken: fromBearerToken.reducer,
    clients: fromClients.reducer,
    company: fromCompany.reducer,
    user: fromUser.reducer
};
