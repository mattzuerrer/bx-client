import { BearerTokenActions } from '../actions';
import { bearerTokenUser } from '../fixtures';

import { reducer } from './bearer-token.reducer';

const testToken = 'my-test-bearer-token';

const initialState = {
    bearerTokenUser: null,
    loaded: false
};

const loadedState = {
    bearerTokenUser,
    loaded: true
};

describe('Bearer Token reducer', () => {
    it('should return initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should set loaded to false and user to null on VERIFY_BEARER_TOKEN', () => {
        const state = initialState;
        const action = new BearerTokenActions.VerifyBearerTokenAction(testToken);
        expect(reducer(state, action).loaded).toEqual(false);
    });

    it('should return new state on VERIFY_BEARER_TOKEN_SUCCESS', () => {
        const state = initialState;
        const action = new BearerTokenActions.VerifyBearerTokenSuccessAction(bearerTokenUser);
        expect(reducer(state, action).loaded).toBe(true);
        expect(reducer(state, action).bearerTokenUser).toEqual(loadedState.bearerTokenUser);
    });

    it('should set loaded to true on VERIFY_BEARER_TOKEN_FAIL', () => {
        const state = initialState;
        const action = new BearerTokenActions.VerifyBearerTokenFailAction();
        expect(reducer(state, action).loaded).toBe(true);
    });
});
