import { applyChanges } from '@bx-client/utils';

import { AnalyticsActions } from '../actions';
import { GoogleAnalytics } from '../models';

export interface State {
    googleAnalytics: GoogleAnalytics;
    googleAnalyticsLoaded: boolean;
}

const initialState: State = {
    googleAnalytics: new GoogleAnalytics(),
    googleAnalyticsLoaded: false
};

export function reducer(state: State = initialState, action: AnalyticsActions.Actions): State {
    switch (action.type) {
        case AnalyticsActions.LOAD_GOOGLE_ANALYTICS:
            return applyChanges(state, { googleAnalyticsLoaded: false });
        case AnalyticsActions.LOAD_GOOGLE_ANALYTICS_SUCCESS:
            return applyChanges(state, { googleAnalytics: action.payload, googleAnalyticsLoaded: true });
        case AnalyticsActions.LOAD_GOOGLE_ANALYTICS_FAIL:
            return applyChanges(state, { googleAnalyticsLoaded: true });
        default:
            return state;
    }
}
