import { applyChanges } from '@bx-client/utils';

import { AccountantActions } from '../actions';
import { Accountant } from '../models';

export interface State {
    collection: Accountant[];
    loaded: boolean;
}

const initialState: State = {
    collection: [],
    loaded: false
};

const singleAccountantReducer = (collection: Accountant[], action: AccountantActions.Actions): Accountant[] => {
    switch (action.type) {
        case AccountantActions.CHANGE_ROLE:
            return collection.map(
                accountant =>
                    accountant.id === action.payload.id
                        ? Object.assign(new Accountant(), accountant, { roleChangeInProgress: true })
                        : accountant
            );
        case AccountantActions.CHANGE_ROLE_SUCCESS:
            return collection.map(
                accountant =>
                    accountant.id === action.payload.id
                        ? Object.assign(new Accountant(), accountant, { roleChangeInProgress: false, role: action.payload.role })
                        : accountant
            );
        case AccountantActions.CHANGE_ROLE_FAIL:
            return collection.map(
                accountant =>
                    accountant.id === action.payload.id
                        ? Object.assign(new Accountant(), accountant, { roleChangeInProgress: false })
                        : accountant
            );
        case AccountantActions.LOAD_ONE_SUCCESS:
        case AccountantActions.ACCEPT_ACCOUNTANT_INVITATION_SUCCESS:
            return collection.map(accountant => (accountant.id === action.payload.id ? action.payload : accountant));
        case AccountantActions.REVOKE_ACCOUNTANT_INVITATION_SUCCESS:
        case AccountantActions.REJECT_ACCOUNTANT_INVITATION_SUCCESS:
            return collection.filter(accountant => accountant.id !== action.payload.id);
        default:
            return collection;
    }
};

export function reducer(state: State = initialState, action: AccountantActions.Actions): State {
    switch (action.type) {
        case AccountantActions.LOAD:
            return applyChanges(state, { loaded: false });
        case AccountantActions.LOAD_SUCCESS:
            return applyChanges(state, { collection: action.payload, loaded: true });
        case AccountantActions.LOAD_FAIL:
            return applyChanges(state, { loaded: true });
        case AccountantActions.CHANGE_ROLE:
        case AccountantActions.CHANGE_ROLE_SUCCESS:
        case AccountantActions.CHANGE_ROLE_FAIL:
        case AccountantActions.LOAD_ONE_SUCCESS:
        case AccountantActions.REVOKE_ACCOUNTANT_INVITATION_SUCCESS:
        case AccountantActions.REJECT_ACCOUNTANT_INVITATION_SUCCESS:
        case AccountantActions.ACCEPT_ACCOUNTANT_INVITATION_SUCCESS:
            const collection = singleAccountantReducer(state.collection, action);
            return applyChanges(state, { collection });
        default:
            return state;
    }
}
