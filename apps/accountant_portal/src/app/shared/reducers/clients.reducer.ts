import { applyChanges } from '@bx-client/utils';

import { ClientsActions } from '../actions';
import { Client, PendingClientInvites } from '../models';

export interface State {
    clientsCollection: Client[];
    clientsLoaded: boolean;
    pendingClientInvitesCollection: PendingClientInvites[];
    pendingClientInvitesLoaded: boolean;
}

const initialState: State = {
    clientsCollection: [],
    clientsLoaded: false,
    pendingClientInvitesCollection: [],
    pendingClientInvitesLoaded: false
};

export function reducer(state: State = initialState, action: ClientsActions.Actions): State {
    switch (action.type) {
        case ClientsActions.LOAD:
            return applyChanges(state, { clientsLoaded: false });
        case ClientsActions.LOAD_SUCCESS:
            return applyChanges(state, { clientsCollection: action.payload, clientsLoaded: true });
        case ClientsActions.LOAD_FAIL:
            return applyChanges(state, { clientsLoaded: true });
        case ClientsActions.LOAD_ACCESS_SUCCESS:
            return applyChanges(state, {
                clientsCollection: state.clientsCollection.map(
                    client =>
                        client.id === action.payload[0].clientId
                            ? Object.assign(new Client(), client, { clientAccesses: action.payload })
                            : client
                )
            });
        case ClientsActions.LOAD_PENDING_INVITES:
            return applyChanges(state, { pendingClientInvitesLoaded: false });
        case ClientsActions.LOAD_PENDING_INVITES_SUCCESS:
            return applyChanges(state, { pendingClientInvitesCollection: action.payload, pendingClientInvitesLoaded: true });
        case ClientsActions.LOAD_PENDING_INVITES_FAIL:
            return applyChanges(state, { pendingClientInvitesLoaded: true });
        case ClientsActions.LOAD_KEY_PERFORMANCE_INDICATORS_SUCCESS:
            return applyChanges(state, {
                clientsCollection: state.clientsCollection.map(
                    client =>
                        client.id === action.payload.id
                            ? Object.assign(new Client(), client, { keyPerformanceIndicators: action.payload.keyPerformanceIndicators })
                            : client
                )
            });
        default:
            return state;
    }
}
