import { AnalyticsActions } from '../actions';
import { googleAnalytics as googleAnalyticsFixture } from '../fixtures';
import { GoogleAnalytics } from '../models';

import { reducer } from './analytics.reducer';

const initialState = {
    googleAnalytics: new GoogleAnalytics(),
    googleAnalyticsLoaded: false
};

const loadedState = {
    googleAnalytics: googleAnalyticsFixture,
    googleAnalyticsLoaded: true
};

describe('Analytics reducer', () => {
    it('should return initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should set loaded to false on LOAD_GOOGLE_ANALYTICS', () => {
        const state = initialState;
        const action = new AnalyticsActions.LoadGoogleAnalyticsAction();
        expect(reducer(state, action).googleAnalyticsLoaded).toEqual(false);
    });

    it('should return new state on LOAD_GOOGLE_ANALYTICS_SUCCESS', () => {
        const state = initialState;
        const action = new AnalyticsActions.LoadGoogleAnalyticsSuccessAction(googleAnalyticsFixture);
        expect(reducer(state, action).googleAnalyticsLoaded).toBe(true);
        expect(reducer(state, action).googleAnalytics).toEqual(loadedState.googleAnalytics);
    });

    it('should set loaded to true on LOAD_GOOGLE_ANALYTICS_FAIL', () => {
        const state = initialState;
        const action = new AnalyticsActions.LoadGoogleAnalyticsFailAction();
        expect(reducer(state, action).googleAnalyticsLoaded).toBe(true);
    });
});
