import { applyChanges } from '@bx-client/utils';

import { CompanyActions } from '../actions';
import { Company } from '../models';

export interface State {
    settings: Company;
    newCompanyId: number;
    loaded: boolean;
    logo: ArrayBuffer;
    loadedLogo: boolean;
    logoUploaded: boolean;
    logoChanged: boolean;
    pdf: ArrayBuffer;
    loadedPdf: boolean;
    pdfUploaded: boolean;
    pdfChanged: boolean;
    uploadLogoAndPdf: boolean;
    collection: Company[];
    collectionLoaded: boolean;
}

const initialState: State = {
    settings: null,
    newCompanyId: null,
    loaded: false,
    logo: null,
    loadedLogo: null,
    logoUploaded: false,
    logoChanged: false,
    pdf: null,
    loadedPdf: null,
    pdfUploaded: false,
    pdfChanged: false,
    uploadLogoAndPdf: false,
    collection: [],
    collectionLoaded: false
};

export function reducer(state: State = initialState, action: CompanyActions.Actions): State {
    switch (action.type) {
        case CompanyActions.LOAD_SETTINGS:
            return applyChanges(state, { loaded: false });
        case CompanyActions.LOAD_SETTINGS_SUCCESS:
            return applyChanges(state, { settings: action.payload, loaded: true });
        case CompanyActions.LOAD_SETTINGS_FAIL:
            return applyChanges(state, { loaded: true });

        case CompanyActions.LOAD_LOGO:
            return applyChanges(state, { loadedLogo: false });
        case CompanyActions.LOAD_LOGO_SUCCESS:
            return applyChanges(state, { logo: action.payload, loadedLogo: true });
        case CompanyActions.LOAD_LOGO_FAIL:
            return applyChanges(state, { loadedLogo: true });

        case CompanyActions.LOGO_UPLOAD:
        case CompanyActions.LOGO_UPLOAD_FAIL:
            return applyChanges(state, { logoChanged: true, logoUploaded: false });
        case CompanyActions.LOGO_UPLOAD_SUCCESS:
            return applyChanges(state, { logoChanged: false, logoUploaded: true });
        case CompanyActions.LOGO_CHANGED:
            return applyChanges(state, { logoChanged: action.payload });

        case CompanyActions.LOAD_PDF:
            return applyChanges(state, { loadedPdf: false });
        case CompanyActions.LOAD_PDF_SUCCESS:
            return applyChanges(state, { pdf: action.payload, loadedPdf: true });
        case CompanyActions.LOAD_PDF_FAIL:
            return applyChanges(state, { loadedPdf: true });

        case CompanyActions.PDF_UPLOAD:
        case CompanyActions.PDF_UPLOAD_FAIL:
            return applyChanges(state, { pdfChanged: true, pdfUploaded: false });
        case CompanyActions.PDF_UPLOAD_SUCCESS:
            return applyChanges(state, { pdfChanged: false, pdfUploaded: true });
        case CompanyActions.PDF_CHANGED:
            return applyChanges(state, { pdfChanged: action.payload });

        case CompanyActions.CREATE_NEW_COMPANY_SUCCESS:
            return applyChanges(state, { newCompanyId: action.response.id });

        case CompanyActions.LOGO_AND_PDF_UPLOAD:
            return applyChanges(state, { uploadLogoAndPdf: true });

        case CompanyActions.SAVE_SETTINGS_SUCCESS:
            return applyChanges(state, { settings: action.payload });

        case CompanyActions.LOAD_COLLECTION:
            return applyChanges(state, { collectionLoaded: false });
        case CompanyActions.LOAD_COLLECTION_SUCCESS:
            return applyChanges(state, { collection: action.payload, collectionLoaded: true });
        case CompanyActions.LOAD_COLLECTION_FAIL:
            return applyChanges(state, { collectionLoaded: true });

        default:
            return state;
    }
}
