import { HttpErrorResponse } from '@angular/common/http';

import { AccountantActions } from '../actions';
import { accountants } from '../fixtures';
import { Accountant } from '../models';

import { reducer } from './accountants.reducer';

const initialState = {
    collection: [],
    loaded: false
};

const loadedState = {
    collection: accountants,
    loaded: true
};

const httpErrorPayload = new HttpErrorResponse({
    error: 'Unprocessable Entity',
    status: 422
});

const loadingAccountant = Object.assign(accountants[0], { roleChangeInProgress: true, role: 'old role' });
const loadedAccountant = Object.assign(accountants[0], { roleChangeInProgress: false, role: 'new role' });

const loadedStateWithLoadingAccountant = {
    collection: [loadingAccountant, accountants[1], accountants[2]],
    loaded: true
};

describe('Accountants reducer', () => {
    it('should return initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should set loaded to false on LOAD', () => {
        const state = initialState;
        const action = new AccountantActions.LoadAction();
        expect(reducer(state, action).loaded).toEqual(false);
    });

    it('should return new state on LOAD_SUCCESS', () => {
        const state = initialState;
        const action = new AccountantActions.LoadSuccessAction(accountants);
        expect(reducer(state, action).loaded).toBe(true);
        expect(reducer(state, action).collection).toEqual(loadedState.collection);
    });

    it('should set loaded to true on LOAD_FAIL', () => {
        const state = initialState;
        const action = new AccountantActions.LoadFailAction();
        expect(reducer(state, action).loaded).toBe(true);
    });

    it('should set accountants roleChangeInProgress to true on CHANGE_ROLE', () => {
        const state = loadedState;
        const action = new AccountantActions.ChangeRoleAction({ id: accountants[0].id, role: 'owner' });
        const newCollection = state.collection.map(accountant => Object.assign(new Accountant(), accountant));
        newCollection[0].roleChangeInProgress = true;
        expect(reducer(state, action).collection).toEqual(newCollection);
    });

    it('should set new accountant to collection on CHANGE_ROLE_SUCCESS', () => {
        const state = loadedStateWithLoadingAccountant;
        const action = new AccountantActions.ChangeRoleSuccessAction(loadedAccountant);
        const newCollection = state.collection.map(accountant => Object.assign(new Accountant(), accountant));
        newCollection[0] = Object.assign(newCollection[0], loadedAccountant);
        expect(reducer(state, action).collection).toEqual(newCollection);
    });

    it('should set accountants roleChangeInProgress to false on CHANGE_ROLE_FAIL', () => {
        const state = loadedStateWithLoadingAccountant;
        const action = new AccountantActions.ChangeRoleFailAction({ id: accountants[0].id, error: httpErrorPayload });
        const newCollection = state.collection.map(accountant => Object.assign(new Accountant(), accountant));
        Object.assign(newCollection[0], { roleChangeInProgress: false });
        expect(reducer(state, action).collection[0]).toEqual(newCollection[0]);
    });

    it('should change accountant in collection to newly loaded one on LOAD_ONE_SUCCESS', () => {
        const state = loadedStateWithLoadingAccountant;
        const action = new AccountantActions.LoadOneSuccessAction(accountants[0]);
        const newCollection = state.collection.map(accountant => Object.assign(new Accountant(), accountant));
        newCollection[0] = accountants[0];
        expect(reducer(state, action).collection[0]).toEqual(newCollection[0]);
    });
});
