import { ClientsActions } from '../actions';
import { clientAccess, clients, keyPerformanceIndicators, pendingClientInvites } from '../fixtures';
import { Client } from '../models';

import { reducer } from './clients.reducer';

const initialState = {
    clientsCollection: [],
    clientsLoaded: false,
    pendingClientInvitesCollection: [],
    pendingClientInvitesLoaded: false
};

const loadedState = {
    clientsCollection: clients,
    clientsLoaded: true,
    pendingClientInvitesCollection: pendingClientInvites,
    pendingClientInvitesLoaded: true
};

describe('Clients reducer', () => {
    it('should return initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should set loaded to false on LOAD', () => {
        const state = initialState;
        const action = new ClientsActions.LoadAction();
        expect(reducer(state, action).clientsLoaded).toEqual(false);
    });

    it('should return new state on LOAD_SUCCESS', () => {
        const state = initialState;
        const action = new ClientsActions.LoadSuccessAction(clients);
        expect(reducer(state, action).clientsLoaded).toBe(true);
        expect(reducer(state, action).clientsCollection).toEqual(loadedState.clientsCollection);
    });

    it('should set loaded to true on LOAD_FAIL', () => {
        const state = initialState;
        const action = new ClientsActions.LoadFailAction();
        expect(reducer(state, action).clientsLoaded).toBe(true);
    });

    it('should update client in collection with newly loaded clientAccess on LOAD_ACCESS_SUCCESS', () => {
        const state = loadedState;
        const action = new ClientsActions.LoadAccessSuccessAction([clientAccess]);
        const newCollection = state.clientsCollection.map(client => Object.assign(new Client(), client));
        Object.assign(newCollection[1], { clientAccesses: [clientAccess] });
        expect(reducer(state, action).clientsCollection[0]).toEqual(newCollection[0]);
    });

    it('should set loaded to false on LOAD_PENDING_INVITES', () => {
        const state = initialState;
        const action = new ClientsActions.LoadPendingInvitesAction();
        expect(reducer(state, action).pendingClientInvitesLoaded).toEqual(false);
    });

    it('should return new state on LOAD_PENDING_INVITES_SUCCESS', () => {
        const state = initialState;
        const action = new ClientsActions.LoadPendingInvitesSuccessAction(pendingClientInvites);
        expect(reducer(state, action).pendingClientInvitesLoaded).toBe(true);
        expect(reducer(state, action).pendingClientInvitesCollection).toEqual(loadedState.pendingClientInvitesCollection);
    });

    it('should set loaded to true on LOAD_PENDING_INVITES_FAIL', () => {
        const state = initialState;
        const action = new ClientsActions.LoadPendingInvitesFailAction();
        expect(reducer(state, action).pendingClientInvitesLoaded).toBe(true);
    });

    it('should update client in collection with keyPerformanceIndicators on LOAD_KEY_PERFORMANCE_INDICATORS_SUCCESS', () => {
        const state = loadedState;
        const action = new ClientsActions.LoadKeyPerformanceIndicatorsSuccessAction({ id: 1, keyPerformanceIndicators });
        const newCollection = state.clientsCollection.map(client => Object.assign(new Client(), client));
        Object.assign(newCollection[0], { keyPerformanceIndicators });
        expect(reducer(state, action).clientsCollection[0]).toEqual(newCollection[0]);
    });
});
