import { applyChanges } from '@bx-client/utils';

import { BearerTokenActions } from '../actions';
import { BearerTokenUser } from '../models';

export interface State {
    bearerTokenUser: BearerTokenUser;
    loaded: boolean;
}

const initialState: State = {
    bearerTokenUser: null,
    loaded: false
};

export function reducer(state: State = initialState, action: BearerTokenActions.Actions): State {
    switch (action.type) {
        case BearerTokenActions.VERIFY_BEARER_TOKEN:
            return applyChanges(state, { loaded: false, bearerTokenUser: null });
        case BearerTokenActions.VERIFY_BEARER_TOKEN_SUCCESS:
            return applyChanges(state, { loaded: true, bearerTokenUser: action.payload });
        case BearerTokenActions.VERIFY_BEARER_TOKEN_FAIL:
            return applyChanges(state, { loaded: true });
        default:
            return state;
    }
}
