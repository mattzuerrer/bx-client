import { HttpErrorResponse } from '@angular/common/http';

import { UserActions } from '../actions';
import { accountants, user } from '../fixtures';
import { Accountant, User } from '../models';

import { reducer } from './user.reducer';

const httpErrorPayload = new HttpErrorResponse({
    error: 'Access Denied',
    status: 403
});

const initialState = {
    loginUser: new User(),
    accountantUser: new Accountant(),
    restApiError: null,
    loginUserloaded: false,
    accountantUserLoaded: false
};

const loadedState = {
    loginUser: user,
    accountantUser: accountants[0],
    restApiError: httpErrorPayload,
    loginUserloaded: true,
    accountantUserLoaded: true
};

describe('User reducer', () => {
    it('should return initial state', () => {
        const state = undefined;
        const action = { type: null };
        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should return new state on LOAD_LOGIN_USER', () => {
        const state = initialState;
        const action = new UserActions.LoadLoginUserAction();
        expect(reducer(state, action).loginUserloaded).toEqual(false);
    });

    it('should return new state on LOAD_LOGIN_USER_SUCCESS', () => {
        const state = initialState;
        const action = new UserActions.LoadLoginUserSuccessAction(user);
        expect(reducer(state, action).loginUserloaded).toBe(loadedState.loginUserloaded);
    });

    it('should return new state on LOAD_LOGIN_USER_FAIL', () => {
        const state = initialState;
        const action = new UserActions.LoadLoginUserFailAction(httpErrorPayload);
        expect(reducer(state, action).loginUserloaded).toBe(loadedState.loginUserloaded);
        expect(reducer(state, action).restApiError).toEqual(loadedState.restApiError);
    });

    it('should return new state on LOAD_ACCOUNTANT_USER', () => {
        const state = initialState;
        const action = new UserActions.LoadAccountantUserAction();
        expect(reducer(state, action).accountantUserLoaded).toEqual(false);
    });

    it('should return new state on LOAD_ACCOUNTANT_USER_SUCCESS', () => {
        const state = initialState;
        const action = new UserActions.LoadAccountantUserSuccessAction(accountants[0]);
        expect(reducer(state, action).accountantUserLoaded).toEqual(loadedState.accountantUserLoaded);
    });

    it('should return new state on LOAD_ACCOUNTANT_USER_FAIL', () => {
        const state = initialState;
        const action = new UserActions.LoadLoginUserFailAction(httpErrorPayload);
        expect(reducer(state, action).loginUserloaded).toBe(loadedState.accountantUserLoaded);
    });
});
