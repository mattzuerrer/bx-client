import { HttpErrorResponse } from '@angular/common/http';
import { applyChanges } from '@bx-client/utils';

import { UserActions } from '../actions';
import { Accountant, User } from '../models';

export interface State {
    loginUser: User;
    accountantUser: Accountant;
    restApiError: HttpErrorResponse;
    loginUserloaded: boolean;
    accountantUserLoaded: boolean;
}

const initialState: State = {
    loginUser: new User(),
    accountantUser: new Accountant(),
    restApiError: null,
    loginUserloaded: false,
    accountantUserLoaded: false
};

export function reducer(state: State = initialState, action: UserActions.Actions): State {
    switch (action.type) {
        case UserActions.LOAD_LOGIN_USER:
            return applyChanges(state, { loginUserloaded: false });
        case UserActions.LOAD_LOGIN_USER_SUCCESS:
            return applyChanges(state, { loginUser: action.payload, loginUserloaded: true });
        case UserActions.LOAD_LOGIN_USER_FAIL:
            return applyChanges(state, { loginUserloaded: true, restApiError: action.payload });
        case UserActions.LOAD_ACCOUNTANT_USER:
            return applyChanges(state, { accountantUserLoaded: false });
        case UserActions.LOAD_ACCOUNTANT_USER_SUCCESS:
            return applyChanges(state, { accountantUserLoaded: true, accountantUser: action.payload });
        case UserActions.LOAD_ACCOUNTANT_USER_FAIL:
            return applyChanges(state, { accountantUserLoaded: true });
        default:
            return state;
    }
}
