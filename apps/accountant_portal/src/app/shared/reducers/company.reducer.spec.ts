import { HttpErrorResponse } from '@angular/common/http';
import { FormGroup } from '@angular/forms';

import { CompanyActions } from '../actions';
import { company } from '../fixtures';
import { PortalCompanyFile } from '../models';

import { reducer } from './company.reducer';

const initialState = {
    settings: null,
    newCompanyId: null,
    loaded: false,
    logo: null,
    loadedLogo: null,
    logoUploaded: false,
    logoChanged: false,
    pdf: null,
    loadedPdf: null,
    pdfUploaded: false,
    pdfChanged: false,
    uploadLogoAndPdf: false,
    collection: [],
    collectionLoaded: false
};

const loadedState = {
    settings: company,
    newCompanyId: 1,
    loaded: true,
    logo: new ArrayBuffer(1),
    loadedLogo: true,
    logoUploaded: false,
    logoChanged: true,
    pdf: new ArrayBuffer(1),
    loadedPdf: true,
    pdfUploaded: false,
    pdfChanged: true,
    uploadLogoAndPdf: true,
    collection: [company],
    collectionLoaded: true
};

const httpErrorPayload = new HttpErrorResponse({
    error: 'Access Denied',
    status: 403
});

const formGroup = new FormGroup({});

describe('Company reducer', () => {
    it('should return initial state', () => {
        const state = undefined;
        const action = { type: null };

        expect(reducer(state, action)).toEqual(initialState);
    });

    it('should set loaded to false on LOAD_SETTINGS', () => {
        const state = initialState;
        const action = new CompanyActions.LoadSettingsAction(1);
        expect(reducer(state, action).loaded).toEqual(false);
    });

    it('should return new state on LOAD_SETTINGS_SUCCESS', () => {
        const state = initialState;
        const action = new CompanyActions.LoadSettingsSuccessAction(company);
        expect(reducer(state, action).loaded).toBe(loadedState.loaded);
        expect(reducer(state, action).settings).toEqual(loadedState.settings);
    });

    it('should return new state on SAVE_SETTINGS_SUCCESS', () => {
        const state = initialState;
        const action = new CompanyActions.SaveSettingsSuccessAction(company, formGroup);
        expect(reducer(state, action).settings).toEqual(loadedState.settings);
    });

    it('should set loaded to true on LOAD_SETTINGS_FAIL', () => {
        const state = initialState;
        const action = new CompanyActions.LoadSettingsFailAction();
        expect(reducer(state, action).loaded).toBe(loadedState.loaded);
    });

    it('should set loaded to false on LOAD_LOGO', () => {
        const state = initialState;
        const action = new CompanyActions.LoadLogoAction(1);
        expect(reducer(state, action).loadedLogo).toEqual(false);
    });

    it('should return new state on LOAD_LOGO_SUCCESS', () => {
        const state = initialState;
        const action = new CompanyActions.LoadLogoSuccessAction(new ArrayBuffer(1));
        expect(reducer(state, action).logo).toEqual(loadedState.logo);
        expect(reducer(state, action).loadedLogo).toEqual(loadedState.loadedLogo);
    });

    it('should set loaded to true on LOAD_LOGO_FAIL', () => {
        const state = initialState;
        const action = new CompanyActions.LoadLogoFailAction();
        expect(reducer(state, action).loadedLogo).toBe(loadedState.loadedLogo);
    });

    it('should set logoUploaded to false on LOGO_UPLOAD', () => {
        const state = initialState;
        const action = new CompanyActions.UploadLogoAction(new PortalCompanyFile(1));
        expect(reducer(state, action).logoUploaded).toEqual(loadedState.logoUploaded);
    });

    it('should set logoUploaded to true on LOGO_UPLOAD_SUCCESS', () => {
        const state = initialState;
        const action = new CompanyActions.UploadLogoSuccessAction();
        expect(reducer(state, action).logoUploaded).toEqual(true);
    });

    it('should set logoUploaded to false on LOGO_UPLOAD_FAIL', () => {
        const state = initialState;
        const action = new CompanyActions.UploadLogoFailAction('error');
        expect(reducer(state, action).logoUploaded).toEqual(loadedState.logoUploaded);
    });

    it('should set logoChanged to true on LOGO_UPLOAD', () => {
        const state = initialState;
        const action = new CompanyActions.UploadLogoAction(new PortalCompanyFile(1));
        expect(reducer(state, action).logoChanged).toEqual(loadedState.logoChanged);
    });

    it('should set logoChanged to false on LOGO_UPLOAD_SUCCESS', () => {
        const state = initialState;
        const action = new CompanyActions.UploadLogoSuccessAction();
        expect(reducer(state, action).logoChanged).toEqual(false);
    });

    it('should set logoChanged to true on LOGO_UPLOAD_FAIL', () => {
        const state = initialState;
        const action = new CompanyActions.UploadLogoFailAction('error');
        expect(reducer(state, action).logoChanged).toEqual(loadedState.logoChanged);
    });

    it('should set logoChanged to payload on LOGO_CHANGED', () => {
        const state = initialState;
        let action;

        action = new CompanyActions.LogoChangedAction(true);
        expect(reducer(state, action).logoChanged).toEqual(true);

        action = new CompanyActions.LogoChangedAction(false);
        expect(reducer(state, action).logoChanged).toEqual(false);
    });

    it('should set loaded to false on LOAD_PDF', () => {
        const state = initialState;
        const action = new CompanyActions.LoadPdfAction(1);
        expect(reducer(state, action).loadedPdf).toEqual(false);
    });

    it('should return new state on LOAD_PDF_SUCCESS', () => {
        const state = initialState;
        const action = new CompanyActions.LoadPdfSuccessAction(new ArrayBuffer(1));
        expect(reducer(state, action).pdf).toEqual(loadedState.pdf);
        expect(reducer(state, action).loadedPdf).toEqual(loadedState.loadedPdf);
    });

    it('should set loaded to true on LOAD_PDF_FAIL', () => {
        const state = initialState;
        const action = new CompanyActions.LoadPdfFailAction();
        expect(reducer(state, action).loadedPdf).toBe(loadedState.loadedPdf);
    });

    it('should set pdfUploaded to false on PDF_UPLOAD', () => {
        const state = initialState;
        const action = new CompanyActions.UploadPdfAction(new PortalCompanyFile(1));
        expect(reducer(state, action).pdfUploaded).toEqual(loadedState.pdfUploaded);
    });

    it('should set pdfUploaded to true on PDF_UPLOAD_SUCCESS', () => {
        const state = initialState;
        const action = new CompanyActions.UploadPdfSuccessAction();
        expect(reducer(state, action).pdfUploaded).toEqual(true);
    });

    it('should set pdfUploaded to false on PDF_UPLOAD_FAIL', () => {
        const state = initialState;
        const action = new CompanyActions.UploadPdfFailAction('error');
        expect(reducer(state, action).pdfUploaded).toEqual(loadedState.pdfUploaded);
    });

    it('should set pdfChanged to true on PDF_UPLOAD', () => {
        const state = initialState;
        const action = new CompanyActions.UploadPdfAction(new PortalCompanyFile(1));
        expect(reducer(state, action).pdfChanged).toEqual(loadedState.pdfChanged);
    });

    it('should set pdfChanged to false on PDF_UPLOAD_SUCCESS', () => {
        const state = initialState;
        const action = new CompanyActions.UploadPdfSuccessAction();
        expect(reducer(state, action).pdfChanged).toEqual(false);
    });

    it('should set pdfChanged to true on PDF_UPLOAD_FAIL', () => {
        const state = initialState;
        const action = new CompanyActions.UploadPdfFailAction('error');
        expect(reducer(state, action).pdfChanged).toEqual(loadedState.pdfChanged);
    });

    it('should set pdfChanged to payload on PDF_CHANGED', () => {
        const state = initialState;
        let action;

        action = new CompanyActions.PdfChangedAction(true);
        expect(reducer(state, action).pdfChanged).toEqual(true);

        action = new CompanyActions.PdfChangedAction(false);
        expect(reducer(state, action).pdfChanged).toEqual(false);
    });

    it('should set collectionLoaded to false on LOAD_COLLECTION', () => {
        const state = initialState;
        const action = new CompanyActions.LoadCollectionAction();
        expect(reducer(state, action).collectionLoaded).toEqual(false);
    });

    it('should return new state on LOAD_COLLECTION_SUCCESS', () => {
        const state = initialState;
        const action = new CompanyActions.LoadCollectionSuccessAction([company]);
        expect(reducer(state, action).collectionLoaded).toBe(loadedState.collectionLoaded);
        expect(reducer(state, action).collection).toEqual(loadedState.collection);
    });

    it('should set loaded to true on LOAD_COLLECTION_FAIL', () => {
        const state = initialState;
        const action = new CompanyActions.LoadCollectionFailAction(httpErrorPayload);
        expect(reducer(state, action).collectionLoaded).toBe(loadedState.collectionLoaded);
    });

    it('should set newCompanyId on CREATE_NEW_COMPANY_SUCCESS', () => {
        const state = initialState;
        const response = { id: 1 };
        const action = new CompanyActions.CreateNewCompanySuccessAction(company, response);
        expect(reducer(state, action).newCompanyId).toBe(loadedState.newCompanyId);
    });

    it('should set uploadLogoAndPdf to true on LOGO_AND_PDF_UPLOAD', () => {
        const state = initialState;
        const action = new CompanyActions.LogoAndPdfUploadAction();
        expect(reducer(state, action).uploadLogoAndPdf).toBe(loadedState.uploadLogoAndPdf);
    });
});
