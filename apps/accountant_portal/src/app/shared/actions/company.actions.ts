import { HttpErrorResponse } from '@angular/common/http';
import { FormGroup } from '@angular/forms';
import { Action } from '@ngrx/store';

import { Company, PortalCompanyFile } from '../models';

export namespace CompanyActions {
    export const LOAD_SETTINGS = '[Company] Load settings';
    export const LOAD_SETTINGS_SUCCESS = '[Company] Load settings success';
    export const LOAD_SETTINGS_FAIL = '[Company] Load settings fail';
    export const SAVE_SETTINGS = '[Company] Save settings';
    export const SAVE_SETTINGS_SUCCESS = '[Company] Save settings success';
    export const SAVE_SETTINGS_FAIL = '[Company] Save settings fail';
    export const CREATE_NEW_COMPANY = '[Company] Create new company';
    export const CREATE_NEW_COMPANY_SUCCESS = '[Company] Create new company success';
    export const CREATE_NEW_COMPANY_FAIL = '[Company] Create new company fail';
    export const LOAD_LOGO = '[Company] Load Logo';
    export const LOAD_LOGO_SUCCESS = '[Company] Load Logo success';
    export const LOAD_LOGO_FAIL = '[Company] Load Logo fail';
    export const LOGO_UPLOAD = '[Company] Upload Logo';
    export const LOGO_UPLOAD_SUCCESS = '[Company] Upload Logo success';
    export const LOGO_UPLOAD_FAIL = '[Company] Upload Logo fail';
    export const LOGO_AND_PDF_UPLOAD = '[Company] Upload Logo and PDF';
    export const LOGO_CHANGED = '[Company] Logo changed';
    export const LOAD_PDF = '[Company] Load PDF';
    export const LOAD_PDF_SUCCESS = '[Company] Load PDF success';
    export const LOAD_PDF_FAIL = '[Company] Load PDF fail';
    export const PDF_UPLOAD = '[Company] Upload PDF';
    export const PDF_UPLOAD_SUCCESS = '[Company] Upload PDF success';
    export const PDF_UPLOAD_FAIL = '[Company] Upload PDF fail';
    export const PDF_CHANGED = '[Company] PDF changed';
    export const LOAD_COLLECTION = '[Company] Load collection';
    export const LOAD_COLLECTION_SUCCESS = '[Company] Load collection success';
    export const LOAD_COLLECTION_FAIL = '[Company] Load collection fail';

    export class LoadSettingsAction implements Action {
        readonly type = LOAD_SETTINGS;

        constructor(public payload: number) {}
    }

    export class LoadSettingsSuccessAction implements Action {
        readonly type = LOAD_SETTINGS_SUCCESS;

        constructor(public payload: Company) {}
    }

    export class LoadSettingsFailAction implements Action {
        readonly type = LOAD_SETTINGS_FAIL;
    }

    export class LoadLogoAction implements Action {
        readonly type = LOAD_LOGO;

        constructor(public payload: number) {}
    }

    export class LoadLogoSuccessAction implements Action {
        readonly type = LOAD_LOGO_SUCCESS;

        constructor(public payload: ArrayBuffer) {}
    }

    export class LoadLogoFailAction implements Action {
        readonly type = LOAD_LOGO_FAIL;
    }

    export class UploadLogoAction implements Action {
        readonly type = LOGO_UPLOAD;

        constructor(public payload: PortalCompanyFile) {}
    }

    export class UploadLogoSuccessAction implements Action {
        readonly type = LOGO_UPLOAD_SUCCESS;
    }

    export class UploadLogoFailAction implements Action {
        readonly type = LOGO_UPLOAD_FAIL;

        constructor(public payload: string) {}
    }

    export class LogoAndPdfUploadAction implements Action {
        readonly type = LOGO_AND_PDF_UPLOAD;
    }

    export class LogoChangedAction implements Action {
        readonly type = LOGO_CHANGED;

        constructor(public payload: boolean) {}
    }

    export class LoadPdfAction implements Action {
        readonly type = LOAD_PDF;

        constructor(public payload: number) {}
    }

    export class LoadPdfSuccessAction implements Action {
        readonly type = LOAD_PDF_SUCCESS;

        constructor(public payload: ArrayBuffer) {}
    }

    export class LoadPdfFailAction implements Action {
        readonly type = LOAD_PDF_FAIL;
    }

    export class UploadPdfAction implements Action {
        readonly type = PDF_UPLOAD;

        constructor(public payload: PortalCompanyFile) {}
    }

    export class UploadPdfSuccessAction implements Action {
        readonly type = PDF_UPLOAD_SUCCESS;
    }

    export class UploadPdfFailAction implements Action {
        readonly type = PDF_UPLOAD_FAIL;

        constructor(public payload: string) {}
    }

    export class PdfChangedAction implements Action {
        readonly type = PDF_CHANGED;

        constructor(public payload: boolean) {}
    }

    export class SaveSettingsAction implements Action {
        readonly type = SAVE_SETTINGS;

        constructor(public payload: Company, public form: FormGroup) {}
    }

    export class SaveSettingsSuccessAction implements Action {
        readonly type = SAVE_SETTINGS_SUCCESS;

        constructor(public payload: Company, public form: FormGroup) {}
    }

    export class SaveSettingsFailAction implements Action {
        readonly type = SAVE_SETTINGS_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class CreateNewCompanyAction implements Action {
        readonly type = CREATE_NEW_COMPANY;

        constructor(public payload: Company) {}
    }

    export class CreateNewCompanySuccessAction implements Action {
        readonly type = CREATE_NEW_COMPANY_SUCCESS;

        constructor(public payload: Company, public response: any) {}
    }

    export class CreateNewCompanyFailAction implements Action {
        readonly type = CREATE_NEW_COMPANY_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class LoadCollectionAction implements Action {
        readonly type = LOAD_COLLECTION;
    }

    export class LoadCollectionSuccessAction implements Action {
        readonly type = LOAD_COLLECTION_SUCCESS;

        constructor(public payload: Company[]) {}
    }

    export class LoadCollectionFailAction implements Action {
        readonly type = LOAD_COLLECTION_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export type Actions =
        | LoadSettingsAction
        | LoadSettingsSuccessAction
        | LoadSettingsFailAction
        | SaveSettingsAction
        | SaveSettingsSuccessAction
        | SaveSettingsFailAction
        | CreateNewCompanyAction
        | CreateNewCompanySuccessAction
        | CreateNewCompanyFailAction
        | LoadLogoAction
        | LoadLogoSuccessAction
        | LoadLogoFailAction
        | LogoAndPdfUploadAction
        | LogoChangedAction
        | UploadLogoAction
        | UploadLogoSuccessAction
        | UploadLogoFailAction
        | LoadPdfAction
        | LoadPdfSuccessAction
        | LoadPdfFailAction
        | UploadPdfAction
        | UploadPdfSuccessAction
        | UploadPdfFailAction
        | PdfChangedAction
        | LoadCollectionAction
        | LoadCollectionSuccessAction
        | LoadCollectionFailAction;
}
