import { Action } from '@ngrx/store';

import { BearerTokenUser } from '../models';

export namespace BearerTokenActions {
    export const VERIFY_BEARER_TOKEN = '[BearerToken] Verify token';
    export const VERIFY_BEARER_TOKEN_SUCCESS = '[BearerToken] Verify token success';
    export const VERIFY_BEARER_TOKEN_FAIL = '[BearerToken] Verify token fail';

    export class VerifyBearerTokenAction implements Action {
        readonly type = VERIFY_BEARER_TOKEN;

        constructor(public payload: string) {}
    }

    export class VerifyBearerTokenSuccessAction implements Action {
        readonly type = VERIFY_BEARER_TOKEN_SUCCESS;

        constructor(public payload: BearerTokenUser) {}
    }

    export class VerifyBearerTokenFailAction implements Action {
        readonly type = VERIFY_BEARER_TOKEN_FAIL;
    }

    export type Actions = VerifyBearerTokenAction | VerifyBearerTokenSuccessAction | VerifyBearerTokenFailAction;
}
