import { Action } from '@ngrx/store';

export namespace ServicesActions {
    export const REDIRECT_TO_PARTNER_SERVICE = '[Services] Redirect to partner service';
    export const REDIRECT_TO_PARTNER_SERVICE_FAIL = '[Services] Redirect to partner service fail';

    export class RedirectToPartnerServiceAction implements Action {
        readonly type = REDIRECT_TO_PARTNER_SERVICE;

        /**
         * @param {string} payload The name of the service to which the user should be redirected.
         */
        constructor(public payload: string) {}
    }

    export type Actions = RedirectToPartnerServiceAction;
}
