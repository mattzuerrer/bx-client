import { HttpErrorResponse } from '@angular/common/http';
import { Action } from '@ngrx/store';

import { Accountant } from '../models';

export namespace AccountantActions {
    export const LOAD = '[Accountants] Load';
    export const LOAD_SUCCESS = '[Accountants] Load success';
    export const LOAD_FAIL = '[Accountants] Load fail';
    export const CHANGE_ROLE = '[Accountants] Change role';
    export const CHANGE_ROLE_SUCCESS = '[Accountants] Change role success';
    export const CHANGE_ROLE_FAIL = '[Accountants] Change role fail';
    export const ADD_NEW_ACCOUNTANT = '[Accountants] Add new accountant';
    export const ADD_NEW_ACCOUNTANT_SUCCESS = '[Accountants] Add new accountant success';
    export const ADD_NEW_ACCOUNTANT_FAIL = '[Accountants] Add new accountant fail';
    export const REMOVE = '[Accountants] Remove';
    export const REMOVE_SUCCESS = '[Accountants] Remove success';
    export const REMOVE_FAIL = '[Accountants] Remove fail';
    export const LOAD_ONE = '[Accountants] Load one';
    export const LOAD_ONE_SUCCESS = '[Accountants] Load one success';
    export const LOAD_ONE_FAIL = '[Accountants] Load one fail';
    export const JOIN_EXISTING_COMPANY = '[Accountants] Join existing company';
    export const JOIN_EXISTING_COMPANY_SUCCESS = '[Accountants] Join existing company success';
    export const JOIN_EXISTING_COMPANY_FAIL = '[Accountants] Join existing company fail';
    export const SAVE_PASSWORD = '[Accountants] Save password';
    export const SAVE_PASSWORD_SUCCESS = '[Accountants] Save password success';
    export const SAVE_PASSWORD_FAIL = '[Accountants] Save password fail';
    export const NEW_ACCOUNTANT_FROM_INVITATION = '[Accountants] New accountant from invitation';
    export const NEW_ACCOUNTANT_FROM_INVITATION_FAIL = '[Accountants] New accountant from invitation fail';
    export const ACCEPT_ACCOUNTANT_INVITATION = '[Accountants] Accept accountant invitation';
    export const ACCEPT_ACCOUNTANT_INVITATION_SUCCESS = '[Accountants] Accept accountant invitation success';
    export const ACCEPT_ACCOUNTANT_INVITATION_FAIL = '[Accountants] Accept accountant invitation fail';
    export const REJECT_ACCOUNTANT_INVITATION = '[Accountants] Reject accountant invitation';
    export const REJECT_ACCOUNTANT_INVITATION_SUCCESS = '[Accountants] Reject accountant invitation success';
    export const REJECT_ACCOUNTANT_INVITATION_FAIL = '[Accountants] Reject accountant invitation fail';
    export const REVOKE_ACCOUNTANT_INVITATION = '[Accountants] Revoke accountant invitation';
    export const REVOKE_ACCOUNTANT_INVITATION_SUCCESS = '[Accountants] Revoke accountant invitation success';
    export const REVOKE_ACCOUNTANT_INVITATION_FAIL = '[Accountants] Revoke accountant invitation fail';
    export const RESEND_ACCOUNTANT_INVITATION = '[Accountants] Resend accountant invitation';
    export const RESEND_ACCOUNTANT_INVITATION_SUCCESS = '[Accountants] Resend accountant invitation success';
    export const RESEND_ACCOUNTANT_INVITATION_FAIL = '[Accountants] Resend accountant invitation fail';

    export class LoadAction implements Action {
        readonly type = LOAD;
    }

    export class LoadSuccessAction implements Action {
        readonly type = LOAD_SUCCESS;

        constructor(public payload: Accountant[]) {}
    }

    export class LoadFailAction implements Action {
        readonly type = LOAD_FAIL;
    }

    export class ChangeRoleAction implements Action {
        readonly type = CHANGE_ROLE;

        constructor(public payload: Pick<Accountant, 'id' | 'role'>) {}
    }

    export class ChangeRoleSuccessAction implements Action {
        readonly type = CHANGE_ROLE_SUCCESS;

        constructor(public payload: Accountant) {}
    }

    export class ChangeRoleFailAction implements Action {
        readonly type = CHANGE_ROLE_FAIL;

        constructor(public payload: { id: number; error: HttpErrorResponse }) {}
    }

    export class AddNewAccountantAction implements Action {
        readonly type = ADD_NEW_ACCOUNTANT;

        constructor(public payload: Partial<Accountant>) {}
    }

    export class AddNewAccountantSuccessAction implements Action {
        readonly type = ADD_NEW_ACCOUNTANT_SUCCESS;

        constructor(public payload: Partial<Accountant>) {}
    }

    export class AddNewAccountantFailAction implements Action {
        readonly type = ADD_NEW_ACCOUNTANT_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class RemoveAction implements Action {
        readonly type = REMOVE;

        constructor(public payload: Accountant) {}
    }

    export class RemoveSuccessAction implements Action {
        readonly type = REMOVE_SUCCESS;

        constructor(public payload: Accountant) {}
    }

    export class RemoveFailAction implements Action {
        readonly type = REMOVE_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class LoadOneAction implements Action {
        readonly type = LOAD_ONE;

        constructor(public payload: number) {}
    }

    export class LoadOneSuccessAction implements Action {
        readonly type = LOAD_ONE_SUCCESS;

        constructor(public payload: Accountant) {}
    }

    export class LoadOneFailAction implements Action {
        readonly type = LOAD_ONE_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class SavePasswordAction implements Action {
        readonly type = SAVE_PASSWORD;

        constructor(public payload: string) {}
    }

    export class SavePasswordSuccessAction implements Action {
        readonly type = SAVE_PASSWORD_SUCCESS;
    }

    export class SavePasswordFailAction implements Action {
        readonly type = SAVE_PASSWORD_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class JoinExistingCompanyAction implements Action {
        readonly type = JOIN_EXISTING_COMPANY;

        constructor(public payload: number) {}
    }

    export class JoinExistingCompanySuccessAction implements Action {
        readonly type = JOIN_EXISTING_COMPANY_SUCCESS;
    }

    export class JoinExistingCompanyFailAction implements Action {
        readonly type = JOIN_EXISTING_COMPANY_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class NewAccountantFromInvitationAction implements Action {
        readonly type = NEW_ACCOUNTANT_FROM_INVITATION;

        constructor(public payload: any) {}
    }

    export class NewAccountantFromInvitationFailAction implements Action {
        readonly type = NEW_ACCOUNTANT_FROM_INVITATION_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class AcceptAccountantInvitationAction implements Action {
        readonly type = ACCEPT_ACCOUNTANT_INVITATION;

        constructor(public payload: number) {}
    }

    export class AcceptAccountantInvitationSuccessAction implements Action {
        readonly type = ACCEPT_ACCOUNTANT_INVITATION_SUCCESS;

        constructor(public payload: Accountant) {}
    }

    export class AcceptAccountantInvitationFailAction implements Action {
        readonly type = ACCEPT_ACCOUNTANT_INVITATION_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class RejectAccountantInvitationAction implements Action {
        readonly type = REJECT_ACCOUNTANT_INVITATION;

        constructor(public payload: number) {}
    }

    export class RejectAccountantInvitationSuccessAction implements Action {
        readonly type = REJECT_ACCOUNTANT_INVITATION_SUCCESS;

        constructor(public payload: Accountant) {}
    }

    export class RejectAccountantInvitationFailAction implements Action {
        readonly type = REJECT_ACCOUNTANT_INVITATION_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class RevokeAccountantInvitationAction implements Action {
        readonly type = REVOKE_ACCOUNTANT_INVITATION;

        constructor(public payload: Accountant) {}
    }

    export class RevokeAccountantInvitationSuccessAction implements Action {
        readonly type = REVOKE_ACCOUNTANT_INVITATION_SUCCESS;

        constructor(public payload: Accountant) {}
    }

    export class RevokeAccountantInvitationFailAction implements Action {
        readonly type = REVOKE_ACCOUNTANT_INVITATION_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class ResendAccountantInvitationAction implements Action {
        readonly type = RESEND_ACCOUNTANT_INVITATION;

        constructor(public payload: number) {}
    }

    export class ResendAccountantInvitationSuccessAction implements Action {
        readonly type = RESEND_ACCOUNTANT_INVITATION_SUCCESS;

        constructor(public payload: Accountant) {}
    }

    export class ResendAccountantInvitationFailAction implements Action {
        readonly type = RESEND_ACCOUNTANT_INVITATION_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export type Actions =
        | LoadAction
        | LoadSuccessAction
        | LoadFailAction
        | ChangeRoleAction
        | ChangeRoleSuccessAction
        | ChangeRoleFailAction
        | ChangeRoleFailAction
        | AddNewAccountantAction
        | AddNewAccountantSuccessAction
        | AddNewAccountantFailAction
        | RemoveAction
        | RemoveSuccessAction
        | RemoveFailAction
        | LoadOneAction
        | LoadOneSuccessAction
        | LoadOneFailAction
        | JoinExistingCompanyAction
        | JoinExistingCompanySuccessAction
        | JoinExistingCompanyFailAction
        | LoadOneFailAction
        | SavePasswordAction
        | SavePasswordSuccessAction
        | SavePasswordFailAction
        | NewAccountantFromInvitationAction
        | NewAccountantFromInvitationFailAction
        | AcceptAccountantInvitationAction
        | AcceptAccountantInvitationSuccessAction
        | AcceptAccountantInvitationFailAction
        | RejectAccountantInvitationAction
        | RejectAccountantInvitationSuccessAction
        | RejectAccountantInvitationFailAction
        | RevokeAccountantInvitationAction
        | RevokeAccountantInvitationSuccessAction
        | RevokeAccountantInvitationFailAction
        | ResendAccountantInvitationAction
        | ResendAccountantInvitationSuccessAction
        | ResendAccountantInvitationFailAction;
}
