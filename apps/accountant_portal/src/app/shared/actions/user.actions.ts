import { HttpErrorResponse } from '@angular/common/http';
import { Action } from '@ngrx/store';

import { Accountant, User } from '../models';

export namespace UserActions {
    export const LOAD_LOGIN_USER = '[User] Load login user';
    export const LOAD_LOGIN_USER_SUCCESS = '[User] Load login user success';
    export const LOAD_LOGIN_USER_FAIL = '[User] Load login user fail';
    export const LOAD_ACCOUNTANT_USER = '[User] Load accountant user';
    export const LOAD_ACCOUNTANT_USER_SUCCESS = '[User] Load accountant user success';
    export const LOAD_ACCOUNTANT_USER_FAIL = '[User] Load accountant user fail';
    export const CREATE_ACCOUNTANT_FROM_EXISTING_USER = '[User] Create a new accountant from existing user';
    export const CREATE_ACCOUNTANT_FROM_EXISTING_USER_SUCCESS = '[User] Create a new accountant from existing user success';
    export const CREATE_ACCOUNTANT_FROM_EXISTING_USER_FAIL = '[User] Create a new accountant from existing user fail';

    export class LoadLoginUserAction implements Action {
        readonly type = LOAD_LOGIN_USER;
    }

    export class LoadLoginUserSuccessAction implements Action {
        readonly type = LOAD_LOGIN_USER_SUCCESS;

        constructor(public payload: User) {}
    }

    export class LoadLoginUserFailAction implements Action {
        readonly type = LOAD_LOGIN_USER_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class LoadAccountantUserAction implements Action {
        readonly type = LOAD_ACCOUNTANT_USER;
    }

    export class LoadAccountantUserSuccessAction implements Action {
        readonly type = LOAD_ACCOUNTANT_USER_SUCCESS;

        constructor(public payload: Accountant) {}
    }

    export class LoadAccountantUserFailAction implements Action {
        readonly type = LOAD_ACCOUNTANT_USER_FAIL;
    }

    export class CreateAccountantFromExistingUserAction implements Action {
        readonly type = CREATE_ACCOUNTANT_FROM_EXISTING_USER;

        constructor(public payload: string) {}
    }

    export class CreateAccountantFromExistingUserSuccessAction implements Action {
        readonly type = CREATE_ACCOUNTANT_FROM_EXISTING_USER_SUCCESS;

        constructor(public payload: Accountant) {}
    }

    export class CreateAccountantFromExistingUserFailAction implements Action {
        readonly type = CREATE_ACCOUNTANT_FROM_EXISTING_USER_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export type Actions =
        | LoadLoginUserAction
        | LoadLoginUserSuccessAction
        | LoadLoginUserFailAction
        | LoadAccountantUserAction
        | LoadAccountantUserSuccessAction
        | LoadAccountantUserFailAction
        | CreateAccountantFromExistingUserAction
        | CreateAccountantFromExistingUserSuccessAction
        | CreateAccountantFromExistingUserFailAction;
}
