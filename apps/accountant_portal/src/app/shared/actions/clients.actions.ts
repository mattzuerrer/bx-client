import { HttpErrorResponse } from '@angular/common/http';
import { Action } from '@ngrx/store';

import {
    Client,
    ClientAccess,
    ClientAccountantIdCollection,
    ClientAccountantIds,
    KeyPerformanceIndicators,
    PendingClientInvites
} from '../models';

export namespace ClientsActions {
    export const LOAD = '[Clients] Load';
    export const LOAD_SUCCESS = '[Clients] Load success';
    export const LOAD_FAIL = '[Clients] Load fail';
    export const GET_LOGIN_LINK = '[Clients] Get login link action';
    export const CONNECT = '[Client] Connect';
    export const CONNECT_SUCCESS = '[Client] Connect success';
    export const CONNECT_FAIL = '[Client] Connect fail';
    export const DISCONNECT = '[Client] Disconnect';
    export const DISCONNECT_SUCCESS = '[Client] Disconnect success';
    export const DISCONNECT_FAIL = '[Client] Disconnect fail';
    export const REINVITE = '[Client] Reinvite';
    export const REINVITE_SUCCESS = '[Client] Reinvite success';
    export const REINVITE_FAIL = '[Client] Reinvite fail';
    export const REJECT = '[Client] Reject';
    export const REJECT_SUCCESS = '[Client] Reject success';
    export const REJECT_FAIL = '[Client] Reject fail';
    export const ACCEPT = '[Client] Accept';
    export const ACCEPT_SUCCESS = '[Client] Accept success';
    export const ACCEPT_FAIL = '[Client] Accept fail';
    export const LOAD_ACCESS = '[Clients] Load access';
    export const LOAD_ACCESS_SUCCESS = '[Clients] Load access success';
    export const LOAD_ACCESS_FAIL = '[Clients] Load access fail';
    export const CREATE_NEW = '[Clients] Create new';
    export const CREATE_NEW_FAIL = '[Clients] Create new fail';
    export const LOAD_PENDING_INVITES = '[Clients] Load pending invites';
    export const LOAD_PENDING_INVITES_SUCCESS = '[Clients] Load pending invites success';
    export const LOAD_PENDING_INVITES_FAIL = '[Clients] Load pending invites fail';
    export const LOAD_KEY_PERFORMANCE_INDICATORS = '[Clients] Load key performance indicators';
    export const LOAD_KEY_PERFORMANCE_INDICATORS_SUCCESS = '[Clients] Load key performance indicators success';
    export const LOAD_KEY_PERFORMANCE_INDICATORS_FAIL = '[Clients] Load key performance indicators fail';

    export class LoadAction implements Action {
        readonly type = LOAD;
    }

    export class LoadSuccessAction implements Action {
        readonly type = LOAD_SUCCESS;

        constructor(public payload: Client[]) {}
    }

    export class LoadFailAction implements Action {
        readonly type = LOAD_FAIL;
    }

    export class GetLoginLinkAction implements Action {
        readonly type = GET_LOGIN_LINK;

        constructor(public payload: number) {}
    }

    export class ConnectAction implements Action {
        readonly type = CONNECT;

        constructor(public payload: ClientAccountantIdCollection) {}
    }

    export class ConnectSuccessAction implements Action {
        readonly type = CONNECT_SUCCESS;
    }

    export class ConnectFailAction implements Action {
        readonly type = CONNECT_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class DisconnectAction implements Action {
        readonly type = DISCONNECT;

        constructor(public payload: ClientAccountantIds) {}
    }

    export class DisconnectSuccessAction implements Action {
        readonly type = DISCONNECT_SUCCESS;

        constructor(public payload: ClientAccess) {}
    }

    export class DisconnectFailAction implements Action {
        readonly type = DISCONNECT_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class ReinviteAction implements Action {
        readonly type = REINVITE;

        constructor(public payload: ClientAccountantIds) {}
    }

    export class ReinviteSuccessAction implements Action {
        readonly type = REINVITE_SUCCESS;
    }

    export class ReinviteFailAction implements Action {
        readonly type = REINVITE_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class RejectAction implements Action {
        readonly type = REJECT;

        constructor(public payload: ClientAccountantIds) {}
    }

    export class RejectFailAction implements Action {
        readonly type = REJECT_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class RejectSuccessAction implements Action {
        readonly type = REJECT_SUCCESS;

        constructor(public payload: ClientAccess) {}
    }

    export class AcceptAction implements Action {
        readonly type = ACCEPT;

        constructor(public payload: ClientAccountantIds) {}
    }

    export class AcceptSuccessAction implements Action {
        readonly type = ACCEPT_SUCCESS;

        constructor(public payload: ClientAccess) {}
    }

    export class AcceptFailAction implements Action {
        readonly type = ACCEPT_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class LoadAccessAction implements Action {
        readonly type = LOAD_ACCESS;

        constructor(public payload: number) {}
    }

    export class LoadAccessSuccessAction implements Action {
        readonly type = LOAD_ACCESS_SUCCESS;

        constructor(public payload: ClientAccess[]) {}
    }

    export class LoadAccessFailAction implements Action {
        readonly type = LOAD_ACCESS_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class CreateNewAction implements Action {
        readonly type = CREATE_NEW;
    }

    export class CreateNewFailActon implements Action {
        readonly type = CREATE_NEW_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class LoadKeyPerformanceIndicatorsAction implements Action {
        readonly type = LOAD_KEY_PERFORMANCE_INDICATORS;

        constructor(public payload: number) {}
    }

    export class LoadKeyPerformanceIndicatorsSuccessAction implements Action {
        readonly type = LOAD_KEY_PERFORMANCE_INDICATORS_SUCCESS;

        constructor(public payload: { id: number; keyPerformanceIndicators: KeyPerformanceIndicators }) {}
    }

    export class LoadKeyPerformanceIndicatorsFailAction implements Action {
        readonly type = LOAD_KEY_PERFORMANCE_INDICATORS_FAIL;

        constructor(public payload: HttpErrorResponse) {}
    }

    export class LoadPendingInvitesAction implements Action {
        readonly type = LOAD_PENDING_INVITES;
    }

    export class LoadPendingInvitesSuccessAction implements Action {
        readonly type = LOAD_PENDING_INVITES_SUCCESS;

        constructor(public payload: PendingClientInvites[]) {}
    }

    export class LoadPendingInvitesFailAction implements Action {
        readonly type = LOAD_PENDING_INVITES_FAIL;
    }

    export type Actions =
        | LoadAction
        | LoadSuccessAction
        | LoadFailAction
        | GetLoginLinkAction
        | ConnectAction
        | DisconnectAction
        | DisconnectFailAction
        | ReinviteAction
        | ReinviteSuccessAction
        | ReinviteFailAction
        | RejectAction
        | RejectSuccessAction
        | RejectFailAction
        | LoadAccessAction
        | LoadAccessSuccessAction
        | LoadAccessFailAction
        | CreateNewAction
        | CreateNewFailActon
        | AcceptAction
        | AcceptSuccessAction
        | AcceptFailAction
        | LoadPendingInvitesAction
        | LoadPendingInvitesSuccessAction
        | LoadPendingInvitesFailAction
        | LoadKeyPerformanceIndicatorsAction
        | LoadKeyPerformanceIndicatorsSuccessAction
        | LoadKeyPerformanceIndicatorsFailAction;
}
