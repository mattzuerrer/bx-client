import { Action } from '@ngrx/store';

import { GoogleAnalytics } from '../models';

export namespace AnalyticsActions {
    export const LOAD_GOOGLE_ANALYTICS = '[Analytics] Load google analytics';
    export const LOAD_GOOGLE_ANALYTICS_SUCCESS = '[Analytics] Load google analytics success';
    export const LOAD_GOOGLE_ANALYTICS_FAIL = '[Analytics] Load google analytics fail';

    export class LoadGoogleAnalyticsAction implements Action {
        readonly type = LOAD_GOOGLE_ANALYTICS;
    }

    export class LoadGoogleAnalyticsSuccessAction implements Action {
        readonly type = LOAD_GOOGLE_ANALYTICS_SUCCESS;

        constructor(public payload: GoogleAnalytics) {}
    }

    export class LoadGoogleAnalyticsFailAction implements Action {
        readonly type = LOAD_GOOGLE_ANALYTICS_FAIL;
    }
    export type Actions = LoadGoogleAnalyticsAction | LoadGoogleAnalyticsSuccessAction | LoadGoogleAnalyticsFailAction;
}
