import { PortalCompanyFile } from '../models';

/**
 * Transforms a raw buffer of binary data into a company logo.
 * @param {ArrayBuffer} rawLogo The raw binary datas from which the logo should be created
 * @param {number} companyId The ID of the company to which the logo is assigned
 * @returns {PortalCompanyFile} The logo
 */
export function createLogoFromBinaryData(rawLogo: ArrayBuffer, companyId: number): PortalCompanyFile {
    const logo = new PortalCompanyFile(companyId);
    logo.base64 = 'data:image/png;base64,' + binaryToBase64(rawLogo);
    return logo;
}

/**
 * Transforms a raw buffer of binary data into a Base64 encoded string.
 * @param {ArrayBuffer} binaryFile The raw binary datas to transform
 * @returns {string} The Base64 encoded string
 */
export function binaryToBase64(binaryFile: ArrayBuffer): string {
    const bytes = new Uint8Array(binaryFile);
    const binary = bytes.reduce<string>((previous, current) => previous += String.fromCharCode(current), '');
    return window.btoa(binary);
}
