export interface ChartData {
    name: string;
    series: [{ name: string; value: string; min?: number; max?: number }];
}
