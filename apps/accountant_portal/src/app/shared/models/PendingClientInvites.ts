import { serializeType } from '@bx-client/utils';

import { Type } from 'class-transformer';

import { Client } from './Client';
import { ClientAccess } from './ClientAccess';

export class PendingClientInvites extends ClientAccess {
    @Type(serializeType(Client))
    client: Client = new Client();
}
