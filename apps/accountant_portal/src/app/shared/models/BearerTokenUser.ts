export class BearerTokenUser {
    accountantEmail: string;
    accountantId: number;
    clientId: number;
    clientLoginUserEmail: string;
    clientLoginUserId: number;
    hasToLogin: boolean;
    id: number;
    isLoggedIn: boolean;
    token: string;
}
