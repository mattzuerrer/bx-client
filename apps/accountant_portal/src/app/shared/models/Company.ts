import { PortalCompanyFile } from './PortalCompanyFile';

export class Company {
    id: number;
    name: string;
    legal: string;
    association: string;
    address: string;
    postcode: string;
    city: string;
    country: string;
    email: string;
    phone: string;
    website: string;
    isPublic: boolean;
    isPublished: boolean;
    description: string;
    quote: string;
    pdfFilePath: string;
    companyLogo: string;
    latitude: number;
    longitude: number;
    imageUrl: string;
    pdfUrl: string;
    createdAt: Date;
    updatedAt: Date;
    logo?: PortalCompanyFile;
    pdf?: PortalCompanyFile;
}
