export class KeyPerformanceIndicators {
    amountInboxDocuments: number;
    amountUsers: number;
    clientCurrency: string;
    lastClosedTaxPeriod: LastClosedTaxPeriod;
    creditorBalance: { entries: Entry[] };
    debtorBalance: { entries: Entry[] };
}

interface Entry {
    date: Date;
    openAmount: number;
    overdueAmount: number;
}

export interface LastClosedTaxPeriod {
    type: string;
    number: string;
    year: number;
}
