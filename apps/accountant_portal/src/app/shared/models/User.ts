export class User {
    loginUserId: number;
    salutationType: string;
    firstName: string;
    lastName: string;
    language: string;
    email: string;
    profileImageUrl: string;
    profileEditUrl: string;
    isAccountantAdmin: string;
}
