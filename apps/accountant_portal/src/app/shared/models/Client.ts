import { serializeType } from '@bx-client/utils';

import { Type } from 'class-transformer';

import { Accountant } from './Accountant';
import { ClientAccess } from './ClientAccess';
import { KeyPerformanceIndicators } from './KeyPerformanceIndicators';

export const ACCEPTED_STATE = 'accepted';

export class Client {
    @Type(serializeType(ClientAccess))
    clientAccesses?: ClientAccess[];

    @Type(serializeType(KeyPerformanceIndicators))
    keyPerformanceIndicators?: KeyPerformanceIndicators;

    id: number;
    companyShortName: string;
    name: string;
    firstName: string;
    lastName: string;
    email: string;
    isTrial: boolean;
    trialEndDate: string;
    lastLogin: Date;

    get fullName(): string {
        return `${this.firstName || ''} ${this.lastName || ''}`;
    }

    hasOtherConnectedAccountants(accountantUser: Accountant): boolean {
        for (const access of this.clientAccesses) {
            if (access.accountantId !== accountantUser.id && access.status === ACCEPTED_STATE) {
                return true;
            }
        }

        return false;
    }
}
