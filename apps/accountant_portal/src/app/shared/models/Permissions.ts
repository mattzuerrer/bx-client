export interface Permissions {
    isPendingAccountant: boolean;
    isUnconnectedAccountant: boolean;
    isOwner: boolean;
    isAccountant: boolean;
    isAdmin: boolean;
}
