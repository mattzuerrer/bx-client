export class PortalCompanyFile {
    companyId: number;

    base64: any;

    name: string;

    file: File;

    constructor(companyId: number) {
        this.companyId = companyId;
        this.name = null;
        this.file = null;
        this.base64 = null;
    }

    hasFile(): boolean {
        return this.base64 && this.base64.length > 0;
    }

    hasName(): boolean {
        return this.name && this.name.length > 0;
    }
}
