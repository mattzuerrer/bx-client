import { Company } from './Company';

export class Accountant {
    companyId: number;
    createdAt: Date;
    email: string;
    firstName: string;
    id: number;
    inviteStatus: string;
    invitedById: number;
    language: string;
    lastName: string;
    loginUserId: number;
    onboardingStatus: string;
    profileImageUrl: string;
    profileEditUrl: string;
    role: string;
    salutationType: string;
    updatedAt: Date;
    roleChangeInProgress = false;
    company?: Company;

    get fullName(): string {
        return `${this.firstName} ${this.lastName}`;
    }

    get isAcceptedAccountant(): boolean {
        return this.inviteStatus === STATUS_ACCEPTED;
    }

    get isPendingAccountant(): boolean {
        return this.inviteStatus === STATUS_PENDING;
    }

    get isOnboardingPending(): boolean {
        return this.onboardingStatus === STATUS_PENDING;
    }

    get isUnconnectedAccountantUser(): boolean {
        return this.isPendingAccountant && !this.companyId;
    }
}

export const STATUS_ACCEPTED = 'accepted';
export const STATUS_PENDING = 'pending';

export const OWNER_ROLE = 'owner';
export const ACCOUNTANT_ROLE = 'accountant';
