import { Accountant } from './Accountant';

export interface ClientAccountantIds {
    clientId: number;
    id: number;
}

export interface ClientAccountantIdCollection {
    clientId: number;
    ids: Accountant[];
}
