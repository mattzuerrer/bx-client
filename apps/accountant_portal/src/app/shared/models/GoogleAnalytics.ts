export class GoogleAnalytics {
    isActivated: boolean;

    id: string;

    data: string;
}
