import { Client } from './Client';

export class ClientAccess {
    accountantAcceptedAt: Date;
    accountantClientUserId: number;
    accountantId: number;
    accountantInvitationId: number;
    accountantRejectedAt: Date;
    accountantRejectedById: number;
    clientAcceptedAt: Date;
    clientId: number;
    clientRejectedAt: Date;
    clientUserEmail: string;
    createdAt: Date;
    id: number;
    inviteType: string;
    invitedByUserId: number;
    status: string;
    updatedAt: Date;
    client?: Client;
}
