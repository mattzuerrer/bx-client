import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';

import { AccountantActions, ClientsActions } from '../actions';
import { AccountantPortalStateService } from '../services';

import { GuardConfig } from './base.guard';
import { LoggedInGuard } from './logged-in-guard.service';

@Injectable()
export class ClientGuard extends LoggedInGuard implements CanActivate {
    constructor(protected accountantPortalStateService: AccountantPortalStateService) {
        super(accountantPortalStateService);
    }

    canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
        return super.canActivate(route).pipe(tap(() => this.loadAdditionalResources()), switchMap(() => super.canActivate(route)));
    }

    private loadAdditionalResources(): void {
        const clientsResource: GuardConfig = {
            stream: this.accountantPortalStateService.clientsLoaded$,
            required: true,
            action: new ClientsActions.LoadAction()
        };

        const invitesResource: GuardConfig = {
            stream: this.accountantPortalStateService.pendingClientInvitesLoaded$,
            required: true,
            action: new ClientsActions.LoadPendingInvitesAction()
        };

        const accountantsResource: GuardConfig = {
            stream: this.accountantPortalStateService.accountantsLoaded$,
            required: true,
            action: new AccountantActions.LoadAction()
        };

        this.accountantPortalStateService.accessPermissions$.subscribe(permissions => {
            if (!permissions.isPendingAccountant) {
                if (permissions.isOwner) {
                    this.addGuardedRessources(accountantsResource);
                }

                this.addGuardedRessources(clientsResource);
                this.addGuardedRessources(invitesResource);
            }
        });
    }
}
