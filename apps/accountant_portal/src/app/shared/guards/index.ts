import { AccountantGuard } from './accountant.guard';
import { BaseGuard } from './base.guard';
import { BearerTokenGuard } from './bearer-token-guard.service';
import { ClientGuard } from './client.guard';
import { CompanyGuard } from './company.guard';
import { JoinCompanyGuard } from './join-company.guard';
import { LoggedInGuard } from './logged-in-guard.service';

export { AccountantGuard } from './accountant.guard';
export { BaseGuard } from './base.guard';
export { BearerTokenGuard } from './bearer-token-guard.service';
export { ClientGuard } from './client.guard';
export { CompanyGuard } from './company.guard';
export { JoinCompanyGuard } from './join-company.guard';
export { LoggedInGuard } from './logged-in-guard.service';

export const sharedGuards = [AccountantGuard, BaseGuard, BearerTokenGuard, ClientGuard, CompanyGuard, JoinCompanyGuard, LoggedInGuard];
