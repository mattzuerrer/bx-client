import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';

import { CompanyActions } from '../actions';
import { AccountantPortalStateService } from '../services';

import { GuardConfig } from './base.guard';
import { LoggedInGuard } from './logged-in-guard.service';

@Injectable()
export class CompanyGuard extends LoggedInGuard implements CanActivate {
    constructor(protected accountantPortalStateService: AccountantPortalStateService) {
        super(accountantPortalStateService);
    }

    canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
        const companySettings: GuardConfig = {
            stream: this.accountantPortalStateService.companySettingsLoaded$,
            required: true,
            action: new CompanyActions.LoadSettingsAction(route.params.accountantUserId)
        };

        const companyLogo: GuardConfig = {
            stream: this.accountantPortalStateService.companyLogoLoaded$,
            required: true,
            action: new CompanyActions.LoadLogoAction(route.params.accountantUserId)
        };

        const companyPdf: GuardConfig = {
            stream: this.accountantPortalStateService.companyPdfLoaded$,
            required: true,
            action: new CompanyActions.LoadPdfAction(route.params.accountantUserId)
        };

        this.addGuardedRessources(companySettings);
        this.addGuardedRessources(companyLogo);
        this.addGuardedRessources(companyPdf);

        return super.canActivate(route);
    }
}
