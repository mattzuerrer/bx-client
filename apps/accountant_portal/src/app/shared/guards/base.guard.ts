import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { identity, negation } from '@bx-client/utils';
import { Action } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { filter, take, tap } from 'rxjs/operators';

import { AccountantPortalStateService } from '../services';

export interface GuardConfig {
    stream: Observable<boolean>;
    required: boolean;
    action: Action;
}

@Injectable()
export class BaseGuard implements CanActivate {
    protected config: GuardConfig[] = [];

    constructor(protected accountantPortalStateService: AccountantPortalStateService) {}

    canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
        this.loadStates();
        return this.waitForStatesLoaded().pipe(tap(() => this.finishGuards(route)));
    }

    protected verifyClientAccess(route: ActivatedRouteSnapshot): Observable<boolean> {
        throw new Error('Method "verifyClientAccess()" from BaseGuard needs to be overwritten');
    }

    protected addGuardedRessources(resource: GuardConfig): void {
        this.config.push(resource);
    }

    private finishGuards(route: ActivatedRouteSnapshot): Observable<boolean> {
        this.accountantPortalStateService.statesLoaded$.next(true);
        return this.verifyClientAccess(route);
    }

    private loadStates(): void {
        this.config.forEach(guardConfig => {
            guardConfig.stream
                .pipe(take(1), filter(negation))
                .subscribe(() => this.accountantPortalStateService.dispatch(guardConfig.action));
        });
    }

    private waitForStatesLoaded(): Observable<boolean> {
        return combineLatest(
            ...this.config.filter(guardConfig => guardConfig.required).map(guardConfig => guardConfig.stream),
            (...loadedStates: boolean[]) => loadedStates.every(identity)
        ).pipe(filter(identity), take(1));
    }
}
