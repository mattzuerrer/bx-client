import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { RouterActions } from '@bx-client/router';
import { Observable, of } from 'rxjs';

import { BearerTokenActions } from '../actions';
import { AccountantPortalStateService, LoginRedirectPathService } from '../services';

import { BaseGuard, GuardConfig } from './base.guard';

const TOKEN_PARAM = 'token';

@Injectable()
export class BearerTokenGuard extends BaseGuard implements CanActivate {
    constructor(protected accountantPortalStateService: AccountantPortalStateService) {
        super(accountantPortalStateService);
    }

    canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
        if (!route.queryParams.hasOwnProperty(TOKEN_PARAM)) {
            this.accountantPortalStateService.dispatch(new RouterActions.Go({ path: ['clients'] }));
            return of(false);
        }

        const verifyBearerTokenResource: GuardConfig = {
            stream: this.accountantPortalStateService.tokenUserLoaded$,
            required: true,
            action: new BearerTokenActions.VerifyBearerTokenAction(route.queryParams[TOKEN_PARAM])
        };
        this.addGuardedRessources(verifyBearerTokenResource);

        return super.canActivate(route);
    }

    protected verifyClientAccess(route: ActivatedRouteSnapshot): Observable<boolean> {
        this.redirectToSetupIfUnfinished(route);
        this.checkRoles(route);
        this.accountantPortalStateService.statesLoaded$.next(true);

        return of(true);
    }

    private redirectToSetupIfUnfinished(route: ActivatedRouteSnapshot): void {
        this.accountantPortalStateService.tokenUser$.subscribe(tokenUser => {
            LoginRedirectPathService.redirectBearerTokenLogin(tokenUser, route.queryParams[TOKEN_PARAM]);
        });
    }

    private checkRoles(route: ActivatedRouteSnapshot): void {
        if (!!route.data && !!route.data.needsRole) {
            let permissions = {};
            this.accountantPortalStateService.tokenUserAccessPermissions$.subscribe(result => (permissions = result));

            route.data.needsRole.forEach(role => {
                if (!permissions[role]) {
                    this.accountantPortalStateService.dispatch(new RouterActions.Go({ path: ['clients'] }));
                    return of(false);
                }
            });
        }
    }
}
