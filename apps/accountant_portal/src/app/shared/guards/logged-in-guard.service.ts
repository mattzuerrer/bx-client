import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { RouterActions } from '@bx-client/router';
import { Observable, of } from 'rxjs';

import { AnalyticsActions, UserActions } from '../actions';
import { AccountantPortalStateService, LoginRedirectPathService } from '../services';

import { BaseGuard, GuardConfig } from './base.guard';

@Injectable()
export class LoggedInGuard extends BaseGuard {
    accountantUserResource: GuardConfig = {
        stream: this.accountantPortalStateService.accountantUserLoaded$,
        required: true,
        action: new UserActions.LoadAccountantUserAction()
    };

    loginUserResource: GuardConfig = {
        stream: this.accountantPortalStateService.loginUserLoaded$,
        required: true,
        action: new UserActions.LoadLoginUserAction()
    };

    analyticsResource: GuardConfig = {
        stream: this.accountantPortalStateService.googleAnalyticsLoaded$,
        required: true,
        action: new AnalyticsActions.LoadGoogleAnalyticsAction()
    };

    constructor(protected accountantPortalStateService: AccountantPortalStateService) {
        super(accountantPortalStateService);

        this.addGuardedRessources(this.accountantUserResource);
        this.addGuardedRessources(this.loginUserResource);
        this.addGuardedRessources(this.analyticsResource);
    }

    canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
        return super.canActivate(route);
    }

    protected verifyClientAccess(route: ActivatedRouteSnapshot): Observable<boolean> {
        this.redirectToSetupIfUnfinished();
        this.checkRoles(route);
        this.accountantPortalStateService.statesLoaded$.next(true);

        return of(true);
    }

    private redirectToSetupIfUnfinished(): void {
        this.accountantPortalStateService.accountantUser$.subscribe(accountantUser => {
            LoginRedirectPathService.redirectAccountantLogin(accountantUser);
        });
    }

    private checkRoles(route: ActivatedRouteSnapshot): void {
        if (!!route.data) {
            let permissions = {};
            this.accountantPortalStateService.accessPermissions$.subscribe(result => (permissions = result));

            if (!!route.data.needsRole) {
                route.data.needsRole.forEach(role => {
                    if (!permissions[role]) {
                        this.accountantPortalStateService.dispatch(new RouterActions.Go({ path: ['clients'] }));
                    }
                });
            }

            if (!!route.data.containsRole) {
                let hasRole = false;
                route.data.containsRole.forEach(role => {
                    if (route.data.containsRole.includes(role) && permissions[role]) {
                        hasRole = true;
                    }
                });

                if (!hasRole) {
                    this.accountantPortalStateService.dispatch(new RouterActions.Go({ path: ['clients'] }));
                }
            }
        }
    }
}
