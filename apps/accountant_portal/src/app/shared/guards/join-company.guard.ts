import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';

import { CompanyActions } from '../actions';
import { AccountantPortalStateService } from '../services';

import { GuardConfig } from './base.guard';
import { LoggedInGuard } from './logged-in-guard.service';

@Injectable()
export class JoinCompanyGuard extends LoggedInGuard implements CanActivate {
    constructor(protected accountantPortalStateService: AccountantPortalStateService) {
        super(accountantPortalStateService);
    }

    canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
        const companiesResource: GuardConfig = {
            stream: this.accountantPortalStateService.companiesLoaded$,
            required: true,
            action: new CompanyActions.LoadCollectionAction()
        };

        this.addGuardedRessources(companiesResource);

        return super.canActivate(route);
    }
}
