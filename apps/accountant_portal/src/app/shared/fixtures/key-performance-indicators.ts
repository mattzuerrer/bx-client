import { plainToClass } from 'class-transformer';

import { KeyPerformanceIndicators } from '../models';

export const keyPerformanceIndicators: KeyPerformanceIndicators = plainToClass(KeyPerformanceIndicators, {
    amountInboxDocuments: 2,
    amountUsers: 3,
    clientCurrency: 'CHF',
    lastClosedTaxPeriod: null
});
