import { Company, PortalCompanyFile } from '../models';

export const company: Company = {
    address: '1187 Hunterwasser',
    association: 'none',
    city: 'Musterstadt',
    companyLogo: null,
    country: 'CH',
    createdAt: new Date(),
    description: null,
    email: 'info@124accounting.com',
    id: 1,
    imageUrl: '',
    isPublic: true,
    isPublished: false,
    latitude: null,
    legal: 'corporation',
    longitude: null,
    name: 'SmartAccounting Inc.',
    pdfFilePath: null,
    pdfUrl: null,
    phone: '0412223344',
    postcode: '90210',
    quote: null,
    updatedAt: new Date(),
    website: 'http://124accounting.com',
    logo: new PortalCompanyFile(1),
    pdf: new PortalCompanyFile(1)
};
