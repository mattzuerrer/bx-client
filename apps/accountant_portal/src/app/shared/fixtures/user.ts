import { User } from '../models';

export const user: User = {
    email: 'lester.bryant@124accounting.com',
    firstName: 'Lester',
    isAccountantAdmin: '',
    language: 'en_GB',
    lastName: 'Bryant',
    loginUserId: 12,
    profileEditUrl: 'http://my.bexio.bx/user/profile',
    profileImageUrl: 'http://my.bexio.bx/img/profile_picture/CDQRyt_Sr7qaDIZWlShoxg.png',
    salutationType: 'male'
};
