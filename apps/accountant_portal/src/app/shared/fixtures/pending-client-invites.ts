import { plainToClass } from 'class-transformer';

import { PendingClientInvites } from '../models';

export const pendingClientInvites: PendingClientInvites[] = plainToClass(PendingClientInvites, [
    {
        client: {
            id: 12,
            companyShortName: 'ihf87eqphrfa',
            name: 'DogFood.Inc',
            firstName: null,
            lastName: null,
            email: 'lester.bryant@124accounting.com',
            isTrial: false,
            trialEndDate: '2019-01-12T10:30:56+0000',
            clientAccesses: [
                {
                    client: null,
                    id: 19,
                    accountantId: 1,
                    accountantInvitationId: null,
                    accountantClientUserId: 138,
                    clientId: null,
                    invitedByUserId: null,
                    clientUserEmail: null,
                    accountantAcceptedAt: '2018-12-13T10:30:57+0000',
                    accountantRejectedAt: null,
                    accountantRejectedById: null,
                    clientAcceptedAt: '2018-12-13T10:30:57+0000',
                    clientRejectedAt: null,
                    status: 'accepted',
                    inviteType: 'self',
                    createdAt: '2018-12-13T10:30:57+0000',
                    updatedAt: '2018-12-13T10:30:57+0000'
                },
                {
                    client: null,
                    id: 20,
                    accountantId: null,
                    accountantInvitationId: 19,
                    accountantClientUserId: null,
                    clientId: null,
                    invitedByUserId: 12,
                    clientUserEmail: 'lester.bryant@124accounting.com',
                    accountantAcceptedAt: null,
                    accountantRejectedAt: null,
                    accountantRejectedById: null,
                    clientAcceptedAt: '2018-12-13T10:34:30+0000',
                    clientRejectedAt: null,
                    status: 'pending',
                    inviteType: 'client',
                    createdAt: '2018-12-13T10:34:30+0000',
                    updatedAt: '2018-12-13T10:34:30+0000'
                }
            ],
            lastLogin: null
        },
        id: 21,
        accountantId: 39,
        accountantInvitationId: null,
        accountantClientUserId: null,
        clientId: 12,
        invitedByUserId: 12,
        clientUserEmail: 'lester.bryant@124accounting.com',
        accountantAcceptedAt: null,
        accountantRejectedAt: null,
        accountantRejectedById: null,
        clientAcceptedAt: '2018-12-13T10:41:53+0000',
        clientRejectedAt: null,
        status: 'pending',
        inviteType: 'client',
        createdAt: '2018-12-13T10:41:53+0000',
        updatedAt: '2018-12-13T10:41:53+0000'
    },
    {
        client: {
            id: 6,
            companyShortName: 'bvcx2rdepl0m',
            name: 'Xtra Corp',
            firstName: null,
            lastName: null,
            email: 'lester.bryant@124accounting.com',
            isTrial: true,
            trialEndDate: '2018-12-30T14:57:49+0000',
            clientAccesses: [
                {
                    client: null,
                    id: 8,
                    accountantId: 1,
                    accountantInvitationId: null,
                    accountantClientUserId: 131,
                    clientId: null,
                    invitedByUserId: null,
                    clientUserEmail: null,
                    accountantAcceptedAt: '2018-11-30T14:57:49+0000',
                    accountantRejectedAt: null,
                    accountantRejectedById: null,
                    clientAcceptedAt: '2018-11-30T14:57:49+0000',
                    clientRejectedAt: null,
                    status: 'accepted',
                    inviteType: 'self',
                    createdAt: '2018-11-30T14:57:49+0000',
                    updatedAt: '2018-11-30T14:57:49+0000'
                }
            ],
            lastLogin: null
        },
        id: 22,
        accountantId: 39,
        accountantInvitationId: null,
        accountantClientUserId: null,
        clientId: 6,
        invitedByUserId: 12,
        clientUserEmail: 'lester.bryant@124accounting.com',
        accountantAcceptedAt: null,
        accountantRejectedAt: null,
        accountantRejectedById: null,
        clientAcceptedAt: '2018-12-13T12:54:35+0000',
        clientRejectedAt: null,
        status: 'pending',
        inviteType: 'client',
        createdAt: '2018-12-13T12:54:35+0000',
        updatedAt: '2018-12-13T12:54:35+0000'
    }
]);
