import { plainToClass } from 'class-transformer';

import { Accountant } from '../models';

export const accountants: Accountant[] = plainToClass(Accountant, [
    {
        id: 1,
        firstName: 'Lester',
        lastName: 'Bryant',
        email: 'lester.bryant@124accounting.com',
        role: 'owner',
        salutationType: 'male',
        language: 'en_GB',
        loginUserId: 12,
        onboardingStatus: 'finished',
        companyId: 1,
        invitedById: null,
        inviteStatus: 'accepted',
        createdAt: new Date(),
        updatedAt: new Date(),
        profileImageUrl: 'http://my.bexio.bx/img/profile_picture/CDQRyt_Sr7qaDIZWlShoxg.png',
        profileEditUrl: 'http://my.bexio.bx/user/profile',
        roleChangeInProgress: false
    },
    {
        id: 2,
        firstName: 'Derrick',
        lastName: 'Montgomery',
        email: 'derick.montgomery@124accounting.com',
        role: 'accountant',
        salutationType: 'male',
        language: 'en_GB',
        loginUserId: 13,
        onboardingStatus: 'finished',
        companyId: 1,
        invitedById: null,
        inviteStatus: 'accepted',
        createdAt: new Date(),
        updatedAt: new Date(),
        profileImageUrl: 'http://my.bexio.bx/img/profile_picture/GVnNvYxQgdCAzSBss9smDA.png',
        profileEditUrl: 'http://my.bexio.bx/user/profile',
        roleChangeInProgress: false
    },
    {
        id: 3,
        firstName: 'John',
        lastName: 'Doe',
        email: 'john.doe@124accounting.com',
        role: 'accountant',
        salutationType: 'male',
        language: 'en_GB',
        loginUserId: 14,
        onboardingStatus: 'finished',
        companyId: 1,
        invitedById: null,
        inviteStatus: 'accepted',
        createdAt: new Date(),
        updatedAt: new Date(),
        profileImageUrl: 'http://my.bexio.bx/img/profile_picture/GVnNvYxQgdCAzSBss9smDA.png',
        profileEditUrl: 'http://my.bexio.bx/user/profile',
        roleChangeInProgress: false
    }
]);
