import { plainToClass } from 'class-transformer';

import { Client } from '../models';

export const clients: Client[] = plainToClass(Client, [
    {
        id: 1,
        companyShortName: 'tfjz3lpmklqc',
        name: 'Edge Garden Services',
        firstName: 'Karl',
        lastName: 'Hensen',
        email: 'karl.hensen@bexio.dev',
        isTrial: true,
        trialEndDate: '2020-10-17T13:52:42+0000',
        lastLogin: null
    },
    {
        id: 4,
        companyShortName: '4ya9xe5kitfd',
        name: null,
        firstName: null,
        lastName: null,
        email: 'lester.bryant@124accounting.com',
        isTrial: true,
        trialEndDate: '2018-11-21T11:54:34+0000',
        lastLogin: '2018-10-22T11:54:39+0000'
    }
]);
