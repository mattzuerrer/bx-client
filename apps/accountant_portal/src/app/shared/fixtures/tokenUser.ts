import { BearerTokenUser } from '../models';

export const bearerTokenUser: BearerTokenUser = {
    accountantEmail: 'fabian.baumgartner+accountant-test@bexio.com',
    accountantId: null,
    clientId: 3,
    clientLoginUserEmail: 'devtesting9@bexio.com',
    clientLoginUserId: 11,
    hasToLogin: false,
    id: 17,
    isLoggedIn: true,
    token: 'ef4cb7dc5677566f3b71a9b201d17805f4479f1a'
};
