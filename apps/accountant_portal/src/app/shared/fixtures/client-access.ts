import { plainToClass } from 'class-transformer';

import { ClientAccess } from '../models';

export const clientAccess: ClientAccess = plainToClass(ClientAccess, {
    accountantAcceptedAt: '2020-10-17T13:52:42+0000',
    accountantClientUserId: 1,
    accountantId: 2,
    accountantInvitationId: 3,
    accountantRejectedAt: '2020-10-17T13:52:42+0000',
    accountantRejectedById: 5,
    clientAcceptedAt: '2020-10-17T13:52:42+0000',
    clientId: 4,
    clientRejectedAt: '2020-10-17T13:52:42+0000',
    clientUserEmail: 'john@email.com',
    createdAt: '2020-10-17T13:52:42+0000',
    id: 6,
    inviteType: '',
    invitedByUserId: 7,
    status: '',
    updatedAt: '2020-10-17T13:52:42+0000'
});
