import { GoogleAnalytics } from '../models';

export const googleAnalytics: GoogleAnalytics = {
    isActivated: true,
    id: 'GTM-TestKey',
    data: '{"user_profile_loginId":1,"testFixture":true}'
};
