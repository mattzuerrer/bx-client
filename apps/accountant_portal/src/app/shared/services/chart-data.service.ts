import { formatDate } from '@angular/common';
import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { GlobalizeService } from '@bx-client/i18n';
import { TranslateService } from '@ngx-translate/core';

import { ChartData, KeyPerformanceIndicators } from '../models';

@Injectable()
export class ChartDataService {
    dateFormat = this.globalizeService.getFormatFromMatDatepicker();

    constructor(
        private translateService: TranslateService,
        @Inject(LOCALE_ID) private locale: string,
        private globalizeService: GlobalizeService
    ) {}

    convertToChartData(data: KeyPerformanceIndicators, balance: string): ChartData[] {
        const legendLabelOpenData = this.translateService.instant('accountant_portal.clients.chart_data.legend_label_open_data');
        const legendLabelOverdueData = this.translateService.instant('accountant_portal.clients.chart_data.legend_label_overdue_data');

        const convertedData = [];
        const seriesOpen = [];
        const seriesOverdue = [];

        for (const entry of data[balance].entries) {
            seriesOpen.push({ name: formatDate(entry.date, this.dateFormat, this.locale), value: entry.openAmount });
            seriesOverdue.push({ name: formatDate(entry.date, this.dateFormat, this.locale), value: entry.overdueAmount });
        }

        convertedData.push({ name: legendLabelOpenData, series: seriesOpen });
        convertedData.push({ name: legendLabelOverdueData, series: seriesOverdue });
        return convertedData;
    }
}
