import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EMPTY, Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class PortalHttpInterceptorService implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let headers = req.headers;
        headers = headers.set('Accept', 'application/json');
        const clone = req.clone({ headers, withCredentials: true });

        return next.handle(clone).pipe(
            catchError(response => {
                if (response.status === 401) {
                    window.location.replace(response.error.location);
                    return EMPTY;
                }

                if (response.status === 403 && window.location.pathname !== '/clients' && window.location.pathname !== '/403') {
                    window.location.replace('/clients');
                    return EMPTY;
                }

                if (response.status === 403 && window.location.pathname !== '/403') {
                    window.location.replace('/403');
                    return EMPTY;
                }

                return throwError(response);
            })
        );
    }
}
