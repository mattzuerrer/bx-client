import { Injectable } from '@angular/core';
import { MessageActions } from '@bx-client/message';

import { AccountantPortalStateService } from './accountant-portal-state.service';

@Injectable()
export class PortalMessageService {
    constructor(private accountantPortalStateService: AccountantPortalStateService) {}

    showSuccessMessage(message: string, translationVariables: object = null): void {
        this.accountantPortalStateService.dispatch(
            new MessageActions.SuccessMessage({
                messageKey: message,
                options: { data: { translateParams: translationVariables } }
            })
        );
    }

    showErrorMessage(httpState: number, translationVariables: object = null): void {
        const message = this.getMessageFromMap(httpState);

        if (message) {
            this.accountantPortalStateService.dispatch(
                new MessageActions.ErrorMessage({
                    messageKey: message,
                    options: { data: { translateParams: translationVariables } }
                })
            );
        }
    }

    showErrorMessageFromPlainString(message: string): void {
        this.accountantPortalStateService.dispatch(new MessageActions.ErrorMessage({ messageKey: message }));
    }

    private getMessageFromMap(httpState: number): string {
        const messages = this.getGeneralErrorMap();

        let message = messages[500];
        if (messages.hasOwnProperty(httpState)) {
            message = messages[httpState];
        }

        return message;
    }

    private getGeneralErrorMap(): object {
        return {
            403: 'accountant_portal.messages.error.403',
            404: 'accountant_portal.messages.error.404',
            422: 'accountant_portal.messages.error.422',
            500: 'accountant_portal.messages.error.500',
            503: 'accountant_portal.messages.error.503'
        };
    }
}
