import { HttpClient } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { UtilitiesService } from '@bx-client/common';
import { csnToken } from '@bx-client/env';
import { httpApiProviderToken, HttpService } from '@bx-client/http';
import { HttpClientMockProvider, HttpServiceMockProvider } from '@bx-client/http/src/mocks';
import { plainToClass } from 'class-transformer';
import { noop, of } from 'rxjs';

import {
    accountants,
    bearerTokenUser,
    clientAccess,
    clients,
    company,
    keyPerformanceIndicators,
    pendingClientInvites,
    user
} from '../fixtures';
import {
    Accountant,
    BearerTokenUser,
    Client,
    ClientAccess,
    Company,
    KeyPerformanceIndicators,
    PendingClientInvites,
    User
} from '../models';

import { ApiService } from './api.service';

const apiProvider = {
    provide: httpApiProviderToken,
    useValue: 'API'
};

const csnProvider = {
    provide: csnToken,
    useValue: 'CSN'
};

const utilitiesServiceStub = {
    camelToSnake: () => noop()
};

describe('Api service', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                ApiService,
                HttpClientMockProvider,
                HttpServiceMockProvider,
                apiProvider,
                csnProvider,
                {
                    provide: UtilitiesService,
                    useValue: utilitiesServiceStub
                }
            ]
        });
    });

    it(
        'should fetch clients',
        inject([ApiService, HttpService], (apiService, httpService) => {
            spyOn(httpService, 'get').and.returnValue(of(plainToClass(Client, clients)));

            apiService.getClients().subscribe(result => {
                expect(httpService.get).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(Client, clients));
            });
        })
    );

    it(
        'should fetch key performance indicators',
        inject([ApiService, HttpService], (apiService, httpService) => {
            spyOn(httpService, 'get').and.returnValue(of(plainToClass(KeyPerformanceIndicators, keyPerformanceIndicators)));

            apiService.loadKeyPerformanceIndicators(1).subscribe(result => {
                expect(httpService.get).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(KeyPerformanceIndicators, keyPerformanceIndicators));
            });
        })
    );

    it(
        'should fetch login user',
        inject([ApiService, HttpService], (apiService, httpService) => {
            spyOn(httpService, 'get').and.returnValue(of(plainToClass(User, user)));

            apiService.getLoginUser().subscribe(result => {
                expect(httpService.get).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(User, user));
            });
        })
    );

    it(
        'should fetch accountants',
        inject([ApiService, HttpService], (apiService, httpService) => {
            spyOn(httpService, 'get').and.returnValue(of(plainToClass(Accountant, accountants)));

            apiService.getAccountants().subscribe(result => {
                expect(httpService.get).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(Accountant, accountants));
            });
        })
    );

    it(
        'should fetch logged in accountant',
        inject([ApiService, HttpService], (apiService, httpService) => {
            spyOn(httpService, 'get').and.returnValue(of(plainToClass(Accountant, accountants[0])));

            apiService.getAccountantUser().subscribe(result => {
                expect(httpService.get).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(Accountant, accountants[0]));
            });
        })
    );

    it(
        'should accept accountant invitation',
        inject([ApiService, HttpService], (apiService, httpService) => {
            spyOn(httpService, 'post').and.returnValue(of(plainToClass(Accountant, accountants[0])));

            apiService.acceptAccountantInvitation(accountants[0]).subscribe(result => {
                expect(httpService.post).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(Accountant, accountants[0]));
            });
        })
    );

    it(
        'should reject accountant invitation',
        inject([ApiService, HttpService], (apiService, httpService) => {
            spyOn(httpService, 'post').and.returnValue(of(plainToClass(Accountant, accountants[0])));

            apiService.rejectAccountantInvitation(accountants[0]).subscribe(result => {
                expect(httpService.post).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(Accountant, accountants[0]));
            });
        })
    );

    it(
        'should revoke accountant invitation',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const response = {};
            spyOn(httpService, 'remove').and.returnValue(of(response));
            apiService.revokeAccountantInvitation(accountants[0]).subscribe(result => {
                expect(httpService.remove).toHaveBeenCalledTimes(1);
                expect(result).toBe(response);
            });
        })
    );

    it(
        'should resend accountant invitation',
        inject([ApiService, HttpService], (apiService, httpService) => {
            spyOn(httpService, 'post').and.returnValue(of(plainToClass(Accountant, accountants[0])));

            apiService.reinviteAccountant(accountants[0]).subscribe(result => {
                expect(httpService.post).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(Accountant, accountants[0]));
            });
        })
    );

    it(
        'should change accountants role',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const changedAccountant = Object.assign(accountants[0], { role: 'new role' });
            const payload = { id: changedAccountant.id, role: changedAccountant.role };

            spyOn(httpService, 'patch').and.returnValue(of(plainToClass(Accountant, changedAccountant)));

            apiService.changeRole(payload).subscribe(result => {
                expect(httpService.patch).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(Accountant, accountants[0]));
            });
        })
    );

    it(
        'should invite accountant',
        inject([ApiService, HttpService, UtilitiesService], (apiService, httpService, utilitiesService) => {
            const payload = accountants[0];

            spyOn(utilitiesService, 'camelToSnake').and.callThrough();
            spyOn(httpService, 'post').and.returnValue(of(plainToClass(Accountant, accountants[0])));

            apiService.inviteAccountant(payload).subscribe(result => {
                expect(httpService.post).toHaveBeenCalledTimes(1);
                expect(utilitiesService.camelToSnake).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(Accountant, accountants[0]));
            });
        })
    );

    it(
        'should fetch company settings',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const payload = company.id;
            spyOn(httpService, 'get').and.returnValue(of(plainToClass(Company, company)));

            apiService.getCompanySettings(payload).subscribe(result => {
                expect(httpService.get).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(Company, company));
            });
        })
    );

    it(
        'should save company settings',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const payload = { id: 1 };

            spyOn(httpService, 'patch').and.returnValue(of(plainToClass(Company, company)));

            apiService.saveCompanySettings(company, payload.id).subscribe(result => {
                expect(httpService.patch).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(Company, company));
            });
        })
    );

    it(
        'should create or update new or existing company',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const payload = { id: 1 };

            spyOn(httpService, 'patch').and.returnValue(of(plainToClass(Company, company)));

            apiService.createOrUpdateNewCompany(company, payload.id).subscribe(result => {
                expect(httpService.patch).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(Company, company));
            });

            spyOn(httpService, 'post').and.returnValue(of(plainToClass(Company, company)));

            apiService.createOrUpdateNewCompany(company).subscribe(result => {
                expect(httpService.patch).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(Company, company));
            });
        })
    );

    it(
        'should fetch company logo',
        inject([ApiService, HttpClient], (apiService, httpClient) => {
            const payload = company.id;
            const logoReturn = new ArrayBuffer(1);
            spyOn(httpClient, 'get').and.returnValue(of(logoReturn));

            apiService.getCompanyLogo(payload).subscribe(result => {
                expect(httpClient.get).toHaveBeenCalledTimes(1);
                expect(result).toEqual(logoReturn);
            });
        })
    );

    it(
        'should save company logo',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const payload = company.id;
            spyOn(httpService, 'post').and.returnValue(of(plainToClass(Company, company)));

            apiService.saveCompanyLogo(payload).subscribe(result => {
                expect(httpService.post).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(Company, company));
            });
        })
    );

    it(
        'should create or update logo for new or existing company',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const payload = company.id;
            spyOn(httpService, 'post').and.returnValue(of(plainToClass(Company, company)));

            apiService.createOrUpdateNewCompanyLogo(company, payload).subscribe(result => {
                expect(httpService.post).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(Company, company));
            });
        })
    );

    it(
        'should create a new user based on a existing one',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const payload = 'my-test-bearer-token';
            spyOn(httpService, 'post').and.returnValue(of(plainToClass(Accountant, accountants[0])));

            apiService.createAccountantFromExistingUser(payload).subscribe(result => {
                expect(httpService.post).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(Accountant, accountants[0]));
            });
        })
    );

    it(
        'should validate the bearer token',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const payload = 'my-test-bearer-token';
            spyOn(httpService, 'get').and.returnValue(of(plainToClass(BearerTokenUser, bearerTokenUser)));

            apiService.validateAccountantToken(payload).subscribe(result => {
                expect(httpService.get).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(BearerTokenUser, bearerTokenUser));
            });
        })
    );

    it(
        'should fetch company pdf',
        inject([ApiService, HttpClient], (apiService, httpClient) => {
            const payload = company.id;
            const pdfReturn = new ArrayBuffer(1);
            spyOn(httpClient, 'get').and.returnValue(of(pdfReturn));

            apiService.getCompanyPdf(payload).subscribe(result => {
                expect(httpClient.get).toHaveBeenCalledTimes(1);
                expect(result).toEqual(pdfReturn);
            });
        })
    );

    it(
        'should save company pdf',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const payload = company.id;
            spyOn(httpService, 'post').and.returnValue(of(plainToClass(Company, company)));

            apiService.saveCompanyPdf(payload).subscribe(result => {
                expect(httpService.post).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(Company, company));
            });
        })
    );

    it(
        'should create or update pdf for new or existing company',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const payload = company.id;
            spyOn(httpService, 'post').and.returnValue(of(plainToClass(Company, company)));

            apiService.createOrUpdateNewCompanyPdf(company, payload).subscribe(result => {
                expect(httpService.post).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(Company, company));
            });
        })
    );

    it(
        'should fetch pending client invites',
        inject([ApiService, HttpService], (apiService, httpService) => {
            spyOn(httpService, 'get').and.returnValue(of(plainToClass(PendingClientInvites, pendingClientInvites)));

            apiService.getPendingClientInvites().subscribe(result => {
                expect(httpService.get).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(PendingClientInvites, pendingClientInvites));
            });
        })
    );

    it(
        'should reject accountant invitation for a client',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const payload = { clientId: 5, id: 21 };
            spyOn(httpService, 'post').and.returnValue(of(plainToClass(ClientAccess, clientAccess)));

            apiService.rejectClientAccountantInvitation(payload).subscribe(result => {
                expect(httpService.post).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(ClientAccess, clientAccess));
            });
        })
    );

    it(
        'should accept accountant invitation for a client',
        inject([ApiService, HttpService], (apiService, httpService) => {
            const payload = { clientId: 5, id: 21 };
            spyOn(httpService, 'post').and.returnValue(of(plainToClass(ClientAccess, clientAccess)));

            apiService.acceptClientAccountantInvitation(payload).subscribe(result => {
                expect(httpService.post).toHaveBeenCalledTimes(1);
                expect(result).toEqual(plainToClass(ClientAccess, clientAccess));
            });
        })
    );
});
