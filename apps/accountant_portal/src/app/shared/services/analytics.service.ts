import { Injectable } from '@angular/core';

import { GoogleAnalyticsService } from './google-analytics.service';

@Injectable()
export class AnalyticsService {
    constructor(private googleAnalyticsService: GoogleAnalyticsService) {}

    process(): void {
        this.googleAnalyticsService.process();
    }
}
