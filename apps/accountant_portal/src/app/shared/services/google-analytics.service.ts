import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';

import { GoogleAnalytics } from '../models';

import { AccountantPortalStateService } from './accountant-portal-state.service';

const GOOGLE_SCRIPT_ID = 'bx-google-tag-manager-script';

@Injectable()
export class GoogleAnalyticsService {
    googleAnalytics: GoogleAnalytics;

    constructor(private accountantPortalStateService: AccountantPortalStateService, @Inject(DOCUMENT) private document: any) {
        const googleAnalytics$ = this.accountantPortalStateService.googleAnalyticsConfiguration$;
        googleAnalytics$.subscribe(result => (this.googleAnalytics = result));
    }

    process(): void {
        const googleAnalyticsAdded = !!this.document.getElementById(GOOGLE_SCRIPT_ID);

        if (!this.googleAnalytics.isActivated) {
            return;
        }

        if (!googleAnalyticsAdded) {
            this.addGoogleAnalyticsDataLayerToHead(this.googleAnalytics.data);
            this.addGoogleAnalyticsScriptToHead(this.googleAnalytics.id);
        }
    }

    private addGoogleAnalyticsDataLayerToHead(gaDataLayer: string): void {
        const gaScript = this.document.createElement('script');
        gaScript.type = 'text/javascript';
        gaScript.innerHTML = 'window.dataLayer = window.dataLayer || []; dataLayer.push(' + gaDataLayer + ');';

        const head = this.document.getElementsByTagName('head')[0];
        head.appendChild(gaScript);
    }

    private addGoogleAnalyticsScriptToHead(gaID: string): void {
        const gaScript = this.document.createElement('script');
        gaScript.type = 'text/javascript';
        gaScript.id = GOOGLE_SCRIPT_ID;
        gaScript.innerHTML =
            '(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push' +
            "({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)" +
            "[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=" +
            "'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})" +
            "(window,document,'script','dataLayer','" +
            gaID +
            "');";

        const head = this.document.getElementsByTagName('head')[0];
        head.appendChild(gaScript);
    }
}
