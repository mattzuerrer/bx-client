import { Inject, Injectable } from '@angular/core';
import { apiToken, appEnvironmentToken, csnToken } from '@bx-client/env';
import { BaseHttpApiProviderService } from '@bx-client/http';

import { devBaseApi, prodBaseApi } from '../../../config';

@Injectable()
export class PortalHttpApiProviderService extends BaseHttpApiProviderService {
    constructor(
        @Inject(appEnvironmentToken) environment: AppEnvironment,
        @Inject(apiToken) baseApi: string,
        @Inject(csnToken) csn: string
    ) {
        super(baseApi);
        baseApi = environment.isProduction ? prodBaseApi : devBaseApi;
        this.api = `${baseApi}/api`;
    }
}
