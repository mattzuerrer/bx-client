import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { httpApiProviderToken } from '@bx-client/http';
import { LanguageService } from '@bx-client/i18n';

import { AccountantPortalStateService } from './accountant-portal-state.service';
import { AnalyticsService } from './analytics.service';
import { ApiService } from './api.service';
import { ChartDataService } from './chart-data.service';
import { GoogleAnalyticsService } from './google-analytics.service';
import { LoginRedirectPathService } from './login-redirect-path.service';
import { PortalHttpApiProviderService } from './portal-http-api-provider.service';
import { PortalHttpInterceptorService } from './portal-http-interceptor.service';
import { PortalMessageService } from './portal-message.service';

export { ApiService } from './api.service';
export { AnalyticsService } from './analytics.service';
export { AccountantPortalStateService } from './accountant-portal-state.service';
export { ChartDataService } from './chart-data.service';
export { GoogleAnalyticsService } from './google-analytics.service';
export { LoginRedirectPathService } from './login-redirect-path.service';
export { PortalMessageService } from './portal-message.service';

export const sharedServices = [
    AccountantPortalStateService,
    AnalyticsService,
    ApiService,
    PortalMessageService,
    LoginRedirectPathService,
    GoogleAnalyticsService,
    ChartDataService
];

export const sharedProviders = [
    LanguageService,
    { provide: HTTP_INTERCEPTORS, useClass: PortalHttpInterceptorService, multi: true },
    { provide: httpApiProviderToken, useClass: PortalHttpApiProviderService }
];
