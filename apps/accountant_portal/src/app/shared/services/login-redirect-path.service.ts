import { Injectable } from '@angular/core';

import {
    COMPONENT_SETUP_BASE_URL,
    COMPONENT_SETUP_COMPANY_URL,
    COMPONENT_SETUP_CREATE_NEW_ACCOUNTANT_URL,
    COMPONENT_SETUP_JOIN_COMPANY_URL,
    COMPONENT_SETUP_PASSWORD_URL,
    COMPONENT_SETUP_STATUS_FORBIDDEN
} from '../../../config';
import { Accountant, BearerTokenUser } from '../models';

@Injectable()
export class LoginRedirectPathService {
    static redirectBearerTokenLogin(userObject: BearerTokenUser, token?: string): void {
        const pathName = window.location.pathname;

        if (pathName !== COMPONENT_SETUP_BASE_URL) {
            return;
        }

        // Token user gets redirected to "Create new Accountant" for user without accountant
        if (!userObject.accountantId) {
            window.location.replace(`${COMPONENT_SETUP_CREATE_NEW_ACCOUNTANT_URL}?token=${token}`);
        }

        // Token user gets redirected to "Set password" for an newly invited accountant (used via website for bexio.com)
        if (userObject.accountantId) {
            window.location.replace(`${COMPONENT_SETUP_PASSWORD_URL}?token=${token}`);
        }
    }

    static redirectAccountantLogin(userObject: Accountant): void {
        const pathName = window.location.pathname;
        const companySetupUrls = [COMPONENT_SETUP_JOIN_COMPANY_URL, COMPONENT_SETUP_COMPANY_URL];

        if (pathName === COMPONENT_SETUP_STATUS_FORBIDDEN) {
            return;
        }

        // Logged-in user without company gets redirected to "Join company" to join a already existing company
        if (!userObject.companyId && !companySetupUrls.includes(pathName)) {
            window.location.replace(COMPONENT_SETUP_JOIN_COMPANY_URL);
        }
    }
}
