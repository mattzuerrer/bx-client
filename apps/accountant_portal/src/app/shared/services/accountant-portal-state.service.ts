import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { StateService } from '@bx-client/ngrx';
import { select } from '@ngrx/store';
import { BehaviorSubject, Observable } from 'rxjs';

import { Accountant, BearerTokenUser, Client, Company, GoogleAnalytics, PendingClientInvites, Permissions, User } from '../models';
import {
    AccountantsSelectors,
    AnalyticsSelectors,
    BearerTokenSelectors,
    ClientsSelectors,
    CompanySelectors,
    UserSelectors
} from '../selectors';

@Injectable()
export class AccountantPortalStateService extends StateService {
    readonly accountantsLoaded$: Observable<boolean> = this.store.pipe(select(AccountantsSelectors.getLoaded));
    readonly accountants$: Observable<Accountant[]> = this.store.pipe(select(AccountantsSelectors.getAccountants));
    readonly accountantsWithFinishedOnboardin$: Observable<Accountant[]> = this.store.pipe(
        select(AccountantsSelectors.getAccountantsWithFinishedOnboarding)
    );

    readonly accountantUserLoaded$: Observable<boolean> = this.store.pipe(select(UserSelectors.getAccountantUserLoaded));
    readonly accountantUser$: Observable<Accountant> = this.store.pipe(select(UserSelectors.getAccountantUser));

    readonly loginUserLoaded$: Observable<boolean> = this.store.pipe(select(UserSelectors.getLoginUserloaded));
    readonly loginUser$: Observable<User> = this.store.pipe(select(UserSelectors.getLoginUser));

    readonly clientsLoaded$: Observable<boolean> = this.store.pipe(select(ClientsSelectors.getClientsLoaded));
    readonly clients$: Observable<Client[]> = this.store.pipe(select(ClientsSelectors.getClients));

    readonly pendingClientInvitesLoaded$: Observable<boolean> = this.store.pipe(select(ClientsSelectors.getPendingClientInvitesLoaded));
    readonly pendingClientInvites$: Observable<PendingClientInvites[]> = this.store.pipe(select(ClientsSelectors.getPendingClientInvites));

    readonly accessPermissions$: Observable<Permissions> = this.store.pipe(select(UserSelectors.getAccessPermissions));
    readonly restApiError$: Observable<HttpErrorResponse> = this.store.pipe(select(UserSelectors.getRestApiError));

    readonly tokenUserLoaded$: Observable<boolean> = this.store.pipe(select(BearerTokenSelectors.getLoaded));
    readonly tokenUser$: Observable<BearerTokenUser> = this.store.pipe(select(BearerTokenSelectors.getUser));
    readonly tokenUserAccessPermissions$: Observable<any> = this.store.pipe(select(BearerTokenSelectors.getAccessPermissions));

    readonly idsOfAccountantsWithConnectedClients$: Observable<number[]> = this.store.pipe(
        select(ClientsSelectors.getAccountantsWithConnectedClients)
    );

    readonly companySettingsLoaded$: Observable<boolean> = this.store.pipe(select(CompanySelectors.getLoaded));
    readonly companySettings$: Observable<Company> = this.store.pipe(select(CompanySelectors.getSettings));

    readonly companyLogoLoaded$: Observable<boolean> = this.store.pipe(select(CompanySelectors.getLogoLoaded));
    readonly companyLogoUploaded$: Observable<boolean> = this.store.pipe(select(CompanySelectors.getLogoUploaded));
    readonly companyLogoChanged$: Observable<boolean> = this.store.pipe(select(CompanySelectors.getLogoChanged));
    readonly companyLogo$: Observable<ArrayBuffer> = this.store.pipe(select(CompanySelectors.getLogo));

    readonly companyPdfLoaded$: Observable<boolean> = this.store.pipe(select(CompanySelectors.getPdfLoaded));
    readonly companyPdfUploaded$: Observable<boolean> = this.store.pipe(select(CompanySelectors.getPdfUploaded));
    readonly companyPdfChanged$: Observable<boolean> = this.store.pipe(select(CompanySelectors.getPdfChanged));
    readonly companyPdf$: Observable<ArrayBuffer> = this.store.pipe(select(CompanySelectors.getPdf));

    readonly newCompanyId$: Observable<number> = this.store.pipe(select(CompanySelectors.getNewCompanyId));

    readonly uploadLogoAndPdf$: Observable<boolean> = this.store.pipe(select(CompanySelectors.getUploadLogoAndPdf));

    readonly companies$: Observable<Company[]> = this.store.pipe(select(CompanySelectors.getCollection));
    readonly companiesLoaded$: Observable<boolean> = this.store.pipe(select(CompanySelectors.getCollectionLoaded));

    readonly googleAnalyticsLoaded$: Observable<boolean> = this.store.pipe(select(AnalyticsSelectors.getGoogleAnalyticsLoaded));
    readonly googleAnalyticsConfiguration$: Observable<GoogleAnalytics> = this.store.pipe(
        select(AnalyticsSelectors.getGoogleAnalyticsConfiguration)
    );

    statesLoaded$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
}
