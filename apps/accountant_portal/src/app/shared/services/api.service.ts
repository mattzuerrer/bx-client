import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { UtilitiesService } from '@bx-client/common';
import { HttpService } from '@bx-client/http';

import {
    Accountant,
    BearerTokenUser,
    Client,
    ClientAccess,
    ClientAccountantIds,
    Company,
    GoogleAnalytics,
    KeyPerformanceIndicators,
    PendingClientInvites,
    PortalCompanyFile,
    User
} from '../models';

@Injectable()
export class ApiService {
    constructor(private httpService: HttpService, private httpClient: HttpClient, private utilitiesService: UtilitiesService) {}

    /**
     * Get clients
     *
     * @returns {Observable<Client[]>}
     */
    getClients(getAll: boolean = false): Observable<Client[]> {
        return this.httpService.get(Client, `clients?${getAll ? 'filter=all&' : ''}embed=clientAccess`);
    }

    /**
     * Get clients login link to the my application
     *
     * @param {number} id
     * @return {Observable<string>}
     */
    getClientLoginLink(id: number): Observable<{ location: string }> {
        return this.httpService.get(null, `clients/${id}/login`);
    }

    /**
     * Connect accountant to client
     *
     * @param {ClientAccountantIds} ids
     * @return {Observable<ClientAccess>}
     */
    connectClientAccountant(ids: ClientAccountantIds): Observable<ClientAccess> {
        return this.httpService.post(ClientAccess, `clients/${ids.clientId}/client_access/connect`, { accountant_id: ids.id });
    }

    /**
     * Disconnect accountant from client
     *
     * @param {ClientAccountantIds} ids
     * @return {Observable<ClientAccess>}
     */
    disconnectClientAccountant(ids: ClientAccountantIds): Observable<ClientAccess> {
        return this.httpService.post(ClientAccess, `clients/${ids.clientId}/client_access/${ids.id}/disconnect`);
    }

    /**
     * Reject accountant invitation for a client
     *
     * @param {ClientAccountantIds} ids
     * @return {Observable<ClientAccess>}
     */
    rejectClientAccountantInvitation(ids: ClientAccountantIds): Observable<ClientAccess> {
        return this.httpService.post(ClientAccess, `clients/${ids.clientId}/client_access/${ids.id}/reject`);
    }

    /**
     * Accept accountant invitation for a client
     *
     * @param {ClientAccountantIds} ids
     * @return {Observable<ClientAccess>}
     */
    acceptClientAccountantInvitation(ids: ClientAccountantIds): Observable<ClientAccess> {
        return this.httpService.post(ClientAccess, `clients/${ids.clientId}/client_access/${ids.id}/accept`);
    }

    /**
     * Resend accountant invitation to client
     *
     * @param {ClientAccountantIds} ids
     * @return {Observable<ClientAccess>}
     */
    reinviteClientToAccountant(ids: ClientAccountantIds): Observable<ClientAccess> {
        return this.httpService.post(ClientAccess, `clients/${ids.clientId}/client_access/${ids.id}/reinvite`);
    }

    /**
     * Join accountant to existing compay
     *
     * @param {number} companyId
     * @param {number} accountantId
     * @return {Observable<Accountant>}
     */
    joinExistingCompany(companyId: number, accountantId: number): Observable<Accountant> {
        return this.httpService.patch(Accountant, `accountants/${accountantId}`, { company_id: companyId });
    }

    /**
     * Get client accesses
     *
     * @param {number} clientId
     * @return {Observable<ClientAccess[]>}
     */
    getClientAccess(clientId: number): Observable<ClientAccess[]> {
        return this.httpService.get(ClientAccess, `clients/${clientId}/client_access`);
    }

    /**
     * Create new client
     *
     * @return {Observable<Client>}
     */
    createClient(): Observable<Client> {
        return this.httpService.post(Client, 'clients');
    }

    /**
     * Get pending client invites
     *
     * @returns {Observable<PendingClientInvites[]>}
     */
    getPendingClientInvites(): Observable<PendingClientInvites[]> {
        return this.httpService.get(PendingClientInvites, 'clients/invites');
    }

    /**
     * Get client's key performance indicators
     *
     * @param {number} clientId
     * @return {Observable<KeyPerformanceIndicators>}
     */
    loadKeyPerformanceIndicators(clientId: number): Observable<KeyPerformanceIndicators> {
        return this.httpService.get(KeyPerformanceIndicators, `clients/${clientId}/key_performance_indicators`);
    }

    /**
     * Get accountants
     *
     * @return {Observable<Accountant[]>}
     */
    getAccountants(): Observable<Accountant[]> {
        return this.httpService.get(Accountant, 'accountants');
    }

    /**
     * Get one accountant
     *
     * @param {number} id
     * @return {Observable<Accountant>}
     */
    getAccountant(id: number): Observable<Accountant> {
        return this.httpService.get(Accountant, `accountants/${id}`);
    }

    /**
     * Accept accountant invitation
     *
     * @param {number} id
     * @return {Observable<Accountant>}
     */
    acceptAccountantInvitation(id: number): Observable<Accountant> {
        return this.httpService.post(Accountant, `accountants/${id}/accept`);
    }

    /**
     * Reject accountant invitation
     *
     * @param {number} id
     * @return {Observable<Accountant>}
     */
    rejectAccountantInvitation(id: number): Observable<Accountant> {
        return this.httpService.post(Accountant, `accountants/${id}/reject`);
    }

    /**
     * Revoke accountant invitation
     *
     * @param {number} id
     * @return {Observable<any>}
     */
    revokeAccountantInvitation(id: number): Observable<any> {
        return this.httpService.remove(null, `accountants/${id}/revoke`);
    }

    /**
     * Reinvite accountant
     *
     * @param {number} id
     * @return {Observable<Accountant>}
     */
    reinviteAccountant(id: number): Observable<Accountant> {
        return this.httpService.post(Accountant, `accountants/${id}/reinvite`);
    }

    /**
     * Get loggin user
     *
     * @return {Observable<User>}
     */
    getLoginUser(getCompany: boolean = false): Observable<User> {
        return this.httpService.get(User, `users/me${getCompany ? '?embed=company' : ''}`);
    }

    /**
     * Get accountant user
     *
     * @return {Observable<Accountant>}
     */
    getAccountantUser(getCompany: boolean = true): Observable<Accountant> {
        return this.httpService.get(Accountant, `accountants/me${getCompany ? '?embed=company' : ''}`);
    }

    /**
     * Change a role
     *
     * @param {Pick<Accountant, 'id' | 'role'>} accountant
     * @return {Observable<Accountant>}
     */
    changeRole(accountant: Pick<Accountant, 'id' | 'role'>): Observable<Accountant> {
        return this.httpService.patch(Accountant, `accountants/${accountant.id}`, { role: accountant.role });
    }

    /**
     * Invite new accountant
     *
     * @param {Partial<Accountant>} accountant
     * @return {Observable<Accountant>}
     */
    inviteAccountant(accountant: Partial<Accountant>): Observable<Accountant> {
        const data = JSON.stringify(this.utilitiesService.camelToSnake(accountant));
        return this.httpService.post(Accountant, 'accountants', data);
    }

    /**
     * Remove accountant from company
     *
     * @param {number} id
     * @return {Observable<any>}
     */
    removeAccountant(id: number): Observable<any> {
        return this.httpService.remove(null, `accountants/${id}`);
    }

    /**
     * Create accountant user based on a existing user
     *
     * @param {string} token
     * @return {Observable<Accountant>}
     */
    createAccountantFromExistingUser(token: string): Observable<Accountant> {
        return this.httpService.post(null, `accountant_invitations/${token}/accountant/existing_user`);
    }

    /**
     * Validate access by token (user without session)
     *
     * @param {string} token
     * @return {Observable<BearerTokenUser>}
     */
    validateAccountantToken(token: string): Observable<BearerTokenUser> {
        return this.httpService.get(null, `accountant_invitations/${token}`);
    }

    /**
     * Save accountant password
     *
     * @param {string} password
     * @param {string} token
     * @return {Observable<any>}
     */
    savePassword(password: string, token: string): Observable<any> {
        return this.httpService.post(null, `accountant_invitations/${token}/accountant/password`, { password });
    }

    /**
     * Create new accountant from invitation
     *
     * @param accountantInfo
     * @param {string} token
     * @return {Observable<Accountant>}
     */
    newAccountantFromInvitation(accountantInfo: any, token: string): Observable<Accountant> {
        const data = JSON.stringify(this.utilitiesService.camelToSnake({ ...accountantInfo, token }));
        return this.httpService.post(Accountant, `accountant_invitations/${token}/accountant`, data);
    }

    /**
     * Get company settings
     *
     * @param {number} id
     * @returns {Observable<Company>}
     */
    getCompanySettings(id: number): Observable<Company> {
        return this.httpService.get(Company, `companies/${id}`);
    }

    /**
     * Save company settings
     *
     * @param {number} id
     * @param {Company} company
     * @returns {Observable<Company>}
     */
    saveCompanySettings(company: Company, id: number): Observable<Company> {
        const formData = JSON.stringify(this.utilitiesService.camelToSnake(company));

        return this.httpService.patch(Company, `companies/${id}`, formData);
    }

    /**
     * Get company logo
     *
     * @param {number} id
     * @returns {Observable<ArrayBuffer>}
     */
    getCompanyLogo(id: number): Observable<ArrayBuffer> {
        return this.httpClient.get(`${this.httpService.getApiUrl()}/companies/${id}/logo`, { responseType: 'arraybuffer' });
    }

    /**
     * Update company logo
     *
     * @param {PortalCompanyFile} logo
     * @returns {Observable<any>}
     */
    saveCompanyLogo(logo: PortalCompanyFile): Observable<any> {
        const formData = new FormData();
        formData.append('logo_file', logo.file);

        return this.httpService.post(null, `companies/${logo.companyId}/logo`, formData);
    }

    /**
     * Creates or updates logo for new or existing company
     *
     * @param {PortalCompanyFile} logo
     * @param id
     * @returns {Observable<any>}
     */
    createOrUpdateNewCompanyLogo(logo: PortalCompanyFile, id: number): Observable<any> {
        const formData = new FormData();
        formData.append('logo_file', logo.file);

        return this.httpService.post(null, `companies/${id}/logo`, formData);
    }

    /**
     * Get company PDF
     *
     * @param {number} id
     * @returns {Observable<ArrayBuffer>}
     */
    getCompanyPdf(id: number): Observable<ArrayBuffer> {
        return this.httpClient.get(`${this.httpService.getApiUrl()}/companies/${id}/pdf`, { responseType: 'arraybuffer' });
    }

    /**
     * Update company PDF
     *
     * @param {PortalCompanyFile} pdf
     * @returns {Observable<string>}
     */
    saveCompanyPdf(pdf: PortalCompanyFile): Observable<string> {
        const formData = new FormData();
        formData.append('pdf_file', pdf.file);

        return this.httpService.post(null, `companies/${pdf.companyId}/pdf`, formData);
    }

    /**
     * Creates or updates pdf for new or existing company
     *
     * @param {PortalCompanyFile} pdf
     * @param {number} id
     * @returns {Observable<any>}
     */
    createOrUpdateNewCompanyPdf(pdf: PortalCompanyFile, id: number): Observable<any> {
        const formData = new FormData();
        formData.append('pdf_file', pdf.file);

        return this.httpService.post(null, `companies/${id}/pdf`, formData);
    }

    /**
     * Get list of public companies
     *
     * @return {Observable<Company[]>}
     */
    getCompanies(): Observable<Company[]> {
        return this.httpService.get(Company, 'companies');
    }

    /**
     * Creates or updates a new or existing company
     *
     * @param {Company} company
     * @param {number} companyId
     * @returns {Observable<Company[]>}
     */
    createOrUpdateNewCompany(company: Company, companyId: number = null): Observable<Company[]> {
        const formData = JSON.stringify(this.utilitiesService.camelToSnake(company));

        if (companyId) {
            return this.httpService.patch(Company, `companies/${companyId}`, formData);
        } else {
            return this.httpService.post(Company, 'companies', formData);
        }
    }

    /**
     * Get Google Analytics Configuration
     *
     * @return {Observable<GoogleAnalytics>}
     */
    getGoogleAnalyticsConfiguration(): Observable<GoogleAnalytics> {
        return this.httpService.get(GoogleAnalytics, `system/gtm`);
    }
}
