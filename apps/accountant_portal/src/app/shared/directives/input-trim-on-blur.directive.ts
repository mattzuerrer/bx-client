import { Directive, ElementRef, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({ selector: 'input[appTrimOnBlur]' })
export class InputTrimOnBlurDirective {
    constructor(private element: ElementRef, private formControl: NgControl) {}

    @HostListener('blur')
    onBlur(): void {
        this.formControl.control.patchValue(this.element.nativeElement.value.trim());
    }
}
