import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule as BxCommonModule } from '@bx-client/common';
import { CoreModule } from '@bx-client/core';
import { PipesModule } from '@bx-client/pipes';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { FileUploadModule } from 'ng2-file-upload';
import { TruncateModule } from 'ng2-truncate';

import { featureName } from '../../config';

import { sharedComponents } from './components';
import { sharedDirectives } from './directives';
import { effects } from './effects';
import { sharedGuards } from './guards';
import { MaterialModule } from './material.module';
import { reducers } from './reducers';
import { sharedProviders, sharedServices } from './services';

@NgModule({
    imports: [
        CoreModule,
        CommonModule,
        BxCommonModule,
        MaterialModule,
        PipesModule,
        FileUploadModule,
        TruncateModule,
        StoreModule.forFeature(featureName, reducers),
        EffectsModule.forFeature(effects)
    ],
    exports: [CoreModule, CommonModule, BxCommonModule, ...sharedComponents, ...sharedDirectives, MaterialModule, PipesModule],
    providers: [...sharedServices, ...sharedProviders],
    declarations: [...sharedComponents, ...sharedDirectives]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [...sharedGuards]
        };
    }
}
