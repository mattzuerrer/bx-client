import { NgModule } from '@angular/core';

import { SharedModule } from '../shared';

import { routing } from './accountants.routes';
import { components } from './components';
import { containers } from './containers';
import { entryComponents } from './entry-components';

@NgModule({
    imports: [routing, SharedModule],
    exports: [containers, components],
    declarations: [...containers, ...components, ...entryComponents],
    entryComponents: [...entryComponents]
})
export class AccountantsModule {}
