import { AddNewAccountantDialogComponent } from './add-new-accountant-dialog/add-new-accountant-dialog.component';
import { RemoveAccountantDialogComponent } from './remove-accountant-dialog/remove-accountant-dialog.component';

export { AddNewAccountantDialogComponent } from './add-new-accountant-dialog/add-new-accountant-dialog.component';
export { RemoveAccountantDialogComponent } from './remove-accountant-dialog/remove-accountant-dialog.component';

export const entryComponents = [AddNewAccountantDialogComponent, RemoveAccountantDialogComponent];
