import { Component, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import { Accountant } from '../../../shared/models';

@Component({
    selector: 'app-remove-accountant-dialog',
    templateUrl: 'remove-accountant-dialog.component.html',
    styleUrls: ['remove-accountant-dialog.component.scss']
})
export class RemoveAccountantDialogComponent {
    @Input() accountant: Accountant;

    constructor(private dialogRef: MatDialogRef<RemoveAccountantDialogComponent>) {}

    deleteAccountant(): void {
        this.dialogRef.close(this.accountant);
    }
}
