import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { ariaInvalidFn, ariaLiveFn, makeAriaInvalidFn, makeAriaLiveFn, Validators as CustomValidators } from '@bx-client/common';

import { LANGUAGES, ROLES } from '../../../../config';

@Component({
    selector: 'app-add-new-accountant-dialog',
    templateUrl: 'add-new-accountant-dialog.component.html',
    styleUrls: ['add-new-accountant-dialog.component.scss']
})
export class AddNewAccountantDialogComponent implements OnInit {
    form: FormGroup;

    roles = ROLES;
    languages = LANGUAGES;

    isAriaInvalid: ariaInvalidFn;
    ariaLive: ariaLiveFn;

    constructor(private dialogRef: MatDialogRef<AddNewAccountantDialogComponent>, private formBuilder: FormBuilder) {}

    ngOnInit(): void {
        this.form = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            email: ['', [Validators.required, CustomValidators.email]],
            role: [this.roles[0]],
            language: [this.languages[0].value]
        });

        this.isAriaInvalid = makeAriaInvalidFn(this.form);
        this.ariaLive = makeAriaLiveFn(this.form);
    }

    addNewAccountant(): void {
        this.dialogRef.close(this.form.getRawValue());
    }
}
