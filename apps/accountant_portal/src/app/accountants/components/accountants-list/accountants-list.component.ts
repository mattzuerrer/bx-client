import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig, MatSort, MatTableDataSource } from '@angular/material';

import { AccountantActions } from '../../../shared/actions';
import { Accountant, ACCOUNTANT_ROLE, OWNER_ROLE } from '../../../shared/models';
import { AccountantPortalStateService } from '../../../shared/services';
import { RemoveAccountantDialogComponent } from '../../entry-components';

const DIALOG_CONFIG: MatDialogConfig = { width: '400px' };

@Component({
    selector: 'app-accountants-list',
    templateUrl: 'accountants-list.component.html',
    styleUrls: ['accountants-list.component.scss']
})
export class AccountantsListComponent {
    ownerRole = OWNER_ROLE;
    accountantRole = ACCOUNTANT_ROLE;

    displayedColumns: string[] = ['profileImage', 'fullName', 'role', 'email', 'actions'];
    dataSource: MatTableDataSource<Accountant>;

    @Input()
    set accountantsList(accountants: Accountant[]) {
        this.dataSource = new MatTableDataSource(accountants);

        this.dataSource.sortingDataAccessor = (item, property) => {
            switch (property) {
                case 'fullName':
                    return item.lastName;
                default:
                    return item[property];
            }
        };

        this.dataSource.sort = this.sort;
    }

    @Input() accountantUser: Accountant;

    @Input() idsOfAccountantsWithConnectedClients: number[];

    @Output() roleChange: EventEmitter<Pick<Accountant, 'id' | 'role'>> = new EventEmitter<Pick<Accountant, 'id' | 'role'>>();

    @Output() acceptInvitation: EventEmitter<Accountant> = new EventEmitter<Accountant>();

    @Output() rejectInvitation: EventEmitter<Accountant> = new EventEmitter<Accountant>();

    @Output() revokeInvitation: EventEmitter<Accountant> = new EventEmitter<Accountant>();

    @Output() resendInvitation: EventEmitter<Accountant> = new EventEmitter<Accountant>();

    @ViewChild(MatSort) sort: MatSort;

    constructor(private matDialog: MatDialog, private accountantPortalStateService: AccountantPortalStateService) {}

    isOwner(role: string): boolean {
        return role === this.ownerRole;
    }

    onRoleChange(id: number, role: string): void {
        this.roleChange.emit({ id, role });
    }

    acceptAccountantInvitation(accountant: Accountant): void {
        this.acceptInvitation.emit(accountant);
    }

    rejectAccountantInvitation(accountant: Accountant): void {
        this.rejectInvitation.emit(accountant);
    }

    revokeAccountantInvitation(accountant: Accountant): void {
        this.revokeInvitation.emit(accountant);
    }

    resendAccountantInvitation(accountant: Accountant): void {
        this.resendInvitation.emit(accountant);
    }

    removeAccountant(accountant: Accountant): void {
        const dialogRef = this.matDialog.open(RemoveAccountantDialogComponent, DIALOG_CONFIG);
        dialogRef.componentInstance.accountant = accountant;
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.accountantPortalStateService.dispatch(new AccountantActions.RemoveAction(result));
            }
        });
    }

    hasConnectedClients(accountant: Accountant): boolean {
        return this.idsOfAccountantsWithConnectedClients.includes(accountant.id);
    }
}
