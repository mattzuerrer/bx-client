import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AccountantGuard } from '../shared/guards';

import { AccountantsPageComponent } from './containers';

const routes: Routes = [
    {
        path: '',
        component: AccountantsPageComponent,
        canActivate: [AccountantGuard],
        data: { needsRole: ['isOwner'] }
    },
    { path: '**', redirectTo: '/clients' }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
