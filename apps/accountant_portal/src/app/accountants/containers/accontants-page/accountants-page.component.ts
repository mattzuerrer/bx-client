import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MatDialogConfig } from '@angular/material/typings/dialog';

import { AccountantActions } from '../../../shared/actions';
import { Accountant } from '../../../shared/models';
import { AccountantPortalStateService, AnalyticsService } from '../../../shared/services';
import { AddNewAccountantDialogComponent } from '../../entry-components';

const DIALOG_CONFIG: MatDialogConfig = { width: '480px' };

@Component({
    selector: 'app-accountants-page',
    templateUrl: 'accountants-page.component.html',
    styleUrls: ['accountants-page.component.scss']
})
export class AccountantsPageComponent implements OnInit {
    constructor(
        private matDialog: MatDialog,
        public accountantPortalStateService: AccountantPortalStateService,
        private analyticsService: AnalyticsService
    ) {}

    ngOnInit(): void {
        this.analyticsService.process();
    }

    onRoleChange(payload: Pick<Accountant, 'id' | 'role'>): void {
        this.accountantPortalStateService.dispatch(new AccountantActions.ChangeRoleAction(payload));
    }

    onAcceptAccountantInvitation(accountant: Accountant): void {
        this.accountantPortalStateService.dispatch(new AccountantActions.AcceptAccountantInvitationAction(accountant.id));
    }

    onRejectAccountantInvitation(accountant: Accountant): void {
        this.accountantPortalStateService.dispatch(new AccountantActions.RejectAccountantInvitationAction(accountant.id));
    }

    onRevokeAccountantInvitation(accountant: Accountant): void {
        this.accountantPortalStateService.dispatch(new AccountantActions.RevokeAccountantInvitationAction(accountant));
    }

    onResendAccountantInvitation(accountant: Accountant): void {
        this.accountantPortalStateService.dispatch(new AccountantActions.ResendAccountantInvitationAction(accountant.id));
    }

    onAddNewAccountant(): void {
        const dialogRef = this.matDialog.open(AddNewAccountantDialogComponent, DIALOG_CONFIG);
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.accountantPortalStateService.dispatch(new AccountantActions.AddNewAccountantAction(result));
            }
        });
    }
}
