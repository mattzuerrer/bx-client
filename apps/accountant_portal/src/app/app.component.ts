import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LoaderService } from '@bx-client/http';
import { MessageService } from '@bx-client/message';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AccountantPortalStateService } from './shared/services';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
    loading$: Observable<boolean>;

    constructor(
        public accountantPortalStateService: AccountantPortalStateService,
        public loaderService: LoaderService,
        private messageService: MessageService
    ) {}

    ngOnInit(): void {
        this.loading$ = this.accountantPortalStateService.statesLoaded$.pipe(map(isLoaded => !isLoaded));
        this.messageService.action$.subscribe(action => this.accountantPortalStateService.dispatch(action));
    }
}
