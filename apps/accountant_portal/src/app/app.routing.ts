import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    { path: 'accountants', loadChildren: './accountants/accountants.module#AccountantsModule' },
    { path: 'clients', loadChildren: './clients/clients.module#ClientsModule' },
    { path: 'company', loadChildren: './company/company.module#CompanyModule' },
    { path: 'services', loadChildren: './services/services.module#ServicesModule' },
    { path: 'setup', loadChildren: './setup/setup.module#SetupModule' },
    { path: '403', loadChildren: './shared/components/http-error-states/status-403/status-403.module#Status403Module' },
    { path: '**', redirectTo: '/clients' }
];

export const routing = RouterModule.forRoot(routes);
