import { NgModule } from '@angular/core';

import { SharedModule } from '../shared';

import { components } from './components';
import { containers } from './containers';
import { entryComponents } from './entry-components';
import { routing } from './services.routes';

@NgModule({
    imports: [routing, SharedModule],
    exports: [containers, components],
    declarations: [...containers, ...components, ...entryComponents],
    entryComponents: [...entryComponents]
})
export class ServicesModule { }
