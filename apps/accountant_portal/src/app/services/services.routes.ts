import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ClientGuard } from '../shared/guards';

import { ServicesPageComponent } from './containers';

const routes: Routes = [
    {
        path: '',
        component: ServicesPageComponent,
        canActivate: [ClientGuard]
    },
    { path: '**', redirectTo: '/clients' }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
