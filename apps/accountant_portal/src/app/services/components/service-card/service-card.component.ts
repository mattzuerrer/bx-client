import { Component, EventEmitter, Input, Output } from '@angular/core';

import { MARKETPLACE_URL } from 'apps/accountant_portal/src/config';

@Component({
    selector: 'app-service-card',
    templateUrl: './service-card.component.html',
    styleUrls: ['./service-card.component.scss']
})
export class ServiceCardComponent {
    @Input() serviceName: string;

    @Output() showConsentRequest = new EventEmitter<string>();

    get marketplaceUrl(): string {
        return `${MARKETPLACE_URL}/${this.serviceName}`;
    }

    get serviceLogoPath(): string {
        return `assets/images/${this.serviceName}_logo@3x.png`;
    }

    get serviceAbstractI18nKey(): string {
        return `accountant_portal.services.${this.serviceName}.abstract`;
    }

    get serviceDescriptionI18nKey(): string {
        return `accountant_portal.services.${this.serviceName}.description`;
    }

    onOpenService(): void {
        this.showConsentRequest.emit(this.serviceName);
    }
}
