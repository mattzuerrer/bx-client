import { Component } from '@angular/core';

import { IDEAS_PORTAL_URL } from '../../../../config';

@Component({
    selector: 'app-service-card-propose-another',
    templateUrl: './service-card-propose-another.component.html',
    styleUrls: ['./service-card-propose-another.component.scss']
})
export class ServiceCardProposeAnotherComponent {
    readonly ideasPortalUrl = IDEAS_PORTAL_URL;
}
