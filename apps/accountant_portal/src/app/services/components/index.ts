import { ServiceCardProposeAnotherComponent } from './service-card-propose-another/service-card-propose-another.component';
import { ServiceCardComponent } from './service-card/service-card.component';

export const components = [ServiceCardComponent, ServiceCardProposeAnotherComponent];
