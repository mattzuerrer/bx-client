import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { CompanyActions, ServicesActions } from '../../../shared/actions';
import { Accountant, PortalCompanyFile } from '../../../shared/models';
import { AccountantPortalStateService } from '../../../shared/services';
import { createLogoFromBinaryData } from '../../../shared/utils/binaries';
import { ServiceConsentDialogComponent, ServiceConsentDialogData } from '../../entry-components/service-consent-dialog';

@Component({
    selector: 'app-services-page',
    templateUrl: 'services-page.component.html',
    styleUrls: ['services-page.component.scss']
})
export class ServicesPageComponent implements OnInit, OnDestroy {
    protected destroyed$ = new Subject();

    private get accountantUserCompanyName(): string | null {
        return this.accountantUser.company ? this.accountantUser.company.name : null;
    }

    private get accountantUserCompanyLogo(): string | null {
        return this.logo ? this.logo.base64 : null;
    }

    private accountantUser: Accountant;
    private logo: PortalCompanyFile | null = null;
    private consentDialogRef: MatDialogRef<ServiceConsentDialogComponent> | null = null;

    constructor(private matDialog: MatDialog,
                private accountantPortalStateService: AccountantPortalStateService,
                private translateService: TranslateService) { }

    ngOnInit(): void {
        this.accountantPortalStateService.accountantUser$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(accountantUser => this.updateAccountantUser(accountantUser));

        this.accountantPortalStateService.companyLogo$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(rawLogo => this.updateLogo(rawLogo));
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    onShowConsentDialog(serviceName: string): void {
        const data: ServiceConsentDialogData = {
            accountantCompanyName: this.accountantUserCompanyName,
            accountantCompanyLogo: this.accountantUserCompanyLogo,
            serviceName,
            serviceDisplayName: this.translateService.instant(`accountant_portal.services.${serviceName}.display_name`)
        };

        this.consentDialogRef = this.matDialog.open(ServiceConsentDialogComponent, { data });
        this.consentDialogRef.afterClosed().subscribe((consented: boolean | undefined) => {
            if (consented) {
                this.accountantPortalStateService.dispatch(new ServicesActions.RedirectToPartnerServiceAction(serviceName));
            }
            this.consentDialogRef = null;
        });
    }

    private updateAccountantUser(accountantUser: Accountant): void {
        this.accountantUser = accountantUser;
        this.accountantPortalStateService.dispatch(new CompanyActions.LoadLogoAction(accountantUser.companyId));
    }

    private updateLogo(rawLogo: ArrayBuffer | null): void {
        let logo: PortalCompanyFile | null = null;

        if (rawLogo) {
            logo = createLogoFromBinaryData(rawLogo, this.accountantUser.companyId);
        }

        this.logo = logo;

        if (this.consentDialogRef && logo) {
            this.consentDialogRef.componentInstance.accountantCompanyLogo = logo.base64;
        }
    }
}
