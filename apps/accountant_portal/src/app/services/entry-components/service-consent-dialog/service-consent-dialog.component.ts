import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

export interface ServiceConsentDialogData {
    accountantCompanyName: string;
    accountantCompanyLogo: string | null;
    serviceName: string;
    serviceDisplayName: string;
}

@Component({
    selector: 'app-service-consent-dialog',
    templateUrl: 'service-consent-dialog.component.html',
    styleUrls: ['service-consent-dialog.component.scss']
})
export class ServiceConsentDialogComponent {
    get accountantCompanyName(): string {
        return this.data.accountantCompanyName;
    }

    get accountantCompanyLogo(): string | null {
        return this.data.accountantCompanyLogo;
    }

    set accountantCompanyLogo(value: string) {
        this.data.accountantCompanyLogo = value;
    }

    get serviceName(): string {
        return this.data.serviceName;
    }

    get serviceDisplayName(): string {
        return this.data.serviceDisplayName;
    }

    get serviceLogoPath(): string {
        return `assets/images/${this.serviceName}_logo@3x.png`;
    }

    constructor(@Inject(MAT_DIALOG_DATA) private data: ServiceConsentDialogData) { }
}
