import { NgModule } from '@angular/core';
import { AutocompleteModule } from '@bx-client/common';

import { SharedModule } from '../shared';

import { components } from './components';
import { containers } from './containers';
import { routing } from './setup.routes';

@NgModule({
    imports: [routing, SharedModule, AutocompleteModule],
    exports: [containers, components],
    declarations: [...containers, ...components]
})
export class SetupModule {}
