import { Component, ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BearerTokenGuard, JoinCompanyGuard, LoggedInGuard } from '../shared/guards';

import {
    AddAccountantCompanyPageComponent,
    CreateNewAccountantPageComponent,
    JoinExistingCompanyPageComponent,
    PasswordFormPageComponent
} from './containers';

const routes: Routes = [
    {
        path: '',
        component: Component,
        canActivate: [BearerTokenGuard],
        data: { needsRole: ['isBearerTokenUser'] }
    },
    {
        path: 'password',
        component: PasswordFormPageComponent,
        canActivate: [BearerTokenGuard],
        data: { needsRole: ['isBearerTokenUser'] }
    },
    {
        path: 'user',
        component: CreateNewAccountantPageComponent,
        canActivate: [BearerTokenGuard],
        data: { needsRole: ['isBearerTokenUser'] }
    },
    {
        path: 'join-company',
        component: JoinExistingCompanyPageComponent,
        canActivate: [JoinCompanyGuard],
        data: { containsRole: ['isPendingAccountant', 'isRejected'] }
    },

    {
        path: 'company',
        component: AddAccountantCompanyPageComponent,
        canActivate: [LoggedInGuard]
    },
    { path: '**', redirectTo: '/clients' }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
