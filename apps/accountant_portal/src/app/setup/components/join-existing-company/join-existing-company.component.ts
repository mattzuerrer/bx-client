import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ariaInvalidFn, makeAriaInvalidFn, Validators as CustomValidators } from '@bx-client/common';
import { AutocompleteComponent } from '@bx-client/common/src/modules/autocomplete/autocomplete.component';
import { byId } from '@bx-client/utils';

import { Company } from '../../../shared/models';

@Component({
    selector: 'app-join-existing-company',
    templateUrl: 'join-existing-company.component.html',
    styleUrls: ['join-existing-company.component.scss']
})
export class JoinExistingCompanyComponent implements OnInit {
    @Input()
    set autocompleteCompanies(collection: Company[]) {
        this.companies = collection
            .filter(company => company.name.trim())
            .map(company => ({ id: company.id, name: company.name, format: () => company.name }));
        this.form.get('company').setValidators([CustomValidators.isInArray(this.companies)]);
    }

    @Output() newCompanyRedirect: EventEmitter<void> = new EventEmitter<void>();

    @Output() joinCompany: EventEmitter<number> = new EventEmitter<number>();

    @ViewChild('autocomplete') autocompleteField: AutocompleteComponent;

    autocompleteNewItemOption = 'accountant_portal.join_existing_company.new_company';

    companies: any[];
    isCompanySelected = false;
    isAriaInvalid: ariaInvalidFn;

    form = new FormGroup({
        company: new FormControl()
    });

    ngOnInit(): void {
        this.isAriaInvalid = makeAriaInvalidFn(this.form);
    }

    onChange(selected: any): void {
        this.isCompanySelected = selected && selected.id;
    }

    onNewItem(): void {
        this.newCompanyRedirect.emit();
    }

    get companySelected(): boolean {
        return this.isCompanySelected && this.form.get('company').value;
    }

    resetInput(): void {
        this.form.get('company').setValue('');
        this.autocompleteField.focusInput();
    }

    onJoinCompany(): void {
        this.joinCompany.emit(this.form.get('company').value);
    }

    get selectedCompanyName(): string {
        return this.companies.find(byId(this.form.get('company').value)).name;
    }
}
