import { Component } from '@angular/core';

@Component({
    selector: 'app-invite-new-accountant-form',
    templateUrl: 'invite-new-accountant-form.component.html',
    styleUrls: ['invite-new-accountant-form.component.scss']
})
export class InviteNewAccountantFormComponent {}
