import { InviteNewAccountantFormComponent } from './invite-new-accountant-form/invite-new-accountant-form.component';
import { JoinExistingCompanyComponent } from './join-existing-company/join-existing-company.component';

export const components = [InviteNewAccountantFormComponent, JoinExistingCompanyComponent];
