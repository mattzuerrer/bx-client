import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';

import { CompanyActions } from '../../../shared/actions';
import { Company } from '../../../shared/models';
import { AccountantPortalStateService } from '../../../shared/services';

@Component({
    selector: 'app-add-accountant-company-page',
    templateUrl: 'add-accountant-company-page.component.html',
    styleUrls: ['add-accountant-company-page.component.scss']
})
export class AddAccountantCompanyPageComponent implements OnInit {
    addAccountantCompanyForm: FormGroup;
    companyForm: AbstractControl;
    associationControl: AbstractControl;

    isSetupForm = true;

    constructor(private formBuilder: FormBuilder, private accountantPortalStateService: AccountantPortalStateService) {}

    ngOnInit(): void {
        this.addAccountantCompanyForm = this.formBuilder.group({});
    }

    formInitialized(controlName: string, formGroup: FormGroup): void {
        this.addAccountantCompanyForm.setControl(controlName, formGroup);
        this.companyForm = this.addAccountantCompanyForm.get('companyForm');

        if (this.companyForm) {
            this.associationControl = this.companyForm.get('association');
        }
    }

    saveSettings(): void {
        this.mapBooleanToAssociation();
        this.handleFormSubmit();
        this.mapAssociationToBoolean();
    }

    private handleFormSubmit(): void {
        const company: Company = this.companyForm.value;
        this.accountantPortalStateService.dispatch(new CompanyActions.CreateNewCompanyAction(company));
    }

    private mapAssociationToBoolean(): void {
        this.associationControl.patchValue(this.associationControl.value !== 'none');
    }

    private mapBooleanToAssociation(): void {
        this.associationControl.value !== false ? this.associationControl.patchValue('swiss') : this.associationControl.patchValue('none');
    }
}
