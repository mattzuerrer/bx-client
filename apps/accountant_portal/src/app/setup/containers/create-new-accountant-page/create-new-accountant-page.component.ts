import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ariaInvalidFn, ariaLiveFn, makeAriaInvalidFn, makeAriaLiveFn, Validators as CustomValidators } from '@bx-client/common';

import { AccountantActions } from '../../../shared/actions';
import { BearerTokenUser } from '../../../shared/models';
import { AccountantPortalStateService } from '../../../shared/services';

@Component({
    selector: 'app-create-new-accountant-page',
    templateUrl: 'create-new-accountant-page.component.html',
    styleUrls: ['create-new-accountant-page.component.scss']
})
export class CreateNewAccountantPageComponent implements OnInit {
    form: FormGroup;
    isAriaInvalid: ariaInvalidFn;
    ariaLive: ariaLiveFn;
    tokenUser: BearerTokenUser;

    constructor(private stateService: AccountantPortalStateService, private formBuilder: FormBuilder) {}

    ngOnInit(): void {
        this.stateService.tokenUser$.subscribe(result => (this.tokenUser = result));

        this.form = this.formBuilder.group(
            {
                salutationType: ['male', Validators.required],
                firstName: ['', Validators.required],
                lastName: ['', Validators.required],
                email: [this.tokenUser.accountantEmail, [Validators.required, CustomValidators.email]],
                password: ['', [Validators.required, CustomValidators.strongPassword]],
                passwordRepeat: ['']
            },
            { validators: CustomValidators.arePasswordsSame }
        );

        this.isAriaInvalid = makeAriaInvalidFn(this.form);
        this.ariaLive = makeAriaLiveFn(this.form);
    }

    get passwordsNotSame(): string {
        return this.form.hasError('notSame') ? 'assertive' : 'off';
    }

    saveSettings(): void {
        this.stateService.dispatch(new AccountantActions.NewAccountantFromInvitationAction(this.form.getRawValue()));
    }
}
