import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AccountantActions } from '../../../shared/actions';
import { AccountantPortalStateService } from '../../../shared/services';

@Component({
    selector: 'app-join-existing-company-page',
    templateUrl: 'join-existing-company-page.component.html',
    styleUrls: ['join-existing-company-page.component.scss']
})
export class JoinExistingCompanyPageComponent {
    constructor(public accountantPortalStateService: AccountantPortalStateService, private router: Router) {}

    onNewCompanyRedirect(): void {
        this.router.navigate(['/setup/company']);
    }

    onJoinCompany(companyId: number): void {
        this.accountantPortalStateService.dispatch(new AccountantActions.JoinExistingCompanyAction(companyId));
    }
}
