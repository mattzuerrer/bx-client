import { AddAccountantCompanyPageComponent } from './add-accountant-company-page/add-accountant-company-page.component';
import { CreateNewAccountantPageComponent } from './create-new-accountant-page/create-new-accountant-page.component';
import { JoinExistingCompanyPageComponent } from './join-existing-company-page/join-existing-company-page.component';
import { PasswordFormPageComponent } from './password-form-page/password-form-page.component';

export { AddAccountantCompanyPageComponent } from './add-accountant-company-page/add-accountant-company-page.component';
export { CreateNewAccountantPageComponent } from './create-new-accountant-page/create-new-accountant-page.component';
export { JoinExistingCompanyPageComponent } from './join-existing-company-page/join-existing-company-page.component';
export { PasswordFormPageComponent } from './password-form-page/password-form-page.component';

export const containers = [
    AddAccountantCompanyPageComponent,
    CreateNewAccountantPageComponent,
    PasswordFormPageComponent,
    JoinExistingCompanyPageComponent
];
