import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ariaInvalidFn, ariaLiveFn, makeAriaInvalidFn, makeAriaLiveFn, Validators as CustomValidators } from '@bx-client/common';

import { AccountantActions } from '../../../shared/actions';
import { AccountantPortalStateService } from '../../../shared/services';

@Component({
    selector: 'app-password-form-page',
    templateUrl: 'password-form-page.component.html',
    styleUrls: ['password-form-page.component.scss']
})
export class PasswordFormPageComponent implements OnInit {
    form: FormGroup;

    isAriaInvalid: ariaInvalidFn;
    ariaLive: ariaLiveFn;

    constructor(private stateService: AccountantPortalStateService, private formBuilder: FormBuilder) {}

    ngOnInit(): void {
        this.form = this.formBuilder.group(
            {
                password: ['', [Validators.required, CustomValidators.strongPassword]],
                passwordRepeat: ['']
            },
            { validators: CustomValidators.arePasswordsSame }
        );

        this.isAriaInvalid = makeAriaInvalidFn(this.form);
        this.ariaLive = makeAriaLiveFn(this.form);
    }

    get passwordsNotSame(): string {
        return this.form.hasError('notSame') ? 'assertive' : 'off';
    }

    saveSettings(): void {
        this.stateService.dispatch(new AccountantActions.SavePasswordAction(this.form.get('password').value));
    }
}
