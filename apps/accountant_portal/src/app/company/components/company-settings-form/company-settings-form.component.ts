import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';

import { CompanyActions } from '../../../shared/actions';
import { Company } from '../../../shared/models';
import { AccountantPortalStateService } from '../../../shared/services';

@Component({
    selector: 'app-company-settings-form',
    templateUrl: 'company-settings-form.component.html',
    styleUrls: ['company-settings-form.component.scss']
})
export class CompanySettingsFormComponent implements OnInit {
    companySettingsForm: FormGroup;

    companyForm: AbstractControl;
    associationControl: AbstractControl;

    company: Company;

    @Input() companySettings: Company;
    @Input() companyLogoChanged: boolean;
    @Input() companyPdfChanged: boolean;

    @Input()
    set logoUploaded(isLogoUploaded: boolean) {
        if (this.company && isLogoUploaded) {
            this.handlePdfFile();
        }
    }

    @Input()
    set pdfUploaded(isPdfUploaded: boolean) {
        if (this.company && isPdfUploaded) {
            this.handleFormSubmit();
        }
    }

    constructor(private formBuilder: FormBuilder, public accountantPortalStateService: AccountantPortalStateService) {}

    ngOnInit(): void {
        this.companySettingsForm = this.formBuilder.group({});
    }

    formInitialized(controlName: string, formGroup: FormGroup): void {
        this.companySettingsForm.setControl(controlName, formGroup);
        this.preFillForm(controlName, this.companySettings);

        this.companyForm = this.companySettingsForm.get('companyForm');
        if (this.companyForm) {
            this.associationControl = this.companyForm.get('association');

            this.mapAssociationToBoolean();
        }
    }

    saveSettings(): void {
        this.mapBooleanToAssociation();
        this.company = this.companyForm.value;
        this.handleLogoFile();
        this.mapAssociationToBoolean();
    }

    onLogoChanged(hasChanged: boolean): void {
        this.accountantPortalStateService.dispatch(new CompanyActions.LogoChangedAction(hasChanged));
    }

    onPdfChanged(hasChanged: boolean): void {
        this.accountantPortalStateService.dispatch(new CompanyActions.PdfChangedAction(hasChanged));
    }

    private preFillForm(controlName: string, companySettings: Company): void {
        Object.keys(companySettings).forEach(key => {
            const control = this.companySettingsForm.get(controlName).get(key);
            if (control) {
                control.patchValue(companySettings[key]);

                if (companySettings[key] && companySettings[key].length > 0) {
                    control.markAsTouched();
                }
            }
        });
    }

    private handleLogoFile(): void {
        if (!!this.company.logo && this.companyLogoChanged) {
            this.accountantPortalStateService.dispatch(new CompanyActions.UploadLogoAction(this.company.logo));
        } else {
            this.logoUploaded = true;
        }
    }

    private handlePdfFile(): void {
        if (!!this.company.pdf && this.companyPdfChanged) {
            this.accountantPortalStateService.dispatch(new CompanyActions.UploadPdfAction(this.company.pdf));
        } else {
            this.pdfUploaded = true;
        }
    }

    private handleFormSubmit(): void {
        this.accountantPortalStateService.dispatch(
            new CompanyActions.SaveSettingsAction(this.company, this.companySettingsForm)
        );
    }

    private mapAssociationToBoolean(): void {
        this.associationControl.patchValue(this.associationControl.value !== 'none');
    }

    private mapBooleanToAssociation(): void {
        this.associationControl.value !== false ? this.associationControl.patchValue('swiss') : this.associationControl.patchValue('none');
    }
}
