import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CompanyGuard } from '../shared/guards';

import { CompanyPageComponent } from './containers';

const routes: Routes = [
    {
        path: ':accountantUserId',
        component: CompanyPageComponent,
        canActivate: [CompanyGuard],
        data: { needsRole: ['isOwner'] }
    },
    { path: '**', redirectTo: '/clients' }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
