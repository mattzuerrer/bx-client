import { Component, OnInit } from '@angular/core';

import { AccountantPortalStateService, AnalyticsService } from '../../../shared/services';

@Component({
    selector: 'app-company-page',
    templateUrl: 'company-page.component.html',
    styleUrls: ['company-page.component.scss']
})
export class CompanyPageComponent implements OnInit {
    constructor(public accountantPortalStateService: AccountantPortalStateService, private analyticsService: AnalyticsService) {}

    ngOnInit(): void {
        this.analyticsService.process();
    }
}
