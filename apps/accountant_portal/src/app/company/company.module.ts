import { NgModule } from '@angular/core';

import { SharedModule } from '../shared';

import { routing } from './company.routes';
import { components } from './components';
import { containers } from './containers';

@NgModule({
    imports: [routing, SharedModule],
    exports: [containers, components],
    declarations: [...containers, ...components]
})
export class CompanyModule {}
