import { Component, Input } from '@angular/core';
import { CdnService } from '@bx-client/core/src/environments/service/cdn.service';

import { version } from '../../../../config';
import { Accountant } from '../../../shared/models';

@Component({
    selector: 'app-pending-accountant',
    templateUrl: 'pending-accountant.component.html',
    styleUrls: ['pending-accountant.component.scss']
})
export class PendingAccountantComponent {
    @Input() accountantUser: Accountant;

    assetUrl: string = this.cdn.getAssetUrl(version);

    constructor(private cdn: CdnService) {}
}
