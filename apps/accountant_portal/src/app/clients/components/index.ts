import { ClientsListComponent } from './clients-list/clients-list.component';
import { NoClientsComponent } from './no-clients/no-clients.component';
import { PendingAccountantComponent } from './pending-accountant/pending-accountant.component';
import { PendingClientInvitesListComponent } from './pending-invites-list/pending-client-invites-list.component';

export const components = [ClientsListComponent, NoClientsComponent, PendingAccountantComponent, PendingClientInvitesListComponent];
