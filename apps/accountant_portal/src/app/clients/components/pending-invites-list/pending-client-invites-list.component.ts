import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig, MatSort, MatTableDataSource } from '@angular/material';

import { PendingClientInvites } from '../../../shared/models';
import { RejectPendingClientInviteDialogComponent } from '../../entry-components';

const DIALOG_CONFIG: MatDialogConfig = { width: '432px' };

@Component({
    selector: 'app-pending-client-invites-list',
    templateUrl: 'pending-client-invites-list.component.html',
    styleUrls: ['pending-client-invites-list.component.scss']
})
export class PendingClientInvitesListComponent {
    displayedColumns = ['name', 'contact', 'inviteRecived', 'actionsInvites'];
    dataSource: MatTableDataSource<PendingClientInvites>;
    type: string;

    @Input() dateFormat: string;

    @Input()
    set pendingClientInvitesList(pendingClientInvites: PendingClientInvites[]) {
        this.dataSource = new MatTableDataSource(pendingClientInvites);
        this.dataSource.sortingDataAccessor = (invite, property) => {
            switch (property) {
                case 'contact':
                    return invite.client.email;
                case 'inviteRecived':
                    return invite.createdAt;
                default:
                    return invite.client[property].toLowerCase();
            }
        };

        this.dataSource.sort = this.sort;
    }

    @Output() acceptPendingClientInvite: EventEmitter<PendingClientInvites> = new EventEmitter<PendingClientInvites>();

    @Output() rejectPendingClientInvite: EventEmitter<PendingClientInvites> = new EventEmitter<PendingClientInvites>();

    @ViewChild(MatSort) sort: MatSort;

    constructor(private matDialog: MatDialog) {}

    onRejectPendingClientInvite(pendingClientInvite: PendingClientInvites): void {
        const dialogRef = this.matDialog.open(RejectPendingClientInviteDialogComponent, DIALOG_CONFIG);
        dialogRef.componentInstance.pendingClientInvite = pendingClientInvite;
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.rejectPendingClientInvite.emit(pendingClientInvite);
            }
        });
    }

    onAcceptPendingClientInvite(pendingClientInvite: PendingClientInvites): void {
        this.acceptPendingClientInvite.emit(pendingClientInvite);
    }
}
