import { animate, state, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { byId } from '@bx-client/utils';

import { Accountant, ChartData, Client, LastClosedTaxPeriod } from '../../../shared/models';
import { ChartDataService } from '../../../shared/services';

const ALL_CLIENTS_TYPE = 'allClients';
const MY_CLIENTS_TYPE = 'myClients';

const DEBTOR_BALANCE = 'debtorBalance';
const CREDITOR_BALANCE = 'creditorBalance';

@Component({
    selector: 'app-clients-list',
    templateUrl: 'clients-list.component.html',
    styleUrls: ['clients-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('detailExpand', [
            state('collapsed, void', style({ height: '0px', minHeight: '0', display: 'none' })),
            state('expanded', style({ height: '*' })),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
            transition('expanded <=> void', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
        ]),
        trigger('detailsTrigger', [
            state('collapsed', style({ transform: 'rotate(180deg)' })),
            state('expanded', style({ transform: 'rotate(0)' })),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
            transition('expanded <=> void', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
        ])
    ]
})
export class ClientsListComponent {
    chartsColors = {
        domain: ['#00a1df', '#66b245']
    };

    displayedColumns: string[];
    dataSource: MatTableDataSource<Client>;
    type: string;

    expandedClient: Client = new Client();

    expandedClientChartData = {
        debtorBalance: [],
        creditorBalance: []
    };

    @Input() dateFormat: string;

    @Input() accountants: Accountant[];

    @Input() isOwner: boolean;

    @Input() accountantUser: Accountant;

    @Input()
    set tableType(type: string) {
        this.type = type;
        switch (type) {
            case ALL_CLIENTS_TYPE:
                this.displayedColumns = ['name', 'lastLogin', 'accountants', 'actionsAllClients'];
                break;
            case MY_CLIENTS_TYPE:
                this.displayedColumns = ['name', 'trial', 'lastLogin', 'actionsMyClients'];
                break;
        }
    }

    @Input()
    set clientsList(clients: Client[]) {
        this.dataSource = new MatTableDataSource(clients);
        this.dataSource.sortingDataAccessor = (client, property) => {
            switch (property) {
                case 'trial':
                    return client.trialEndDate;
                case 'accountants':
                    return this.getClientAccountantsNames(client);
                default:
                    // TODO remove this when BX-9228 was done
                    if (!client.name) {
                        break;
                    }
                    return client[property].toLowerCase();
            }
        };

        this.dataSource.sort = this.sort;
        switch (this.type) {
            case ALL_CLIENTS_TYPE:
                this.dataSource.filterPredicate = (client: Client, filter: string) => {
                    return (
                        (client.name &&
                            client.name
                                .trim()
                                .toLowerCase()
                                .indexOf(filter) !== -1) ||
                        this.existsInAccountants(this.getClientAccountantsNames(client), filter)
                    );
                };
                break;
            case MY_CLIENTS_TYPE:
                this.dataSource.filterPredicate = (client: Client, filter: string) =>
                    client.name &&
                    client.name
                        .trim()
                        .toLowerCase()
                        .indexOf(filter) !== -1;
                break;
        }
        if (this.expandedClient.id) {
            this.expandedClient = clients.find(byId(this.expandedClient.id));
            if (this.expandedClient.id && this.expandedClient.keyPerformanceIndicators) {
                this.expandedClientChartData[DEBTOR_BALANCE] = this.chartDataService.convertToChartData(
                    this.expandedClient.keyPerformanceIndicators,
                    DEBTOR_BALANCE
                );
                this.expandedClientChartData[CREDITOR_BALANCE] = this.chartDataService.convertToChartData(
                    this.expandedClient.keyPerformanceIndicators,
                    CREDITOR_BALANCE
                );
            }
        }
    }

    @Output() login: EventEmitter<number> = new EventEmitter<number>();

    @Output() manageClient: EventEmitter<Client> = new EventEmitter<Client>();

    @Output() disconnectClient: EventEmitter<Client> = new EventEmitter<Client>();

    @Output() loadKeyPerformanceIndicators: EventEmitter<number> = new EventEmitter<number>();

    @ViewChild(MatSort) sort: MatSort;

    constructor(private chartDataService: ChartDataService) {}

    filter(value: string): void {
        this.dataSource.filter = value.trim().toLowerCase();
    }

    getClientAccountantsNames(client: Client): string[] {
        const names: string[] = [];
        for (const clientAccess of client.clientAccesses) {
            const accountant = this.accountants.find(byId(clientAccess.accountantId));
            if (accountant) {
                names.push(accountant.fullName);
            }
        }

        return names;
    }

    formatLastClosedTaxPeriod(lastClosedTaxPeriod: LastClosedTaxPeriod): string {
        if (lastClosedTaxPeriod) {
            const taxPeriodType = lastClosedTaxPeriod.type;
            return `${taxPeriodType.charAt(0).toUpperCase()}${lastClosedTaxPeriod.number} / ${lastClosedTaxPeriod.year}`;
        }
        return '-';
    }

    onManageClient(client: Client): void {
        this.manageClient.emit(client);
    }

    onLogin(id: number): void {
        this.login.emit(id);
    }

    onDisconnect(client: Client): void {
        this.disconnectClient.emit(client);
    }

    onOpenClientDetails(client: Client): void {
        this.expandedClient = this.expandedClient === client ? new Client() : client;
        if (!this.expandedClient.id) {
            return;
        }

        if (!client.keyPerformanceIndicators) {
            this.loadKeyPerformanceIndicators.emit(client.id);
        } else {
            this.expandedClientChartData[DEBTOR_BALANCE] = this.chartDataService.convertToChartData(
                this.expandedClient.keyPerformanceIndicators,
                DEBTOR_BALANCE
            );
            this.expandedClientChartData[CREDITOR_BALANCE] = this.chartDataService.convertToChartData(
                this.expandedClient.keyPerformanceIndicators,
                CREDITOR_BALANCE
            );
        }
    }

    getLastKeyPerformanceEntry(client: Client, balance: string): any {
        const length = client.keyPerformanceIndicators[balance].entries.length;
        return client.keyPerformanceIndicators[balance].entries[length - 1];
    }

    getChartData(client: Client, balance: string): ChartData[] | void {
        if (this.expandedClient.id === client.id) {
            return this.expandedClientChartData[balance];
        }
    }

    private existsInAccountants(names: string[], filter: string): boolean {
        let doesExists = false;
        for (const name of names) {
            if (
                name
                    .trim()
                    .toLowerCase()
                    .indexOf(filter) !== -1
            ) {
                doesExists = true;
            }
        }
        return doesExists;
    }
}
