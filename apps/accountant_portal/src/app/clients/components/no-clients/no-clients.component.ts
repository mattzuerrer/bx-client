import { Component, EventEmitter, Output } from '@angular/core';
import { CdnService } from '@bx-client/core/src/environments/service/cdn.service';

import { version } from '../../../../config';

@Component({
    selector: 'app-no-clients',
    templateUrl: 'no-clients.component.html',
    styleUrls: ['no-clients.component.scss']
})
export class NoClientsComponent {
    @Output() addNewClient: EventEmitter<void> = new EventEmitter<void>();

    assetUrl: string = this.cdn.getAssetUrl(version);

    constructor(private cdn: CdnService) {}

    onAddNewClient(): void {
        this.addNewClient.emit();
    }
}
