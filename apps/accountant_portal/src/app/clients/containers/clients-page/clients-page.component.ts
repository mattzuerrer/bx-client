import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MatDialogConfig } from '@angular/material/typings/dialog';
import { LoaderService } from '@bx-client/http';
import { GlobalizeService } from '@bx-client/i18n';
import { byId } from '@bx-client/utils';
import { Subscription } from 'rxjs';

import { ClientsActions } from '../../../shared/actions';
import {
    ACCEPTED_STATE,
    Accountant,
    Client,
    PendingClientInvites,
    Permissions,
    STATUS_ACCEPTED,
    STATUS_PENDING
} from '../../../shared/models';
import { AccountantPortalStateService, AnalyticsService } from '../../../shared/services';
import { AddNewClientDialogComponent, DisconnectClientDialogComponent, ManageClientDialogComponent } from '../../entry-components';

const DIALOG_CONFIG: MatDialogConfig = { width: '432px' };

@Component({
    selector: 'app-clients-page',
    templateUrl: 'clients-page.component.html',
    styleUrls: ['clients-page.component.scss']
})
export class ClientsPageComponent implements OnInit, OnDestroy {
    dateFormat: string;

    myClients: Client[];

    pendingClientInvites: PendingClientInvites[];

    accessPermissions: Permissions;

    private accountantsWithFinishedOnboarding: Accountant[];

    private accountantUser: Accountant;

    private subscriptions: Subscription[] = [];

    constructor(
        public accountantPortalStateService: AccountantPortalStateService,
        private loaderService: LoaderService,
        private globalizeService: GlobalizeService,
        private analyticsService: AnalyticsService,
        private matDialog: MatDialog
    ) {}

    ngOnInit(): void {
        this.dateFormat = this.globalizeService.getFormatFromMatDatepicker();
        this.analyticsService.process();
        this.subscriptions.push(
            this.accountantPortalStateService.accessPermissions$.subscribe(permissions => (this.accessPermissions = permissions))
        );
        this.subscriptions.push(
            this.accountantPortalStateService.accountantUser$.subscribe(accountantUser => (this.accountantUser = accountantUser))
        );
        this.subscriptions.push(
            this.accountantPortalStateService.clients$.subscribe(allClients => {
                this.myClients = allClients.filter(client =>
                    client.clientAccesses.some(access => access.accountantId === this.accountantUser.id && access.status === ACCEPTED_STATE)
                );
            })
        );

        this.subscriptions.push(
            this.accountantPortalStateService.accountantsWithFinishedOnboardin$.subscribe(
                accountants => (this.accountantsWithFinishedOnboarding = accountants)
            )
        );

        this.subscriptions.push(
            this.accountantPortalStateService.pendingClientInvites$.subscribe(
                pendingClientInvites => (this.pendingClientInvites = pendingClientInvites)
            )
        );
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    onLogin(id: number): void {
        this.accountantPortalStateService.dispatch(new ClientsActions.GetLoginLinkAction(id));
    }

    onManageClient(client: Client): void {
        const connectedAccountants = this.getClientConnectedAccountants(client, STATUS_ACCEPTED);
        const accountantsWithPendingInvite = this.getClientConnectedAccountants(client, STATUS_PENDING);
        const notConnectedAccountants = this.accountantsWithFinishedOnboarding.filter(
            accountant => !connectedAccountants.includes(accountant) && !accountantsWithPendingInvite.includes(accountant)
        );
        const dialogRef = this.matDialog.open(ManageClientDialogComponent, DIALOG_CONFIG);

        dialogRef.componentInstance.client = client;
        dialogRef.componentInstance.connectedAccountants = connectedAccountants;
        dialogRef.componentInstance.notConnectedAccountants = notConnectedAccountants;
        dialogRef.componentInstance.accountantsWithPendingInvite = accountantsWithPendingInvite;

        dialogRef.afterClosed().subscribe(accountantsToChange => {
            if (accountantsToChange) {
                if (accountantsToChange.toConnect.length > 0) {
                    this.accountantPortalStateService.dispatch(
                        new ClientsActions.ConnectAction({ clientId: client.id, ids: accountantsToChange.toConnect })
                    );
                }
                for (const accountant of accountantsToChange.toDisconnect) {
                    const accessId = this.getAccessId(client, accountant);
                    this.accountantPortalStateService.dispatch(new ClientsActions.DisconnectAction({ clientId: client.id, id: accessId }));
                }
                for (const accountant of accountantsToChange.toCancelInvitation) {
                    const accessId = this.getAccessId(client, accountant);
                    this.accountantPortalStateService.dispatch(new ClientsActions.RejectAction({ clientId: client.id, id: accessId }));
                }
            }
        });
    }

    onDisconnectClient(client: Client): void {
        const dialogRef = this.matDialog.open(DisconnectClientDialogComponent, DIALOG_CONFIG);
        dialogRef.componentInstance.client = client;

        dialogRef.afterClosed().subscribe(accountantToChange => {
            if (accountantToChange) {
                const access = client.clientAccesses.filter(clientAccess => this.accountantUser.id === clientAccess.accountantId);

                this.accountantPortalStateService.dispatch(
                    new ClientsActions.DisconnectAction({
                        clientId: accountantToChange.id,
                        id: access[0].id
                    })
                );
            }
        });
    }

    onAddNewClient(): void {
        this.matDialog.open(AddNewClientDialogComponent, DIALOG_CONFIG);
    }

    onLoadKeyPerformanceIndicators(clientId: number): void {
        this.loaderService.disableLoader$.next(true);
        this.accountantPortalStateService.dispatch(new ClientsActions.LoadKeyPerformanceIndicatorsAction(clientId));
    }

    onAcceptPendingClientInvite(pendingClientInvite: PendingClientInvites): void {
        this.accountantPortalStateService.dispatch(
            new ClientsActions.AcceptAction({
                clientId: pendingClientInvite.client.id,
                id: pendingClientInvite.id
            })
        );
    }

    onRejectPendingClientInvite(pendingClientInvite: PendingClientInvites): void {
        this.accountantPortalStateService.dispatch(
            new ClientsActions.RejectAction({
                clientId: pendingClientInvite.client.id,
                id: pendingClientInvite.id
            })
        );
    }

    private getClientConnectedAccountants(client: Client, status: string = 'accepted'): Accountant[] {
        const connectedAccountants = [];
        for (const acceptedAccess of client.clientAccesses.filter(access => access.status === status)) {
            const accountant = this.accountantsWithFinishedOnboarding.find(byId(acceptedAccess.accountantId));
            if (accountant) {
                connectedAccountants.push(accountant);
            }
        }

        return connectedAccountants;
    }

    private getAccessId(client: Client, accountant: Accountant): number {
        return client.clientAccesses.find(access => access.accountantId === accountant.id).id;
    }
}
