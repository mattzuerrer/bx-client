import { Component } from '@angular/core';

import { ClientsActions } from '../../../shared/actions';
import { AccountantPortalStateService } from '../../../shared/services';

@Component({
    selector: 'app-add-new-client-dialog',
    templateUrl: 'add-new-client-dialog.component.html',
    styleUrls: ['add-new-client-dialog.component.scss']
})
export class AddNewClientDialogComponent {
    disableAddNewButton = false;

    constructor(private stateService: AccountantPortalStateService) {}

    addNewClient(): void {
        this.disableAddNewButton = true;
        this.stateService.dispatch(new ClientsActions.CreateNewAction());
    }

    get dialogStatus(): string {
        return this.disableAddNewButton ? 'progress' : '';
    }
}
