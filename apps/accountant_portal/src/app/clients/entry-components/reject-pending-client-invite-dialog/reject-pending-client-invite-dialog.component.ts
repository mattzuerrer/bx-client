import { Component, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import { PendingClientInvites } from '../../../shared/models';

@Component({
    selector: 'app-reject-pending-client-invite-dialog',
    templateUrl: 'reject-pending-client-invite-dialog.component.html',
    styleUrls: ['reject-pending-client-invite-dialog.component.scss']
})
export class RejectPendingClientInviteDialogComponent {
    @Input() pendingClientInvite: PendingClientInvites;

    constructor(private dialogRef: MatDialogRef<RejectPendingClientInviteDialogComponent>) {}

    rejectPendingClientInvite(): void {
        this.dialogRef.close(this.pendingClientInvite);
    }
}
