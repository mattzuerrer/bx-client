import { AddNewClientDialogComponent } from './add-new-client-dialog/add-new-client-dialog.component';
import { DisconnectClientDialogComponent } from './disconnect-client-dialog/disconnect-client-dialog.component';
import { ManageClientDialogComponent } from './manage-client-dialog/manage-client-dialog.component';
// tslint:disable-next-line:max-line-length
import { RejectPendingClientInviteDialogComponent } from './reject-pending-client-invite-dialog/reject-pending-client-invite-dialog.component';

export { AddNewClientDialogComponent } from './add-new-client-dialog/add-new-client-dialog.component';
export {
    RejectPendingClientInviteDialogComponent
} from './reject-pending-client-invite-dialog/reject-pending-client-invite-dialog.component';

export { DisconnectClientDialogComponent } from './disconnect-client-dialog/disconnect-client-dialog.component';

export { ManageClientDialogComponent } from './manage-client-dialog/manage-client-dialog.component';

export const entryComponents = [
    AddNewClientDialogComponent,
    DisconnectClientDialogComponent,
    ManageClientDialogComponent,
    RejectPendingClientInviteDialogComponent
];
