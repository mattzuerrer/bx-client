import { Component, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import { ClientsActions } from '../../../shared/actions';
import { Accountant, Client } from '../../../shared/models';
import { AccountantPortalStateService } from '../../../shared/services';

@Component({
    selector: 'app-manage-client-dialog',
    templateUrl: 'manage-client-dialog.component.html',
    styleUrls: ['manage-client-dialog.component.scss']
})
export class ManageClientDialogComponent {
    accountantsToConnect: Accountant[] = [];
    accountantsToDisconnect: Accountant[] = [];
    accountantsToCancelInvitation: Accountant[] = [];

    @Input() client: Client;

    @Input() connectedAccountants: Accountant[];

    @Input() notConnectedAccountants: Accountant[];

    @Input() accountantsWithPendingInvite: Accountant[];

    constructor(private dialogRef: MatDialogRef<ManageClientDialogComponent>, private stateService: AccountantPortalStateService) {}

    onSave(): void {
        const returnArrays = {
            toConnect: this.accountantsToConnect,
            toDisconnect: this.accountantsToDisconnect,
            toCancelInvitation: this.accountantsToCancelInvitation
        };
        this.dialogRef.close(returnArrays);
    }

    onDisconnect(accountant: Accountant): void {
        this.accountantsToDisconnect.push(accountant);
        this.connectedAccountants = this.removeAccountantFromArray(this.connectedAccountants, accountant);
    }

    onCancelDisconnecting(accountant: Accountant): void {
        this.connectedAccountants.push(accountant);
        this.accountantsToDisconnect = this.removeAccountantFromArray(this.accountantsToDisconnect, accountant);
    }

    onConnect(accountant: Accountant): void {
        this.accountantsToConnect.push(accountant);
        this.notConnectedAccountants = this.removeAccountantFromArray(this.notConnectedAccountants, accountant);
    }

    onCancelConnecting(accountant: Accountant): void {
        this.notConnectedAccountants.push(accountant);
        this.accountantsToConnect = this.removeAccountantFromArray(this.accountantsToConnect, accountant);
    }

    onCancelExistingInvite(accountant: Accountant): void {
        this.accountantsToCancelInvitation.push(accountant);
        this.accountantsWithPendingInvite = this.removeAccountantFromArray(this.accountantsWithPendingInvite, accountant);
    }

    onCancelInvitationCanceling(accountant: Accountant): void {
        this.accountantsWithPendingInvite.push(accountant);
        this.accountantsToCancelInvitation = this.removeAccountantFromArray(this.accountantsToCancelInvitation, accountant);
    }

    onResendInvite(accountant: Accountant): void {
        const accessId = this.client.clientAccesses.find(access => access.accountantId === accountant.id).id;
        this.stateService.dispatch(new ClientsActions.ReinviteAction({ id: accessId, clientId: this.client.id }));
    }

    get hasConnectedAccountants(): boolean {
        return this.connectedAccountants.length + this.accountantsWithPendingInvite.length + this.accountantsToConnect.length > 0;
    }

    get hasDisconnectedAccountants(): boolean {
        return this.notConnectedAccountants.length + this.accountantsToCancelInvitation.length + this.accountantsToDisconnect.length > 0;
    }

    private removeAccountantFromArray(array: Accountant[], accountantToRemove: Accountant): Accountant[] {
        return array.filter(accountant => accountant !== accountantToRemove);
    }
}
