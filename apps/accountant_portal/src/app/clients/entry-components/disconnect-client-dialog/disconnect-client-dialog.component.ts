import { Component, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { CdnService } from '@bx-client/core/src/environments/service/cdn.service';

import { version } from '../../../../config';
import { Client } from '../../../shared/models';

@Component({
    selector: 'app-disconnect-client-dialog',
    templateUrl: 'disconnect-client-dialog.component.html',
    styleUrls: ['disconnect-client-dialog.component.scss']
})
export class DisconnectClientDialogComponent {
    @Input() client: Client;

    assetUrl: string = this.cdn.getAssetUrl(version);

    constructor(private dialogRef: MatDialogRef<DisconnectClientDialogComponent>, private cdn: CdnService) {}

    disconnectAccountant(): void {
        this.dialogRef.close(this.client);
    }
}
