import { NgModule } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { SharedModule } from '../shared';

import { routing } from './clients.routes';
import { components } from './components';
import { containers } from './containers';
import { entryComponents } from './entry-components';

@NgModule({
    imports: [routing, SharedModule, NgxChartsModule],
    exports: [containers, components],
    declarations: [...containers, ...components, ...entryComponents],
    entryComponents: [...entryComponents]
})
export class ClientsModule {}
