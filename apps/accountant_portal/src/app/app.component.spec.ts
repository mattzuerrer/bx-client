import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { LoaderService } from '@bx-client/http';
import { MessageService } from '@bx-client/message';

import { AppComponent } from './app.component';
import { BxSpinnerMockComponent, HeaderMockComponent, LoaderOverlayMockComponent } from './shared/mocks/components';
import { AccountantPortalStateService } from './shared/services';

const accountantPortalStateServiceStub = {};

const loaderServiceServiceStub = {};

const messageServiceServiceStub = {};

describe('AppComponent', () => {
    beforeEach(
        async(() => {
            TestBed.configureTestingModule({
                imports: [RouterTestingModule],
                declarations: [AppComponent, LoaderOverlayMockComponent, HeaderMockComponent, BxSpinnerMockComponent],
                providers: [
                    {
                        provide: AccountantPortalStateService,
                        useValue: accountantPortalStateServiceStub
                    },
                    {
                        provide: LoaderService,
                        useValue: loaderServiceServiceStub
                    },
                    {
                        provide: MessageService,
                        useValue: messageServiceServiceStub
                    }
                ]
            }).compileComponents();
        })
    );

    it(
        'should create the app',
        async(() => {
            const fixture = TestBed.createComponent(AppComponent);
            const app = fixture.debugElement.componentInstance;
            expect(app).toBeTruthy();
        })
    );
});
