import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CoreModule } from '@bx-client/core';
import { MessageModule } from '@bx-client/message';
import { NxModule } from '@nrwl/nx';

import { version } from '../config';

import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { SharedModule } from './shared';

@NgModule({
    declarations: [AppComponent],
    imports: [
        routing,
        BrowserModule,
        CoreModule.forRoot(version),
        NxModule.forRoot(),
        SharedModule.forRoot(),
        MessageModule.forRoot(),
        BrowserAnimationsModule
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
