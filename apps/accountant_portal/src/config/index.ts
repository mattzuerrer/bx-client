export const version = '_BEXIO_FE_DEPLOYMENT_REPLACED_VERSION_';
export const featureName = 'accountantPortal';

export const prodBaseApi = '';
export const devBaseApi = 'http://portal.bexio.test';

export const IDEAS_PORTAL_URL = 'https://ideas.bexio.com/?project=TREU';
export const MARKETPLACE_URL = 'https://www.bexio.com/treuhand/marketplace';

export const ROLES = ['accountant', 'owner'];
export const LANGUAGES = [
    {name: 'german', value: 'de_CH'},
    {name: 'french', value: 'fr_CH'},
    {name: 'english', value: 'en_GB'},
    {name: 'italian', value: 'it_CH'}
];

export const LEGAL_ENTITIES = ['sole_proprietorship', 'joint_partnership', 'foundation', 'corporation', 'limited_liability_company'];
export const COUNTRIES = ['CH', 'AT', 'DE'];

export const UPLOAD_LOGO_MAX_SIZE_IN_MB = 1;
export const UPLOAD_LOGO_MIME_TYPES = ['image/png', 'image/jpeg', 'image/gif'];

export const UPLOAD_PDF_MAX_SIZE_IN_MB = 10;
export const UPLOAD_PDF_MIME_TYPES = ['application/pdf'];

export const COMPONENT_SETUP_STATUS_FORBIDDEN = '/403';
export const COMPONENT_SETUP_BASE_URL = '/setup';
export const COMPONENT_SETUP_PASSWORD_URL = '/setup/password';
export const COMPONENT_SETUP_CREATE_NEW_ACCOUNTANT_URL = '/setup/user';
export const COMPONENT_SETUP_JOIN_COMPANY_URL = '/setup/join-company';
export const COMPONENT_SETUP_COMPANY_URL = '/setup/company';
