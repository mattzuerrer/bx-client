# accountant_portal

In order to connect to the backend during local development with Vagrant,
the config of the web server needs to be adapted in the
_accountant_dashboard_ module.

To do so, adjust the file _sites/accountant_dashboard/api/web/.htaccess_ as
follows:

```apache
# api/web/.htaccess

<IfModule mod_rewrite.c>
    …
    # Add this piece of code to the end of <IfModule mod_rewrite.c>
    Header Set Access-Control-Allow-Origin "http://localhost:4200"
    Header Set Access-Control-Allow-Methods "GET, POST, OPTIONS, PUT, DELETE, PATCH"
    Header Set Access-Control-Allow-Credentials true
    Header Set Access-Control-Allow-Headers "Content-Type"
 </IfModule>
```
